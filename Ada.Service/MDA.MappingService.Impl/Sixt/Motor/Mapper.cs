﻿using FileHelpers;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common.Server;
using MDA.MappingService.Sixt.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using MDA.MappingService.Interface;
using MDA.Common;
using Ionic.Zip;


namespace MDA.MappingService.Sixt.Motor
{
    public class Mapper : IMapper
    {
        private Stream _fs;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private object statusTracking;
        private FileHelperEngine _coreClaimDataEngine;
        private FileHelperEngine _claimantDataEngine;
        private List<ClaimData> _lstClaim;
        private List<ClaimantData> _lstClaimant;
              
        
        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
        public string ClaimantFile { get; set; }
        private string _coreClaimDataFile;
        private string _claimDataFile;
        private ClaimData[] _coreClaimData;
        private ClaimantData[] _claimantData;
        private StreamReader _claimDataFileStreamReader;
        private StreamReader _claimantDataStreamReader;


        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));

            _lstClaim = new List<ClaimData>();
        }

        public void AttemptToExtractFilesFromZip()
        {
            try
            {
                if (_fs != null)
                {
                    using (ZipFile zip1 = ZipFile.Read(_fs)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(ClientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);


                            string unpackDirectory = ClientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in extraction of files: " + ex);
            }
        }

        public void AssignFiles()
        {

            try
            {

                ClaimFile = ClientFolder + @"\RF0417 - Sixt Monthly Claims Report - Claim (RS).csv";
                ClaimantFile = ClientFolder + @"\Sixt Claimant Report.csv";
                
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in creating csv file: " + ex);
            }
            
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
                _claimantDataEngine = new FileHelperEngine(typeof(ClaimantData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                _coreClaimData = _coreClaimDataEngine.ReadFile(ClaimFile) as ClaimData[];
                _claimantData = _claimantDataEngine.ReadFile(ClaimantFile) as ClaimantData[];


            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            try
            {
                //if (_debug)
                //{
                //    Console.WriteLine("Start");
                //}

                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helper Engine (core) : ", false);
                }

                _coreClaimData = _coreClaimDataEngine.ReadFile(_coreClaimDataFile) as ClaimData[];
                _claimantData = _claimantDataEngine.ReadFile(_claimDataFile) as ClaimantData[];

            }

            catch (Exception ex)
            {
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "PopulateFileHelpersEngines Error : " + ex.Message, true);
                }

                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers 
                if (_coreClaimData != null)
                    _lstClaim = _coreClaimData
                        .GroupBy(i => i.AccidentNum)
                        .Select(g => g.First())
                        .ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }

        }

        public void Translate()
        {
            //int _skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["LVSkipNum"]);
            //int _takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["LVTakeNum"]);
            
            foreach (var claim in _lstClaim) //.Skip(_skipValue).Take(_takeValue))
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                #region Claim

                motorClaim.ClaimNumber = claim.ClaimNumber.Remove(claim.ClaimNumber.Length - 6);
                motorClaim.IncidentDate = Convert.ToDateTime(claim.AccidentDate);
                motorClaim.ClaimType_Id = (int)ClaimType.Motor;

                #endregion

                #region Claim Info

                motorClaim.ExtraClaimInfo.ClaimNotificationDate =  Convert.ToDateTime(claim.ReportedDate);
                motorClaim.ExtraClaimInfo.ClaimCode = claim.ClientReference + " , " + claim.CoverageCode + " , " + claim.LitigatedFlag;
                motorClaim.ExtraClaimInfo.IncidentCircumstances = claim.ClaimDescription;
                motorClaim.ExtraClaimInfo.Reserve = Convert.ToDecimal(claim.TotalIncurred);
                motorClaim.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(claim.TotalPaid);

                switch (claim.StatusCode.ToUpper())
                {
                    case "OP":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                        break;
                    case "CL":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                        break;
                    default:
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                        break;
                }

                #endregion

                #region Policy

                motorClaim.Policy.Insurer = "Sixt";

                #endregion

                #region Insured Vehicle

                if (claim.VehicleID != null)
                {
                    PipelineVehicle insuredVehicle = new PipelineVehicle();

                    insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    insuredVehicle.VehicleRegistration = claim.VehicleID;

                    #region Insured Driver

                    if (claim.DriverFullName != null)
                    {
                        PipelinePerson insuredDriver = new PipelinePerson();

                        insuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        insuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        string[] words = claim.DriverFullName.Split(' ');

                        if (words.Count() == 1)
                        {
                            insuredDriver.LastName = words[0].Trim();   
                        }
                        else if (words.Count() == 2)
                        {
                            insuredDriver.FirstName = words[0].Trim();
                            insuredDriver.LastName = words[1].Trim();                            
                        }
                        else if (words.Count() == 3)
                        {
                            insuredDriver.FirstName = words[0].Trim();
                            insuredDriver.MiddleName = words[1].Trim();
                            insuredDriver.LastName = words[2].Trim();
                        }
                        else if (words.Count() == 4)
                        {
                            insuredDriver.FirstName = words[0].Trim();
                            insuredDriver.MiddleName = words[1].Trim() + " " + words[2].Trim();
                            insuredDriver.LastName = words[3].Trim();                      
                        }
                        else if (words.Count() == 5)
                        {
                            insuredDriver.FirstName = words[0].Trim();
                            insuredDriver.MiddleName = words[1].Trim() + " " + words[2].Trim() + " " + words[3].Trim();
                            insuredDriver.LastName = words[4].Trim();
                        }
                        else if (words.Count() == 6)
                        {
                            insuredDriver.FirstName = words[0].Trim();
                            insuredDriver.MiddleName = words[1].Trim() + " " + words[2].Trim() + " " + words[3].Trim() + " " + words[4].Trim();
                            insuredDriver.LastName = words[5].Trim();
                        }
                        else
                        {
                            insuredDriver.LastName = words[0].Trim();
                        }

                        insuredVehicle.People.Add(insuredDriver);
                    }

                    #endregion

                    motorClaim.Vehicles.Add(insuredVehicle);
                }

                #endregion

                #region Claimant Vehicle

                var claimantList = _claimantData.Where(x => x.ClaimNumber.Substring(0,13) == claim.ClaimNumber.Substring(0,13)).ToList();

                if (claimantList.Count() > 0)
                {
                    PipelineVehicle tpv = new PipelineVehicle();

                    #region Claimant

                    int personCount = 0;

                    foreach (var c in claimantList)
                    {
                        #region Claimant Address

                        PipelineAddress address = new PipelineAddress();

                        address.Street = c.Address1;
                        if (c.City != "NK")
                        {
                            address.Town = c.City;
                        }
                        address.PostCode = c.PostalCode;

                        #endregion

                        #region Claimant Email
                        PipelineEmail email = new PipelineEmail();
                        email.EmailAddress = c.ClaimantEmailID;
                        #endregion

                        #region Cliamant Telephone

                        PipelineTelephone telephone = new PipelineTelephone();
                        telephone.ClientSuppliedNumber = c.ClaimantPhoneNumber;

                        #endregion

                        #region Cliamant Organisation 

                        PipelineOrganisation solicitor = new PipelineOrganisation();
                        solicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;
                        solicitor.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                        
                        solicitor.OrganisationName = c.ClientDefineCode61.Trim();

                        #endregion

                        string[] words = c.ClaimantName.Split(',');

                        if (words.Count() == 2)
                        {
                            PipelinePerson claimant = new PipelinePerson();

                            if(!string.IsNullOrEmpty(c.BirthDate))
                            {
                                DateTime dob = Convert.ToDateTime(c.BirthDate);
                                claimant.DateOfBirth = dob;
                            }

                            claimant.Gender_Id = (int)GenderHelper.GetGender(c.Sex);
                            claimant.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                            claimant.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                            claimant.FirstName = words[1].Trim();
                            claimant.LastName = words[0].Trim();

                            if (!string.IsNullOrWhiteSpace(address.Street) || !string.IsNullOrWhiteSpace(address.Town) || !string.IsNullOrWhiteSpace(address.PostCode))
                            {
                                claimant.Addresses.Add(address);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClaimantEmailID))
                            {
                                claimant.EmailAddresses.Add(email);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClaimantPhoneNumber))
                            {
                                claimant.Telephones.Add(telephone);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClientDefineCode61))
                            {
                                claimant.Organisations.Add(solicitor);
                            }

                            tpv.People.Add(claimant);

                            personCount++;
                        }
                        else if (words.Count() == 3)
                        {
                            PipelinePerson claimant = new PipelinePerson();

                            claimant.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                            claimant.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                            claimant.FirstName = words[2].Trim();
                            claimant.MiddleName = words[1].Trim();
                            claimant.LastName = words[0].Trim();

                            if (!string.IsNullOrWhiteSpace(address.Street) || !string.IsNullOrWhiteSpace(address.Town) || !string.IsNullOrWhiteSpace(address.PostCode))
                            {
                                claimant.Addresses.Add(address);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClaimantEmailID))
                            {
                                claimant.EmailAddresses.Add(email);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClaimantPhoneNumber))
                            {
                                claimant.Telephones.Add(telephone);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClientDefineCode61))
                            {
                                claimant.Organisations.Add(solicitor);
                            }

                            tpv.People.Add(claimant);

                            personCount++;
                        }
                        else if (words.Count() > 3)
                        {
                            PipelinePerson claimant = new PipelinePerson();

                            claimant.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                            claimant.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                            claimant.LastName = words.ToString().Trim();

                            if (!string.IsNullOrWhiteSpace(address.Street) || !string.IsNullOrWhiteSpace(address.Town) || !string.IsNullOrWhiteSpace(address.PostCode))
                            {
                                claimant.Addresses.Add(address);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClaimantEmailID))
                            {
                                claimant.EmailAddresses.Add(email);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClaimantPhoneNumber))
                            {
                                claimant.Telephones.Add(telephone);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClientDefineCode61))
                            {
                                claimant.Organisations.Add(solicitor);
                            }

                            tpv.People.Add(claimant);

                            personCount++;
                        }
                        else if (words.Count() == 1)
                        {
                            PipelineOrganisation claimant = new PipelineOrganisation();

                            claimant.OrganisationName = words[0].Trim();

                            if (!string.IsNullOrWhiteSpace(address.Street) || !string.IsNullOrWhiteSpace(address.Town) || !string.IsNullOrWhiteSpace(address.PostCode))
                            {
                                claimant.Addresses.Add(address);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClaimantEmailID))
                            {
                                claimant.EmailAddresses.Add(email);
                            }

                            if (!string.IsNullOrWhiteSpace(c.ClaimantPhoneNumber))
                            {
                                claimant.Telephones.Add(telephone);
                            }

                            motorClaim.Organisations.Add(claimant);
                        }

                    }

                    #endregion

                    if (personCount > 0)
                    {
                        motorClaim.Vehicles.Add(tpv);
                    }                    
                }

                #endregion

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in motorClaim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;

                        foreach (var person in vehicle.People)
                        {
                            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                            {
                                insuredDriverCheck = true;
                            }

                            if (insuredDriverCheck == false && vehicle.People.Count() == 1)
                            {
                                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                insuredDriverCheck = true;
                            }
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {
                    PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                    defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                    }

                    motorClaim.Vehicles.Add(defaultInsuredVehicle);
                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in motorClaim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion

                if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;

            }
        }

    }
}
