﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.LV.Motor.Model;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics;

namespace MDA.MappingService.LV.Motor
{
    public class Mapper : IMapper
    {
        private Stream _fs;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string FilePath { get; set; }
        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
        public string PolicyFile { get; set; }
        public FileHelperEngine ClaimEngine { get; set; }
        public FileHelperEngine PolicyEngine { get; set; }
        public List<string> defaultAddressesForClient { get; set; }
        public List<string> defaultPeopleForClient { get; set; }
        public List<string> defaultOrganisationsForClient { get; set; }

        private string _coreClaimDataFile;
        private string _vehicleClaimDataFile;
        private bool _debug;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private List<string> _uniqueClaimNumberList;
        private string _claimCode;
        private IEnumerable<ClaimData> _policyItemRows;
        private IEnumerable<ClaimData> _claimItemRows;
        private string _filePath = null;
        private bool _deleteExtractedFiles;
        private int _claimCount = 0;
        private string _clientFolder;
        private StreamReader _claimDataFileStreamReader;

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking, List<string> defaultAddressesForClient, List<string> defaultPeopleForClient, List<string> defaultOrganisationsForClient)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;
            this.defaultAddressesForClient = defaultAddressesForClient;
            this.defaultPeopleForClient = defaultPeopleForClient;
            this.defaultOrganisationsForClient = defaultOrganisationsForClient;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void AssignFiles()
        {
            if (_fs == null)
            {
                FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\LV=\Data\Data Received 21.04";
                ClaimFile = FolderPath + @"\LVFS_MBC2_GIOS_20150415.txt";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(_fs);
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }
            }
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                if (_fs == null)
                {
                    _coreClaimData = _coreClaimDataEngine.ReadFile(ClaimFile) as ClaimData[];
                }
                else
                {
                    _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            try
            {
                if (_debug)
                {
                    Console.WriteLine("Start");
                }

                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helper Engine (core) : ", false);
                }

                _coreClaimData = _coreClaimDataEngine.ReadFile(_coreClaimDataFile) as ClaimData[];
            }
            catch (Exception ex)
            {
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "PopulateFileHelpersEngines Error : " + ex.Message, true);
                }

                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                ////// Get Distinct Claim Numbers

                //////var list = _coreClaimData.Where(x => x.Row_Type == "CLAIM_MOTOR"
                //////                                      && x.Data_Column_17 != ""
                //////                                      && x.Enquiry_External_UID != ""
                //////                                      ).ToList();

                //////foreach (var item in list)
                //////{                    
                //////    var num = Convert.ToDouble(item.Data_Column_17);
                //////    if (num > 5500.00)
                //////    {
                //////        _uniqueClaimNumberList.Add(item.Enquiry_External_UID);
                //////    }
                //////}

                ////// Get a list of claim references where the liability is not non fault
                ////var faultClaims = (from c in _coreClaimData
                ////                   where c.Row_Type == "ENQUIRY_ADDITIONAL_STRING"
                ////                   && c.Data_Column_11 != "NON FAULT"
                ////                   && c.Enquiry_External_UID != ""
                ////                   select c.Enquiry_External_UID);

                ////// Get the Claim Reserve from Claim Motor
                ////var list = _coreClaimData.Where(x => x.Row_Type == "CLAIM_MOTOR"
                ////                                      && x.Data_Column_17 != ""
                ////                                    && faultClaims.Contains(x.Enquiry_External_UID)
                ////                                    ).ToList();

                ////// Check the reserve is greater than 5500
                ////foreach (var item in list)
                ////{
                ////    var num = Convert.ToDouble(item.Data_Column_17);
                ////    if (num > 5500.00)
                ////    {
                ////        _uniqueClaimNumberList.Add(item.Enquiry_External_UID);
                ////    }
                ////}

                // Get a list of claim references where the liability is not non fault
                var faultClaims = (from c in _coreClaimData.AsParallel()
                                   where c.Row_Type == "ENQUIRY_ADDITIONAL_STRING"
                                   && c.Data_Column_11 != "NON FAULT"
                                   && c.Enquiry_External_UID != ""
                                   select c.Enquiry_External_UID);

                double value = 5500.00;

                // If reserve != "" && reserve is > 5500
                // OR reserve != null && payment != null && reserve - payment > 5.5

                _uniqueClaimNumberList = _coreClaimData.AsParallel()
                                                    .Where(x => x.Row_Type == "CLAIM_MOTOR"

                                                         && ((x.Data_Column_17 != "" && x.Data_Column_18 == "" && Convert.ToDouble(x.Data_Column_17) > value) || (x.Data_Column_17 != "" && x.Data_Column_18 != "" && (Math.Abs(Convert.ToDouble(x.Data_Column_17) - Convert.ToDouble(x.Data_Column_18)) > value)))
                                                         && faultClaims.Contains(x.Enquiry_External_UID)
                                                         && x.Enquiry_External_UID != ""
                                                         )
                                                     .Select(s => s.Enquiry_External_UID)
                                                     .Distinct()
                                                     .ToList();

            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        }

        public void Translate()
        {
            int _skipValue = (ConfigurationManager.AppSettings["LVSkipNum"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["LVSkipNum"]) : 0);
            int _takeValue = (ConfigurationManager.AppSettings["LVTakeNum"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["LVTakeNum"]) : 500000);

            foreach (var claim in _uniqueClaimNumberList.Skip(_skipValue).Take(_takeValue))
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.AsParallel().Where(x => x.Enquiry_External_UID == claim).ToList();

                if (uniqueClaim != null)
                {
                    var systemName = (from sn in uniqueClaim
                                      where sn.Row_Type == "ENQUIRY_ADDITIONAL_STRING"
                                      select sn.Data_Column_5).FirstOrDefault();

                    #region Claim

                    var claimInfo = uniqueClaim.Where(x => x.Row_Type == "CLAIM_MOTOR").FirstOrDefault();

                    var additionalClaimInfo = uniqueClaim.Where(x => x.Row_Type == "ENQUIRY_ADDITIONAL_STRING").FirstOrDefault();

                    try
                    {
                        if (!string.IsNullOrEmpty(claimInfo.Enquiry_External_UID))
                        {
                            motorClaim.ClaimNumber = claimInfo.Enquiry_External_UID;
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_6))
                        {
                            DateTime incidentDate = _ConvertDate(claimInfo.Data_Column_6);
                            motorClaim.IncidentDate = incidentDate;
                        }

                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;

                        #region Claim Status

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_9))
                        {
                            switch (claimInfo.Data_Column_9.ToUpper())
                            {
                                case "CLAIM FNOL":
                                case "CLAIM UPDATE":
                                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                            break;
                                case "CLAIM CLOSED":
                                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                            break;
                                case "CLAIM REOPENED":
                                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                                            break;
                                        default:
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                            break;
                                    }
                                }

                        #endregion Claim Status

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_4))
                        {
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = _ConvertDate(claimInfo.Data_Column_4);
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_15))
                        {
                            motorClaim.ExtraClaimInfo.ClaimCode = claimInfo.Data_Column_15;
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_26))
                        {
                            motorClaim.ExtraClaimInfo.IncidentLocation = claimInfo.Data_Column_26;
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_25))
                        {
                            motorClaim.ExtraClaimInfo.IncidentCircumstances = claimInfo.Data_Column_25;
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_17))
                        {
                            motorClaim.ExtraClaimInfo.Reserve = Convert.ToDecimal(claimInfo.Data_Column_17);
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_18))
                        {
                            motorClaim.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(claimInfo.Data_Column_18);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion Claim

                    #region Policy

                    if (!string.IsNullOrEmpty(claimInfo.Data_Column_12))
                    {
                        motorClaim.Policy.PolicyNumber = claimInfo.Data_Column_12;
                    }

                    if (!string.IsNullOrEmpty(claimInfo.Data_Column_1))
                    {
                        if (claimInfo.Data_Column_1 == "MOTOR BROKER" || claimInfo.Data_Column_1 == "MOTOR DIRECT")
                        {
                            motorClaim.Policy.PolicyType_Id = (int)PolicyType.PersonalMotor;
                        }
                        else if (claimInfo.Data_Column_1 == "COMMERCIAL")
                        {
                            motorClaim.Policy.PolicyType_Id = (int)PolicyType.CommercialMotor;
                        }
                    }

                    //var additionalClaimInfo = uniqueClaim.Where(x => x.Row_Type == "ENQUIRY_ADDITIONAL_STRING").FirstOrDefault();

                    motorClaim.Policy.Insurer = "LV=";

                    if (!string.IsNullOrEmpty(additionalClaimInfo.Data_Column_6))
                    {
                        motorClaim.Policy.InsurerTradingName = additionalClaimInfo.Data_Column_6;
                    }

                    if (!string.IsNullOrEmpty(additionalClaimInfo.Data_Column_15))
                    {
                        motorClaim.Policy.Broker = additionalClaimInfo.Data_Column_15;
                    }

                    #endregion Policy

                    #region Vehicles

                    var vehicleInfo = uniqueClaim.Where(x => x.Row_Type == "VEHICLE").ToList();

                    foreach (var v in vehicleInfo)
                    {
                        PipelineVehicle vehicle = new PipelineVehicle();

                        try
                        {
                            #region Vehicle Involvement Groups (Incident2VehicleLinkType)

                            if (v.Entity_Type.Contains("INV"))
                            {
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                            }

                            if (v.Entity_Type.Contains("TPV"))
                            {
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                            }

                            #endregion Vehicle Involvement Groups (Incident2VehcileLinkType)

                            if (v.Data_Column_1 != null)
                            {
                                vehicle.VehicleRegistration = v.Data_Column_1;
                            }

                            if (v.Data_Column_19 != null)
                            {
                                vehicle.VIN = v.Data_Column_19;
                            }

                            if (v.Data_Column_8 != null)
                            {
                                vehicle.VehicleMake = v.Data_Column_8.ToUpper();
                            }

                            if (v.Data_Column_9 != null)
                            {
                                vehicle.VehicleModel = v.Data_Column_9.ToUpper();
                            }

                            if (v.Data_Column_23 != null)
                            {
                                vehicle.DamageDescription = v.Data_Column_23.ToUpper();
                            }

                            if (v.Data_Column_3 != null)
                            {
                                vehicle.EngineCapacity = v.Data_Column_3.ToUpper();
                            }

                            #region Vehicle Colour

                            if (!string.IsNullOrEmpty(v.Data_Column_20))
                            {
                                switch (v.Data_Column_20.ToUpper())
                                {
                                    case "SILVER":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Silver;
                                        break;

                                    case "BEIGE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Beige;
                                        break;

                                    case "BLACK":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Black;
                                        break;

                                    case "BLUE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Blue;
                                        break;

                                    case "BRONZE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Bronze;
                                        break;

                                    case "BROWN":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Brown;
                                        break;

                                    case "CREAM":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Cream;
                                        break;

                                    case "GOLD":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Gold;
                                        break;

                                    case "GREEN":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Green;
                                        break;

                                    case "GREY":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Grey;
                                        break;

                                    case "MAROON":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Maroon;
                                        break;

                                    case "MAUVE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Mauve;
                                        break;

                                    case "MIXED":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Unknown;
                                        break;

                                    case "ORANGE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Orange;
                                        break;

                                    case "PINK":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Pink;
                                        break;

                                    case "PURPLE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Purple;
                                        break;

                                    case "RED":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Red;
                                        break;

                                    case "TURQUOISE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Turquoise;
                                        break;

                                    case "WHITE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.White;
                                        break;

                                    case "YELLOW":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Yellow;
                                        break;

                                    default:
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Unknown;
                                        break;
                                }
                            }

                            #endregion Vehicle Colour

                            #region Vehicle Type

                            if (v.Data_Column_15 != null)
                            {
                                switch (additionalClaimInfo.Data_Column_15.ToUpper())
                                {
                                    case "CARAVAN":
                                    case "TRACTOR ONLY":
                                        vehicle.VehicleType_Id = (int)VehicleType.Unknown;
                                        break;

                                    case "PASSENGER CAR":
                                        vehicle.VehicleType_Id = (int)VehicleType.Car;
                                        break;

                                    case "LORRY":
                                        vehicle.VehicleType_Id = (int)VehicleType.Lorry;
                                        break;

                                    case "VAN":
                                        vehicle.VehicleType_Id = (int)VehicleType.Van;
                                        break;

                                    case "MOTORCYCLE":
                                        vehicle.VehicleType_Id = (int)VehicleType.Motorcycle;
                                        break;

                                    case "BICYCLE":
                                        vehicle.VehicleType_Id = (int)VehicleType.Bicycle;
                                        break;

                                    default:
                                        vehicle.VehicleType_Id = (int)VehicleType.Unknown;
                                        break;
                                }
                            }

                            #endregion Vehicle Type

                            #region Category of Loss

                            if (v.Data_Column_27 != null)
                            {
                                switch (additionalClaimInfo.Data_Column_27.ToUpper())
                                {
                                    case "A":
                                        vehicle.I2V_LinkData.VehicleCategoryOfLoss_Id = (int)VehicleCategoryOfLoss.CatA;
                                        break;

                                    case "B":
                                        vehicle.I2V_LinkData.VehicleCategoryOfLoss_Id = (int)VehicleCategoryOfLoss.CatB;
                                        break;

                                    case "C":
                                        vehicle.I2V_LinkData.VehicleCategoryOfLoss_Id = (int)VehicleCategoryOfLoss.CatC;
                                        break;

                                    case "D":
                                        vehicle.I2V_LinkData.VehicleCategoryOfLoss_Id = (int)VehicleCategoryOfLoss.CatD;
                                        break;

                                    default:
                                        vehicle.I2V_LinkData.VehicleCategoryOfLoss_Id = (int)VehicleCategoryOfLoss.Unknown;
                                        break;
                                }
                            }

                            #endregion Category of Loss
                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping vehicle information: " + ex);
                        }

                        #region Vehicle People

                        var assetRelationshipInfo = (from a in uniqueClaim
                                                     where a.Row_Type == "ASSET_RELATIONSHIP"
                                                     && a.Data_Column_1 == v.Entity_External_UID
                                                     select a.Party_External_UID).ToList();

                        var personInfo = (from pm in uniqueClaim
                                          where pm.Row_Type == "PARTY_MOTOR"
                                          && assetRelationshipInfo.Contains(pm.Party_External_UID)
                                          && pm.Data_Column_20 == ""
                                          select pm).ToList();

                        foreach (var p in personInfo)
                        {
                            PipelinePerson person = new PipelinePerson();

                            bool ignorePartyType = false;

                            try
                            {
                                #region Occupation

                                var occupationList = (from o in uniqueClaim
                                                      where o.Row_Type == "EMPLOYMENT"
                                                      && o.Party_External_UID == p.Party_External_UID
                                                      select o.Data_Column_4.ToUpper()).ToList();

                                if (occupationList != null)
                                {
                                    foreach (var occupation in occupationList)
                                    {
                                        person.Occupation += occupation + " ";
                                    }
                                }

                                #endregion Occupation

                                #region NINumber

                                var niNumbers = (from o in uniqueClaim
                                                 where o.Row_Type == "IDENTIFICATION"
                                                 && o.Party_External_UID == p.Party_External_UID
                                                 select o.Data_Column_1).ToList();

                                if (niNumbers != null)
                                {
                                    foreach (var niNumber in niNumbers)
                                    {
                                        if (!string.IsNullOrWhiteSpace(niNumber))
                                        {
                                            PipelineNINumber pNiNumber = new PipelineNINumber();

                                            pNiNumber.NINumber1 = niNumber;

                                            person.NINumbers.Add(pNiNumber);
                                        }
                                    }
                                }

                                #endregion NINumber

                                #region PartyTypes

                                var partyTypeInfo = (from pti in uniqueClaim
                                                     where pti.Row_Type == "PARTY_ADDITIONAL_NVP"
                                                     && pti.Party_External_UID == p.Party_External_UID
                                                     select pti.Data_Column_5).ToList();

                                #region Claim Centre
                                if (systemName.ToUpper() == "CLAIMCENTER")
                                {
                                    var fullEntityTypes = (from a in uniqueClaim
                                                           where a.Row_Type == "ASSET_RELATIONSHIP"
                                                           && a.Party_External_UID == p.Party_External_UID
                                                           select a.Entity_Type.ToUpper()).ToList();

                                    List<string> trimmedEntityTypes = new List<string>();

                                    foreach (var a in fullEntityTypes)
                                    {
                                        var output = Regex.Replace(a, @"[\d-]", string.Empty);

                                        if (!string.IsNullOrEmpty(output))
                                        {
                                            trimmedEntityTypes.Add(output);
                                        }
                                    }

                                    #region Orgs
                                    if (p.Party_Type == "DOC" || p.Party_Type == "TPDOC")
                                    {
                                        ignorePartyType = true;

                                        PipelineOrganisation organisation = new PipelineOrganisation();

                                        try
                                        {
                                            if (p.Data_Column_3 != null || p.Data_Column_4 != null || p.Data_Column_5 != null)
                                            {
                                                organisation.OrganisationName = p.Data_Column_3 + " " + p.Data_Column_4 + " " + p.Data_Column_5;

                                                bool ignoreOrganisationFlag = _OrganisationMatch(organisation);

                                                if (ignoreOrganisationFlag)
                                                {
                                                    organisation.OrganisationName = "";
                                                }
                                            }

                                            organisation.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                                            organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;

                                            #region Address

                                            var orgAddresses = (from a in uniqueClaim
                                                                where a.Row_Type == "ADDRESS"
                                                                && a.Party_External_UID == p.Party_External_UID
                                                                select a).ToList();

                                            foreach (var a in orgAddresses)
                                            {
                                                PipelineAddress orgAddress = new PipelineAddress();

                                                orgAddress.SubBuilding = a.Data_Column_1;
                                                orgAddress.Building = a.Data_Column_2;
                                                orgAddress.BuildingNumber = a.Data_Column_3;
                                                orgAddress.Street = a.Data_Column_4;
                                                orgAddress.Town = a.Data_Column_6;
                                                orgAddress.PostCode = a.Data_Column_8;

                                                if (a.Entity_Type == "CORR")
                                                {
                                                    orgAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                                                }

                                                bool ignoreAddressFlag = _AddressMatch(orgAddress);

                                                if (!ignoreAddressFlag)
                                                    organisation.Addresses.Add(orgAddress);
                                            }

                                            #endregion Address

                                            #region Contact Details

                                            var orgPartyContactInfo = (from pci in uniqueClaim
                                                                       where pci.Row_Type == "CONTACT"
                                                                       && pci.Party_External_UID == p.Party_External_UID
                                                                       select pci).ToList();

                                            foreach (var pci in orgPartyContactInfo)
                                            {
                                                if (pci.Entity_Type == "CORRESTEL1" || pci.Entity_Type == "WRKTEL" || pci.Entity_Type == "CONTEL" || pci.Entity_Type == "HOMTEL" || pci.Entity_Type == "OTHTEL")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);
                                                }
                                                else if (pci.Entity_Type == "CORRESTEL2")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.Unknown;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);
                                                }
                                                else if (pci.Entity_Type == "MOBILE")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);
                                                }
                                                else if (pci.Entity_Type == "GENFAX" || pci.Entity_Type == "WRKFAX")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.FAX;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);
                                                }
                                                else if (pci.Entity_Type == "GENEML" || pci.Entity_Type == "WRKEML")
                                                {
                                                    PipelineEmail email = new PipelineEmail();
                                                    email.EmailAddress = pci.Data_Column_1;
                                                    organisation.EmailAddresses.Add(email);
                                                }
                                            }

                                            #endregion Contact Details
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new CustomException("Error in mapping organisation information: " + ex);
                                        }

                                        motorClaim.Organisations.Add(organisation);
                                    }
                                    #endregion Orgs

                                    else
                                    {
                                        ignorePartyType = _PartyType(p.Party_Type, person, partyTypeInfo, trimmedEntityTypes, vehicle.I2V_LinkData.Incident2VehicleLinkType_Id);
                                    }
                                }
                                #endregion Claim Centre

                                #region GIOS
                                else if (systemName.ToUpper() == "GIOS")
                                {
                                    ignorePartyType = _PartyType(p.Party_Type, person, partyTypeInfo);
                                }
                                #endregion GIOS

                                #endregion PartyTypes


                                #region Saltutation

                                if (!string.IsNullOrEmpty(p.Data_Column_1))
                                {
                                    switch (p.Data_Column_1.ToUpper())
                                    {
                                        case "MR":
                                            person.Salutation_Id = (int)Salutation.Mr;
                                            person.Gender_Id = (int)Gender.Male;
                                            break;

                                        case "MRS":
                                        case "MRS.":
                                            person.Salutation_Id = (int)Salutation.Mrs;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;

                                        case "MS":
                                            person.Salutation_Id = (int)Salutation.Ms;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;

                                        case "MISS":
                                            person.Salutation_Id = (int)Salutation.Miss;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;

                                        case "MASTER":
                                            person.Salutation_Id = (int)Salutation.Master;
                                            person.Gender_Id = (int)Gender.Male;
                                            break;

                                        case "DR":
                                            person.Salutation_Id = (int)Salutation.Dr;
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;

                                        case "REV":
                                            person.Salutation_Id = (int)Salutation.Reverend;
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;

                                        default:
                                            person.Salutation_Id = (int)Salutation.Unknown;
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;
                                    }
                                }

                                #endregion Saltutation

                                if (!string.IsNullOrEmpty(p.Data_Column_3))
                                {
                                    person.FirstName = p.Data_Column_3.ToUpper();
                                }

                                if (!string.IsNullOrEmpty(p.Data_Column_4))
                                {
                                    person.MiddleName = p.Data_Column_4.ToUpper();
                                }


                                if (p.Party_Type == "PAYE")
                                {
                                    string[] names = System.Text.RegularExpressions.Regex.Split(p.Data_Column_5, @"\s+");
                                    person.LastName = names[2].ToUpper();
                                }
                                else
                                {
                                    person.LastName = p.Data_Column_5.ToUpper();
                                }

                                if (!string.IsNullOrEmpty(p.Data_Column_6))
                                {
                                    DateTime dateOfBirth = _ConvertDate(p.Data_Column_6);
                                    person.DateOfBirth = dateOfBirth;
                                }

                                #region Gender

                                if (!string.IsNullOrEmpty(p.Data_Column_9))
                                {
                                    switch (p.Data_Column_9.ToUpper())
                                    {
                                        case "M":
                                            person.Gender_Id = (int)Gender.Male;
                                            break;

                                        case "F":
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                    }
                                }

                                #endregion Gender

                                #region Address

                                var addresses = (from a in uniqueClaim
                                                 where a.Row_Type == "ADDRESS"
                                                 && a.Party_External_UID == p.Party_External_UID
                                                 select a).ToList();

                                foreach (var a in addresses)
                                {
                                    PipelineAddress address = new PipelineAddress();

                                    address.SubBuilding = a.Data_Column_1.ToUpper();
                                    address.Building = a.Data_Column_2.ToUpper();
                                    address.BuildingNumber = a.Data_Column_3.ToUpper();
                                    address.Street = a.Data_Column_4.ToUpper();
                                    address.Town = a.Data_Column_6.ToUpper();
                                    address.PostCode = a.Data_Column_8.ToUpper();

                                    if (a.Entity_Type == "CORR")
                                    {
                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                    }

                                    bool ignoreAddressFlag = _AddressMatch(address);

                                    if (!ignoreAddressFlag)
                                        person.Addresses.Add(address);
                                }

                                #endregion Address

                                #region Contact Details

                                var partyContactInfo = (from pci in uniqueClaim
                                                        where pci.Row_Type == "CONTACT"
                                                        && pci.Party_External_UID == p.Party_External_UID
                                                        select pci).ToList();

                                foreach (var pci in partyContactInfo)
                                {
                                    if (pci.Entity_Type == "CORRESTEL1" || pci.Entity_Type == "WRKTEL" || pci.Entity_Type == "CONTEL" || pci.Entity_Type == "HOMTEL" || pci.Entity_Type == "OTHTEL")
                                    {
                                        PipelineTelephone telephone = new PipelineTelephone();
                                        telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                        person.Telephones.Add(telephone);
                                    }
                                    else if (pci.Entity_Type == "CORRESTEL2")
                                    {
                                        PipelineTelephone telephone = new PipelineTelephone();
                                        telephone.TelephoneType_Id = (int)TelephoneType.Unknown;
                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                        person.Telephones.Add(telephone);
                                    }
                                    else if (pci.Entity_Type == "MOBILE")
                                    {
                                        PipelineTelephone telephone = new PipelineTelephone();
                                        telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                        person.Telephones.Add(telephone);
                                    }
                                    else if (pci.Entity_Type == "GENFAX" || pci.Entity_Type == "WRKFAX")
                                    {
                                        PipelineTelephone telephone = new PipelineTelephone();
                                        telephone.TelephoneType_Id = (int)TelephoneType.FAX;
                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                        person.Telephones.Add(telephone);
                                    }
                                    else if (pci.Entity_Type == "GENEML" || pci.Entity_Type == "WRKEML")
                                    {
                                        PipelineEmail email = new PipelineEmail();
                                        email.EmailAddress = pci.Data_Column_1;
                                        person.EmailAddresses.Add(email);
                                    }
                                }

                                #endregion Contact Details

                                if (person.FirstName != null && person.LastName != null)
                                {
                                    bool ignorePersonFlag = _PersonMatch(person);

                                    if (ignorePersonFlag)
                                    {
                                        person.FirstName = "";
                                        person.LastName = "";
                                    }
                                }

                                if (!ignorePartyType)
                                {
                                    vehicle.People.Add(person);
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new CustomException("Error in mapping person information: " + ex);
                            }
                        }

                        #endregion Vehicle People

                        motorClaim.Vehicles.Add(vehicle);
                    }

                    #endregion Vehicles

                    #region Organisations

                    var organisations = (from p in uniqueClaim
                                         where p.Row_Type == "PARTY_MOTOR"
                                         && p.Data_Column_20 != ""
                                         select p).ToList();

                    foreach (var org in organisations)
                    {
                        PipelineOrganisation organisation = new PipelineOrganisation();

                        bool ignorePartyType = false;

                        try
                        {
                            #region Party Type

                            if (systemName.ToUpper() == "CLAIMCENTER")
                            {
                                ignorePartyType = _PartyType(org.Party_Type, organisation);
                            }
                            else if (systemName.ToUpper() == "GIOS")
                            {
                                var partyTypeInfo = (from pti in uniqueClaim
                                                     where pti.Row_Type == "PARTY_ADDITIONAL_NVP"
                                                     && pti.Party_External_UID == org.Party_External_UID
                                                     select pti.Data_Column_5).ToList();

                                ignorePartyType = _PartyType(org.Party_Type, organisation, partyTypeInfo);
                            }

                            #endregion Party Type

                            if (org.Data_Column_20 != null)
                            {
                                organisation.OrganisationName = org.Data_Column_20.ToUpper();

                                bool ignoreOrganisationFlag = _OrganisationMatch(organisation);

                                if (ignoreOrganisationFlag)
                                {
                                    organisation.OrganisationName = "";
                                }
                            }

                            #region Address

                            var addresses = (from a in uniqueClaim
                                             where a.Row_Type == "ADDRESS"
                                             && a.Party_External_UID == org.Party_External_UID
                                             select a).ToList();

                            foreach (var a in addresses)
                            {
                                PipelineAddress address = new PipelineAddress();

                                address.SubBuilding = a.Data_Column_1.ToUpper();
                                address.Building = a.Data_Column_2.ToUpper();
                                address.BuildingNumber = a.Data_Column_3.ToUpper();
                                address.Street = a.Data_Column_4.ToUpper();
                                address.Town = a.Data_Column_6.ToUpper();
                                address.PostCode = a.Data_Column_8.ToUpper();

                                if (a.Entity_Type == "CORR")
                                {
                                    address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                                }

                                bool ignoreAddressFlag = _AddressMatch(address);

                                if (!ignoreAddressFlag)
                                    organisation.Addresses.Add(address);
                            }

                            #endregion Address

                            #region Contact Details

                            var partyContactInfo = (from pci in uniqueClaim
                                                    where pci.Row_Type == "CONTACT"
                                                    && pci.Party_External_UID == org.Party_External_UID
                                                    select pci).ToList();

                            foreach (var pci in partyContactInfo)
                            {
                                if (pci.Entity_Type == "CORRESTEL1" || pci.Entity_Type == "WRKTEL" || pci.Entity_Type == "CONTEL" || pci.Entity_Type == "HOMTEL" || pci.Entity_Type == "OTHTEL")
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                    organisation.Telephones.Add(telephone);
                                }
                                else if (pci.Entity_Type == "CORRESTEL2")
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.TelephoneType_Id = (int)TelephoneType.Unknown;
                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                    organisation.Telephones.Add(telephone);
                                }
                                else if (pci.Entity_Type == "MOBILE")
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                    organisation.Telephones.Add(telephone);
                                }
                                else if (pci.Entity_Type == "GENFAX" || pci.Entity_Type == "WRKFAX")
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.TelephoneType_Id = (int)TelephoneType.FAX;
                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                    organisation.Telephones.Add(telephone);
                                }
                                else if (pci.Entity_Type == "GENEML" || pci.Entity_Type == "WRKEML")
                                {
                                    PipelineEmail email = new PipelineEmail();
                                    email.EmailAddress = pci.Data_Column_1;
                                    organisation.EmailAddresses.Add(email);
                                }
                            }

                            #endregion Contact Details
                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping organisation information: " + ex);
                        }

                        if (!ignorePartyType)
                        {
                            motorClaim.Organisations.Add(organisation);
                        }
                    }

                    #endregion Organisations

                    #region NoneVehiclePeople

                    var assetRelationships = (from a in uniqueClaim
                                              where a.Row_Type == "ASSET_RELATIONSHIP"
                                              select a.Party_External_UID).ToList();

                    var people = (from p in uniqueClaim
                                  where p.Row_Type == "PARTY_MOTOR"
                                  && !assetRelationships.Contains(p.Party_External_UID)
                                  && p.Data_Column_20 == ""
                                  select p).ToList();

                    if (people != null)
                    {
                        if (people.Count > 0)
                        {
                            PipelineVehicle noVehicleInvolved = new PipelineVehicle();
                            noVehicleInvolved.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;

                            foreach (var p in people)
                            {
                                PipelinePerson person = new PipelinePerson();

                                bool ignorePartyType = false;

                                try
                                {
                                    #region Occupation

                                    var occupationList = (from o in uniqueClaim
                                                          where o.Row_Type == "EMPLOYMENT"
                                                          && o.Party_External_UID == p.Party_External_UID
                                                          select o.Data_Column_4).ToList();

                                    if (occupationList != null)
                                    {
                                        foreach (var occupation in occupationList)
                                        {
                                            person.Occupation += occupation + " ";
                                        }
                                    }

                                    #endregion Occupation

                                    #region NINumber

                                    var niNumbers = (from o in uniqueClaim
                                                     where o.Row_Type == "IDENTIFICATION"
                                                     && o.Party_External_UID == p.Party_External_UID
                                                     select o.Data_Column_1).ToList();

                                    if (niNumbers != null)
                                    {
                                        foreach (var niNumber in niNumbers)
                                        {
                                            if (!string.IsNullOrWhiteSpace(niNumber))
                                            {
                                                PipelineNINumber pNiNumber = new PipelineNINumber();

                                                pNiNumber.NINumber1 = niNumber;

                                                person.NINumbers.Add(pNiNumber);
                                            }
                                        }
                                    }

                                    #endregion NINumber

                                    #region PartyTypes

                                    var partyTypeInfo = (from pti in uniqueClaim
                                                         where pti.Row_Type == "PARTY_ADDITIONAL_NVP"
                                                         && pti.Party_External_UID == p.Party_External_UID
                                                         select pti.Data_Column_5).ToList();

                                    if (systemName.ToUpper() == "CLAIMCENTER")
                                    {
                                        var fullEntityTypes = (from a in uniqueClaim
                                                               where a.Row_Type == "ASSET_RELATIONSHIP"
                                                               && a.Party_External_UID == p.Party_External_UID
                                                               select a.Entity_Type.ToUpper()).ToList();

                                        List<string> trimmedEntityTypes = new List<string>();

                                        foreach (var a in fullEntityTypes)
                                        {
                                            var output = Regex.Replace(a, @"[\d-]", string.Empty);

                                            if (!string.IsNullOrEmpty(output))
                                            {
                                                trimmedEntityTypes.Add(output);
                                            }
                                        }

                                        if (p.Party_Type == "DOC" || p.Party_Type == "TPDOC")
                                        {
                                            ignorePartyType = true;

                                            PipelineOrganisation organisation = new PipelineOrganisation();

                                            try
                                            {
                                                if (p.Data_Column_3 != null || p.Data_Column_4 != null || p.Data_Column_5 != null)
                                                {
                                                    organisation.OrganisationName = p.Data_Column_3 + " " + p.Data_Column_4 + " " + p.Data_Column_5;

                                                    bool ignoreOrganisationFlag = _OrganisationMatch(organisation);

                                                    if (ignoreOrganisationFlag)
                                                    {
                                                        organisation.OrganisationName = "";
                                                    }
                                                }

                                                organisation.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                                                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;

                                                #region Address

                                                var orgAddresses = (from a in uniqueClaim
                                                                    where a.Row_Type == "ADDRESS"
                                                                    && a.Party_External_UID == p.Party_External_UID
                                                                    select a).ToList();

                                                foreach (var a in orgAddresses)
                                                {
                                                    PipelineAddress orgAddress = new PipelineAddress();

                                                    orgAddress.SubBuilding = a.Data_Column_1;
                                                    orgAddress.Building = a.Data_Column_2;
                                                    orgAddress.BuildingNumber = a.Data_Column_3;
                                                    orgAddress.Street = a.Data_Column_4;
                                                    orgAddress.Town = a.Data_Column_6;
                                                    orgAddress.PostCode = a.Data_Column_8;

                                                    if (a.Entity_Type == "CORR")
                                                    {
                                                        orgAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                                                    }

                                                    bool ignoreAddressFlag = _AddressMatch(orgAddress);

                                                    if (!ignoreAddressFlag)
                                                        organisation.Addresses.Add(orgAddress);
                                                }

                                                #endregion Address

                                                #region Contact Details

                                                var orgPartyContactInfo = (from pci in uniqueClaim
                                                                           where pci.Row_Type == "CONTACT"
                                                                           && pci.Party_External_UID == p.Party_External_UID
                                                                           select pci).ToList();

                                                foreach (var pci in orgPartyContactInfo)
                                                {
                                                    if (pci.Entity_Type == "CORRESTEL1" || pci.Entity_Type == "WRKTEL" || pci.Entity_Type == "CONTEL" || pci.Entity_Type == "HOMTEL" || pci.Entity_Type == "OTHTEL")
                                                    {
                                                        PipelineTelephone telephone = new PipelineTelephone();
                                                        telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                        person.Telephones.Add(telephone);
                                                    }
                                                    else if (pci.Entity_Type == "CORRESTEL2")
                                                    {
                                                        PipelineTelephone telephone = new PipelineTelephone();
                                                        telephone.TelephoneType_Id = (int)TelephoneType.Unknown;
                                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                        person.Telephones.Add(telephone);
                                                    }
                                                    else if (pci.Entity_Type == "MOBILE")
                                                    {
                                                        PipelineTelephone telephone = new PipelineTelephone();
                                                        telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                        person.Telephones.Add(telephone);
                                                    }
                                                    else if (pci.Entity_Type == "GENFAX" || pci.Entity_Type == "WRKFAX")
                                                    {
                                                        PipelineTelephone telephone = new PipelineTelephone();
                                                        telephone.TelephoneType_Id = (int)TelephoneType.FAX;
                                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                        person.Telephones.Add(telephone);
                                                    }
                                                    else if (pci.Entity_Type == "GENEML" || pci.Entity_Type == "WRKEML")
                                                    {
                                                        PipelineEmail email = new PipelineEmail();
                                                        email.EmailAddress = pci.Data_Column_1;
                                                        person.EmailAddresses.Add(email);
                                                    }
                                                }

                                                #endregion Contact Details
                                            }
                                            catch (Exception ex)
                                            {
                                                throw new CustomException("Error in mapping organisation information: " + ex);
                                            }

                                            motorClaim.Organisations.Add(organisation);
                                        }
                                        else
                                        {
                                            ignorePartyType = _PartyType(p.Party_Type, person, partyTypeInfo, trimmedEntityTypes, noVehicleInvolved.I2V_LinkData.Incident2VehicleLinkType_Id);
                                        }
                                    }
                                    else if (systemName.ToUpper() == "GIOS")
                                    {
                                        ignorePartyType = _PartyType(p.Party_Type, person, partyTypeInfo);
                                    }

                                    #endregion PartyTypes

                                    #region Saltutation

                                    if (!string.IsNullOrEmpty(p.Data_Column_1))
                                    {
                                        switch (p.Data_Column_1.ToUpper())
                                        {
                                            case "MR":
                                                person.Salutation_Id = (int)Salutation.Mr;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;

                                            case "MRS":
                                                person.Salutation_Id = (int)Salutation.Mrs;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;

                                            case "MS":
                                                person.Salutation_Id = (int)Salutation.Ms;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;

                                            case "MISS":
                                                person.Salutation_Id = (int)Salutation.Miss;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;

                                            case "MASTER":
                                                person.Salutation_Id = (int)Salutation.Master;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;

                                            case "DR":
                                                person.Salutation_Id = (int)Salutation.Dr;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;

                                            case "REV":
                                                person.Salutation_Id = (int)Salutation.Reverend;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;

                                            default:
                                                person.Salutation_Id = (int)Salutation.Unknown;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                        }
                                    }

                                    #endregion Saltutation

                                    if (!string.IsNullOrEmpty(p.Data_Column_3))
                                    {
                                        person.FirstName = p.Data_Column_3.ToUpper();
                                    }

                                    if (!string.IsNullOrEmpty(p.Data_Column_4))
                                    {
                                        person.MiddleName = p.Data_Column_4.ToUpper();
                                    }

                                    if (!string.IsNullOrEmpty(p.Data_Column_5))
                                    {
                                        if (p.Party_Type == "PAYE")
                                        {
                                            string[] names = System.Text.RegularExpressions.Regex.Split(p.Data_Column_5, @"\s+");
                                            person.LastName = names[2].ToUpper();
                                        }
                                        else
                                        {
                                            person.LastName = p.Data_Column_5.ToUpper();
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(p.Data_Column_6))
                                    {
                                        DateTime dateOfBirth = _ConvertDate(p.Data_Column_6);
                                        person.DateOfBirth = dateOfBirth;
                                    }

                                    //if (p.Party_Type == "PAYE")
                                    //{
                                    //    foreach (var vehicle in motorClaim.Vehicles)
                                    //    {
                                    //        foreach (var insuredDriverPerson in vehicle.People)
                                    //        {
                                    //            if (insuredDriverPerson.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && insuredDriverPerson.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                                    //            {
                                    //                if (insuredDriverPerson.FirstName.ToUpper() == person.FirstName.ToUpper() && insuredDriverPerson.LastName.ToUpper() == person.LastName.ToUpper() && insuredDriverPerson.DateOfBirth == person.DateOfBirth)
                                    //                {
                                    //                    insuredDriverPerson.Pe2Po_LinkData.PolicyLinkType_Id = (int)PolicyLinkType.PaidPolicy;

                                    //                    ignorePartyType = true;   
                                    //                }                                                    
                                    //            }
                                    //        }
                                            
                                    //    }
                                    //}

                                    #region Gender

                                    if (!string.IsNullOrEmpty(p.Data_Column_9))
                                    {
                                        switch (p.Data_Column_9.ToUpper())
                                        {
                                            case "M":
                                                person.Gender_Id = (int)Gender.Male;
                                                break;

                                            case "F":
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                        }
                                    }

                                    #endregion Gender

                                    #region Address

                                    var addresses = (from a in uniqueClaim
                                                     where a.Row_Type == "ADDRESS"
                                                     && a.Party_External_UID == p.Party_External_UID
                                                     select a).ToList();

                                    foreach (var a in addresses)
                                    {
                                        PipelineAddress address = new PipelineAddress();

                                        address.SubBuilding = a.Data_Column_1.ToUpper();
                                        address.Building = a.Data_Column_2.ToUpper();
                                        address.BuildingNumber = a.Data_Column_3.ToUpper();
                                        address.Street = a.Data_Column_4.ToUpper();
                                        address.Town = a.Data_Column_6.ToUpper();
                                        address.PostCode = a.Data_Column_8.ToUpper();

                                        if (a.Entity_Type == "CORR")
                                        {
                                            address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                        }

                                        bool ignoreAddressFlag = _AddressMatch(address);

                                        if (!ignoreAddressFlag)
                                            person.Addresses.Add(address);
                                    }

                                    #endregion Address

                                    #region Contact Details

                                    var partyContactInfo = (from pci in uniqueClaim
                                                            where pci.Row_Type == "CONTACT"
                                                            && pci.Party_External_UID == p.Party_External_UID
                                                            select pci).ToList();

                                    foreach (var pci in partyContactInfo)
                                    {
                                        if (pci.Entity_Type == "CORRESTEL1" || pci.Entity_Type == "WRKTEL" || pci.Entity_Type == "CONTEL" || pci.Entity_Type == "HOMTEL" || pci.Entity_Type == "OTHTEL")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);
                                        }
                                        else if (pci.Entity_Type == "CORRESTEL2")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Unknown;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);
                                        }
                                        else if (pci.Entity_Type == "MOBILE")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);
                                        }
                                        else if (pci.Entity_Type == "GENFAX" || pci.Entity_Type == "WRKFAX")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.FAX;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);
                                        }
                                        else if (pci.Entity_Type == "GENEML" || pci.Entity_Type == "WRKEML")
                                        {
                                            PipelineEmail email = new PipelineEmail();
                                            email.EmailAddress = pci.Data_Column_1;
                                            person.EmailAddresses.Add(email);
                                        }
                                    }

                                    #endregion Contact Details

                                    if (person.FirstName != null && person.LastName != null)
                                    {
                                        bool ignorePersonFlag = _PersonMatch(person);

                                        if (ignorePersonFlag)
                                        {
                                            person.FirstName = "";
                                            person.LastName = "";
                                        }
                                    }

                                    //if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured || person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Claimant)
                                    //{
                                    //    foreach (var vehicle in motorClaim.Vehicles)
                                    //    {
                                    //        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                    //        {
                                    //            if (!ignorePartyType)
                                    //            {
                                    //                vehicle.People.Add(person);
                                    //            }
                                    //        }
                                    //    }
                                    //}
                                    //else
                                    //{
                                        if (!ignorePartyType)
                                        {
                                            noVehicleInvolved.People.Add(person);
                                        }
                                    //}
                                }
                                catch (Exception ex)
                                {
                                    throw new CustomException("Error in mapping person information: " + ex);
                                }
                            }

                            if (noVehicleInvolved.People.Count() > 0)
                            {
                                motorClaim.Vehicles.Add(noVehicleInvolved);
                            }
                        }
                    }

                    #endregion NoneVehiclePeople

                    #region Insured Vehicle and Driver Check

                    bool insuredVehicle = false;
                    bool insuredDriver = false;

                    foreach (var vehicle in motorClaim.Vehicles)
                    {
                        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                        {
                            insuredVehicle = true;

                            foreach (var person in vehicle.People)
                            {
                                if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                                {
                                    insuredDriver = true;
                                }

                                if (insuredDriver == false && vehicle.People.Count() == 1)
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                    insuredDriver = true;
                                }
                            }
                        }
                    }

                    if (insuredVehicle == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            defaultInsuredVehicle.People.Add(defaultInsuredDriver);
                        }

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);
                    }
                    else
                    {
                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            foreach (var vehicle in motorClaim.Vehicles)
                            {
                                if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                {
                                    vehicle.People.Add(defaultInsuredDriver);
                                }
                            }
                        }
                    }

                    #endregion Insured Vehicle and Driver Check

                    if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;
                }
            }
        }

        private DateTime _ConvertDate(string s)
        {
            int year = 0;
            int month = 0;
            int day = 0;

            try
            {
                year = Convert.ToInt32(s.Substring(0, 4));
                month = Convert.ToInt32(s.Substring(4, 2));
                day = Convert.ToInt32(s.Substring(6, 2));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in method _ConvertDate: " + ex);
            }

            return new DateTime(year, month, day);
        }

        private decimal _ToDecimal(string str)
        {

            decimal d = 0.00M;

            try
            {
                d = Convert.ToDecimal(str);
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in method ToDecimal: " + ex);
            }

            return d;
        }

        public List<PipelineAddress> defaultAddresses(List<string> defaultAddressesForClient)
        {
            List<PipelineAddress> defaultAddresses = new List<PipelineAddress>();

            foreach (var str in defaultAddressesForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelineAddress address = new PipelineAddress();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //SB=[],BN=[],BLD=[],ST=[],LOC=[],TWN=[],CNTY=[],PC=[]

                    address.SubBuilding = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.BuildingNumber = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Building = match[2].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Street = match[3].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Locality = match[4].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Town = match[5].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.County = match[6].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.PostCode = match[7].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultAddresses.Add(address);
                }
            }

            return defaultAddresses;
        }

        public List<PipelinePerson> defaultPeople(List<string> defaultPeopleForClient)
        {
            List<PipelinePerson> defaultPeople = new List<PipelinePerson>();

            foreach (var str in defaultPeopleForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelinePerson person = new PipelinePerson();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //FN=[],LN=[]

                    person.FirstName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    person.LastName = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultPeople.Add(person);
                }
            }

            return defaultPeople;
        }

        public List<PipelineOrganisation> defaultOrganisation(List<string> defaultPeopleForClient)
        {
            List<PipelineOrganisation> defaultOrganisations = new List<PipelineOrganisation>();

            foreach (var str in defaultOrganisationsForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelineOrganisation organisation = new PipelineOrganisation();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //ON=[]

                    organisation.OrganisationName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultOrganisations.Add(organisation);
                }
            }

            return defaultOrganisations;
        }

        private bool _AddressMatch(PipelineAddress address)
        {
            var dadr = defaultAddresses(defaultAddressesForClient);

            foreach (var adr in dadr)
            {
                bool match = false;

                if (!string.IsNullOrEmpty(adr.SubBuilding))
                {
                    if (address.SubBuilding.ToUpper() == adr.SubBuilding.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.BuildingNumber))
                {
                    if (address.BuildingNumber.ToUpper() == adr.BuildingNumber.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.Building))
                {
                    if (address.Building.ToUpper() == adr.Building.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.Street))
                {
                    if (address.Street.ToUpper() == adr.Street.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.Locality))
                {
                    if (address.Locality.ToUpper() == adr.Locality.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.Town))
                {
                    if (address.Town.ToUpper() == adr.Town.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.County))
                {
                    if (address.County.ToUpper() == adr.County.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.PostCode))
                {
                    if (address.PostCode.ToUpper() == adr.PostCode.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                return match;
            }

            return false;
        }

        private bool _PersonMatch(PipelinePerson person)
        {
            var dper = defaultPeople(defaultPeopleForClient);

            foreach (var p in dper)
            {
                if (person.FirstName.ToUpper() == p.FirstName.ToUpper() && person.LastName.ToUpper() == p.LastName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        private bool _OrganisationMatch(PipelineOrganisation organisation)
        {
            var dorg = defaultOrganisation(defaultOrganisationsForClient);

            foreach (var o in dorg)
            {
                if (organisation.OrganisationName.ToUpper() == o.OrganisationName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        // GIOS - People
        private bool _PartyType(string partyType, PipelinePerson person, List<string> partyTypeInfo)
        {
            if (partyTypeInfo != null)
            {
                if (partyType == "CLM" && partyTypeInfo.Contains("DRIVER"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "CLM" && partyTypeInfo.Contains("CLAIMANT"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "CLMPAS" && partyTypeInfo.Contains("PASSENGER"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    return false;
                }

                if (partyType == "CLR" && partyTypeInfo.Contains("REPRESENTATIVE"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "OWN" && partyTypeInfo.Contains("OWNER"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("DRIVER"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("INSURER"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("RELATIVE"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("REPRESENTATIVE"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("SOLICITOR"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("THIRD PARTY"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("UNKNOWN"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "WIT" && partyTypeInfo.Contains("WITNESS"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Witness;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("PASSENGER"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("INVESTIGATOR"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("PEDESTRIAN"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("LOSS ADJUSTER"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "CLR" && partyTypeInfo.Contains("POLICYHOLDER EMPLOYER"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "CLR" && partyTypeInfo.Contains("SOLICITOR"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "CLR" && partyTypeInfo.Contains("RELATIVE"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }
            }

            if (partyType == "BRK" || partyType == "REPRTR" || partyType == "TPI")
            {
                return true;
            }

            return false;
        }

        // Claim Centre - People
        private bool _PartyType(string partyType, PipelinePerson person, List<string> itemValue, List<string> entityType, int incident2VehicleLinkType)
        {

            if (incident2VehicleLinkType == (int)Incident2VehicleLinkType.InsuredVehicle)
            {

                if (itemValue.Contains("POLICYHOLDER"))
                {
                    person.Pe2Po_LinkData.PolicyLinkType_Id = (int)PolicyLinkType.Policyholder;
                }

                if (partyType == "CLM" && entityType.Contains("MND"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "CLM" && entityType.Contains("DRV"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "CLM")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "DRI" && entityType.Contains("MND"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (itemValue != null && entityType != null && partyType != null)
                {
                    if (partyType == "DRI" && entityType.Contains("DRV") && itemValue.Contains("INJURED PARTY"))
                    {
                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                        return false;
                    }
                }

                if (partyType == "PAS" && entityType.Contains("PAS"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    return false;
                }

                if (partyType == "PAS" && entityType.Contains("MND"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "PAYE")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.PaidPolicy;
                    return false;
                }

                if (partyType == "BRK" || partyType == "REPRTR" || partyType == "TPI")
                {
                    return true;
                }

                if (partyType == "DRI" && entityType.Contains("DRV") && itemValue.Contains("DRIVER"))
                {
                    return true;
                }

                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                return false;

            }
            else if (incident2VehicleLinkType == (int)Incident2VehicleLinkType.ThirdPartyVehicle)
            {
                if (partyType == "THPDRV" && entityType.Contains("DRV"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "THPC" && entityType.Contains("DRV"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "THP" && entityType.Contains("DRV"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "THP" && entityType.Contains("MND"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "THPPAS" && entityType.Contains("PAS"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    return false;
                }

                if (partyType == "THPC" && entityType.Contains("PAS"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    return false;
                }

                if (partyType == "THP" && entityType.Contains("OWN"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                    return false;
                }

                if (partyType == "THPC" && entityType.Contains("OWN"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                    return false;
                }

                if (partyType == "THPC" && entityType.Contains("INJ"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                    return false;
                }

                if (partyType == "THP")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    return false;
                }

                if (partyType == "THPC")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                    return false;
                }

                if (partyType == "BRK" || partyType == "REPRTR" || partyType == "TPI")
                {
                    return true;
                }

                if (partyType == "DRI" && entityType.Contains("DRV") && itemValue.Contains("DRIVER"))
                {
                    return true;
                }

                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                return false;

            }
            else
            {

                if (partyType == "WIT")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
                    return false;
                }

                if (partyType == "PAS")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    return false;
                }

                if (partyType == "CLM" && entityType.Contains("MND"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "CLM" && entityType.Contains("DRV"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "CLM")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    return false;
                }

                if (partyType == "DRI" && entityType.Contains("MND"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (itemValue != null && entityType != null && partyType != null)
                {
                    if (partyType == "DRI" && entityType.Contains("DRV") && itemValue.Contains("INJURED PARTY"))
                    {
                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                        return false;
                    }
                }

                if (partyType == "PAS" && entityType.Contains("PAS"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    return false;
                }

                if (partyType == "PAS" && entityType.Contains("MND"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "PAYE")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.PaidPolicy;
                    return false;
                }

                if (partyType == "THPDRV" && entityType.Contains("DRV"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "THPC" && entityType.Contains("DRV"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "THP" && entityType.Contains("DRV"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "THP" && entityType.Contains("MND"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    return false;
                }

                if (partyType == "THPPAS" && entityType.Contains("PAS"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    return false;
                }

                if (partyType == "THPC" && entityType.Contains("PAS"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    return false;
                }

                if (partyType == "THP" && entityType.Contains("OWN"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                    return false;
                }

                if (partyType == "THPC" && entityType.Contains("OWN"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                    return false;
                }

                if (partyType == "THPC" && entityType.Contains("INJ"))
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                    return false;
                }

                if (partyType == "THP")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    return false;
                }

                if (partyType == "THPC")
                {
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                    return false;
                }

                if (partyType == "BRK" || partyType == "REPRTR" || partyType == "TPI")
                {
                    return true;
                }

                if (partyType == "DRI" && entityType.Contains("DRV") && itemValue.Contains("DRIVER"))
                {
                    return true;
                }

                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                return false;
            }

        }

        // GIOS - Organisations
        private bool _PartyType(string partyType, PipelineOrganisation organisation, List<string> partyTypeInfo)
        {
            if (partyTypeInfo != null)
            {
                if (partyType == "CHO" && partyTypeInfo.Contains("CAR HIRE"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.CreditHire;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire;
                    return false;
                }

                if (partyType == "CLM" && partyTypeInfo.Contains("DRIVER")) //Policy Holder
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                    return false;
                }

                if (partyType == "CLM" && partyTypeInfo.Contains("CLAIMANT")) //Policy Holder
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                    return false;
                }

                if (partyType == "CLR" && partyTypeInfo.Contains("REPRESENTATIVE"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    return false;
                }

                if (partyType == "OWN" && partyTypeInfo.Contains("OWNER"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("DRIVER"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("INSURER"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Insurer;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("RELATIVE"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("REPRESENTATIVE"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("SOLICITOR"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("THIRD PARTY"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    return false;
                }

                if (partyType == "THP" && partyTypeInfo.Contains("UNKNOWN"))
                {
                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                    return false;
                }
            }

            if (partyType == "CSE")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.VehicleEngineer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Engineer;
                return false;
            }

            if (partyType == "AMC")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.AccidentManagement;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.AccidentManagement;
                return false;
            }

            if (partyType == "TPS")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                return false;
            }

            if (partyType == "WIND")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Repairer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer;
                return false;
            }

            if (partyType == "DOC")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;
                return false;
            }

            if (partyType == "TPDOC")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;
                return false;
            }

            if (partyType == "BRK" || partyType == "REPRTR" || partyType == "TPI")
            {
                return true;
            }

            return false;
        }

        // Claim Centre - Organisations
        private bool _PartyType(string partyType, PipelineOrganisation organisation)
        {
            if (partyType == "CSE")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.VehicleEngineer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Engineer;
                return false;
            }

            if (partyType == "AMC")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.AccidentManagement;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.AccidentManagement;
                return false;
            }

            if (partyType == "TPS")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                return false;
            }

            if (partyType == "CSOL")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                return false;
            }

            if (partyType == "FIN")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Owner;
                return false;
            }

            if (partyType == "REP")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Repairer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer;
                return false;
            }

            if (partyType == "SAL")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Recovery;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Recovery;
                return false;
            }

            if (partyType == "MED")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;
                return false;
            }

            if (partyType == "CHO")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.CreditHire;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire;
                return false;
            }

            if (partyType == "THP")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                return false;
            }

            if (partyType == "WIND")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Repairer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer;
                return false;
            }

            if (partyType == "DOC")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;
                return false;
            }

            if (partyType == "TPDOC")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;
                return false;
            }

            if (partyType == "BRK" || partyType == "REPRTR" || partyType == "TPI")
            {
                return true;
            }

            return false;
        } 
    }
}