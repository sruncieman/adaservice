﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Impl.BrokerDirect.Motor.Model;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Ionic.Zip;

namespace MDA.MappingService.Impl.BrokerDirect.Motor
{
    public class Mapper : IMapper
    {

        private readonly Stream _fs;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] claimData;

        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public List<string> FileNames;
        public string File1;
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;
        private List<string> DefaultAddressesForClient { get; set; }
        private List<string> DefaultPeopleForClient { get; set; }
        private List<string> DefaultOrganisationsForClient { get; set; }

        public Mapper(Stream fs,
                        string clientFolder,
                        CurrentContext ctx,
                        Func<CurrentContext, IPipelineClaim, object, int> processClaimFn,
                        object statusTracking,
                        List<string> defaultAddressesForClient,
                        List<string> defaultPeopleForClient,
                        List<string> defaultOrganisationsForClient)
        {
            _fs = fs;
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;
            DefaultAddressesForClient = defaultAddressesForClient;
            DefaultPeopleForClient = defaultPeopleForClient;
            DefaultOrganisationsForClient = defaultOrganisationsForClient;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();
            FileNames = new List<string>();

            #region Extract Files From Zip

            if (_fs != null)
            {
                try
                {

                    using (var zip1 = ZipFile.Read(_fs))
                    {

                        if (zip1 == null) { throw new Exception("Error reading Zip file"); }

                        foreach (var e in zip1)
                        {
                            var extractTmpPath = string.Concat(ClientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);

                            var unpackDirectory = ClientFolder;
                            var password = ConfigurationManager.AppSettings["BrokerDirectFilePassword"];

                            // Could have passwoord
                            var extractSuccess = TryFileExtractor(e, password, unpackDirectory);
                            // else could have password with space
                            if (!extractSuccess)
                                extractSuccess = TryFileExtractor(e, password + " ", unpackDirectory);

                            // else could have no password!
                            if (!extractSuccess)
                            {
                                extractSuccess = TryFileExtractor(e, string.Empty, unpackDirectory);
                                if (!extractSuccess)
                                {
                                    throw new BadPasswordException("Tried password, password + space, no password but still errored");
                                }
                            }

                            FileNames.Add("\\" + e.FileName);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }

                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in extraction of files: " + ex);
                }
            }

            #endregion

        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                var query = (from c in claimData
                             where c.ClaimNo != ""
                             select c.ClaimNo);

                _uniqueClaimNumberList = query.Distinct().ToList();

            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        }

        /// <summary>
        /// Tries to extract files, returns false if bad password
        /// </summary>
        /// <param name="zipFileEntry"><see cref="ZipEntry"/>ZipEntry</param>
        /// <param name="password"><see cref="string"/>string of password (optional)</param>
        /// <param name="unpackDirectory"><see cref="string"/>string path of folder to extract to</param>
        /// <returns>true if successful</returns>
        private static bool TryFileExtractor(ZipEntry zipFileEntry, string password, string unpackDirectory)
        {
            if (zipFileEntry == null) throw new ArgumentNullException("zipFileEntry");

            if (string.IsNullOrEmpty(unpackDirectory))
                throw new ArgumentException("Value cannot be null or empty.", "unpackDirectory");

            try
            {
                if (!string.IsNullOrEmpty(password))
                    zipFileEntry.Password = password;

                zipFileEntry.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

            }
            catch (BadPasswordException)
            {
                return false;
            }
            return true;
        }
        public void AssignFiles()
        {

            File1 = ClientFolder + FileNames.FirstOrDefault(x => x.Contains("_BrokerDirect_")).ToString();

        }
        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }
        public void PopulateFileHelperEngines()
        {
            try
            {
                claimData = _coreClaimDataEngine.ReadFile(File1) as ClaimData[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }
        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }
        public void Translate()
        {
            try
            {
                foreach (var claim in claimData)
                {
                    // var claim = claimData.FirstOrDefault(c => c.ClaimNo == claimId);

                    if (claim == null)
                        continue;

                    var motorClaim = new PipelineMotorClaim();
                    {
                        #region Claim

                        try
                        {

                            //Check that there is a value in claim number and map it to ClaimNumber
                            if (IsValid(claim.ClaimNo))
                                motorClaim.ClaimNumber = claim.ClaimNo;

                            motorClaim.Policy = new PipelinePolicy();

                            //Check that there is a value in loss date and map it to IncidentDate
                            if (claim.IncidentDate != null)
                                motorClaim.IncidentDate = Convert.ToDateTime(claim.IncidentDate);

                            motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping claim information: " + ex);
                        }

                        #endregion Claim

                        #region ClaimInfo
                        try
                        {

                            if (claim.ClaimStatus == "Settled")
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                            else
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping claim information: " + ex);
                        }

                        #endregion ClaimInfo

                        #region Policy

                        try
                        {
                            motorClaim.Policy = new PipelinePolicy();
                            motorClaim.Policy.Insurer = "Broker Direct";

                            if (!string.IsNullOrEmpty(claim.PolicyNo))
                                motorClaim.Policy.PolicyNumber = DefaultValue(claim.PolicyNo);

                            motorClaim.Vehicles = new List<PipelineVehicle>();

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping policy: " + ex);
                        }
                        #endregion Policy

                        #region Insured
                        var vehicle = new PipelineVehicle();
                        vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                        vehicle.VehicleRegistration = DefaultValue(claim.Insured_VEH_Reg);
                        vehicle.VehicleMake = DefaultValue(claim.Insured_VEH_Make);
                        vehicle.VehicleModel = DefaultValue(claim.Insured_VEH_Model);
                        vehicle.People = new List<PipelinePerson> { 
                        this.Generate_Insured_Person(claim),
                        this.Generate_PH_Person(claim)
                    };
                        motorClaim.Vehicles.Add(vehicle);
                        #endregion

                        try
                        {
                            #region Vehicle(s) & Person(s)
                            try
                            {
                                var veh = ManageVehicleType(claim);
                                if(veh != null)
                                    motorClaim.Vehicles.Add(veh);
                            }
                            catch (Exception ex)
                            {
                                throw new CustomException("Error in mapping vehicle/person information: " + ex);
                            }

                            #endregion
                        }
                        catch (Exception ex) { 

                        }
                    }
                    try
                    {
                        if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1)
                            return;
                    }
                    catch (Exception ex)
                    {
                        // continue
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public PipelineVehicle ManageVehicleType(ClaimData claimData)
        {
            if (claimData.Claimant_Type.Contains("other vehicle"))
                return ManageVehicle(Incident2VehicleLinkType.ThirdPartyVehicle, claimData);
            else if (claimData.Claimant_Type.Contains("other") && !claimData.Claimant_Type.Contains("vehicle"))
                return ManageVehicle(Incident2VehicleLinkType.InsuredVehicle, claimData);
            else
            {
                // ManageVehivle(Incident2VehicleLinkType.)
                
            }

            return null;
        }
        public PipelineVehicle ManageVehicle(Incident2VehicleLinkType incident2VehicleLinkType, ClaimData claim)
        {
            var vehicle = new PipelineVehicle();
            vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)incident2VehicleLinkType;
            vehicle.VehicleRegistration = DefaultValue(claim.Claimant_VEH_Reg);
            vehicle.VehicleMake = DefaultValue(claim.Claimant_VEH_Make);
            vehicle.VehicleModel = DefaultValue(claim.Claimant_VEH_Model);

            var per = this.ManagePerson(incident2VehicleLinkType, claim);
            if(per != null)
                vehicle.People = new List<PipelinePerson> { per };

            return vehicle;
        }
        public PipelinePerson ManagePerson(Incident2VehicleLinkType incident2VehicleLinkType, ClaimData claim)
        {
            var person = new PipelinePerson();

            if (incident2VehicleLinkType == Incident2VehicleLinkType.InsuredVehicle)
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;

            if (incident2VehicleLinkType == Incident2VehicleLinkType.ThirdPartyVehicle)
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;

            if(IsValid(claim.Claimant_TITLE))
                person.Salutation_Id = (int)SalutationConvertor.ConvertTo(claim.Claimant_TITLE);

            if (IsValid(claim.Claimant_Surname))
                person.LastName = DefaultValue(claim.Claimant_Surname);

            if (IsValid(claim.Claimant_Forename))
                person.FirstName = DefaultValue(claim.Claimant_Forename);

            if (claim.Claimant_DateOfBirth != null)
                person.DateOfBirth = claim.Claimant_DateOfBirth;

            if (claim.Claimant_Gender == "M")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Male;
            else if (claim.Claimant_Gender == "F")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Female;
            else
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Unknown;

            if (IsValid(claim.Claimant_NiNumber))
                person.NINumbers.Add(new PipelineNINumber { NINumber1 = DefaultValue(claim.Claimant_NiNumber).ToUpper() });

            if (IsValid(claim.Claimant_ContactNumber))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = DefaultValue(claim.Claimant_ContactNumber), TelephoneType_Id = (int)TelephoneType.Unknown }); //Type=1

            if (IsValid(claim.Claimant_EmailAddress))
                person.EmailAddresses.Add(new PipelineEmail { EmailAddress = DefaultValue(claim.Claimant_EmailAddress) });

            person.Addresses = new List<PipelineAddress>
            {
                new PipelineAddress
                {
                    Building = DefaultValue(claim.Claimant_Addr1),
                    Street = DefaultValue(claim.Claimant_Addr2),
                    Locality = DefaultValue(claim.Claimant_Addr3),
                    Town = DefaultValue(claim.Claimant_Addr4),
                    PostCode = DefaultValue(claim.Claimant_PostCode),
                }
            };

            return person;
        }

        private PipelinePerson Generate_PH_Person(ClaimData claim)
        {
            var person = new PipelinePerson();

            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Policyholder;
            person.Pe2Po_LinkData.PolicyLinkType_Id = (int)PolicyLinkType.Insured;
            if (IsValid(claim.PH_Surname))
                person.LastName = DefaultValue(claim.PH_Surname);

            if (IsValid(claim.PH_Forename))
                person.FirstName = DefaultValue(claim.PH_Forename);

            if (IsValid(claim.PH_TITLE))
                person.Salutation_Id = (int)SalutationConvertor.ConvertTo(claim.PH_TITLE);

            if (claim.PH_Gender == "M")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Male;
            else if (claim.PH_Gender == "F")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Female;
            else
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Unknown;

            if (claim.PH_DateOfBirth != null)
                person.DateOfBirth = claim.PH_DateOfBirth;

            if (IsValid(claim.PH_NiNumber))
                person.NINumbers.Add(new PipelineNINumber { NINumber1 = claim.PH_NiNumber.ToUpper() });

            if (IsValid(claim.PH_ContactNumber))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claim.PH_ContactNumber, TelephoneType_Id = (int)TelephoneType.Unknown }); //Type=1

            if (IsValid(claim.PH_EmailAddress))
                person.EmailAddresses.Add(new PipelineEmail { EmailAddress = claim.PH_EmailAddress });

            person.Addresses = new List<PipelineAddress>();
            person.Addresses.Add(new PipelineAddress
            {
                Building = DefaultValue(claim.PH_Addr1),
                Street = DefaultValue(claim.PH_Addr2),
                Locality = DefaultValue(claim.PH_Addr3),
                Town = DefaultValue(claim.PH_Addr4),
                PostCode = DefaultValue(claim.PH_PostCode)
            });

            return person;
        }
        private PipelinePerson Generate_Insured_Person(ClaimData claim)
        {
            var person = new PipelinePerson();

            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
            person.Pe2Po_LinkData.PolicyLinkType_Id = (int)PolicyLinkType.Insured;

            if (IsValid(claim.Insured_Surname))
                person.LastName = DefaultValue(claim.Insured_Surname);

            if (IsValid(claim.Insured_TITLE))
                person.Salutation_Id = (int)SalutationConvertor.ConvertTo(claim.Insured_TITLE);

            if (claim.Insured_Gender == "M")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Male;
            else if (claim.Insured_Gender == "F")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Female;
            else
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Unknown;

            if (IsValid(claim.Insured_Forename))
                person.FirstName = DefaultValue(claim.Insured_Forename);

            if (claim.Insured_DateOfBirth.HasValue)
                person.DateOfBirth = claim.Insured_DateOfBirth;

            if (IsValid(claim.Insured_NiNumber))
                person.NINumbers.Add(new PipelineNINumber { NINumber1 = DefaultValue(claim.Insured_NiNumber.ToUpper()) });

            if (IsValid(claim.Insured_ContactNumber))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claim.Insured_ContactNumber, TelephoneType_Id = (int)TelephoneType.Unknown }); //Type=1

            if (IsValid(claim.Insured_EmailAddress))
                person.EmailAddresses.Add(new PipelineEmail { EmailAddress = DefaultValue(claim.Insured_EmailAddress) });

            person.Addresses = new List<PipelineAddress>();
            person.Addresses.Add(new PipelineAddress
            {
                Building = DefaultValue(claim.Insured_Addr1),
                Street = DefaultValue(claim.Insured_Addr2),
                Locality = DefaultValue(claim.Insured_Addr3),
                Town = DefaultValue(claim.Insured_Addr4),
                PostCode = DefaultValue(claim.Insured_PostCode),
            });

            return person;
        }
        
        /// If input string value is null, return null, else return value
        public static string DefaultValue(string value)
        {
            return IsValid(value) ? value : null;
        }
        public static bool IsValid(string value)
        {
            return !string.IsNullOrEmpty(value) && !value.Equals("NULL", StringComparison.InvariantCultureIgnoreCase) && value != "Not Recorded" && value != "-";
        }
    }
}
