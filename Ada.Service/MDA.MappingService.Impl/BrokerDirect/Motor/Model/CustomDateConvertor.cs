﻿using System;
using System.Globalization;
using FileHelpers;

namespace MDA.MappingService.Impl.BrokerDirect.Motor.Model
{
    public class CustomDateConvertor : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;

            value = value.Replace("\"", "");

            if (DateTime.TryParseExact(value, "dd/MM/yy", null, DateTimeStyles.None, out dt))
                return dt;

            if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, DateTimeStyles.None, out dt))
                return dt;
                     
            if (DateTime.TryParseExact(value, "dd/MM/yyyy hh:mm:ss", null, DateTimeStyles.None, out dt))
                return dt;

            return dt;
        }
    }
}
