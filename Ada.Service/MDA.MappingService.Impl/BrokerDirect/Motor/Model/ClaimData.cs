﻿using System;
using FileHelpers;
// ReSharper disable InconsistentNaming

namespace MDA.MappingService.Impl.BrokerDirect.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldValueDiscarded]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string UniqueId;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNo;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? IncidentDate;

        [FieldValueDiscarded]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimStatus;

        [FieldValueDiscarded]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insurer;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyNo;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(SalutationConvertor))]
        public string PH_TITLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Forename;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Surname;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PH_DateOfBirth;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Gender;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_NiNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_ContactNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_EmailAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Addr1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Addr2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Addr3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Addr4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_PostCode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_VEH_Reg;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_VEH_Make;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_VEH_Model;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(SalutationConvertor))]
        public string Insured_TITLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Forename;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Surname;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? Insured_DateOfBirth;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Gender;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_NiNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_ContactNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_EmailAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Addr1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Addr2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Addr3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Addr4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_PostCode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LiabilityType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_VEH_Reg;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_VEH_Make;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_VEH_Model;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(SalutationConvertor))]
        public string Claimant_TITLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Forename;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Surname;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? Claimant_DateOfBirth;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Gender;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_NiNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_ContactNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_EmailAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Addr1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Addr2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Addr3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Addr4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_PostCode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantHire_VEH_Reg;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantHire_VEH_Make;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantHire_VEH_Model;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantHire_VEH_StartDate;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantHire_VEH_EndDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Name;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_ContactNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_EmailAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Addr1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Addr2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Addr3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Addr4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_PostCode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimentRepType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Name;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_ContactNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_EmailAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Addr1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Addr2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Addr3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Addr4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_PostCode;
    }
}
