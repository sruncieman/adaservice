﻿using System;
using System.Collections.Generic;
using FileHelpers;
using MDA.Common.Enum;

namespace MDA.MappingService.Impl.BrokerDirect.Motor.Model
{
    public class SalutationConvertor : ConverterBase
    {
        public override object StringToField(string value)
        {
            string result;

            GetSalutations().TryGetValue(value, out result);

            return result;            
        }

        public static Dictionary<string, string> GetSalutations()
        {
            var dic = new Dictionary<string, string>
            {
                {"1", "Doctor"},
                {"2", "Miss"},
                {"3", "Mr"},
                {"4", "Mrs"},
                {"5", "Ms"},
                {"6", "Reverend"},
                {"7", "Sir"},
                {"8", "Brigadier"},
                {"9", "Captain"},
                {"10", "Chief"},
                {"11", "Colonel"},
                {"12", "Commander"},
                {"13", "Commodore"},
                {"14", "Dame"},
                {"15", "Flight Lieutenant"},
                {"16", "General"},
                {"17", "Honourable"},
                {"18", "Judge"},
                {"19", "Lady"},
                {"20", "Lieutenant"},
                {"21", "Lord"},
                {"22", "Major"},
                {"23", "Master"},
                {"24", "Professor"},
                {"25", "Squadron Leader"},
                {"26", "Wing Commander"},
                {"27", "Brother"},
                {"28", "Councillor"},
                {"29", "Rabbi"},
                {"30", "Sister"},
                {"31", "The Right Honourable"},
                {"32", "Right Reverend"},
                {"33", "Very Reverend"},
                {"34", "Executor(s) of"},
                {"35", "Father"},
                {"36", "Mother"},
                {"37", "An tUasal"},
                {"38", "Bean"},
                {"39", "Air Chief Marshal"},
                {"40", "Admiral"},
                {"41", "Air Commodore"},
                {"42", "Airman"},
                {"43", "Air Marshal"},
                {"44", "Air Vice Marshall"},
                {"45", "Bombardier"},
                {"46", "Canon"},
                {"47", "Corporal"},
                {"48", "Dean"},
                {"49", "Field Marshal"},
                {"50", "Flight Sergeant"},
                {"51", "Group Captain"},
                {"52", "Gunner"},
                {"53", "Lance Bombardier"},
                {"54", "Lieutenant Colonel"},
                {"55", "Lance Corporal"},
                {"56", "Lieutenant General"},
                {"57", "Private"},
                {"58", "Rear Admiral"},
                {"59", "Regimental Sergeant Major"},
                {"60", "Sergeant"},
                {"61", "Staff Sergeant"},
                {"62", "Warrant Officer 1"},
                {"63", "Warrant Officer 2"},
                {"64", "Master Sergeant"},
                {"65", "Viscount"},
                {"66", "The Most Reverend"},
                {"67", "The Venerable"},
                {"68", "Baron"},
                {"69", "Baroness"},
                {"70", "Cardinal"},
                {"71", "Count"},
                {"72", "Countess"},
                {"73", "The Honourable Lady"},
                {"74", "The Honourable Mrs"},
                {"75", "The Honourable Sir"},
                {"76", "Lieutenant Commander"},
                {"77", "Provost"},
                {"78", "Viscountess"},
                {"79", "Mr Justice"},
                {"80", "Prince"},
                {"81", "Sheikh"},
                {"82", "The Earl of"},
                {"83", "The Duke of"},
                {"84", "The Marquess of"},
                {"85", "Major General"},
                {"86", "The Honourable Miss"},
                {"87", "Estate Of"},
                {"88", "Trustees Of"},
                {"89", "Colour Sergeant"},
                {"90", "Duchess"},
                {"91", "Flight Officer"},
                {"92", "HRH"},
                {"93", "Madam"},
                {"94", "Marquis"},
                {"95", "Marshal Of The Royal Air Force"},
                {"96", "Pilot Officer"},
                {"97", "Princess"},
                {"98", "Reverend Doctor"},
                {"99", "Sachem"},
                {"100", "Surgeon Captain"},
                {"101", "Sheriff"},
                {"102", "Vice Admiral"},
                {"103", "Advocate"},
                {"104", "His Excellency"},
                {"105", "Her Excellency"},
                {"999", "Unknown"},
                {"g_080", "For"},
                {"g_999", "Employees of"},
                {"UU", "Unknown"},

            };

            return dic;
        }

        public static Salutation ConvertTo(string salutation)
        {
            Salutation result;

            return Enum.TryParse(salutation, true, out result) 
                ? result 
                : Salutation.Unknown;
        }

    }

}