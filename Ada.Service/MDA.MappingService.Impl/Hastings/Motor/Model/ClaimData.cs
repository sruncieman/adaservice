﻿using System;
using FileHelpers;
// ReSharper disable InconsistentNaming

namespace MDA.MappingService.Impl.Hastings.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNo;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? IncidentDate;

        [FieldValueDiscarded]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimStatus;

        [FieldValueDiscarded]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insurer;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyNo;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(SalutationConvertor))]
        public string PH_TITLE;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Forename;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Surname;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PH_DateOfBirth;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Gender;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_NiNumber;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_ContactNumber;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_EmailAddress;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Addr1;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Addr2;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Addr3;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Addr4;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_PostCode;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_VEH_Reg;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_VEH_Make;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_VEH_Model;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(SalutationConvertor))]
        public string Insured_TITLE;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Forename;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Surname;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? Insured_DateOfBirth;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Gender;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_NiNumber;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_ContactNumber;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_EmailAddress;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Addr1;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Addr2;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Addr3;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Addr4;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_PostCode;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_VEH_Reg;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_VEH_Make;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_VEH_Model;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(SalutationConvertor))]
        public string Claimant_TITLE;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Forename;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Surname;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? Claimant_DateOfBirth;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Gender;
        
        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_NiNumber;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_ContactNumber;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_EmailAddress;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Addr1;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Addr2;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Addr3;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Addr4;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_PostCode;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantHire_VEH_Reg;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantHire_VEH_Make;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantHire_VEH_Model;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? ClaimantHire_VEH_StartDate;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? ClaimantHire_VEH_EndDate;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Name;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_ContactNumber;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_EmailAddress;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Addr1;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Addr2;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Addr3;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_Addr4;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CreditHire_ORG_PostCode;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Name;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_ContactNumber;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_EmailAddress;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Addr1;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Addr2;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Addr3;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_Addr4;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_SOL_ORG_PostCode;
    }
}
