﻿using System;
using FileHelpers;

namespace MDA.MappingService.Impl.Hastings.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class CreditHireData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HastingsPolicyNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HastingsClaimRef;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? IncidentDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AccidentLocation;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AccidentCircumstances;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Fullname;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_HouseName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_HouseNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_StreetName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Locality;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Town;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_County;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Postcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Country;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_ContactNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insured_Email;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Fullename;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Insurer_Name;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimaint_Policy_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Veh_Reg;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CHO_Reg;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CHO_Registered_Name;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CHO_Postcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Repairs_Claimed;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Storage_Claimed;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Recovery_Claimed;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Pav_Claimed;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Hastings_Insured_Vehicle;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Reported_By_Ph;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Indemnity_Confirmed;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Liability_Stance;
    }
}
