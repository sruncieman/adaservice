﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Ionic.Zip;
using MDA.MappingService.Impl.XSDirect.Motor.Model;
using System.Text;

namespace MDA.MappingService.Impl.XSDirect.Motor
{
    class Mapper : IMapper
    {
        private readonly Stream _fs;
        private StreamReader _claimDataFileStreamReader;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public string ClaimFile { get; set; }
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;
        private List<string> DefaultAddressesForClient { get; set; }
        private List<string> DefaultPeopleForClient { get; set; }
        private List<string> DefaultOrganisationsForClient { get; set; }

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx,
            Func<CurrentContext, IPipelineClaim, object, int> processClaimFn, object statusTracking,
                        List<string> defaultAddressesForClient,
                        List<string> defaultPeopleForClient,
                        List<string> defaultOrganisationsForClient)

        {
            _fs = fs;
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();

            DefaultAddressesForClient = defaultAddressesForClient;
            DefaultPeopleForClient = defaultPeopleForClient;
            DefaultOrganisationsForClient = defaultOrganisationsForClient;
        }

        public void AssignFiles()
        {
            _claimDataFileStreamReader = new StreamReader(_fs);
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                _uniqueClaimNumberList = _coreClaimData.Where(x => x.CLAIMSREFERENCE != ""
                    && x.ACCIDENTDATE.HasValue
                    ).Select(x => x.CLAIMSREFERENCE).AsParallel().Distinct().ToList();
            }
            catch (Exception ex)
            {                
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }            
        }

        public void Translate()
        {
            foreach(var claim in _uniqueClaimNumberList)
            {
                var motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.FirstOrDefault(x => x.CLAIMSREFERENCE == claim);

                if (uniqueClaim != null)
                {
                    #region Claim

                    try
                    {
                        if (IsValid(uniqueClaim.CLAIMSREFERENCE))
                            motorClaim.ClaimNumber = uniqueClaim.CLAIMSREFERENCE;

                        if (uniqueClaim.ACCIDENTDATE != null)
                            motorClaim.IncidentDate = uniqueClaim.ACCIDENTDATE.Value;

                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    }
                    catch (Exception ex)
                    {                        
                        throw new CustomException("Error in mapping claim: " + ex);
                    }
                    #endregion

                    #region ClaimInfo
                    // Get all records in import linked to this Claim Number
                    var claimInfo = _coreClaimData.Where(c => c.CLAIMSREFERENCE == claim).ToList();

                    try
                    {
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int) ClaimStatus.Open;

                        if (uniqueClaim.NOTIFIEDDATE != null)
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = uniqueClaim.NOTIFIEDDATE;
                        
                        StringBuilder s = new StringBuilder();
                        
                            if (IsValid(uniqueClaim.DRIVERATFAULT))
                                s.Append("Driver at Fault: " + uniqueClaim.DRIVERATFAULT + ". ");
                            if (IsValid(uniqueClaim.ISINJURYCLAIM))
                                s.Append("Is Injury Claim: " + uniqueClaim.ISINJURYCLAIM + ". ");
                            if (IsValid(uniqueClaim.INJURYTYPE))
                                s.Append("Injury Type: " + uniqueClaim.INJURYTYPE + ". ");
                            if (IsValid(uniqueClaim.INJURYDETAILS))
                                s.Append("Injury Details: " + uniqueClaim.INJURYDETAILS + ". ");

                        motorClaim.ExtraClaimInfo.ClaimCode = s.ToString();
                        
                    }
                    catch (Exception ex)
                    {                        
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion

                    #region Policy

                    try
                    {
                        motorClaim.Policy.Insurer = "XS Direct";

                        if (uniqueClaim.POLICYINCEPTION != null)
                            motorClaim.Policy.PolicyStartDate = uniqueClaim.POLICYINCEPTION;

                        if (IsValid(uniqueClaim.POLICYNUMBER))
                            motorClaim.Policy.PolicyNumber = uniqueClaim.POLICYNUMBER;
                    }
                    catch (Exception ex)
                    {
                        
                        throw new CustomException("Error in mapping Policy information: " + ex);
                    }

                    #endregion

                    # region Vehicle Person and Orgs New

                    try
                    {

                        # region insured vehicle and person
                        //Create an insured vehicle
                        if (claimInfo.FirstOrDefault().PHREGNO != null)
                        {
                            var insuredVehicleData = claimInfo.Where(x => x.PHREGNO != null).FirstOrDefault();                                                        
                            var insuredVehicle = GenerateInsuredVehicle(insuredVehicleData);
                            insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                            motorClaim.Vehicles.Add(insuredVehicle);
                        }
                        else
                        {
                            var insuredVehicle = new PipelineVehicle();
                            insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                            motorClaim.Vehicles.Add(insuredVehicle);
                        }

                        // add the insured driver to the insured vehicle
                        if (claimInfo.FirstOrDefault().POLICYHOLDERNAME != null)
                        {
                            var insuredData = claimInfo.Where(x => x.POLICYHOLDERNAME != null).FirstOrDefault();
                            var insuredDriver = GenerateInsuredDriverPerson(insuredData);
                            if (insuredDriver != null)
                                motorClaim.Vehicles[0].People.Add(insuredDriver);
                        }
                        else
                        {
                            var insuredDriver = new PipelinePerson();
                            insuredDriver.V2Pe_LinkData.VehicleLinkType_Id = (int)VehicleLinkType.Driver;
                            insuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            insuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                            motorClaim.Vehicles[0].People.Add(insuredDriver);
                        }
                        
                        # endregion

                        #region tp claimants
                        //Now create a blank vehicle and add the third party claimants   
                        var tpdata = claimInfo.Where(x=>x.THIRDPARTYSURNAMES != null);
                        if (tpdata !=null)
                        {
                            var blankvehicle = new PipelineVehicle();
                            blankvehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                            
                            var tpPeople = new List<PipelinePerson>();
                            foreach (var p in tpdata)
                            {
                                //create a new tp person and add to list
                                var tpPerson = new PipelinePerson();
                                tpPerson = GenerateThirdPartyPerson(p);
                                if (tpPerson != null)
                                    tpPeople.Add(tpPerson);                                
                            }

                            // add list of tp people to vehicle
                            blankvehicle.People.AddRange(tpPeople);
                            motorClaim.Vehicles.Add(blankvehicle);
                        }
                        #endregion

                        #region tp vehicles
                        //Now add TP vehicles
                        var tpVehicleData = claimInfo.Where(x => x.VEHICLEREGISTRATIONNO != null);
                        if (tpVehicleData != null)
                        {
                            var tpVehicles = new List<PipelineVehicle>();
                            foreach(var v in tpVehicleData)
                            {
                                //create a new tp vheicle and add to the list
                                var tpVehicle = new PipelineVehicle();
                                tpVehicle = GenerateThirdPartyVehicle(v);
                                if (tpVehicle != null)
                                    tpVehicles.Add(tpVehicle);                                
                            }

                            // add list of tp vehicles to claim
                            motorClaim.Vehicles.AddRange(tpVehicles);
                        }

                        #endregion

                    }
                    catch (Exception ex)
                    {
                        
                        throw new CustomException("Error mapping vehicle information: " + ex);
                    }
                    #endregion
                }
                if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1) return;

            }
        }

        public static bool IsValid(string value)
        {
            return !string.IsNullOrEmpty(value) &&
                   !value.Equals("NULL", StringComparison.InvariantCultureIgnoreCase);
        }

        public static string DefaultValue(string value)
        {
            return IsValid(value) ? value : null;
        }

        private PipelineVehicle GenerateInsuredVehicle(ClaimData claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }
                        
            var data = claimInfo;
            var insuredVehicle = MapToPipelineInsuredVehicle(data);

            return insuredVehicle;
        }

        private PipelineVehicle MapToPipelineInsuredVehicle(ClaimData claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var insuredVehicle = new PipelineVehicle
            {
                VehicleModel = DefaultValue(claimInfo.PHVEHICLE),
                VehicleRegistration = DefaultValue(claimInfo.PHREGNO)
            };

            return insuredVehicle;
        }

        private PipelinePerson GenerateInsuredDriverPerson(ClaimData claimInfo)
        {

            var data = claimInfo;

            var person = new PipelinePerson
            {
                I2Pe_LinkData =
                {
                    PartyType_Id = (int)PartyType.Insured,
                    SubPartyType_Id = (int)SubPartyType.Driver
                },
                Pe2Po_LinkData =
                {
                    PolicyLinkType_Id = (int)PolicyLinkType.Policyholder
                },
                V2Pe_LinkData = 
                {
                    VehicleLinkType_Id = (int)VehicleLinkType.Driver
                }               

            };
            
            if (IsValid(data.POLICYHOLDERNAME))
            {
                try
                {
                    var names = data.POLICYHOLDERNAME.Split(' ');
                    if (names.Length > 1)
                    {
                        person.FirstName = names.First();
                        person.LastName = names.Last();
                    }
                    else
                    {

                    }
                    
                }
                catch (Exception ex)
                {                    
                    throw new CustomException("Error splitting policyholder name in mapping: " + ex);
                }
            }

            if (data.PHDOB != null)
                person.DateOfBirth = data.PHDOB.Value;

            var insuredDriverAddress = createInsuredDriverAddress(data);
            if (insuredDriverAddress != null)
            {
                person.Addresses.Add(insuredDriverAddress);
                insuredDriverAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
            }
                       
            return person;
        }
        
        private PipelineAddress createInsuredDriverAddress(ClaimData data)
        {
            return translateAddress(null,DefaultValue(data.PHADDRESS1),
                                    DefaultValue(data.PHADDRESS2),
                                    DefaultValue(data.PHADDRESS3),                                    
                                    DefaultValue(data.PHPOSTCODE));
        }

        private PipelineAddress translateAddress(string address1, string address2, string address3, string address4, string postcode)
        {
            try
            {
                var address = new PipelineAddress
                {
                    BuildingNumber = DefaultValue(address1),
                    Street = DefaultValue(address2),
                    Locality = DefaultValue(address3),
                    Town = DefaultValue(address4),
                    PostCode = DefaultValue(postcode)
                };

                return address;
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in mapping address information: " + ex);
            }


        }
        
        private PipelineAddress createThirdPartyAddress(ClaimData data)
        {
            return translateAddress(null,data.THIRDPARTYADDRESS,null,null,
                                    DefaultValue(data.THIRDPARTYPOSTCODE));
        }       

        private PipelineVehicle GenerateThirdPartyVehicle(ClaimData claimInfo)
        {

            var data = claimInfo;

            var thirdPartyVehicle = new PipelineVehicle();

            thirdPartyVehicle.VehicleRegistration = data.VEHICLEREGISTRATIONNO;
            thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;

            return thirdPartyVehicle;
        }
             
        private PipelinePerson GenerateThirdPartyPerson(ClaimData p)
        {
            if (p == null) { throw new ArgumentNullException("ClaimInfo");  }
            
            var thirdPartyPerson = new PipelinePerson();

            thirdPartyPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
            thirdPartyPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
            
            if (IsValid(p.THIRDPARTYTITLE))
            {
                switch (p.THIRDPARTYTITLE.ToUpper())
                {
                    case "MISS":
                        thirdPartyPerson.Salutation_Id = (int)Salutation.Miss;
                        break;
                    case "MR":
                        thirdPartyPerson.Salutation_Id = (int)Salutation.Mr;
                        break;
                    case "NA":
                        thirdPartyPerson.Salutation_Id = (int)Salutation.Unknown;
                        break;
                    case "MRS":
                        thirdPartyPerson.Salutation_Id = (int)Salutation.Mrs;
                        break;
                    case "MS":
                        thirdPartyPerson.Salutation_Id = (int)Salutation.Ms;
                        break;
                    case "MASTER":
                        thirdPartyPerson.Salutation_Id = (int)Salutation.Master;
                        break;
                    default:
                        thirdPartyPerson.Salutation_Id = (int)Salutation.Unknown;
                        break;
                }
                                
            }

            if (IsValid(p.THIRDPARTYFORENAMES))
                thirdPartyPerson.FirstName = p.THIRDPARTYFORENAMES;

            if (IsValid(p.THIRDPARTYSURNAMES))
                thirdPartyPerson.LastName = p.THIRDPARTYSURNAMES;

            if (IsValid(p.THIRDPARTYTELEPHONENO))
                thirdPartyPerson.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = p.THIRDPARTYTELEPHONENO, TelephoneType_Id = (int)TelephoneType.Landline });

            if (IsValid(p.THIRDPARTYMOBILENO))
                thirdPartyPerson.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = p.THIRDPARTYMOBILENO, TelephoneType_Id = (int)TelephoneType.Mobile });

            if (IsValid(p.THIRDPARTYNATIONALINSNO))
                thirdPartyPerson.NINumbers.Add(new PipelineNINumber { NINumber1 = p.THIRDPARTYNATIONALINSNO });

            var thirdPartyDriverAddress = createThirdPartyAddress(p);
            if (thirdPartyDriverAddress != null)
            {
                thirdPartyPerson.Addresses.Add(thirdPartyDriverAddress);
                thirdPartyDriverAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
            }

            if (p.LOCALSOLICITORNAME != null)
            {
                var thirdpartySol = GenerateOrgSol(p);
                if (thirdpartySol != null)
                    thirdPartyPerson.Organisations.Add(thirdpartySol);
            }
                  
            if (p.CARHIREPROVIDER != null)
            {
                var thirdPartyHire = GenerateOrgHire(p);
                if (thirdPartyHire != null)
                    thirdPartyPerson.Organisations.Add(thirdPartyHire);
            }


            return thirdPartyPerson;
        }
        
        private PipelineOrganisation GenerateOrgSol(ClaimData p)
        {
            if (p == null) { throw new ArgumentNullException("ClaimInfo"); }

            var orgSol = new PipelineOrganisation();

            if (p.SOLICITORSNAME == null)
            {
                return orgSol;
            }
            else 
            {
                orgSol.OrganisationType_Id = (int)OrganisationType.Solicitor;

                if (IsValid(p.SOLICITORSNAME))
                    orgSol.OrganisationName = p.SOLICITORSNAME;

                if (IsValid(p.LOCALSOLICITORADDRESS))
                {
                    var orgSolAddress = new PipelineAddress();
                    orgSolAddress.Street = p.LOCALSOLICITORADDRESS;

                    if (orgSolAddress != null)
                        orgSol.Addresses.Add(orgSolAddress);
                }

                if (IsValid(p.LOCALSOLICITORTELEPHONE))
                {
                    var orgSolTel = new PipelineTelephone();
                }

                if (IsValid(p.LOCALSOLICITORTELEPHONE))
                {
                    var orgSolTel = new PipelineTelephone();
                    orgSolTel.ClientSuppliedNumber = p.LOCALSOLICITORTELEPHONE;

                    if (orgSolTel != null)
                        orgSol.Telephones.Add(orgSolTel);
                }                

            }
             
            return orgSol;

        }
        
        private PipelineOrganisation GenerateOrgHire(ClaimData p)
        {
            if (p == null) { throw new ArgumentNullException("ClaimInfo"); }

            var orgHire = new PipelineOrganisation();

            if (p.CARHIREPROVIDER == null)
            {

            }
            else
            {
                if (IsValid(p.CARHIREPROVIDER))
                    orgHire.OrganisationName = p.CARHIREPROVIDER;
            }

            return orgHire;

        }
    }
}
