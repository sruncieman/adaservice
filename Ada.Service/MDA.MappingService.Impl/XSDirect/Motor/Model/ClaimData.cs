﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Pipeline.Model;
using System.ComponentModel;

namespace MDA.MappingService.Impl.XSDirect.Motor.Model
{
    [DelimitedRecord(",")]
    [IgnoreFirst(1)]
    public class ClaimData
    {

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CLAIMSREFERENCE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string POLICYNUMBER;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? NOTIFIEDDATE;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? ACCIDENTDATE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DRIVERATFAULT;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? POLICYINCEPTION;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string POLICYHOLDERNAME;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PHDOB;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHADDRESS1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHADDRESS2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHADDRESS3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHADDRESS4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHADDRESS5;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHPOSTCODE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHREGNO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHVEHICLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHVEHICLEYEAR;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHLICENCE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string THIRDPARTYTITLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string THIRDPARTYFORENAMES;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string THIRDPARTYSURNAMES;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string THIRDPARTYADDRESS;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string THIRDPARTYPOSTCODE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string THIRDPARTYTELEPHONENO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string THIRDPARTYMOBILENO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CLAIMANTNO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VEHICLEREGISTRATIONNO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ISINJURYCLAIM;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DATEINJURYCLAIMRECEIVED;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string THIRDPARTYNATIONALINSNO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string INJURYTYPE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string INJURYDETAILS;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SOLICITORSNAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SOLICITORREFERENCE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LOCALSOLICITORNAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LOCALSOLICITORADDRESS;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LOCALSOLICITOREMAIL;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LOCALSOLICITORTELEPHONE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ATTENDHOSPITIAL;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HOSPITIALNAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HOSPITIALADDRESS;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HOSPITIALPOSTCODE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CARHIREPROVIDER;

    }
}
