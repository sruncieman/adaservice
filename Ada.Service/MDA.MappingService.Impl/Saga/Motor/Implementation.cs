﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MDA.MappingService.Interface;
using MDA.Common;
using MDA.Common.Server;
using Ionic.Zip;

namespace MDA.MappingService.Impl.Saga.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private readonly string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];

        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
            Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> processClaim,
            out ProcessingResults processingResults)
        {
            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            try
            {
                var defaultDataAddressForClient = new RiskService.RiskServices(ctx).GetRiskDefaultDataAddressForClient(ctx.RiskClientId);
                var defaultDataPeopleForClient = new RiskService.RiskServices(ctx).GetRiskDefaultDataPeopleForClient(ctx.RiskClientId);
                var defaultDataOrganisationsForClient = new RiskService.RiskServices(ctx).GetRiskDefaultDataOrganisationsForClient(ctx.RiskClientId);

                var mapper = new Mapper(fs,
                                        clientFolder,
                                        ctx,
                                        processClaim,
                                        statusTracking,
                                        defaultDataAddressForClient,
                                        defaultDataPeopleForClient,
                                        defaultDataOrganisationsForClient);


                mapper.AssignFiles();
                mapper.InitialiseFileHelperEngines();
                mapper.PopulateFileHelperEngines();
                mapper.RetrieveDistinctClaims();
                mapper.Translate();
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Saga file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = _tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }

        private static void Fixfile(string fileNamePath, char delim, int cols)
        {
            if (string.IsNullOrWhiteSpace(fileNamePath))
                throw new ArgumentException("Value cannot be null or whitespace.", "fileNamePath");
            
            StringBuilder sb;

            using (var streamReader = new StreamReader(fileNamePath, Encoding.UTF8))
            {
                sb = new StringBuilder(streamReader.ReadToEnd());
            };
            
            sb.Replace("\n", "");

            sb.Replace("\r", "\r\n");

            sb.Replace("\"\"", "");            

            using (var reader = new StringReader(sb.ToString()))
            {
                string line;

                // reuse string builder
                sb.Clear();
                var linecount = 0;

                while ((line = reader.ReadLine()) != null)
                {
                    linecount++;
                    sb.AppendLine(FixLine(line, delim, cols));                    
                }                              
            };

            File.WriteAllText(fileNamePath, sb.ToString());                

        }

        private static string FixLine(string line, char delim, int colCount)
        {
            if (string.IsNullOrWhiteSpace(line))
            {
                return null;
            }

            var cells = line.Split(delim);

            var listToFix = new List<int>();

            // iterate throw each column looking for items to fix
            var count = 0;
            foreach (var cell in cells)
            {
                var valid = CheckCell(cell);

                if (!valid)
                {
                    listToFix.Add(count);
                }

                count++;
            }

            var sb = new StringBuilder();

            if (listToFix.Count > 0)
            {
                var loop = 0;

                foreach (var i in listToFix)
                {
                    for (var x = loop; x < i; x++)
                    {
                        sb.Append(FixCell(cells[x]));
                        sb.Append(delim);
                        loop = x + 1;
                    }

                    // merge two into 1 -> don't put back |
                    // don't add empty " when merging
                    if (cells[loop] != "\"")
                    {
                        sb.Append(cells[loop]);
                    }

                    loop = loop + 1;

                    sb.Append(cells[loop]);
                    sb.Append('|');

                }

                // may not be at end of string yet:

                if (loop < line.Length)
                {
                    loop = loop + 1;
                    for (var x = loop; x < cells.Length; x++)
                    {
                        sb.Append(FixCell(cells[x]));
                        sb.Append(delim);
                    }
                }

                sb.Length--;
            }
            else
            {
                foreach (var cell in cells)
                {
                    sb.Append(FixCell(cell));
                    sb.Append(delim);
                }
                sb.Length--;
            }




            // add extra rows
            if (count < colCount)
            {
                for (var i = 0; i < (colCount - count); i++)
                {
                    sb.Append(delim);
                }
            }


            return sb.ToString();

        }

        private static string FixCell(string cell)
        {
            if (string.IsNullOrWhiteSpace(cell)) return string.Empty;
            if (cell == "\"") return string.Empty;

            if (cell.StartsWith("\"") && cell.Length > 2)
            {
                var newCell = cell.Replace("\"", "");

                return "\"" + newCell + "\"";
            }

            return cell;
        }

        private static bool CheckCell(string cell)
        {
            // if cell starts with " does it end with "
            if (string.IsNullOrWhiteSpace(cell) || cell.Equals("\"\"")) return true;

            if (cell.StartsWith("\"") || cell.EndsWith("\""))
            {
                return cell.Length > 1 && cell.EndsWith("\"");
            }

            return true;
        }

        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard,
            int settleTime, int batchSize, string postOp, string archiveFolder,
            Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {
            // Create structure to hold our new batch of claims
            var batch = new List<object>();

            string firstFileName = null;

            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            var files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (!files.Any()) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Any(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now))
                return;

            // If we get here we have files to process

            // Looking for two files: 
            //SAGA_Core_yyyymmdd_hhmmss.txt
            //SAGA_Vehi_yyyymmdd_hhmmss.txt
            
            foreach (var file in files)
            {
                var fileName = Path.GetFileName(file);

                // Is this file the core or vehicle?
                if (fileName.Contains("SAGA_Vehi"))
                {
                    // skip if secondary file, looking for main file first
                    continue;
                }

                // build additionalFileName from initialFileName
                var suffix = fileName.Substring(10, fileName.Length - 14);
                var additionalFileName = "SAGA_Vehi_" + suffix + ".txt";

                var originalAdditionalFileName = "SAGA_Vehi_" + suffix + "_Orig.txt";
                var originalFileName = "SAGA_Core_" + suffix + "_Orig.txt";
                
                // does additionalFileName Exist?
                if (File.Exists(folderPath + "\\" + additionalFileName))
                {
                    // we have a pair -> fix, zip and continue                    
                    const string mimeType = "zip";
                    const string clientName = "Saga";
                    var batchRef = string.Format("{0}-{1}", clientName, suffix);
                    var zipFileName = "Saga-" + suffix + "." + mimeType;

                    // Create backup copy
                    File.Copy(folderPath + "\\" + additionalFileName, folderPath + "\\" + originalAdditionalFileName);
                    File.Copy(folderPath + "\\" + fileName, folderPath + "\\" + originalFileName);
                    
                    // Run Fix Parser
                    Fixfile(folderPath + "\\" + fileName, '|', 115);
                    Fixfile(folderPath + "\\" + additionalFileName, '|', 115);
                                        
                    using (var zip = new ZipFile())
                    {
                        zip.AddFile(folderPath + "\\" + fileName, "");
                        zip.AddFile(folderPath + "\\" + additionalFileName, "");
                        zip.AddFile(folderPath + "\\" + originalAdditionalFileName, "");
                        zip.AddFile(folderPath + "\\" + originalFileName, "");
                        zip.Save(folderPath + "\\" + zipFileName);
                    }

                    using (var fileStream = File.OpenRead(folderPath + "\\" + zipFileName))
                    {
                        var stream = new MemoryStream();
                        stream.SetLength(fileStream.Length);
                        fileStream.Read(stream.GetBuffer(), 0, (int)fileStream.Length);

                        #region Process the batch (call the callback function)
                        processClientBatch(ctx, stream, zipFileName, mimeType, "Motor", batchRef);
                        #endregion
                    }
                    #region Process all the options we invented for our postOp functionality

                    // We are passed the string we entered into config.  Now we have to code it!
                    // In this case we have implemented RENAME, DELETE, ARCHIVE
                    // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                    // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                    switch (postOp.ToUpper())
                    {
                        case "RENAME":
                            File.Move(folderPath + "\\" + fileName, folderPath + "\\" + fileName + ".old");
                            File.Move(folderPath + "\\" + additionalFileName, folderPath + "\\" + additionalFileName + ".old");
                            File.Move(folderPath + "\\" + originalFileName, folderPath + "\\" + originalFileName + ".old");
                            File.Move(folderPath + "\\" + originalAdditionalFileName, folderPath + "\\" + originalAdditionalFileName + ".old");
                            File.Move(folderPath + "\\" + zipFileName, folderPath + "\\" + zipFileName + ".old");
                            break;

                        case "DELETE":
                            File.Delete(folderPath + "\\" + fileName);
                            File.Delete(folderPath + "\\" + additionalFileName);
                            File.Delete(folderPath + "\\" + originalFileName);
                            File.Delete(folderPath + "\\" + originalAdditionalFileName);
                            File.Delete(folderPath + "\\" + zipFileName);
                            break;

                        case "ARCHIVE":
                            if (!Directory.Exists(archiveFolder))
                                Directory.CreateDirectory(archiveFolder);   // Create destination if needed
                           
                            var zipfname = archiveFolder + "\\" + zipFileName;  // build dest path

                            if (File.Exists(zipfname))  // Check if it already exists
                            {
                                zipfname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                            }

                            File.Delete(folderPath + "\\" + fileName);
                            File.Delete(folderPath + "\\" + additionalFileName);
                            File.Delete(folderPath + "\\" + originalFileName);
                            File.Delete(folderPath + "\\" + originalAdditionalFileName);
                            File.Move(folderPath + "\\" + zipFileName, zipfname);
                            break;
                    }
                    #endregion


                }    
            }
        }
    }
}
