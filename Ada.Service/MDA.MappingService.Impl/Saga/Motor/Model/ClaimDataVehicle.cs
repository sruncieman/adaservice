﻿using FileHelpers;
// ReSharper disable InconsistentNaming
namespace MDA.MappingService.Impl.Saga.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord("|")]
    public class ClaimDataVehicle
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Client_Reference_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Party_Type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Relationship_Type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Identifier;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Coverage;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Added_Date;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Class;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Storage;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Foreign_Registered;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Green_Card_Reference;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Registration_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Make;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Model;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Date_of_Theft;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Date_Scrapped;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Value_of_Insured_Vehicle;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Vehicle_Use;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DVLA_Reg_Owner;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DVLA_Reg_Address;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Blame_Code;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Feed_Type;
    }
}
