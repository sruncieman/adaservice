﻿using System;
using System.Globalization;
using FileHelpers;

namespace MDA.MappingService.Impl.Saga.Motor.Model
{
    public class CustomDateConvertor : ConverterBase
    {
        public override object StringToField(string value)
        {
            if (value == null) return null;

            DateTime dt;

            value = value.Replace("\"", "");

            if (DateTime.TryParseExact(value, "yyyyMMdd", null, DateTimeStyles.None, out dt))
                return dt;

            return null;            
        }
    }
}
