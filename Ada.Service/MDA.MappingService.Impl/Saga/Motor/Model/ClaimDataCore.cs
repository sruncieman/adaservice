﻿using System;
using FileHelpers;
// ReSharper disable InconsistentNaming

namespace MDA.MappingService.Impl.Saga.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord("|")]
    public class ClaimDataCore
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Feed_Type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Policy_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Claim_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Closed_Claim_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Info_Only_Flag;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? Start_Date;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Start_Time;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? Claim_Incident_Date;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Claim_Incident_Time;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? Policy_Change_Date;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Status;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? Status_Change_Date;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Product_Code;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Product_Description;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Policy_Group;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Policy_Type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Brand;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Broker;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Agent;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Handler;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public decimal? Premium;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Premium_Payment_Method;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Policy_Value;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Source_Of_Business;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? Policy_End_Date;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Term;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Multiple_Policy_Holders;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string IP_Address;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Previous_Insurer;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Claim_Type_Description;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Claim_Type_Code;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public decimal? Claim_Current_Estimate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public decimal? Claim_Total_Paid;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public decimal? Claim_Recovery_Estimate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public decimal? Claim_Total_Recovered;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Date_Claim_Settled;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Cancellation_Reason;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Incident_Address_1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Incident_Address_2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Incident_Address_3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]       
        public string Incident_Address_4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        
        public string Incident_Address_5;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Incident_Address_6;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Incident_Address_7;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Incident_Address_8;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Incident_Address_9;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldTrim(TrimMode.Both, '"')]
        public string Incident_Address_10;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Accident_Circumstance;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Accident_Grade;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Party_Type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Contact_Reference;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Title;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Forename;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Initials;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Surname;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Alias_Forename;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Alias_Surname;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? Date_Of_Birth;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Gender;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Nationality;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Country_Of_Birth;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Deceased_Indicator;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? Date_Of_Death;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Company_Name;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Driving_License_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string License_Type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NI_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Passport_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public string Marital_Status;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Previous_Surname;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Mothers_Maiden_Name;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Income;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Employment_Type_Description;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Employment_Description;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string No_Claims_Discount;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string No_Claims_Years;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Number_Of_Motoring_Convictions;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Number_Of_Non_Motoring_Convictions;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PI_Flag;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PI_Code;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? PI_Realisation_Date;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Bank_Sort_Code;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Bank_Account_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Credit_card_number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_Type;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_4;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_5;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_6;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_7;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_8;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_9;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address_10;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? From_Date;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConvertor))]        
        public DateTime? To_Date;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Residential_Term;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Residential_Status;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Fax_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Home_Telephone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Work_Telephone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Mobile_Telephone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Home_Email_Address;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Work_Email_Address;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Police_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Police_Crime_Number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Policy_Branch;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Voluntary_Excess;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Compulsory_Excess;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Relationship;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Relationship_Type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Large_Loss_Indicator;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Driving_Experience;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Previous_accidents;


      
    }
}
