﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Ionic.Zip;
using MDA.MappingService.Impl.Saga.Motor.Model;

namespace MDA.MappingService.Impl.Saga.Motor
{
    public class Mapper : IMapper
    {
        private FileHelperEngine _coreClaimDataEngine;
        private FileHelperEngine _vehicleClaimDataEngine;
        

        private List<string> DefaultAddressesForClient { get; set; }
        private List<string> DefaultPeopleForClient { get; set; }
        private List<string> DefaultOrganisationsForClient { get; set; }

        private ClaimDataCore[] _coreClaimData;
        private ClaimDataVehicle[] _vehicleClaimData;
        
        private string CoreFilePath { get; set; }
        private string VehicleFilePath { get; set; }
        

        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public string ClaimFile { get; set; }
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;
        public List<string> FileNames;

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx,
            Func<CurrentContext, IPipelineClaim, object, int> processClaimFn, object statusTracking,
                        List<string> defaultAddressesForClient,
                        List<string> defaultPeopleForClient,
                        List<string> defaultOrganisationsForClient)
        {            
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;

            _uniqueClaimNumberList = new List<string>();

            FileNames = new List<string>();
            DefaultAddressesForClient = defaultAddressesForClient;
            DefaultPeopleForClient = defaultPeopleForClient;
            DefaultOrganisationsForClient = defaultOrganisationsForClient;
            if (fs != null)
            {
                try
                {

                    using (var zip = ZipFile.Read(fs))
                    {

                        if (zip == null) { throw new Exception("Error reading Zip file"); }

                        foreach (var file in zip)
                        {
                            var extractTmpPath = string.Concat(ClientFolder + @"\" + file.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + file.FileName, false);

                            var unpackDirectory = ClientFolder;

                            // Could have passwoord
                            var extractSuccess = TryFileExtractor(file, string.Empty, unpackDirectory);
                            if (!extractSuccess)
                            {
                                throw new ZipException();
                            }

                            FileNames.Add("\\" + file.FileName);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }

                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in extraction of files: " + ex);
                }
            }
        }

        /// <summary>
        /// Tries to extract files, returns false if bad password
        /// </summary>
        /// <param name="zipFileEntry"><see cref="ZipEntry"/>ZipEntry</param>
        /// <param name="password"><see cref="string"/>string of password (optional)</param>
        /// <param name="unpackDirectory"><see cref="string"/>string path of folder to extract to</param>
        /// <returns>true if successful</returns>
        private static bool TryFileExtractor(ZipEntry zipFileEntry, string password, string unpackDirectory)
        {
            if (zipFileEntry == null) throw new ArgumentNullException("zipFileEntry");

            if (string.IsNullOrEmpty(unpackDirectory))
                throw new ArgumentException("Value cannot be null or empty.", "unpackDirectory");

            try
            {
                if (!string.IsNullOrEmpty(password))
                {
                    zipFileEntry.Password = password;
                }

                zipFileEntry.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

            }
            catch (BadPasswordException)
            {
                return false;
            }
            return true;
        }

        public void AssignFiles()
        {
            CoreFilePath = ClientFolder + FileNames.First(x => x.Contains("SAGA_Core"));
            VehicleFilePath = ClientFolder + FileNames.First(x => x.Contains("SAGA_Vehi"));            
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimDataCore));
                _vehicleClaimDataEngine = new FileHelperEngine(typeof(ClaimDataVehicle));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }        

        public void PopulateFileHelperEngines()
        {
            try
            {
                _coreClaimData = _coreClaimDataEngine.ReadFile(CoreFilePath) as ClaimDataCore[];
                _vehicleClaimData = _vehicleClaimDataEngine.ReadFile(VehicleFilePath) as ClaimDataVehicle[];                
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }


        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers                
                DateTime skipDate;

                var skipdateValue = ConfigurationManager.AppSettings["SagaSkipDate"];

                if (!string.IsNullOrEmpty(skipdateValue))
                {
                    if (DateTime.TryParseExact(skipdateValue, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out skipDate))
                    {
                        _uniqueClaimNumberList = _coreClaimData.Where(x => x.Claim_Number != "" && x.Feed_Type != "Policy" && x.Claim_Incident_Date.Value >= skipDate).Select(x => x.Claim_Number).AsParallel().Distinct().ToList();
                        return;
                    }
                }

                _uniqueClaimNumberList = _coreClaimData.Where(x => x.Claim_Number != "" && x.Feed_Type != "Policy").Select(x => x.Claim_Number).AsParallel().Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        }

        public void Translate()
        {
            var skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["SagaSkipNum"]);
            var takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["SagaTakeNum"]);

            if (skipValue >= 0 && takeValue > 0)
            {
                _uniqueClaimNumberList = _uniqueClaimNumberList.Skip(skipValue).Take(takeValue).ToList();
            }

            foreach (var claim in _uniqueClaimNumberList)
            {
                var motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.FirstOrDefault(x => x.Feed_Type != "POLICY" && x.Claim_Number == claim);                

                if (uniqueClaim != null)
                {


                    var policyInfo = _coreClaimData.FirstOrDefault(x => x.Feed_Type == "POLICY" && x.Policy_Number == uniqueClaim.Policy_Number);
                    // Get all records in import linked to this Claim Number
                    var claimInfo = _coreClaimData.Where(x => x.Feed_Type != "POLICY" && x.Claim_Number == claim).ToList();


                    #region Claim

                    try
                    {
                        if (IsValid(uniqueClaim.Claim_Number))
                            motorClaim.ClaimNumber = uniqueClaim.Claim_Number;

                        if (uniqueClaim.Claim_Incident_Date != null)
                            motorClaim.IncidentDate = uniqueClaim.Claim_Incident_Date.Value;

                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;                        
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim: " + ex);
                    }

                    #endregion Claim

                    #region ClaimInfo

                    try
                    {
                        if (IsValid(uniqueClaim.Status))
                        {
                            switch (uniqueClaim.Status.ToUpper())
                            {
                                    
                                case "OPEN":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                    break;
                                case "CLOSED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                    break;
                                case "REOPENED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                                    break;
                                case "CANCELLED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Withdrawn;
                                    break;


                                case "FULL SUSPENSION":
                                case "MID TERM ADJUSTMENT":
                                case "NEW BUSINESS":
                                case "NOT OK TO PROCEED":
                                case "PARTIAL SUSPENSION":
                                case "PENDING":
                                case "REINSTATEMENT (AFTER CANCELLATION)":
                                case "RENEWAL ACCEPTANCE":
                                case "RENEWAL INVITATION":
                                case "UNKNOWN":

                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                                    break;




                                default:
                                    throw new InvalidOperationException(
                                        string.Format("Invalid status: [{0}] on claimId [{1}]", uniqueClaim.Status,
                                            uniqueClaim.Claim_Number));
                            }
                        }

                        if (uniqueClaim.Start_Date != null)
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = uniqueClaim.Start_Date;



                        if (IsValid(uniqueClaim.Claim_Type_Description))
                            motorClaim.ExtraClaimInfo.IncidentCircumstances = uniqueClaim.Claim_Type_Description;


                        motorClaim.ExtraClaimInfo.Reserve = uniqueClaim.Claim_Current_Estimate;

                        motorClaim.ExtraClaimInfo.PaymentsToDate = uniqueClaim.Claim_Total_Paid;

                        var policeRef = claimInfo.Select(x=>x.Police_Crime_Number).FirstOrDefault(x => x != null);

                        if (IsValid(policeRef))                            
                            motorClaim.ExtraClaimInfo.PoliceReference = policeRef;

                        if (IsValid(uniqueClaim.Incident_Address_1))
                            motorClaim.ExtraClaimInfo.IncidentLocation = uniqueClaim.Incident_Address_1;
                        
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion ClaimInfo

                    #region Policy

                    try
                    {                        
                        
                        if (IsValid(uniqueClaim.Policy_Number))
                        {
                            motorClaim.Policy.PolicyNumber = uniqueClaim.Policy_Number;
                            
                            if (policyInfo != null)
                            {
                                if (policyInfo.Start_Date.HasValue)
                                    motorClaim.Policy.PolicyStartDate = policyInfo.Start_Date.Value;

                                if (policyInfo.Policy_End_Date.HasValue)
                                    motorClaim.Policy.PolicyEndDate = policyInfo.Policy_End_Date.Value;

                                if (IsValid(policyInfo.Broker))
                                {
                                    motorClaim.Policy.Broker = policyInfo.Broker;    
                                }

                                if (IsValid(policyInfo.Brand))
                                {
                                    motorClaim.Policy.InsurerTradingName = policyInfo.Brand;
                                }
                                
                                motorClaim.Policy.Insurer = "Saga";
                                if (policyInfo.Premium.HasValue)
                                    motorClaim.Policy.Premium = policyInfo.Premium.Value;
                            }
                        }

                        

                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping policy: " + ex);
                    }

                    #endregion Policy

                    #region Vehicle(s) And Person(s) and Organisation(s)

                    try
                    {
                        //-Org
                        //-|- Addr
                        // |    - Veh
                        // |        -Per
                        // |    
                        // VEH
                        // - Per
                        //     - Addr

                        var claimVehicles = _vehicleClaimData.Where(x=>x.Client_Reference_Number == uniqueClaim.Claim_Number).ToList();

                        // For each record in claiminfo list
                        //   If Company Name is Valid
                        //     if PartyType = REP then add Repairer Org + Addr
                        //     if PartyType = TPI then add (Third Party) Insurer Org + Addr
                        //     if PartyType = TPA then add (Third Party) AccidentManagement Org + Addr
                        //     if PartyType = TPS then add (Third Party) Solicitors Org + Addr
                        //     if PartyType = REC then add Recovery Org + Addr
                        //     if PartyType = CLR then Skip?
                        //     if PartyType = THP then Skip?                        
                        //     if PartyType = TPC then add Org | Addr | Veh (third Party) | Per (Third Party Claimant)
                        //     if PartyType = XXX then Skip?

                        //   If Company Name is blank
                        //      if PartyType = CLM then add Vehicle - Per (Claimant Insured)- Addr
                        //      if PartyType = CLR then add Vehicle - Per (Unknown Insured) - Addr
                        //      if PartyType = DRV then add Vehicle - Per (Driver Insured) - Addr
                        //      if PartyType = POL then Skip?
                        //      if PartyType = THP then add Vehicle - Per (Unknown Third Party) - Addr
                        //      if PartyType = TPA then Skip?
                        //      if PartyType = TPI then Skip?
                        //      if PartyType = WIT then add Vehicle - Per (Witness Insured) - Addr

                        // todo: unsure? add as no vehicle involvement



                        var vehicles = GenerateVehicles(claimInfo, claimVehicles, policyInfo);

                        motorClaim.Vehicles.AddRange(vehicles);


                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping vehicle information: " + ex);
                    }

                    #endregion

                }

                if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1) return;
            }


        }

        private IEnumerable<PipelineVehicle> GenerateVehicles(List<ClaimDataCore> claimRecords, List<ClaimDataVehicle> claimVehicles, ClaimDataCore policyInfo)
        {
            if (claimRecords == null) return null;
            if (claimVehicles == null) return null;

            var vehicles = new List<PipelineVehicle>();

            var uniqueVehicleRegs = claimVehicles.Select(x => x.Vehicle_Registration_Number).AsParallel().Distinct();
                
            // Foreach different vehicle
            foreach (var vehicleReg in uniqueVehicleRegs)
            {
                if (!string.IsNullOrEmpty(vehicleReg))
                {
                    var vehicleDetails = claimVehicles.FirstOrDefault(x => x.Vehicle_Registration_Number == vehicleReg);

                    var vehicle = MapToPipelineVehicle(vehicleDetails);
                    
                    if (vehicle != null)
                    {  
                        var personVehicleDetails = claimVehicles.Where(x => x.Vehicle_Registration_Number == vehicleReg);

                        // for each different person in vehicle
                        foreach (var personVehicleDetail in personVehicleDetails)
                        {                            
                            if (personVehicleDetail.Party_Type.StartsWith("CLM"))
                            {
                                // vehicle must be insured vehicle
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                                // this is the insured driver
                                if (personVehicleDetail.Relationship_Type.StartsWith("DRV"))
                                {
                                    var personCoreDetail = claimRecords.FirstOrDefault(x => x.Party_Type == "CLM0001");

                                    if (personCoreDetail != null)
                                    {
                                        // Create this person, add Solicitor, Repairer, etc

                                        // if name matches policyholder?
                                        var isPolicyHolder = false;
                                        if (policyInfo != null)
                                        {
                                            if (policyInfo.Forename == personCoreDetail.Forename && 
                                                policyInfo.Surname == personCoreDetail.Surname && 
                                                policyInfo.Date_Of_Birth == personCoreDetail.Date_Of_Birth)
                                            {
                                                isPolicyHolder = true;
                                            }
                                        }


                                        var person = MapToPipelinePerson(personCoreDetail, PartyType.Insured, SubPartyType.Driver, VehicleLinkType.Driver, isPolicyHolder);

                                        if (person != null)
                                        {
                                            // Get orgs to link to Insured
                                            var orgs = GenerateOrganisationsForInsuredDriver(claimRecords);
                                            
                                            if (orgs != null)
                                            {
                                                person.Organisations = new List<PipelineOrganisation>();
                                                foreach (var org in orgs)
                                                {
                                                    if (!person.Organisations.Any(x => x.OrganisationName == org.OrganisationName))
                                                        person.Organisations.Add(org);
                                                }   
                                            }

                                            // Get witnesses and add them to this car
                                            var witnesses = GenerateWitnesses(claimRecords);

                                            if (witnesses != null)
                                            {
                                                foreach (var witness in witnesses)
                                                {
                                                    if (!vehicle.People.Any(x => x.FirstName == witness.FirstName && x.LastName == witness.LastName && x.DateOfBirth == witness.DateOfBirth))
                                                        vehicle.People.Add(witness);    
                                                }
                                            }
                                                

                                            // Add driver to car
                                            if (!vehicle.People.Any(x => x.FirstName == person.FirstName && x.LastName == person.LastName && x.DateOfBirth == person.DateOfBirth))
                                                vehicle.People.Add(person);
                                        }
                                    }
                                }
                                else
                                {
                                    // this must be an insured passenger
                                    var personCoreDetail = claimRecords.FirstOrDefault(x => x.Party_Type == personVehicleDetail.Party_Type);

                                    if (personCoreDetail != null)
                                    {
                                        // Create this person, add Solicitor, Repairer, etc
                                        var person = MapToPipelinePerson(personCoreDetail, PartyType.Insured, SubPartyType.Passenger, VehicleLinkType.Passenger, false);

                                        if (person != null)
                                        {
                                            if (!vehicle.People.Any(x => x.FirstName == person.FirstName && x.LastName == person.LastName && x.DateOfBirth == person.DateOfBirth))
                                                vehicle.People.Add(person);
                                        }
                                    }

                                }
                            }                            
                            // this is the third party driver
                            else if (personVehicleDetail.Party_Type.StartsWith("THP"))
                            {          
                                // vehicle must be third party
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;

                                if (personVehicleDetail.Relationship_Type.StartsWith("DRV"))
                                {
                                    var personCoreDetail = claimRecords.FirstOrDefault(x => x.Party_Type == personVehicleDetail.Party_Type);

                                    if (personCoreDetail != null)
                                    {
                                        // Create this person, add Solicitor, Repairer, etc
                                        var person = MapToPipelinePerson(personCoreDetail, PartyType.ThirdParty, SubPartyType.Driver, VehicleLinkType.Driver, false);

                                        if (person != null)
                                        {
                                            // Get orgs to link to Insured
                                            var orgs = GenerateOrganisationsForThirdPartyDriver(claimRecords);

                                            if (orgs != null)
                                            {
                                                person.Organisations = new List<PipelineOrganisation>();

                                                foreach (var org in orgs)
                                                {
                                                    if (!person.Organisations.Any(x=>x.OrganisationName == org.OrganisationName))
                                                        person.Organisations.Add(org);    
                                                }                                                
                                            }

                                            if (!vehicle.People.Any(x => x.FirstName == person.FirstName && x.LastName == person.LastName && x.DateOfBirth == person.DateOfBirth))
                                                vehicle.People.Add(person);
                                        }
                                    }   
                                }
                                else
                                {
                                    var personCoreDetail = claimRecords.FirstOrDefault(x => x.Party_Type == personVehicleDetail.Party_Type);

                                    if (personCoreDetail != null)
                                    {
                                        // Create this person, add Solicitor, Repairer, etc
                                        var person = MapToPipelinePerson(personCoreDetail, PartyType.ThirdParty, SubPartyType.Passenger, VehicleLinkType.Passenger, false);

                                        if (person != null)
                                        {               
                                            // is person already added?
                                            if (!vehicle.People.Any(x=>x.FirstName == person.FirstName && x.LastName == person.LastName && x.DateOfBirth == person.DateOfBirth))
                                                vehicle.People.Add(person);
                                        }
                                    }   
                                }
                            }
                        }
                                          
                        vehicles.Add(vehicle);
                    }
                }
            }

            return vehicles;
           
        }

        private IEnumerable<PipelinePerson> GenerateWitnesses(IEnumerable<ClaimDataCore> data)
        {
            if (data == null) return null;

            var witnesses = data.Where(x => x.Party_Type.StartsWith("WIT")).ToList();

            return witnesses.Select(witness => MapToPipelinePerson(witness, PartyType.Witness, SubPartyType.Witness, VehicleLinkType.Witness, false)).ToList();

        }

        private List<PipelineOrganisation> GenerateOrganisationsForThirdPartyDriver(List<ClaimDataCore> data)
        {
            return data == null ? null : (from claimdetail in data where claimdetail != null && IsValidName(claimdetail.Company_Name) select MapToThirdPartyOrganisation(claimdetail) into org where org != null select org).ToList();
        }

        private List<PipelineOrganisation> GenerateOrganisationsForInsuredDriver(List<ClaimDataCore> data)
        {
            return data == null ? null : (from claimdetail in data where claimdetail != null && IsValidName(claimdetail.Company_Name) select MapToInsuredOrganisation(claimdetail) into org where org != null select org).ToList();
        }

        private PipelinePerson MapToPipelinePerson(ClaimDataCore data, PartyType partyType, SubPartyType subPartyType, VehicleLinkType vehicleLinkType, bool isPolicyHolder)
        {
            var person = new PipelinePerson
            {
                I2Pe_LinkData =
                {
                    PartyType_Id = (int)partyType,
                    SubPartyType_Id = (int)subPartyType,
                },
                V2Pe_LinkData =
                {
                    VehicleLinkType_Id = (int)vehicleLinkType
                },
                EmailAddresses = new List<PipelineEmail>(),
            };

            if (isPolicyHolder)
            {
                person.Pe2Po_LinkData = new PipelinePolicyLink
                {
                    PolicyLinkType_Id = (int)PolicyLinkType.Policyholder,
                };
            }

            if (IsValid(data.Title))
                person.Salutation_Id = (int)SalutationConvertor.ConvertTo(data.Title);

            if (IsValid(data.Forename))
                person.FirstName = data.Forename;

            if (IsValid(data.Surname))
                person.LastName = data.Surname;

            if (data.Date_Of_Birth.HasValue)
                person.DateOfBirth = data.Date_Of_Birth;

            if (IsValid(data.NI_Number))
            {
                person.NINumbers = new List<PipelineNINumber> { new PipelineNINumber { NINumber1 = data.NI_Number } };
            }

            if (IsValid(data.Home_Telephone))
            {
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.Home_Telephone, TelephoneType_Id = (int)TelephoneType.Landline });
            }


            if (IsValid(data.Work_Telephone))
            {
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.Home_Telephone, TelephoneType_Id = (int)TelephoneType.Unknown });
            }

            if (IsValid(data.Mobile_Telephone))
            {
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.Mobile_Telephone, TelephoneType_Id = (int)TelephoneType.Mobile });
            }

            if (IsValid(data.Home_Email_Address))
            {
                person.EmailAddresses.Add(new PipelineEmail { EmailAddress = data.Home_Email_Address });
            }

            if (IsValid(data.Work_Email_Address))
            {
                person.EmailAddresses.Add(new PipelineEmail { EmailAddress = data.Work_Email_Address });
            }

            if (IsValid(data.Driving_License_Number))
            {
                person.DrivingLicenseNumbers = new List<PipelineDrivingLicense> { new PipelineDrivingLicense { DriverNumber = data.Driving_License_Number } };
            }

            if (IsValid(data.Passport_Number))
            {
                person.PassportNumbers = new List<PipelinePassport> { new PipelinePassport { PassportNumber = data.Passport_Number } };
            }

            if (IsValid(data.Gender))
            {
                switch (data.Gender)
                {
                    case "Male":
                        person.Gender_Id = 1;
                        break;
                    case "Female":
                        person.Gender_Id = 2;
                        break;
                    default:
                        person.Gender_Id = 0;
                        break;
                }
            }

            if (IsValid(data.Nationality))
            {
                person.Nationality = data.Nationality;
            }

            if (_PersonMatch(person))
            {
                return null;
            }

            var address = MapToPipelineAddress(data);

            if (address != null)
            {
                person.Addresses = new List<PipelineAddress> { address };
            }

            return person;
        }

        private PipelineAddress MapToPipelineAddress(ClaimDataCore data)
        {
            if (data == null) return null;

            var address = new PipelineAddress();
            var addressPopulated = false;

            if (IsValid(data.Address_1))
            {
                address.Building = data.Address_1;
                addressPopulated = true;
            }

            if (IsValid(data.Address_2))
            {
                address.Street = data.Address_2;
                addressPopulated = true;
            }

            if (IsValid(data.Address_3))
            {
                address.Locality = data.Address_3;
                addressPopulated = true;
            }

            if (IsValid(data.Address_4))
            {
                address.Town = data.Address_4;
                addressPopulated = true;
            }

            if (IsValid(data.Address_5))
            {
                address.Town += "," + data.Address_5;
                addressPopulated = true;
            }

            if (IsValid(data.Address_6))
            {
                address.Town += "," + data.Address_6;
                addressPopulated = true;
            }

            if (IsValid(data.Address_7))
            {
                address.County = data.Address_7;
                addressPopulated = true;
            }

            if (IsValid(data.Address_8))
            {
                address.PostCode = data.Address_8;
                addressPopulated = true;
            }

            if (IsValid(data.Address_9))
            {
                address.County += "," + data.Address_9;
                addressPopulated = true;
            }

            if (IsValid(data.Address_10))
            {
                address.County += "," + data.Address_10;
                addressPopulated = true;
            }

            if (_AddressMatch(address) || !addressPopulated)
            {
                return null;
            }

            return address;
        }

        private static PipelineVehicle MapToPipelineVehicle(ClaimDataVehicle data)
        {
            if (data == null) { throw new ArgumentNullException("data"); }

            var vehicle = new PipelineVehicle
            {
                VehicleMake = DefaultValue(data.Vehicle_Make),
                VehicleModel = DefaultValue(data.Vehicle_Model),
                VehicleRegistration = DefaultValue(data.Vehicle_Registration_Number),                
            };

            return vehicle;
        }

        private PipelineOrganisation MapToInsuredOrganisation(ClaimDataCore data)
        {
            if (data == null) throw new ArgumentNullException("data");
            var org = new PipelineOrganisation
            {
                OrganisationName = data.Company_Name,
                Addresses = new List<PipelineAddress>()
            };

            // check party type is valid

            //     if PartyType = REP then add Repairer Org + Addr
            //     if PartyType = TPI then add (Third Party) Insurer Org + Addr
            //     if PartyType = TPA then add (Third Party) AccidentManagement Org + Addr
            //     if PartyType = TPS then add (Third Party) Solicitors Org + Addr
            //     if PartyType = REC then add Recovery Org + Addr
            if (data.Party_Type.StartsWith("REP") || data.Party_Type.StartsWith("REC"))
            {

                if (data.Party_Type.StartsWith("REP"))
                {
                    org.OrganisationType_Id = (int)OrganisationType.Repairer;
                    org.P2O_LinkData = new PipelinePerson2OrganisationLink
                    {
                        Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer
                    };
                }
                
                if (data.Party_Type.StartsWith("REC"))
                {
                    org.OrganisationType_Id = (int)OrganisationType.Recovery;
                    org.P2O_LinkData = new PipelinePerson2OrganisationLink
                    {
                        Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Recovery
                    };
                }

                if (_OrganisationMatch(org))
                {
                    return null;
                }
                
                var address = MapToPipelineAddress(data);
                if (address != null) { org.Addresses.Add(address); }

                return org;
            }

            return null;
        }

        private PipelineOrganisation MapToThirdPartyOrganisation(ClaimDataCore data)
        {
            if (data == null) throw new ArgumentNullException("data");
            var org = new PipelineOrganisation
            {
                OrganisationName = data.Company_Name,
                Addresses = new List<PipelineAddress>()
            };

            // check party type is valid

            //     if PartyType = REP then add Repairer Org + Addr
            //     if PartyType = TPI then add (Third Party) Insurer Org + Addr
            //     if PartyType = TPA then add (Third Party) AccidentManagement Org + Addr
            //     if PartyType = TPS then add (Third Party) Solicitors Org + Addr
            //     if PartyType = REC then add Recovery Org + Addr
            if (data.Party_Type.StartsWith("TPI") || data.Party_Type.StartsWith("TPA") || data.Party_Type.StartsWith("TPS"))
            {              

                if (data.Party_Type.StartsWith("TPI"))
                {
                    org.OrganisationType_Id = (int)OrganisationType.Repairer;
                    org.P2O_LinkData = new PipelinePerson2OrganisationLink
                    {
                        Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer
                    };
                }

                if (data.Party_Type.StartsWith("TPA"))
                {
                    org.OrganisationType_Id = (int)OrganisationType.AccidentManagement;
                    org.P2O_LinkData = new PipelinePerson2OrganisationLink
                    {
                        Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.AccidentManagement
                    };
                }

                if (data.Party_Type.StartsWith("TPS"))
                {
                    org.OrganisationType_Id = (int)OrganisationType.Solicitor;
                    org.P2O_LinkData = new PipelinePerson2OrganisationLink
                    {
                        Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor
                    };
                }               

                if (_OrganisationMatch(org))
                {
                    return null;
                }

                var address = MapToPipelineAddress(data);
                if (address != null) { org.Addresses.Add(address); }

                return org;
            }

            return null;
        }

       
        /// <summary>
        /// Detect if "NULL" is in string and ignore, value is null or value is empty
        /// </summary>
        /// <param name="value"><see cref="String"/> or null</param>
        /// <returns><see cref="bool"/>true if string has valid value</returns>
        public static bool IsValid(string value)
        {
            return !string.IsNullOrWhiteSpace(value) &&
                   !value.Equals("NULL", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Detect if "NULL" is in string and ignore, value is null or value is empty
        /// </summary>
        /// <param name="value"><see cref="String"/> or null</param>
        /// <returns><see cref="bool"/>true if string has valid value</returns>
        public static bool IsValidName(string value)
        {
            if (!IsValid(value))
                return false;

            if (value.StartsWith("*") || value.StartsWith("-") || value.StartsWith("?") || value.StartsWith("<blank>"))
            {
                return false;
            }

            return true;
        }



        /// <summary>
        /// If input string value is null, return null
        /// </summary>
        /// <param name="value">input string</param>
        /// <returns>null if input value empty or null, else returns value</returns>
        public static string DefaultValue(string value)
        {
            return IsValid(value) ? value : null;
        }

        public List<PipelineAddress> DefaultAddresses(List<string> defaultAddressesForClient)
        {
            var defaultAddresses = new List<PipelineAddress>();

            if (defaultAddressesForClient != null)
                foreach (var str in defaultAddressesForClient)
                {
                    if (!string.IsNullOrEmpty(str))
                    {
                        var address = new PipelineAddress();

                        var match = Regex.Matches(str, @"\[(.*?)\]");

                        //SB=[],BN=[],BLD=[],ST=[],LOC=[],TWN=[],CNTY=[],PC=[]

                        address.SubBuilding = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.BuildingNumber = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.Building = match[2].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.Street = match[3].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.Locality = match[4].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.Town = match[5].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.County = match[6].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.PostCode = match[7].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                        defaultAddresses.Add(address);
                    }
                }

            return defaultAddresses;
        }

        private bool _AddressMatch(PipelineAddress address)
        {

            if (address == null) return false;

            var defaultAddresses = DefaultAddresses(DefaultAddressesForClient);

            if (defaultAddresses != null)
                foreach (var adr in defaultAddresses)
                {
                    var match = false;

                    if (adr != null)
                    {
                        if (!string.IsNullOrEmpty(adr.Street) && !string.IsNullOrEmpty(address.Street))
                        {
                            if (address.Street.Equals(adr.Street, StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = true;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        if (!string.IsNullOrEmpty(adr.Town) && !string.IsNullOrEmpty(address.Town))
                        {
                            if (address.Town.Equals(adr.Town, StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = true;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        if (!string.IsNullOrEmpty(adr.County) && !string.IsNullOrEmpty(address.County))
                        {
                            if (address.County.Equals(adr.County, StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = true;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        if (!string.IsNullOrEmpty(adr.PostCode) && !string.IsNullOrEmpty(address.PostCode))
                        {
                            if (address.PostCode.Equals(adr.PostCode, StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = true;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        return match;
                    }
                }
            return false;
        }

        public List<PipelinePerson> DefaultPeople(List<string> defaultPeopleForClient)
        {
            var defaultPeople = new List<PipelinePerson>();
            if (defaultPeopleForClient == null) return defaultPeople;


            foreach (var str in defaultPeopleForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelinePerson person = new PipelinePerson();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //FN=[],LN=[]

                    person.FirstName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    person.LastName = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultPeople.Add(person);
                }
            }

            return defaultPeople;
        }

        public List<PipelineOrganisation> DefaultOrganisation(List<string> defaultOrganisationsForClient)
        {
            var defaultOrganisations = new List<PipelineOrganisation>();
            if (defaultOrganisationsForClient == null) return defaultOrganisations;

            foreach (var orgName in defaultOrganisationsForClient)
            {
                if (!string.IsNullOrEmpty(orgName))
                {
                    PipelineOrganisation organisation = new PipelineOrganisation();

                    MatchCollection match = Regex.Matches(orgName, @"\[(.*?)\]");

                    //ON=[]

                    organisation.OrganisationName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultOrganisations.Add(organisation);
                }
            }

            return defaultOrganisations;
        }

        private bool _PersonMatch(PipelinePerson person)
        {
            if (person == null) return false;

            var people = DefaultPeople(DefaultPeopleForClient);

            if (people != null)
                foreach (var p in people)
                {
                    if (p != null)
                    {
                        if (person.LastName != null &&
                           (person.FirstName != null &&
                           (person.FirstName.Equals(p.FirstName, StringComparison.InvariantCultureIgnoreCase) &&
                            person.LastName.Equals(p.LastName, StringComparison.InvariantCultureIgnoreCase))))
                        {
                            return true;
                        }
                    }
                }

            return false;
        }

        private bool _OrganisationMatch(PipelineOrganisation organisation)
        {
            if (organisation == null) return false;
            var defaultOrganisations = DefaultOrganisation(DefaultOrganisationsForClient);

            if (defaultOrganisations != null)
                foreach (var o in defaultOrganisations)
                {
                    if (o != null &&
                       (organisation.OrganisationName != null &&
                        organisation.OrganisationName.Equals(o.OrganisationName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        return true;
                    }
                }

            return false;
        }

    }
}
