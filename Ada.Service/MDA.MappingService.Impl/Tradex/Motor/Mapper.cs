﻿using FileHelpers;
using MDA.Common.Server;
using MDA.MappingService.Tradex.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using System.Text.RegularExpressions;
using MDA.Common;

namespace MDA.MappingService.Tradex.Motor
{
    public class Mapper
    {
        private Stream _fs;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string FilePath { get; set; }
        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
        public string PolicyFile { get; set; }
        public FileHelperEngine ClaimEngine { get; set; }
        public FileHelperEngine PolicyEngine { get; set; }

        private string _coreClaimDataFile;
        private string _vehicleClaimDataFile;
        private bool _debug;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private List<string> _uniqueClaimNumberList;
        private string _claimCode;
        private IEnumerable<ClaimData> _policyItemRows;
        private IEnumerable<ClaimData> _claimItemRows;
        private string _filePath = null;
        private bool _deleteExtractedFiles;
        private int _claimCount = 0;
        private string _clientFolder;
        private StreamReader _claimDataFileStreamReader;


        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void AssignFiles()
        {

            if (_fs == null)
            {
                FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Tradex\Data";
                ClaimFile = FolderPath + @"\Copy of Keoghs Data Wash13-05-15-44319.csv";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(_fs);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }

            }


        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {

                if (_fs == null)
                {
                    _coreClaimData = _coreClaimDataEngine.ReadFile(ClaimFile) as ClaimData[];
                }
                else
                {
                    _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }


            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        private void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            try
            {
                if (_debug)
                {
                    Console.WriteLine("Start");
                }

                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helper Engine (core) : ", false);
                }

                _coreClaimData = _coreClaimDataEngine.ReadFile(_coreClaimDataFile) as ClaimData[];

            }

            catch (Exception ex)
            {
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "PopulateFileHelpersEngines Error : " + ex.Message, true);
                }

                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers 
                _uniqueClaimNumberList = _coreClaimData.Where(x => x.HarloshClaim != "").Select(x => x.HarloshClaim).Distinct().ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }

        }


        public void Translate()
        {


            foreach (var claim in _uniqueClaimNumberList)
            {

                PipelineMotorClaim motorClaim = new PipelineMotorClaim();


                var uniqueClaim = _coreClaimData.Where(x => x.HarloshClaim == claim).ToList();


                if (uniqueClaim != null)
                {

                    #region Claim

                    var claimInfo = uniqueClaim.FirstOrDefault();

                    try
                    {

                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                        motorClaim.ClaimNumber = claimInfo.HarloshClaim;
                        motorClaim.IncidentDate = Convert.ToDateTime(claimInfo.AccidentDate);

                        motorClaim.ExtraClaimInfo.IncidentCircumstances = claimInfo.ClaimantsAccidentDescription;

                        #region Claim Status

                        if (claimInfo.Status == "ARCHIVED")
                        {
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                        }
                        else
                        {
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                        }

                        #endregion

                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion

                    #region Policy

                    var policyInfo = uniqueClaim.FirstOrDefault();

                    try
                    {

                        motorClaim.Policy.Insurer = "Tradex";
                        motorClaim.Policy.PolicyNumber = policyInfo.PolicyNumber;
                        motorClaim.Policy.PolicyStartDate = policyInfo.PolicyInceptionDate;
                        motorClaim.Policy.PolicyEndDate = policyInfo.PolicyLapseDate;
                        motorClaim.Policy.Broker = policyInfo.Broker;

                        switch (policyInfo.PolicyCover.ToUpper())
                        {
                            case "COMPREHENSIVE":
                            case "COMP":
                            case "COPM":
                                motorClaim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Comprehensive;
                                break;
                            case "THIRD PARTY FIRE & THEFT":
                            case "THIRD PARTY FIRE AND THEFT":
                            case "TP FIRE & THEFT":
                            case "TPF&T":
                            case "TPF&F":
                            case "TPFT":
                            case "TPT&F":
                                motorClaim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.ThirdPartyFireAndTheft;
                                break;
                            case "TP ONLY":
                            case "TPO":
                                motorClaim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.ThirdPartyOnly;
                                break;
                            default:
                                motorClaim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Unknown;
                                break;

                        }

                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping policy information: " + ex);
                    }

                    #endregion

                    #region Vehicles (If Policy Holder Reg is the same as Claimant Vehicle = Insured Vehicle. Otherwise Claimant Vehicle = Third Party Vehicle and Policy Holder Reg = Insured Vehicle)

                    #region Claimant Vehicle

                    var claimantVehicleInfo = uniqueClaim.Select(x => x.ClaimantVehicle.Replace(" ", "")).Distinct().ToList();
                    bool claimantVehicleSameRegAsInsuredVehicle = false;

                    foreach (var v in claimantVehicleInfo)
                    {
                        var uniqueVehicle = uniqueClaim.Where(x => x.ClaimantVehicle.Replace(" ", "") == v).FirstOrDefault();

                        PipelineVehicle claimantVehicle = new PipelineVehicle();

                        try
                        {

                            claimantVehicle.VehicleRegistration = uniqueVehicle.ClaimantVehicle;

                            #region Vehicle Involvement Groups (Incident2VehcileLinkType)

                            string insuredVehicleReg = uniqueVehicle.ClaimantVehicle.Replace(" ", "");
                            string thirdtPartyVehicleReg = uniqueVehicle.PolicyHolderReg.Replace(" ", "");

                            if (insuredVehicleReg == thirdtPartyVehicleReg)
                            {
                                claimantVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                                claimantVehicleSameRegAsInsuredVehicle = true;
                            }
                            else
                            {
                                claimantVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                            }

                            #endregion

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping vehicle information: " + ex);
                        }

                        motorClaim.Vehicles.Add(claimantVehicle);


                    }

                    #endregion

                    #region Insured Vehicle

                    if (claimantVehicleSameRegAsInsuredVehicle == false)
                    {

                        var insuredVehicleInfo = uniqueClaim.Select(x => x.PolicyHolderReg.Replace(" ", "")).Distinct().ToList();

                        foreach (var v in insuredVehicleInfo)
                        {
                            var uniqueVehicle = uniqueClaim.Where(x => x.PolicyHolderReg.Replace(" ", "") == v).FirstOrDefault();

                            PipelineVehicle insVehicle = new PipelineVehicle();

                            try
                            {

                                insVehicle.VehicleRegistration = uniqueVehicle.PolicyHolderReg;

                                insVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;


                            }
                            catch (Exception ex)
                            {
                                throw new CustomException("Error in mapping vehicle information: " + ex);
                            }

                            motorClaim.Vehicles.Add(insVehicle);


                        }

                    }

                    #endregion

                    #endregion

                    #region Vehicle People

                    #region Claimant

                    var claimantInfo = uniqueClaim.Where(x => x.Claimant != "").ToList();

                    foreach (var p in claimantInfo)
                    {

                        PipelinePerson claimant = new PipelinePerson();

                        try
                        {
                            var claimantName = p.Claimant.Split();

                            if (claimantName.Count() == 7)
                            {
                                claimant.Salutation_Id = SalutationHelper(claimantName[0]);
                                claimant.FirstName = claimantName[1];
                                claimant.MiddleName = claimantName[2] + " " + claimantName[3] + " " + claimantName[4] + " " + claimantName[5];
                                claimant.LastName = claimantName[6];
                            }
                            else if (claimantName.Count() == 6)
                            {
                                claimant.Salutation_Id = SalutationHelper(claimantName[0]);
                                claimant.FirstName = claimantName[1];
                                claimant.MiddleName = claimantName[2] + " " + claimantName[3] + " " + claimantName[4];
                                claimant.LastName = claimantName[5];
                            }
                            else if (claimantName.Count() == 5)
                            {
                                claimant.Salutation_Id = SalutationHelper(claimantName[0]);
                                claimant.FirstName = claimantName[1];
                                claimant.MiddleName = claimantName[2] + " " + claimantName[3];
                                claimant.LastName = claimantName[4];
                            }
                            else if (claimantName.Count() == 4)
                            {
                                claimant.Salutation_Id = SalutationHelper(claimantName[0]);
                                claimant.FirstName = claimantName[1];
                                claimant.MiddleName = claimantName[2];
                                claimant.LastName = claimantName[3];
                            }
                            else if (claimantName.Count() == 3)
                            {
                                claimant.Salutation_Id = SalutationHelper(claimantName[0]);
                                claimant.FirstName = claimantName[1];
                                claimant.LastName = claimantName[2];
                            }
                            else if (claimantName.Count() == 2)
                            {
                                claimant.Salutation_Id = SalutationHelper(claimantName[0]);
                                claimant.LastName = claimantName[1];
                            }
                            else if (claimantName.Count() == 1)
                            {
                                claimant.LastName = claimantName[0];
                            }
                            else
                            {
                                claimant.LastName = p.Claimant;
                            }

                            claimant.DateOfBirth = p.DOB;

                            if (!string.IsNullOrEmpty(p.NINumber))
                            {
                                PipelineNINumber nInumber = new PipelineNINumber();

                                nInumber.NINumber1 = p.NINumber;

                                claimant.NINumbers.Add(nInumber);
                            }

                            if (!string.IsNullOrEmpty(p.ClaimantTelephone))
                            {
                                PipelineTelephone telephone = new PipelineTelephone();

                                telephone.ClientSuppliedNumber = p.ClaimantTelephone;

                                claimant.Telephones.Add(telephone);
                            }

                            claimant.Gender_Id = GenderHelper(claimant.Salutation_Id);

                            claimant.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;


                            switch (p.ClaimantType.ToUpper())
                            {
                                case "A PASSENGER IN A VEHICLE OWNED BY SOMEONE ELSE":
                                    claimant.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                    break;
                                case "THE DRIVER":
                                    claimant.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                    break;
                                default:
                                    claimant.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    break;
                            }

                            #region Address

                            PipelineAddress address = new PipelineAddress();

                            address.Street = p.ClaimantAddressLine1;
                            address.Locality = p.ClaimantAddressLine2;
                            address.Town = p.ClaimantAddressLine3;
                            address.County = p.ClaimantAddressLine4;
                            address.PostCode = p.ClaimantPostcode;

                            claimant.Addresses.Add(address);

                            #endregion

                            #region Claimant Solicitor

                            if (!string.IsNullOrEmpty(p.ClaimantSolicitor))
                            {
                                PipelineOrganisation claimantSolicitor = new PipelineOrganisation();
                                claimantSolicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                claimantSolicitor.OrganisationName = p.ClaimantSolicitor;
                                claimant.Organisations.Add(claimantSolicitor);
                            }


                            #endregion

                            foreach (var v in motorClaim.Vehicles)
                            {
                                if (v.VehicleRegistration.Replace(" ", "") == p.ClaimantVehicle.Replace(" ", ""))
                                {
                                    v.People.Add(claimant);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping person information: " + ex);
                        }

                    }

                    #endregion

                    #region Policy Holder

                    var policyHolderInfo = uniqueClaim.Select(x => x.PolicyHolder).Distinct().ToList();


                    foreach (var p in policyHolderInfo)
                    {

                        var pcInfo = uniqueClaim.Where(x => x.PolicyHolder == p).FirstOrDefault();

                        try
                        {

                            if (pcInfo.PolicyHolder.ToUpper().Contains("T/A"))
                            {
                                var parts = Regex.Split(pcInfo.PolicyHolder, "T/A", RegexOptions.IgnoreCase);

                                PipelineOrganisation orgPolicyHolder = new PipelineOrganisation();
                                orgPolicyHolder.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                parts[1] = parts[1].TrimStart('s');
                                parts[1] = parts[1].Trim();
                                orgPolicyHolder.OrganisationName = parts[1];

                                #region Address

                                PipelineAddress address = new PipelineAddress();

                                address.Street = pcInfo.PolicyHolderAddressLine1;
                                address.Locality = pcInfo.PolicyHolderAddressLine2;
                                address.Town = pcInfo.PolicyHolderAddressLine3;
                                address.County = pcInfo.PolicyHolderAddressLine4;
                                address.PostCode = pcInfo.PolicyHolderPostcode;

                                orgPolicyHolder.Addresses.Add(address);

                                #endregion

                                motorClaim.Organisations.Add(orgPolicyHolder);

                                PipelinePerson policyHolder = new PipelinePerson();
                                parts[0] = parts[0].Trim();
                                var policyHolderName = parts[0].Split();

                                if (policyHolderName[0].ToUpper().Contains("MR") || policyHolderName[0].ToUpper().Contains("MRS") || policyHolderName[0].ToUpper().Contains("MS") || policyHolderName[0].ToUpper().Contains("MISS"))
                                {

                                    if (policyHolderName.Count() == 5)
                                    {
                                        policyHolder.Salutation_Id = SalutationHelper(policyHolderName[0]);
                                        policyHolder.FirstName = policyHolderName[1];
                                        policyHolder.MiddleName = policyHolderName[2] + " " + policyHolderName[3];
                                        policyHolder.LastName = policyHolderName[4];
                                    }
                                    else if (policyHolderName.Count() == 4)
                                    {
                                        policyHolder.Salutation_Id = SalutationHelper(policyHolderName[0]);
                                        policyHolder.FirstName = policyHolderName[1];
                                        policyHolder.MiddleName = policyHolderName[2];
                                        policyHolder.LastName = policyHolderName[3];
                                    }
                                    else if (policyHolderName.Count() == 3)
                                    {
                                        policyHolder.Salutation_Id = SalutationHelper(policyHolderName[0]);
                                        policyHolder.FirstName = policyHolderName[1];
                                        policyHolder.LastName = policyHolderName[2];
                                    }
                                    else if (policyHolderName.Count() == 2)
                                    {
                                        policyHolder.Salutation_Id = SalutationHelper(policyHolderName[0]);
                                        policyHolder.LastName = policyHolderName[1];
                                    }
                                    else if (policyHolderName.Count() == 1)
                                    {
                                        policyHolder.LastName = policyHolderName[0];
                                    }
                                    else
                                    {
                                        policyHolder.LastName = pcInfo.PolicyHolder;
                                    }
                                }
                                else
                                {
                                    if (policyHolderName.Count() == 5)
                                    {
                                        policyHolder.FirstName = policyHolderName[0];
                                        policyHolder.MiddleName = policyHolderName[1] + " " + policyHolderName[2] + " " + policyHolderName[3];
                                        policyHolder.LastName = policyHolderName[4];
                                    }
                                    else if (policyHolderName.Count() == 4)
                                    {
                                        policyHolder.FirstName = policyHolderName[0];
                                        policyHolder.MiddleName = policyHolderName[1] + " " + policyHolderName[2];
                                        policyHolder.LastName = policyHolderName[3];
                                    }
                                    else if (policyHolderName.Count() == 3)
                                    {
                                        policyHolder.FirstName = policyHolderName[0];
                                        policyHolder.MiddleName = policyHolderName[1];
                                        policyHolder.LastName = policyHolderName[2];
                                    }
                                    else if (policyHolderName.Count() == 2)
                                    {
                                        policyHolder.FirstName = policyHolderName[0];
                                        policyHolder.LastName = policyHolderName[1];
                                    }
                                    else if (policyHolderName.Count() == 1)
                                    {
                                        policyHolder.LastName = policyHolderName[0];
                                    }
                                    else
                                    {
                                        policyHolder.LastName = pcInfo.PolicyHolder;
                                    }
                                }

                                policyHolder.Gender_Id = GenderHelper(policyHolder.Salutation_Id);

                                policyHolder.I2Pe_LinkData.PartyType_Id = (int)PartyType.Policyholder;

                                #region Address

                                policyHolder.Addresses.Add(address);

                                #endregion

                                foreach (var v in motorClaim.Vehicles)
                                {
                                    if (v.VehicleRegistration.Replace(" ", "") == pcInfo.PolicyHolderReg.Replace(" ", ""))
                                    {
                                        v.People.Add(policyHolder);
                                    }
                                }
                            }
                            else if (pcInfo.PolicyHolder.ToUpper().Contains("LTD") || pcInfo.PolicyHolder.ToUpper().Contains("LLP") || pcInfo.PolicyHolder.Contains("&"))
                            {
                                PipelineOrganisation orgPolicyHolder = new PipelineOrganisation();
                                orgPolicyHolder.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                orgPolicyHolder.OrganisationName = pcInfo.PolicyHolder;

                                #region Address

                                PipelineAddress address = new PipelineAddress();

                                address.Street = pcInfo.PolicyHolderAddressLine1;
                                address.Locality = pcInfo.PolicyHolderAddressLine2;
                                address.Town = pcInfo.PolicyHolderAddressLine3;
                                address.County = pcInfo.PolicyHolderAddressLine4;
                                address.PostCode = pcInfo.PolicyHolderPostcode;

                                orgPolicyHolder.Addresses.Add(address);

                                #endregion

                                motorClaim.Organisations.Add(orgPolicyHolder);
                            }
                            else
                            {

                                PipelinePerson policyHolder = new PipelinePerson();

                                var policyHolderName = pcInfo.PolicyHolder.Split();

                                if (policyHolderName[0].ToUpper().Contains("MR") || policyHolderName[0].ToUpper().Contains("MRS") || policyHolderName[0].ToUpper().Contains("MS") || policyHolderName[0].ToUpper().Contains("MISS"))
                                {

                                    if (policyHolderName.Count() == 5)
                                    {
                                        policyHolder.Salutation_Id = SalutationHelper(policyHolderName[0]);
                                        policyHolder.FirstName = policyHolderName[1];
                                        policyHolder.MiddleName = policyHolderName[2] + " " + policyHolderName[3];
                                        policyHolder.LastName = policyHolderName[4];
                                    }
                                    else if (policyHolderName.Count() == 4)
                                    {
                                        policyHolder.Salutation_Id = SalutationHelper(policyHolderName[0]);
                                        policyHolder.FirstName = policyHolderName[1];
                                        policyHolder.MiddleName = policyHolderName[2];
                                        policyHolder.LastName = policyHolderName[3];
                                    }
                                    else if (policyHolderName.Count() == 3)
                                    {
                                        policyHolder.Salutation_Id = SalutationHelper(policyHolderName[0]);
                                        policyHolder.FirstName = policyHolderName[1];
                                        policyHolder.LastName = policyHolderName[2];
                                    }
                                    else if (policyHolderName.Count() == 2)
                                    {
                                        policyHolder.Salutation_Id = SalutationHelper(policyHolderName[0]);
                                        policyHolder.LastName = policyHolderName[1];
                                    }
                                    else if (policyHolderName.Count() == 1)
                                    {
                                        policyHolder.LastName = policyHolderName[0];
                                    }
                                    else
                                    {
                                        policyHolder.LastName = pcInfo.PolicyHolder;
                                    }
                                }
                                else
                                {
                                    if (policyHolderName.Count() == 5)
                                    {
                                        policyHolder.FirstName = policyHolderName[0];
                                        policyHolder.MiddleName = policyHolderName[1] + " " + policyHolderName[2] + " " + policyHolderName[3];
                                        policyHolder.LastName = policyHolderName[4];
                                    }
                                    else if (policyHolderName.Count() == 4)
                                    {
                                        policyHolder.FirstName = policyHolderName[0];
                                        policyHolder.MiddleName = policyHolderName[1] + " " + policyHolderName[2];
                                        policyHolder.LastName = policyHolderName[3];
                                    }
                                    else if (policyHolderName.Count() == 3)
                                    {
                                        policyHolder.FirstName = policyHolderName[0];
                                        policyHolder.MiddleName = policyHolderName[1];
                                        policyHolder.LastName = policyHolderName[2];
                                    }
                                    else if (policyHolderName.Count() == 2)
                                    {
                                        policyHolder.FirstName = policyHolderName[0];
                                        policyHolder.LastName = policyHolderName[1];
                                    }
                                    else if (policyHolderName.Count() == 1)
                                    {
                                        policyHolder.LastName = policyHolderName[0];
                                    }
                                    else
                                    {
                                        policyHolder.LastName = pcInfo.PolicyHolder;
                                    }
                                }

                                policyHolder.Gender_Id = GenderHelper(policyHolder.Salutation_Id);

                                policyHolder.I2Pe_LinkData.PartyType_Id = (int)PartyType.Policyholder;

                                #region Address

                                PipelineAddress address = new PipelineAddress();

                                address.Street = pcInfo.PolicyHolderAddressLine1;
                                address.Locality = pcInfo.PolicyHolderAddressLine2;
                                address.Town = pcInfo.PolicyHolderAddressLine3;
                                address.County = pcInfo.PolicyHolderAddressLine4;
                                address.PostCode = pcInfo.PolicyHolderPostcode;

                                policyHolder.Addresses.Add(address);

                                #endregion

                                foreach (var v in motorClaim.Vehicles)
                                {
                                    if (v.VehicleRegistration.Replace(" ", "") == pcInfo.PolicyHolderReg.Replace(" ", ""))
                                    {
                                        v.People.Add(policyHolder);
                                    }
                                }

                            }




                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping person information: " + ex);
                        }

                    }

                    #endregion

                    #endregion

                    #region Insured Vehicle and Driver Check

                    bool insuredVehicle = false;
                    bool insuredDriver = false;

                    foreach (var vehicle in motorClaim.Vehicles)
                    {
                        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                        {
                            insuredVehicle = true;

                            foreach (var person in vehicle.People)
                            {
                                if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                                {
                                    insuredDriver = true;
                                }

                                if (insuredDriver == false && vehicle.People.Count() == 1)
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                    insuredDriver = true;
                                }
                            }
                        }
                    }

                    if (insuredVehicle == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                        }

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);
                    }
                    else
                    {
                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            foreach (var vehicle in motorClaim.Vehicles)
                            {
                                if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                {

                                    vehicle.People.Add(defaultInsuredDriver);

                                }

                            }

                        }
                    }

                    #endregion

                    if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;

                }
            }


        }


        public int SalutationHelper(string salutation)
        {
            int salutationId;

            switch (salutation.ToUpper())
            {
                case "MR":
                    salutationId = (int)Salutation.Mr;
                    break;
                case "MRS":
                    salutationId = (int)Salutation.Mrs;
                    break;
                case "MS":
                    salutationId = (int)Salutation.Ms;
                    break;
                case "MISS":
                    salutationId = (int)Salutation.Miss;
                    break;
                case "MASTER":
                    salutationId = (int)Salutation.Master;
                    break;
                case "DR":
                    salutationId = (int)Salutation.Dr;
                    break;
                case "REV":
                    salutationId = (int)Salutation.Reverend;
                    break;
                default:
                    salutationId = (int)Salutation.Unknown;
                    break;
            }

            return salutationId;

        }

        public int GenderHelper(int salutation)
        {
            int genderId;

            switch (salutation)
            {
                case 1:
                case 5:
                    genderId = (int)Gender.Male;
                    break;
                case 2:
                case 3:
                case 4:
                    genderId = (int)Gender.Female;
                    break;
                default:
                    genderId = (int)Gender.Unknown;
                    break;
            }

            return genderId;

        }

    }
}
