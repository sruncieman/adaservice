﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Tradex.Motor.Model
{
    [IgnoreFirst(3)]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Proclaim;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HarloshClaim;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NINumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantAddressLine1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantAddressLine2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantAddressLine3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantAddressLine4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantTelephone;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DOB;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantVehicle;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? AccidentDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Status;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantsAccidentDescription;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantSolicitor;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Street1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Broker;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyHolder;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyHolderAddressLine1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyHolderAddressLine2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyHolderAddressLine3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyHolderAddressLine4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyHolderPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyHolderReg;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyInceptionDate;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyLapseDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyCover;

    }
}


