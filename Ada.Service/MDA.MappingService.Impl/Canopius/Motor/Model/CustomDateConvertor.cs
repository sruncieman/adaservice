﻿using System;
using System.Globalization;
using FileHelpers;

namespace MDA.MappingService.Impl.Canopius.Motor.Model
{
    public class CustomDateConvertor : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;

            if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, DateTimeStyles.None, out dt))
                return dt;

            return dt;
        }
    }
}
