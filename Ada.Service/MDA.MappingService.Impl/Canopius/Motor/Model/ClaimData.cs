﻿using System;
using FileHelpers;
// ReSharper disable InconsistentNaming
namespace MDA.MappingService.Impl.Canopius.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string UniqueID;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LastUpdate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SeqNbr;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LineofBusiness;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? IncidentDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ReportedDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EnteredDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MOJStatus;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ReasonforChange;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PortalApplicationID;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? CNFReceivedDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AcceptedDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Jurisdiction;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Scheme;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Brand;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimAllocationCategory;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NonCaptureLossDCReason;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Handler;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Team;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Office;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyholderFirstName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyholderLastName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyholderAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyholderCityCounty;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyholderPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredVehicleRegistration;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredDriverFirstName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredDriverLastName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredDriverAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredDriverCityCounty;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredDriverPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantFirstName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantLastName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConvertor))]
        public DateTime? ClaimantDOB;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantGender;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NINumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantCityCounty;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantPhoneNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantEmail;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantInvolvementRTAOnly;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantVehicleRegistration;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Represented;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantSolicitor;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantSolicitorPortalID;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantSolicitorAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantSolicitorCityCounty;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantSolicitorPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantFeeEarner;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RehabilitationRequired;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RehabilitationArranged;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RehabilitationReason;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PreparedtoProvideRehabilitation;


        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CNFResponseDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LiabilityDecision;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PreMedStatus;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstPreMedDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstPreMedGDAmount;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LastPreMedDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LastPreMedGDAmount;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ExitProcessParty;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MOJExitReason;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ExitDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InterimRequestDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InterimRequestAmount;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InterimResponseDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InterimResponseAmount;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PICASIndicator;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PICASDeclineReason;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackTotal;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackGeneralDamages;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackCarHire;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackCareServices;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackClaimantLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackClothing;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackEmployerLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackFares;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackLOU;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackMedicalExpenses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackOtherLosses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackPolicyExcess;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackRepairCosts;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseTime;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseTotal;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseGeneralDamages;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseCarHire;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseCareServices;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseClaimantLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseClothing;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseEmployerLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseFares;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseLOU;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseMedicalExpenses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseOtherLosses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponsePolicyExcess;


        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstSettlementPackResponseRepairCosts;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackTotal;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackGeneralDamages;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackCarHire;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackCareServices;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackClaimantLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackClothing;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackEmployerLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackFares;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackLOU;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackMedicalExpenses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackOtherLosses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackPolicyExcess;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackRepairCosts;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseTime;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseTotal;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseGeneralDamages;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseCarHire;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseCareServices;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseClaimantLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseClothing;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseEmployerLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseFares;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseLOU;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseMedicalExpenses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseOtherLosses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponsePolicyExcess;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestSettlementPackResponseRepairCosts;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CountofInsuerCounterOffers;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage2DeadlineDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackTotal;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackGeneralDamages;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackCarHire;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackCareServices;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackClaimantLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackClothing;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackEmployerLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackFares;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackLOU;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackMedicalExpenses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackOtherLosses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackPolicyExcess;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Stage3PackRepairCosts;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettlementDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettlementType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public decimal? SettledTotal;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantLiability;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledNet;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SolicitorCosts;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledGeneralDamages;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledCarHire;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledCareServices;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledClaimantLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledClothing;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledEmployerLOE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledFares;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledLOU;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledMedicalExpenses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledOtherLosses;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledPolicyExcess;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SettledRepairCosts;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstPrognosisDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstMedicalExpertFirstName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstMedicalExpertLastName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstMedicalExpertType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestPrognosisDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestMedicalExpertFirstName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestMedicalExpertLastName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestMedicalExpertType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LatestCLADate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COAGDEstimateLow;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COAGDEstimateHigh;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COACalibration;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COALow;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COAHigh;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CLAFrequency;

    }
}
