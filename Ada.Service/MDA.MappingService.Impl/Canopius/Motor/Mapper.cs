﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using MDA.MappingService.Impl.Canopius.Motor.Model;
// ReSharper disable InconsistentNaming

namespace MDA.MappingService.Impl.Canopius.Motor
{
    public class Mapper : IMapper
    {
        private readonly Stream _fs;
        private StreamReader _claimDataFileStreamReader;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public string ClaimFile { get; set; }
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx,
            Func<CurrentContext, IPipelineClaim, object, int> processClaimFn, object statusTracking)
        {
            _fs = fs;
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void AssignFiles()
        {
            _claimDataFileStreamReader = new StreamReader(_fs);
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: ", ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: ", ex);
            }
        }


        public void PopulateFileHelpersEngines(Common.Server.CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers

                DateTime skipDate;

                var skipdateValue = ConfigurationManager.AppSettings["CanopiusSkipDate"];

                if (!string.IsNullOrEmpty(skipdateValue))
                {
                    if (DateTime.TryParseExact(skipdateValue, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out skipDate))
                    {
                        _uniqueClaimNumberList = _coreClaimData.Where(x => x.ClaimNumber != "" && x.IncidentDate.HasValue && x.IncidentDate.Value >= skipDate).Select(x => x.ClaimNumber).AsParallel().Distinct().OrderBy(x => x).ToList();
                        return;
                    }
                }

                _uniqueClaimNumberList = _coreClaimData.Where(x => x.ClaimNumber != "").Select(x => x.ClaimNumber).AsParallel().Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retrieving distinct claims: ", ex);
            }
        }

        public void Translate()
        {
            var skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["CanopiusSkipNum"]);
            var takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["CanopiusTakeNum"]);

            List<string> filteredUniqueClaimList;

            if ((skipValue == 0) && (takeValue == 0))
            {
                filteredUniqueClaimList = _uniqueClaimNumberList;
            }
            else
            {
                filteredUniqueClaimList = _uniqueClaimNumberList.Skip(skipValue).Take(takeValue).ToList();    
            }

            foreach (var claim in filteredUniqueClaimList)
            {
                var motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.FirstOrDefault(x => x.ClaimNumber == claim);

                if (uniqueClaim != null)
                {
                    // bug check - too many people being linked to same claim where policy number = claim number
                    if (uniqueClaim.ClaimNumber != null && uniqueClaim.PolicyNumber != null)
                    {
                        if (uniqueClaim.ClaimNumber.Equals(uniqueClaim.PolicyNumber))
                        {
                            continue;
                        }
                    }

                    // Get all records in import linked to this Claim Number
                    var claimInfo = _coreClaimData
                        .Where(c => c.ClaimNumber == claim)
                        .ToList();

                    


                    #region Claim

                    try
                    {
                        if (IsValid(uniqueClaim.ClaimNumber))
                            motorClaim.ClaimNumber = uniqueClaim.ClaimNumber;

                        if (uniqueClaim.IncidentDate != null)
                            motorClaim.IncidentDate = uniqueClaim.IncidentDate.Value;

                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim: ", ex);
                    }

                    #endregion Claim

                    #region ClaimInfo

                    try
                    {
                        if (IsValid(uniqueClaim.SettlementType))
                        {
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                        }
                        else
                        {
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                        }

                        if (uniqueClaim.CNFReceivedDate.HasValue)
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = uniqueClaim.CNFReceivedDate;

                        if (IsValid(uniqueClaim.LineofBusiness))
                        {
                            motorClaim.ExtraClaimInfo.ClaimCode = uniqueClaim.LineofBusiness;
                        }

                        if (uniqueClaim.SettledTotal.HasValue)
                        {
                            motorClaim.ExtraClaimInfo.PaymentsToDate = PopulatePaymentsToDate(claimInfo); ;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: ", ex);
                    }

                    #endregion ClaimInfo

                    #region Policy

                    try
                    {
                        motorClaim.Policy.Insurer = "Canopius";

                        motorClaim.Policy.PolicyNumber = uniqueClaim.PolicyNumber;

                        motorClaim.Policy.InsurerTradingName = uniqueClaim.Brand;
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping policy: ", ex);
                    }

                    #endregion Policy

                    // is policy holder an organisation or a person? 
                    // Or is there no policy holder data
                    if (IsValid(uniqueClaim.PolicyholderFirstName) || (!IsValid(uniqueClaim.PolicyholderFirstName) && !(IsValid(uniqueClaim.PolicyholderLastName))))

                        
                    {
                        // must be a person, add a vehicle to incident
                        #region Personal Insured Vehicles

                        try
                        {
                            var insuredVehicle = GenerateInsuredVehicleAndPolicyHolderAndInsuredDriversAndAddress(claimInfo);
                            if (insuredVehicle != null)
                            {
                                motorClaim.Vehicles.Add(insuredVehicle);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping vehicle information: ", ex);
                        }

                        #endregion Personal Vehicles

                    }
                    else
                    {
                        // policy holder is an org, add the vehicle to an org
                        #region Company Insured Vehicles

                        try
                        {
                            // Add an organisation as a policy holder
                            var policyOrg = GeneratePolicyHolderOrgAndVehicleAndPersonAndAddress(claimInfo);

                            if (policyOrg != null)
                            {
                                motorClaim.Organisations.Add(policyOrg);
                            }
                            else
                            {
                                throw new CustomException("Unable to map company vehicle:" + uniqueClaim.UniqueID);
                            }                            
                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping vehicle information: " + ex);
                        }

                        #endregion Company Insured Vehicles
                    }

                    #region ThirdPartyVehicle
                    var tpVehicles = GenerateTPVehicles(claimInfo);
                    if (tpVehicles != null)
                    {
                        motorClaim.Vehicles.AddRange(tpVehicles);
                    }
                    #endregion

                    #region Organisations


                    #endregion Organisations
                }

                if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1) return;

            }
        }

        /// <summary>
        /// Extract Third Party Vehicles from claim data
        /// </summary>
        /// <param name="claimInfo"><see cref="List{ClaimData}"/></param>
        /// <returns><see cref="List{PipelineVehicle}"/></returns>
        private List<PipelineVehicle> GenerateTPVehicles(List<ClaimData> claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            // order reg so same vehicles together
            var claimInfoOrdered = claimInfo.OrderBy(x => x.ClaimantVehicleRegistration);

            var list = new List<PipelineVehicle>();            

            PipelineVehicle vehicle = null;
            var prevReg = string.Empty;

            
            foreach (var data in claimInfoOrdered)
            {   
                // if prevReg != thisReg, get new vehicle
                if (vehicle == null || !prevReg.Equals(data.ClaimantVehicleRegistration, StringComparison.InvariantCultureIgnoreCase))
                {
                    vehicle = MapToPipelineTPVehicle(data);    
                }

                if (vehicle != null)
                {
                    var person = MapToPipelineThirdPartyPerson(data);
                    if (person != null)
                    {
                        vehicle.People.Add(person);
                    }


                    if (!prevReg.Equals(data.ClaimantVehicleRegistration, StringComparison.InvariantCultureIgnoreCase))
                    {
                        list.Add(vehicle);
                    }

                    prevReg = vehicle.VehicleRegistration;
                }                

            }

            return list;
        }


        /// <summary>
        /// Extract Third Party Person information from Claim
        /// </summary>
        /// <param name="data"><see cref="ClaimData"/></param>
        /// <returns><see cref="PipelinePerson"/></returns>
        private PipelinePerson MapToPipelineThirdPartyPerson(ClaimData data)
        {
            if (data == null) { throw new ArgumentNullException("data"); }

            if (IsValid(data.ClaimantLastName))
            {
                var subParty = SubPartyType.Unknown;
                var party = PartyType.ThirdParty;

                if (IsValid(data.ClaimantInvolvementRTAOnly))
                {
                    switch (data.ClaimantInvolvementRTAOnly.ToLower())
                    {
                        case "driver":
                        case "motorcyclist":
                            subParty = SubPartyType.Driver;
                            break;
                        case "a passenger in or on a vehicle owned by someone else":
                            subParty = SubPartyType.Passenger;
                            break;
                    }    
                }
                
                var person = new PipelinePerson
                {
                    I2Pe_LinkData =
                    {
                        PartyType_Id = (int)party,
                        SubPartyType_Id = (int)subParty
                    },
                    
                    FirstName = DefaultValue(data.ClaimantFirstName),
                    LastName = DefaultValue(data.ClaimantLastName),
                    DateOfBirth = data.ClaimantDOB
                };

                switch(data.ClaimantGender.ToLower())
                {
                    case "male":
                        person.Gender_Id = (int)Gender.Male;
                        break;
                    case "female":
                        person.Gender_Id = (int)Gender.Female;
                        break;
                    default:
                        person.Gender_Id = (int)Gender.Unknown;
                        break;
                }


                // Additional info
                if (IsValid(data.NINumber))
                {
                    person.NINumbers.Add(new PipelineNINumber { NINumber1 = DefaultValue(data.NINumber) });
                }

                if (IsValid(data.ClaimantEmail))
                {
                    person.EmailAddresses.Add(new PipelineEmail { EmailAddress = DefaultValue(data.ClaimantEmail) });
                }

                if (IsValid(data.ClaimantPhoneNumber))
                {
                    person.Telephones.Add(new PipelineTelephone{ ClientSuppliedNumber = DefaultValue(data.ClaimantPhoneNumber) });
                }                
                
                // Address
                var address = MapToPipelineAddress(data.InsuredDriverAddress, null, data.InsuredDriverCityCounty, data.InsuredDriverPostcode);

                if (address != null)
                {
                    person.Addresses.Add(address);
                }

                // solicitors
                var org = MapToTPClaimantSolicitors(data);

                if (org != null)
                    person.Organisations.Add(org);

                return person;
            }

            return null;

        }

        /// <summary>
        /// Extract Third Party Solicitors Organisation from claim
        /// </summary>
        /// <param name="data"><see cref="ClaimData"/></param>
        /// <returns><see cref="PipelineOrganisation"/></returns>
        private static PipelineOrganisation MapToTPClaimantSolicitors(ClaimData data)
        {
            if (data == null) { throw new ArgumentNullException("data"); }
            
            if (IsValid(data.ClaimantSolicitor))
            {
                var org = new PipelineOrganisation
                {
                    I2O_LinkData = { OrganisationType_Id = (int)OrganisationType.Solicitor},
                    OrganisationName = DefaultValue(data.ClaimantSolicitor),
                    OrganisationType_Id = (int)OrganisationType.Solicitor
                };

                var address = MapToPipelineAddress(data.ClaimantSolicitorAddress, null, data.ClaimantSolicitorCityCounty, data.ClaimantSolicitorPostcode);

                if (address != null)
                {
                    org.Addresses.Add(address);
                }

                return org;
            }

            return null;
        }

        /// <summary>
        /// Extract Third Party Vehicle information from claim
        /// </summary>
        /// <param name="data"><see cref="ClaimData"/></param>
        /// <returns><see cref="PipelineVehicle"/></returns>
        private static PipelineVehicle MapToPipelineTPVehicle(ClaimData data)
        {
            if (data == null) { throw new ArgumentNullException("data"); }

            if (IsValid(data.ClaimantVehicleRegistration))
            {
                var vehicle = new PipelineVehicle
                {
                    VehicleRegistration = DefaultValue(data.ClaimantVehicleRegistration),
                    I2V_LinkData = { Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle }
                };

                return vehicle;
            }

            return null;            
        }


        /// <summary>
        /// Extract pipeline vehicle data from claim
        /// </summary>
        /// <param name="claimInfo"><see cref="List{ClaimData}"/>data from which to extract vehicle</param>
        /// <returns><see cref="PipelineVehicle"/>Populated Pipeline Vehicle Entity</returns>
        private PipelineVehicle GenerateInsuredVehicleAndPolicyHolderAndInsuredDriversAndAddress(List<ClaimData> claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var insuredVehicle = MapToPipelineInsuredVehicle(claimInfo.FirstOrDefault());            

            var data = claimInfo.FirstOrDefault();

            if (data != null)
            {
                // Add a policy holder
                if (IsValid(data.PolicyholderFirstName) && IsValid(data.PolicyholderLastName))
                {
                    // Add a person policy holder
                    var policyHolder = GeneratePolicyHolderPerson(data);
                    var insuredPersons = GenerateInsuredPersons(claimInfo);
                    
                    if (insuredPersons != null)
                    {
                        // if policy holder in here, set party type to driver, don't add another person
                        foreach (var person in insuredPersons)
                        {
                            if (!PolicyHolderMatchesInsured(policyHolder, person))
                            {
                                insuredVehicle.People.Add(person);
                            }
                            else
                            {
                                policyHolder.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                            }
                        }
                    }                    

                    if (policyHolder != null)
                    {
                        insuredVehicle.People.Add(policyHolder);
                    }
                }
                else
                {
                    // Missing policy holder, add insured drivers
                    var insuredPersons = GenerateInsuredPersons(claimInfo);

                    foreach (var person in insuredPersons)
                    {
                        if (person != null)
                        {
                            // Check for duplicates
                            if (!insuredVehicle.People.Any(x=>x.FirstName == person.FirstName && x.LastName == person.LastName))
                            {
                                insuredVehicle.People.Add(person);    
                            }
                            
                        }
                    }
                }
            }

            return insuredVehicle;
        }

        /// <summary>
        /// Extract Person and Address information from Claim
        /// </summary>
        /// <param name="data"><see cref="ClaimData"/></param>
        /// <returns><see cref="PipelinePerson"/></returns>
        private static PipelinePerson MapToPipelinePersonInsuredDriver(ClaimData data)
        {
            if (data == null) { throw new ArgumentNullException("data"); }

            if (IsValid(data.InsuredDriverFirstName))
            {
                var person = new PipelinePerson
                {
                    I2Pe_LinkData =
                    {
                        PartyType_Id = (int)PartyType.Insured,
                        SubPartyType_Id = (int)SubPartyType.Driver
                    },
                    Pe2Po_LinkData =
                    {
                        PolicyLinkType_Id = (int)PolicyLinkType.Insured,
                    },
                    FirstName = DefaultValue(data.InsuredDriverFirstName),
                    LastName = DefaultValue(data.InsuredDriverLastName)
                };

                var address = MapToPipelineAddress(data.InsuredDriverAddress, null, data.InsuredDriverCityCounty, data.InsuredDriverPostcode);

                if (address != null)
                {                    
                    person.Addresses.Add(address);                    
                }                

                return person;
            }

            return null;
        }

        /// <summary>
        /// Extract insured drivers from claim info
        /// </summary>
        /// <param name="claimInfo"><see cref="List{ClaimData}"/></param>        
        /// <returns><see cref="List{PipelinePerson}"/></returns>
        private List<PipelinePerson> GenerateInsuredPersons(List<ClaimData> claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }
            
            var list = new List<PipelinePerson>();

            foreach (var data in claimInfo)
            {
                var person = MapToPipelinePersonInsuredDriver(data);

                if (person != null)
                {
                    if (!list.Any(x => x.FirstName == person.FirstName && x.LastName == person.LastName))
                    {
                        list.Add(person);
                    }
                }
            }

            return list;
        }


        /// <summary>
        /// Check if two PipelinePerson's are equal
        /// </summary>
        /// <param name="policyHolder"><see cref="PipelinePerson"/></param>
        /// <param name="person"><see cref="PipelinePerson"/></param>
        /// <returns><see cref="bool"/> true if match</returns>
        private static bool PolicyHolderMatchesInsured(PipelinePerson policyHolder, PipelinePerson person)
        {
            if ((policyHolder == null) || (person == null)) return false;

            
            return policyHolder.FirstName.Equals(person.FirstName, StringComparison.InvariantCultureIgnoreCase) &&
                   policyHolder.LastName.Equals(person.LastName, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Extract Policy Holder Organisation from claim data with child entities
        /// </summary>        
        /// <param name="claimInfo"><see cref="List{ClaimData}"/></param>
        /// <returns><see cref="PipelineOrganisation"/></returns>
        private PipelineOrganisation GeneratePolicyHolderOrgAndVehicleAndPersonAndAddress(List<ClaimData> claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var data = claimInfo.FirstOrDefault();

            var org = GeneratePolicyHolderOrg(data);

            if (org != null)
            {
                var insuredVehicle = MapToPipelineInsuredVehicle(data);
                insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                insuredVehicle.V2Po_LinkData.PolicyLinkType_Id = (int)PolicyLinkType.Policyholder;

                var insuredPersons = GenerateInsuredPersons(claimInfo);

                if (insuredPersons != null && insuredPersons.Any())
                {
                    insuredVehicle.People.AddRange(insuredPersons);
                }

                return org;
            }

            return null;
        }

        /// <summary>
        /// Extract Policy Holder Organisation from claim data
        /// </summary>
        /// <param name="data"><see cref="ClaimData"/></param>
        /// <returns><see cref="PipelineOrganisation"/></returns>
        private static PipelineOrganisation GeneratePolicyHolderOrg(ClaimData data)
        {
            if (data == null) { throw new ArgumentNullException("data"); }

            if (IsValid(data.PolicyholderLastName))
            {
                var org = new PipelineOrganisation
                {
                    OrganisationName = DefaultValue(data.PolicyholderLastName),
                    OrganisationType_Id = (int)OrganisationType.PolicyHolder,
                    pV2O_LinkData = { VehicleLinkType_Id = (int)VehicleLinkType.Unknown },
                    P2O_LinkData = { Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown}
                };

                var address = MapToPipelineAddress(data.PolicyholderAddress, null, data.PolicyholderCityCounty, data.PolicyholderPostcode);

                if (address != null)
                {
                    org.Addresses.Add(address);
                }                
                return org;
            }

            return null;
            
        }

        /// <summary>
        /// Extract Policy Holder Person from claim data
        /// </summary>
        /// <param name="data"><see cref="ClaimData"/></param>
        /// <returns><see cref="PipelinePerson"/></returns>
        private static PipelinePerson GeneratePolicyHolderPerson(ClaimData data)
        {
            if (data == null) {  throw new ArgumentNullException("data");}

            if (IsValid(data.PolicyholderLastName))
            {
                var person = new PipelinePerson
                {
                    I2Pe_LinkData =
                    {
                        PartyType_Id = (int)PartyType.Policyholder,
                        SubPartyType_Id = (int)SubPartyType.Unknown
                    },
                    Pe2Po_LinkData =
                    {
                        PolicyLinkType_Id = (int)PolicyLinkType.Policyholder,
                    },

                    FirstName = DefaultValue(data.PolicyholderFirstName),
                    LastName = DefaultValue(data.PolicyholderLastName)

                };

                var address = MapToPipelineAddress(data.PolicyholderAddress, null, data.PolicyholderCityCounty, data.PolicyholderPostcode);

                if (address != null)
                {
                    person.Addresses.Add(address);
                }

                return person;

            }
            return null;
        }

        /// <summary>
        /// Extract Pipeline Address from claim info
        /// </summary>
        /// <param name="address1">address1</param>
        /// <param name="address2">address2</param>
        /// <param name="town">town</param>
        /// <param name="postcode">postcode</param>
        /// <param name="addressType"><see cref="AddressLinkType"/>Type of address</param>
        /// <returns><see cref="PipelineAddress"/></returns>
        private static PipelineAddress MapToPipelineAddress(string address1, string address2, string town, string postcode, AddressLinkType addressType = AddressLinkType.Unknown)
        {
            try
            {
                if (((IsValid(address1)) || IsValid(address2)) && IsValid(postcode))
                {


                    var address = new PipelineAddress
                    {
                        Street = DefaultValue(address1),
                        Locality = DefaultValue(address2),
                        Town = DefaultValue(town),
                        PostCode = DefaultValue(postcode),
                        AddressType_Id = (int)addressType,                        
                    };

                    return address;
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in mapping address information: " + ex);
            }

            return null;
        }

        /// <summary>
        /// Extract pipeline vehicle data from claim
        /// </summary>
        /// <param name="claimInfo"><see cref="ClaimData"/>data from which to extract vehicle</param>
        /// <returns><see cref="PipelineVehicle"/>Populated Pipeline Vehicle Entity</returns>
        private static PipelineVehicle MapToPipelineInsuredVehicle(ClaimData claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var insuredVehicle = new PipelineVehicle
            {
                VehicleRegistration = DefaultValue(claimInfo.InsuredVehicleRegistration),
                I2V_LinkData = { Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle }
            };

            return insuredVehicle;
        }

        /// <summary>
        /// Detect if "NULL" is in string and ignore, value is null or value is empty
        /// </summary>
        /// <param name="value"><see cref="String"/> or null</param>
        /// <returns><see cref="bool"/>true if string has valid value</returns>
        public static bool IsValid(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            value = Fix(value);
            
            return !string.IsNullOrEmpty(value) &&
                   !value.Equals("NULL", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// If input string value is null, return null
        /// </summary>
        /// <param name="value">input string</param>
        /// <returns>null if input value empty or null, else returns value</returns>
        public static string DefaultValue(string value)
        {
            return IsValid(value) ? Fix(value) : null;
        }

        private static string Fix(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            // Bug with trailing whitespace fix
            return value.Trim();            
        }


        /// <summary>
        /// Add all paymentsToDate values together for this claim
        /// </summary>
        /// <param name="claimInfo"><see cref="IEnumerable{claimInfo}"/>List of entries with same claim number</param>
        /// <returns><see cref="decimal"/>Sum of all PaymentsToDate</returns>
        private static decimal? PopulatePaymentsToDate(IEnumerable<ClaimData> claimInfo)
        {
            return claimInfo.Where(claimData => claimData.SettledTotal.HasValue).Sum(claimData => claimData.SettledTotal.Value);
        }
    }
}
