﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CoOperativeInsurance.Motor.Model
{
    [IgnoreFirst(1)]
    [IgnoreEmptyLines]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Policyholder_Claimant;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimStatus;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyId;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? NotificationDate;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? IncidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Forename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Surname;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PartyDOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AddressLine1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AddressLine2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AddressLine3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AddressLine4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AddressLine5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HomeTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string WorkTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleReg;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantAge;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimPayments;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimReserve;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RiskIND;

        [FieldOptional]
        [FieldValueDiscarded]
        public String[] Dummy;

    }
}
