﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Impl.Haven.Motor.Model
{
    [DelimitedRecord("|")]
    public class ClaimData
    {
        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Feed_Type;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Row_Type;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Enquiry_External_UID;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Party_External_UID;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Party_Type;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Entity_External_UID;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Entity_Type;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_1;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_2;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_3;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_4;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_5;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_6;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_7;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_8;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_9;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_10;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_11;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_12;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_13;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_14;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_15;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_16;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_17;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_18;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_19;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_20;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_21;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_22;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_23;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_24;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_25;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_26;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_27;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_28;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_29;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_30;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_31;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_32;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_33;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_34;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_35;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_36;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_37;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_38;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_39;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_40;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_41;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_42;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_43;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_44;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_45;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_46;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_47;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_48;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_49;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_50;


        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Notes;

        [FieldOptional]
        [FieldValueDiscarded]
        public String[] Dummy;


    }

}
