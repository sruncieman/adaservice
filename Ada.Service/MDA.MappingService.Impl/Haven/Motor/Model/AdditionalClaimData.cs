﻿using System;
using FileHelpers;


namespace MDA.MappingService.Impl.Haven.Motor.Model
{
    //[IgnoreFirst(1)]
    [DelimitedRecord("|")]
    public class AdditionalClaimData
    {
        [FieldOptional] // 1
        [FieldTrim(TrimMode.Both)]
        public String Enquiry_External_UID;

        [FieldOptional] // 2
        [FieldTrim(TrimMode.Both)]
        [FieldConverter(typeof(CustomDateConvertor))]
        public DateTime? InceptionDate;


        [FieldOptional] // 3
        [FieldTrim(TrimMode.Both)]
        public String IncidentCircumstances;

        [FieldOptional] // 4
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_4;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_5;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_6;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_7;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_8;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_9;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_10;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String Data_Column_11;

        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        public String FaultNonFault;

    }
}
