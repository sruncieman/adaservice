﻿using System;
using System.Globalization;
using FileHelpers;

namespace MDA.MappingService.Impl.Haven.Motor.Model
{
    public class CustomDateConvertor : ConverterBase
    {

        public override object StringToField(string value)
        {
            DateTime dt;

            if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, DateTimeStyles.None, out dt))
                return dt;

            if (DateTime.TryParseExact(value, "dd-MMM-yy", null, DateTimeStyles.None, out dt))
                return dt;

            if (DateTime.TryParseExact(value, "ddMMMyyyy", null, DateTimeStyles.None, out dt))
                return dt;

            //if (DateTime.TryParseExact(value, "ddMMMYYYY:HH:MM:SS", null, DateTimeStyles.None, out dt))
            //    return dt;

            else
            {
                return null;
            }

        }

    }
}
