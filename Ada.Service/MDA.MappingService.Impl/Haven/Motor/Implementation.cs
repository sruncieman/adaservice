﻿using System;
using System.Configuration;
using System.IO;
using MDA.MappingService.Interface;
using MDA.Common;
using MDA.Common.Server;
using System.Linq;
using Ionic.Zip;

namespace MDA.MappingService.Impl.Haven.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private readonly string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];
                
        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking, Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, out ProcessingResults processingResults)
        {
            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            try
            {
                var mapper = new Mapper(fs, clientFolder, ctx, ProcessClaimFn, statusTracking);
                mapper.AssignFiles();
                mapper.InitialiseFileHelperEngines();
                mapper.PopulateFileHelperEngines();
                mapper.RetrieveDistinctClaims();
                mapper.Translate();
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Haven file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = _tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }

        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard, int settleTime, int batchSize, string postOp, string archiveFolder, Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {            
            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            var files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (!files.Any()) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Any(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now))
                return;

            // If we get here we have files to process
            foreach (var file in files)
            {
                // Looking for two files, can't process until both appear - could be 5 hours apart
                // HAVE_MCLM_20161102_001723.TXT and 
                // HAVE_MCLM_20161102_001723_ADDITIONAL.TXT


                var _fileName = Path.GetFileName(file);

                if (_fileName.Contains("_ADDITIONAL.TXT"))
                {
                    // skip if secondary file, looking for main file first
                    continue;
                }

                var _additionalFileName = Path.GetFileNameWithoutExtension(file) + "_ADDITIONAL.TXT";
                var _zipFileName = Path.GetFileNameWithoutExtension(file) + ".ZIP";

                if (File.Exists(folderPath + "\\" + _additionalFileName))
                {                                                            
                    const string mimeType = "zip";
                    const string clientName = "Haven";

                    // Get middle of file name - date
                    var name = _fileName.Substring(10, 8);

                    var batchRef = string.Format("{0}-{1}", clientName , name);
                    
                    // create temp zip

                    using (var zip = new ZipFile())
                    {
                        zip.AddFile(folderPath + "\\" + _fileName, "");
                        zip.AddFile(folderPath + "\\" + _additionalFileName, "");                                                
                        zip.Save(folderPath + "\\" + _zipFileName);
                    }

                    using (var fileStream = File.OpenRead(folderPath + "\\" + _zipFileName))
                    {
                        var stream = new MemoryStream();
                        stream.SetLength(fileStream.Length);
                        fileStream.Read(stream.GetBuffer(), 0, (int)fileStream.Length);

                        #region Process the batch (call the callback function)
                        processClientBatch(ctx, stream, _zipFileName, mimeType, "Motor", batchRef);
                        #endregion
                    }
                    
                #region Process all the options we invented for our postOp functionality

                // We are passed the string we entered into config.  Now we have to code it!
                // In this case we have implemented RENAME, DELETE, ARCHIVE
                // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                switch (postOp.ToUpper())
                {
                    case "RENAME":
                        File.Move(folderPath + "\\" + _fileName, folderPath + "\\" + _fileName + ".old");
                        File.Move(folderPath + "\\" + _additionalFileName, folderPath + "\\" + _additionalFileName + ".old");
                        File.Move(folderPath + "\\" + _zipFileName, folderPath + "\\" + _zipFileName + ".old");
                        break;

                    case "DELETE":
                        File.Delete(folderPath + "\\" + _fileName);
                        File.Delete(folderPath + "\\" + _additionalFileName);
                        File.Delete(folderPath + "\\" + _zipFileName);
                        break;

                    case "ARCHIVE":
                        if (!Directory.Exists(archiveFolder))
                            Directory.CreateDirectory(archiveFolder);   // Create destination if needed

                        string fname = archiveFolder + "\\" + _fileName;  // build dest path
                        string addfname = archiveFolder + "\\" + _additionalFileName;  // build dest path
                        string zipfname = archiveFolder + "\\" + _zipFileName;  // build dest path

                        if (File.Exists(fname))  // Check if it already exists
                        {
                            fname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                        }
                        if (File.Exists(addfname))  // Check if it already exists
                        {
                            addfname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                        }
                        if (File.Exists(zipfname))  // Check if it already exists
                        {
                            zipfname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                        }

                        File.Move(folderPath + "\\" + _fileName, fname);
                        File.Move(folderPath + "\\" + _additionalFileName, addfname);
                        File.Move(folderPath + "\\" + _zipFileName, zipfname);
                        break;
                }
                #endregion
            }
            }

        }
    }
}
