﻿using FileHelpers;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common.Server;
using MDA.MappingService.Impl.Haven.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Ionic.Zip;
using MDA.Common;

namespace MDA.MappingService.Impl.Haven.Motor
{
    public class Mapper
    {
        private Stream _fs;
        private Stream _fs2;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string FilePath { get; set; }
        public string AdditionalFilePath { get; set; }
        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
        public string PolicyFile { get; set; }
        public FileHelperEngine ClaimEngine { get; set; }
        public FileHelperEngine PolicyEngine { get; set; }

        private string _coreClaimDataFile;
        private string _vehicleClaimDataFile;
        private bool _debug;
        private FileHelperEngine _coreClaimDataEngine;
        private FileHelperEngine _additionalClaimDataEngine;
        
        private ClaimData[] _coreClaimData;
        private AdditionalClaimData[] _additionalClaimData; 

        private List<string> _uniqueClaimNumberList;
        private string _claimCode;
        private IEnumerable<ClaimData> _policyItemRows;
        private IEnumerable<ClaimData> _claimItemRows;
        private string _filePath = null;
        private bool _deleteExtractedFiles;
        private int _claimCount = 0;
        private string _clientFolder;
        private StreamReader _claimDataFileStreamReader;
        public List<string> FileNames;

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;
            
            _uniqueClaimNumberList = new List<string>();
            FileNames = new List<string>();
        }


        /// <summary>
        /// Tries to extract files, returns false if bad password
        /// </summary>
        /// <param name="zipFileEntry"><see cref="ZipEntry"/>ZipEntry</param>
        /// <param name="password"><see cref="string"/>string of password (optional)</param>
        /// <param name="unpackDirectory"><see cref="string"/>string path of folder to extract to</param>
        /// <returns>true if successful</returns>
        private static bool TryFileExtractor(ZipEntry zipFileEntry, string password, string unpackDirectory)
        {
            if (zipFileEntry == null) throw new ArgumentNullException("zipFileEntry");

            if (string.IsNullOrEmpty(unpackDirectory))
                throw new ArgumentException("Value cannot be null or empty.", "unpackDirectory");

            try
            {
                if (!string.IsNullOrEmpty(password))
                {
                    zipFileEntry.Password = password;
                }

                zipFileEntry.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

            }
            catch (BadPasswordException)
            {
                return false;
            }
            return true;
        }

        public void AssignFiles()
        {
            FilePath = ClientFolder + FileNames.FirstOrDefault(x => !x.Contains("_ADDITIONAL.TXT"));
            AdditionalFilePath = ClientFolder + FileNames.FirstOrDefault(x => x.Contains("_ADDITIONAL.TXT"));           
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
                _additionalClaimDataEngine = new FileHelperEngine(typeof(AdditionalClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {                
                _coreClaimData = _coreClaimDataEngine.ReadFile(FilePath) as ClaimData[];
                _additionalClaimData = _additionalClaimDataEngine.ReadFile(AdditionalFilePath) as AdditionalClaimData[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers 
                _uniqueClaimNumberList = _coreClaimData.Where(x => x.Enquiry_External_UID != "").Select(x => x.Enquiry_External_UID).Distinct().ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }

        }

        public void Translate()
        {

            var _skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["HavenSkipNum"]);
            var _takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["HavenTakeNum"]);

            if (_skipValue > 0 && _takeValue > 0)
            {
                _uniqueClaimNumberList = _uniqueClaimNumberList.Skip(_skipValue).Take(_takeValue).ToList();
            }            

            foreach (var claim in _uniqueClaimNumberList)
            {

                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.Where(x => x.Enquiry_External_UID == claim).ToList();
                var additionalfileInfo = _additionalClaimData.FirstOrDefault(x => x.Enquiry_External_UID == claim);

                if (uniqueClaim != null)
                {

                    #region Claim

                    var claimInfo = uniqueClaim.Where(x => x.Row_Type == "CLAIM_MOTOR").FirstOrDefault();
                    
                    try
                    {

                        if (!string.IsNullOrEmpty(claimInfo.Enquiry_External_UID))
                        {
                            motorClaim.ClaimNumber = claimInfo.Enquiry_External_UID;
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_6))
                        {
                            DateTime incidentDate = _ConvertDate(claimInfo.Data_Column_6);
                            motorClaim.IncidentDate = incidentDate;
                        }                        

                        #region Claim Status

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_9))
                        {
                            switch (claimInfo.Data_Column_9.ToUpper())
                            {
                                case "OPEN CLAIM":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                    break;
                                case "CLAIM CLOSED":
                                case "RE-CLOSED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                    break;
                                case "CLAIM REOPENED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                                    break;
                                default:
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                                    break;
                            }
                        }

                        #endregion

                        
                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_4))
                        {
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = _ConvertDate(claimInfo.Data_Column_4);
                        }

                        if (additionalfileInfo != null)
                        {
                            if (!string.IsNullOrEmpty(additionalfileInfo.FaultNonFault))
                            {
                                motorClaim.ExtraClaimInfo.ClaimCode = additionalfileInfo.FaultNonFault;
                            }

                            if (!string.IsNullOrEmpty(additionalfileInfo.IncidentCircumstances))
                            {
                                motorClaim.ExtraClaimInfo.IncidentCircumstances = additionalfileInfo.IncidentCircumstances;
                            }
                        }
                        
                        //if (!string.IsNullOrEmpty(claimInfo.Data_Column_24))
                        //{
                        //     motorClaim.ExtraClaimInfo.ClaimCode = claimInfo.Data_Column_24;
                        //}

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_26))
                        {
                            motorClaim.ExtraClaimInfo.IncidentLocation = claimInfo.Data_Column_26;
                        }
                        
                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;                       

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_17))
                        {
                            motorClaim.ExtraClaimInfo.Reserve = Convert.ToDecimal(claimInfo.Data_Column_17);
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_18))
                        {
                            motorClaim.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(claimInfo.Data_Column_18);
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion

                    #region Policy

                    if (!string.IsNullOrEmpty(claimInfo.Data_Column_12))
                    {
                        motorClaim.Policy.PolicyNumber = claimInfo.Data_Column_12;
                    }

                    if (!string.IsNullOrEmpty(claimInfo.Data_Column_1))
                    {
                        switch (claimInfo.Data_Column_1.ToUpper())
                        {
                            case "PRIVATE CAR":
                                motorClaim.Policy.PolicyType_Id = (int)PolicyType.PersonalMotor;
                                break;
                            case "COMMERCIAL VEHICLE":
                            case "FLEET ANNUAL":
                            case "FLEET SHORT TERM":
                            case "MOTOR TRADE":
                            case "TAXI ANNUAL":
                            case "TAXI SHORT TERM":
                                motorClaim.Policy.PolicyType_Id = (int)PolicyType.CommercialMotor;
                                break;
                            default:
                                motorClaim.Policy.PolicyType_Id = (int)PolicyType.Unknown;
                                break;
                        }
                    }

                    //var additionalClaimInfo = uniqueClaim.Where(x => x.Row_Type == "ENQUIRY_ADDITIONAL_STRING").FirstOrDefault();

                    motorClaim.Policy.Insurer = "Prospect Legal Ltd";

                    motorClaim.Policy.InsurerTradingName = "Haven Claims";

                    if (additionalfileInfo != null && additionalfileInfo.InceptionDate.HasValue)
                    {
                        motorClaim.Policy.PolicyStartDate = additionalfileInfo.InceptionDate;
                    }

                    #endregion

                    #region Vehicles

                    var vehicleInfo = uniqueClaim.Where(x => x.Row_Type == "VEHICLE").ToList();

                    foreach (var v in vehicleInfo)
                    {
                        PipelineVehicle vehicle = new PipelineVehicle();

                        try
                        {

                            if (v.Entity_Type.Contains("INV"))
                            {
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                            }

                            if (v.Entity_Type.Contains("TPV"))
                            {
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                            }

                            if (v.Data_Column_1 != null)
                            {
                                vehicle.VehicleRegistration = v.Data_Column_1;
                            }

                            if (v.Data_Column_19 != null)
                            {
                                vehicle.VIN = v.Data_Column_19;
                            }

                            #region Vehicle Type

                            if (!string.IsNullOrEmpty(v.Data_Column_15))
                            {
                                switch (v.Data_Column_15.ToUpper())
                                {
                                    case "2 DOOR SALOON":
                                    case "3 DOOR HATCHBACK":
                                    case "4 DOOR SALOON":
                                    case "5 DOOR HATCHBACK":
                                    case "CONVERTIBLE":
                                    case "COUPE":
                                    case "ESTATE; SALOON":
                                    case "SPORTS":
                                    case "LIGHT 4X4 UTILITY":
                                    case "MPV (MULTI-PURPOSE VEHICLE)":
                                        vehicle.VehicleType_Id = (int)VehicleType.Car;
                                        break;
                                    case "S/D BUS/COACH":
                                        vehicle.VehicleType_Id = (int)VehicleType.Coach;
                                        break;
                                    case "CAR TRANSPORTER":
                                    case "CURTAIN SIDED":
                                        vehicle.VehicleType_Id = (int)VehicleType.Lorry;
                                        break;
                                    case "MINIBUS":
                                        vehicle.VehicleType_Id = (int)VehicleType.Minibus;
                                        break;
                                    case "MOTORCYCLE":
                                        vehicle.VehicleType_Id = (int)VehicleType.Motorcycle;
                                        break;
                                    case "PICK-UP":
                                        vehicle.VehicleType_Id = (int)VehicleType.Pickup;
                                        break;
                                    case "BOX VAN":
                                    case "BREAKDOWN":
                                    case "CAR DERIVED VAN":
                                    case "DROPSIDE":
                                    case "FLAT":
                                    case "FLOAT":
                                    case "INSULATED/REFRIGERATED":
                                    case "LIGHT GOODS":
                                    case "LIGHT VAN":
                                    case "LUTON":
                                    case "PANEL VAN (INTEGRAL)":
                                    case "TIPPER":
                                    case "VAN":
                                    case "VAN/SIDE WINDOWS":
                                    case "SKIP LOADER":
                                        vehicle.VehicleType_Id = (int)VehicleType.Van;
                                        break;
                                    case "TAXI":
                                        vehicle.VehicleType_Id = (int)VehicleType.Taxi;
                                        break;
                                    default:
                                        vehicle.VehicleType_Id = (int)VehicleType.Unknown;
                                        break;
                                }
                            }

                            #endregion

                            if (v.Data_Column_8 != null)
                            {
                                vehicle.VehicleMake = v.Data_Column_8;
                            }

                            if (v.Data_Column_9 != null)
                            {
                                vehicle.VehicleModel = v.Data_Column_9;
                            }

                            if (v.Data_Column_3 != null)
                            {
                                vehicle.EngineCapacity = v.Data_Column_3;
                            }

                            #region Vehicle Colour

                            if (!string.IsNullOrEmpty(v.Data_Column_20))
                            {
                                switch (v.Data_Column_20.ToUpper())
                                {
                                    case "ALUMINIUM/SILVER":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Silver;
                                        break;
                                    case "ALUMINIUM/SILVER/BLACK":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Silver;
                                        break;
                                    case "ALUMINIUM/SILVER/GREEN":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Silver;
                                        break;
                                    case "ALUMINIUM/SILVER/GREY":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Silver;
                                        break;
                                    case "BEIGE/BUFF":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Beige;
                                        break;
                                    case "BLACK":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Black;
                                        break;
                                    case "BLACK/BLUE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Black;
                                        break;
                                    case "BLACK/WHITE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Black;
                                        break;
                                    case "BLACK/YELLOW":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Black;
                                        break;
                                    case "BLUE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Blue;
                                        break;
                                    case "BLUE/ALUMINIUM/SILVER":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Blue;
                                        break;
                                    case "BLUE/BLACK":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Blue;
                                        break;
                                    case "BLUE/GREY":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Blue;
                                        break;
                                    case "BRONZE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Bronze;
                                        break;
                                    case "BROWN":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Brown;
                                        break;
                                    case "CREAM/IVORY":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Cream;
                                        break;
                                    case "GOLD":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Gold;
                                        break;
                                    case "GOLD/WHITE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Gold;
                                        break;
                                    case "GREEN":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Green;
                                        break;
                                    case "GREEN/ALUMINIUM/SILVER":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Green;
                                        break;
                                    case "GREEN/BLUE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Green;
                                        break;
                                    case "GREY":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Grey;
                                        break;
                                    case "GREY/BLACK":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Grey;
                                        break;
                                    case "GREY/BLUE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Grey;
                                        break;
                                    case "MAROON":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Maroon;
                                        break;
                                    case "MAUVE/PURPLE/VIOLET":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Mauve;
                                        break;
                                    case "MULTI-COLOURED (3+)":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Unknown;
                                        break;
                                    case "ORANGE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Orange;
                                        break;
                                    case "ORANGE/GREY":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Orange;
                                        break;
                                    case "ORANGE/WHITE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Orange;
                                        break;
                                    case "PINK":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Pink;
                                        break;
                                    case "RED":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Red;
                                        break;
                                    case "RED/ALUMINIUM/SILVER":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Red;
                                        break;
                                    case "SILVER":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Silver;
                                        break;
                                    case "TURQUOISE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Turquoise;
                                        break;
                                    case "WHITE":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.White;
                                        break;
                                    case "WHITE/BLACK":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.White;
                                        break;
                                    case "WHITE/GREEN":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.White;
                                        break;
                                    case "WHITE/GREY":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.White;
                                        break;
                                    case "WHITE/MAUVE/PURPLE/VIOLET":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.White;
                                        break;
                                    case "YELLOW":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Yellow;
                                        break;
                                    case "YELLOW/GREEN":
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Yellow;
                                        break;
                                    default:
                                        vehicle.VehicleColour_Id = (int)VehicleColour.Unknown;
                                        break;
                                }
                            }

                            #endregion

                            if (v.Data_Column_22 != null)
                            {
                                vehicle.DamageDescription = v.Data_Column_22;
                            }

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping vehicle information: " + ex);
                        }

                        #region Insured People

                        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                        {

                            var insuredpersonInfo = (from pm in uniqueClaim
                                                     where pm.Row_Type == "PARTY_MOTOR"
                                                     && (pm.Party_Type.Contains("CLM") || pm.Party_Type.Contains("NDV") || pm.Party_Type.Contains("OWN"))
                                                         //&& assetRelationshipInfo.Contains(pm.Party_Type)
                                                     && pm.Data_Column_20 == ""
                                                     select pm).ToList();

                            foreach (var p in insuredpersonInfo)
                            {

                                PipelinePerson person = new PipelinePerson();

                                try
                                {

                                    var iPoccupationList = (from o in uniqueClaim
                                                            where o.Row_Type == "EMPLOYMENT"
                                                            && o.Party_Type == p.Party_Type
                                                            select o.Data_Column_4).ToList();

                                    if (iPoccupationList != null)
                                    {
                                        foreach (var occupation in iPoccupationList)
                                        {
                                            person.Occupation += occupation + " ";
                                        }

                                    }

                                    if (additionalfileInfo != null)
                                    {
                                        person.I2Pe_LinkData.ClaimInfo.IncidentCircumstances = additionalfileInfo.IncidentCircumstances;

                                        person.I2Pe_LinkData.ClaimInfo.ClaimCode = additionalfileInfo.FaultNonFault;
                                    }

                                    #region PartyTypes

                                    if (p.Party_Type.Contains("CLM"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                        
                                    }
                                    else
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    #endregion

                                    #region Saltutation

                                    if (!string.IsNullOrEmpty(p.Data_Column_1))
                                    {
                                        switch (p.Data_Column_1.ToUpper())
                                        {
                                            case "MR":
                                                person.Salutation_Id = (int)Salutation.Mr;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "MRS":
                                                person.Salutation_Id = (int)Salutation.Mrs;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MS":
                                                person.Salutation_Id = (int)Salutation.Ms;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MISS":
                                                person.Salutation_Id = (int)Salutation.Miss;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MASTER":
                                                person.Salutation_Id = (int)Salutation.Master;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "DR":
                                                person.Salutation_Id = (int)Salutation.Dr;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                            case "REV":
                                                person.Salutation_Id = (int)Salutation.Reverend;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                            default:
                                                person.Salutation_Id = (int)Salutation.Unknown;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                        }
                                    }

                                    #endregion

                                    #region Name

                                    // List of Salutations
                                    List<string> salutations = new List<string>();
                                    salutations.Add("MR");
                                    salutations.Add("MS");
                                    salutations.Add("MISS");
                                    salutations.Add("MRS");
                                    salutations.Add("MASTER");
                                    salutations.Add("DR");

                                    var personName = p.Data_Column_5.Split(); // Surname may contain fullname

                                    if (personName.Count() == 7)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2] + " " + personName[3] + " " + personName[4] + " " + personName[5];
                                            person.LastName = personName[6];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3] + " " + personName[4] + " " + personName[5];
                                            person.LastName = personName[6];
                                        }
                                    }
                                    else if (personName.Count() == 6)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2] + " " + personName[3] + " " + personName[4];
                                            person.LastName = personName[5];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3] + " " + personName[4];
                                            person.LastName = personName[5];
                                        }
                                    }
                                    else if (personName.Count() == 5)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2] + " " + personName[3];
                                            person.LastName = personName[4];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3];
                                            person.LastName = personName[4];
                                        }
                                    }
                                    else if (personName.Count() == 4)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2];
                                            person.LastName = personName[3];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2];
                                            person.LastName = personName[3];
                                        }
                                    }
                                    else if (personName.Count() == 3)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.LastName = personName[2];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1];
                                            person.LastName = personName[2];
                                        }
                                    }
                                    else if (personName.Count() == 2)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.LastName = personName[1];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.LastName = personName[1];
                                        }
                                    }
                                    else if (personName.Count() == 1)
                                    {
                                        if (!string.IsNullOrEmpty(p.Data_Column_3))
                                        {
                                            person.FirstName = p.Data_Column_3;
                                        }

                                        person.LastName = personName[0];
                                    }
                                    else
                                    {
                                        person.LastName = p.Data_Column_5;

                                        if (!string.IsNullOrEmpty(p.Data_Column_3))
                                        {
                                            person.FirstName = p.Data_Column_3;
                                        }
                                    }

                                    #endregion

                                    if (!string.IsNullOrEmpty(p.Data_Column_6))
                                    {
                                        DateTime dateOfBirth = _ConvertDate(p.Data_Column_6);
                                        person.DateOfBirth = dateOfBirth;
                                    }

                                    if (!string.IsNullOrEmpty(p.Data_Column_4))
                                    {
                                        person.Occupation = p.Data_Column_4;
                                    }

                                    #region Gender

                                    if (!string.IsNullOrEmpty(p.Data_Column_9))
                                    {
                                        switch (p.Data_Column_9.ToUpper())
                                        {
                                            case "M":
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "F":
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                        }
                                    }

                                    #endregion

                                    #region Address

                                    var addresses = (from a in uniqueClaim
                                                     where a.Row_Type == "ADDRESS"
                                                     && a.Party_Type == p.Party_Type
                                                     select a).ToList();

                                    foreach (var a in addresses)
                                    {
                                        PipelineAddress address = new PipelineAddress();

                                        address.SubBuilding = a.Data_Column_1;
                                        address.Building = a.Data_Column_2;
                                        address.BuildingNumber = a.Data_Column_3;
                                        address.Street = a.Data_Column_4;
                                        address.Town = a.Data_Column_6;
                                        address.PostCode = a.Data_Column_8;

                                        if (a.Entity_Type == "CORR")
                                        {
                                            address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                        }

                                        person.Addresses.Add(address);

                                    }

                                    #endregion

                                    #region Contact Details

                                    var partyContactInfo = (from pci in uniqueClaim
                                                            where pci.Row_Type == "CONTACT"
                                                            && pci.Party_Type == p.Party_Type
                                                            select pci).ToList();

                                    foreach (var pci in partyContactInfo)
                                    {

                                        if (pci.Entity_Type == "GENTEL")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);

                                        }
                                        else if (pci.Entity_Type == "MOBILE")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);
                                        }
                                        else if (pci.Entity_Type == "GENEML")
                                        {
                                            PipelineEmail email = new PipelineEmail();
                                            email.EmailAddress = pci.Data_Column_1;
                                            person.EmailAddresses.Add(email);
                                        }

                                    }

                                    #endregion

                                    #region Organisations

                                    var partyRelationshipInfo = (from a in uniqueClaim
                                                                 where a.Row_Type == "PARTY_RELATIONSHIP"
                                                                 && a.Party_Type == p.Party_Type
                                                                 select a.Data_Column_1).ToList();

                                    var organisations = (from pm in uniqueClaim
                                                         where pm.Row_Type == "PARTY_MOTOR"
                                                         && partyRelationshipInfo.Contains(pm.Party_Type)
                                                         && !pm.Party_Type.Contains("TPI")
                                                         select pm).ToList();

                                    foreach (var org in organisations)
                                    {
                                        PipelineOrganisation organisation = new PipelineOrganisation();

                                        try
                                        {

                                            #region Party Type

                                            if (!string.IsNullOrEmpty(org.Party_Type))
                                            {
                                                string partyType = org.Party_Type.Substring(0, 3);

                                                switch (partyType.ToUpper())
                                                {
                                                    case "CLM":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                                        organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                                        break;
                                                    case "TPS":
                                                    case "SOL":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                                        break;
                                                    case "TPI":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Insurer;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                                        break;
                                                    default:
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                                        break;
                                                }
                                            }

                                            #endregion

                                            if (org.Data_Column_20 != null)
                                            {
                                                organisation.OrganisationName = org.Data_Column_20;
                                            }

                                            #region Address

                                            var orgAddresses = (from a in uniqueClaim
                                                                where a.Row_Type == "ADDRESS"
                                                                && a.Party_Type == org.Party_Type
                                                                select a).ToList();

                                            foreach (var a in orgAddresses)
                                            {
                                                bool ignoreAddressFlag = false;
                                                PipelineAddress address = new PipelineAddress();

                                                address.SubBuilding = a.Data_Column_1;
                                                address.Building = a.Data_Column_2;
                                                address.BuildingNumber = a.Data_Column_3;
                                                address.Street = a.Data_Column_4;
                                                address.Town = a.Data_Column_6;
                                                address.PostCode = a.Data_Column_8;

                                                if (a.Entity_Type == "CORR")
                                                {
                                                    address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                                                }


                                                if (address.PostCode.ToUpper().Contains("IP1 3HS") && (address.Building.ToUpper().Contains("CROWN HOUSE") && address.Building.ToUpper().Contains("CROWN STREET")))
                                                    ignoreAddressFlag = true;

                                                if (address.PostCode.ToUpper().Contains("SS14 1NT") && address.Building.ToUpper().Contains("KELTING HOUSE"))
                                                    ignoreAddressFlag = true;

                                                if (address.BuildingNumber == "37" && address.PostCode.ToUpper().Contains("AL4 0UN") && address.Street.ToUpper().Contains("CENTRAL DRIVE"))
                                                {
                                                    ignoreAddressFlag = true;

                                                }

                                                if (!ignoreAddressFlag)
                                                    organisation.Addresses.Add(address);


                                            }

                                            #endregion

                                            #region Contact Details

                                            var orgPartyContactInfo = (from pci in uniqueClaim
                                                                       where pci.Row_Type == "CONTACT"
                                                                       && pci.Party_Type == org.Party_Type
                                                                       select pci).ToList();

                                            foreach (var pci in orgPartyContactInfo)
                                            {

                                                if (pci.Entity_Type == "GENTEL")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);

                                                }
                                                else if (pci.Entity_Type == "MOBILE")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);
                                                }
                                                else if (pci.Entity_Type == "GENEML")
                                                {
                                                    PipelineEmail email = new PipelineEmail();
                                                    email.EmailAddress = pci.Data_Column_1;
                                                    organisation.EmailAddresses.Add(email);
                                                }

                                            }

                                            #endregion

                                        }
                                        catch (Exception ex)
                                        {
                                            throw new CustomException("Error in mapping organisation information: " + ex);
                                        }

                                        person.Organisations.Add(organisation);

                                    }

                                    #endregion

                                    vehicle.People.Add(person);

                                }
                                catch (Exception ex)
                                {
                                    throw new CustomException("Error in mapping person information: " + ex);
                                }

                            }

                        }

                        #endregion

                        #region Third Party Person 1 (THP0001 should be linked to the TPV0001)

                        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.ThirdPartyVehicle && v.Entity_Type == "TPV0001")
                        {

                            var personInfo = (from pm in uniqueClaim
                                              where pm.Row_Type == "PARTY_MOTOR"
                                              && pm.Party_Type.Contains("THP0001")
                                              && pm.Data_Column_20 == ""
                                              select pm).ToList();

                            foreach (var p in personInfo)
                            {

                                PipelinePerson person = new PipelinePerson();

                                try
                                {

                                    var tp1OccupationList = (from o in uniqueClaim
                                                             where o.Row_Type == "EMPLOYMENT"
                                                             && o.Party_Type == p.Party_Type
                                                             select o.Data_Column_4).ToList();

                                    if (tp1OccupationList != null)
                                    {
                                        foreach (var occupation in tp1OccupationList)
                                        {
                                            person.Occupation += occupation + " ";
                                        }

                                    }

                                    #region PartyTypes

                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    //person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                    #endregion

                                    #region Saltutation

                                    if (!string.IsNullOrEmpty(p.Data_Column_1))
                                    {
                                        switch (p.Data_Column_1.ToUpper())
                                        {
                                            case "MR":
                                                person.Salutation_Id = (int)Salutation.Mr;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "MRS":
                                                person.Salutation_Id = (int)Salutation.Mrs;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MS":
                                                person.Salutation_Id = (int)Salutation.Ms;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MISS":
                                                person.Salutation_Id = (int)Salutation.Miss;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MASTER":
                                                person.Salutation_Id = (int)Salutation.Master;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "DR":
                                                person.Salutation_Id = (int)Salutation.Dr;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                            case "REV":
                                                person.Salutation_Id = (int)Salutation.Reverend;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                            default:
                                                person.Salutation_Id = (int)Salutation.Unknown;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                        }
                                    }

                                    #endregion

                                    #region Name

                                    // List of Salutations
                                    List<string> salutations = new List<string>();
                                    salutations.Add("MR");
                                    salutations.Add("MS");
                                    salutations.Add("MISS");
                                    salutations.Add("MRS");
                                    salutations.Add("MASTER");
                                    salutations.Add("DR");

                                    var personName = p.Data_Column_5.Split(); // Surname may contain fullname

                                    if (personName.Count() == 7)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2] + " " + personName[3] + " " + personName[4] + " " + personName[5];
                                            person.LastName = personName[6];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3] + " " + personName[4] + " " + personName[5];
                                            person.LastName = personName[6];
                                        }
                                    }
                                    else if (personName.Count() == 6)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2] + " " + personName[3] + " " + personName[4];
                                            person.LastName = personName[5];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3] + " " + personName[4];
                                            person.LastName = personName[5];
                                        }
                                    }
                                    else if (personName.Count() == 5)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2] + " " + personName[3];
                                            person.LastName = personName[4];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3];
                                            person.LastName = personName[4];
                                        }
                                    }
                                    else if (personName.Count() == 4)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2];
                                            person.LastName = personName[3];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2];
                                            person.LastName = personName[3];
                                        }
                                    }
                                    else if (personName.Count() == 3)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.LastName = personName[2];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1];
                                            person.LastName = personName[2];
                                        }
                                    }
                                    else if (personName.Count() == 2)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.LastName = personName[1];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.LastName = personName[1];
                                        }
                                    }
                                    else if (personName.Count() == 1)
                                    {
                                        if (!string.IsNullOrEmpty(p.Data_Column_3))
                                        {
                                            person.FirstName = p.Data_Column_3;
                                        }

                                        person.LastName = personName[0];
                                    }
                                    else
                                    {
                                        person.LastName = p.Data_Column_5;

                                        if (!string.IsNullOrEmpty(p.Data_Column_3))
                                        {
                                            person.FirstName = p.Data_Column_3;
                                        }
                                    }

                                    #endregion

                                    if (!string.IsNullOrEmpty(p.Data_Column_6))
                                    {
                                        DateTime dateOfBirth = _ConvertDate(p.Data_Column_6);
                                        person.DateOfBirth = dateOfBirth;
                                    }

                                    if (!string.IsNullOrEmpty(p.Data_Column_4))
                                    {
                                        person.Occupation = p.Data_Column_4;
                                    }

                                    #region Gender

                                    if (!string.IsNullOrEmpty(p.Data_Column_9))
                                    {
                                        switch (p.Data_Column_9.ToUpper())
                                        {
                                            case "M":
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "F":
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                        }
                                    }

                                    #endregion

                                    #region Address

                                    var addresses = (from a in uniqueClaim
                                                     where a.Row_Type == "ADDRESS"
                                                     && a.Party_Type == p.Party_Type
                                                     select a).ToList();

                                    foreach (var a in addresses)
                                    {
                                        PipelineAddress address = new PipelineAddress();

                                        address.SubBuilding = a.Data_Column_1;
                                        address.Building = a.Data_Column_2;
                                        address.BuildingNumber = a.Data_Column_3;
                                        address.Street = a.Data_Column_4;
                                        address.Town = a.Data_Column_6;
                                        address.PostCode = a.Data_Column_8;

                                        if (a.Entity_Type == "CORR")
                                        {
                                            address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                        }

                                        person.Addresses.Add(address);

                                    }

                                    #endregion

                                    #region Contact Details

                                    var partyContactInfo = (from pci in uniqueClaim
                                                            where pci.Row_Type == "CONTACT"
                                                            && pci.Party_Type == p.Party_Type
                                                            select pci).ToList();

                                    foreach (var pci in partyContactInfo)
                                    {

                                        if (pci.Entity_Type == "GENTEL")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);

                                        }
                                        else if (pci.Entity_Type == "MOBILE")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);
                                        }
                                        else if (pci.Entity_Type == "GENEML")
                                        {
                                            PipelineEmail email = new PipelineEmail();
                                            email.EmailAddress = pci.Data_Column_1;
                                            person.EmailAddresses.Add(email);
                                        }

                                    }

                                    #endregion

                                    #region Organisations

                                    var partyRelationshipInfo = (from a in uniqueClaim
                                                                 where a.Row_Type == "PARTY_RELATIONSHIP"
                                                                 && a.Party_Type == p.Party_Type
                                                                 select a.Data_Column_1).ToList();

                                    var organisations = (from pm in uniqueClaim
                                                         where pm.Row_Type == "PARTY_MOTOR"
                                                         && partyRelationshipInfo.Contains(pm.Party_Type)
                                                         && !pm.Party_Type.Contains("TPI")
                                                         select pm).ToList();

                                    foreach (var org in organisations)
                                    {
                                        PipelineOrganisation organisation = new PipelineOrganisation();

                                        try
                                        {

                                            #region Party Type

                                            if (!string.IsNullOrEmpty(org.Party_Type))
                                            {
                                                string partyType = org.Party_Type.Substring(0, 3);

                                                switch (partyType.ToUpper())
                                                {
                                                    case "CLM":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                                        organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                                        break;
                                                    case "TPS":
                                                    case "SOL":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                                        break;
                                                    case "TPI":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Insurer;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                                        break;
                                                    default:
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                                        break;
                                                }
                                            }

                                            #endregion

                                            if (org.Data_Column_20 != null)
                                            {
                                                organisation.OrganisationName = org.Data_Column_20;
                                            }

                                            #region Address

                                            var orgAddresses = (from a in uniqueClaim
                                                                where a.Row_Type == "ADDRESS"
                                                                && a.Party_Type == org.Party_Type
                                                                select a).ToList();

                                            foreach (var a in orgAddresses)
                                            {
                                                bool ignoreAddressFlag = false;
                                                PipelineAddress address = new PipelineAddress();

                                                address.SubBuilding = a.Data_Column_1;
                                                address.Building = a.Data_Column_2;
                                                address.BuildingNumber = a.Data_Column_3;
                                                address.Street = a.Data_Column_4;
                                                address.Town = a.Data_Column_6;
                                                address.PostCode = a.Data_Column_8;

                                                if (a.Entity_Type == "CORR")
                                                {
                                                    address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                                                }


                                                if (address.PostCode.ToUpper().Contains("IP1 3HS") && (address.Building.ToUpper().Contains("CROWN HOUSE") && address.Building.ToUpper().Contains("CROWN STREET")))
                                                    ignoreAddressFlag = true;

                                                if (address.PostCode.ToUpper().Contains("SS14 1NT") && address.Building.ToUpper().Contains("KELTING HOUSE"))
                                                    ignoreAddressFlag = true;

                                                if (address.BuildingNumber == "37" && address.PostCode.ToUpper().Contains("AL4 0UN") && address.Street.ToUpper().Contains("CENTRAL DRIVE"))
                                                {
                                                    ignoreAddressFlag = true;

                                                }

                                                if (!ignoreAddressFlag)
                                                    organisation.Addresses.Add(address);


                                            }

                                            #endregion

                                            #region Contact Details

                                            var orgPartyContactInfo = (from pci in uniqueClaim
                                                                       where pci.Row_Type == "CONTACT"
                                                                       && pci.Party_Type == org.Party_Type
                                                                       select pci).ToList();

                                            foreach (var pci in orgPartyContactInfo)
                                            {

                                                if (pci.Entity_Type == "GENTEL")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);

                                                }
                                                else if (pci.Entity_Type == "MOBILE")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);
                                                }
                                                else if (pci.Entity_Type == "GENEML")
                                                {
                                                    PipelineEmail email = new PipelineEmail();
                                                    email.EmailAddress = pci.Data_Column_1;
                                                    organisation.EmailAddresses.Add(email);
                                                }

                                            }

                                            #endregion

                                        }
                                        catch (Exception ex)
                                        {
                                            throw new CustomException("Error in mapping organisation information: " + ex);
                                        }

                                        person.Organisations.Add(organisation);

                                    }

                                    #endregion

                                    vehicle.People.Add(person);

                                }
                                catch (Exception ex)
                                {
                                    throw new CustomException("Error in mapping person information: " + ex);
                                }

                            }

                        }

                        #endregion

                        #region Third Party People

                        var assetRelationshipInfo = (from a in uniqueClaim
                                                     where a.Row_Type == "ASSET_RELATIONSHIP"
                                                     && a.Data_Column_1 == v.Entity_Type
                                                     select a.Party_Type).ToList();


                        var tpPersonInfo = (from pm in uniqueClaim
                                            where pm.Row_Type == "PARTY_MOTOR"
                                            && assetRelationshipInfo.Contains(pm.Party_Type)
                                            && pm.Data_Column_20 == ""
                                            select pm).ToList();

                        foreach (var p in tpPersonInfo)
                        {

                            PipelinePerson person = new PipelinePerson();

                            try
                            {

                                var tPoccupationList = (from o in uniqueClaim
                                                        where o.Row_Type == "EMPLOYMENT"
                                                        && o.Party_Type == p.Party_Type
                                                        select o.Data_Column_4).ToList();

                                if (tPoccupationList != null)
                                {
                                    foreach (var occupation in tPoccupationList)
                                    {
                                        person.Occupation += occupation + " ";
                                    }

                                }

                                #region PartyTypes

                                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                //person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                #endregion

                                #region Saltutation

                                if (!string.IsNullOrEmpty(p.Data_Column_1))
                                {
                                    switch (p.Data_Column_1.ToUpper())
                                    {
                                        case "MR":
                                            person.Salutation_Id = (int)Salutation.Mr;
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "MRS":
                                            person.Salutation_Id = (int)Salutation.Mrs;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        case "MS":
                                            person.Salutation_Id = (int)Salutation.Ms;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        case "MISS":
                                            person.Salutation_Id = (int)Salutation.Miss;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        case "MASTER":
                                            person.Salutation_Id = (int)Salutation.Master;
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "DR":
                                            person.Salutation_Id = (int)Salutation.Dr;
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;
                                        case "REV":
                                            person.Salutation_Id = (int)Salutation.Reverend;
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;
                                        default:
                                            person.Salutation_Id = (int)Salutation.Unknown;
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;
                                    }
                                }

                                #endregion

                                #region Name

                                // List of Salutations
                                List<string> salutations = new List<string>();
                                salutations.Add("MR");
                                salutations.Add("MS");
                                salutations.Add("MISS");
                                salutations.Add("MRS");
                                salutations.Add("MASTER");
                                salutations.Add("DR");

                                var personName = p.Data_Column_5.Split(); // Surname may contain fullname

                                if (personName.Count() == 7)
                                {
                                    if (salutations.Contains(personName[0].ToUpper()))
                                    {
                                        person.FirstName = personName[1];
                                        person.MiddleName = personName[2] + " " + personName[3] + " " + personName[4] + " " + personName[5];
                                        person.LastName = personName[6];
                                    }
                                    else
                                    {
                                        person.FirstName = personName[0];
                                        person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3] + " " + personName[4] + " " + personName[5];
                                        person.LastName = personName[6];
                                    }
                                }
                                else if (personName.Count() == 6)
                                {
                                    if (salutations.Contains(personName[0].ToUpper()))
                                    {
                                        person.FirstName = personName[1];
                                        person.MiddleName = personName[2] + " " + personName[3] + " " + personName[4];
                                        person.LastName = personName[5];
                                    }
                                    else
                                    {
                                        person.FirstName = personName[0];
                                        person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3] + " " + personName[4];
                                        person.LastName = personName[5];
                                    }
                                }
                                else if (personName.Count() == 5)
                                {
                                    if (salutations.Contains(personName[0].ToUpper()))
                                    {
                                        person.FirstName = personName[1];
                                        person.MiddleName = personName[2] + " " + personName[3];
                                        person.LastName = personName[4];
                                    }
                                    else
                                    {
                                        person.FirstName = personName[0];
                                        person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3];
                                        person.LastName = personName[4];
                                    }
                                }
                                else if (personName.Count() == 4)
                                {
                                    if (salutations.Contains(personName[0].ToUpper()))
                                    {
                                        person.FirstName = personName[1];
                                        person.MiddleName = personName[2];
                                        person.LastName = personName[3];
                                    }
                                    else
                                    {
                                        person.FirstName = personName[0];
                                        person.MiddleName = personName[1] + " " + personName[2];
                                        person.LastName = personName[3];
                                    }
                                }
                                else if (personName.Count() == 3)
                                {
                                    if (salutations.Contains(personName[0].ToUpper()))
                                    {
                                        person.FirstName = personName[1];
                                        person.LastName = personName[2];
                                    }
                                    else
                                    {
                                        person.FirstName = personName[0];
                                        person.MiddleName = personName[1];
                                        person.LastName = personName[2];
                                    }
                                }
                                else if (personName.Count() == 2)
                                {
                                    if (salutations.Contains(personName[0].ToUpper()))
                                    {
                                        person.LastName = personName[1];
                                    }
                                    else
                                    {
                                        person.FirstName = personName[0];
                                        person.LastName = personName[1];
                                    }
                                }
                                else if (personName.Count() == 1)
                                {
                                    if (!string.IsNullOrEmpty(p.Data_Column_3))
                                    {
                                        person.FirstName = p.Data_Column_3;
                                    }

                                    person.LastName = personName[0];
                                }
                                else
                                {
                                    person.LastName = p.Data_Column_5;

                                    if (!string.IsNullOrEmpty(p.Data_Column_3))
                                    {
                                        person.FirstName = p.Data_Column_3;
                                    }
                                }

                                #endregion

                                if (!string.IsNullOrEmpty(p.Data_Column_6))
                                {
                                    DateTime dateOfBirth = _ConvertDate(p.Data_Column_6);
                                    person.DateOfBirth = dateOfBirth;
                                }

                                if (!string.IsNullOrEmpty(p.Data_Column_4))
                                {
                                    person.Occupation = p.Data_Column_4;
                                }

                                #region Gender

                                if (!string.IsNullOrEmpty(p.Data_Column_9))
                                {
                                    switch (p.Data_Column_9.ToUpper())
                                    {
                                        case "M":
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "F":
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                    }
                                }

                                #endregion

                                #region Address

                                var addresses = (from a in uniqueClaim
                                                 where a.Row_Type == "ADDRESS"
                                                 && a.Party_Type == p.Party_Type
                                                 select a).ToList();

                                foreach (var a in addresses)
                                {
                                    PipelineAddress address = new PipelineAddress();

                                    address.SubBuilding = a.Data_Column_1;
                                    address.Building = a.Data_Column_2;
                                    address.BuildingNumber = a.Data_Column_3;
                                    address.Street = a.Data_Column_4;
                                    address.Town = a.Data_Column_6;
                                    address.PostCode = a.Data_Column_8;

                                    if (a.Entity_Type == "CORR")
                                    {
                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                    }

                                    person.Addresses.Add(address);

                                }

                                #endregion

                                #region Contact Details

                                var partyContactInfo = (from pci in uniqueClaim
                                                        where pci.Row_Type == "CONTACT"
                                                        && pci.Party_Type == p.Party_Type
                                                        select pci).ToList();

                                foreach (var pci in partyContactInfo)
                                {

                                    if (pci.Entity_Type == "GENTEL")
                                    {
                                        PipelineTelephone telephone = new PipelineTelephone();
                                        telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                        person.Telephones.Add(telephone);

                                    }
                                    else if (pci.Entity_Type == "MOBILE")
                                    {
                                        PipelineTelephone telephone = new PipelineTelephone();
                                        telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                        person.Telephones.Add(telephone);
                                    }
                                    else if (pci.Entity_Type == "GENEML")
                                    {
                                        PipelineEmail email = new PipelineEmail();
                                        email.EmailAddress = pci.Data_Column_1;
                                        person.EmailAddresses.Add(email);
                                    }

                                }

                                #endregion

                                #region Organisations

                                var partyRelationshipInfo = (from a in uniqueClaim
                                                             where a.Row_Type == "PARTY_RELATIONSHIP"
                                                             && a.Party_Type == p.Party_Type
                                                             select a.Data_Column_1).ToList();

                                var organisations = (from pm in uniqueClaim
                                                     where pm.Row_Type == "PARTY_MOTOR"
                                                     && partyRelationshipInfo.Contains(pm.Party_Type)
                                                     && !pm.Party_Type.Contains("TPI")
                                                     select pm).ToList();

                                foreach (var org in organisations)
                                {
                                    PipelineOrganisation organisation = new PipelineOrganisation();

                                    try
                                    {

                                        #region Party Type

                                        if (!string.IsNullOrEmpty(org.Party_Type))
                                        {
                                            string partyType = org.Party_Type.Substring(0, 3);

                                            switch (partyType.ToUpper())
                                            {
                                                case "CLM":
                                                    organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                                    organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                                    break;
                                                case "TPS":
                                                case "SOL":
                                                    organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                                    break;
                                                case "TPI":
                                                    organisation.OrganisationType_Id = (int)OrganisationType.Insurer;
                                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                                    break;
                                                default:
                                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                                    break;
                                            }
                                        }

                                        #endregion

                                        if (org.Data_Column_20 != null)
                                        {
                                            organisation.OrganisationName = org.Data_Column_20;
                                        }

                                        #region Address

                                        var orgAddresses = (from a in uniqueClaim
                                                            where a.Row_Type == "ADDRESS"
                                                            && a.Party_Type == org.Party_Type
                                                            select a).ToList();

                                        foreach (var a in orgAddresses)
                                        {
                                            bool ignoreAddressFlag = false;
                                            PipelineAddress address = new PipelineAddress();

                                            address.SubBuilding = a.Data_Column_1;
                                            address.Building = a.Data_Column_2;
                                            address.BuildingNumber = a.Data_Column_3;
                                            address.Street = a.Data_Column_4;
                                            address.Town = a.Data_Column_6;
                                            address.PostCode = a.Data_Column_8;

                                            if (a.Entity_Type == "CORR")
                                            {
                                                address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                                            }


                                            if (address.PostCode.ToUpper().Contains("IP1 3HS") && (address.Building.ToUpper().Contains("CROWN HOUSE") && address.Building.ToUpper().Contains("CROWN STREET")))
                                                ignoreAddressFlag = true;

                                            if (address.PostCode.ToUpper().Contains("SS14 1NT") && address.Building.ToUpper().Contains("KELTING HOUSE"))
                                                ignoreAddressFlag = true;

                                            if (address.BuildingNumber == "37" && address.PostCode.ToUpper().Contains("AL4 0UN") && address.Street.ToUpper().Contains("CENTRAL DRIVE"))
                                            {
                                                ignoreAddressFlag = true;

                                            }

                                            if (!ignoreAddressFlag)
                                                organisation.Addresses.Add(address);


                                        }

                                        #endregion

                                        #region Contact Details

                                        var orgPartyContactInfo = (from pci in uniqueClaim
                                                                   where pci.Row_Type == "CONTACT"
                                                                   && pci.Party_Type == org.Party_Type
                                                                   select pci).ToList();

                                        foreach (var pci in orgPartyContactInfo)
                                        {

                                            if (pci.Entity_Type == "GENTEL")
                                            {
                                                PipelineTelephone telephone = new PipelineTelephone();
                                                telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                                telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                organisation.Telephones.Add(telephone);

                                            }
                                            else if (pci.Entity_Type == "MOBILE")
                                            {
                                                PipelineTelephone telephone = new PipelineTelephone();
                                                telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                                telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                organisation.Telephones.Add(telephone);
                                            }
                                            else if (pci.Entity_Type == "GENEML")
                                            {
                                                PipelineEmail email = new PipelineEmail();
                                                email.EmailAddress = pci.Data_Column_1;
                                                organisation.EmailAddresses.Add(email);
                                            }

                                        }

                                        #endregion

                                    }
                                    catch (Exception ex)
                                    {
                                        throw new CustomException("Error in mapping organisation information: " + ex);
                                    }

                                    person.Organisations.Add(organisation);

                                }

                                #endregion

                                vehicle.People.Add(person);

                            }
                            catch (Exception ex)
                            {
                                throw new CustomException("Error in mapping person information: " + ex);
                            }

                        }

                        #endregion

                        motorClaim.Vehicles.Add(vehicle);
                    }

                    #endregion

                    #region Organisations


                    var allPartyRelationshipInfo = (from a in uniqueClaim
                                                    where a.Row_Type == "PARTY_RELATIONSHIP"
                                                    select a.Data_Column_1).ToList();


                    var claimOrganisations = (from p in uniqueClaim
                                              where p.Row_Type == "PARTY_MOTOR"
                                              && !allPartyRelationshipInfo.Contains(p.Party_Type)
                                              && !p.Party_Type.Contains("TPI")
                                              && p.Data_Column_20 != ""
                                              select p).ToList();


                    foreach (var org in claimOrganisations)
                    {
                        PipelineOrganisation organisation = new PipelineOrganisation();

                        try
                        {

                            #region Party Type

                            if (!string.IsNullOrEmpty(org.Party_Type))
                            {
                                string partyType = org.Party_Type.Substring(0, 3);

                                switch (partyType.ToUpper())
                                {
                                    case "CLM":
                                        organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                        organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                        break;
                                    case "TPS":
                                    case "SOL":
                                        organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                        break;
                                    case "TPI":
                                        organisation.OrganisationType_Id = (int)OrganisationType.Insurer;
                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                        break;
                                    default:
                                        organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                        break;
                                }
                            }

                            #endregion

                            if (org.Data_Column_20 != null)
                            {
                                organisation.OrganisationName = org.Data_Column_20;
                            }

                            #region Address

                            var addresses = (from a in uniqueClaim
                                             where a.Row_Type == "ADDRESS"
                                             && a.Party_Type == org.Party_Type
                                             select a).ToList();

                            foreach (var a in addresses)
                            {
                                bool ignoreAddressFlag = false;
                                PipelineAddress address = new PipelineAddress();

                                address.SubBuilding = a.Data_Column_1;
                                address.Building = a.Data_Column_2;
                                address.BuildingNumber = a.Data_Column_3;
                                address.Street = a.Data_Column_4;
                                address.Town = a.Data_Column_6;
                                address.PostCode = a.Data_Column_8;

                                if (a.Entity_Type == "CORR")
                                {
                                    address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                                }


                                if (address.PostCode.ToUpper().Contains("IP1 3HS") && (address.Building.ToUpper().Contains("CROWN HOUSE") && address.Building.ToUpper().Contains("CROWN STREET")))
                                    ignoreAddressFlag = true;

                                if (address.PostCode.ToUpper().Contains("SS14 1NT") && address.Building.ToUpper().Contains("KELTING HOUSE"))
                                    ignoreAddressFlag = true;

                                if (address.BuildingNumber == "37" && address.PostCode.ToUpper().Contains("AL4 0UN") && address.Street.ToUpper().Contains("CENTRAL DRIVE"))
                                {
                                    ignoreAddressFlag = true;

                                }

                                if (!ignoreAddressFlag)
                                    organisation.Addresses.Add(address);


                            }

                            #endregion

                            #region Contact Details

                            var partyContactInfo = (from pci in uniqueClaim
                                                    where pci.Row_Type == "CONTACT"
                                                    && pci.Party_Type == org.Party_Type
                                                    select pci).ToList();

                            foreach (var pci in partyContactInfo)
                            {

                                if (pci.Entity_Type == "GENTEL")
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                    organisation.Telephones.Add(telephone);

                                }
                                else if (pci.Entity_Type == "MOBILE")
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                    organisation.Telephones.Add(telephone);
                                }
                                else if (pci.Entity_Type == "GENEML")
                                {
                                    PipelineEmail email = new PipelineEmail();
                                    email.EmailAddress = pci.Data_Column_1;
                                    organisation.EmailAddresses.Add(email);
                                }

                            }

                            #endregion

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping organisation information: " + ex);
                        }

                        motorClaim.Organisations.Add(organisation);

                    }


                    #endregion

                    #region NoneVehiclePeople

                    var assetRelationships = (from a in uniqueClaim
                                              where a.Row_Type == "ASSET_RELATIONSHIP"
                                              select a.Party_Type).ToList();

                    var people = (from p in uniqueClaim
                                  where p.Row_Type == "PARTY_MOTOR"
                                  && !p.Party_Type.Contains("CLM")
                                  && !p.Party_Type.Contains("NDV")
                                      //&& !p.Party_Type.Contains("THP0001")
                                  && !assetRelationships.Contains(p.Party_Type)
                                  && string.IsNullOrEmpty(p.Data_Column_20)
                                  select p).ToList();

                    if (people != null)
                    {

                        if (people.Count > 0)
                        {

                            PipelineVehicle noVehicleInvolved = new PipelineVehicle();
                            noVehicleInvolved.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;

                            foreach (var p in people)
                            {

                                PipelinePerson person = new PipelinePerson();

                                try
                                {

                                    var occupationList = (from o in uniqueClaim
                                                          where o.Row_Type == "EMPLOYMENT"
                                                          && o.Party_Type == p.Party_Type
                                                          select o.Data_Column_4).ToList();

                                    if (occupationList != null)
                                    {
                                        foreach (var occupation in occupationList)
                                        {
                                            person.Occupation += occupation + " ";
                                        }

                                    }

                                    #region PartyTypes

                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                                    #endregion

                                    #region Saltutation

                                    if (!string.IsNullOrEmpty(p.Data_Column_1))
                                    {
                                        switch (p.Data_Column_1.ToUpper())
                                        {
                                            case "MR":
                                                person.Salutation_Id = (int)Salutation.Mr;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "MRS":
                                                person.Salutation_Id = (int)Salutation.Mrs;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MS":
                                                person.Salutation_Id = (int)Salutation.Ms;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MISS":
                                                person.Salutation_Id = (int)Salutation.Miss;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MASTER":
                                                person.Salutation_Id = (int)Salutation.Master;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "DR":
                                                person.Salutation_Id = (int)Salutation.Dr;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                            case "REV":
                                                person.Salutation_Id = (int)Salutation.Reverend;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                            default:
                                                person.Salutation_Id = (int)Salutation.Unknown;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                        }
                                    }

                                    #endregion

                                    #region Name

                                    // List of Salutations
                                    List<string> salutations = new List<string>();
                                    salutations.Add("MR");
                                    salutations.Add("MS");
                                    salutations.Add("MISS");
                                    salutations.Add("MRS");
                                    salutations.Add("MASTER");
                                    salutations.Add("DR");

                                    var personName = p.Data_Column_5.Split(); // Surname may contain fullname

                                    if (personName.Count() == 7)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2] + " " + personName[3] + " " + personName[4] + " " + personName[5];
                                            person.LastName = personName[6];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3] + " " + personName[4] + " " + personName[5];
                                            person.LastName = personName[6];
                                        }
                                    }
                                    else if (personName.Count() == 6)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2] + " " + personName[3] + " " + personName[4];
                                            person.LastName = personName[5];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3] + " " + personName[4];
                                            person.LastName = personName[5];
                                        }
                                    }
                                    else if (personName.Count() == 5)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2] + " " + personName[3];
                                            person.LastName = personName[4];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2] + " " + personName[3];
                                            person.LastName = personName[4];
                                        }
                                    }
                                    else if (personName.Count() == 4)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.MiddleName = personName[2];
                                            person.LastName = personName[3];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1] + " " + personName[2];
                                            person.LastName = personName[3];
                                        }
                                    }
                                    else if (personName.Count() == 3)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.FirstName = personName[1];
                                            person.LastName = personName[2];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.MiddleName = personName[1];
                                            person.LastName = personName[2];
                                        }
                                    }
                                    else if (personName.Count() == 2)
                                    {
                                        if (salutations.Contains(personName[0].ToUpper()))
                                        {
                                            person.LastName = personName[1];
                                        }
                                        else
                                        {
                                            person.FirstName = personName[0];
                                            person.LastName = personName[1];
                                        }
                                    }
                                    else if (personName.Count() == 1)
                                    {
                                        if (!string.IsNullOrEmpty(p.Data_Column_3))
                                        {
                                            person.FirstName = p.Data_Column_3;
                                        }

                                        person.LastName = personName[0];
                                    }
                                    else
                                    {
                                        person.LastName = p.Data_Column_5;

                                        if (!string.IsNullOrEmpty(p.Data_Column_3))
                                        {
                                            person.FirstName = p.Data_Column_3;
                                        }
                                    }

                                    #endregion

                                    if (!string.IsNullOrEmpty(p.Data_Column_6))
                                    {
                                        DateTime dateOfBirth = _ConvertDate(p.Data_Column_6);
                                        person.DateOfBirth = dateOfBirth;
                                    }

                                    #region Gender

                                    if (!string.IsNullOrEmpty(p.Data_Column_9))
                                    {
                                        switch (p.Data_Column_9.ToUpper())
                                        {
                                            case "M":
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "F":
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                        }
                                    }

                                    #endregion

                                    #region Address


                                    var addresses = (from a in uniqueClaim
                                                     where a.Row_Type == "ADDRESS"
                                                     && a.Party_Type == p.Party_Type
                                                     select a).ToList();



                                    foreach (var a in addresses)
                                    {
                                        bool ignoreAddressFlag = false;

                                        PipelineAddress address = new PipelineAddress();

                                        address.SubBuilding = a.Data_Column_1;
                                        address.Building = a.Data_Column_2;
                                        address.BuildingNumber = a.Data_Column_3;
                                        address.Street = a.Data_Column_4;
                                        address.Town = a.Data_Column_6;
                                        address.PostCode = a.Data_Column_8;

                                        if (a.Entity_Type == "CORR")
                                        {
                                            address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                        }


                                        if (address.PostCode.ToUpper() == "WS11 6XH" && address.BuildingNumber == "35" && address.Street.ToUpper() == "HANNAFORD WAY" && address.Town.ToUpper() == "CANNOCK")
                                        {
                                            ignoreAddressFlag = true;
                                            if (person.FirstName.ToUpper() == "DIANNE" && person.LastName.ToUpper() == "BROOME")
                                            {
                                                person.FirstName = "";
                                                person.LastName = "";
                                            }
                                        }

                                        if (address.PostCode.ToUpper() == "PE9 4RU" && address.Street.ToUpper() == "CHURCH LANE" && address.Town.ToUpper() == "TALLINGTON")
                                        {
                                            ignoreAddressFlag = true;
                                            if (person.FirstName.ToUpper() == "HEATHER" && person.LastName.ToUpper() == "GASPON")
                                            {
                                                person.FirstName = "";
                                                person.LastName = "";
                                            }
                                        }

                                        if (address.PostCode.ToUpper().Contains("IP1 3HS") && (address.Building.ToUpper().Contains("CROWN HOUSE") && address.Building.ToUpper().Contains("CROWN STREET")))
                                            ignoreAddressFlag = true;

                                        if (address.PostCode.ToUpper().Contains("SS14 1NT") && address.Building.ToUpper().Contains("KELTING HOUSE"))
                                            ignoreAddressFlag = true;

                                        if (address.BuildingNumber == "37" && address.PostCode.ToUpper().Contains("AL4 0UN") && address.Street.ToUpper().Contains("CENTRAL DRIVE"))
                                        {
                                            ignoreAddressFlag = true;
                                            if (person.LastName.ToUpper() == "KALIN" && person.FirstName == null)
                                            {
                                                person.FirstName = "";
                                                person.LastName = "";
                                            }
                                            if ((person.FirstName.ToUpper() == "ALAN" && person.LastName.ToUpper() == "HAVERS") || (person.FirstName.ToUpper() == "THOMAS" && person.LastName.ToUpper() == "HILL"))
                                            {
                                                person.FirstName = "";
                                                person.LastName = "";
                                            }
                                        }


                                        if (!ignoreAddressFlag)
                                            person.Addresses.Add(address);
                                    }

                                    #endregion

                                    #region Contact Details

                                    var partyContactInfo = (from pci in uniqueClaim
                                                            where pci.Row_Type == "CONTACT"
                                                            && pci.Party_Type == p.Party_Type
                                                            select pci).ToList();

                                    foreach (var pci in partyContactInfo)
                                    {

                                        if (pci.Entity_Type == "GENTEL")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);

                                        }
                                        else if (pci.Entity_Type == "MOBILE")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);
                                        }
                                        else if (pci.Entity_Type == "GENEML")
                                        {
                                            PipelineEmail email = new PipelineEmail();
                                            email.EmailAddress = pci.Data_Column_1;
                                            person.EmailAddresses.Add(email);
                                        }

                                    }

                                    #endregion

                                    #region Organisations

                                    var partyRelationshipInfo = (from a in uniqueClaim
                                                                 where a.Row_Type == "PARTY_RELATIONSHIP"
                                                                 && a.Party_Type == p.Party_Type
                                                                 select a.Data_Column_1).ToList();

                                    var organisations = (from pm in uniqueClaim
                                                         where pm.Row_Type == "PARTY_MOTOR"
                                                         && partyRelationshipInfo.Contains(pm.Party_Type)
                                                         && !pm.Party_Type.Contains("TPI")
                                                         select pm).ToList();

                                    foreach (var org in organisations)
                                    {
                                        PipelineOrganisation organisation = new PipelineOrganisation();

                                        try
                                        {

                                            #region Party Type

                                            if (!string.IsNullOrEmpty(org.Party_Type))
                                            {
                                                string partyType = org.Party_Type.Substring(0, 3);

                                                switch (partyType.ToUpper())
                                                {
                                                    case "CLM":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                                        organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                                        break;
                                                    case "TPS":
                                                    case "SOL":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                                        break;
                                                    case "TPI":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Insurer;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                                        break;
                                                    default:
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                                        break;
                                                }
                                            }

                                            #endregion

                                            if (org.Data_Column_20 != null)
                                            {
                                                organisation.OrganisationName = org.Data_Column_20;
                                            }

                                            #region Address

                                            var orgAddresses = (from a in uniqueClaim
                                                                where a.Row_Type == "ADDRESS"
                                                                && a.Party_Type == org.Party_Type
                                                                select a).ToList();

                                            foreach (var a in orgAddresses)
                                            {
                                                bool ignoreAddressFlag = false;
                                                PipelineAddress address = new PipelineAddress();

                                                address.SubBuilding = a.Data_Column_1;
                                                address.Building = a.Data_Column_2;
                                                address.BuildingNumber = a.Data_Column_3;
                                                address.Street = a.Data_Column_4;
                                                address.Town = a.Data_Column_6;
                                                address.PostCode = a.Data_Column_8;

                                                if (a.Entity_Type == "CORR")
                                                {
                                                    address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                                                }


                                                if (address.PostCode.ToUpper().Contains("IP1 3HS") && (address.Building.ToUpper().Contains("CROWN HOUSE") && address.Building.ToUpper().Contains("CROWN STREET")))
                                                    ignoreAddressFlag = true;

                                                if (address.PostCode.ToUpper().Contains("SS14 1NT") && address.Building.ToUpper().Contains("KELTING HOUSE"))
                                                    ignoreAddressFlag = true;

                                                if (address.BuildingNumber == "37" && address.PostCode.ToUpper().Contains("AL4 0UN") && address.Street.ToUpper().Contains("CENTRAL DRIVE"))
                                                {
                                                    ignoreAddressFlag = true;

                                                }

                                                if (!ignoreAddressFlag)
                                                    organisation.Addresses.Add(address);


                                            }

                                            #endregion

                                            #region Contact Details

                                            var orgPartyContactInfo = (from pci in uniqueClaim
                                                                       where pci.Row_Type == "CONTACT"
                                                                       && pci.Party_Type == org.Party_Type
                                                                       select pci).ToList();

                                            foreach (var pci in orgPartyContactInfo)
                                            {

                                                if (pci.Entity_Type == "GENTEL")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);

                                                }
                                                else if (pci.Entity_Type == "MOBILE")
                                                {
                                                    PipelineTelephone telephone = new PipelineTelephone();
                                                    telephone.TelephoneType_Id = (int)TelephoneType.Mobile;
                                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                                    organisation.Telephones.Add(telephone);
                                                }
                                                else if (pci.Entity_Type == "GENEML")
                                                {
                                                    PipelineEmail email = new PipelineEmail();
                                                    email.EmailAddress = pci.Data_Column_1;
                                                    organisation.EmailAddresses.Add(email);
                                                }

                                            }

                                            #endregion

                                        }
                                        catch (Exception ex)
                                        {
                                            throw new CustomException("Error in mapping organisation information: " + ex);
                                        }

                                        person.Organisations.Add(organisation);

                                    }

                                    #endregion

                                    if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured || person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Claimant)
                                    {
                                        foreach (var vehicle in motorClaim.Vehicles)
                                        {

                                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                            {
                                                vehicle.People.Add(person);
                                            }
                                        }

                                    }
                                    else
                                    {
                                        noVehicleInvolved.People.Add(person);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new CustomException("Error in mapping person information: " + ex);
                                }

                            }

                            if (noVehicleInvolved.People.Count() > 0)
                            {
                                motorClaim.Vehicles.Add(noVehicleInvolved);
                            }

                        }
                    }

                    #endregion

                    #region Insured Vehicle and Driver Check

                    bool insuredVehicle = false;
                    bool insuredDriver = false;

                    foreach (var vehicle in motorClaim.Vehicles)
                    {
                        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                        {
                            insuredVehicle = true;

                            foreach (var person in vehicle.People)
                            {                                
                                if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                                {                                    
                                    insuredDriver = true;
                                }

                                if (insuredDriver == false && vehicle.People.Count() == 1)
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                    insuredDriver = true;
                                }
                            }
                        }
                    }

                    if (insuredVehicle == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                        }

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);
                    }
                    else
                    {
                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                            
                            foreach (var vehicle in motorClaim.Vehicles)
                            {
                                if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                {

                                    vehicle.People.Add(defaultInsuredDriver);

                                }

                            }

                        }
                    }

                    #endregion

                    if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;

                }
            }


        }

        private DateTime _ConvertDate(string s)
        {
            int year = 0;
            int month = 0;
            int day = 0;

            try
            {
                year = Convert.ToInt32(s.Substring(0, 4));
                month = Convert.ToInt32(s.Substring(4, 2));
                day = Convert.ToInt32(s.Substring(6, 2));
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method _ConvertDate: " + ex);
            }

            return new DateTime(year, month, day);
        }

    }
}
