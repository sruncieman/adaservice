﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MDA.MappingService.Impl.EsureNew.Motor.Model;
using System.Text;
using System.Globalization;
using MDA.MappingService.Helpers;

namespace MDA.MappingService.Impl.EsureNew.Motor
{
    public class Mapper : IMapper
    {
        private readonly Stream _fs;
        private StreamReader _claimDataFileStreamReader;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public string ClaimFile { get; set; }
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx,
            Func<CurrentContext, IPipelineClaim, object, int> processClaimFn, object statusTracking)
        {
            _fs = fs;
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void AssignFiles()
        {
            _claimDataFileStreamReader = new StreamReader(_fs);
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: ", ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: ", ex);
            }
        }
        
        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }

        public void RetrieveDistinctClaims()
        {
            try
            {

                _uniqueClaimNumberList = _coreClaimData.Where(x => x.CLAIM_CASE_NO != "" && x.Notification_date.HasValue).Select(x => x.CLAIM_CASE_NO).AsParallel().Distinct().OrderBy(x => x).ToList();

            }
            catch (Exception ex)
            {

                throw new CustomException("Error in retrieving distinct claims: ", ex);
            }
        }

        public void Translate()
        {
            foreach(var claim in _uniqueClaimNumberList)
            {
                var motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.FirstOrDefault(x => x.CLAIM_CASE_NO == claim);

                if (uniqueClaim != null)
                {
                    // Get all records in import linked to this Claim Number
                    var claimInfo = _coreClaimData.Where(c => c.CLAIM_CASE_NO == claim).ToList();

                    #region Claim

                    try
                    {
                        if (IsValid(uniqueClaim.CLAIM_CASE_NO))
                            motorClaim.ClaimNumber = uniqueClaim.CLAIM_CASE_NO;

                        if (uniqueClaim.Notification_date != null)
                            motorClaim.IncidentDate = uniqueClaim.incident_date.Value;

                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim: ", ex);
                    }

                    #endregion

                    #region ClaimInfo

                    try
                    {
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;

                        var s = new StringBuilder();
                        if (IsValid(uniqueClaim.CURRENT_EVENT_TYPE))
                            s.Append(uniqueClaim.CURRENT_EVENT_TYPE + ": ");
                        if (IsValid(uniqueClaim.CURRENT_EVENT_CAUSE_TYPE))
                            s.Append(uniqueClaim.CURRENT_EVENT_CAUSE_TYPE);
                        motorClaim.ExtraClaimInfo.IncidentCircumstances = s.ToString();
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: ", ex);
                    }

                    #endregion

                    #region Insured

                    try
                    {
                        var insuredVehicle = GenerateInsuredVehicle(claimInfo);
                        motorClaim.Vehicles.Add(insuredVehicle);
                    }
                    catch (Exception ex)
                    {                        
                        throw new CustomException("Error in mapping insured vehicle information: ", ex);
                    }

                    #endregion

                    #region Claimants

                    try
                    {
                        var blankVehicle = GenerateBlankVehicle(claimInfo);
                        if (blankVehicle != null)
                            motorClaim.Vehicles.Add(blankVehicle);
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claimant vehicle information: ", ex);
                    }

                    #endregion

                    #region

                    #endregion

                }

                if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1) return;
            }
        }

        private PipelineVehicle GenerateBlankVehicle(List<ClaimData> claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var tpdata = claimInfo.Where(x => x.PARTY_NAME != null || x.PARTY_NAME != "").ToList();
            if (!tpdata.Any())
                return null;
            
            var blankVehicle = new PipelineVehicle { I2V_LinkData = { Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved } };
            var claimants = GenerateClaimants(tpdata);
            if (claimants != null && claimants.Any())
                blankVehicle.People.AddRange(claimants);

            return blankVehicle;
        }
        private List<PipelinePerson> GenerateClaimants(IEnumerable<ClaimData> claimData)
        {
            if (claimData == null)
                throw new ArgumentNullException("claimData");

            var claimants = new List<PipelinePerson>();

            foreach (var claimDatum in claimData)
            {
                var claimant = GenerateClaimant(claimDatum);
                
                if (claimant == null) continue;

                if (PersonMatch(claimants, claimant))
                    continue;

                try
                {
                    claimant.Organisations = GeneratePipelineOrganisations(claimDatum, claimData);
                } catch(Exception ex)
                {}

                claimants.Add(claimant);
            }

            return claimants;
        }
        private static PipelinePerson GenerateClaimant(ClaimData claimDatum)
        {
            if (claimDatum == null) { throw new ArgumentNullException("claimData"); }

            var claimant = new PipelinePerson { I2Pe_LinkData = { PartyType_Id = (int)PartyType.Claimant } };

            if (IsValid(claimDatum.tp_claimant_name))
            {
                try
                {
                    var names = claimDatum.tp_claimant_name.Split(' ');
                    if (names.Length > 1)
                    {
                        claimant.FirstName = names.First();
                        claimant.LastName = names.Last();
                    }

                    var address = MapToTpPipelineAddress(claimDatum);
                    if (address != null)
                        claimant.Addresses = new List<PipelineAddress> { address };

                    if (IsValid(claimDatum.tp_day_number))
                        claimant.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claimDatum.tp_day_number, TelephoneType_Id = (int)TelephoneType.Unknown });

                    if (IsValid(claimDatum.tp_evening_no))
                        claimant.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claimDatum.tp_evening_no, TelephoneType_Id = (int)TelephoneType.Unknown });

                    if (IsValid(claimDatum.tp_mobile))
                        claimant.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claimDatum.tp_mobile, TelephoneType_Id = (int)TelephoneType.Mobile });

                    if (NiNumberHelper.IsValid(claimDatum.tp_NI_num))
                        claimant.NINumbers.Add(new PipelineNINumber { NINumber1 = NiNumberHelper.ReturnValueOrNull(claimDatum.tp_NI_num) });

                    if (IsValid(claimDatum.tp_claimant_dob))
                        claimant.DateOfBirth = FormatDate(claimDatum.tp_claimant_dob);
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error splitting claimant name in mapping: " + ex);
                }
            }
            else
                claimant = null;

            return claimant;

        }

        private List<PipelineOrganisation> GeneratePipelineOrganisations(ClaimData claimDatum, IEnumerable<ClaimData> claimData)
        {
            // claimDatum is the single row used for making the Claimaint
            // claimData is all the rows related that may contain additional orgs

            List<PipelineOrganisation> organisations = new List<PipelineOrganisation>();

            if(claimDatum != null && !string.IsNullOrEmpty(claimDatum.cho_name))
            {
                PipelineOrganisation org = new PipelineOrganisation
                {
                    OrganisationName = claimDatum.cho_name,
                    OrganisationType_Id = (int)OrganisationType.CreditHire,
                    P2O_LinkData = { Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire },
                    Addresses = new List<PipelineAddress>()
                    {
                        new PipelineAddress
                        {
                            PostCode = claimDatum.cho_postcode
                        }
                    }
                };

                organisations.Add(org);
            }

            return organisations;
        }

        private PipelineVehicle GenerateInsuredVehicle(List<ClaimData> claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var insuredVehicle = MapToPipelineInsuredVehicle(claimInfo.FirstOrDefault());

            var data = claimInfo.FirstOrDefault();

            if (data == null) return insuredVehicle;

            // Add the insured driver
            var insuredDriver = GenerateInsuredDriver(claimInfo);
            insuredVehicle.People.Add(insuredDriver);

            return insuredVehicle;
        }
        private PipelinePerson GenerateInsuredDriver(List<ClaimData> claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var person = new PipelinePerson
            {
                I2Pe_LinkData =
                    {
                        PartyType_Id = (int)PartyType.Insured,
                        SubPartyType_Id = (int)SubPartyType.Driver
                    },
                Pe2Po_LinkData =
                {
                    PolicyLinkType_Id = (int)PolicyLinkType.Insured,
                }
            };    

            var data = claimInfo.FirstOrDefault();

            if (data != null)
            {               
                if(IsValid(data.TITLE))
                {
                    switch(data.TITLE.ToUpper())
                    {
                        case "MRS":
                            person.Salutation_Id = (int)Salutation.Mrs;
                            break;
                        case "MISS":
                            person.Salutation_Id = (int)Salutation.Miss;
                            break;
                        case "MR":
                            person.Salutation_Id = (int)Salutation.Mrs;
                            break;
                        case "MS":
                            person.Salutation_Id = (int)Salutation.Mrs;
                            break;
                        case "DR":
                            person.Salutation_Id = (int)Salutation.Mrs;
                            break;
                        default:
                            person.Salutation_Id = (int)Salutation.Unknown;
                            break;
                    }
                }
                
                if (IsValid(data.FORENAME))
                    person.FirstName = data.FORENAME;

                if (IsValid(data.SURNAME))
                    person.LastName = data.SURNAME;

                if (data.DATE_OF_BIRTH.HasValue)
                    person.DateOfBirth = data.DATE_OF_BIRTH;

                var address = MapToPipelineAddress(data);
                if (address != null)
                    person.Addresses = new List<PipelineAddress> { address };

                if (EmailHelper.IsValid(data.EMAIL_ADDRESS))
                    person.EmailAddresses.Add(new PipelineEmail { EmailAddress = EmailHelper.ReturnValueOrNull(data.EMAIL_ADDRESS) });

                if (IsValid(data.PHONE_DAY))
                    person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.PHONE_DAY, TelephoneType_Id = (int)TelephoneType.Unknown });
                
                if (IsValid(data.PHONE_EVENING))
                    person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.PHONE_EVENING, TelephoneType_Id = (int)TelephoneType.Unknown });

                if (IsValid(data.PHONE_MOBILE))
                    person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.PHONE_MOBILE, TelephoneType_Id = (int)TelephoneType.Mobile });

            }

            return person;

        }

        private static PipelineAddress MapToPipelineAddress(ClaimData data)
        {
            if (data == null)
                return null;

            var address = new PipelineAddress();

            if (IsValid(data.HOUSE_NUMBER))
                address.BuildingNumber = data.HOUSE_NUMBER;

            if (IsValid(data.HOUSE_NAME))
                address.Building = data.HOUSE_NAME;

            if (IsValid(data.STREET))
                address.Street = data.STREET;

            if (IsValid(data.TOWN))
                address.Town = data.TOWN;

            if (IsValid(data.COUNTY))
                address.County = data.COUNTY;

            if (IsValid(data.POSTCODE))
                address.PostCode = data.POSTCODE;

            return address;
        }
        private static PipelineAddress MapToTpPipelineAddress(ClaimData data)
        {
            if (data == null)
                return null;

            var address = new PipelineAddress();

            if (IsValid(data.tp_claimant_house_no))
                address.BuildingNumber = data.tp_claimant_house_no;

            if (IsValid(data.TP_town))
                address.Town = data.TP_town;

            if (IsValid(data.tp_county))
                address.County = data.tp_county;

            if (IsValid(data.tp_postcode))
                address.PostCode = data.tp_postcode;

            return address;
        }
        private static PipelineVehicle MapToPipelineInsuredVehicle(ClaimData claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var insuredVehicle = new PipelineVehicle
            {
                VehicleRegistration = DefaultValue(claimInfo.REGISTRATION_NUMBER),
                VehicleModel = DefaultValue(claimInfo.MODEL),
                VehicleMake = DefaultValue(claimInfo.MAKE),
                I2V_LinkData = { Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle }
            };

            return insuredVehicle;
        }

        private static bool PersonMatch(List<PipelinePerson> claimants, PipelinePerson claimant)
        {
            if (claimants == null) return false;
            if (claimant == null) return false;

            try
            {
                return claimants.Any(c => claimant.FirstName != null && claimant.LastName != null && claimant.FirstName.Equals(c.FirstName, StringComparison.InvariantCultureIgnoreCase) && claimant.LastName.Equals(c.LastName, StringComparison.InvariantCultureIgnoreCase));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in matching people in mapper: ", ex);
            }
        }
        public static bool IsValid(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            value = Fix(value);

            return !string.IsNullOrEmpty(value) &&
                   !value.Equals("NULL", StringComparison.InvariantCultureIgnoreCase);
        }
        public static string DefaultValue(string value)
        {
            return IsValid(value) ? Fix(value) : null;
        }
        public static DateTime? FormatDate(string date)
        {
            DateTime dt;

            date = date.Replace("\"", "");

            if (DateTime.TryParseExact(date, "dd/MM/yyyy", null, DateTimeStyles.None, out dt))
                return dt;

            if (DateTime.TryParseExact(date, "dd/MM/yyyy hh:mm:ss", null, DateTimeStyles.None, out dt))
                return dt;

            if (DateTime.TryParseExact(date, "ddMMMyyyy:hh:mm:ss", null, DateTimeStyles.None, out dt))
                return dt;

            if (dt.Year == 1899 && dt.Month == 12 & dt.Day == 30)
                return null;

            return dt;
        }
        private static string Fix(string value)
        {
            return string.IsNullOrEmpty(value) ? null : value.Trim();            
        }
    }
}
