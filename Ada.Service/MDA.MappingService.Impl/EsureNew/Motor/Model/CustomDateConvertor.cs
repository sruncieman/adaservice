﻿using System;
using System.Globalization;
using FileHelpers;

namespace MDA.MappingService.Impl.EsureNew.Motor.Model
{
    public class CustomDateConvertor : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;

            value = value.Replace("\"", "");

            if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, DateTimeStyles.None, out dt))
                return dt;


            if (DateTime.TryParseExact(value, "dd/MM/yyyy hh:mm:ss", null, DateTimeStyles.None, out dt))
                return dt;

            if (DateTime.TryParseExact(value, "ddMMMyyyy:hh:mm:ss", null, DateTimeStyles.None, out dt))
                return dt;

            if (dt.Year == 1899 && dt.Month == 12 & dt.Day == 30)
            {
                return null;
            }

            return dt;
        }
    }
}
