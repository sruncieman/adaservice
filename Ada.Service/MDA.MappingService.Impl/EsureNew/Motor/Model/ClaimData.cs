﻿using System;
using FileHelpers;
// ReSharper disable InconsistentNaming

namespace MDA.MappingService.Impl.EsureNew.Motor.Model
{
    [DelimitedRecord(",")]
    [IgnoreFirst(1)]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CLAIM_CASE_NO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CLA_SUBCASE_NO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string POLICY_NO;

        [FieldConverter(typeof(CustomDateConvertor))]
        public DateTime? Notification_date;

        [FieldConverter(typeof(CustomDateConvertor))]
        public DateTime? subcase_open_date;

        [FieldConverter(typeof(CustomDateConvertor))]
        public DateTime? incident_date;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TITLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FORENAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SURNAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FULL_NAME;

        [FieldConverter(typeof(CustomDateConvertor))]
        public DateTime? DATE_OF_BIRTH;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HOUSE_NUMBER;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HOUSE_NAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string STREET;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TOWN;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COUNTY;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string POSTCODE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EMAIL_ADDRESS;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHONE_DAY;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHONE_EVENING;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PHONE_MOBILE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MAKE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MODEL;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string REGISTRATION_NUMBER;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string REGISTERED_KEEPER;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NUMBER_CONVICTIONS;

        [FieldConverter(typeof(CustomDateConvertor))]
        public DateTime? DAY_START_DATE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string INITIAL_EVENT_TYPE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CURRENT_EVENT_TYPE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string INITIAL_EVENT_CAUSE_TYPE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CURRENT_EVENT_CAUSE_TYPE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TP_town;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SUBCASE_TYPE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ORGANISED_INSPECTION_IND;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_claimant_name;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_county;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_postcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_day_number;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_evening_no;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_mobile;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TP_VEHICLE_DETAILS;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TP_VEHICLE_DAMAGE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TOC_HIRE_IND;

        [FieldConverter(typeof(CustomDateConvertor))]
        public DateTime? CAR_HIRE_DATE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string THIRD_PARTY_SPEED;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CLASS_OF_VEHICLE_ENTITLEMENT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LOCATION_IN_VEHICLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_NI_num;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_party_type;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PARTY_NAME; // tp_party_name

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string subcase_grouping;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string cho_name;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string cho_postcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string cho_involvement_date;

        [FieldOptional]
        [FieldConverter(typeof(PotentialCommaConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_claimant_dob;

        [FieldOptional]
        [FieldConverter(typeof(CommonNonValuesConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string tp_claimant_house_no;
    }
}
