﻿using System;
using System.Globalization;
using FileHelpers;

namespace MDA.MappingService.Impl.EsureNew.Motor.Model
{
    public class PotentialCommaConverter : ConverterBase
    {
        public override object StringToField(string value)
        {
            if (value != null && value.Contains(","))
                value = string.Format("\"{0}\"", value);

            return value;
        }
    }
}
