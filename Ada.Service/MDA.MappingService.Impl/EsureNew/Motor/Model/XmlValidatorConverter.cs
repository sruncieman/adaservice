﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Impl.EsureNew.Motor.Model
{
    public class XmlValidatorConverter : ConverterBase
    {
        public override object StringToField(string value)
        {
            if (string.IsNullOrEmpty(value.ToString()))
                return value;

            string[] charToReplace = new string[] {
                "<",
                ">"
            };

            foreach (string item in charToReplace)
                value = value.Replace(item, "");

            return value;
        }
    }
}
