﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Impl.Hertz.Motor.Model
{
    public class CustomDateConverter : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;

            if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, DateTimeStyles.None, out dt))
                return dt;

            return null;
        }
    }
}
