﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using FileHelpers;
using System;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Pipeline.Model;

namespace MDA.MappingService.Impl.Hertz.Motor.Model
{
    [DelimitedRecord(",")]
    [IgnoreFirst(1)]
    public class ClaimData
    {

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Feature;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? CreatedDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimId;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimStatus;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? AccidentDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentTime;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentCircumstances;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentSubCode;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? ClaimMadeDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentLocationAddressType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentLocationAddress1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentLocationAddress2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentLocationAddress3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentLocationAddress4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentLocationAddressPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentLocationFurtherSpecifics;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EstimatedValueOfClaim;

        // This Field Seems to be Blank and a Duplicate
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EstimatedValueofClaim;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimStage;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Litigated;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateLitigated;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String OutcomeOfTrial;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? TrialDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TotalPaymentsMade;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TotalRecoveryExpected;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PoliceReference;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PoliceOfficerName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PoliceTelephoneNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Involvement;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Title;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Firstname;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MiddleName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Surname;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Alias;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Gender;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? BirthDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PassportNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NationalInsuranceNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DrivingLicenceNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DrivingLicenceType;

        // ReSharper disable InconsistentNaming
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ADDRESS1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ADDRESS2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ADDRESS3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ADDRESS4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String POSTCODE;
        // ReSharper restore InconsistentNaming

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EmailAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LandlineTelephone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MobileTelephone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WorkTelephone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuranceClaimNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Occupation;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EmployerCompanyName;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? EmploymentStartDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PreviousEmployerCompanyName;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PreviousEmploymentStartDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NatureOfInjury;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LocationOfInjury;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? MedicalAttentionSoughtDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String FullRecoveryMade;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateFullRecoveryMade;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AddressType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Postcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyStatus;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyStartDate;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyEndDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CoverType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyUsage;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NumberOfPreviousAccidentsDisclosed;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateOfPreviousAccident;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Premium;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyExcess;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyPaymentType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String IpAddressUsedForInception;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleRegistration;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VinNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AbiCode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleMake;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleModel;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleMileage;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleColour;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PreviousVehicleColour;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? VehicleColourChangeDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NumberOfPreviousKeepers;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? StartDateOfCurrentKeeper;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateFirstRegistered;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateOfManufacture;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EngineCapacity;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Transmission;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LrHandDrive;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String BodyStyleDescription;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NumberOfDoors;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NumberOfSeats;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VrmChange;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CherishedTransferMarker;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? CherishedTransferDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ExportMarker;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? ExportDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ImportMarker;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? ImportDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ScrappedMarker;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? ScrappedDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PncForceCode;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PncRecordDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleInsuranceGroup;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MainHirerName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MainHirerDrivingLicenceNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireStatus;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? HireStartDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireStartTime;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? HireEndDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireEndTime;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireStartMileage;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireEndMileage;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleRegistration;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleMake;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleModel;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleDeliveryAddress1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleDeliveryAddress2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleDeliveryAddress3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleDeliveryAddress4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleDeliveryAddressPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehiclePickupAddress1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehiclePickupAddress2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehiclePickupAddress3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehiclePickupAddress4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehiclePickupAddressPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ReasonForCollection;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NumberOfDaysHire;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TotalCostOfHire;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CondtionofHireVehicleOnCollection;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String IpAddressUsedForHire;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Channelofbusiness;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Methodofpayment;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Cdw;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Branchdeliveringvehicle;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Coi;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyRegistrationNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyAddress1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyAddress2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyAddress3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyAddress4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyAddressPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyLandlineTelephone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyMobileTelephone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyWorkTelephone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyEmailAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CompanyWebAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MainContactName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NameofEngineer;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleInspectedRegistration;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleInspectedVin;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InstructedBy;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateInstructionsReceived;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? InspectionDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InspectionLocationName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InspectionAddress1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InspectionAddress2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InspectionAddress3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InspectionAddress4;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InspectionAddressPostcode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EstimatedVehicleValue;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleCondition;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleDamageDescription;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ConditionofSteering;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ConditionofBrakes;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Audio;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Extras;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Modifications;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? TaxExpiryDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AirBagsDeployed;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EstimatedTotalRepairCost;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EstimatedDurationOfRepairs;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Roadworthy;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TotalLoss;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CategoryOfLoss;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SalvageCategory;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NameofRepairer;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DurationOfRepairs;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? RepairCompletionDate;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RepairCost;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccountHolderName;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String BankName;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String BranchName;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SortCode;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccountNumber;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CreditCardNumber;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? CreditCardStartDate;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? CreditCardExpiryDate;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateDetailsTaken;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AmountReceived;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PaymentDescription;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ApplicationType;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ApplicationDescription;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ApplicationAmount;

        [FieldOptional]
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateOfApplication;

        [FieldOptional]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ApplicationStatus;

        [FieldOptional]
        [FieldValueDiscarded]
        public String[] Dummy;

        #region Derived Functionality

        [FieldNotInFile]
        private static readonly string[] OrganisationIndicators = { "LTD", "LIMITED", "PLC", "LLP" };

        [FieldNotInFile]
        private static readonly string[] InsuredKeys = { "renter", "first party" };

        [FieldNotInFile]
        private static readonly string[] DriverKeys = { "renter", "driver", "cyclist", "animal owner" };

        [FieldNotInFile]
        private static readonly string[] PassengerKeys = { "passenger" };

        public bool HasVehicleData
        {
            get
            {
                return !string.IsNullOrEmpty(VehicleRegistration);
            }
        }

        public bool HasHireVehicleData
        {
            get
            {
                return
                    !string.IsNullOrEmpty(HireVehicleRegistration) ||
                    !string.IsNullOrEmpty(HireVehicleMake) ||
                    !string.IsNullOrEmpty(HireVehicleModel) ||
                    HireStartDate.HasValue ||
                    HireEndDate.HasValue;
            }
        }

        public bool HasPersonData
        {
            get
            {
                return !string.IsNullOrEmpty(Involvement);
            }
        }

        public bool HasAddressData
        {
            get
            {
                return
                    !string.IsNullOrEmpty(ADDRESS1) ||
                    !string.IsNullOrEmpty(ADDRESS2) ||
                    !string.IsNullOrEmpty(POSTCODE);
            }
        }

        public bool HasNoNames
        {
            get
            { 
                return
                    string.IsNullOrEmpty(Surname) &&
                    string.IsNullOrEmpty(MiddleName) &&
                    string.IsNullOrEmpty(Firstname);
            }
        }

        [FieldNotInFile]
        private List<string> _surnameSplit;
        public List<string> SurnameSplit
        {
            get
            {
                if (_surnameSplit == null)
                {
                    if (string.IsNullOrEmpty(Surname))
                    {
                        _surnameSplit = new List<string>();
                    }
                    else
                    {
                        var names = Surname.Split(' ').ToList();

                        names.RemoveAll(n => string.IsNullOrEmpty(n) || n.Trim(' ') == string.Empty);

                        _surnameSplit = names;
                    }
                }
                return _surnameSplit;
            }
        }

        [FieldNotInFile]
        private Salutation? _salutation;
        public Salutation Salutation
        {
            get
            {
                if (!_salutation.HasValue)
                {
                    foreach (var possibleSalutation in SurnameSplit)
                    {
                        if (!string.IsNullOrEmpty(possibleSalutation))
                        {
                            var salutationAttempt = SalutationHelper.GetSalutation(possibleSalutation);
                            if (salutationAttempt != Salutation.Unknown)
                            {
                                _salutation = salutationAttempt;
                            }
                        }
                    }

                    // Still Not Loaded? Try to get the Salutation from the csv Title field
                    if (!_salutation.HasValue && !string.IsNullOrEmpty(Title.Trim(' ')))
                    {
                        Salutation salutationFromTitle = SalutationHelper.GetSalutation(Title);
                        if (salutationFromTitle != Salutation.Unknown)
                        {
                            _salutation = salutationFromTitle;
                        }
                    }

                    if (!_salutation.HasValue)
                    {
                        _salutation = Salutation.Unknown;
                    }
                }
                return _salutation.Value;
            }
        }

        public bool HasOrganisationIndicator
        {
            get
            {
                if (SurnameSplit == null || !SurnameSplit.Any())
                    return false;

                return
                    OrganisationIndicators.Contains(SurnameSplit.Last().ToUpper());
            }
        }

        [FieldNotInFile]
        private Incident2VehicleLinkType? _vehicleLinkType;
        public Incident2VehicleLinkType VehicleLinkType
        {
            get
            {
                if (!_vehicleLinkType.HasValue)
                {
                    if (string.IsNullOrEmpty(Involvement))
                    {
                        _vehicleLinkType = Incident2VehicleLinkType.Unknown;
                    }
                    else
                    {
                        var isInsured = InsuredKeys.Any(term => Involvement.ToLower().Contains(term));

                        _vehicleLinkType =
                            isInsured
                            ? Incident2VehicleLinkType.InsuredVehicle
                            : Incident2VehicleLinkType.ThirdPartyVehicle;
                    }
                }
                return _vehicleLinkType.Value;
            }
        }

        [FieldNotInFile]
        private PartyType? _partyType;
        public PartyType PartyType
        {
            get
            {
                if (!_partyType.HasValue)
                {
                    if (string.IsNullOrEmpty(Involvement))
                    {
                        _partyType = PartyType.Unknown;
                    }
                    else
                    {
                        var isInsured = InsuredKeys.Any(term => Involvement.ToLower().Contains(term));

                        _partyType = isInsured
                            ? PartyType.Insured
                            : PartyType.ThirdParty;
                    }
                }
                return _partyType.Value;
            }
        }

        [FieldNotInFile]
        private SubPartyType? _subPartyType;
        public SubPartyType SubPartyType
        {
            get
            {
                if (!_subPartyType.HasValue)
                {
                    if (string.IsNullOrEmpty(Involvement))
                    {
                        _subPartyType = SubPartyType.Unknown;
                    }
                    else
                    {
                        var isDriver = DriverKeys.Any(term => Involvement.ToLower().Contains(term));
                        var isPassenger = PassengerKeys.Any(term => Involvement.ToLower().Contains(term));

                        if (isDriver)
                            _subPartyType = SubPartyType.Driver;
                        else if (isPassenger)
                            _subPartyType = SubPartyType.Passenger;
                        else
                            _subPartyType = SubPartyType.Unknown;
                    }
                }
                return _subPartyType.Value;
            }
        }

        #endregion
    }
}