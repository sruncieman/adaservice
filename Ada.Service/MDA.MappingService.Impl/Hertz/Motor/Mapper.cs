﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Impl.Hertz.Motor.Model;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace MDA.MappingService.Impl.Hertz.Motor
{
    public class Mapper : IMapper
    {
        private readonly Stream _fs;
        private StreamReader _claimDataFileStreamReader;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public string ClaimFile { get; set; }
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;
        private List<string> DefaultAddressesForClient { get; set; }
        private List<string> DefaultPeopleForClient { get; set; }
        private List<string> DefaultOrganisationsForClient { get; set; }

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> processClaimFn, object statusTracking, List<string> defaultAddressesForClient, List<string> defaultPeopleForClient, List<string> defaultOrganisationsForClient)
        {
            _fs = fs;
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;
            DefaultAddressesForClient = defaultAddressesForClient;
            DefaultPeopleForClient = defaultPeopleForClient;
            DefaultOrganisationsForClient = defaultOrganisationsForClient;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void AssignFiles()
        {
            _claimDataFileStreamReader = new StreamReader(_fs);
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers
                _uniqueClaimNumberList = _coreClaimData.Where(x => x.ClaimId != "").Select(x => x.ClaimId).Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        }

        public void Translate()
        {
            foreach (var claim in _uniqueClaimNumberList)
            {
                var motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.FirstOrDefault(x => x.ClaimId == claim);

                if (uniqueClaim != null)
                {
                    #region Claim

                    try
                    {
                        if (!string.IsNullOrEmpty(uniqueClaim.ClaimId))
                            motorClaim.ClaimNumber = uniqueClaim.ClaimId;

                        if (uniqueClaim.AccidentDate != null)
                            motorClaim.IncidentDate = Convert.ToDateTime(uniqueClaim.AccidentDate);

                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion Claim

                    #region ClaimInfo

                    var claimInfo = _coreClaimData
                        .Where(c => c.ClaimId == claim)
                        .ToList();

                    try
                    {
                        if (!string.IsNullOrEmpty(uniqueClaim.ClaimStatus))
                        {
                            switch (uniqueClaim.ClaimStatus.ToUpper())
                            {
                                case "OPEN":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                    break;

                                case "REOPENED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                    break;

                                case "CLOSED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                    break;
                            }
                        }

                        if (uniqueClaim.ClaimMadeDate != null)
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = uniqueClaim.ClaimMadeDate;

                        if (!string.IsNullOrEmpty(uniqueClaim.AccidentSubCode))
                            motorClaim.ExtraClaimInfo.ClaimCode = uniqueClaim.AccidentSubCode;

                        if (!string.IsNullOrEmpty(uniqueClaim.AccidentLocationAddressType))
                            motorClaim.ExtraClaimInfo.IncidentLocation = uniqueClaim.AccidentLocationAddressType;

                        if (!string.IsNullOrEmpty(uniqueClaim.AccidentCircumstances))
                            motorClaim.ExtraClaimInfo.IncidentCircumstances = uniqueClaim.AccidentCircumstances;

                        if (!string.IsNullOrEmpty(uniqueClaim.EstimatedValueOfClaim))
                            motorClaim.ExtraClaimInfo.Reserve = Convert.ToDecimal(uniqueClaim.EstimatedValueOfClaim);

                        if (!string.IsNullOrEmpty(uniqueClaim.TotalPaymentsMade))
                            motorClaim.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(uniqueClaim.TotalPaymentsMade);

                        if (!string.IsNullOrEmpty(uniqueClaim.PoliceReference))
                            motorClaim.ExtraClaimInfo.PoliceReference = uniqueClaim.PoliceReference;

                        motorClaim.ExtraClaimInfo.Reserve = PopulateReserve(claimInfo);

                        motorClaim.ExtraClaimInfo.PaymentsToDate = PopulatePaymentsToDate(claimInfo);
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion ClaimInfo

                    #region Policy

                    try
                    {
                        motorClaim.Policy.Insurer = "Hertz";

                        #region Fields Removed Due to Too Many Duplicates - Request by Phil 21-April-2016
                        //if (!string.IsNullOrEmpty(uniqueClaim.PolicyNumber))
                        //    motorClaim.Policy.PolicyNumber = uniqueClaim.PolicyNumber;

                        //if (uniqueClaim.PolicyStartDate != null)
                        //    motorClaim.Policy.PolicyStartDate = uniqueClaim.PolicyStartDate;

                        //if (uniqueClaim.PolicyEndDate != null)
                        //    motorClaim.Policy.PolicyEndDate = uniqueClaim.PolicyEndDate;
                        #endregion

                        if (!string.IsNullOrEmpty(uniqueClaim.Premium))
                            motorClaim.Policy.Premium = Convert.ToDecimal(uniqueClaim.Premium);

                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping policy: " + ex);
                    }

                    #endregion Policy

                    #region Vehicle(s)

                    PipelineVehicle insuredVehicle;
                    List<PipelineVehicle> otherInsuredVehicles;
                    List<PipelineVehicle> thirdPartyVehicles;

                    try
                    {
                        insuredVehicle = GenerateInsuredVehicle(claimInfo);

                        motorClaim.Vehicles.Add(insuredVehicle);

                        otherInsuredVehicles = CreateOptionalPartyVehicleRecords
                        (
                            claimInfo.OtherInsuredPartyPersonData(),
                            claimInfo.InsuredVehicleData(),
                            Incident2VehicleLinkType.InsuredVehicle
                        );

                        motorClaim.Vehicles.AddRange(otherInsuredVehicles);

                        thirdPartyVehicles = CreateOptionalPartyVehicleRecords
                        (
                            claimInfo.ThirdPartyPersonData(),
                            claimInfo.ThirdPartyVehicleData(),
                            Incident2VehicleLinkType.ThirdPartyVehicle
                        );

                        motorClaim.Vehicles.AddRange(thirdPartyVehicles);
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping vehicle information: " + ex);
                    }

                    #endregion

                    #region Person(s)

                    try
                    {
                        GenerateInsuredDriver(claimInfo, insuredVehicle, motorClaim);

                        var allInsuredVehicles = otherInsuredVehicles.ToArray().ToList();
                        // Need this because Other Insured People Need to go into the Default Insured Vehicle if they are First Party Insured
                        allInsuredVehicles.Add(insuredVehicle);

                        GeneratePartiesInVehicles(claimInfo.OtherInsuredPartyPersonData(), allInsuredVehicles, motorClaim);

                        GeneratePartiesInVehicles(claimInfo.ThirdPartyPersonData(), thirdPartyVehicles, motorClaim);

                        // Remove Blank Vehicles With No Assignees
                        motorClaim.Vehicles.RemoveAll(v => string.IsNullOrEmpty(v.VehicleRegistration) && !v.People.Any());
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping person information: " + ex);
                    }

                    #endregion
                }

                if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1) return;
            }
        }

        private decimal? PopulateReserve(IEnumerable<ClaimData> claimInfo)
        {
            decimal? reserve = null;
            foreach (var claimData in claimInfo)
            {
                decimal estimatedValueOfClaim;
                if (Decimal.TryParse(claimData.EstimatedValueOfClaim, out estimatedValueOfClaim))
                {
                    if (!reserve.HasValue) reserve = decimal.Zero;
                    reserve += estimatedValueOfClaim;
                }
            }

            return reserve;
        }

        private decimal? PopulatePaymentsToDate(IEnumerable<ClaimData> claimInfo)
        {
            decimal? paymentsToDate = null;
            foreach (var claimData in claimInfo)
            {
                decimal totalPaymentsMade;
                if (Decimal.TryParse(claimData.TotalPaymentsMade, out totalPaymentsMade))
                {
                    if (!paymentsToDate.HasValue) paymentsToDate = decimal.Zero;
                    paymentsToDate += totalPaymentsMade;
                }
            }

            return paymentsToDate;
        }

        private void GenerateInsuredDriver(IEnumerable<ClaimData> claimInfo, PipelineVehicle insuredVehicle, PipelineMotorClaim motorClaim)
        {
            var insuredDriver = new PipelinePerson
                {
                    I2Pe_LinkData = 
                    {
                        PartyType_Id = (int) PartyType.Insured, 
                        SubPartyType_Id = (int) SubPartyType.Driver
                    }
                };

            var insuredDriverData = claimInfo.InsuredPartyPersonData().FirstOrDefault();

            if (insuredDriverData != null)
            {
                var isOrgDriver = ClaimDataIsOrganisation(insuredDriver, insuredDriverData);
                if (isOrgDriver)
                {
                    var insuredOganisation = new PipelineOrganisation();

                    PopulateHertzOrg(insuredOganisation, insuredDriverData);
                    if (!_OrganisationMatch(insuredOganisation))
                        motorClaim.Organisations.Add(insuredOganisation);
                }
                else
                {
                    PopulateHertzPerson(insuredDriver, insuredDriverData);
                    
                }
            }

            // always add the Insured Driver Record Even if it's Blank
            if (!_PersonMatch(insuredDriver))
                insuredVehicle.People.Add(insuredDriver);
        }

        private void GeneratePartiesInVehicles(IList<ClaimData> partyClaimRows, IList<PipelineVehicle> existingPartyVehicles, PipelineMotorClaim motorClaim)
        {
            foreach (var partyData in partyClaimRows)
            {
                var partyPerson = new PipelinePerson();

                var isPartyOrg = ClaimDataIsOrganisation(partyPerson, partyData);

                if (isPartyOrg)
                {
                    var partyOrganisation = new PipelineOrganisation();

                    PopulateHertzOrg(partyOrganisation, partyData);

                    if (partyOrganisation != null)
                    {
                        // Does this Organisation Already Exist in the Organisation(s)? (Deals with Duplicate Rows)
                        var existing = motorClaim
                            .Organisations
                            .Any(o => o.OrganisationName == partyOrganisation.OrganisationName);

                        if (!existing && !string.IsNullOrEmpty(partyOrganisation.OrganisationName))
                        {
                            if (!_OrganisationMatch(partyOrganisation))
                                motorClaim.Organisations.Add(partyOrganisation);
                        }
                    }
                }
                else
                {
                    PopulateHertzPerson(partyPerson, partyData);

                    // Does this Person Already Exist in the Party Vehicle(s)? (Deals with Duplicate Rows)
                    var existing = existingPartyVehicles
                        .SelectMany(v => v.People)
                        .Any(p => p.FirstName == partyPerson.FirstName &&
                                  p.LastName == partyPerson.LastName &&
                                  p.DateOfBirth == partyPerson.DateOfBirth);

                    if (!existing && !string.IsNullOrEmpty(partyPerson.LastName))
                    {
                        var partyVehicle = FindPartyVehicle(partyPerson, partyClaimRows, existingPartyVehicles);

                        if (!_PersonMatch(partyPerson))
                            partyVehicle.People.Add(partyPerson);
                    }
                }
            }
        }

        /// <summary>
        /// Find the Vehicle that this Person was Related to in the Original Records in case we have > 1 Third Party Vehicle
        /// </summary>
        private PipelineVehicle FindPartyVehicle(PipelinePerson pipelinePerson, IList<ClaimData> personClaimInfo, IList<PipelineVehicle> existingPartyVehicles)
        {
            // This will Cover the Vast Majority of Scenarios Including Those Where we Generated a Blank 3rd Party Vehicle
            var nonBlankVehicles = existingPartyVehicles
                .Where(v => !string.IsNullOrEmpty(v.VehicleRegistration))
                .ToList();

            if (nonBlankVehicles.Count() == 1)
                return nonBlankVehicles.First();

            if (existingPartyVehicles.Count() == 1)
                return existingPartyVehicles.First();

            // More than One Vehicle for the Party Type - Is the Person's Data Row Linked to a Vehicle? If So put them in the One
            // NOTE : The Registration Details Tend to be on Only One of any Duplicate Record(s)
            var personRows = personClaimInfo
                .Where(p =>  
                    p.Surname != null &&
                    p.Surname.Contains(pipelinePerson.LastName) &&
                    (
                        p.Surname.Contains(pipelinePerson.FirstName) ||
                        (p.Firstname ?? string.Empty).Contains(pipelinePerson.FirstName)
                    ) &&
                    p.BirthDate == pipelinePerson.DateOfBirth)
                .ToList();

            PipelineVehicle vehicleInSameRow = null;

            if (personRows.Any())
            {
                vehicleInSameRow = existingPartyVehicles
                    .FirstOrDefault(v =>
                        personRows.Any(p => 
                            !string.IsNullOrEmpty(p.VehicleRegistration) &&
                            p.VehicleRegistration == v.VehicleRegistration)
                    );
            }

            if (vehicleInSameRow != null)
                return vehicleInSameRow;

            // Assign the Person to a Blank Vehicle if there's More than One in the Data 
            // and it's not linked to the Same row as the Person (this Should be Created Already)
            var defaultVehicle = existingPartyVehicles.FirstOrDefault(v => string.IsNullOrEmpty(v.VehicleRegistration));

            return defaultVehicle;
        }

        /// <summary>
        /// If there are No Parties or Party Vehicles Do Nothing
        /// If we have Party Vehicles then Generate them (we will match up the People to these Later)
        /// If we have Parties but No Vehicles Create a Blank Record to Hold all Party People
        /// </summary>
        /// <returns></returns>
        private List<PipelineVehicle> CreateOptionalPartyVehicleRecords(IList<ClaimData> partyPersonData, IList<ClaimData> partyVehicleData, Incident2VehicleLinkType incident2VehicleLinkType)
        {
            var pipelineVehicles = new List<PipelineVehicle>();

            if (!partyPersonData.Any() && !partyVehicleData.Any())
            {
                return pipelineVehicles; // If there are No Parties or Party Vehicles Do Nothing
            }

            // If we have Party Vehicles then Generate them (we will match up the People to these Later)
            foreach (var partyVehicle in partyVehicleData)
            {
                var existing = pipelineVehicles
                    .FirstOrDefault(v => v.VehicleRegistration == partyVehicle.VehicleRegistration);

                if (existing == null)
                {
                    pipelineVehicles.Add(
                        new PipelineVehicle
                        {
                            VehicleMake = partyVehicle.VehicleMake,
                            VehicleModel = partyVehicle.VehicleModel,
                            VehicleRegistration = partyVehicle.VehicleRegistration,
                            VIN = partyVehicle.VinNumber,
                            VehicleColour_Id = (int)partyVehicle.VehicleColour.DeriveVehicleColour(),

                            I2V_LinkData = new PipelineIncident2VehicleLink
                            {
                                Incident2VehicleLinkType_Id = (int)incident2VehicleLinkType,
                            },
                        });
                }
            }

            // In the Case of Insured Vehicles we have a Default Added for the Insured Driver so Don't Duplicate this One With Another Blank
            var existingInsuredVehicle = incident2VehicleLinkType == Incident2VehicleLinkType.InsuredVehicle;
            if (existingInsuredVehicle)
            {
                return pipelineVehicles;
            }

            // If we have 3rd Parties but No Vehicles Create a Blank Record to Hold all Party People
            var peopleButNoVehicles = partyPersonData.Any() && !partyVehicleData.Any();

            // In this Scenario we have more People than Vehicles AND multiple Vehicles so we need a Bin for those not linked
            var multipleVehiclesAndEvenMorePeople = partyVehicleData.Count > 1 && partyPersonData.Count > partyVehicleData.Count;

            if (peopleButNoVehicles || multipleVehiclesAndEvenMorePeople)
            {
                var newBlankVehicle = new PipelineVehicle
                    {   // Blank Record for Party Vehicle to Hold the Party People
                        I2V_LinkData = new PipelineIncident2VehicleLink
                        {
                            Incident2VehicleLinkType_Id = (int)incident2VehicleLinkType,
                        },
                    };

                pipelineVehicles.Add(newBlankVehicle);
            }

            return pipelineVehicles;
        }

        /// <summary>
        /// If we have Renters but no Vehicles Create a Blank Entry for the Insured Vehicle to Hold the Insured Person Record(s)
        /// </summary>
        /// <param name="personClaimInfo"></param>
        /// <returns></returns>
        private PipelineVehicle GenerateInsuredVehicle(IEnumerable<ClaimData> personClaimInfo)
        {
            var insuredVehicleData = personClaimInfo.InsuredVehicleData().FirstOrDefault();
            
            PipelineVehicle insuredVehicle = insuredVehicleData != null
                ? new PipelineVehicle
                {
                    VehicleMake = insuredVehicleData.VehicleMake,
                    VehicleModel = insuredVehicleData.VehicleModel,
                    VehicleRegistration = insuredVehicleData.VehicleRegistration,
                    VIN = insuredVehicleData.VinNumber,
                    VehicleColour_Id = (int)insuredVehicleData.VehicleColour.DeriveVehicleColour(),
                }
                : new PipelineVehicle();

            insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

            return insuredVehicle;
        }

        private void PopulateHertzOrg(PipelineOrganisation pipelineOrganisation, ClaimData orgClaimData)
        {
            if (orgClaimData != null)
            {
                pipelineOrganisation.I2O_LinkData.PartyType_Id = (int)orgClaimData.PartyType;
                pipelineOrganisation.I2O_LinkData.SubPartyType_Id = (int)orgClaimData.SubPartyType;

                if (!string.IsNullOrEmpty(orgClaimData.Surname) || !string.IsNullOrEmpty(orgClaimData.Firstname))
                    pipelineOrganisation.OrganisationName = (orgClaimData.Surname ?? string.Empty) + (orgClaimData.Firstname ?? string.Empty);

                if (!string.IsNullOrEmpty(orgClaimData.EmailAddress))
                    pipelineOrganisation.EmailAddresses.Add(new PipelineEmail { EmailAddress = orgClaimData.EmailAddress });

                if (!string.IsNullOrEmpty(orgClaimData.LandlineTelephone))
                    pipelineOrganisation.Telephones.Add(new PipelineTelephone
                    {
                        ClientSuppliedNumber = orgClaimData.LandlineTelephone,
                        TelephoneType_Id = (int)TelephoneType.Landline
                    });

                if (!string.IsNullOrEmpty(orgClaimData.MobileTelephone))
                    pipelineOrganisation.Telephones.Add(new PipelineTelephone
                    {
                        ClientSuppliedNumber = orgClaimData.MobileTelephone,
                        TelephoneType_Id = (int)TelephoneType.Mobile
                    });

                if (!string.IsNullOrEmpty(orgClaimData.WorkTelephone))
                    pipelineOrganisation.Telephones.Add(new PipelineTelephone
                    {
                        ClientSuppliedNumber = orgClaimData.WorkTelephone,
                        TelephoneType_Id = (int)TelephoneType.Landline
                    });

                var organisationAddress = CreateAddress(orgClaimData);
                if (organisationAddress != null)
                {
                    pipelineOrganisation.Addresses.Add(organisationAddress);
                }

                var orgHireVehicle = CreateHireVehicle(orgClaimData);
                if (orgHireVehicle != null)
                {
                    orgHireVehicle.V2O_LinkData.VehicleLinkType_Id = (int)VehicleLinkType.Hirer;
                    pipelineOrganisation.Vehicles.Add(orgHireVehicle);
                }               
            }
        }

        private void PopulateHertzPerson(PipelinePerson pipelinePerson, ClaimData personClaimData)
        {
            if (personClaimData != null)
            {
                pipelinePerson.I2Pe_LinkData.PartyType_Id = (int)personClaimData.PartyType;
                pipelinePerson.I2Pe_LinkData.SubPartyType_Id = (int)personClaimData.SubPartyType;

                if (personClaimData.BirthDate.HasValue)
                    pipelinePerson.DateOfBirth = personClaimData.BirthDate;

                if (!string.IsNullOrEmpty(personClaimData.Occupation))
                    pipelinePerson.Occupation = personClaimData.Occupation;

                if (!string.IsNullOrEmpty(personClaimData.NationalInsuranceNumber))
                    pipelinePerson.NINumbers.Add(new PipelineNINumber { NINumber1 = personClaimData.NationalInsuranceNumber });

                if (!string.IsNullOrEmpty(personClaimData.DrivingLicenceNumber))
                    pipelinePerson.DrivingLicenseNumbers.Add(new PipelineDrivingLicense { DriverNumber = personClaimData.DrivingLicenceNumber });

                if (!string.IsNullOrEmpty(personClaimData.PassportNumber))
                    pipelinePerson.PassportNumbers.Add(new PipelinePassport { PassportNumber = personClaimData.PassportNumber });

                if (!string.IsNullOrEmpty(personClaimData.EmailAddress))
                    pipelinePerson.EmailAddresses.Add(new PipelineEmail { EmailAddress = personClaimData.EmailAddress });

                if (!string.IsNullOrEmpty(personClaimData.LandlineTelephone))
                    pipelinePerson.Telephones.Add(new PipelineTelephone
                    {
                        ClientSuppliedNumber = personClaimData.LandlineTelephone,
                        TelephoneType_Id = (int)TelephoneType.Landline
                    });

                if (!string.IsNullOrEmpty(personClaimData.MobileTelephone))
                    pipelinePerson.Telephones.Add(new PipelineTelephone
                    {
                        ClientSuppliedNumber = personClaimData.MobileTelephone,
                        TelephoneType_Id = (int)TelephoneType.Mobile
                    });

                if (!string.IsNullOrEmpty(personClaimData.WorkTelephone))
                    pipelinePerson.Telephones.Add(new PipelineTelephone
                    {
                        ClientSuppliedNumber = personClaimData.WorkTelephone,
                        TelephoneType_Id = (int)TelephoneType.Landline
                    });

                var personAddress = CreateAddress(personClaimData);
                if (personAddress != null)
                {
                    pipelinePerson.Addresses.Add(personAddress);
                }

                var personHireVehicle = CreateHireVehicle(personClaimData);
                if (personHireVehicle != null)
                {
                    personHireVehicle.V2O_LinkData.VehicleLinkType_Id = (int)VehicleLinkType.HiredFrom;

                    var hireCompany = new PipelineOrganisation
                    {
                        P2O_LinkData = {Person2OrganisationLinkType_Id = (int) Person2OrganisationLinkType.Hire}
                    };
                    hireCompany.Vehicles.Add(personHireVehicle);

                    pipelinePerson.Organisations.Add(hireCompany);
                }
            }
        }

        private PipelineAddress CreateAddress(ClaimData personClaimData)
        {
            if (personClaimData == null) return null;

            if (personClaimData.HasAddressData)
            {                
                var personAddress = new PipelineAddress();

                if (!string.IsNullOrEmpty(personClaimData.ADDRESS1))
                    personAddress.BuildingNumber = personClaimData.ADDRESS1;

                if (!string.IsNullOrEmpty(personClaimData.ADDRESS2))
                    personAddress.Street = personClaimData.ADDRESS2;

                if (!string.IsNullOrEmpty(personClaimData.POSTCODE))
                    personAddress.PostCode = personClaimData.POSTCODE;

                // check if address is on default data                
                if (!_AddressMatch(personAddress)) return personAddress;                
            }

            return null;
        }

        /// <summary>
        /// This Needs Refactoring Because it's Telling us if we need an Organisation or Not
        /// </summary>
        /// <param name="pipelinePerson"></param>
        /// <param name="personClaimData"></param>
        private bool ClaimDataIsOrganisation(PipelinePerson pipelinePerson, ClaimData personClaimData)
        {
            bool claimDataIsOrganisation = false;

            // try to Parse Salutation and Name from the Surname re: Documentation
            if (personClaimData != null)
            {
                var hasNoNames = personClaimData.HasNoNames; // Phil Wants Empty Entries to be People

                var canParseSalutation = personClaimData.Salutation != Salutation.Unknown;
                
                var canMatchPersonName = personClaimData.MatchNameToPerson(pipelinePerson);

                var hasOrgIndicator = personClaimData.HasOrganisationIndicator;

                claimDataIsOrganisation = !hasNoNames && !canParseSalutation && !canMatchPersonName || hasOrgIndicator;
            }

            return claimDataIsOrganisation;
        }

        private PipelineOrganisationVehicle CreateHireVehicle(ClaimData claimData)
        {
            if (claimData == null || !claimData.HasHireVehicleData)
                return null;

            var pipelineOrganisationVehicle = new PipelineOrganisationVehicle();

            if (!string.IsNullOrEmpty(claimData.HireVehicleRegistration))
                pipelineOrganisationVehicle.VehicleRegistration = claimData.HireVehicleRegistration;

            if (!string.IsNullOrEmpty(claimData.HireVehicleMake))
                pipelineOrganisationVehicle.VehicleMake = claimData.HireVehicleMake;

            if (!string.IsNullOrEmpty(claimData.HireVehicleModel))
                pipelineOrganisationVehicle.VehicleModel = claimData.HireVehicleModel;

            if (claimData.HireStartDate.HasValue)
                pipelineOrganisationVehicle.V2O_LinkData.HireStartDate = claimData.HireStartDate;

            if (claimData.HireEndDate.HasValue)
                pipelineOrganisationVehicle.V2O_LinkData.HireEndDate = claimData.HireEndDate;

            return pipelineOrganisationVehicle;
        }

        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }

        public List<PipelineAddress> DefaultAddresses(List<string> defaultAddressesForClient)
        {
            var defaultAddresses = new List<PipelineAddress>();

              if (defaultAddressesForClient != null)
                  foreach (var str in defaultAddressesForClient)
                  {
                      if (!string.IsNullOrEmpty(str))
                      {
                          var address = new PipelineAddress();

                          var match = Regex.Matches(str, @"\[(.*?)\]");

                          //SB=[],BN=[],BLD=[],ST=[],LOC=[],TWN=[],CNTY=[],PC=[]

                          address.SubBuilding = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                          address.BuildingNumber = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                          address.Building = match[2].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                          address.Street = match[3].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                          address.Locality = match[4].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                          address.Town = match[5].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                          address.County = match[6].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                          address.PostCode = match[7].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                          defaultAddresses.Add(address);
                      }
                  }

              return defaultAddresses;
        }

        private bool _AddressMatch(PipelineAddress address)
        {

            if (address == null) return false;

            var defaultAddresses = DefaultAddresses(DefaultAddressesForClient);

            if (defaultAddresses != null)
                foreach (var adr in defaultAddresses)
                {
                    var match = false;

                    if (adr != null)
                    {
                    if (!string.IsNullOrEmpty(adr.Street) && !string.IsNullOrEmpty(address.Street))
                    {
                        if (address.Street.Equals(adr.Street, StringComparison.InvariantCultureIgnoreCase))
                        {
                            match = true;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    if (!string.IsNullOrEmpty(adr.Town) && !string.IsNullOrEmpty(address.Town))
                    {
                        if (address.Town.Equals(adr.Town, StringComparison.InvariantCultureIgnoreCase))
                        {
                            match = true;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    if (!string.IsNullOrEmpty(adr.County) && !string.IsNullOrEmpty(address.County))
                    {
                        if (address.County.Equals(adr.County, StringComparison.InvariantCultureIgnoreCase))
                        {
                            match = true;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    if (!string.IsNullOrEmpty(adr.PostCode) && !string.IsNullOrEmpty(address.PostCode))
                    {
                        if (address.PostCode.Equals(adr.PostCode, StringComparison.InvariantCultureIgnoreCase))
                        {
                            match = true;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    return match;
                }
                }
            return false;
        }

        public List<PipelinePerson> DefaultPeople(List<string> defaultPeopleForClient)
        {
             var defaultPeople = new List<PipelinePerson>();
             if (defaultPeopleForClient == null) return defaultPeople;
             

            foreach (var str in defaultPeopleForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelinePerson person = new PipelinePerson();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //FN=[],LN=[]

                    person.FirstName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    person.LastName = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultPeople.Add(person);
                }
            }

            return defaultPeople;
        }

        public List<PipelineOrganisation> DefaultOrganisation(List<string> defaultOrganisationsForClient)
        {            
            var defaultOrganisations = new List<PipelineOrganisation>();
            if (defaultOrganisationsForClient == null) return defaultOrganisations;

            foreach (var orgName in defaultOrganisationsForClient)
            {
                if (!string.IsNullOrEmpty(orgName))
                {
                    PipelineOrganisation organisation = new PipelineOrganisation();

                    MatchCollection match = Regex.Matches(orgName, @"\[(.*?)\]");

                    //ON=[]

                    organisation.OrganisationName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultOrganisations.Add(organisation);
                }
            }

            return defaultOrganisations;
        }

        private bool _PersonMatch(PipelinePerson person)
        {
             if (person == null) return false;

             var people = DefaultPeople(DefaultPeopleForClient);

             if (people != null)
                 foreach (var p in people)
                 {
                     if (p != null)
                     {
                         if (person.LastName != null && 
                            (person.FirstName != null && 
                            (person.FirstName.Equals(p.FirstName, StringComparison.InvariantCultureIgnoreCase) &&
                             person.LastName.Equals(p.LastName, StringComparison.InvariantCultureIgnoreCase))))
                         {
                             return true;
                         }
                     }
                 }

             return false;
        }

        private bool _OrganisationMatch(PipelineOrganisation organisation)
        {
            if (organisation == null) return false;
            var defaultOrganisations = DefaultOrganisation(DefaultOrganisationsForClient);

            if (defaultOrganisations != null)
                foreach (var o in defaultOrganisations)
                {
                    if (o != null && 
                       (organisation.OrganisationName != null && 
                        organisation.OrganisationName.Equals(o.OrganisationName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        return true;
                    }
                }

            return false;
        }
    }
}