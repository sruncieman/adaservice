﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.MappingService.Impl.Hertz.Motor.Model;
using MDA.Pipeline.Model;

namespace MDA.MappingService.Impl.Hertz.Motor
{
    public static class ClaimDataExtensions
    {
        #region Derived Functionality

        //private static readonly string[] OrganisationIndicators = { "LTD", "LIMITED", "PLC", "LLP" };

        //private static readonly string[] InsuredKeys = { "renter", "first party" };

        //private static readonly string[] DriverKeys = { "renter", "driver", "cyclist", "animal owner" };

        //private static readonly string[] PassengerKeys = { "passenger" };

        //public static bool HasVehicleData(this ClaimData claimData)
        //{
        //    return !string.IsNullOrEmpty(claimData.VehicleRegistration);
        //}

        //public static bool HasHireVehicleData(this ClaimData claimData)
        //{
        //    return
        //        !string.IsNullOrEmpty(claimData.HireVehicleRegistration) ||
        //        !string.IsNullOrEmpty(claimData.HireVehicleMake) ||
        //        !string.IsNullOrEmpty(claimData.HireVehicleModel) ||
        //        claimData.HireStartDate.HasValue ||
        //        claimData.HireEndDate.HasValue;
        //}

        //public static bool HasPersonData(this ClaimData claimData)
        //{
        //    return !string.IsNullOrEmpty(claimData.Involvement);
        //}

        //public static bool HasAddressData(this ClaimData claimData)
        //{
        //    return
        //        !string.IsNullOrEmpty(claimData.ADDRESS1) ||
        //        !string.IsNullOrEmpty(claimData.ADDRESS2) ||
        //        !string.IsNullOrEmpty(claimData.POSTCODE);
        //}
        
        //public static List<string> SurnameSplit(this ClaimData claimData)
        //{
        //    List<string> surnameSplit;

        //    if (string.IsNullOrEmpty(claimData.Surname))
        //    {
        //        surnameSplit = new List<string>();
        //    }
        //    else
        //    {
        //        var names = claimData.Surname.Split(' ').ToList();

        //        names.RemoveAll(n => string.IsNullOrEmpty(n) || n.Trim(' ') == string.Empty);

        //        surnameSplit = names;
        //    }

        //    return surnameSplit;
        //}
        
        //public static Salutation Salutation(this ClaimData claimData)
        //{
        //    Salutation? salutation = null;

        //    foreach (var possibleSalutation in claimData.SurnameSplit())
        //    {
        //        if (!string.IsNullOrEmpty(possibleSalutation))
        //        {
        //            var salutationAttempt = SalutationHelper.GetSalutation(possibleSalutation);
        //            if (salutationAttempt != Common.Enum.Salutation.Unknown)
        //            {
        //                salutation = salutationAttempt;
        //            }
        //        }
        //    }

        //    // Still Not Loaded? Try to get the Salutation from the csv Title field
        //    if (!salutation.HasValue && !string.IsNullOrEmpty(claimData.Title.Trim(' ')))
        //    {
        //        Salutation salutationFromTitle = SalutationHelper.GetSalutation(claimData.Title);
        //        if (salutationFromTitle != Common.Enum.Salutation.Unknown)
        //        {
        //            salutation = salutationFromTitle;
        //        }
        //    }

        //    if (!salutation.HasValue)
        //    {
        //        salutation = Common.Enum.Salutation.Unknown;
        //    }

        //    return salutation.Value;
        //}

        //public static bool HasOrganisationIndicator(this ClaimData claimData)
        //{
        //    var surnameSplit = claimData.SurnameSplit();

        //    if (surnameSplit == null || !surnameSplit.Any())
        //        return false;

        //    return
        //        OrganisationIndicators.Contains(surnameSplit.Last().ToUpper());
        //}
        
        //public static Incident2VehicleLinkType VehicleLinkType(this ClaimData claimData)
        //{
        //    Incident2VehicleLinkType? vehicleLinkType;

        //    if (string.IsNullOrEmpty(claimData.Involvement))
        //    {
        //        vehicleLinkType = Incident2VehicleLinkType.Unknown;
        //    }
        //    else
        //    {
        //        var isInsured = InsuredKeys.Any(term => claimData.Involvement.ToLower().Contains(term));

        //        vehicleLinkType =
        //            isInsured
        //            ? Incident2VehicleLinkType.InsuredVehicle
        //            : Incident2VehicleLinkType.ThirdPartyVehicle;
        //    }

        //    return vehicleLinkType.Value;
        //}
        
        //public static PartyType PartyType(this ClaimData claimData)
        //{
        //    PartyType? partyType;

        //    if (string.IsNullOrEmpty(claimData.Involvement))
        //    {
        //        partyType = Common.Enum.PartyType.Unknown;
        //    }
        //    else
        //    {
        //        var isInsured = InsuredKeys.Any(term => claimData.Involvement.ToLower().Contains(term));

        //        partyType = isInsured
        //            ? Common.Enum.PartyType.Insured
        //            : Common.Enum.PartyType.ThirdParty;
        //    }

        //    return partyType.Value;
        //}
        
        //public static SubPartyType SubPartyType(this ClaimData claimData)
        //{
        //    SubPartyType? subPartyType;

        //    if (string.IsNullOrEmpty(claimData.Involvement))
        //    {
        //        subPartyType = Common.Enum.SubPartyType.Unknown;
        //    }
        //    else
        //    {
        //        var isDriver = DriverKeys.Any(term => claimData.Involvement.ToLower().Contains(term));
        //        var isPassenger = PassengerKeys.Any(term => claimData.Involvement.ToLower().Contains(term));

        //        if (isDriver)
        //            subPartyType = Common.Enum.SubPartyType.Driver;
        //        else if (isPassenger)
        //            subPartyType = Common.Enum.SubPartyType.Passenger;
        //        else
        //            subPartyType = Common.Enum.SubPartyType.Unknown;
        //    }

        //    return subPartyType.Value;
        //}

        #endregion

        public static IList<ClaimData> InsuredVehicleData(this IEnumerable<ClaimData> claimData)
        {
            if (claimData == null) return null;

            var insuredVehicleData = claimData
                .Where(p => p != null && p.VehicleLinkType == Incident2VehicleLinkType.InsuredVehicle && p.HasVehicleData)
                .ToList();

            return insuredVehicleData;
        }

        public static IList<ClaimData> InsuredPartyPersonData(this IEnumerable<ClaimData> claimData)
        {
            if (claimData == null) return null;

            var otherInsuredPartyPersonData = claimData
                .Where(p => p != null && (p.PartyType == PartyType.Insured &&
                                          p.SubPartyType == SubPartyType.Driver &&
                                          p.HasPersonData))
                .ToList();

            return otherInsuredPartyPersonData;
        }

        public static IList<ClaimData> OtherInsuredPartyPersonData(this IEnumerable<ClaimData> claimData)
        {
            if (claimData == null) return null;

            var otherInsuredPartyPersonData = claimData
                .Where(p => p != null && (p.PartyType == PartyType.Insured &&
                                          p.SubPartyType != SubPartyType.Driver &&
                                          p.HasPersonData))
                .ToList();

            return otherInsuredPartyPersonData;
        }

        public static IList<ClaimData> ThirdPartyVehicleData(this IEnumerable<ClaimData> claimData)
        {
            if (claimData == null) return null;

            var thirdPartyVehicleData = claimData
                .Where(p => p != null && (p.VehicleLinkType == Incident2VehicleLinkType.ThirdPartyVehicle &&
                                          p.HasVehicleData))
                .ToList();

            return thirdPartyVehicleData;
        }

        public static IList<ClaimData> ThirdPartyPersonData(this IEnumerable<ClaimData> claimData)
        {
            if (claimData == null) return null;
            var thirdPartyPersonData = claimData
                .Where(p => p != null && (p.PartyType == PartyType.ThirdParty &&
                                          p.HasPersonData))
                .ToList();

            return thirdPartyPersonData;
        }

        public static bool MatchNameToPerson(this ClaimData claimData, PipelinePerson pipelinePerson)
        {
            if (claimData == null || pipelinePerson == null)
                return false;

            if (!string.IsNullOrEmpty(claimData.Firstname) &&
                !string.IsNullOrEmpty(claimData.Surname))
            {
                pipelinePerson.FirstName = claimData.Firstname;
                pipelinePerson.LastName = claimData.Surname;

                if (!string.IsNullOrEmpty(claimData.MiddleName))
                    pipelinePerson.MiddleName = claimData.MiddleName;

                return true;
            }

            var salutation = claimData.Salutation;

            pipelinePerson.Salutation_Id = (int)salutation;
            pipelinePerson.Gender_Id = (int)GenderHelper.Salutation2Gender(salutation);

            if (salutation == Salutation.Unknown) return !string.IsNullOrEmpty(pipelinePerson.LastName);

            var surnameSplit = claimData.SurnameSplit;
            if (surnameSplit == null) return !string.IsNullOrEmpty(pipelinePerson.LastName);

            var surnameWithoutSalutation = new List<string>(surnameSplit);
            surnameWithoutSalutation
                .RemoveAll(s =>
                    string.IsNullOrEmpty(s) ||
                    string.Equals(s, salutation.ToString(), StringComparison.InvariantCultureIgnoreCase));

            if (surnameWithoutSalutation.Count == 3)
            {
                pipelinePerson.FirstName = surnameSplit[2];
                pipelinePerson.MiddleName = surnameSplit[1];
                pipelinePerson.LastName = surnameSplit[0];
            }

            if (surnameWithoutSalutation.Count == 2)
            {
                pipelinePerson.FirstName = surnameSplit[1];
                pipelinePerson.LastName = surnameSplit[0];
            }

            if (surnameWithoutSalutation.Count == 1)
            {
                pipelinePerson.LastName = surnameSplit[0];
            }

            return !string.IsNullOrEmpty(pipelinePerson.LastName);
        }
    }
}
