﻿using MDA.Common;
using MDA.Common.Server;
using MDA.MappingService.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace MDA.MappingService.Impl.Hertz.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];
        public void ConvertToClaimBatch(CurrentContext ctx, System.IO.Stream fs, object statusTracking, Func<Common.Server.CurrentContext, Pipeline.Model.IPipelineClaim, object, int> processClaim, out Common.ProcessingResults processingResults)
        {
            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            var defaultDataAddressForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataAddressForClient(ctx.RiskClientId);
            var defaultDataPeopleForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataPeopleForClient(ctx.RiskClientId);
            var defaultDataOrganisationsForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataOrganisationsForClient(ctx.RiskClientId);


            try
            {
                var mapper = new Mapper(fs, clientFolder, ctx, processClaim, statusTracking, defaultDataAddressForClient, defaultDataPeopleForClient, defaultDataOrganisationsForClient);

                mapper.AssignFiles();
                mapper.InitialiseFileHelperEngines();
                mapper.PopulateFileHelperEngines();
                mapper.RetrieveDistinctClaims();
                mapper.Translate();
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Hertz file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }
        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = _tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }

        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard, int settleTime, int batchSize, string postOp, string archiveFolder, Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {
            // Create structure to hold our new batch of claims
            List<object> batch = new List<object>();

            int batchCount = 0;
            string firstFileName = null;

            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            string[] files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (files.Count() == 0) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Where(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now).Any())
                return;

            // If we get here we have files to process
            foreach (var file in files)
            {
                // Create a filename to use for this batch.  We will just use the first file name in this case
                if (firstFileName == null)
                    firstFileName = Path.GetFileName(file);

                // Add this new claim to the batch
                batch.Add(file);

                #region If Batch full process it

                if (batch.Count >= batchSize)
                {
                    batchCount++;

                    string fileName = firstFileName;
                    string mimeType = "csv";

                    string clientName = "HERTZ";
                    string date;


                    date = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
                    date = date.Replace("/", "");
                    date = date.Replace(":", "");
                    date = date.Replace(" ", "");

                    string batchRef = clientName + date;

                    using (FileStream fileStream = File.OpenRead(file))
                    {
                        MemoryStream stream = new MemoryStream();
                        stream.SetLength(fileStream.Length);
                        fileStream.Read(stream.GetBuffer(), 0, (int)fileStream.Length);

                        #region Process the batch (call the callback function)
                        processClientBatch(ctx, stream, fileName, mimeType, "Motor", batchRef);
                        #endregion
                    }

                    // Setup for a new batch and go round the loop again
                    batch.Clear();
                    firstFileName = null;
                }

                #endregion

                #region Process all the options we invented for our postOp functionality

                // We are passed the string we entered into config.  Now we have to code it!
                // In this case we have implemented RENAME, DELETE, ARCHIVE
                // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                switch (postOp.ToUpper())
                {
                    case "RENAME":
                        File.Move(file, file + ".old");
                        break;

                    case "DELETE":
                        File.Delete(file);
                        break;

                    case "ARCHIVE":
                        if (!Directory.Exists(archiveFolder))
                            Directory.CreateDirectory(archiveFolder);   // Create destination if needed

                        string fname = archiveFolder + "\\" + Path.GetFileName(file);  // build dest path

                        if (File.Exists(fname))  // Check if it already exists
                        {
                            fname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                        }

                        File.Move(file, fname);
                        break;
                }
                #endregion

            }

        }
    }


}
