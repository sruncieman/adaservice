﻿using FileHelpers;
using Ionic.Zip;
using MDA.Common.Server;
using MDA.MappingService.KeoghsCH.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common;
using System.Text.RegularExpressions;

namespace MDA.MappingService.KeoghsCH.Motor
{
    public class GetMotorClaimBatch
    {

        private static bool boolDebug = false;
        private static string filePath;
        private static string folderPath;

        public List<string> defaultAddressesForClient { get; set; }
        public List<string> defaultPeopleForClient { get; set; }
        public List<string> defaultOrganisationsForClient { get; set; }

        internal void RetrieveMotorClaimBatch(Common.Server.CurrentContext ctx, System.IO.Stream filem, string clientFolder, bool DeleteUploadedFiles, object statusTracking, Func<Common.Server.CurrentContext, Pipeline.Model.IPipelineClaim, object, int> processClaim, Common.ProcessingResults processingResults, List<string> defaultAddressesForClient, List<string> defaultPeopleForClient, List<string> defaultOrganisationsForClient)
        {
            #region Initialise variables
            string ClaimFile = string.Empty;
            string OrganisationFile = string.Empty;
            string PersonFile = string.Empty;
            string VehicleFile = string.Empty;
            string fileDateString = string.Empty;

            Claim[] KeoghsCH_Claim;
            Organisation[] KeoghsCH_Organisation;
            Person[] KeoghsCH_Person;
            Vehicle[] KeoghsCH_Vehicle;

            this.defaultAddressesForClient = defaultAddressesForClient;
            this.defaultPeopleForClient = defaultPeopleForClient;
            this.defaultOrganisationsForClient = defaultOrganisationsForClient;
            #endregion

            #region Extract Files from Zip
            if (filem != null)
            {
                try
                {
                    boolDebug = false;

                    using (ZipFile zip1 = ZipFile.Read(filem)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(clientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);

                            string unpackDirectory = clientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

                            fileDateString = e.FileName;

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                        ClaimFile = clientFolder + @"\Claim.csv";
                        OrganisationFile = clientFolder + @"\Organisation.csv";
                        PersonFile = clientFolder + @"\Person.csv";
                        VehicleFile = clientFolder + @"\Vehicle.csv";
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in extraction of files: " + ex);
                }
            }
            else
            {
                boolDebug = true;
                folderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Keoghs Credit Hire\Resource\";
                ClaimFile = folderPath + @"\Claim.csv";
                OrganisationFile = folderPath + @"\Organisation.csv";
                PersonFile = folderPath + @"\Person.csv";
                VehicleFile = folderPath + @"\Vehicle.csv";
            }
            #endregion

            DeleteExtractedFiles(clientFolder, DeleteUploadedFiles);

            #region FileHelper Engines
            try
            {
                FileHelperEngine KeoghsCHClaimEngine = new FileHelperEngine(typeof(Claim));
                FileHelperEngine KeoghsCHOrganisationEngine = new FileHelperEngine(typeof(Organisation));
                FileHelperEngine KeoghsCHPersonEngine = new FileHelperEngine(typeof(Person));
                FileHelperEngine KeoghsCHVehicleEngine = new FileHelperEngine(typeof(Vehicle));

                #region Populate FileHelper Engines with data from files
                KeoghsCH_Claim = KeoghsCHClaimEngine.ReadFile(ClaimFile) as Claim[];
                KeoghsCH_Organisation = KeoghsCHOrganisationEngine.ReadFile(OrganisationFile) as Organisation[];
                KeoghsCH_Person = KeoghsCHPersonEngine.ReadFile(PersonFile) as Person[];
                KeoghsCH_Vehicle = KeoghsCHVehicleEngine.ReadFile(VehicleFile) as Vehicle[];
                #endregion

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising FileHelper Engines: " + ex);
            }

            #endregion

            #region Get Distinct Claims List
            List<Claim> lstClaims = new List<Claim>();

            if (KeoghsCH_Claim != null)
                lstClaims = KeoghsCH_Claim
                    .GroupBy(i => i.MatterNum)
                    .Select(g => g.First())
                    .ToList();
            #endregion

            #region Translate to XML

            foreach (var claim in lstClaims)
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                #region CLAIM

                motorClaim.ClaimNumber = claim.MatterNum.ToString();
                motorClaim.IncidentDate = Convert.ToDateTime(claim.IncidentDt);
                motorClaim.ClaimType_Id = (int)ClaimType.Motor;

                #endregion

                #region CLAIM INFO

                #region ClaimStatus
                switch (claim.ClaimStatus.ToUpper())
                {
                    case "NULL":
                    case "":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                        break;
                    case "NEGOTIATED SETTLEMENT":
                    case "LITIGATED":
                    case "TRANSFER TO COMPLEX CREDIT HIRE TEAM":
                    case "TRANSFER TO FRAUD UNIT":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                        break;
                    case "DISCONTINUED":
                    case "PASS TO ALTERNATIVE SUPPLIER - ALREADY INSTRUCTED":
                    case "PASS TO ALTERNATIVE SUPPLER - ALREADY INSTRUCTED":
                    case "TRANSFER TO ALTERNATIVE SUPPLIER AS NEW INSTRUCTIONS":
                    case "CAPTURED & RETURNED TO INSURER":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Withdrawn;
                        break;
                    case "REOPENED":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                        break;
                    case "DELETE":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Delete;
                        break;
                    default:
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                        break;
                }
                #endregion

                motorClaim.ExtraClaimInfo.ClaimCode = claim.InsurerClaimRef.ToString();
                motorClaim.ExtraClaimInfo.IncidentCircumstances = claim.IncidentCircumstances;

                #endregion

                #region POLICY

                motorClaim.Policy.Insurer = claim.Insurer;
                //motorClaim.Policy.PolicyType_Id = (int)PolicyType.PersonalMotor;

                #endregion

                #region CLAIM LEVEL ORGANISATION

                var organisationList = KeoghsCH_Organisation.Where(x => x.MatterNum == claim.MatterNum).ToList();

                var vehiclesList = KeoghsCH_Vehicle.Where(x => x.MatterNum == claim.MatterNum).ToList();

                foreach (var claimOrgItem in organisationList)
                {
                    if (claimOrgItem.PersonId == null)
                    {
                        PipelineOrganisation claimOrg = new PipelineOrganisation();


                #endregion

                        if (!string.IsNullOrEmpty(claimOrgItem.CompanyName))
                        {
                            claimOrg.OrganisationName = claimOrgItem.CompanyName;

                            bool ignoreOrganisationFlag = _OrganisationMatch(claimOrg);

                            if (ignoreOrganisationFlag)
                            {
                                claimOrg.OrganisationName = "";
                            }
                        }

                        if (!string.IsNullOrEmpty(claimOrgItem.Telephone))
                        {
                            PipelineTelephone telephone = new PipelineTelephone();

                            telephone.ClientSuppliedNumber = claimOrgItem.Telephone;

                            claimOrg.Telephones.Add(telephone);
                        }

                        if (!string.IsNullOrEmpty(claimOrgItem.Address) || !string.IsNullOrEmpty(claimOrgItem.Postcode))
                        {
                            PipelineAddress address = new PipelineAddress();
                            address.AddressType_Id = (int)AddressLinkType.TradingAddress;
                            address.Street = claimOrgItem.Address;
                            address.PostCode = claimOrgItem.Postcode;

                            bool ignoreAddressFlag = _AddressMatch(address);

                            if (!ignoreAddressFlag)
                                claimOrg.Addresses.Add(address);

                        }

                        #region ORGANISATION VEHICLE

                        foreach (var hvItem in vehiclesList)
                        {

                            if (hvItem.OrganisationId == claimOrgItem.OrganisationId)
                            {
                                PipelineOrganisationVehicle orgVehicle = new PipelineOrganisationVehicle();

                                orgVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyHireVehicle;

                                if (!string.IsNullOrEmpty(hvItem.VehicleRegistration))
                                {
                                    orgVehicle.VehicleRegistration = hvItem.VehicleRegistration;
                                }
                                if (!string.IsNullOrEmpty(hvItem.VehicleMake))
                                {
                                    orgVehicle.VehicleMake = hvItem.VehicleMake;
                                }
                                if (!string.IsNullOrEmpty(hvItem.VehicleModel))
                                {
                                    orgVehicle.VehicleModel = hvItem.VehicleModel;
                                }
                                if (hvItem.DtHireStarted != null)
                                {
                                    orgVehicle.V2O_LinkData.HireStartDate = hvItem.DtHireStarted;
                                }
                                if (hvItem.DtHireEnded != null)
                                {
                                    orgVehicle.V2O_LinkData.HireEndDate = hvItem.DtHireEnded;
                                }

                                claimOrg.Vehicles.Add(orgVehicle);
                            }
                        }
                        #endregion

                        motorClaim.Organisations.Add(claimOrg);

                    }
                }

            #endregion

                #region VEHICLE

                foreach (var vItem in vehiclesList)
                {

                    if (vItem.VehicleInvolvement.ToUpper() != "HIREVEHICLE")
                    {

                        PipelineVehicle vehicle = new PipelineVehicle();

                        switch (vItem.VehicleInvolvement.ToUpper())
                        {
                            case "INSUREDVEHICLE":
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                                break;
                            case "THIRDPARTYVEHICLE":
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                                break;
                            case "HIREVEHICLE":
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyHireVehicle;
                                break;
                            case "NOVEHICLEINVOLVED":
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                                break;
                            default:
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.Unknown;
                                break;
                        }

                        if (!string.IsNullOrEmpty(vItem.VehicleRegistration))
                        {
                            vehicle.VehicleRegistration = vItem.VehicleRegistration;
                        }
                        if (!string.IsNullOrEmpty(vItem.VehicleMake))
                        {
                            vehicle.VehicleMake = vItem.VehicleMake;
                        }
                        if (!string.IsNullOrEmpty(vItem.VehicleModel))
                        {
                            vehicle.VehicleModel = vItem.VehicleModel;
                        }



                        motorClaim.Vehicles.Add(vehicle);

                #endregion

                        #region PERSON

                        var peopleList = KeoghsCH_Person.Where(x => x.VehicleId == vItem.VehicleId && x.MatterNum == vItem.MatterNum).ToList();

                        var noInsuredDriverCount = KeoghsCH_Person.Where(x => x.VehicleId == vItem.VehicleId && x.PersonInvolvement.ToUpper() == "INSUREDDRIVER").Count();

                        foreach (var pItem in peopleList)
                        {

                            PipelinePerson person = new PipelinePerson();

                            #region PersonInvolvement

                            int insuredDriverCount = 0;

                            switch (pItem.PersonInvolvement.ToUpper())
                            {
                                case "INSURED":
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = PersonInvolvementSubPartyType(noInsuredDriverCount, insuredDriverCount);
                                    break;
                                case "INSUREDDRIVER":
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                    break;
                                case "CLAIMANT":
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    break;
                                default:
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    break;
                            }

                            if (PersonInvolvementSubPartyType(noInsuredDriverCount, insuredDriverCount) == (int)SubPartyType.Driver)
                            {
                                insuredDriverCount++;
                            }

                            #endregion

                            if (!string.IsNullOrEmpty(pItem.Salutation))
                            {
                                person.Salutation_Id = (int)SalutationHelper.GetSalutation(pItem.Salutation);
                            }

                            if (!string.IsNullOrEmpty(pItem.FirstName))
                            {
                                person.FirstName = pItem.FirstName;
                            }
                            if (!string.IsNullOrEmpty(pItem.LastName))
                            {
                                person.LastName = pItem.LastName;
                            }
                            if (pItem.Dob != null)
                            {
                                person.DateOfBirth = pItem.Dob;
                            }

                            if (!string.IsNullOrEmpty(pItem.Gender))
                            {
                                person.Gender_Id = (int)GenderHelper.Salutation2Gender(pItem.Salutation.ToLower());
                            }

                            #region NINUMBER

                            if (!string.IsNullOrEmpty(pItem.NiNumber))
                            {
                                PipelineNINumber nInumber = new PipelineNINumber();

                                nInumber.NINumber1 = pItem.NiNumber;

                                person.NINumbers.Add(nInumber);
                            }

                            #endregion

                            #region TELEPHONES

                            if (!string.IsNullOrEmpty(pItem.LandlineTel))
                            {
                                PipelineTelephone landLine = new PipelineTelephone();

                                landLine.ClientSuppliedNumber = pItem.LandlineTel;
                                landLine.TelephoneType_Id = (int)TelephoneType.Landline;

                                person.Telephones.Add(landLine);
                            }

                            if (!string.IsNullOrEmpty(pItem.MobileTel))
                            {
                                PipelineTelephone mobile = new PipelineTelephone();

                                mobile.ClientSuppliedNumber = pItem.MobileTel;
                                mobile.TelephoneType_Id = (int)TelephoneType.Mobile;

                                person.Telephones.Add(mobile);
                            }

                            #endregion

                            #region ADDRESS

                            if (!string.IsNullOrEmpty(pItem.Address) || !string.IsNullOrEmpty(pItem.Postcode))
                            {
                                PipelineAddress address = new PipelineAddress();

                                address.Street = pItem.Address;
                                address.PostCode = pItem.Postcode;

                                address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                                bool ignoreAddressFlag = _AddressMatch(address);

                                if (!ignoreAddressFlag)
                                    person.Addresses.Add(address);

                            }

                            #endregion

                            #region Organisation

                            foreach (var orgItem in organisationList)
                            {
                                if (orgItem.PersonId == pItem.PersonId)
                                {
                                    PipelineOrganisation personOrg = new PipelineOrganisation();

                                    #region OrgInvolvement
                                    switch (orgItem.OrgInvolvement.ToUpper())
                                    {
                                        case "CHO":
                                            personOrg.OrganisationType_Id = (int)OrganisationType.CreditHire;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire;
                                            break;
                                        case "CLAIMANTSOLICITOR":
                                            personOrg.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                            break;
                                        case "CLAIMANTINSURER":
                                            personOrg.OrganisationType_Id = (int)OrganisationType.Insurer;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                            break;
                                        case "CLAIMANTBROKER":
                                            personOrg.OrganisationType_Id = (int)OrganisationType.Broker;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Broker;
                                            break;
                                        case "CLAIMANTALTERNATIVEREP.":
                                            personOrg.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                            break;
                                        case "REPAIRER":
                                            personOrg.OrganisationType_Id = (int)OrganisationType.Repairer;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer;
                                            break;
                                        case "GENERALPARTY":
                                            personOrg.OrganisationType_Id = (int)OrganisationType.Unknown;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                            break;
                                        case "ENGINEER":
                                            personOrg.OrganisationType_Id = (int)OrganisationType.VehicleEngineer;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Engineer;
                                            break;
                                        case "RECOVERYSALVAGEAGENT":
                                            personOrg.OrganisationType_Id = (int)OrganisationType.Recovery;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Recovery;
                                            break;
                                        default:
                                            personOrg.OrganisationType_Id = (int)OrganisationType.Unknown;
                                            personOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                            break;
                                    }
                                    #endregion

                                    if (!string.IsNullOrEmpty(orgItem.CompanyName))
                                    {
                                        personOrg.OrganisationName = orgItem.CompanyName;

                                        bool ignoreOrganisationFlag = _OrganisationMatch(personOrg);

                                        if (ignoreOrganisationFlag)
                                        {
                                            personOrg.OrganisationName = "";
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(orgItem.Telephone))
                                    {
                                        PipelineTelephone telephone = new PipelineTelephone();

                                        telephone.ClientSuppliedNumber = orgItem.Telephone;

                                        personOrg.Telephones.Add(telephone);
                                    }

                                    #region ADDRESS

                                    if (!string.IsNullOrEmpty(orgItem.Address) || !string.IsNullOrEmpty(orgItem.Postcode))
                                    {
                                        PipelineAddress address = new PipelineAddress();

                                        address.AddressType_Id = (int)AddressLinkType.TradingAddress;
                                        address.Street = orgItem.Address;
                                        address.PostCode = orgItem.Postcode;

                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                                        bool ignoreAddressFlag = _AddressMatch(address);

                                        if (!ignoreAddressFlag)
                                            personOrg.Addresses.Add(address);

                                    }
                                    #endregion

                                    #region ORGANISATION VEHICLE

                                    foreach (var hvItem in vehiclesList)
                                    {

                                        if (hvItem.OrganisationId == orgItem.OrganisationId)
                                        {
                                            PipelineOrganisationVehicle orgVehicle = new PipelineOrganisationVehicle();

                                            orgVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyHireVehicle;

                                            if (!string.IsNullOrEmpty(hvItem.VehicleRegistration))
                                            {
                                                orgVehicle.VehicleRegistration = hvItem.VehicleRegistration;
                                            }
                                            if (!string.IsNullOrEmpty(hvItem.VehicleMake))
                                            {
                                                orgVehicle.VehicleMake = hvItem.VehicleMake;
                                            }
                                            if (!string.IsNullOrEmpty(hvItem.VehicleModel))
                                            {
                                                orgVehicle.VehicleModel = hvItem.VehicleModel;
                                            }
                                            if (hvItem.DtHireStarted != null)
                                            {
                                                orgVehicle.V2O_LinkData.HireStartDate = hvItem.DtHireStarted;
                                            }
                                            if (hvItem.DtHireEnded != null)
                                            {
                                                orgVehicle.V2O_LinkData.HireEndDate = hvItem.DtHireEnded;
                                            }

                                            personOrg.Vehicles.Add(orgVehicle);
                                        }
                                    }
                                    #endregion

                                    person.Organisations.Add(personOrg);
                                }
                            }

                            #endregion

                            vehicle.People.Add(person);
                        }

                    }
                }
                        #endregion

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in motorClaim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;
                    }

                    foreach (var person in vehicle.People)
                    {
                        if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                        {
                            insuredDriverCheck = true;
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {

                    if (insuredDriverCheck == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;


                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);

                    }

                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in motorClaim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion

                if (processClaim(ctx, motorClaim, statusTracking) == -1) return;
            }

            // Remove extracted zip files
            DeleteExtractedZipFiles(clientFolder, true);


        }

        /// <summary>
        /// Where there is no Insured Driver and only Insured Unknown is provided then the sub party type must be set to ‘Driver’ instead of ‘Unknown’.
        /// </summary>
        /// <param name="noInsuredDriverCount"></param>
        /// <param name="insuredDriverCount"></param>
        /// <returns>SubPartyType Id</returns>
        private int PersonInvolvementSubPartyType(int noInsuredDriverCount, int insuredDriverCount)
        {
            int subPartyType = (int)SubPartyType.Unknown;

            if (noInsuredDriverCount == 0 && insuredDriverCount == 0)
            {
                subPartyType = (int)SubPartyType.Driver;
            }

            return subPartyType;
        }

        private void DeleteExtractedZipFiles(string clientFolder, bool deleteFiles)
        {
            if (deleteFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        string extension = Path.GetExtension(file);
                        if (extension.ToUpper() != ".ZIP")
                        {
                            File.SetAttributes(file, FileAttributes.Normal);
                            File.Delete(file);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }

        private static void DeleteExtractedFiles(string clientFolder, bool deleteExtractedFiles)
        {
            if (deleteExtractedFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        File.Delete(file);
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }

        public List<PipelineAddress> defaultAddresses(List<string> defaultAddressesForClient)
        {
            List<PipelineAddress> defaultAddresses = new List<PipelineAddress>();

            foreach (var str in defaultAddressesForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelineAddress address = new PipelineAddress();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //SB=[],BN=[],BLD=[],ST=[],LOC=[],TWN=[],CNTY=[],PC=[]

                    address.SubBuilding = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.BuildingNumber = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Building = match[2].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Street = match[3].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Locality = match[4].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Town = match[5].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.County = match[6].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.PostCode = match[7].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultAddresses.Add(address);
                }
            }

            return defaultAddresses;
        }

        public List<PipelinePerson> defaultPeople(List<string> defaultPeopleForClient)
        {
            List<PipelinePerson> defaultPeople = new List<PipelinePerson>();

            foreach (var str in defaultPeopleForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelinePerson person = new PipelinePerson();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //FN=[],LN=[]

                    person.FirstName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    person.LastName = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultPeople.Add(person);
                }
            }

            return defaultPeople;
        }

        public List<PipelineOrganisation> defaultOrganisation(List<string> defaultPeopleForClient)
        {
            List<PipelineOrganisation> defaultOrganisations = new List<PipelineOrganisation>();

            foreach (var str in defaultOrganisationsForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelineOrganisation organisation = new PipelineOrganisation();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //ON=[]

                    organisation.OrganisationName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultOrganisations.Add(organisation);
                }
            }

            return defaultOrganisations;
        }

        private bool _AddressMatch(PipelineAddress address)
        {
            var dadr = defaultAddresses(defaultAddressesForClient);

            foreach (var adr in dadr)
            {
                bool match = false;

                if (!string.IsNullOrEmpty(adr.SubBuilding) && !string.IsNullOrEmpty(address.SubBuilding))
                {
                    if (address.SubBuilding.ToUpper() == adr.SubBuilding.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.BuildingNumber) && !string.IsNullOrEmpty(address.BuildingNumber))
                {
                    if (address.BuildingNumber.ToUpper() == adr.BuildingNumber.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.Building) && !string.IsNullOrEmpty(address.Building))
                {
                    if (address.Building.ToUpper() == adr.Building.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.Street) && !string.IsNullOrEmpty(address.Street))
                {
                    if (address.Street.ToUpper() == adr.Street.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.Locality) && !string.IsNullOrEmpty(address.Locality))
                {
                    if (address.Locality.ToUpper() == adr.Locality.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.Town) && !string.IsNullOrEmpty(address.Town))
                {
                    if (address.Town.ToUpper() == adr.Town.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.County) && !string.IsNullOrEmpty(address.County))
                {
                    if (address.County.ToUpper() == adr.County.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.PostCode) && !string.IsNullOrEmpty(address.PostCode))
                {
                    if (address.PostCode.ToUpper() == adr.PostCode.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                return match;
            }

            return false;
        }

        private bool _PersonMatch(PipelinePerson person)
        {
            var dper = defaultPeople(defaultPeopleForClient);

            foreach (var p in dper)
            {
                if (person.FirstName.ToUpper() == p.FirstName.ToUpper() && person.LastName.ToUpper() == p.LastName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        private bool _OrganisationMatch(PipelineOrganisation organisation)
        {
            var dorg = defaultOrganisation(defaultOrganisationsForClient);

            foreach (var o in dorg)
            {
                if (organisation.OrganisationName.ToUpper() == o.OrganisationName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

    }
}
