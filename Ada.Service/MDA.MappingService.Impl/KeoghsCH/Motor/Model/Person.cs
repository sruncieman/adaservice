﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.KeoghsCH.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class Person
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int MatterNum;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int VehicleId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int PersonId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PersonInvolvement;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Salutation;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? Dob;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Gender;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NiNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LandlineTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MobileTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Postcode;


    }
}
