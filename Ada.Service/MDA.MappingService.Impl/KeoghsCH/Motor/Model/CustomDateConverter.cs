﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.KeoghsCH.Motor
{
    public class CustomDateConverter : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;
            value = value.TrimEnd();
            if (DateTime.TryParseExact(value, "yyyy-MM-dd HH:mm:ss", null, DateTimeStyles.None, out dt))
                return dt;

            return null;
        }
    }
}
