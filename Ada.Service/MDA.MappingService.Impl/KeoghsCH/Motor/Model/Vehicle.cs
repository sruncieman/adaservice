﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.KeoghsCH.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class Vehicle
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int MatterNum;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? VehicleId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? OrganisationId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleInvolvement;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleRegistration;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleMake;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleModel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? DtHireStarted;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? DtHireEnded;
    }
}
