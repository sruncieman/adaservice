﻿using MDA.Common;
using MDA.MappingService.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.Server;

namespace MDA.MappingService.KeoghsCH.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private string tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="fs"></param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim"></param>
        /// <param name="processingResults"></param>
        public void ConvertToClaimBatch(MDA.Common.Server.CurrentContext ctx, System.IO.Stream fs, object statusTracking, Func<MDA.Common.Server.CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> processClaim, out MDA.Common.ProcessingResults processingResults)
        {

            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            var defaultDataAddressForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataAddressForClient(ctx.RiskClientId);
            var defaultDataPeopleForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataPeopleForClient(ctx.RiskClientId);
            var defaultDataOrganisationsForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataOrganisationsForClient(ctx.RiskClientId);

            try
            {
                bool DeleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);

                new GetMotorClaimBatch().RetrieveMotorClaimBatch(ctx, fs, clientFolder, DeleteUploadedFiles, statusTracking, processClaim, processingResults, defaultDataAddressForClient, defaultDataPeopleForClient, defaultDataOrganisationsForClient);
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping KeoghsCH file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }

        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard, int settleTime, int batchSize, string postOp, string archiveFolder, Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {
            // Create structure to hold our new batch of claims
            List<object> batch = new List<object>();

            int batchCount = 0;
            string firstFileName = null;

            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            string[] files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (files.Count() == 0) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Where(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now).Any())
                return;

            // If we get here we have files to process
            foreach (var file in files)
            {
                // Create a filename to use for this batch.  We will just use the first file name in this case
                if (firstFileName == null)
                    firstFileName = Path.GetFileName(file);

                // Add this new claim to the batch
                batch.Add(file);

                #region If Batch full process it

                if (batch.Count >= batchSize)
                {
                    batchCount++;

                    string fileName = firstFileName;
                    string mimeType = "zip";

                    string clientName = "";
                    string date;

                    switch (ctx.RiskClientId)
                    {
                        case 3:
                            clientName = "COCH";
                            break;
                        case 13:
                            clientName = "AVPC";
                            break;
                        case 14:
                            clientName = "SEPC";
                            break;
                        case 15:
                            clientName = "SERV";
                            break;
                        case 16:
                            clientName = "COLC";
                            break;
                        case 17:
                            clientName = "SELC";
                            break;
                        case 18:
                            clientName = "AVLC";
                            break;
                        case 21:
                            clientName = "ALLC";
                            break;
                        case 22:
                            clientName = "AXLC";
                            break;
                        case 23:
                            clientName = "CHLC";
                            break;
                        case 24:
                            clientName = "DLLC";
                            break;
                        case 25:
                            clientName = "ESLC";
                            break;
                        case 26:
                            clientName = "HSLC";
                            break;
                        case 27:
                            clientName = "HALC";
                            break;
                        case 28:
                            clientName = "LVLC";
                            break;
                        case 29:
                            clientName = "NFLC";
                            break;
                        case 30:
                            clientName = "PVLC";
                            break;
                        case 31:
                            clientName = "PLLC";
                            break;
                        case 32:
                            clientName = "UKLC";
                            break;
                        case 33:
                            clientName = "ZCLC";
                            break;
                        case 34:
                            clientName = "ZGLC";
                            break;
                        case 35:
                            clientName = "ZMLC";
                            break;
                        case 36:
                            clientName = "ZPLC";
                            break;
                        case 38:
                            clientName = "ESLC";
                            break;
                        case 42:
                            clientName = "LVPC";
                            break;
                        case 48:
                            clientName = "CVPC";
                            break;
                        case 49:
                            clientName = "ADPC";
                            break;
                        case 50:
                            clientName = "HSPC";
                            break;

                    }


                    date = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
                    date = date.Replace("/", "");
                    date = date.Replace(":", "");
                    date = date.Replace(" ", "");

                    string batchRef = clientName + date;

                    using (FileStream fileStream = File.OpenRead(file))
                    {
                        MemoryStream stream = new MemoryStream();
                        stream.SetLength(fileStream.Length);
                        fileStream.Read(stream.GetBuffer(), 0, (int)fileStream.Length);

                        #region Process the batch (call the callback function)
                        processClientBatch(ctx, stream, fileName, mimeType, "Motor", batchRef);
                        #endregion
                    }


                    // Setup for a new batch and go round the loop again
                    batch.Clear();
                    firstFileName = null;
                }
                #endregion

                #region Process all the options we invented for our postOp functionality

                // We are passed the string we entered into config.  Now we have to code it!
                // In this case we have implemented RENAME, DELETE, ARCHIVE
                // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                switch (postOp.ToUpper())
                {
                    case "RENAME":
                        File.Move(file, file + ".old");
                        break;

                    case "DELETE":
                        File.Delete(file);
                        break;

                    case "ARCHIVE":
                        if (!Directory.Exists(archiveFolder))
                            Directory.CreateDirectory(archiveFolder);   // Create destination if needed

                        string fname = archiveFolder + "\\" + Path.GetFileName(file);  // build dest path

                        if (File.Exists(fname))  // Check if it already exists
                        {
                            fname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                        }

                        File.Move(file, fname);
                        break;
                }
                #endregion

            }

        }

    }
}
