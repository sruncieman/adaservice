﻿using FileHelpers;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common.Server;
using MDA.MappingService.eSure.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using MDA.MappingService.Interface;
using MDA.Common;
using Ionic.Zip;
using System.Text.RegularExpressions;


namespace MDA.MappingService.eSure.Motor
{
    public class Mapper : IMapper
    {
        private Stream _fs;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private object statusTracking;
        private FileHelperEngine _coreClaimDataEngine;
        private List<string> _lstClaim;


        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
        private string _coreClaimDataFile;
        private string _claimDataFile;
        private ClaimData[] _coreClaimData;
        private StreamReader _claimDataFileStreamReader;


        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));

            _lstClaim = new List<string>();
        }
        
        public void AssignFiles()
        {

            if (_fs == null)
            {
                FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Esure\Data";
                ClaimFile = FolderPath + @"\PI Claim Extract - V2.csv";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(_fs);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }

            }


        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {

                if (_fs == null)
                {
                    _coreClaimData = _coreClaimDataEngine.ReadFile(ClaimFile) as ClaimData[];
                }
                else
                {
                    _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }


            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            try
            {

                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helper Engine (core) : ", false);
                }

                _coreClaimData = _coreClaimDataEngine.ReadFile(_coreClaimDataFile) as ClaimData[];

            }

            catch (Exception ex)
            {
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "PopulateFileHelpersEngines Error : " + ex.Message, true);
                }

                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers 
                //if (_coreClaimData != null)
                //    _lstClaim = _coreClaimData
                //        .GroupBy(i => i.ClaimNumber)
                //        .Select(g => g.First())
                //        .ToList();

                _lstClaim = _coreClaimData.Where(x => x.ClaimNumber != "").Select(x => x.ClaimNumber).Distinct().ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }

        }

        public void Translate()
        {
            //int _skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["LVSkipNum"]);
            //int _takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["LVTakeNum"]);

            foreach (var claim in _lstClaim) //.Skip(_skipValue).Take(_takeValue))
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.Where(x => x.ClaimNumber == claim);

                var claimInfo = uniqueClaim.FirstOrDefault();

                motorClaim.ClaimNumber = claimInfo.ClaimNumber;
                motorClaim.IncidentDate = claimInfo.ClaimDate;
                motorClaim.ClaimType_Id = (int)ClaimType.Motor;

                motorClaim.ExtraClaimInfo.ClaimStatus_Id = ClaimStatus(claimInfo.ClaimStatus);
                motorClaim.ExtraClaimInfo.ClaimNotificationDate = claimInfo.NotificationDate;
                motorClaim.ExtraClaimInfo.IncidentLocation = claimInfo.IncidentLocation;
                motorClaim.ExtraClaimInfo.IncidentCircumstances = claimInfo.IncidentDescription;

                motorClaim.Policy.Insurer = "eSure";

                foreach (var o in uniqueClaim)
                {
                    var partyType = Regex.Replace(o.PartyType, @"[\d-]", string.Empty);

                    if (OrganisationPartyTypes().Contains(partyType.Trim().ToUpper()))
                    {
                        PipelineOrganisation organisation = new PipelineOrganisation();

                        organisation.OrganisationName = o.FullName;
                        _PartyType(o.PartyType.ToUpper(), organisation);

                        if (!string.IsNullOrWhiteSpace(o.EmailCaptured))
                        {
                            PipelineEmail email = new PipelineEmail();

                            email.EmailAddress = o.EmailCaptured;

                            organisation.EmailAddresses.Add(email);
                        }

                        if (!string.IsNullOrWhiteSpace(o.MainPhoneNumber))
                        {
                            PipelineTelephone telephone = new PipelineTelephone();

                            telephone.ClientSuppliedNumber = o.MainPhoneNumber;

                            organisation.Telephones.Add(telephone);
                        }

                        if (!string.IsNullOrWhiteSpace(o.FullAddress))
                        {
                            PipelineAddress address = new PipelineAddress();

                            address.Street = o.FullAddress;

                            organisation.Addresses.Add(address);
                        }

                        motorClaim.Organisations.Add(organisation);
                    }

                    if (partyType.Trim().ToUpper() == "OTHERS")
                    {
                        string[] names = o.FullName.Split(' ');

                        if (Salutation(names[0]) == (int)MDA.Common.Enum.Salutation.Unknown)
                        {
                            PipelineOrganisation organisation = new PipelineOrganisation();

                            organisation.OrganisationName = o.FullName;
                            _PartyType(o.PartyType.ToUpper(), organisation);

                            if (!string.IsNullOrWhiteSpace(o.EmailCaptured))
                            {
                                PipelineEmail email = new PipelineEmail();

                                email.EmailAddress = o.EmailCaptured;

                                organisation.EmailAddresses.Add(email);
                            }

                            if (!string.IsNullOrWhiteSpace(o.MainPhoneNumber))
                            {
                                PipelineTelephone telephone = new PipelineTelephone();

                                telephone.ClientSuppliedNumber = o.MainPhoneNumber;

                                organisation.Telephones.Add(telephone);
                            }

                            if (!string.IsNullOrWhiteSpace(o.FullAddress))
                            {
                                PipelineAddress address = new PipelineAddress();

                                address.Street = o.FullAddress;

                                organisation.Addresses.Add(address);
                            }

                            motorClaim.Organisations.Add(organisation);
                        }

                    }
                }

                foreach (var v in uniqueClaim)
                {
                    if (!string.IsNullOrWhiteSpace(v.VehicleRegistration))
                    {
                        PipelineVehicle vehicle = new PipelineVehicle();

                        vehicle.VehicleRegistration = v.VehicleRegistration;

                        bool match = false;

                        foreach (var veh in motorClaim.Vehicles)
                        {
                            if (veh.VehicleRegistration == v.VehicleRegistration)
                            {
                                match = true;
                            }
                        }

                        if (match == false)
                        {
                            motorClaim.Vehicles.Add(vehicle);
                        }                        
                    }
                }

                foreach (var p in uniqueClaim)
                {
                    var partyType = Regex.Replace(p.PartyType, @"[\d-]", string.Empty);

                    if (PersonPartyTypes().Contains(partyType.Trim().ToUpper()))
                    {
                        if (partyType.Trim().ToUpper() == "OTHERS")
                        {
                            string[] names = p.FullName.Split(' ');

                            if (Salutation(names[0]) == (int)MDA.Common.Enum.Salutation.Unknown)
                            {
                                continue;
                            }

                        }

                        PipelinePerson person = new PipelinePerson();

                        PersonName(person, p.FullName);
                        _PartyType(partyType.Trim().ToUpper(), person);

                        person.DateOfBirth = p.DateofBirth;

                        if (!string.IsNullOrWhiteSpace(p.EmailCaptured))
                        {
                            PipelineEmail email = new PipelineEmail();

                            email.EmailAddress = p.EmailCaptured;

                            person.EmailAddresses.Add(email);
                        }

                        if (!string.IsNullOrWhiteSpace(p.MainPhoneNumber))
                        {
                            PipelineTelephone telephone = new PipelineTelephone();

                            telephone.ClientSuppliedNumber = p.MainPhoneNumber;

                            person.Telephones.Add(telephone);
                        }

                        if (!string.IsNullOrWhiteSpace(p.FullAddress))
                        {
                            PipelineAddress address = new PipelineAddress();

                            address.Street = p.FullAddress;

                            person.Addresses.Add(address);
                        }

                        bool vehicleFound = false;

                        foreach (var v in motorClaim.Vehicles)
                        {
                            if (v.VehicleRegistration == p.VehicleRegistration)
                            {
                                v.People.Add(person);
                                vehicleFound = true;

                                if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured)
                                {
                                    v.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;   
                                }
                                else if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.ThirdParty)
                                {
                                    v.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                                }                               

                                break;
                            }

                        }

                        foreach (var v in motorClaim.Vehicles)
                        {
                            if (!vehicleFound)
                            {
                                if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured)
                                {
                                    foreach (var iv in motorClaim.Vehicles)
                                    {
                                        if (iv.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                        {
                                            iv.People.Add(person);
                                            vehicleFound = true;
                                            break;

                                        }
                                    }
                                }
                                else if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.ThirdParty)
                                {
                                    foreach (var tpv in motorClaim.Vehicles)
                                    {
                                        if (tpv.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.ThirdPartyVehicle)
                                        {
                                            tpv.People.Add(person);
                                            vehicleFound = true;
                                            break;

                                        }
                                    }
                                }

                                if (vehicleFound)
                                {
                                    break;
                                }

                            }
                        }
                        

                        if (!vehicleFound)
                        {
                            bool noVehicleInvolved = false;

                            foreach (var v in motorClaim.Vehicles)
                            {
                                if (v.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.NoVehicleInvolved)
                                {
                                    v.People.Add(person);

                                    noVehicleInvolved = true;
                                }
                            }

                            if (!noVehicleInvolved)
                            {
                                PipelineVehicle vehicle = new PipelineVehicle();
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                                vehicle.People.Add(person);
                                motorClaim.Vehicles.Add(vehicle);
                            }

                        }
                    }
                }


                if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;

            }
        }

        private List<string> OrganisationPartyTypes()
        {
            List<string> partyTypes = new List<string>();

            partyTypes.Add("ACCIDENT MANAGEMENT COMPANY");
            partyTypes.Add("APPLS SOLICITOR");
            partyTypes.Add("CREDIT HIRE ORGANISATION");
            partyTypes.Add("DOCTOR");
            partyTypes.Add("DVLA");
            partyTypes.Add("EMERGENCY SERVICES");
            partyTypes.Add("RECOVERY AGENT");
            partyTypes.Add("REPAIRER OR SUPPLIER");
            partyTypes.Add("STORAGE FACILITY");

            return partyTypes;
        }

        private List<string> PersonPartyTypes()
        {
            List<string> partyTypes = new List<string>();

            partyTypes.Add("CARD HOLDER");
            partyTypes.Add("CLAIMANT");
            partyTypes.Add("CLAIMANT PASSENGER");
            partyTypes.Add("CLAIMANT WITNESS");
            partyTypes.Add("CYCLIST");
            partyTypes.Add("DIRECT CLAIMANT");
            partyTypes.Add("LITIGATION FRIEND");
            partyTypes.Add("MINOR");
            partyTypes.Add("OTHERS");
            partyTypes.Add("OWNER");
            partyTypes.Add("PEDESTRIAN");
            partyTypes.Add("THIRD PARTY DRIVER");
            partyTypes.Add("THIRD PARTY PASSENGER");
            partyTypes.Add("THIRD PARTY SPOUSE/PARTNER");
            partyTypes.Add("WITNESS");

            return partyTypes;
        }

        private int ClaimStatus(string claimStatus)
        {
            switch (claimStatus.ToUpper())
            {
                case "OPEN":
                case "NOTIFIED ONLY":
                    return (int)MDA.Common.Enum.ClaimStatus.Open;
                case "CLOSED":
                    return (int)MDA.Common.Enum.ClaimStatus.Settled;
                case "DECLINED":
                    return (int)MDA.Common.Enum.ClaimStatus.Withdrawn;
                case "REOPENED":
                    return (int)MDA.Common.Enum.ClaimStatus.Reopened;
                case "DELETE":
                    return (int)MDA.Common.Enum.ClaimStatus.Delete;
                default:
                    return (int)MDA.Common.Enum.ClaimStatus.Unknown;
            }
        }

        private void PersonName(PipelinePerson person, string FullName)
        {
            string[] names = FullName.Split(' ');

            if (Salutations().Contains(names[0]))
            {
                person.Salutation_Id = Salutation(names[0]);

                if (names.Count() == 2)
                {
                    person.LastName = names[1];
                }
                else if (names.Count() == 3)
                {
                    person.FirstName = names[1];
                    person.LastName = names[2];
                }
                else if (names.Count() == 4)
                {
                    person.FirstName = names[1];
                    person.MiddleName = names[2];
                    person.LastName = names[3];
                }
                else
                {
                    person.LastName = FullName;
                }
            }
            else
            {
                if (names.Count() == 1)
                {
                    person.LastName = names[0];
                }
                else if (names.Count() == 2)
                {
                    person.FirstName = names[0];
                    person.LastName = names[1];
                }
                else if (names.Count() == 3)
                {
                    person.FirstName = names[0];
                    person.MiddleName = names[1];
                    person.LastName = names[2];
                }
                else
                {
                    person.LastName = FullName;
                }
            }


        }

        private List<string> Salutations()
        {
            List<string> salutations = new List<string>();

            salutations.Add("MISS");
            salutations.Add("MRS");
            salutations.Add("MS");
            salutations.Add("MR");

            return salutations;
        }

        private int Salutation(string salutation)
        {
            switch (salutation)
            {
                case "MISS":
                    return (int)MDA.Common.Enum.Salutation.Miss;
                case "MRS":
                    return (int)MDA.Common.Enum.Salutation.Mrs;
                case "MS":
                    return (int)MDA.Common.Enum.Salutation.Ms;
                case "MR":
                    return (int)MDA.Common.Enum.Salutation.Mr;
                default:
                    return (int)MDA.Common.Enum.Salutation.Unknown;
            }
        }
        
        private void _PartyType(string partyType, PipelineOrganisation organisation)
        {
            if (partyType == "ACCIDENT MANAGEMENT COMPANY")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.AccidentManagement;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.AccidentManagement;
            }

            if (partyType == "APPLS SOLICITOR")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
            }

            if (partyType == "CREDIT HIRE ORGANISATION")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.CreditHire;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire;
            }

            if (partyType == "DOCTOR")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;
            }

            if (partyType == "DVLA")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
            }

            if (partyType == "EMERGENCY SERVICES")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
            }

            if (partyType == "RECOVERY AGENT")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Recovery;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Recovery;
            }

            if (partyType == "REPAIRER OR SUPPLIER")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Repairer;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer;
            }

            if (partyType == "STORAGE FACILITY")
            {
                organisation.OrganisationType_Id = (int)OrganisationType.Storage;
                organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Storage;
            }

        }

        private void _PartyType(string partyType, PipelinePerson person)
        {
            if (partyType == "CARD HOLDER")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.PaidPolicy;
            }

            if (partyType == "CLAIMANT")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
            }

            if (partyType == "CLAIMANT PASSENGER")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
            }

            if (partyType == "CLAIMANT WITNESS")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
            }

            if (partyType == "CYCLIST")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
            }

            if (partyType == "DIRECT CLAIMANT")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
            }

            if (partyType == "LITIGATION FRIEND")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.LitigationFriend;
            }

            if (partyType == "MINOR")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
            }

            if (partyType == "OTHERS")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
            }

            if (partyType == "OWNER")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
            }

            if (partyType == "PEDESTRIAN")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
            }

            if (partyType == "THIRD PARTY DRIVER")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
            }

            if (partyType == "THIRD PARTY PASSENGER")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
            }

            if (partyType == "THIRD PARTY SPOUSE/PARTNER")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
            }

            if (partyType == "WITNESS")
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;

            }
            
        } 

    }
}
