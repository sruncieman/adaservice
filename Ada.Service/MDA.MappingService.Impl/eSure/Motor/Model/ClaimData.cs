﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.eSure.Motor.Model
{
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime ClaimDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? NotificationDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimStatus;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PartyType;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String FullName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? DateofBirth;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String FullAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MainPhoneNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EmailCaptured;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleRegistration;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String IncidentDescription;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String IncidentLocation;

    }
}
