﻿using MDA.RiskService.Model;
using System.Collections.Generic;
using MDA.DAL;
using ADAServices.Interface;
using Newtonsoft.Json;
using MDA.Common;
using System;

namespace ADAServices.Translator
{
    public partial class Translator
    {
        private class Lookups
        {
            //public static string[] ClaimStatusText = { "Bad", "Created", "Loaded", "Scored" };
            //public static string[] BatchStatusText = { "Bad", "Created", "Loading", "Loaded" };
            public static string[] RiskColourText = { "Green", "Amber", "Red" };
        }

        public static string ClaimStatusToText(int claimStatus)
        {
            switch(claimStatus)
            {
                case -8: return "Claim is Keoghs CFS";
                case -7: return "Claim was valid but batch was rejected";
                case -6: return "Claim failed validation";
                case -5: return "Claim failed cleansing";
                case -4: return "Claim failed to score";
                case -3: return "Claim failed external service call";
                case -2: return "Claim failed to load";
                case -1: return "Claim failed to create";
                case 0: return "New";
                case 1: return "Queued for loading";
                case 2: return "Loading";
                case 3: return "Loading Complete";
                case 4: return "Queued for 3rd party";
                case 5: return "3rd Party Processing";
                case 6: return "3rd Party Complete";
                case 7: return "Queued for scoring";
                case 8: return "Scoring";
                case 9: return "Scored";
                case 10: return "Archived";
            }
            return "Error";
        }

        public static string BatchStatusToText(int batchStatus)
        {
            switch (batchStatus)
            {
                case -6: return "Duplicate";
                case -4: return "Error";     //UnableToProcessMappingFailed
                case -3: return "Error";     //UnableToProcessNoMappingDefined
                case -2: return "Error";     //UnableToProcessNoFileFound
                case -1: return "Rejected";  //Bad
                case 0: return "Created";
                case 1: return "Queued";
                case 2: return "Loading";
                case 3: return "Loading";
                case 4: return "Loading";
                case 5: return "Loaded";
            }
            return "Error";
        }

        public static string ReportRequestStatusToText(int requestStatus)
        {
            switch (requestStatus)
            {
                case 0: return "Request Level 1";
                case 1: return "Level 1 Pending";
                case 2: return "View Level 1";
                case 3: return "Level 1 Unavailable";
            }
            return "Error";
        }

        public static EEntityScoreMessageList Translate(EntityScoreMessageList l)
        {
            return new EEntityScoreMessageList()
            {
                EntityHeader = l.EntityHeader,
                Messages = l.Messages
            };
        }

        public static ERiskClaimDetails Translate(RiskClaimRunDetails rcr)
        {
            var r = new ERiskClaimDetails()
            {
                RiskBatch_Id                    = rcr.RiskBatch_Id,
                BatchReference                  = rcr.BatchReference,
                ClientBatchReference            = rcr.ClientBatchReference,
                BatchStatus                     = rcr.BatchStatus,
                BatchStatusAsString             = BatchStatusToText(rcr.BatchStatus),
                RiskClaim_Id                    = rcr.RiskClaim_Id,
                BaseRiskClaim_Id                = rcr.BaseRiskClaim_Id,
                MDAClaimRef                     = rcr.MDAClaimRef,
                CreatedDate                     = rcr.CreatedDate,
                CreatedBy                       = rcr.CreatedBy,
                ClientClaimRefNumber            = rcr.ClientClaimRefNumber,
                LevelOneRequestedCount          = rcr.LevelOneRequestedCount,
                LevelTwoRequestedCount          = rcr.LevelTwoRequestedCount,
                LevelOneRequestedWhen           = rcr.LevelOneRequestedWhen,
                LevelTwoRequestedWhen           = rcr.LevelTwoRequestedWhen,
                LevelOneRequestedWho            = rcr.LevelOneRequestedWho,
                LevelTwoRequestedWho            = rcr.LevelTwoRequestedWho,
                LevelOneRequestedStatus         = rcr.LevelOneRequestedStatus,
                LevelTwoRequestedStatus         = rcr.LevelTwoRequestedStatus,
                LevelOneRequestedStatusAsString = ReportRequestStatusToText(rcr.LevelOneRequestedStatus),
                LevelTwoRequestedStatusAsString = ReportRequestStatusToText(rcr.LevelTwoRequestedStatus),
                ClaimStatus                     = rcr.ClaimStatus,
                ClaimReadStatus                 = rcr.ClaimReadStatus,
                ClaimStatusAsString             = ClaimStatusToText(rcr.ClaimStatus),
                Incident_Id                     = rcr.Incident_Id,
                IncidentDate                    = rcr.IncidentDate,
                Reserve                         = rcr.Reserve,
                PaymentsToDate                  = rcr.PaymentsToDate,
                RiskClaimRun_Id                 = rcr.RiskClaimRun_Id,
                TotalScore                      = rcr.TotalScore,
                TotalKeyAttractorCount          = rcr.TotalKeyAttractorCount,
                CleansingResults                = ((rcr.CleansingResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(rcr.CleansingResults).Message : null),
                ValidationResults               = ((rcr.ValidationResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(rcr.ValidationResults).Message : null),
                LatestScored                    = rcr.LatestScored,
                LatestVersion                   = rcr.LatestVersion,
                NoteAdded                       = rcr.NoteAdded,
                IntelAdded                      = rcr.IntelAdded,
                DecisionAdded                   = rcr.DecisionAdded, 
                ReserveChanged                  = rcr.ReserveChanged,
                DecisionId                      = rcr.DecisionId,
            };

            if (rcr.TotalScore < rcr.AmberThreshold)
            {
                r.RiskRatingAsString = Lookups.RiskColourText[0];
            }
            else if (rcr.TotalScore < rcr.RedThreshold)
            {
                r.RiskRatingAsString = Lookups.RiskColourText[1];
            }
            else
                r.RiskRatingAsString = Lookups.RiskColourText[2];

            r.MessageList = new List<EEntityScoreMessageList>();

            if (rcr.MessageList != null)
            {
                foreach (var i in rcr.MessageList)
                    r.MessageList.Add(Translator.Translate(i));
            }

            return r;
        }

        public static ERiskClaimEdit Translate(ERiskClaimEdit claim)
        {
            var r = new ERiskClaimEdit()
                {

                };

            return r;
        }

        public static ERiskClaim Translate(ERiskClaim claim)
        {
            var r = new ERiskClaim()
            {

            };

            return r;
        }

        //public static EBatchNumbers Translate(RiskBatchNumbers esc)
        //{
        //    return new EBatchNumbers()
        //    {

        //    };
        //}

        public static ERiskClaim Translate(RiskClaim rc, int latestScore)
        {
            return new ERiskClaim()
            {
                Id                     = rc.Id,
                RiskBatch_Id           = rc.RiskBatch_Id,
                Incident_Id            = rc.Incident_Id,
                MdaClaimRef            = rc.MdaClaimRef,
                BaseRiskClaim_Id       = rc.BaseRiskClaim_Id,
                ClaimStatus            = rc.ClaimStatus,
                ClaimReadStatus        = rc.ClaimReadStatus,
                ClientClaimRefNumber   = rc.ClientClaimRefNumber,
                LevelOneRequestedCount = rc.LevelOneRequestedCount,
                LevelTwoRequestedCount = rc.LevelTwoRequestedCount,
                LevelOneRequestedWhen  = rc.LevelOneRequestedWhen,
                LevelTwoRequestedWhen  = rc.LevelTwoRequestedWhen,
                LevelOneRequestedWho   = rc.LevelOneRequestedWho,
                LevelTwoRequestedWho   = rc.LevelTwoRequestedWho,
                SourceReference        = rc.SourceReference,
                CreatedDate            = rc.CreatedDate,
                CreatedBy              = rc.CreatedBy,
                TotalScore             = latestScore,
                CleansingResults       = ((rc.CleansingResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(rc.CleansingResults).Message : null),
                ValidationResults      = ((rc.ValidationResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(rc.ValidationResults).Message : null),
            };
        }

        public static ERiskBatch Translate(RiskBatch rb)
        {
            if (rb == null)
                return null;

            return new ERiskBatch()
            {
                BatchReference       = rb.BatchReference,
                BatchStatus          = rb.BatchStatus,
                BatchStatusAsString  = BatchStatusToText(rb.BatchStatus),
                ClientBatchReference = rb.ClientBatchReference,
                CreatedBy            = rb.CreatedBy,
                CreatedDate          = rb.CreatedDate,
                Id                   = rb.Id,
                RiskClient_Id        = rb.RiskClient_Id,
                MappingResults = ((rb.MappingResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(rb.MappingResults).Message : null),
                VerificationResults = ((rb.VerificationResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(rb.VerificationResults).Message : null)
            };
        }

        public static ERiskClaimRun Translate(RiskClaimRun rcr)
        {
            if (rcr == null) return null;

            return new ERiskClaimRun()
            {
                Id                     = rcr.Id,
                RiskClaim_Id           = rcr.RiskClaim_Id,
                TotalScore             = rcr.TotalScore,
                TotalKeyAttractorCount = rcr.TotalKeyAttractorCount,
                ExecutedBy             = rcr.ExecutedBy,
                ExecutedWhen           = rcr.ExecutedWhen,
            };
        }

        public static ERiskClient Translate(RiskClient rc)
        {
            return new ERiskClient()
            {
                Id             = rc.Id,
                ClientName     = rc.ClientName,
                AmberThreshold = rc.AmberThreshold,
                RedThreshold = rc.RedThreshold,
                ExpectedFileExtension = rc.ExpectedFileExtension,
                LevelOneReportDelay = rc.LevelOneReportDelay,
                LevelTwoReportDelay = rc.LevelTwoReportDelay,
                InsurerName = rc.InsurersClient.ClientName,
                Status = rc.Status,
                InsuersClientsId = rc.InsurersClients_Id,
                RecordReserveChanges = rc.RecordReserveChanges,
                BatchPriority = rc.BatchPriority

            };
        }

        public static ERiskWord Translate(RiskWordFilter rw)
        {
            return new ERiskWord()
            {
                Id = rw.Id,
                DateAdded = rw.DateAdded,
                FieldName = rw.FieldName,
                LookupWord = rw.LookupWord,
                ReplacementWord = rw.ReplacementWord,
                ReplaceWholeString = rw.ReplaceWholeString,
                RiskClient_Id = rw.RiskClient_Id,
                SearchType = rw.SearchType,
                TableName = rw.TableName,

            };
        }

        //public static ERiskNote Translate(ERiskNote rn)
        //{
        //    return new ERiskNote()
        //    {
        //        Id = rn.Id,
        //        BaseRiskClaim_Id = rn.BaseRiskClaim_Id,
        //        CreatedBy = rn.CreatedBy,
        //        CreatedDate = rn.CreatedDate,
        //        Decision = rn.DecisionId,
        //        Deleted = rn.Deleted,
        //        NoteDesc = rn.Note,
        //        RiskClaim_Id = rn.RiskClaim_Id,
        //        Visibilty = rn.Visibility
                


        //    };
        //}

        public static ERiskNoteDecision Translate(RiskNoteDecision rnd)
        {
            return new ERiskNoteDecision()
            {
                Id = rnd.Id,
                ADARecordStatus = rnd.ADARecordStatus,
                Decision = rnd.Decision,
                RiskClient_Id = rnd.RiskClient_Id
            };
        }

        public static ERiskDefaultData Translate(RiskConfigurationValue rcv)
        {
            return new ERiskDefaultData()
            {
                Id = rcv.Id,
                ConfigurationValue = rcv.ConfigurationValue,
                IsActive = rcv.IsActive,
                RiskClient_Id = rcv.RiskClient_Id,
                RiskConfigurationDescription = rcv.RiskConfigurationDescription.ConfigurationDescription
            };
        }

        public static ERiskRole Translate(RiskRole rc)
        {
            return new ERiskRole()
            {
                Id = rc.Id,
                RoleName = rc.Name,

            };
        }

        public static EInsurersClient Translate(InsurersClient ic)
        {
            return new EInsurersClient()
            {
                Id = ic.Id,
                ClientName = ic.ClientName,

            };
        }

        public static ERiskTeam Translate(RiskTeam rc)
        {
            return new ERiskTeam()
            {
                Id = rc.Id,
                TeamName = rc.TeamName,
            };
        }

        public static ERiskUser Translate(RiskUserDetails rc)
        {
            return new ERiskUser()
            {
                Id              = rc.Id,
                RiskClientId    = rc.RiskClientId,
                RiskClientName  = rc.RiskClientName,
                RiskRoleId      = rc.RiskRoleId,
                RiskRoleName    = rc.RiskRoleName,
                RiskTeamId      = rc.RiskTeamId,
                RiskTeamName    = rc.RiskTeamName,
                UserName        = rc.UserName,
                FirstName       = rc.FirstName,
                LastName        = rc.LastName,
                TelephoneNumber = rc.TelephoneNumber,
                Status          = rc.Status,
                Password        = rc.Password,
                PasswordResetDate = rc.PasswordResetDate,
                AutoLoader      = rc.AutoLoader,
                TemplateFunctions = rc.TemplateFunctions,
                UserTypeId      = rc.UserTypeId
            };
        }

        public static EWebsiteBatchDetails Translate(RiskBatchDetails rb)
        {
            return new EWebsiteBatchDetails()
            {
                BatchId              = rb.Id,
                RiskClientId         = rb.RiskClientId,
                BatchReference       = rb.BatchReference,
                BatchEntityType      = rb.BatchEntityType,
                ClientBatchReference = rb.ClientBatchReference,
                BatchStatusAsString = BatchStatusToText(rb.BatchStatus),
                BatchStatusId = rb.BatchStatus,
                UploadedDate = rb.CreatedDate,
                UploadedBy = rb.CreatedBy,
                VerificationResults = rb.VerificationResults,
                MappingResults = rb.MappingResults,
                TotalNumberOfClaims = rb.TotalNumberOfClaims
            };
        }

        public static EWebsiteSingleClaimsDetails Translate(ERiskSingleClaim singleClaim)
        {
            var r = new EWebsiteSingleClaimsDetails()
            {
                RiskClaim_Id = singleClaim.Id,
                ClaimType = singleClaim.ClaimType,
                ClaimNumber = singleClaim.ClaimNumber,
                IncidentDate = singleClaim.IncidentDate,
                SavedBy = singleClaim.SavedBy,
                DateSaved = singleClaim.DateSaved,
            };

            return r;
        }

        public static EWebsiteSingleClaimsDetail Translate(ERiskSingleClaimDetail singleClaim)
        {
            var r = new EWebsiteSingleClaimsDetail()
                {
                    RiskClaim_Id = singleClaim.RiskClaimId,
                    claim = singleClaim.Claim
                };

            return r;
        }

        public static ECueReportDetails Translate(MDA.CueService.Model.Results reportStructure)
        {
            return new ECueReportDetails()
            {
                ClaimNumber = reportStructure.ClaimNumber,
                SearchDate = reportStructure.SearchDate,
                ListResultInfo = Translator.Translate(reportStructure.ListResultInfo)
            };
        }

        private static List<Dictionary<string, ECUE_ReportModel>> Translate(List<Dictionary<string, MDA.CueService.Model.Result>> lstResultInfo)
        {

            List<Dictionary<string, ECUE_ReportModel>> lstReport = new List<Dictionary<string, ECUE_ReportModel>>();

         

            foreach (var item in lstResultInfo)
            {
                Dictionary<string, ECUE_ReportModel> dictECUE_ReportModel = new Dictionary<string, ECUE_ReportModel>();
                
                foreach (var dictItem in item)
                {
                    dictECUE_ReportModel.Add(dictItem.Key, Translator.Translate(dictItem.Value));
                }

                lstReport.Add(dictECUE_ReportModel);
            }

            return lstReport;
        }

        private static ECUE_ReportModel Translate(MDA.CueService.Model.Result cueServiceModel)
        {

            ECUE_ReportModel reportModel = new ECUE_ReportModel();

            //reportModel.ClaimNumber = cueServiceModel.ClaimNumber;
            //reportModel.SearchDate = cueServiceModel.SearchDate;

            reportModel.SearchSummary = Translator.Translate(cueServiceModel.SearchSummary);
            reportModel.ResultsSummary = Translator.Translate(cueServiceModel.ResultsSummary);
            reportModel.SearchCriteria = Translator.Translate(cueServiceModel.SearchCriteria);
            reportModel.IncidentSummary = Translator.Translate(cueServiceModel.IncidentSummary);
            reportModel.CUE_Incidents = Translator.Translate(cueServiceModel.CUE_Incidents);

            return reportModel;
        }

        private static List<ECUEIncident> Translate(List<MDA.CueService.Model.CUEIncident> cueLstIncident)
        {
            List<ECUEIncident> lstIncident = new List<ECUEIncident>();

            foreach (var item in cueLstIncident)
            {
                ECUEIncident incident = new ECUEIncident();
                incident.GeneralData = Translator.Translate(item.GeneralData);
                incident.ClaimData = Translator.Translate(item.ClaimData);
                incident.IncidentId = item.IncidentId;
                incident.IncidentType = item.IncidentType;
                if (item.IncidentDetails != null) {
                    var iDetails = incident.IncidentDetails = new ECUEIncidentDetails();
                    iDetails.HospitalAttended = item.IncidentDetails.HospitalAttended;
                    iDetails.IllnessDiseaseDescription = item.IncidentDetails.IllnessDiseaseDescription;
                    iDetails.InjuryDescription = item.IncidentDetails.InjuryDescription;

                    incident.IncidentDetails = iDetails;
                }

                incident.CUEIncidentInvolvements = Translator.Translate(item.CUEIncidentInvolvements);
                if (item.CUEIncidentInvolvementVehicleDetails != null)
                    incident.CUEIncidentInvolvementVehicleDetails = Translator.Translate(item.CUEIncidentInvolvementVehicleDetails);
               
                if(item.Suppliers!=null)
                    incident.Suppliers = Translator.Translate(item.Suppliers);

                if (item.IncidentDetails != null)
                    incident.IncidentDetails = Translator.Translate(item.IncidentDetails);

                lstIncident.Add(incident);
            }

            return lstIncident;
        }

        private static ECUEGeneralData Translate(MDA.CueService.Model.CUEGeneralData cueGeneralData)
        {
            ECUEGeneralData cueGeneralDataInfo = new ECUEGeneralData();

            cueGeneralDataInfo.CatastropheRelated = cueGeneralData.CatastropheRelated;
            cueGeneralDataInfo.CauseOfLoss = cueGeneralData.CauseOfLoss;
            cueGeneralDataInfo.ClaimStatus = cueGeneralData.ClaimStatus;
            cueGeneralDataInfo.ClosedDate = cueGeneralData.ClosedDate;
            cueGeneralDataInfo.CollectivePolicyIndicator = cueGeneralData.CollectivePolicyIndicator;
            cueGeneralDataInfo.CUEPayments = Translator.Translate(cueGeneralData.CUEPayments);
            cueGeneralDataInfo.IncidentDescription = cueGeneralData.IncidentDescription;
            cueGeneralDataInfo.InsurerContactName = cueGeneralData.InsurerContactName;
            cueGeneralDataInfo.InsurerContactTel = cueGeneralData.InsurerContactTel;
            cueGeneralDataInfo.LossSetupDate = cueGeneralData.LossSetupDate;
            cueGeneralDataInfo.PolicyInceptionDate = cueGeneralData.PolicyInceptionDate;
            cueGeneralDataInfo.PolicyNumber = cueGeneralData.PolicyNumber;
            cueGeneralDataInfo.PolicyNumberID = cueGeneralData.PolicyNumberID;
            cueGeneralDataInfo.PolicyPeriodEndDate = cueGeneralData.PolicyPeriodEndDate;
            cueGeneralDataInfo.PolicyType = cueGeneralData.PolicyType;
            cueGeneralDataInfo.RiskAddressCity = cueGeneralData.RiskAddressCity;
            cueGeneralDataInfo.RiskAddressHouseName = cueGeneralData.RiskAddressHouseName;
            cueGeneralDataInfo.RiskAddressLocality = cueGeneralData.RiskAddressLocality;
            cueGeneralDataInfo.RiskAddressNumber = cueGeneralData.RiskAddressNumber;
            cueGeneralDataInfo.RiskAddressPostCode = cueGeneralData.RiskAddressPostCode;
            cueGeneralDataInfo.RiskAddressStreet = cueGeneralData.RiskAddressStreet;
            cueGeneralDataInfo.RiskAddressTown = cueGeneralData.RiskAddressTown;
            cueGeneralDataInfo.IncidentDate = cueGeneralData.IncidentDate;

            return cueGeneralDataInfo;
        }

        private static List<ECUEPayment> Translate(List<MDA.CueService.Model.CUEPayment> cuePaymentList)
        {
            List<ECUEPayment> lstPayments = new List<ECUEPayment>();

            foreach (var item in cuePaymentList)
            {
                ECUEPayment cuePayment = new ECUEPayment();
                cuePayment.IncidentId = item.IncidentId;
                cuePayment.PaymentAmount = item.PaymentAmount;
                cuePayment.PaymentType = item.PaymentType;
                lstPayments.Add(cuePayment);
            }

            return lstPayments;
        }

        private static ECUEIncidentDetails Translate(MDA.CueService.Model.CUEIncidentDetails cueIncidentDetails)
        {
            ECUEIncidentDetails cueIncidentDetail = new ECUEIncidentDetails();

            cueIncidentDetail.HospitalAttended = cueIncidentDetails.HospitalAttended;
            cueIncidentDetail.IllnessDiseaseDescription = cueIncidentDetails.IllnessDiseaseDescription;
            cueIncidentDetail.InjuryDescription = cueIncidentDetails.InjuryDescription;

            return cueIncidentDetail;

        }

        private static List<ECUESupplier> Translate(List<MDA.CueService.Model.CUESupplier> cueListSuppliers)
        {
            List<ECUESupplier> lstSuppliers = new List<ECUESupplier>();

            foreach (var item in cueListSuppliers)
            {
                ECUESupplier cueSupplier = new ECUESupplier();
                cueSupplier.AddressCity = item.AddressCity;
                cueSupplier.AddressHouseName = item.AddressHouseName;
                cueSupplier.AddressIndicator = item.AddressIndicator;
                cueSupplier.AddressLocality = item.AddressLocality;
                cueSupplier.AddressNumber = item.AddressNumber;
                cueSupplier.AddressPostCode = item.AddressPostCode;
                cueSupplier.AddressStreet = item.AddressStreet;
                cueSupplier.AddressTown = item.AddressTown;
                cueSupplier.CompanyName = item.CompanyName;
                cueSupplier.IncidentId = item.IncidentId;
                cueSupplier.PaymentAmount = item.PaymentAmount;
                cueSupplier.SupplierStatus = item.SupplierStatus;
                cueSupplier.Telephone = item.Telephone;
                cueSupplier.VAT = item.VAT;

                lstSuppliers.Add(cueSupplier);
            }

            return lstSuppliers;
        }

        private static List<ECUEIncidentInvolvementVehicleDetails> Translate(List<MDA.CueService.Model.CUEIncidentInvolvementVehicleDetails> cueListInvolvementVehicleDetails)
        {
            List<ECUEIncidentInvolvementVehicleDetails> lstIncidentInvolvementVehicleDetails = new List<ECUEIncidentInvolvementVehicleDetails>();

            foreach (var item in cueListInvolvementVehicleDetails)
            {
                ECUEIncidentInvolvementVehicleDetails incidentInvolvementVehicleDetail = new ECUEIncidentInvolvementVehicleDetails();
                incidentInvolvementVehicleDetail.AddressCity = item.AddressCity;
                incidentInvolvementVehicleDetail.AddressHouseName = item.AddressHouseName;
                incidentInvolvementVehicleDetail.AddressIndicator = item.AddressIndicator;
                incidentInvolvementVehicleDetail.AddressLocality = item.AddressLocality;
                incidentInvolvementVehicleDetail.AddressNumber = item.AddressNumber;
                incidentInvolvementVehicleDetail.AddressPostCode = item.AddressPostCode;
                incidentInvolvementVehicleDetail.AddressStreet = item.AddressStreet;
                incidentInvolvementVehicleDetail.AddressTown = item.AddressTown;
                incidentInvolvementVehicleDetail.Colour = item.Colour;
                incidentInvolvementVehicleDetail.CoverType = item.CoverType;
                incidentInvolvementVehicleDetail.DamageStatus = item.DamageStatus;
                incidentInvolvementVehicleDetail.Dob = item.Dob;
                incidentInvolvementVehicleDetail.Forename = item.Forename;
                incidentInvolvementVehicleDetail.Incident_Id = item.Incident_Id;
                incidentInvolvementVehicleDetail.LastName = item.LastName;
                incidentInvolvementVehicleDetail.Make = item.Make;
                incidentInvolvementVehicleDetail.MiddleName = item.MiddleName;
                incidentInvolvementVehicleDetail.Model = item.Model;
                incidentInvolvementVehicleDetail.NewForOldIndicator = item.NewForOldIndicator;
                incidentInvolvementVehicleDetail.NI_Number = item.NI_Number;
                incidentInvolvementVehicleDetail.Occupation = item.Occupation;
                incidentInvolvementVehicleDetail.PaymentAmount = item.PaymentAmount;
                incidentInvolvementVehicleDetail.RecoveredStatus = item.RecoveredStatus;
                incidentInvolvementVehicleDetail.RegistrationStatus = item.RegistrationStatus;
                incidentInvolvementVehicleDetail.Sex = item.Sex;
                incidentInvolvementVehicleDetail.Status = item.Status;
                incidentInvolvementVehicleDetail.Telephone = item.Telephone;
                incidentInvolvementVehicleDetail.Title = item.Title;
                incidentInvolvementVehicleDetail.Type = item.Type;
                incidentInvolvementVehicleDetail.VAT = item.VAT;
                incidentInvolvementVehicleDetail.VehicleIdentificationNumber = item.VehicleIdentificationNumber;
                incidentInvolvementVehicleDetail.VehicleRegistration = item.VehicleRegistration;
                incidentInvolvementVehicleDetail.VIN = item.VIN;

                lstIncidentInvolvementVehicleDetails.Add(incidentInvolvementVehicleDetail);
            }

            return lstIncidentInvolvementVehicleDetails;
        }

        private static List<ECUEIncidentInvolvement> Translate(List<MDA.CueService.Model.CUEIncidentInvolvement> cueListIncidentInvolvements)
        {
            List<ECUEIncidentInvolvement> lstIncidentInvolvements = new List<ECUEIncidentInvolvement>();

            foreach (var item in cueListIncidentInvolvements)
            {
                ECUEIncidentInvolvement incidentInvolvement = new ECUEIncidentInvolvement();
                incidentInvolvement.AddressCity = item.AddressCity;
                incidentInvolvement.AddressHouseName = item.AddressHouseName;
                incidentInvolvement.AddressIndicator = item.AddressIndicator;
                incidentInvolvement.AddressLocality = item.AddressLocality;
                incidentInvolvement.AddressNumber = item.AddressNumber;
                incidentInvolvement.AddressPostCode = item.AddressPostCode;
                incidentInvolvement.AddressStreet = item.AddressStreet;
                incidentInvolvement.AddressTown = item.AddressTown;
                incidentInvolvement.Dob = item.Dob;
                incidentInvolvement.Forename = item.Forename;
                incidentInvolvement.Incident_Id = item.Incident_Id;
                incidentInvolvement.LastName = item.LastName;
                incidentInvolvement.MiddleName = item.MiddleName;
                incidentInvolvement.NI_Number = item.NI_Number;
                incidentInvolvement.Occupation = item.Occupation;
                incidentInvolvement.PaymentAmount = item.PaymentAmount;
                incidentInvolvement.Sex = item.Sex;
                incidentInvolvement.Status = item.Status;
                incidentInvolvement.Telephone = item.Telephone;
                incidentInvolvement.Title = item.Title;
                incidentInvolvement.Type = item.Type;
                incidentInvolvement.VAT = item.VAT;

                lstIncidentInvolvements.Add(incidentInvolvement);
            }

            return lstIncidentInvolvements;
        }

        private static ECUEClaimData Translate(MDA.CueService.Model.CUEClaimData cueClaimData)
        {
            ECUEClaimData claimData = new ECUEClaimData();
            claimData.ClaimNumber = cueClaimData.ClaimNumber;
            claimData.Insurer = cueClaimData.Insurer;
            claimData.PolicyHolders =  Translator.Translate(cueClaimData.PolicyHolders);

            return claimData;
        }

        private static List<ECUEClaimDataPolicyHolder> Translate(List<MDA.CueService.Model.CUEClaimDataPolicyHolder> cueLstClaimDataPolicyHolders)
        {
            List<ECUEClaimDataPolicyHolder> lstClaimDataPolicyHolders = new List<ECUEClaimDataPolicyHolder>();

            foreach (var item in cueLstClaimDataPolicyHolders)
            {
                ECUEClaimDataPolicyHolder claimDataPolicyHolder = new ECUEClaimDataPolicyHolder();
                claimDataPolicyHolder.Address = item.Address;
                claimDataPolicyHolder.FullName = item.FullName;
                claimDataPolicyHolder.IncidentId = item.IncidentId;

                lstClaimDataPolicyHolders.Add(claimDataPolicyHolder);
            }

            return lstClaimDataPolicyHolders;
        }

        private static List<ECUEIncidentSummary> Translate(List<MDA.CueService.Model.CUEIncidentSummary> cueLstIncidentSummary)
        {
            List<ECUEIncidentSummary> lstIncidentSummary = new List<ECUEIncidentSummary>();

            foreach (var item in cueLstIncidentSummary)
            {
                ECUEIncidentSummary incidentSummary = new ECUEIncidentSummary();
                incidentSummary.From = item.From;
                incidentSummary.HH_Claims = item.HH_Claims;
                incidentSummary.IncidentId = item.IncidentId;
                incidentSummary.MO_Claims = item.MO_Claims;
                incidentSummary.PI_Claims = item.PI_Claims;
                incidentSummary.To = item.To;
                incidentSummary.CUE_Incidents = Translator.Translate(item.CUE_Incidents);

                lstIncidentSummary.Add(incidentSummary);

            }

            return lstIncidentSummary;
        }

        private static List<ECUESummaryIncident> Translate(List<MDA.CueService.Model.CUESummaryIncident> cueLstSummaryIncident)
        {
            List<ECUESummaryIncident> lstSummaryIncidents = new List<ECUESummaryIncident>();

            foreach(var item in cueLstSummaryIncident)
            {
                ECUESummaryIncident summaryIncident = new ECUESummaryIncident();
                summaryIncident.ClaimStatus = item.ClaimStatus;
                summaryIncident.Description = item.Description;
                summaryIncident.IncidentDate = item.IncidentDate;
                summaryIncident.IncidentId = item.IncidentId;
                summaryIncident.IncidentType = item.IncidentType;
                summaryIncident.Insurer = item.Insurer;
                summaryIncident.InvolvementType = item.InvolvementType;
                summaryIncident.MatchCode = item.MatchCode;
                summaryIncident.Subject = item.Subject;

                lstSummaryIncidents.Add(summaryIncident);

            }

            return lstSummaryIncidents;
        }

        private static ECueSearchCriteria Translate(MDA.CueService.Model.CueSearchCriteria cueSearchCriteria)
        {
            return new ECueSearchCriteria()
            {
                AddressCity = cueSearchCriteria.AddressCity,
                AddressHouseName = cueSearchCriteria.AddressHouseName,
                AddressLocality = cueSearchCriteria.AddressLocality,
                AddressNumber = cueSearchCriteria.AddressNumber,
                AddressPostCode = cueSearchCriteria.AddressPostCode,
                AddressStreet = cueSearchCriteria.AddressCity,
                AddressTown = cueSearchCriteria.AddressTown,
                Dob = cueSearchCriteria.Dob,
                DrivingLicenceNum = cueSearchCriteria.DrivingLicenceNum,
                Name = cueSearchCriteria.Name,
                NI_Number = cueSearchCriteria.NI_Number,
                VehicleReg = cueSearchCriteria.VehicleReg,
                VIN = cueSearchCriteria.VIN
            };
        }

        private static List<ECUEResultsSummary> Translate(List<MDA.CueService.Model.CUEResultsSummary> cueLstResultsSummary)
        {
            List<ECUEResultsSummary> lstResultsSummary = new List<ECUEResultsSummary>();

            foreach(var item in cueLstResultsSummary)
            {
                ECUEResultsSummary resultsSummary = new ECUEResultsSummary();
                resultsSummary.Incidents = item.Incidents;
                resultsSummary.Involved_Household_Claims = item.Involved_Household_Claims;
                resultsSummary.Involved_Motor_Claims = item.Involved_Motor_Claims;
                resultsSummary.Involved_PI_Claims = item.Involved_PI_Claims;

                lstResultsSummary.Add(resultsSummary);
            }

            return lstResultsSummary;
        }

        private static List<ECUESearchSummary> Translate(List<MDA.CueService.Model.CUESearchSummary> cueLstSearchSummary)
        {
            List<ECUESearchSummary> lstSearchSummary = new List<ECUESearchSummary>();

            foreach (var item in cueLstSearchSummary)
            {
                ECUESearchSummary searchSummary = new ECUESearchSummary();
                searchSummary.Cross_Enquiry = item.Cross_Enquiry;
                searchSummary.Household = item.Household;
                searchSummary.Involvement = item.Involvement;
                searchSummary.Motor = item.Motor;
                searchSummary.PI = item.PI;
                searchSummary.SearchDate = item.SearchDate;
                searchSummary.Subject = item.Subject;

                lstSearchSummary.Add(searchSummary);
            }
            return lstSearchSummary;
        }

        public static Interface.ESearchDetails Translate(MDA.RiskService.Model.ESearchDetails i)
        {
            return new Interface.ESearchDetails()
            {
                ClaimType = i.ClaimType,
                Date = i.Date,
                Involvement = i.Involvement,
                MatchedOn = i.MatchedOn,
                Title = i.Title,
                FirstName = i.FirstName,
                MiddleName = i.MiddleName,
                LastName = i.LastName,
                Address = i.Address,
                DateOfBirth = i.DateOfBirth,
                Postcode = i.Postcode,
                VehicleMake = i.VehicleMake,
                VehicleModel = i.VehicleModel,
                VehicleReg = i.VehicleReg,
                Reference = i.Reference,
                Source = i.Source,
                Status = i.Status
            };
        }
    }
}
