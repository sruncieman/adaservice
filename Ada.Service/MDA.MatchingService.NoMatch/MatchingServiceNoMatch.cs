﻿using System.Collections.Generic;
using System.Linq;
using MDA.MatchingService.Model;
using MDA.MatchingService.Interface;
using MDA.Common.Server;
using MDA.Pipeline.Model;
using MDA.DAL;

namespace MDA.MatchingService.NoMatch
{
    /// <summary>
    /// A debugging class. An implementation of IMatchingService that matches nothing.  Using this implementation
    /// means that nothing is matched so everything is inserted into the database.  This causes duplicates and
    /// nothing get linked together.
    /// </summary>
    public class MatchingService : IMatchingService
    {
        private bool _trace = false;
        private CurrentContext _ctx;
        private MdaDbContext _db;

        public MatchingService(CurrentContext ctx)
        {
            _ctx = ctx;
            _db = ctx.db as MdaDbContext;
            _trace = _ctx.TraceLoad;
        }

        private MatchResults CreateDummyResults(object o)
        {
            MatchResults _DummyResults = new MatchResults(o)
            {
                MatchIdList = new List<int>(),
            };

            return _DummyResults;
        }

        public CurrentContext CurrentContext
        {
            set
            {
                _ctx = value;
                _db = _ctx.db as MdaDbContext;
            }
        }

        public MatchResults MatchExistingAddress(int addressId, int? existingRiskClaimId)
        {
            return CreateDummyResults(null);
        }

        public MatchResults MatchExistingClaim(int incidentId, int? existingRiskClaimId)
        {
            return CreateDummyResults(null);
        }


        public MatchResults MatchClaim(PipelineMotorClaim xmlClaim, int? existingIncidentId, int? existingRiskClaimId)
        {
            return new MatchResults(xmlClaim);
        }
        public MatchResults MatchClaim(PipelinePIClaim xmlClaim, int? existingIncidentId, int? existingRiskClaimId)
        {
            return new MatchResults(xmlClaim);
        }

        public MatchResults MatchClaim(PipelineMobileClaim xmlClaim, int? existingIncidentId, int? existingRiskClaimId)
        {
            return new MatchResults(xmlClaim);
        }

        public MatchResults MatchEmailAddress(MDA.Pipeline.Model.PipelineEmail pipeEmail, int? existingRiskClaimId)
        {
            return CreateDummyResults(pipeEmail);
        }

        public MatchResults MatchPolicy(PipelinePolicy policy, int? IncidentId, int? existingRiskClaimId)
        {
            return CreateDummyResults(policy);
        }

        public MatchResults MatchPassportNumber(MDA.Pipeline.Model.PipelinePassport pipePassport, int? existingRiskClaimId)
        {
            return CreateDummyResults(pipePassport);
        }

        public MatchResults MatchNINumber(MDA.Pipeline.Model.PipelineNINumber pipeNiNumber, int? existingRiskClaimId)
        {
            return CreateDummyResults(pipeNiNumber);
        }

        public MatchResults MatchLicenseNumber(MDA.Pipeline.Model.PipelineDrivingLicense pipeDrivingLicense, int? existingRiskClaimId)
        {
            return CreateDummyResults(pipeDrivingLicense);
        }

        public MatchResults MatchWebSite(MDA.Pipeline.Model.PipelineWebSite pipeWebSite, int? existingRiskClaimId)
        {
            return CreateDummyResults(pipeWebSite);
        }

        //public void ProcessTelephoneNumber(PipelineTelephone telephone)
        //{
        //    var x = _db.uspGetMatchingTelephone(telephone.ClientSuppliedNumber).ToList();

        //    // one record.  With ID it was found. even if not found reformatted telephone returned
        //    if (x.Count() == 1)
        //    {
        //        telephone.AreaCode = x[0].AreaCode;
        //        telephone.FullNumber = x[0].Number;
        //        telephone.InternationalCode = x[0].IntCode;
        //    }

        //    if (telephone.FullNumber != null)
        //    {
        //        if (telephone.FullNumber.StartsWith("07"))
        //            telephone.TelephoneType_Id = (int)Common.Enum.TelephoneType.Mobile;
        //        else if (telephone.TelephoneType_Id == (int)Common.Enum.TelephoneType.Mobile)
        //            telephone.TelephoneType_Id = (int)Common.Enum.TelephoneType.Unknown;
        //    }
        //}

        public MatchResults MatchTelephoneNumber(PipelineTelephone telephone, int? existingRiskClaimId)
        {
            return CreateDummyResults(telephone);
        }

        public MatchResults MatchBankAccount(PipelineBankAccount bankAccount, int? existingRiskClaimId)
        {
            return CreateDummyResults(bankAccount);
        }

        public MatchResults MatchPaymentCard(PipelinePaymentCard paymentCard, int? existingRiskClaimId)
        {
            return CreateDummyResults(paymentCard);
        }

        public MatchResults MatchExistingPerson(int personId, int? existingRiskClaimId)
        {
            return new MatchResults(null);
        }

        public MatchResults MatchPerson(PipelinePerson person, PipelineVehicle vehicle, IPipelineClaim claim, int? existingRiskClaimId)
        {
            return CreateDummyResults(person);
        }

        public MatchResults MatchExistingVehicle(int vehicleId, int? existingRiskClaimId)
        {
            return new MatchResults(null);
        }

        public MatchResults MatchVehicle(PipelineVehicle vehicle, List<PipelineAddress> addresses, int? existingRiskClaimId)
        {
            return CreateDummyResults(vehicle);
        }

        public MatchResults MatchAddress(PipelineAddress address, string orgName, int? existingRiskClaimId)
        {
            return CreateDummyResults(address);
        }

        public MatchResults MatchExistingOrganisation(int orgId, int? existingRiskClaimId)
        {
            return new MatchResults(null);
        }

        public MatchResults MatchOrganisation(PipelineOrganisation organisation, int? existingRiskClaimId)
        {
            return CreateDummyResults(organisation);
        }

        public MatchResults MatchExistingBankAccount(int accountId, int? existingRiskClaimId)
        {
            return new MatchResults(null);
        }
        public MatchResults MatchExistingPaymentCard(int cardId, int? existingRiskClaimId)
        {
            return new MatchResults(null);
        }

        public MatchResults MatchExistingPolicy(int policyId, int? existingRiskClaimId)
        {
            return new MatchResults(null);
        }


        public MatchResults MatchSanctionsOrganisation(PipelineOrganisation organisation, int? existingBaseRiskClaimId)
        {
            return new MatchResults(null);
        }


        public MatchResults MatchSanctionsPerson(PipelinePerson person, int? existingBaseRiskClaimId)
        {
            return new MatchResults(null);
        }


        public MatchResults MatchPerson(PipelinePerson person, PipelineHandset handset, IPipelineClaim claim, int? existingBaseRiskClaimId)
        {
            throw new System.NotImplementedException();
        }

        public MatchResults MatchHandset(PipelineHandset handset, List<PipelineAddress> addresses, int? existingBaseRiskClaimId)
        {
            throw new System.NotImplementedException();
        }

        public MatchResults MatchHandsetIMEI(PipelineHandset handset, int? existingBaseRiskClaimId)
        {
            throw new System.NotImplementedException();
        }
    }
}
