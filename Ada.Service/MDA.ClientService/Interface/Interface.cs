﻿using MDA.Common.Server;
using RiskEngine.Scoring.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.RiskService.Model;
using MDA.Pipeline.Model;
using MDA.ExternalServices.Model;

namespace MDA.ClientService.Interface
{
    public interface IClientService
    {
        void BeforeCreate(CurrentContext ctx, int riskBatchId);

        void AfterCreate(CurrentContext ctx, int riskBatchId);

        void BeforeCleansing(CurrentContext ctx, RiskClaimToProcess claimToProcess);

        void AfterCleansing(CurrentContext ctx, RiskClaimToProcess claimToProcess, IPipelineClaim claimXml);

        void BeforeLoading(CurrentContext ctx, RiskClaimToProcess claimToProcess);

        void AfterLoading(CurrentContext ctx, RiskClaimToProcess claimToProcess);

        /// <summary>
        /// Callback that is called within the Transaction.  Throwing an exception from here will cause RollBack
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="res"></param>
        void BeforeExternal(CurrentContext ctx, ExternalServiceCallRequest req);

        /// <summary>
        /// Callback that is called within the Transaction.  Throwing an exception from here will cause RollBack
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="res"></param>
        void AfterExternal(CurrentContext ctx, ExternalServiceCallResponse res);

        /// <summary>
        /// Not called from within a transaction
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="claimToProcess"></param>
        /// <param name="scoreWithLiveRules"></param>
        void BeforeScore(CurrentContext ctx, RiskClaimToProcess claimToProcess, bool scoreWithLiveRules);

        /// <summary>
        /// Not called from within a transaction
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="claimToProcess"></param>
        /// <param name="scoreWithLiveRules"></param>
        /// <param name="unableToScore"></param>
        /// <param name="results"></param>
        void AfterScore(CurrentContext ctx, RiskClaimToProcess claimToProcess, bool scoreWithLiveRules, bool unableToScore, ClaimScoreResults results);
    }
}
