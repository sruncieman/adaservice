﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace MDA.Common.Proxy
{
    public class MyProxy : IWebProxy
    {
        public ICredentials Credentials
        {
            get 
            {
                string userName = ConfigurationManager.AppSettings["ProxyUsername"];
                string password = ConfigurationManager.AppSettings["ProxyPassword"];
                string domain = ConfigurationManager.AppSettings["ProxyUserDomain"];
           
                return new NetworkCredential(userName, password, domain); 
            }
            set { }
        }

        public Uri GetProxy(Uri destination)
        {
            string addr = ConfigurationManager.AppSettings["ProxyServerAndPort"];

            return new Uri(addr);
        }

        public bool IsBypassed(Uri host)
        {
            return false;
        }
    }


}
