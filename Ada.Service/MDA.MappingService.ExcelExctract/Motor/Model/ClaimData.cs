﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Excel.Motor.Model
{
     [IgnoreFirst(1)]
     [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimNumber;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? IncidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimStatus;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Insurer;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_Title;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_Forename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_Surname;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? PolicyHolder_DOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_Gender;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_NI;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_ContactNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_Email;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_AddressLine1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_AddressLine2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_AddressLine3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_AddressLine4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder_Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredVehicle_Registration;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredVehicle_Make;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredVehicle_Model;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_Title;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_Forename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_Surname;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? InsuredDriver_DOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_Gender;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_NI;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_ContactNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_EmailAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_Address1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_Address2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_Address3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_Address4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver_Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantHire_Registration;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantHire_Make;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantHire_Model;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? ClaimantHire_StartDate;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? ClaimantHire_EndDate;
        public String ClaimantVehicle_Registration;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantVehicle_Make;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantVehicle_Model;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Title;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Forename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Surname;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? Claimant_DOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Gender;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_NI;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_ContactNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Email;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Address1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Address2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Address3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Address4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHO_OrganisationName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHO_ContactNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHO_Email;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHO_Address1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHO_Address2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHO_Address3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHO_Address4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHO_Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitors_OrganisationName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitors_ContactNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitors_EmailAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitors_AddressLine1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitors_AddressLine2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitors_AddressLine3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitors_AddressLine4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitors_Postcode;
    }
}
