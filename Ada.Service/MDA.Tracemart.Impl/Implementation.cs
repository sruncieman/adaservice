﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.TracesmartService.Impl.TMServiceReference;
using MDA.TracesmartService.Interface;
using MDA.TracesmartService.Model;
using System.Net;
using MDA.Common.FileModel;
using MDA.Common.Server;
using MDA.DataService;
using System.Data.Entity;
using System.IO;
using System.Runtime.Serialization;
using MDA.RiskService;
using MDA.RiskService.Model;
using MDA.Common.Debug;
using System.Configuration;
using MDA.DAL;
using System.Xml;
using MDA.RiskService.Interface;
using MDA.ExternalServices.Model;
using MDA.ExternalService.Interface;

namespace MDA.TracesmartService.Impl
{
    //public class TracesmartException : SystemException
    //{
    //    public TracesmartException(string message) : base(message)
    //    {
    //    }
    //}

    public class TracesmartService : ITracesmartService, IExternalService
    {
        public TracesmartService()
        {
        }

        private string NewSource( string oldSource )
        {
            string s = "Tracesmart IDUAML";

            if (string.IsNullOrEmpty(oldSource)) 
                return s;

            return (!oldSource.Contains(s) && oldSource.Length + s.Length + 2 < 50) ? oldSource + "; " + s : oldSource;
        }

        private string NewSourceRef(string oldSource, string summaryId)
        {
            string s = summaryId; // "Tracesmart";

            if (string.IsNullOrEmpty(summaryId))
                return oldSource;

            if (string.IsNullOrEmpty(oldSource))
                return s;

            return (!oldSource.Contains(s) && oldSource.Length + s.Length + 2 < 50) ? oldSource + "; " + s : oldSource;
        }

        private string NewSourceDesc(string oldSource, string extra)
        {
            string s = extra;

            if (string.IsNullOrEmpty(extra))
                return oldSource;

            if (string.IsNullOrEmpty(oldSource))
                return s;

            return (!oldSource.Contains(s) && oldSource.Length + s.Length + 2 < 1024) ? oldSource + "; " + s : oldSource;
        }

        private string[] FormatAddress(DAL.Address address)
        {
            string[] lines = new string[6] { "", "", "", "", "", "" };

            int line = 0;
            bool needLine = false;

            if (!string.IsNullOrEmpty(address.Building))
            {
                lines[line] = address.Building.Trim();
                line++;
            }

            if (!string.IsNullOrEmpty(address.BuildingNumber))
            {
                lines[line] = address.BuildingNumber.Trim();
                needLine = true;
            }

            if (!string.IsNullOrEmpty(address.Street))
            {
                lines[line] = (lines[line] + " " + address.Street).Trim();
                line++;
                needLine = false;
            }

            if (!string.IsNullOrEmpty(address.Locality))
            {
                lines[line] = (lines[line] + " " + address.Locality).Trim();
                line++;
                needLine = false;
            }

            if (needLine) line++;

            if (!string.IsNullOrEmpty(address.Town))
            {
                lines[line] = address.Town.Trim();
                line++;
            }

            return lines;
        }

        /// <summary>
        /// Take the riskClaimId and uniqueId and work out which sub-services of the external service can be called.  Typically you would fetch all the data
        /// taking the uniqueId as the starting point. You would then work out which fields are available to call the sub-service.  You need to create an
        /// inityial mask of 0 and then or (|) into the mask the bit the spcifies the services that can be called. eg  mask |= 0x0001 sets the first bit
        /// </summary>
        /// <param name="ctx">Current Context</param>
        /// <param name="riskClaimId">RiskClaim_Id of this claim</param>
        /// <param name="uniqueId">The unique ID specific to this service</param>
        /// <returns>The bit mask and optional error message string</returns>
        public ExternalServicesMaskResponse BuildMaskOfServicesThatCanBeCalled(CurrentContext ctx, int riskClaimId, int uniqueId)
        {
            IRiskServices riskServices = new RiskServices(ctx);

            var riskClaim = (from x in ctx.db.RiskClaims where x.Id == riskClaimId select x).First();

            #region get the LIST of servicesToCall for this client. Also Name and Password of account

            string sname = ConfigurationManager.AppSettings["TracesmartServiceNameInDb"].ToString();

            var servicesToCall = (from x in ctx.db.RiskClient2RiskExternalServices
                                  where x.RiskClient_Id == ctx.RiskClientId && x.RiskExternalService.ServiceName == sname
                                  select new ServicesToCall() { Client2Services = x, ExternalService = x.RiskExternalService }).First();
            #endregion

            #region Get the PERSON record identified by the UniqueID which is the ID of a P2A link record. Abort if not found

            MDA.DAL.Person2Address p2a = ctx.db.Person2Address.Include("Address").Include("Person").Where(x => x.Id == uniqueId).First();

            if (p2a == null)
            {
                if (ctx.TraceLoad) ADATrace.WriteLine("P2A record not found");

                return new ExternalServicesMaskResponse()
                {
                    ServicesThatCanBeCalledMask = 0,
                    ErrorMessage = "P2A record not found"
                };
            }

            var dbPerson = p2a.Person;
            var dbAddress = p2a.Address;

            if (dbPerson == null || dbAddress == null)
            {
                if (ctx.TraceLoad) ADATrace.WriteLine("Person or Address record not found");

                return new ExternalServicesMaskResponse()
                {
                    ServicesThatCanBeCalledMask = 0,
                    ErrorMessage = "Person or Address record not found"
                };
            }

            #endregion

            #region Populate PersonInfo

            MDA.DAL.Person2BankAccount p2ba;
            MDA.DAL.Person2PaymentCard p2pc;
            MDA.DAL.Person2DrivingLicense p2dl;
            MDA.DAL.Person2NINumber p2ni;
            MDA.DAL.Person2Passport p2pass;
            MDA.DAL.Person2Telephone p2lt;
            MDA.DAL.Person2Telephone p2mt;

            PersonInfo pi = PopulatePersonInfo(riskClaimId, servicesToCall, dbPerson, dbAddress, p2a, out p2ba, out p2pc, out p2dl, out p2ni, out p2pass, out p2lt, out p2mt);
            #endregion

            #region Decide which services we need (and can) call.

            // Returns NULL and sets errorOut if the call cannot be made
            string serviceCallError;
            int serviceCallMask;
            var serviceDetails = BuildServiceDetails(ctx, pi, servicesToCall.Client2Services, out serviceCallError, out serviceCallMask);

            return new ExternalServicesMaskResponse()
            {
                ServicesThatCanBeCalledMask = serviceCallMask,
                ErrorMessage = serviceCallError
            };
            #endregion
        }

        private PersonInfo PopulatePersonInfo(int riskClaimId, ServicesToCall servicesToCall, MDA.DAL.Person dbPerson, MDA.DAL.Address dbAddress, MDA.DAL.Person2Address p2a,
                        out MDA.DAL.Person2BankAccount p2ba,
                        out MDA.DAL.Person2PaymentCard p2pc,
                        out MDA.DAL.Person2DrivingLicense p2dl,
                        out MDA.DAL.Person2NINumber p2ni,
                        out MDA.DAL.Person2Passport p2pass,
                        out MDA.DAL.Person2Telephone p2lt,
                        out MDA.DAL.Person2Telephone p2mt)
        {
            PersonInfo pi = new PersonInfo();

            #region Person
            pi.Forename = dbPerson.FirstName;
            pi.Surname = dbPerson.LastName;
            pi.Middle = dbPerson.MiddleName;

            string[] addressLines = FormatAddress(dbAddress);

            pi.Address1 = addressLines[0];
            pi.Address2 = addressLines[1];
            pi.Address3 = addressLines[2];
            pi.Address4 = addressLines[3];
            pi.Address5 = addressLines[4];
            pi.Address5 = addressLines[5];

            pi.Postcode = dbAddress.PostCode;
            pi.IKey = p2a.TracesmartIKey;

            if (dbPerson.DateOfBirth.HasValue)
            {
                pi.DOB = string.Format("{0:00}-{1:00}-{2:00}", dbPerson.DateOfBirth.Value.Year,
                    dbPerson.DateOfBirth.Value.Month, dbPerson.DateOfBirth.Value.Day);
            }

            int gender = dbPerson.Gender_Id;

            if (gender == (int)MDA.Common.Enum.Gender.Unknown &&
                    dbPerson.Salutation_Id != (int)MDA.Common.Enum.Salutation.Unknown)
            {
                gender = (int)MDA.Common.Helpers.GenderHelper.Salutation2Gender((MDA.Common.Enum.Salutation)dbPerson.Salutation_Id);
            }

            if (gender != (int)Common.Enum.Gender.Unknown)
                pi.Gender = (gender == (int)Common.Enum.Gender.Male) ? "M" : "F";
            #endregion

            #region BankAccount
            p2ba = null;

            if (servicesToCall.Client2Services.Tracesmart_Use_BankAccount > 0) //..Convert.ToBoolean(ConfigurationManager.AppSettings["Tracesmart_Use_BankAccount"]))
            {
                p2ba = dbPerson.Person2BankAccount.Where(x => x.RiskClaim_Id == riskClaimId && x.ADARecordStatus == (byte)ADARecordStatus.Current).FirstOrDefault();

                if (p2ba != null)
                {
                    pi.AccountNumber = p2ba.BankAccount.AccountNumber;
                    pi.Sortcode = p2ba.BankAccount.SortCode;
                }
            }
            #endregion

            #region Payment Card
            p2pc = null;

            if (servicesToCall.Client2Services.Tracesmart_Use_CardNumber > 0)
            {
                p2pc = dbPerson.Person2PaymentCard.Where(x => x.RiskClaim_Id == riskClaimId && x.ADARecordStatus == (byte)ADARecordStatus.Current).FirstOrDefault();

                if (p2pc != null)
                    pi.CardNumber = p2pc.PaymentCard.PaymentCardNumber;
            }
            #endregion

            #region Driving License
            p2dl = null;

            if (servicesToCall.Client2Services.Tracesmart_Use_Driving > 0)
            {
                p2dl = dbPerson.Person2DrivingLicense.Where(x => x.RiskClaim_Id == riskClaimId && x.ADARecordStatus == (byte)ADARecordStatus.Current).FirstOrDefault();

                if (p2dl != null && p2dl.DrivingLicense.DriverNumber.Length == 16)
                {
                    pi.Drivinglicence1 = p2dl.DrivingLicense.DriverNumber.Substring(0, 5);
                    pi.Drivinglicence2 = p2dl.DrivingLicense.DriverNumber.Substring(5, 6);
                    pi.Drivinglicence3 = p2dl.DrivingLicense.DriverNumber.Substring(11, 5);
                }
            }
            #endregion

            #region NI Number
            p2ni = null;

            if (servicesToCall.Client2Services.Tracesmart_Use_NI > 0)
            {
                p2ni = dbPerson.Person2NINumber.Where(x => x.RiskClaim_Id == riskClaimId && x.ADARecordStatus == (byte)ADARecordStatus.Current).FirstOrDefault();

                if (p2ni != null)
                    pi.NI = p2ni.NINumber.NINumber1;
            }
            #endregion

            #region Passport
            p2pass = null;

            if (servicesToCall.Client2Services.Tracesmart_Use_Passport > 0) 
            {
                //p2pass = dbPerson.Person2Passport.Where(x => x.RiskClaim_Id == riskClaimId && x.ADARecordStatus == (byte)ADARecordStatus.Current).FirstOrDefault();

                //if (p2pass != null)
                //    pi.Passport1 = p2pass.Passport.PassportNumber;
            }
            #endregion

            #region Landline Telephone
            p2lt = null;

            if (servicesToCall.Client2Services.Tracesmart_Use_Telephone > 0) 
            {
                p2lt = dbPerson.Person2Telephone.Where(x => x.RiskClaim_Id == riskClaimId
                                && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                && x.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.Landline).FirstOrDefault();

                if (p2lt != null)
                    pi.LandlineTelephone = p2lt.Telephone.TelephoneNumber;
            }
            #endregion

            #region Mobile Number
            p2mt = null;

            if (servicesToCall.Client2Services.Tracesmart_Use_Mobile > 0) 
            {
                p2mt = dbPerson.Person2Telephone.Where(x => x.RiskClaim_Id == riskClaimId
                                && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                && x.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.Mobile).FirstOrDefault();

                if (p2mt != null)
                    pi.MobileTelephone = p2mt.Telephone.TelephoneNumber;
            }
            #endregion

            #region Address

            //int aCount = 0;

            //MDA.DAL.Person2Address p2a = db.Person2Address.Include("Address").Where(x => x.Id == exServiceReq.Unique_Id).First();

            //pi.Address1 = p2a.Address.BuildingNumber + " " + p2a.Address.Street;
            //pi.Postcode = p2a.Address.PostCode;
            //pi.IKey     = p2a.TracesmartIKey;

            //var Addresses = dbPerson.Person2Address.Where(x => x.RiskClaim_Id == riskClaimId && x.ADARecordStatus == (byte)ADARecordStatus.Current);
            //if (Addresses != null)
            //{
            //    foreach (var person2Address in Addresses)
            //    {
            //        var a = person2Address.Address;

            //        if (aCount == 0 || person2Address.AddressLinkType_Id == (int)MDA.Common.Enum.AddressLinkType.CurrentAddress)
            //        {
            //            aCount++;

            //            pi.Address1 = a.BuildingNumber + " " + a.Street;
            //            pi.Postcode = a.PostCode;

            //            p2a = person2Address;
            //            pi.IKey = person2Address.TracesmartIKey;
            //        }
            //    }
            //}
            #endregion

            return pi;
        }

        /// <summary>
        /// This method accepts the 3-field KEY in the request object that identifies the UNQIUE record in the
        /// RiskExternalServicesRequests table. From that information it will read data direct from ADA, make an external call, update ADA
        /// directly and return a populated Response object.
        /// return a populated Response object.
        /// </summary>
        /// <param name="ctx">The current context</param>
        /// <param name="resr">An object holding the 3-key index into the RiskExternalServicesRequests table</param>
        /// <returns>A Response object that contains the call status, any errors and the data sent and received via the service</returns>
        public ExternalServiceCallResponse ProcessPipelineClaim(CurrentContext ctx, ExternalServiceCallRequest exServiceReq)
        {
            IRiskServices riskServices = new RiskServices(ctx);

            int riskClaimId = exServiceReq.RiskClaim_Id;

            var riskClaim = (from x in ctx.db.RiskClaims where x.Id == riskClaimId select x).First();

            #region get the LIST of servicesToCall for this client. Also Name and Password of account

            string sname = ConfigurationManager.AppSettings["TracesmartServiceNameInDb"].ToString();

            var servicesToCall = (from x in ctx.db.RiskClient2RiskExternalServices
                                  where x.RiskClient_Id == ctx.RiskClientId && x.RiskExternalService.ServiceName == sname
                                  select new ServicesToCall() { Client2Services = x, ExternalService = x.RiskExternalService }).First();

            string username = servicesToCall.Client2Services.ServiceUserName;
            string password = servicesToCall.Client2Services.ServicePassword;
            #endregion

            #region Get the PERSON record identified by the UniqueID which is the ID of a P2A link record. Abort if not found
            //var dbPerson = db.Person2Address.Include("Address").Include("Person").Where(x => x.Id == exServiceReq.Unique_Id).First();

            MDA.DAL.Person2Address p2a = ctx.db.Person2Address.Include("Address").Include("Person").Where(x => x.Id == exServiceReq.Unique_Id).First();

            if (p2a == null)
            {
                if (ctx.TraceLoad) ADATrace.WriteLine("P2A record not found");

                return new ExternalServiceCallResponse { CallStatus = RiskExternalServiceCallStatus.UniqueIdDataNotFound };
            }

            var dbPerson = p2a.Person;
            var dbAddress = p2a.Address;

            if (dbPerson == null || dbAddress == null)
            {
                if (ctx.TraceLoad) ADATrace.WriteLine("Person or Address record not found");

                return new ExternalServiceCallResponse { CallStatus = RiskExternalServiceCallStatus.PrimaryEntityNotFound };
            }

            if (ctx.TraceLoad) ADATrace.WriteLine("Process Person [" + dbPerson.Id + "] Starting...ClientClaimRef[" + riskClaim.ClientClaimRefNumber + "]");

            #endregion

            #region Populate PersonInfo

            MDA.DAL.Person2BankAccount p2ba;
            MDA.DAL.Person2PaymentCard p2pc;
            MDA.DAL.Person2DrivingLicense p2dl;
            MDA.DAL.Person2NINumber p2ni;
            MDA.DAL.Person2Passport p2pass;
            MDA.DAL.Person2Telephone p2lt;
            MDA.DAL.Person2Telephone p2mt;

            PersonInfo pi = PopulatePersonInfo(riskClaimId, servicesToCall, dbPerson, dbAddress, p2a, out p2ba, out p2pc, out p2dl, out p2ni, out p2pass, out p2lt, out p2mt);
            #endregion

            #region If no P2A link abort
            //if (p2a == null)
            //{
            //    if (ctx.TraceLoad) ADATrace.WriteLine("Person has no Address link, so skip directly to next person on claim. Set ABORTED service bit");
            //    riskServices.CreateTracesmartServiceCallHistory(db, null, resr.RiskClaim_Id, resr.RiskExternalService_Id, resr.Unique_Id, RiskExternalServiceCallStatus.P2ALinkNotFound);
            //    return;
            //}
            #endregion

            #region Decide which services we need (and can) call. Abort if none

            // Returns NULL and sets errorOut if the call cannot be made
            string serviceCallError;
            int serviceCallMask;
            var serviceDetails = BuildServiceDetails(ctx, pi, servicesToCall.Client2Services, out serviceCallError, out serviceCallMask);

            if (serviceDetails == null)
            {
                if (ctx.TraceLoad) ADATrace.WriteLine("Call aborted. Set ABORTED service bit");

                return new ExternalServiceCallResponse
                {
                    CallStatus = RiskExternalServiceCallStatus.Aborted,
                    ServicesCalledMask = 0,
                    ErrorMessage = serviceCallError,
                    RequestData = pi
                };
            }
            #endregion

            try
            {
                ResultInfo results = CallTracemartService(serviceDetails, pi, username, password, ctx.TraceLoad);

                #region If NO Results abort
                if (results == null)
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("No Results");

                    return new ExternalServiceCallResponse 
                    { 
                        CallStatus = RiskExternalServiceCallStatus.NoResults,
                        ServicesCalledMask = serviceCallMask,
                        ErrorMessage = "No Results",
                        RequestData = pi
                    };
                }
                #endregion

                #region If NO Summary or STATUS is False abort

                if (results.Summary == null || results.Summary.Status == false)
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Results Summary False");

                    return new ExternalServiceCallResponse 
                    { 
                        CallStatus = RiskExternalServiceCallStatus.ResultsFalse,
                        ServicesCalledMask = serviceCallMask,
                        ErrorMessage = "Results Summary False",
                        RequestData = pi
                    };
                }

                #endregion

                #region Translate Results (remove what we don't use)
                if (ctx.TraceLoad) ADATrace.Write("Translate results..");

                FilteredResults r = Translator.Translate(results);

                if (ctx.TraceLoad) ADATrace.WriteLine("Done");
                #endregion

                #region Update Person in database

                #region Update Person
                bool personChanged = false;
                if (r.Person_SanctionDate != null && MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(r.Person_SanctionDate) != dbPerson.SanctionDate)
                {
                    personChanged = true;
                    dbPerson.SanctionDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(r.Person_SanctionDate);
                }

                if (r.Person_SanctionSource != null && r.Person_SanctionSource != dbPerson.SanctionSource)
                {
                    personChanged = true;
                    dbPerson.SanctionSource = r.Person_SanctionSource;
                }

                if (personChanged)
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person Changed");

                    dbPerson.ModifiedBy = ctx.Who;
                    dbPerson.ModifiedDate = DateTime.Now;
                    dbPerson.Source = NewSource(dbPerson.Source);
                    dbPerson.SourceReference = NewSourceRef(dbPerson.SourceReference, results.Summary.Id);
                }
                else
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person Unchanged");
                }
                #endregion

                #region Person2Address
                if (p2a != null)
                {
                    bool changed = false;

                    //if (r.P2A_CCJAmount != null)
                    //{
                    //    decimal newValue = 0;

                    //    try
                    //    {
                    //        newValue = Convert.ToDecimal(r.P2A_CCJAmount);

                    //        if (newValue != p2a.CCJAmount)
                    //        {
                    //            changed = true;
                    //            p2a.CCJAmount = newValue;
                    //        }
                    //    }
                    //    catch (Exception)
                    //    {
                    //        if (ctx.TraceLoad) ADATrace.WriteLine("Exception: CCJAmount would not convert. Discarded.");
                    //    }
                    //}

                    //if (r.P2A_CCJDate != null)
                    //{
                    //    DateTime? newDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(r.P2A_CCJDate);

                    //    if (newDate == null)
                    //    {
                    //        if (ctx.TraceLoad) ADATrace.WriteLine("Exception: CCJDate would not convert. Discarded.");
                    //    }
                    //    else if (newDate != p2a.CCJDate)
                    //    {
                    //        p2a.CCJDate = newDate;
                    //        changed = true;
                    //    }
                    //}

                    //if (r.P2A_CCJEndDate != null)
                    //{
                    //    DateTime? newDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(r.P2A_CCJEndDate);

                    //    if (newDate == null)
                    //    {
                    //        if (ctx.TraceLoad) ADATrace.WriteLine("Exception: CCJEndDate would not convert. Discarded.");
                    //    }
                    //    else if (newDate != p2a.CCJEndDate)
                    //    {
                    //        p2a.CCJEndDate = newDate;
                    //        changed = true;
                    //    }
                    //}

                    //if (r.P2A_CCJType != null && p2a.CCJType != r.P2A_CCJType)
                    //{
                    //    changed = true;
                    //    p2a.CCJType = r.P2A_CCJType;
                    //}

                    if (r.P2A_CCJHistory != null && p2a.CCJHistory != r.P2A_CCJHistory)
                    {
                        changed = true;
                        p2a.CCJHistory = r.P2A_CCJHistory;
                    }

                    if (r.P2A_CredivaCheck != null && p2a.CredivaCheck != r.P2A_CredivaCheck && servicesToCall.Client2Services.Tracesmart_Use_Crediva > 0) //Convert.ToBoolean(ConfigurationManager.AppSettings["Tracesmart_Use_Crediva"]))
                    {
                        changed = true;
                        p2a.CredivaCheck = r.P2A_CredivaCheck;
                    }

                    if (r.P2A_ExperianDOB != null && p2a.ExperianDOB != r.P2A_ExperianDOB)
                    {
                        changed = true;
                        p2a.ExperianDOB = r.P2A_ExperianDOB;
                    }

                    if (r.P2A_GoneAway != null && p2a.GoneAway != r.P2A_GoneAway)
                    {
                        changed = true;
                        p2a.GoneAway = r.P2A_GoneAway;
                    }

                    if (r.P2A_InsolvencyHistory != null && p2a.Insolvency != r.P2A_InsolvencyHistory)
                    {
                        changed = true;
                        p2a.InsolvencyHistory = r.P2A_InsolvencyHistory;
                    }

                    //if (r.P2A_InsolvencyStartDate != null)
                    //{
                    //    DateTime? newDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(r.P2A_InsolvencyStartDate);

                    //    if (newDate == null)
                    //    {
                    //        if (ctx.TraceLoad) ADATrace.WriteLine("Exception: InsolvencyStartDate would not convert. Discarded.");
                    //    }
                    //    else if (newDate != p2a.InsolvencyStartDate)
                    //    {
                    //        p2a.InsolvencyStartDate = newDate;
                    //        changed = true;
                    //    }
                    //}

                    //if (r.P2A_InsolvencyStatus != null && p2a.InsolvencyStatus != r.P2A_InsolvencyStatus)
                    //{
                    //    changed = true;
                    //    if (r.P2A_InsolvencyStatus.Length > 250)
                    //        p2a.InsolvencyStatus = r.P2A_InsolvencyStatus.Substring(0, 250);
                    //    else
                    //        p2a.InsolvencyStatus = r.P2A_InsolvencyStatus;

                    //}


                    if (r.P2A_DeathScreenDoD != null && p2a.DeathScreenDoD != r.P2A_DeathScreenDoD)
                    {
                        changed = true;
                        p2a.DeathScreenDoD = r.P2A_DeathScreenDoD;
                    }

                    if (r.P2A_DeathScreenMatchType != null && p2a.DeathScreenMatchType != r.P2A_DeathScreenMatchType)
                    {
                        changed = true;
                        p2a.DeathScreenMatchType = r.P2A_DeathScreenMatchType;
                    }

                    if (r.P2A_DeathScreenRegNo != null && p2a.DeathScreenRegNo != r.P2A_DeathScreenRegNo)
                    {
                        changed = true;
                        p2a.DeathScreenRegNo = r.P2A_DeathScreenRegNo;
                    }

                    if (r.P2A_TracesmartDOB != null && p2a.TracesmartDOB != r.P2A_TracesmartDOB)
                    {
                        changed = true;
                        p2a.TracesmartDOB = r.P2A_TracesmartDOB;
                    }

                    if (r.P2A_TSIDUAMLResult != null && p2a.TSIDUAMLResult != r.P2A_TSIDUAMLResult.ToUpper())
                    {
                        changed = true;
                        p2a.TSIDUAMLResult = r.P2A_TSIDUAMLResult.ToUpper();
                    }

                    if (r.ADR_TracesmartSource != null && p2a.TracesmartAddrSource != r.ADR_TracesmartSource.ToUpper())
                    {
                        changed = true;
                        p2a.TracesmartAddrSource = r.ADR_TracesmartSource.ToUpper();
                    }

                    p2a.TracesmartIKey = r.IKey;

                    if (!string.IsNullOrEmpty(r.P2A_AddressMatchType))
                    {
                        switch (r.P2A_AddressMatchType.ToUpper()[0])
                        {
                            case 'F':
                                if (p2a.LinkConfidence != (int)MDA.DataService.LinkConfidence.Confirmed)
                                {
                                    changed = true;
                                    p2a.LinkConfidence = (int)MDA.DataService.LinkConfidence.Confirmed;
                                }
                                break;
                            case 'P':
                                if (p2a.LinkConfidence != (int)MDA.DataService.LinkConfidence.Unconfirmed)
                                {
                                    changed = true;
                                    p2a.LinkConfidence = (int)MDA.DataService.LinkConfidence.Unconfirmed;
                                }
                                break;
                            default:
                                if (ctx.TraceLoad) ADATrace.WriteLine("P2A_AddressMatchType has invalid value (not F or P)");
                                break;
                        }
                    }

                    if (changed)
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Person2Address Changed");

                        p2a.ModifiedBy = ctx.Who;
                        p2a.ModifiedDate = DateTime.Now;
                        p2a.FiveGrading = "B21";
                        p2a.Source = NewSource(p2a.Source);
                        p2a.SourceReference = NewSourceRef(p2a.SourceReference, results.Summary.Id);

                        if (results.Address != null)
                            p2a.SourceDescription = NewSourceDesc(p2a.SourceDescription, results.Address.Source);
                        else
                        {
                            if (ctx.TraceLoad) ADATrace.WriteLine("Results.Address null, P2A SourceDescription not updated");
                        }
                    }
                    else
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Person2Address Unchanged");
                    }

                    if (r.ADR_Validated)
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Address Changed");

                        p2a.Address.MosaicCode = r.ADR_MosaicCode;
                        p2a.Address.PropertyType = r.ADR_PropertyType;
                        p2a.Address.Source = NewSource(p2a.Address.Source);
                        p2a.Address.SourceReference = NewSourceRef(p2a.Address.SourceReference, results.Summary.Id);
                    }
                    else
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Address Unchanged");
                    }
                }
                #endregion

                #region Person2BankAccount
                if (p2ba != null && r.P2BA_Validated != null && servicesToCall.Client2Services.Tracesmart_Use_BankAccount > 0) //Convert.ToBoolean(ConfigurationManager.AppSettings["Tracesmart_Use_BankAccount"]))
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person2Bank Changed");

                    p2ba.Validated = r.P2BA_Validated;
                    p2ba.FiveGrading = "B21";
                    p2ba.Source = NewSource(p2ba.Source);
                    p2ba.SourceReference = NewSourceRef(p2ba.SourceReference, results.Summary.Id);
                    p2ba.ModifiedBy = ctx.Who;
                    p2ba.ModifiedDate = DateTime.Now;
                }
                else
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person2Bank Unchanged");
                }
                #endregion

                #region Person2DrivingLicense
                if (p2dl != null && r.P2DL_Validated != null && servicesToCall.Client2Services.Tracesmart_Use_Driving > 0) //Convert.ToBoolean(ConfigurationManager.AppSettings["Tracesmart_Use_Driving"]))
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person2License Changed");

                    p2dl.Validated = r.P2DL_Validated;
                    p2dl.FiveGrading = "B21";
                    p2dl.Source = NewSource(p2dl.Source);
                    p2dl.SourceReference = NewSourceRef(p2dl.SourceReference, results.Summary.Id);
                    p2dl.ModifiedBy = ctx.Who;
                    p2dl.ModifiedDate = DateTime.Now;
                }
                else
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person2License Unchanged");
                }
                #endregion

                #region Person2NINumber
                if (p2ni != null && r.P2NI_Validated != null && servicesToCall.Client2Services.Tracesmart_Use_NI > 0) //Convert.ToBoolean(ConfigurationManager.AppSettings["Tracesmart_Use_NI"]))
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person2NI Changed");

                    p2ni.Validated = r.P2NI_Validated;
                    p2ni.FiveGrading = "B21";
                    p2ni.Source = NewSource(p2ni.Source);
                    p2ni.SourceReference = NewSourceRef(p2ni.SourceReference, results.Summary.Id);
                    p2ni.ModifiedBy = ctx.Who;
                    p2ni.ModifiedDate = DateTime.Now;
                }
                else
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person2NI Unchanged");
                }
                #endregion

                #region Person2Passport
                if (p2pass != null && servicesToCall.Client2Services.Tracesmart_Use_Passport > 0) //Convert.ToBoolean(ConfigurationManager.AppSettings["Tracesmart_Use_Passport"]))
                {
                    bool changed = false;

                    if (r.P2PP_DOBValid != null)
                    {
                        p2pass.DOBValid = r.P2PP_DOBValid;
                        changed = true;
                    }

                    if (r.P2PP_MRZValid != null)
                    {
                        p2pass.MRZValid = r.P2PP_MRZValid;
                        changed = true;
                    }

                    if (r.P2PP_GenderValid != null)
                    {
                        p2pass.GenderValid = r.P2PP_GenderValid;
                        changed = true;
                    }

                    if (changed)
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Person2Passport Changed");

                        p2pass.FiveGrading = "B21";
                        p2pass.Source = NewSource(p2pass.Source);
                        p2pass.SourceReference = NewSourceRef(p2pass.SourceReference, results.Summary.Id);
                        p2pass.ModifiedBy = ctx.Who;
                        p2pass.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Person2Passport Unchanged");
                    }
                }
                #endregion

                #region Person2PaymentCard
                if (p2pc != null && r.P2PC_CardNumberValid != null && servicesToCall.Client2Services.Tracesmart_Use_CardNumber > 0) //Convert.ToBoolean(ConfigurationManager.AppSettings["Tracesmart_Use_CardNumber"]))
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person2PaymentCard Changed");

                    p2pc.CardNumberValid = r.P2PC_CardNumberValid;
                    p2pc.FiveGrading = "B21";
                    p2pc.Source = NewSource(p2pc.Source);
                    p2pc.SourceReference = NewSourceRef(p2pc.SourceReference, results.Summary.Id);
                    p2pc.ModifiedBy = ctx.Who;
                    p2pc.ModifiedDate = DateTime.Now;
                }
                else
                {
                    if (ctx.TraceLoad) ADATrace.WriteLine("Person2PaymentCard Unchanged");
                }
                #endregion

                #region Person2landline
                if (p2lt != null && servicesToCall.Client2Services.Tracesmart_Use_Telephone > 0) //Convert.ToBoolean(ConfigurationManager.AppSettings["Tracesmart_Use_Telephone"]))
                {
                    bool changed = false;

                    if (r.P2T_Validated != null)
                    {
                        changed = true;
                        p2lt.Validated = r.P2T_Validated;
                    }

                    if (r.P2T_TelephoneStatus != null)
                    {
                        changed = true;
                        p2lt.Status = r.P2T_TelephoneStatus;
                    }

                    if (changed)
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Person2LandLine Changed");

                        p2lt.FiveGrading = "B21";
                        p2lt.Source = NewSource(p2lt.Source);
                        p2lt.SourceReference = NewSourceRef(p2lt.SourceReference, results.Summary.Id);
                        p2lt.ModifiedBy = ctx.Who;
                        p2lt.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Person2LandLine Unchanged");
                    }
                }
                #endregion

                #region Person2Mobile
                if (p2mt != null && servicesToCall.Client2Services.Tracesmart_Use_Mobile > 0) //Convert.ToBoolean(ConfigurationManager.AppSettings["Tracesmart_Use_Mobile"]))
                {
                    bool changed = false;

                    if (r.P2T_MobileStatus != null)
                    {
                        changed = true;
                        p2mt.Status = r.P2T_MobileStatus;
                    }

                    if (r.P2T_CurrentLocation != null)
                    {
                        changed = true;
                        p2mt.CurrentLocation = r.P2T_CurrentLocation;
                    }

                    if (changed)
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Person2Mobile Changed");

                        p2mt.FiveGrading = "B21";
                        p2mt.Source = NewSource(p2mt.Source);
                        p2mt.SourceReference = NewSourceRef(p2mt.SourceReference, results.Summary.Id);
                        p2mt.ModifiedBy = ctx.Who;
                        p2mt.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        if (ctx.TraceLoad) ADATrace.WriteLine("Person2Mobile Unchanged");
                    }
                }
                #endregion

                if (ctx.TraceLoad) ADATrace.WriteLine("Database update complete");

                var retData = new Dictionary<string, object>();
                retData.Add("Return", results);

                return new ExternalServiceCallResponse
                { 
                    CallStatus = RiskExternalServiceCallStatus.Success, 
                    ServicesCalledMask = serviceCallMask, 
                    RequestData = pi,
                    ReturnedData = retData
                };
                #endregion
            }
            catch (Exception)
            {
                if (ctx.TraceLoad) ADATrace.WriteLine("Handling Unexpected error, queue for retry");

                return new ExternalServiceCallResponse 
                { 
                    CallStatus = RiskExternalServiceCallStatus.FailedNeedsRetry, 
                    ServicesCalledMask = serviceCallMask, 
                    ErrorMessage = "Unexpected Error, Retry required",
                    RequestData = pi
                };
            }
        }

        private string SerializeParam(TMServiceReference.PersonDetails info)
        {
            MemoryStream ms = new MemoryStream();

            DataContractSerializer ser = new DataContractSerializer(info.GetType());

            var settings = new XmlWriterSettings() { Indent = true, IndentChars = "\t", NewLineChars = "\n" };

            using (var w = XmlWriter.Create(ms, settings))
                ser.WriteObject(w, info);
    
            ms.Position = 0;
            var sr = new StreamReader(ms);
            return sr.ReadToEnd();
        }

        private string SerializeResults(TMServiceReference.Result info)
        {
            MemoryStream ms = new MemoryStream();

            DataContractSerializer ser = new DataContractSerializer(info.GetType());

            var settings = new XmlWriterSettings() { Indent = true, IndentChars = "\t", NewLineChars = "\n" };

            using (var w = XmlWriter.Create(ms, settings))
                ser.WriteObject(w, info);

            ms.Position = 0;
            var sr = new StreamReader(ms);
            return sr.ReadToEnd();
        }

        private ServiceDetails BuildServiceDetails(CurrentContext ctx, PersonInfo personInfo, MDA.DAL.RiskClient2RiskExternalServices extService, out string errorMsg, out int servicesCalledMask)
        {
            var serviceDetails = new ServiceDetails();

            List<string> errors = new List<string>();
            servicesCalledMask = 0;
            errorMsg = "";

            bool haveAllReqdFields = !string.IsNullOrEmpty(personInfo.Forename)
                                  && !string.IsNullOrEmpty(personInfo.Surname)
                                  && !string.IsNullOrEmpty(personInfo.Address1)
                                  && !string.IsNullOrEmpty(personInfo.Postcode);

            bool haveAllReqdFieldsAndDob = haveAllReqdFields && !string.IsNullOrEmpty(personInfo.DOB);

            serviceDetails.address = false;
            if (extService.Tracesmart_Use_Address > 0)
            {
                if (haveAllReqdFields)
                {
                    serviceDetails.address = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Address;
                }
                else if (extService.Tracesmart_Use_Address == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("Address");
                }
            }

            serviceDetails.deathscreen = false;
            if (extService.Tracesmart_Use_DeathScreen > 0)
            {
                if (haveAllReqdFieldsAndDob)
                {
                    serviceDetails.deathscreen = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_DeathScreen;
                }
                else if (extService.Tracesmart_Use_DeathScreen == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("DeathScreen");
                }
            }

            serviceDetails.dob = false;
            if (extService.Tracesmart_Use_DoB > 0)
            {
                if (haveAllReqdFieldsAndDob)
                {
                    serviceDetails.dob = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_DoB;
                }
                else if (extService.Tracesmart_Use_DoB == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("DateOfbirth");
                }
            }

            serviceDetails.sanction = false;
            if (extService.Tracesmart_Use_Sanction > 0)
            {
                if (haveAllReqdFields)
                {
                    serviceDetails.sanction = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Sanction;
                }
                else if (extService.Tracesmart_Use_Sanction == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("Sanction");
                }
            }

            serviceDetails.insolvency = false;
            if (extService.Tracesmart_Use_Insolvency > 0)
            {
                if (haveAllReqdFieldsAndDob && !string.IsNullOrEmpty(personInfo.Gender))
                {
                    serviceDetails.insolvency = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Insolvency;
                }
                else if (extService.Tracesmart_Use_Insolvency == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("Insolvency");
                }
            }

            serviceDetails.ccj = false;
            if (extService.Tracesmart_Use_CCJ > 0)
            {
                bool canCallCCJ = !string.IsNullOrEmpty(personInfo.Forename)
                                                    && !string.IsNullOrEmpty(personInfo.Surname)
                                                    && ((!string.IsNullOrEmpty(personInfo.DOB) || (!string.IsNullOrEmpty(personInfo.Address1)
                                                                                               && !string.IsNullOrEmpty(personInfo.Postcode))));
                if (canCallCCJ)
                {
                    serviceDetails.ccj = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_CCJ;
                }
                else if (extService.Tracesmart_Use_CCJ == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("CCJ");
                }
            }

            serviceDetails.passport = false;
            //if (extService.Tracesmart_Use_Passport > 0)
            //{
            //    if (false)
            //    {
            //        serviceDetails.passport = true;
            //        servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Passport;
            //    }
            //    else if (extService.Tracesmart_Use_Passport == (int)RiskExternalServiceTracesmartUse.MustCall)
            //    {
            //        errors.Add("Passport");
            //    }
            //}

            serviceDetails.birth = false;
            //if (extService.Tracesmart_Use_Birth > 0)
            //{
            //    if (false)
            //    {
            //        serviceDetails.birth = true;
            //        servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Birth;
            //    }
            //    else if (extService.Tracesmart_Use_Birth == (int)RiskExternalServiceTracesmartUse.MustCall)
            //    {
            //        errors.Add("Birth");
            //    }
            //}

            serviceDetails.telephone = false;
            if (extService.Tracesmart_Use_Telephone > 0)
            {
                if (haveAllReqdFieldsAndDob && !string.IsNullOrEmpty(personInfo.LandlineTelephone))
                {
                    serviceDetails.telephone = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Telephone;
                }
                else if (extService.Tracesmart_Use_Telephone == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("Telephone");
                }
            }

            serviceDetails.mobile = false;
            if (extService.Tracesmart_Use_Mobile > 0)
            {
                if (haveAllReqdFieldsAndDob && !string.IsNullOrEmpty(personInfo.MobileTelephone))
                {
                    serviceDetails.mobile = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Mobile;
                }
                else if (extService.Tracesmart_Use_Mobile == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("Mobile");
                }
            }

            if (extService.Tracesmart_Use_Driving > 0)
            {
                if (haveAllReqdFieldsAndDob && !string.IsNullOrEmpty(personInfo.Drivinglicence1)
                                           && !string.IsNullOrEmpty(personInfo.Drivinglicence2)
                                           && !string.IsNullOrEmpty(personInfo.Drivinglicence3)
                                           && !string.IsNullOrEmpty(personInfo.Middle))
                {
                    serviceDetails.driving = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Driving;
                }
                else if (extService.Tracesmart_Use_Driving == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("Driving");
                }
            }

            if (extService.Tracesmart_Use_Smartlink > 0)
            {
                if (haveAllReqdFieldsAndDob)
                {
                    serviceDetails.smartlink = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_SmartLink;
                }
                else if (extService.Tracesmart_Use_Smartlink == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("SmartLink");
                }
            }

            if (extService.Tracesmart_Use_NI > 0)
            {
                if (haveAllReqdFieldsAndDob && !string.IsNullOrEmpty(personInfo.NI))
                {
                    serviceDetails.ni = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_NI;
                }
                else if (extService.Tracesmart_Use_NI == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("NI");
                }
            }

            if (extService.Tracesmart_Use_CardNumber > 0)
            {
                if (haveAllReqdFieldsAndDob && !string.IsNullOrEmpty(personInfo.CardNumber))
                {
                    serviceDetails.cardnumber = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_CardNumber;
                }
                else if (extService.Tracesmart_Use_CardNumber == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("CardNumber");
                }

            }

            if (extService.Tracesmart_Use_BankAccount > 0)
            {
                if (haveAllReqdFieldsAndDob && !string.IsNullOrEmpty(personInfo.Sortcode)
                                                    && !string.IsNullOrEmpty(personInfo.AccountNumber))
                {
                    serviceDetails.bankmatch = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_BankAccount;
                }
                else if (extService.Tracesmart_Use_BankAccount == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("BankAccount");
                }
            }

            serviceDetails.crediva = false;
            if (extService.Tracesmart_Use_Crediva > 0)
            {
                if (haveAllReqdFieldsAndDob)
                {
                    serviceDetails.crediva = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Crediva;
                }
                else if (extService.Tracesmart_Use_Crediva == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("Crediva");
                }
            }

            serviceDetails.creditactive = false;
            if (extService.Tracesmart_Use_CreditActive > 0)
            {
                if (haveAllReqdFieldsAndDob)
                {
                    serviceDetails.creditactive = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_CreditActive;
                }
                else if (extService.Tracesmart_Use_CreditActive == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("CreditActive");
                }
            }

            serviceDetails.nhs = false;
            if (extService.Tracesmart_Use_NHS > 0)
            {
                if (haveAllReqdFieldsAndDob)
                {
                    serviceDetails.nhs = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_NHS;
                }
                else if (extService.Tracesmart_Use_NHS == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("NHS");
                }
            }

            serviceDetails.cardavs = false;
            if (extService.Tracesmart_Use_Cardavs > 0)
            {
                if (haveAllReqdFieldsAndDob)
                {
                    serviceDetails.cardavs = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_Cardavs;
                }
                else if (extService.Tracesmart_Use_Cardavs == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("CardAvs");
                }
            }

            serviceDetails.mpan = false;
            if (extService.Tracesmart_Use_MPan > 0)
            {
                if (haveAllReqdFieldsAndDob)
                {
                    serviceDetails.mpan = true;
                    servicesCalledMask |= TracesmartServicesMask.Tracesmart_Use_MPan;
                }
                else if (extService.Tracesmart_Use_MPan == (int)RiskExternalServiceTracesmartUse.MustCall)
                {
                    errors.Add("MPan");
                }
            }

            if (errors.Count > 0)
            {
                errorMsg = "Services that are set MUST CALL but can't be called because fields are missing [" + string.Join(",", errors) + "]";
                if (ctx.TraceLoad) ADATrace.WriteLine(errorMsg);
            }

            if (ctx.TraceLoad)
            {
                ADATrace.WriteLine("Address      [" + serviceDetails.address.ToString() + "]");
                ADATrace.WriteLine("Bankaccount  [" + serviceDetails.bankmatch.ToString() + "]");
                ADATrace.WriteLine("Birth        [" + serviceDetails.birth.ToString() + "]");
                ADATrace.WriteLine("Cardavs      [" + serviceDetails.cardavs.ToString() + "]");
                ADATrace.WriteLine("Cardnumber   [" + serviceDetails.cardnumber.ToString() + "]");
                ADATrace.WriteLine("Ccj          [" + serviceDetails.ccj.ToString() + "]");
                ADATrace.WriteLine("creditactive [" + serviceDetails.creditactive.ToString() + "]");
                ADATrace.WriteLine("Crediva      [" + serviceDetails.crediva.ToString() + "]");
                ADATrace.WriteLine("Deathscreen  [" + serviceDetails.deathscreen.ToString() + "]");
                ADATrace.WriteLine("Dob          [" + serviceDetails.dob.ToString() + "]");
                ADATrace.WriteLine("Driving      [" + serviceDetails.driving.ToString() + "]");
                ADATrace.WriteLine("Insolvency   [" + serviceDetails.insolvency.ToString() + "]");
                ADATrace.WriteLine("Mobile       [" + serviceDetails.mobile.ToString() + "]");
                ADATrace.WriteLine("Mpan         [" + serviceDetails.mpan.ToString() + "]");
                ADATrace.WriteLine("Nhs          [" + serviceDetails.nhs.ToString() + "]");
                ADATrace.WriteLine("Ni           [" + serviceDetails.ni.ToString() + "]");
                ADATrace.WriteLine("Passport     [" + serviceDetails.passport.ToString() + "]");
                ADATrace.WriteLine("Sanction     [" + serviceDetails.sanction.ToString() + "]");
                ADATrace.WriteLine("Smartlink    [" + serviceDetails.smartlink.ToString() + "]");
                ADATrace.WriteLine("Telephone    [" + serviceDetails.telephone.ToString() + "]");
            }

            if (errors.Count > 0 || servicesCalledMask == 0) return null;

            return serviceDetails;
        }

        private ResultInfo CallTracemartService(ServiceDetails serviceDetails, PersonInfo personInfo, string userName, string password, bool _trace)
        {
            var login = new LoginDetails()
            {
                username = userName,
                password = password, 
            };

            // Optional, to aid request/response tracking
            //var iduDetails = new IDUDetails()
            //{
            //    ID = "your-id",
            //    IKey = "your-ikey",
            //    Reference = "your-reference",
            //    Scorecard = "IDU Default",
            //    equifaxUsername = ""
            //};

            // Create Request
            var request = new Request()
            {
                Login = login,
                Person = Translator.Translate(personInfo),
                Services = serviceDetails,
                //IDU    = iduDetails
            };

            // Make synchronous call to IduService 
            //service.Timeout = 20000;

            if (_trace) ADATrace.WriteLine("------------------------------------------");
            if (_trace) ADATrace.WriteLine("Input :: " + SerializeParam(request.Person));
            if (_trace) ADATrace.WriteLine("------------------------------------------");

            try
            {
                iduPortClient proxy = new iduPortClient();

                var a = proxy.IDUProcess(request);

                if (_trace) ADATrace.WriteLine("Output :: " + SerializeResults(a));
                if (_trace) ADATrace.WriteLine("------------------------------------------");

                ResultInfo results = Translator.Translate(a);

                return results;
            }
            catch (Exception ex)
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Unexpected Exception - Tracesmart Proxy Call");
                    ADATrace.TraceException(ex);
                }

                throw ex;
            }
        }
    }

    class ServicesToCall
    {
        public RiskClient2RiskExternalServices Client2Services { get; set; }
        public RiskExternalService ExternalService { get; set; }
    }
}

