﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.TracesmartService.Impl.TMServiceReference;
using MDA.TracesmartService.Model;

namespace MDA.TracesmartService.Impl
{
    public class Translator
    {
        public static ErrorPartInfo Translate(TMServiceReference.ErrorPart error)
        {
            if (error == null) return null;

            return new ErrorPartInfo()
            {
                Details = error.Details,
                Service = error.Service
            };
        }



        public static SummaryPartInfo Translate(TMServiceReference.SummaryPart summary)
        {
            if (summary == null) return null;

            return new SummaryPartInfo()
            {
                Credits         = summary.Credits,
                EquifaxUsername = summary.equifaxUsername,
                Id              = summary.ID,
                IKey            = summary.IKey,
                ProfileURL      = summary.ProfileURL,
                Reference       = summary.Reference,
                ResultText      = summary.ResultText,
                Scorecard       = summary.Scorecard,
                Smartscore      = summary.Smartscore,
                Status          = summary.Status
            };
        }

        public static OccupantPartInfo Translate(TMServiceReference.OccupantPart occupant)
        {
            if (occupant == null) return null;

            return new OccupantPartInfo()
            {
                DOB           = occupant.DOB,
                Name          = occupant.Name,
                Recency       = occupant.Recency,
                Residency     = occupant.Residency,
                Source        = occupant.Source,
                Telephone     = occupant.Telephone,
                TelephoneName = occupant.TelephoneName
            };
        }

        public static PropertyPartInfo Translate(TMServiceReference.PropertyPart property)
        {
            if (property == null) return null;

            return new PropertyPartInfo()
            {
                Date   = property.Date,
                Price  = property.Price,
                Tenure = property.Tenure,
                Type   = property.Type
            };
        }

        public static AddressPartInfo Translate(TMServiceReference.AddressPart addr)
        {
            if (addr == null) return null;

            AddressPartInfo r = new AddressPartInfo()
            {
                AddressValidated   = addr.AddressValidated,
                DOBAppended        = addr.DOBAppended,
                DOB                = addr.DOB,
                ForenameAppended   = addr.ForenameAppended,
                Forename           = addr.Forename,
                GoneAway           = addr.GoneAway,
                MatchType          = addr.MatchType,
                MiddleNameAppended = addr.MiddleNameAppended,
                MiddleName         = addr.MiddleName,
                Mosaic             = addr.Mosaic,
                Recency            = addr.Recency,
                Source             = addr.Source,
                Surname            = addr.Surname,
                Telephone          = addr.Telephone,
                Telephonename      = addr.Telephonename
            };

            if (addr.Occupants != null)
            {
                r.Occupants = new List<OccupantPartInfo>();

                foreach (var o in addr.Occupants)
                    r.Occupants.Add(Translate(o));
            }

            if (addr.Property != null)
            {
                r.Property = new List<PropertyPartInfo>();

                foreach (var o in addr.Property)
                    r.Property.Add(Translate(o));
            }

            return r;
        }

        public static SmartlinkPartInfo Translate(TMServiceReference.SmartlinkPart smart)
        {
            if (smart == null) return null;

            return new SmartlinkPartInfo()
            {
                Address    = Translate(smart.Address),
                DOB        = smart.DOB,
                Forename   = smart.Forename,
                LinkSource = smart.LinkSource,
                Middle     = smart.Middle,
                Recency    = smart.Recency,
                Residency  = smart.Residency,
                Surname    = smart.Surname,
                Title      = smart.Title
            };
        }

        public static DeathscreenResultInfo Translate(TMServiceReference.DeathscreenResult death)
        {
            if (death == null) return null;

            return new DeathscreenResultInfo()
            {
                Address1     = death.address1,
                Address2     = death.address2,
                Address3     = death.address3,
                Address4     = death.address4,
                Address5     = death.address5,
                DistNo       = death.DistNo,
                District     = death.District,
                DOB          = death.DOB,
                DOD          = death.DOD,
                DOR          = death.DOR,
                EntryNo      = death.EntryNo,
                Forename     = death.Forename,
                FourthName   = death.FourthName,
                GROReference = death.GROReference,
                MaidenName   = death.MaidenName,
                MatchType    = death.MatchType,
                PageNo       = death.PageNo,
                PlaceOfBirth = death.PlaceOfBirth,
                Postcode     = death.postcode,
                RegNo        = death.RegNo,
                SecondName   = death.SecondName,
                Surname      = death.Surname,
                ThirdName    = death.ThirdName,
                VolumeNo     = death.VolumeNo
            };
        }

        public static TelephonePartInfo Translate(TMServiceReference.TelephonePart telephone)
        {
            if (telephone == null) return null;

            return new TelephonePartInfo()
            {
                Areaname   = telephone.Areaname,
                Locality   = telephone.Locality,
                Operator   = telephone.Operator,
                Postcode   = telephone.Postcode,
                ResultFlag = telephone.ResultFlag,
                Status     = telephone.Status
            };
        }

        public static DrivingLicensePartInfo Translate(TMServiceReference.DrivingLicencePart license)
        {
            if (license == null) return null;

            return new DrivingLicensePartInfo()
            {
                MailSortFlag      = license.MailSortFlag,
                MiddleNameWarning = license.MiddleNameWarning,
                ResultFlag        = license.ResultFlag
            };
        }

        public static AddressLabelInfo Translate(TMServiceReference.AddressLabel a)
        {
            if (a == null) return null;

            return new AddressLabelInfo()
            {
                Address1 = a.Address1,
                Address2 = a.Address2,
                Address3 = a.Address3,
                Address4 = a.Address4,
                Address5 = a.Address5,
                DPS      = a.DPS,
                Postcode = a.Postcode
            };
        }

        public static DOBPartInfo Translate(TMServiceReference.DOBPart dob)
        {
            if (dob == null) return null;

            return new DOBPartInfo()
            {
                EquifaxDOB               = dob.EquifaxDOB,
                EquifaxDOBFieldSpecified = dob.EquifaxDOBSpecified,
                EquifaxDOBStatusField    = dob.EquifaxDOBStatus,
                ExperianDOB              = dob.ExperianDOB,
                TracesmartDOB            = dob.TracesmartDOB
            };
        }

        public static NHSPartInfo Translate(TMServiceReference.NHSPart nhs)
        {
            if (nhs == null) return null;

            return new NHSPartInfo()
            {
                ResultFlag = nhs.ResultFlag
            };
        }

        public static NIPartInfo Translate(TMServiceReference.NIPart nhs)
        {
            if (nhs == null) return null;

            return new NIPartInfo()
            {
                ResultFlag = nhs.ResultFlag
            };
        }

        public static AliasPartInfo Translate(TMServiceReference.AliasPart alias)
        {
            if (alias == null) return null;

            return new AliasPartInfo()
            {
                Name = alias.Name
            };
        }

        public static SanctionPartInfo Translate(TMServiceReference.SanctionPart sanction)
        {
            if (sanction == null) return null;

            var r = new SanctionPartInfo()
            {
                Name      = sanction.Name,
                Positions = sanction.Positions,
                Recency   = sanction.Recency,
                Source    = sanction.Source
            };

            if (sanction.Addresses != null)
            {
                r.Addresses = new List<AddressLabelInfo>();

                foreach (var rr in sanction.Addresses)
                    r.Addresses.Add(Translate(rr));
            }

            if (sanction.Aliases != null)
            {
                r.Aliases = new List<AliasPartInfo>();

                foreach (var rr in sanction.Aliases)
                    r.Aliases.Add(Translate(rr));
            }

            return r;
        }

        public static PassportPartInfo Translate(TMServiceReference.PassportPart passport)
        {
            if (passport == null) return null;

            return new PassportPartInfo()
            {
                DOBValid    = passport.DOBValid,
                GenderValid = passport.GenderValid,
                MRZValid    = passport.MRZValid
            };
        }

        public static BirthPartInfo Translate(TMServiceReference.BirthPart birth)
        {
            if (birth == null) return null;

            return new BirthPartInfo()
            {
                Maiden          = birth.Maiden,
                Name            = birth.Name,
                RegDate         = birth.RegDate,
                RegDistrict     = birth.RegDistrict,
                ResultIndicator = birth.ResultIndicator
            };
        }

        public static CardPartInfo Translate(TMServiceReference.CardPart card)
        {
            if (card == null) return null;

            return new CardPartInfo()
            {
                Address       = card.Address,
                Authorisation = card.Authorisation,
                CV2           = card.CV2,
                Postcode      = card.Postcode
            };
        }

        public static CardNumberPartInfo Translate(TMServiceReference.CardNumberPart cardNum)
        {
            if (cardNum == null) return null;

            return new CardNumberPartInfo()
            {
                CardNumberValid = cardNum.CardNumberValid,
                CardTypeConfirm = cardNum.CardTypeConfirm,
                CardTypeValid   = cardNum.CardTypeValid
            };
        }

        public static MpanPartInfo Translate(TMServiceReference.MpanPart mpan)
        {
            if (mpan == null) return null;

            return new MpanPartInfo()
            {
                ResultFlag = mpan.ResultFlag
            };
        }

        public static InsolvencyResultInfo Translate(TMServiceReference.InsolvencyResult insolv)
        {
            if (insolv == null) return null;

            return new InsolvencyResultInfo()
            {
                Address   = Translate(insolv.Address),
                CaseNo    = insolv.CaseNo,

                // The CaseType field only applies to bankruptcies in Scotland and Northern Ireland. 
                // It would bring back any relevant information such as Sequestration, Section5 Sequestration. 
                // The two fields are described as:
                // Case Type: Additional definition of the type of case, displayed as a text description.
                // Type: Insolvency result type
                CaseType  = insolv.Type,
                //Type      = insolv.Type,

                Court     = insolv.Court,
                DOB       = insolv.DOB,
                Name      = insolv.Name,
                StartDate = insolv.StartDate,
                Status    = insolv.Status
            };
        }

        public static BankAccountPartInfo Translate(TMServiceReference.BankmatchPart bank)
        {
            if (bank == null) return null;

            return new BankAccountPartInfo()
            {
                BACSPayments     = bank.BACSPayments,
                BankAccountValid = bank.BankAccountValid,
                BankName         = bank.BankName,
                BranchDetails    = bank.BranchDetails,
                CHAPSPayments    = bank.CHAPSPayments,
                FasterPayments   = bank.FasterPayments
            };
        }

        public static CredivaPartInfo Translate(TMServiceReference.CredivaPart cred)
        {
            if (cred == null) return null;

            return new CredivaPartInfo()
            {
                FullER = cred.FullER
            };
        }

        public static CcjResultInfo Translate(TMServiceReference.CcjResult ccj)
        {
            if (ccj == null) return null;

            return new CcjResultInfo()
            {
                Address1      = ccj.address1,
                Address2      = ccj.address2,
                Address3      = ccj.address3,
                Address4      = ccj.address4,
                Address5      = ccj.address5,
                Amount        = ccj.amount,
                CaseNumber    = ccj.caseNumber,
                CourtName     = ccj.courtName,
                DateEnd       = ccj.dateEnd,
                DOB           = ccj.dob,
                JudgementDate = ccj.judgementDate,
                JudgementType = ccj.judgementType,
                Name          = ccj.name,
                Postcode      = ccj.postcode
            };
        }

        public static MobilePartInfo Translate(TMServiceReference.MobilePart mobile)
        {
            if (mobile == null) return null;

            return new MobilePartInfo()
            {
                CurrentLocation = mobile.currentLocation,
                Provider        = mobile.provider,
                Status          = mobile.status
            };
        }

        public static CreditActivePartInfo Translate(TMServiceReference.CreditActivePart credit)
        {
            if (credit == null) return null;

            return new CreditActivePartInfo()
            {
                CIFAS           = credit.CIFAS,
                InsightAccounts = credit.InsightAccounts,
                InsightLenders  = credit.InsightLenders
            };
        }

        public static ResultInfo Translate(TMServiceReference.Result result)
        {
            if (result == null) return null;

            var r = new ResultInfo()
            {
                Address        = Translate(result.Address),
                BankAccount    = Translate(result.Bankmatch),
                Birth          = Translate(result.Birth),
                CardAVS        = Translate(result.CardAVS),
                CardNumber     = Translate(result.CardNumber),
                CreditActive   = Translate(result.CreditActive),
                Crediva        = Translate(result.Crediva),
                DOB            = Translate(result.DOB),
                DrivingLicence = Translate(result.DrivingLicence),
                Mobile         = Translate(result.Mobile),
                Mpan           = Translate(result.Mpan),
                NHS            = Translate(result.NHS),
                NI             = Translate(result.NI),
                Passport       = Translate(result.Passport),
                Summary        = Translate(result.Summary),
                Telephone      = Translate(result.Telephone),
            };

            if (result.Ccj != null)
            {
                r.CCJ = new List<CcjResultInfo>();

                foreach (var rr in result.Ccj)
                    r.CCJ.Add(Translate(rr));
            }

            if (result.Deathscreen != null)
            {
                r.Deathscreen = new List<DeathscreenResultInfo>();

                foreach (var rr in result.Deathscreen)
                    r.Deathscreen.Add(Translate(rr));
            }

            if (result.Insolvency != null)
            {
                r.Insolvency = new List<InsolvencyResultInfo>();

                foreach (var rr in result.Insolvency)
                    r.Insolvency.Add(Translate(rr));
            }

            if (result.Sanction != null)
            {
                r.Sanction = new List<SanctionPartInfo>();

                foreach (var rr in result.Sanction)
                    r.Sanction.Add(Translate(rr));
            }

            if (result.Smartlink != null)
            {
                r.Smartlink = new List<SmartlinkPartInfo>();

                foreach (var rr in result.Smartlink)
                    r.Smartlink.Add(Translate(rr));
            }

            return r;
        }

        public static FilteredResults Translate(ResultInfo a)
        {
            FilteredResults results = new FilteredResults();

            if (a.Sanction != null && a.Sanction.Count() > 0)
            {
                SanctionPartInfo s = a.Sanction[0];

                try
                {
                    results.Person_SanctionDate = DateTime.ParseExact(s.Recency, "yyyy-M-d", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (System.FormatException) { }

                results.Person_SanctionSource = s.Source;
            }

            if (a.Summary != null)
            {
                results.IKey = a.Summary.IKey;
                results.P2A_TSIDUAMLResult = a.Summary.ResultText;
            }

            if (a.DOB != null)
            {
                results.P2A_TracesmartDOB = a.DOB.TracesmartDOB;
                results.P2A_ExperianDOB = a.DOB.ExperianDOB;
            }

            if (a.Deathscreen != null && a.Deathscreen.Count() > 0)
            {
                results.P2A_DeathScreenMatchType = a.Deathscreen[0].MatchType;
                results.P2A_DeathScreenDoD = a.Deathscreen[0].DOD;
                results.P2A_DeathScreenRegNo = a.Deathscreen[0].RegNo;
            }

            if (a.Insolvency != null && a.Insolvency.Count() > 0)
            {
                results.P2A_InsolvencyHistory = "";

                foreach (var i in a.Insolvency)
                {
                    string comma = "";

                    string ss = string.Format("[{0},{1},{2},{3},{4}]", i.CaseNo, i.CaseType, i.Court, i.Status, i.StartDate);

                    results.P2A_InsolvencyHistory += comma + ss;

                    comma = ":";
                }

                //results.P2A_Insolvency = i.CaseType;

                //try
                //{
                //    results.P2A_InsolvencyStartDate = DateTime.ParseExact(i.StartDate, "yyyy-M-d", System.Globalization.CultureInfo.InvariantCulture);
                //}
                //catch (System.FormatException) { }

                //results.P2A_InsolvencyStatus = i.Status;
            }

            if (a.CCJ != null && a.CCJ.Count() > 0)
            {
                results.P2A_CCJHistory = "";

                foreach (var c in a.CCJ)
                {
                    string comma = "";

                    StringBuilder sb = new StringBuilder();
                    foreach (char ch in c.Amount)
                    {
                        if ((ch >= '0' && ch <= '9') || ch == '.')
                            sb.Append(ch);
                    }

                    string CCJAmount = sb.ToString();

                    //try
                    //{
                    //    results.P2A_CCJDate = DateTime.ParseExact(c.JudgementDate, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    //}
                    //catch (System.FormatException) { }

                    //try
                    //{
                    //    results.P2A_CCJEndDate = DateTime.ParseExact(c.DateEnd, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    //}
                    //catch (System.FormatException) { }

                    string ss = string.Format("[{0},{1},{2},{3},{4}]", c.CaseNumber, c.JudgementType, c.CourtName, c.JudgementDate, CCJAmount);


                    results.P2A_CCJHistory += comma + ss;

                    comma = ":";

                }
            }

            if (a.Crediva != null)
                results.P2A_CredivaCheck = a.Crediva.FullER;

            if (a.BankAccount != null)
                results.P2BA_Validated = a.BankAccount.BankAccountValid;

            if (a.DrivingLicence != null)
                results.P2DL_Validated = a.DrivingLicence.ResultFlag;

            if (a.NI != null)
                results.P2NI_Validated = a.NI.ResultFlag;

            if (a.Passport != null)
            {
                results.P2PP_MRZValid    = a.Passport.MRZValid;
                results.P2PP_DOBValid    = a.Passport.DOBValid;
                results.P2PP_GenderValid = a.Passport.GenderValid;
            }

            if (a.CardNumber != null)
                results.P2PC_CardNumberValid = a.CardNumber.CardNumberValid;

            if (a.Telephone != null)
            {
                results.P2T_Validated       = a.Telephone.ResultFlag;
                results.P2T_TelephoneStatus = a.Telephone.Status;
            }

            if (a.Mobile != null)
            {
                results.P2T_MobileStatus    = a.Mobile.Status;
                results.P2T_CurrentLocation = a.Mobile.CurrentLocation;
            }

            if (a.Address != null)
            {
                results.ADR_Validated = a.Address.AddressValidated;
                results.ADR_TracesmartSource = a.Address.Source;

                results.P2A_GoneAway = a.Address.GoneAway;
                results.P2A_AddressMatchType = a.Address.MatchType;

                if (a.Address.Property != null && a.Address.Property.Count() > 0)
                    results.ADR_PropertyType = a.Address.Property[0].Type;

                results.ADR_MosaicCode = a.Address.Mosaic;
            }

            return results;
        }

        public static TMServiceReference.PersonDetails Translate(PersonInfo person)
        {
            if (person == null) return null;

            return new PersonDetails()
            {
                accountnumber   = person.AccountNumber,
                address1        = person.Address1,
                address2        = person.Address2,
                address3        = person.Address3,
                address4        = person.Address4,
                address5        = person.Address5,
                address6        = person.Address6,
                bforename       = person.BForename,
                bmiddle         = person.BMiddle,
                bsurname        = person.BSurname,
                cardavs         = Translate(person.Cardavs),
                cardnumber      = person.CardNumber,
                cardtype        = person.Cardtype,
                dob             = person.DOB,
                drivinglicence1 = person.Drivinglicence1,
                drivinglicence2 = person.Drivinglicence2,
                drivinglicence3 = person.Drivinglicence3,
                drivingmailsort = person.DrivingMailsort,
                drivingpostcode = person.DrivingPostcode,
                forename        = person.Forename,
                gender          = person.Gender,
                maiden          = person.Maiden,
                middle          = person.Middle,
                mobile          = person.MobileTelephone,
                mpannumber1     = person.MpanNumber1,
                mpannumber2     = person.MpanNumber2,
                mpannumber3     = person.MpanNumber3,
                mpannumber4     = person.MpanNumber4,
                nhs             = person.Nhs,
                ni              = person.NI,
                passport1       = person.Passport1,
                passport2       = person.Passport2,
                passport3       = person.Passport3,
                passport4       = person.Passport4,
                passport5       = person.Passport5,
                passport6       = person.Passport6,
                passport7       = person.Passport7,
                passport8       = person.Passport8,
                postcode        = person.Postcode,
                sortcode        = person.Sortcode,
                surname         = person.Surname,
                telephone       = person.LandlineTelephone
            };
        }

        public static TMServiceReference.CardDetails Translate(CardInfo card)
        {
            if (card == null) return null;

            return new CardDetails()
            {
                CardAddress = Translate(card.CardAddress),
                CardExpire  = card.CardExpire,
                CardHolder  = card.CardHolder,
                CardNumber  = card.CardNumber,
                CardStart   = card.CardStart,
                CardType    = card.CardType,
                CV2         = card.CV2,
                IssueNumber = card.IssueNumber
            };
        }

        public static TMServiceReference.AddressLabel Translate(AddressLabelInfo address)
        {
            if (address == null) return null;

            return new AddressLabel()
            {
                Address1 = address.Address1,
                Address2 = address.Address2,
                Address3 = address.Address3,
                Address4 = address.Address4,
                Address5 = address.Address5,
                DPS      = address.DPS,
                Postcode = address.Postcode
            };
        }
    }
}
