﻿using System;
using System.Configuration;
using System.IO;
using MDA.Common;
using MDA.MappingService.Interface;
using MDA.Common.Server;
using MDA.MappingService;

namespace MDA.MappingService.CH1.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
                Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn,
                out ProcessingResults processingResults)
        {
            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            try
            {
                GetMotorClaimBatch claimBatch = new GetMotorClaimBatch();
                
                bool DeleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);

                claimBatch.RetrieveMotorClaimBatch(ctx, fs, clientFolder, DeleteUploadedFiles, statusTracking, ProcessClaimFn, processingResults); 
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping CH1 file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = _tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }
    }
}
