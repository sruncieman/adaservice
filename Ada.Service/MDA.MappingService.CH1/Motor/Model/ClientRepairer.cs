﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClientRepairer
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int ClientRepairerId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerFullName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerAddr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerAddr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerAddr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerAddr4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerAddr5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerAddr6;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientRepairerFax;
    }
}
