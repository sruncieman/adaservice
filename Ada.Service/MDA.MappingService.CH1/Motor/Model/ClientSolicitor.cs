﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClientSolicitor
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int ClientSolicitorId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientSolicitorFullName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientSolicitorAddr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientSolicitorAddr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientSolicitorAddr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientSolicitorAddr4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientSolicitorAddr5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientSolicitorAddr6;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientSolicitorTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientSolicitorFax;



    }
}
