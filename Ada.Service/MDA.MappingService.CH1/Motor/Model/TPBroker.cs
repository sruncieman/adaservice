﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class TPBroker
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int TPBrokerId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerFullName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerAddr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerAddr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerAddr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerAddr4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerAddr5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerAddr6;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPBrokerFax;
    }
}
