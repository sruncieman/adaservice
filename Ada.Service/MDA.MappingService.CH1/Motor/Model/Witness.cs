﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class Witness
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string WitnessCaseId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessTitle;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessForename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessInitials;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessAddr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessAddr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessAddr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessAddr4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessPostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WitnessMobile;
    }
}
