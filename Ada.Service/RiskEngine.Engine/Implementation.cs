﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Workflow.Activities.Rules;
using RiskEngine.Model;
using RiskEngine.Interfaces;
using System.IO;
using System.Diagnostics;
using MDA.Common.Debug;
using System.Configuration;
using MDA.DataService;
using MDA.Common.Enum;
using MDA.DAL;

namespace RiskEngine.Engine
{
    public class RiskEngine : IRiskEngine
    {
        /// <summary>
        /// Combine the Default Rule Scores and Custom Rule Scores. Takes ALL the Custom Scores and only adds Default ones
        /// with different rule names. 
        /// </summary>
        /// <param name="DefRuleScores">Default Rule Scores</param>
        /// <param name="RuleScores">Custom Rule Scores</param>
        /// <returns>Combined List</returns>
        private List<RuleScoreData> CombineScores(List<RuleScoreData> DefRuleScores, List<RuleScoreData> RuleScores)
        {
            List<RuleScoreData> ret = new List<RuleScoreData>();

            if (RuleScores != null) // Add all the Custom ones
            {
                ret.AddRange(RuleScores);
            }

            if (DefRuleScores != null)
            {
                foreach (RuleScoreData rsd in DefRuleScores)
                {
                    // Check if it exists in the combined list already
                    var y = (from x in ret where x.RootRuleSetId == rsd.RootRuleSetId && x.RuleName == rsd.RuleName select x).FirstOrDefault();

                    if (y == null)  // It doesn't so add it
                        ret.Add(rsd);
                }
            }

            return ret;
        }

        /// <summary>
        /// Combine the Default Rule Variables and Custom Rule Variables. Takes ALL the Custom Variables and only adds Default ones
        /// with different names. 
        /// </summary>
        /// <param name="DefRuleVars">Default Rule Variables</param>
        /// <param name="RuleVars">Custom Rule Variables</param>
        /// <returns>Combined List</returns>
        private List<RuleVariableData> CombineVariables(List<RuleVariableData> DefRuleVars, List<RuleVariableData> RuleVars)
        {
            List<RuleVariableData> ret = new List<RuleVariableData>();

            if (RuleVars != null)   // Add all the Custom ones
                ret.AddRange(RuleVars);
            
            if (DefRuleVars != null)
            {
                foreach (RuleVariableData rsd in DefRuleVars)
                {
                    // Check if it exists in the combined list already
                    var y = (from x in ret where x.VariableName == rsd.VariableName select x).FirstOrDefault();

                    if (y == null)   // It doesn't so add it
                        ret.Add(rsd);
                }
            }

            return ret;
        }

        //public static void RunRules<T>(T target, RuleSet rules)
        //{
        //    RuleEngine engine = new RuleEngine(rules, typeof(T));
        //    engine.Execute(target);
        //}

        /// <summary>
        /// Calulate risk of given entity. Pass in RuleGroupId and Entity to risk.  Entity must implement IRiskEntity interface.
        /// A structure containing Score and Message collections is returned. A Score = -1 means exception occured (Message collections 
        /// are populated up until the point of exception)
        /// </summary>
        /// <param name="request">Rule Group ID and Entity to risk (IRiskEntity)</param>
        /// <returns>A score, error list and message collections.  Score = -1 if exception occurs.</returns>
        public CalculateRiskResponse CalculateRisk(CalculateRiskRequest request)
        {
            CalculateRiskResponse ret = new CalculateRiskResponse();

            ret.MethodResultHistory = new DictionaryOfStringString(); // same as new Dictionary<string, string>() but smaller XML


            bool _trace = request.Entity.ctx.TraceScoring;

            bool _recordTimingMetrics = (ConfigurationManager.AppSettings["RecordTimingMetrics"].ToUpper() == "TRUE") ? true : false;

            try
            {
                // The request provides the RuleGroup, RuleSets, Scores and Variables all in one place
                RuleGroupData ruleGroupData = request.RuleGroupData;

                ret.RuleGroupScore = 0;
                ret.RuleGroupKeyAttractorCount = 0;

                // Combine DEFAULT and CUSTOM scores and variables
                request.Entity.RuleScoreData = CombineScores(ruleGroupData.DefRuleScores, ruleGroupData.RuleScores);
                request.Entity.RuleVariableData = CombineVariables(ruleGroupData.DefRuleVariables, ruleGroupData.RuleVariables);

                if (ruleGroupData != null)
                {
                    #region Execute Default Rulesets
                    if (ruleGroupData.DefRuleSets != null)
                    {
                        foreach (RuleSetData ruleSetData in ruleGroupData.DefRuleSets)
                        {
                            int saveIndentLevel = ADATrace.IndentLevel;

                            try
                            {                               

                                if (_trace)
                                {
                                    ADATrace.WriteLine(" ");
                                    ADATrace.WriteLine("Scoring Default RuleSet: " + ruleSetData.Name);
                                    ADATrace.IndentLevel += 1;
                                }

                                // Clone the entity. Ensures that any changes made on the entity by the rules are "lost" for each ruleset
                                IRiskEntity e = (IRiskEntity)request.Entity.Clone();

                                e.ctx = request.Entity.ctx;
                                //e.db = e.ctx.db;
                                //e.Trace = e.ctx.Trace;

                                // Set a flag which indicates this "copy" is being scored
                                e.Locked = false;

                                // Reset entity instance variables
                                e.Errors = new ListOfStrings(); 
                                e.Results = new DictionaryOfRiskResults(); 
                                e.MethodResultHistory = new DictionaryOfMethodResultRecords(); 

                                // Attach Scores and variables to the new Entity instance
                                e.RuleScoreData = request.Entity.RuleScoreData;
                                e.RuleVariableData = request.Entity.RuleVariableData;

                                if (_trace)
                                {
                                    ADATrace.WriteLine("Variables Retrieved");
                                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;

                                    foreach (var x in e.RuleVariableData)
                                    {
                                        ADATrace.WriteLine(x.VariableName + " = " + x.VariableValue);
                                    }
                                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                                }

                                // push in the ruleset name and ID
                                e.RuleSetName = ruleSetData.Name;
                                e.RuleSetId = ruleSetData.RuleSetId;

                                if (_trace)
                                {
                                    ADATrace.WriteLine("Before Call to Execute Score");
                                    ADATrace.IndentLevel += 1;
                                }

                                //int? autoKACount;

                                try
                                {
                                    Stopwatch sw = Stopwatch.StartNew();

                                    new RuleEngine(ruleSetData.RuleSet, e.GetType()).Execute(e);

                                    DataServices dataServices = new DataServices(request.Entity.ctx);

                                    if (_recordTimingMetrics)
                                        InsertScoringTiming(0, 0, dataServices, sw, e.RuleSetId, request.Entity.ctx.Who, e.RuleSetName);
                                  
                                    sw.Restart();
                                    //autoKACount = e.AutoKACount;
                                }

                                catch (Exception ex)
                                {
                                    if (_trace)
                                    {
                                        string err = "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";

                                        ADATrace.WriteLine(err);
                                        ADATrace.WriteLine(err);
                                        ADATrace.WriteLine("EXCEPTION: Custom RuleSet:" + ruleSetData.Name + " : " + ex.Message);
                                        ADATrace.WriteLine(err);
                                        ADATrace.WriteLine(err);
                                    }

                                    // Check for our special exception thrown by rules developer
                                    if (ex.InnerException != null && ex.InnerException is ScoringException)
                                    {
                                        ScoringException sex = ex.InnerException as ScoringException;

                                        throw sex;
                                    }

                                    throw ex;
                                }
                                finally
                                {
                                    if (_trace)
                                    {
                                        ADATrace.IndentLevel -= 1;
                                        ADATrace.WriteLine("After Call to Execute Score");
                                    }

                                    e.Locked = true;
                                }

                                // concat any errors to the errors collection
                                if (e.Errors.Count() > 0)
                                {
                                    ADATrace.WriteLine("Adding Errors to return value");
                                    ret.Errors.AddRange(e.Errors);
                                }

                                foreach (KeyValuePair<string, MethodResultRecord> x in e.MethodResultHistory)
                                {
                                    string newKey = ruleSetData.Name + "." + x.Key;

                                    if (_trace) ADATrace.WriteLine("Adding MethodResult History. Key [" + newKey + "]");
                                    ret.MethodResultHistory.Add(newKey, x.Value.Result.ToString());
                                }

                                // Add this set of results to the return dictionary, key on RuleSetName
                                if (_trace) ADATrace.WriteLine("Adding Results to return value. RuleSetName [" + ruleSetData.Name + "]");
                                ret.Results.Add(ruleSetData.Name, e.Results);

                                if (_trace)
                                    ADATrace.WriteLine("Entity Total Score=" + e.TotalEntityScore.ToString() + ". UserKACount=" + e.UserKACount.ToString());

                                ret.RuleGroupScore += e.AddUpEntityRuleSetScores();
                                ret.RuleGroupKeyAttractorCount += e.UserKACount; // +((autoKACount == null) ? 0 : (int)autoKACount);

                                if (_trace)
                                    ADATrace.WriteLine("RuleGroupScore=" + ret.RuleGroupScore.ToString() + ". RuleGroupKeyAttractorCount=" + ret.RuleGroupKeyAttractorCount.ToString());

                                e.AssignUsedProperties(request.Entity);

                            }
                            finally
                            {
                                if (_trace)
                                {
                                    ADATrace.IndentLevel -= 1;
                                    ADATrace.WriteLine("Finished scoring ruleset: " + ruleSetData.Name);

                                    ADATrace.IndentLevel = saveIndentLevel;
                                }                              

                            }
                        }
                    }
                    #endregion

                    #region Execute Client Rulesets
                    if (ruleGroupData.RuleSets != null)
                    {
                        foreach (RuleSetData ruleSetData in ruleGroupData.RuleSets)
                        {
                            int saveIndentLevel = ADATrace.IndentLevel;

                            Stopwatch sw = Stopwatch.StartNew();

                            try
                            {
                                if (_trace)
                                {
                                    ADATrace.WriteLine(" ");
                                    ADATrace.WriteLine("Scoring Custom RuleSet: " + ruleSetData.Name);
                                    ADATrace.IndentLevel += 1;
                                }

                                // Clone the entity. Ensures that any changes made on the entity by the rules are "lost" for each ruleset
                                IRiskEntity e = (IRiskEntity)request.Entity.Clone();

                                e.ctx = request.Entity.ctx;
                                //e.db = e.ctx.db;

                                e.Locked = false;

                                // Reset entity instance variables
                                e.Errors = new ListOfStrings(); // new List<string>();
                                e.Results = new DictionaryOfRiskResults(); // new Dictionary<string, RiskResult>();
                                e.MethodResultHistory = new DictionaryOfMethodResultRecords(); // new Dictionary<string, MethodResultRecord>();

                                // Attach Scores and variables to the Entity instance
                                e.RuleScoreData = request.Entity.RuleScoreData;
                                e.RuleVariableData = request.Entity.RuleVariableData;

                                if (_trace)
                                {
                                    ADATrace.WriteLine("Variables Retrieved");
                                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;

                                    foreach (var x in e.RuleVariableData)
                                    {
                                        ADATrace.WriteLine(x.VariableName + " = " + x.VariableValue);
                                    }
                                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                                }

                                // push in the ruleset name
                                e.RuleSetName = ruleSetData.Name;
                                e.RuleSetId = ruleSetData.RuleSetId;

                                if (_trace)
                                {
                                    ADATrace.WriteLine("Before Call to Execute Score");
                                    ADATrace.IndentLevel += 1;
                                }

                                // int? autoKACount;

                                try
                                {
                                    //if (ruleGroupData.ClientId == 67)
                                    //{
                                        //if (ruleGroupData.RuleGroupId == 277)
                                            new RuleEngine(ruleSetData.RuleSet, e.GetType()).Execute(e);
                                    //}

                                    // new RuleEngine(ruleSetData.RuleSet, e.GetType()).Execute(e);

                                    //autoKACount = e.AutoKACount;
                                }

                                catch (Exception ex)
                                {
                                    if (_trace)
                                    {
                                        string err = "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";

                                        ADATrace.WriteLine(err);
                                        ADATrace.WriteLine(err);
                                        ADATrace.WriteLine("EXCEPTION: Custom RuleSet:" + ruleSetData.Name + " : " + ex.Message);
                                        ADATrace.WriteLine(err);
                                        ADATrace.WriteLine(err);
                                    }

                                    if (ex.InnerException != null && ex.InnerException is ScoringException)
                                    {
                                        ScoringException sex = ex.InnerException as ScoringException;

                                        throw sex;
                                    }

                                    throw ex;
                                }
                                finally
                                {
                                    if (_trace)
                                    {
                                        ADATrace.IndentLevel -= 1;
                                        ADATrace.WriteLine("After Call to Execute Score");
                                    }

                                    e.Locked = true;
                                }

                                // concat any errors to the errors collection
                                if (e.Errors.Count() > 0)
                                {
                                    ADATrace.WriteLine("Adding Errors to return value");
                                    ret.Errors.AddRange(e.Errors);
                                }

                                foreach (KeyValuePair<string, MethodResultRecord> x in e.MethodResultHistory)
                                {
                                    string newKey = ruleSetData.Name + "." + x.Key;

                                    if (_trace) ADATrace.WriteLine("Adding MethodResult History. Key [" + newKey + "]");
                                    ret.MethodResultHistory.Add(newKey, x.Value.Result.ToString());
                                }

                                // Add this set of results to the return dictionary, key on RuleSetName
                                if (_trace) ADATrace.WriteLine("Adding Results to return value. RuleSetName [" + ruleSetData.Name + "]");
                                ret.Results.Add(ruleSetData.Name, e.Results);

                                if (_trace)
                                    ADATrace.WriteLine("Entity Total Score=" + e.TotalEntityScore.ToString() + ". UserKACount=" + e.UserKACount.ToString());

                                ret.RuleGroupScore += e.AddUpEntityRuleSetScores();
                                ret.RuleGroupKeyAttractorCount += e.UserKACount; // +((autoKACount == null) ? 0 : (int)autoKACount);

                                if (_trace)
                                    ADATrace.WriteLine("RuleGroupScore=" + ret.RuleGroupScore.ToString() + ". RuleGroupKeyAttractorCount=" + ret.RuleGroupKeyAttractorCount.ToString());

                                e.AssignUsedProperties(request.Entity);

                                DataServices dataServices = new DataServices(request.Entity.ctx);

                                if(_recordTimingMetrics)
                                    InsertScoringTiming(0, 0, dataServices, sw, request.Entity.RuleSetId, request.Entity.ctx.Who, ruleSetData.Name);

                                sw.Restart();
                            }
                            finally
                            {
                                if (_trace)
                                {
                                    ADATrace.IndentLevel -= 1;
                                    ADATrace.WriteLine("Finished scoring ruleset: " + ruleSetData.Name);

                                    ADATrace.IndentLevel = saveIndentLevel;
                                }
                            }
                        }
                    }
                    #endregion
                }

            }
            catch (ScoringException sex)  // Special exception generated by developer in the rule code itself.
            {
                ret.RuleGroupScore = 0;
                ret.RuleGroupKeyAttractorCount = 0;

                if (_trace) ADATrace.WriteLine("USER SCORE EXCEPTION: Calculate Risk: " + sex.Message);

                throw sex;

            }
            catch (Exception ex)   //  Something went wrong. Usually mismatch in rule code and properties (parameters, return type, etc)
            {
                ret.RuleGroupScore = -1;
                ret.RuleGroupKeyAttractorCount = -1;
                ret.Errors.Add("Risk Engine Exception: " + ex.Message);

                if (_trace) ADATrace.WriteLine("EXCEPTION: Calculate Risk: " + ex.Message + " Possible mismatch in rule code and property parameters or return type.)");
            }
            finally
            {
                request.Entity.Locked = true;
            }
            return ret;
        }

        public RuleGroupData GetWorkingRuleGroupByPath(string entityPath, int orgId)
        {
            return new Service.Components.RiskEngineService().GetWorkingRuleGroupByPath(entityPath, orgId);
        }

        public RuleGroupData GetLiveRuleGroupByPath(string entityPath, int orgId)
        {
            return new Service.Components.RiskEngineService().GetLiveRuleGroupByPath(entityPath, orgId);
        }

        private void InsertScoringTiming(int riskClaimId, int? existingBaseRiskClaimId, DataServices dataServices, Stopwatch sw, int ruleNumber, string who, string entityType)
        {
            dataServices.InsertTimingMetric(new TimingMetric
            {
                EntityType = entityType,
                RuleNumber = ruleNumber,
                TimingInMs = Convert.ToInt32(sw.ElapsedMilliseconds),
                Created = DateTime.Now,
                CreatedBy = who,
                RiskClaim_Id = riskClaimId,
                BaseRiskClaim_Id = Convert.ToInt32(existingBaseRiskClaimId),
                TimingType = (int)MDA.Common.Enum.TimingType.Scoring
            });
        }

    }
}
