﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
 
		/// <summary>
		/// Do any of the email addresses on this claim provided by this person have a key attractor
		/// </summary>
		/// <param name="db"></param>
		/// <param name="personId"></param>
		/// <param name="riskClaimId"></param>
		/// <returns></returns>
		public List<Person_IsEmailKeyAttractor> Person_IsEmailKeyAttractor(int personId, int riskClaimId)
		{
			return (from p2e in _db.Person2Email.AsNoTracking()
					where p2e.Person_Id == personId
					&& p2e.RiskClaim_Id == riskClaimId
					&& p2e.RecordStatus == (byte)ADARecordStatus.Current
					&& p2e.Email.KeyAttractor != null && p2e.Email.KeyAttractor.Length > 0
                    && (p2e.Email.FraudRingClosed_ == null || p2e.Email.FraudRingClosed_ == "No")
					select new Person_IsEmailKeyAttractor()
					{
						Email = p2e.Email.EmailAddress,
						KeyAttractor = p2e.Email.KeyAttractor
					}).ToList();

		}

	}
}
