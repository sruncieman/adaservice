﻿using MDA.Common.Server;
using MDA.DAL;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        private bool _trace = false;
        private CurrentContext _ctx;
        private MdaDbContext _db;
        private DataServices _dataServices;

        public PropertyDataServices(CurrentContext ctx)
        {
            _ctx = ctx;
            _db = ctx.db as MdaDbContext;
            _trace = ctx.TraceLoad;

            _dataServices = new MDA.DataService.DataServices(ctx);
        }
    }
}
