﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
  
        public List<Organisation_IsKeyAttractor_Result> Organisation_IsKeyAttractor(EntityAliases organisationAliases, int organisationId, string cacheKey, MessageCache messageCache, bool _trace)
        {

            List<int> organisationsId = _dataServices.FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o in _db.Organisations
                    where organisationsId.Contains(o.Id)
                      && o.KeyAttractor != null
                      && o.KeyAttractor.Length > 0 
                      && (o.FraudRingClosed_ == null || o.FraudRingClosed_ == "No")
                    select new Organisation_IsKeyAttractor_Result()
                    {
                        OrganisationName = o.OrganisationName,
                        KeyAttractor = o.KeyAttractor
                    }).ToList();

        }

    }
}
