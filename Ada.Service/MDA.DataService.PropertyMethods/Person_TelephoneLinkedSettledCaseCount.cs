﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using MDA.Common.Helpers;
using MDA.Common.Debug;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        public List<uspAddressLinkedSettledCaseCountMessages_Result> Person_TelephoneLinkedSettledCaseCount(int personId, int riskClaimId, int category, int? claimTypeId, bool? isPotentialClaimant, DateTime incidentDate, ListOfInts peopleAliasIds, string periodSkip, string periodCount, bool _trace)
        {

            var telephoneId = (from t in _db.Person2Telephone
                               where t.Person_Id == personId
                               && t.RiskClaim_Id == riskClaimId
                               select t.Telephone_Id).ToList();

            // Get people attached telephone who are not on this claim/incident or an alias of a person on this claim/incident
            List<int> peopleNotOnClaim = (from p2t in _db.Person2Telephone
                                          where telephoneId.Contains(p2t.Telephone_Id)
                                          && p2t.RiskClaim_Id != riskClaimId
                                          && !peopleAliasIds.Contains(p2t.Person_Id)
                                          && p2t.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select p2t.Person_Id).ToList();

            #region Set start and end dates

            PropertyDateRange dateRange = new PropertyDateRange(null, periodSkip, periodCount);

            if (_trace)
                ADATrace.WriteLine(string.Format("Date Range: StartDate[{0}] : EndDate[{1}]", dateRange.StartDate.ToString(), dateRange.EndDate.ToString()));

            #endregion


            if (peopleNotOnClaim.Count() > 0)
            {

                string personIdsAsCommaList = string.Join(",", peopleNotOnClaim);

                if (_trace)
                    ADATrace.WriteLine(string.Format("Other People Linked to Telephone: [{0}]", personIdsAsCommaList));

                var ret = _db.uspAddressLinkedSettledCaseCountMessages(personIdsAsCommaList, true, true, false, false,
                    dateRange.StartDate, dateRange.EndDate, category, claimTypeId, isPotentialClaimant).ToList();

                if (ret.Count() > 0)
                {
                    return ret;
                }
            }

            return new List<uspAddressLinkedSettledCaseCountMessages_Result>();

        }

    }
}
