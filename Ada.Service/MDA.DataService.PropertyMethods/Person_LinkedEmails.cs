﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
 
		public List<Person_LinkedEmails> Person_LinkedEmails(EntityAliases personAliases, int personId, int riskClaimId, bool _trace)
		{

			List<int> emails = (from e in _db.Person2Email.AsNoTracking()
								where e.Person_Id == personId
								&& e.RiskClaim_Id == riskClaimId
								select e.Email.Id).ToList();

			List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

			return (from p2e in _db.Person2Email.AsNoTracking()
					where peopleId.Contains(p2e.Person_Id)
					&& !emails.Contains(p2e.Email.Id)
					&& (p2e.RiskClaim_Id != riskClaimId || p2e.RiskClaim_Id == null)
					&& (p2e.LinkConfidence == (int)LinkConfidence.Confirmed || p2e.LinkConfidence == (int)LinkConfidence.Unconfirmed)
					&& p2e.ADARecordStatus == (byte)ADARecordStatus.Current
					select new Person_LinkedEmails()
					{
						Confirmed = p2e.LinkConfidence == (int)LinkConfidence.Confirmed,
						EmailAddress = p2e.Email.EmailAddress
					}).ToList();
		}

	}
}
