﻿using System.Linq;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        public int Incident_NumberOfClaimants(int incidentId, int riskClaimId)
        {

            int count = 0;

            var people = (from i2p in _db.Incident2Person 
                          where i2p.Incident_Id == incidentId
                          && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                          && i2p.RiskClaim_Id == riskClaimId
                          select i2p).Distinct();

            if (people == null) return 0;

            foreach (var p in people)
            {
                int partyType = p.PartyType_Id;
                int subPartyType = p.SubPartyType_Id;

                var claimantTypes = _db.uspGetPotentialClaimant(partyType, subPartyType);

                foreach (var pc in claimantTypes)
                {
                    if (pc.Flag == 1)
                    {
                        count++;
                    }
                }

            }

            return count;
        }

    }

}
