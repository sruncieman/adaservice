﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using MDA.Common.Helpers;
using MDA.Common.Debug;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<uspAddressNumberOpenCasesMessages_Result> Address_NumberOpenCases(int addressId, int riskClaimId, string linkType, string keoghsCaseLinkType, bool stagedContrived, bool? isPotentialClaimant, DateTime incidentDate, ListOfInts peopleAliasIds, string periodSkip, string periodCount, string cacheKey, MessageCache messageCache, bool _trace)
        {

            #region Build list of acceptable p2a address links
            List<int> addressLinkType = new List<int>();

            linkType = linkType.ToLower();

            if (linkType == "current")
            {
                addressLinkType.Add((int)MDA.Common.Enum.AddressLinkType.CurrentAddress);
                addressLinkType.Add((int)MDA.Common.Enum.AddressLinkType.LivesAt);
                addressLinkType.Add((int)MDA.Common.Enum.AddressLinkType.Owner);
            }
            else if (linkType == "notcurrent")
            {

                addressLinkType = (from alt in _db.AddressLinkTypes
                                   where alt.Id != (int)MDA.Common.Enum.AddressLinkType.CurrentAddress
                                   && alt.Id != (int)MDA.Common.Enum.AddressLinkType.LivesAt
                                   && alt.Id != (int)MDA.Common.Enum.AddressLinkType.Owner
                                   select alt.Id).ToList();
            }
            else
            {
                addressLinkType = (from alt in _db.AddressLinkTypes
                                   select alt.Id).ToList();
            }
            #endregion

            // Get people attached to THIS address who are not on this claim/incident or an alias of a person on this claim/incident
            List<int> peopleNotOnClaim = (from p2a in _db.Person2Address
                                          where p2a.Address_Id == addressId
                                          && addressLinkType.Contains(p2a.AddressLinkType_Id)  // Check the linktype between the person and the address
                                          && p2a.RiskClaim_Id != riskClaimId
                                          && !peopleAliasIds.Contains(p2a.Person_Id)
                                          && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select p2a.Person_Id).ToList();

            #region Set start and end dates

            PropertyDateRange dateRange = new PropertyDateRange(null, periodSkip, periodCount);

            if (_trace)
                ADATrace.WriteLine(string.Format("Date Range: StartDate[{0}] : EndDate[{1}]", dateRange.StartDate.ToString(), dateRange.EndDate.ToString()));

            #endregion


            if (peopleNotOnClaim.Count() > 0)
            {

                int keoghsCase2IncidentLinkType = 1;

                if (keoghsCaseLinkType == "Info Only")
                {
                    keoghsCase2IncidentLinkType = 2;
                }

                List<int> i = new List<int>();

                foreach (var personId in peopleNotOnClaim)
                {

                    var ret = _db.uspAddressNumberOpenCasesMessages(personId, true, true, false, false, dateRange.StartDate, dateRange.EndDate, keoghsCase2IncidentLinkType, isPotentialClaimant, stagedContrived).ToList();

                    if (ret.Count() > 0)
                    {
                        return ret;
                    }

                }


            }

            return new List<uspAddressNumberOpenCasesMessages_Result>();

        }
    }
}
