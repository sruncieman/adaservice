﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.Common.Helpers;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;


namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        

        #region Backup
        //public int Address_NumberOfCases(int addressId, string matchType, /*string caseSource,*/ bool? isPotentialClaimant, string periodSkip, string periodCount, int? personId, string cacheKey, MessageCache messageCache, bool _trace)
        //{
        //    int count = 0;

        //    bool LookForConfirmed = string.Compare(matchType, "confirmed", true) == 0;
        //    bool LookForUnConfirmed = string.Compare(matchType, "unconfirmed", true) == 0;
        //    //bool LookForTentative = string.Compare(matchType, "tentative", true) == 0;

        //    //PropertyDateRange dateRange = new PropertyDateRange(periodSkip, periodCount);

        //    //DateTime enddate = dateRange.EndDate; //DateTime.Now.AddMonths(-monthSkip).Date;
        //   // DateTime startdate = dateRange.StartDate; //enddate.AddMonths(-monthCount).Date;

        //    IQueryable<int> people = null;

        //    if (LookForConfirmed)
        //    {
        //        people = from p2a in _db.Person2Address.AsNoTracking()
        //                 where
        //                     p2a.Address_Id == addressId
        //                     && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
        //                     && p2a.LinkConfidence == (int)LinkConfidence.Confirmed
        //                 select p2a.Person_Id;
        //    }
        //    else if (LookForUnConfirmed)
        //    {
        //        people = from p2a in _db.Person2Address.AsNoTracking()
        //                 where
        //                    p2a.Address_Id == addressId
        //                    && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
        //                    && p2a.LinkConfidence == (int)LinkConfidence.Unconfirmed
        //                 select p2a.Person_Id;
        //    }


        //    foreach (int pid in people)
        //    {
        //        if (pid != personId)
        //        {
        //            ListOfInts aliases = FindAllOrganisationAliasesFromDb(pid, true, false, false, _trace);   // Only confirmed people

        //            EntityAliases personAliases = new EntityAliases("confirmed", aliases, null, null);

        //            var x = Person_NumberOfCases(pid, personAliases, /*caseSource,*/ isPotentialClaimant, periodSkip, periodCount, _trace);

        //            count += (x != null) ? x.Count() : 0;
        //        }
        //    }

        //    return count;
        //}
        #endregion 

        public List<Address_NumberOfCases_Result> Address_NumberOfCases(int addressId, int riskClaimId, string linkType, bool? isPotentialClaimant, DateTime incidentDate, ListOfInts peopleAliasIds, string incidentType, string claimType, string periodSkip, string periodCount, string cacheKey, MessageCache messageCache, bool _trace)
        {

            #region Build list of acceptable p2a address links
            List<int> addressLinkType = new List<int>();

            linkType = linkType.ToLower();

            if (linkType == "current")
            {
                addressLinkType.Add((int)MDA.Common.Enum.AddressLinkType.CurrentAddress);
                addressLinkType.Add((int)MDA.Common.Enum.AddressLinkType.LivesAt);
                addressLinkType.Add((int)MDA.Common.Enum.AddressLinkType.Owner);
            }
            else if (linkType == "notcurrent")
            {

                addressLinkType = (from alt in _db.AddressLinkTypes
                                   where alt.Id != (int)MDA.Common.Enum.AddressLinkType.CurrentAddress
                                   && alt.Id != (int)MDA.Common.Enum.AddressLinkType.LivesAt
                                   && alt.Id != (int)MDA.Common.Enum.AddressLinkType.Owner
                                   select alt.Id).ToList();
            }
            else
            {
                addressLinkType = (from alt in _db.AddressLinkTypes
                                   select alt.Id).ToList();
            }
            #endregion

            // Get people attached to THIS address who are not on this claim/incident or an alias of a person on this claim/incident
            List<int> peopleNotOnClaim = (from p2a in _db.Person2Address
                                         where p2a.Address_Id == addressId
                                         && addressLinkType.Contains(p2a.AddressLinkType_Id)  // Check the linktype between the person and the address
                                         && p2a.RiskClaim_Id != riskClaimId
                                         && !peopleAliasIds.Contains(p2a.Person_Id)
                                         && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select p2a.Person_Id).ToList();

            #region Set start and end dates

            DateTime enddate; 
            DateTime startdate; 

            PropertyDateRange dateRange = new PropertyDateRange(incidentDate, periodSkip, periodCount);

            enddate = dateRange.EndDate; 
            startdate = dateRange.StartDate;

            #endregion

            #region Build list of incident types

            int it = 99;
            List<int> incidentTypes = new List<int>();

            if (string.IsNullOrEmpty(incidentType))
            {
                incidentTypes = (from x in _db.IncidentTypes select x.Id).ToList();
            }
            else if (incidentType.StartsWith("-"))
            {
                //incidentType = incidentType.Replace("-","");

                it = Math.Abs(Convert.ToInt32(incidentType));

                incidentTypes = (from x in _db.IncidentTypes
                                 where x.Id != it
                                 select x.Id).ToList();

            }
            else
            {
                it = Convert.ToInt32(incidentType);
                incidentTypes.Add(it);
            }

            #endregion

            #region Build list of claim types

            int ct = 99;
            List<int> claimTypes = new List<int>();

            if (string.IsNullOrEmpty(claimType))
            {
                claimTypes = (from x in _db.ClaimTypes select x.Id).ToList();
            }
            else if (claimType.StartsWith("-"))
            {
                //claimType = claimType.Replace("-", "");
                ct = Math.Abs(Convert.ToInt32(claimType));

                claimTypes = (from x in _db.ClaimTypes
                              where x.Id != ct
                              select x.Id).ToList();

            }
            else
            {
                ct = Convert.ToInt32(claimType);
                claimTypes.Add(ct);
            }

            #endregion

            if (peopleNotOnClaim.Count() > 0)
            {

                List<int> i = new List<int>();

                if (isPotentialClaimant != null)
                {
                    i.Add(Convert.ToInt32(isPotentialClaimant));                  
                }
                else
                {
                    i.Add(0);
                    i.Add(1);
                }

                // Get cases for all people not on this claim
                var cases = (from i2p in _db.Incident2Person
                             join kc2i in _db.KeoghsCase2Incident on i2p.Incident_Id equals kc2i.Incident_Id
                             join pc in _db.PotentialClaimants on new { i2p.PartyType_Id, i2p.SubPartyType_Id } equals new { PartyType_Id = pc.PartyType, SubPartyType_Id = pc.SubPartyType }
                             where peopleNotOnClaim.Contains(i2p.Person_Id)
                             && i2p.Incident.IncidentDate >= startdate && i2p.Incident.IncidentDate <= enddate
                             && claimTypes.Contains(kc2i.Incident.ClaimType_Id)
                             && incidentTypes.Contains(kc2i.Incident.IncidentType_Id) 
                             && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                             && i2p.Incident.ADARecordStatus == (byte)ADARecordStatus.Current
                             && kc2i.ADARecordStatus == (byte)ADARecordStatus.Current
                             && kc2i.KeoghsCase.ADARecordStatus == (byte)ADARecordStatus.Current
                             && i.Contains(pc.Flag) //|| i == 3
                             select new Address_NumberOfCases_Result
                             {
                                 DateOfBirth = i2p.Person.DateOfBirth,
                                 EliteReference = kc2i.KeoghsCase.KeoghsEliteReference,
                                 FirstName = i2p.Person.FirstName,
                                 FiveGrading = kc2i.FiveGrading,
                                 IncidentDate = i2p.Incident.IncidentDate,
                                 LastName = i2p.Person.LastName,
                                 PartyTypeText = i2p.PartyType.PartyTypeText,
                                 SubPartyText = i2p.SubPartyType.SubPartyText,
                                 LinkedClaimType = i2p.Incident.ClaimType.ClaimType1,
                                 Insurer = i2p.Insurer,
                                 InsurerClaimRef = i2p.ClaimNumber
                             }).Distinct().ToList();


                return cases;

            }

            return new List<Address_NumberOfCases_Result>();

        }

  
    }
}
