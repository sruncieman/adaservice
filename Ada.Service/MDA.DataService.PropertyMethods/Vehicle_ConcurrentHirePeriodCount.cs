﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public int Vehicle_ConcurrentHirePeriodCount(int riskClaimId, int vehicleId, EntityAliases vehicleAliases, DateTime? hireStartDate, DateTime? hireEndDate)
        {
            var count = _db.uspConcurrentHireVehiclePeriodCount(vehicleId, riskClaimId, vehicleAliases.IncludeThis, vehicleAliases.IncludeConfirmed, vehicleAliases.IncludeUnconfirmed, vehicleAliases.IncludeTentative, hireStartDate, hireEndDate).FirstOrDefault();

            return (count == null) ? 0 : (int)count;

            //List<int> vehicleAliasIds = _dataServices.FindAllVehicleAliases(vehicleId, vehicleAliases, _trace);

            //var hirePeriod = (from x in _db.Vehicle2Person
            //                  where vehicleAliasIds.Contains(x.Vehicle_Id)
            //                  && x.RiskClaim_Id != riskClaimId
            //                  && x.HireStartDate <= hireEndDate && x.HireEndDate >= hireStartDate
            //                  && x.VehicleLinkType_Id == (int)MDA.Common.Enum.VehicleLinkType.Hirer
            //                  && x.ADARecordStatus == (byte)ADARecordStatus.Current
            //                  select x).ToList();

            //return hirePeriod.Count();

        }

    }

}
