﻿using System;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_IsTOGAttractor_Result
    {
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public DateTime? DateOfBirth { get; set; }

        public Person_IsTOGAttractor_Result Clone()
        {
            return new Person_IsTOGAttractor_Result()
            {
                FirstName = this.FirstName,
                LastName = this.LastName,
                DateOfBirth = this.DateOfBirth,
            };
        }
    }
}
