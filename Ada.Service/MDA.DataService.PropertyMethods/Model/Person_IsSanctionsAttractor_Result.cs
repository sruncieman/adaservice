﻿using System;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_IsSanctionsAttractor_Result
    {
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public DateTime? DateOfBirth { get; set; }
        [DataMember]
        public string KeyAttractor { get; set; }

        public Person_IsSanctionsAttractor_Result Clone()
        {
            return new Person_IsSanctionsAttractor_Result()
            {
                FirstName = this.FirstName,
                LastName = this.LastName,
                DateOfBirth = this.DateOfBirth,
                KeyAttractor = this.KeyAttractor
            };
        }
    }
}
