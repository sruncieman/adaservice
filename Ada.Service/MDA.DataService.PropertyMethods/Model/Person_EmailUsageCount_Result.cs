﻿using System;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_EmailUsageCount_Result
    {
        [DataMember]
        public int Person_Id { get; set; }
        [DataMember]
        public string Salutation { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public DateTime? DateOfBirth { get; set; }
        [DataMember]
        public DateTime? IncidentDateWhereEmailUsedByThisPerson { get; set; }
        [DataMember]
        public string Incident2PersonFiveGrading { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public int IncidentType { get; set; }

        public Person_EmailUsageCount_Result Clone()
        {
            return new Person_EmailUsageCount_Result()
            {
                Person_Id = this.Person_Id,
                Salutation = this.Salutation,
                FirstName = this.FirstName,
                LastName = this.LastName,
                DateOfBirth = this.DateOfBirth,
                IncidentDateWhereEmailUsedByThisPerson = this.IncidentDateWhereEmailUsedByThisPerson,
                Incident2PersonFiveGrading = this.Incident2PersonFiveGrading,
                Source = this.Source,
                IncidentType = this.IncidentType
            };
        }
    }
}
