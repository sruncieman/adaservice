﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Organisation_LinkedAddresses
    {
        [DataMember]
        public string FullAddress { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Organisation_LinkedAddresses Clone()
        {
            return new Organisation_LinkedAddresses()
            {
                FullAddress = this.FullAddress,
                Confirmed = this.Confirmed
            };
        }

    }
}
