﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Organisation_IsKeyAttractor_Result
    {
        [DataMember]
        public string OrganisationName { get; set; }
        [DataMember]
        public string KeyAttractor { get; set; }

        public Organisation_IsKeyAttractor_Result Clone()
        {
            return new Organisation_IsKeyAttractor_Result()
            {
                OrganisationName = this.OrganisationName,
                KeyAttractor = this.KeyAttractor
            };
        }
    }
}
