﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_LinkedAddresses_Query
    {
        [DataMember]
        public string FullAddress { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }
    }

    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_LinkedAddresses_Result
    {
        [DataMember]
        public string FullAddress { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Person_LinkedAddresses_Result Clone()
        {
            return new Person_LinkedAddresses_Result()
            {
                Confirmed = this.Confirmed,
                //FullAddress = this.FullAddress
            };
        }
    }
}
