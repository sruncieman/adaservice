﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_LinkedDrivingLicenceNumbers
    {
        [DataMember]
        public string DrivingLicenceNumber { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Person_LinkedDrivingLicenceNumbers Clone()
        {
            return new Person_LinkedDrivingLicenceNumbers()
            {
                DrivingLicenceNumber = this.DrivingLicenceNumber,
                Confirmed = this.Confirmed
            };
        }
    }
}
