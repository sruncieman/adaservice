﻿using System;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_AliasInformation_Result
    {
        [DataMember]
        public int Person_Id { get; set; }
        [DataMember]
        public string Salutation { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public DateTime? DateOfBirth { get; set; }
        [DataMember]
        public string Occupation { get; set; }

        public Person_AliasInformation_Result Clone()
        {
            return new Person_AliasInformation_Result()
            {
                DateOfBirth = this.DateOfBirth,
                FirstName = this.FirstName,
                LastName = this.LastName,
                Occupation = this.Occupation,
                Person_Id = this.Person_Id,
                Salutation = this.Salutation
            };
        }
    }

}
