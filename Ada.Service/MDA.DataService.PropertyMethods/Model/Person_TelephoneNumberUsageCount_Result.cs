﻿using System;
using MDA.DAL;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_TelephoneNumberUsageCount_Result
    {
        [DataMember]
        public int Person_Id { get; set; }
        [DataMember]
        public string Salutation { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public DateTime? DateOfBirth { get; set; }
        [DataMember]
        public DateTime? IncidentDateWhereTeleNumberUsedByThisPerson { get; set; }
        [DataMember]
        public string Incident2PersonFiveGrading { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string IncidentType { get; set; }
        [DataMember]
        public string TelephoneTypeText { get; set; }
        [DataMember]
        public string TelephoneNumber { get; set; }
        [DataMember]
        public string FormattedAddress { get; set; }
        [DataMember]
        public Address Address { get; set; }

        public Person_TelephoneNumberUsageCount_Result Clone()
        {
            return new Person_TelephoneNumberUsageCount_Result()
            {
                Person_Id = this.Person_Id,
                Salutation = this.Salutation,
                FirstName = this.FirstName,
                LastName = this.LastName,
                DateOfBirth = this.DateOfBirth,
                IncidentDateWhereTeleNumberUsedByThisPerson = this.IncidentDateWhereTeleNumberUsedByThisPerson,
                Incident2PersonFiveGrading = this.Incident2PersonFiveGrading,
                Source = this.Source,
                IncidentType = this.IncidentType,
                TelephoneTypeText = this.TelephoneTypeText,
                TelephoneNumber = this.TelephoneNumber,
                FormattedAddress = this.FormattedAddress,
                Address = this.Address
            };
        }
    }
}
