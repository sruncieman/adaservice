﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;


namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<Address_IsKeyAttractor_Result> Address_IsKeyAttractor(int addressId)
        {

            return (from x in _db.Addresses
                    where x.Id == addressId
                    && x.RecordStatus == (byte)ADARecordStatus.Current
                    && x.KeyAttractor != null && x.KeyAttractor.Length > 0
                    select new Address_IsKeyAttractor_Result()
                    {
                        //Address = x.BuildingNumber + " " + x.SubBuilding + " " + x.Building + " " + x.Street + " " + x.Locality + " " + x.Town + " " + x.County + " " + x.PostCode,
                        Address = x,
                        KeyAttractor = x.KeyAttractor
                    }).ToList();

        }
  
    }
}
