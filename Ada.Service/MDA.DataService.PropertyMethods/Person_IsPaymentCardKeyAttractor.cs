﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Do any of the cards provided with this claim by this person have a key attractor
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <param name="riskClaimId"></param>
        /// <returns></returns>
        /// 

        public List<Person_IsPaymentCardKeyAttractor> Person_IsPaymentCardKeyAttractor(int personId, int riskClaimId)
        {
            return (from p2c in _db.Person2PaymentCard.AsNoTracking()
                    where p2c.Person_Id == personId
                    && p2c.RiskClaim_Id == riskClaimId
                    && p2c.RecordStatus == (byte)ADARecordStatus.Current
                    && p2c.PaymentCard.KeyAttractor != null && p2c.PaymentCard.KeyAttractor.Length > 0
                    select new Person_IsPaymentCardKeyAttractor()
                    {
                        PaymentCard = p2c.PaymentCard.PaymentCardNumber,
                        KeyAttractor = p2c.PaymentCard.KeyAttractor
                    }).ToList();

        }

	}
}
