﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
 
		public List<Person_LinkedNINumbers> Person_LinkedNINumbers(EntityAliases personAliases, int personId, int riskClaimId, bool _trace)
		{

			List<int> nInumbers = (from e in _db.Person2Email.AsNoTracking()
								   where e.Person_Id == personId
								   && e.RiskClaim_Id == riskClaimId
								   select e.Email.Id).ToList();

			List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

			return (from p2n in _db.Person2NINumber.AsNoTracking()
					where peopleId.Contains(p2n.Person_Id)
					&& !nInumbers.Contains(p2n.NINumber.Id)
					&& (p2n.RiskClaim_Id != riskClaimId || p2n.RiskClaim_Id == null)
					&& (p2n.LinkConfidence == (int)LinkConfidence.Confirmed || p2n.LinkConfidence == (int)LinkConfidence.Unconfirmed)
					&& p2n.ADARecordStatus == (byte)ADARecordStatus.Current
					select new Person_LinkedNINumbers()
					{
						Confirmed = p2n.LinkConfidence == (int)LinkConfidence.Confirmed,
						NINumber = p2n.NINumber.NINumber1
					}).ToList();
		}

	}
}
