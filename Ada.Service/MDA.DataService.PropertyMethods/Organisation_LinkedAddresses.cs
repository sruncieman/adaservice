﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        public List<Organisation_LinkedAddresses> Organisation_LinkedAddresses(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            List<int> addresses = (from a in _db.Organisation2Address.AsNoTracking()
                                   where a.Organisation_Id == organisationId
                                   && a.RiskClaim_Id == riskClaimId
                                   select a.Address.Id).ToList();

            List<int> organisationsId = _dataServices.FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            foreach (var oId in organisationsId)
            {

                var addressList = (from o2a in _db.Organisation2Address.AsNoTracking()
                                   where organisationsId.Contains(o2a.Organisation_Id)
                                   && !addresses.Contains(o2a.Address.Id)
                                   && (o2a.RiskClaim_Id != riskClaimId || o2a.RiskClaim_Id == null)
                                   && o2a.LinkConfidence == (int)LinkConfidence.Confirmed
                                   && o2a.ADARecordStatus == (byte)ADARecordStatus.Current
                                   select o2a).ToList();

                List<Organisation_LinkedAddresses> res = (from x in addressList
                                                          select new Organisation_LinkedAddresses()
                                                          {
                                                              Confirmed = x.LinkConfidence == (int)LinkConfidence.Confirmed,
                                                              FullAddress = MDA.Common.Helpers.AddressHelper.FormatAddress(x.Address) //x.Address.BuildingNumber + " " + x.Address.SubBuilding + " " + x.Address.Building + " " + x.Address.Street + " " + x.Address.Locality + " " + x.Address.Town + " " + x.Address.County + " " + x.Address.PostCode
                                                          }).ToList();
                return res;
            }

            return null;

        }

    }
}
