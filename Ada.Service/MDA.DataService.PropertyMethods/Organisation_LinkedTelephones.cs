﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        public List<Organisation_LinkedTelephones> Organisation_LinkedTelephones(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            //List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            //return (from o2t in _db.Organisation2Telephone.AsNoTracking()
            //        where organisationsId.Contains(o2t.Organisation_Id)
            //        && (o2t.RiskClaim_Id != riskClaimId || o2t.RiskClaim_Id == null)
            //        && (o2t.LinkConfidence == (int)LinkConfidence.Confirmed || o2t.LinkConfidence == (int)LinkConfidence.Unconfirmed)
            //        && o2t.ADARecordStatus == (byte)ADARecordStatus.Current
            //        select new Organisation_LinkedTelephones()
            //        {
            //            Confirmed = o2t.LinkConfidence == (int)LinkConfidence.Confirmed,
            //            Telephone = o2t.Telephone.TelephoneNumber,
            //        }).ToList();

            List<int> telephones = (from t in _db.Organisation2Telephone
                                    where t.Organisation_Id == organisationId
                                    && t.RiskClaim_Id == riskClaimId
                                    select t.Telephone_Id).ToList();

            List<int> organisationsId = _dataServices.FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o2t in _db.Organisation2Telephone
                    where organisationsId.Contains(o2t.Organisation_Id)
                    && !telephones.Contains(o2t.Telephone.Id)
                    && (o2t.RiskClaim_Id != riskClaimId || o2t.RiskClaim_Id == null)
                    && (o2t.LinkConfidence == (int)LinkConfidence.Confirmed || o2t.LinkConfidence == (int)LinkConfidence.Unconfirmed)
                    && o2t.ADARecordStatus == (byte)ADARecordStatus.Current
                    select new Organisation_LinkedTelephones()
                    {
                        Confirmed = o2t.LinkConfidence == (int)LinkConfidence.Confirmed,
                        Telephone = o2t.Telephone.TelephoneNumber,
                    }).ToList();
        }

    }
}
