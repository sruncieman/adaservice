﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        public List<Organisation_TradingNames> Organisation_TradingNames(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            List<int> organisationsId = _dataServices.FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            #region Get list of links where the OrgId we want is at one end of the link
            var o2oList1 = (from o2o in _db.Organisation2Organisation.AsNoTracking()
                            where organisationsId.Contains(o2o.Organisation1_Id)
                            && (o2o.LinkConfidence == (int)LinkConfidence.Confirmed)
                            && o2o.ADARecordStatus == (byte)ADARecordStatus.Current
                            && o2o.OrganisationLinkType_Id == (int)MDA.Common.Enum.OrganisationLinkType.TradingNameOf
                            select new
                            {
                                id = o2o.Organisation1_Id,
                                name = o2o.Organisation.OrganisationName,
                                confidence = o2o.LinkConfidence
                            });
            #endregion

            #region Get list of links where the OrgId we want is at the other end of the link
            var o2oList2 = (from o2o in _db.Organisation2Organisation.AsNoTracking()
                            where organisationsId.Contains(o2o.Organisation2_Id)
                            && (o2o.LinkConfidence == (int)LinkConfidence.Unconfirmed)
                            && o2o.ADARecordStatus == (byte)ADARecordStatus.Current
                            && o2o.OrganisationLinkType_Id == (int)MDA.Common.Enum.OrganisationLinkType.TradingNameOf
                            select new
                            {
                                id = o2o.Organisation1_Id,
                                name = o2o.Organisation.OrganisationName,
                                confidence = o2o.LinkConfidence
                            });
            #endregion

            #region Join both lists together (UNION does a distinct)
            var fullList = o2oList1.Union(o2oList2);
            #endregion

            // Not sure if we have to worry about removing aliases here????

            #region Build return list. use compare class to only add a company name once
            List<Organisation_TradingNames> ret = new List<Organisation_TradingNames>();

            foreach (var x in fullList)
            {
                var r = new Organisation_TradingNames()
                {
                    TradingName = x.name,
                    Confirmed = x.confidence == (int)LinkConfidence.Confirmed
                };

                if (!ret.Contains(r, new Organisation_TradingNames_Comparer()))
                    ret.Add(r);
            }
            #endregion
            return ret;
        }
    }
    
    class Organisation_TradingNames_Comparer : IEqualityComparer<Organisation_TradingNames>
    {
        public bool Equals(Organisation_TradingNames x1, Organisation_TradingNames x2)
        {
            if (object.ReferenceEquals(x1, x2))
                return true;

            if (x1 == null || x2 == null)
                return false;

            return x1.TradingName.Equals(x2.TradingName);
        }

        public int GetHashCode(Organisation_TradingNames x)
        {
            return x.TradingName.GetHashCode();
        }
    }
}
