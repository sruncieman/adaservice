﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
  
        public List<Organisation_AliasInformation_Result> Organisation_AliasInformation(EntityAliases organisationAliases, int organisationId, bool _trace)
        {
            List<int> organisationsId = _dataServices.FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o in _db.Organisations
                    //join o2a in db.Organisation2Address on o.Id equals o2a.Organisation_Id
                    //join o2t in db.Organisation2Telephone on o.Id equals o2t.Organisation_Id
                    where organisationsId.Contains(o.Id)
                    select new Organisation_AliasInformation_Result()
                    {
                        OrganisationId = o.Id,
                        OrganisationName = o.OrganisationName,

                    }).ToList();
        }

    }
}
