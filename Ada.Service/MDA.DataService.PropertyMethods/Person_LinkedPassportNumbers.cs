﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
 
		public List<Person_LinkedPassportNumbers> Person_LinkedPassportNumbers(EntityAliases personAliases, int personId, int riskClaimId, bool _trace)
		{

			List<int> passports = (from p in _db.Person2Passport.AsNoTracking()
								   where p.Person_Id == personId
								   && p.RiskClaim_Id == riskClaimId
								   select p.Passport.Id).ToList();

			List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

			return (from p2pn in _db.Person2Passport.AsNoTracking()
					where peopleId.Contains(p2pn.Person_Id)
					&& !passports.Contains(p2pn.Passport.Id)
					&& (p2pn.RiskClaim_Id != riskClaimId || p2pn.RiskClaim_Id == null)
					&& (p2pn.LinkConfidence == (int)LinkConfidence.Confirmed || p2pn.LinkConfidence == (int)LinkConfidence.Unconfirmed)
					&& p2pn.ADARecordStatus == (byte)ADARecordStatus.Current
					select new Person_LinkedPassportNumbers()
					{
						Confirmed = p2pn.LinkConfidence == (int)LinkConfidence.Confirmed,
						PassportNumber = p2pn.Passport.PassportNumber
					}).ToList();
		}

	}
}
