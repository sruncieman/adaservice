﻿using System.Linq;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public bool Address_IsPostCodeKeyAttractor(int addressId)
        {
            return (from x in _db.Address2Address
                    where x.Address1_Id == addressId
                    && x.RecordStatus == (byte)ADARecordStatus.Current
                    && x.Address.KeyAttractor != null && x.Address.KeyAttractor.Length > 0
                    select x).Count() > 0;

        }
 
    }
}
