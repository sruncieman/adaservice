﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
 
		public List<Person_LinkedBankAccounts> Person_LinkedBankAccounts(EntityAliases personAliases, int personId, int riskClaimId, bool _trace)
		{

			List<int> bankAccounts = (from b in _db.Person2BankAccount.AsNoTracking()
									  where b.Person_Id == personId
									  && b.RiskClaim_Id == riskClaimId
									  select b.BankAccount.Id).ToList();

			List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

			return (from p2ba in _db.Person2BankAccount.AsNoTracking()
					where peopleId.Contains(p2ba.Person_Id)
					&& !bankAccounts.Contains(p2ba.BankAccount.Id)
					&& (p2ba.RiskClaim_Id != riskClaimId || p2ba.RiskClaim_Id == null)
					&& (p2ba.LinkConfidence == (int)LinkConfidence.Confirmed || p2ba.LinkConfidence == (int)LinkConfidence.Unconfirmed)
					&& p2ba.ADARecordStatus == (byte)ADARecordStatus.Current
					select new Person_LinkedBankAccounts()
					{
						Confirmed = p2ba.LinkConfidence == (int)LinkConfidence.Confirmed,
						BankAccounts = p2ba.BankAccount.AccountNumber
					}).ToList();
		}

	}
}
