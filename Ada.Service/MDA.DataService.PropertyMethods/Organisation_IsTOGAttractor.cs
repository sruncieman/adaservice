﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<Organisation_IsTOGAttractor_Result> Organisation_IsTOGAttractor(EntityAliases organisationAliases, int organisationId, string cacheKey, MessageCache messageCache, bool _trace)
        {

            List<int> organisationsId = _dataServices.FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o in _db.Organisations
                    where organisationsId.Contains(o.Id)
                    && o.CHFKeyAttractor == true
                    && o.RecordStatus == (byte)ADARecordStatus.Current
                    select new Organisation_IsTOGAttractor_Result()
                    {
                        OrganisationName = o.OrganisationName
                    }).ToList();
        }

    }
}
