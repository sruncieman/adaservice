﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
 
		public List<Person_LinkedDrivingLicenceNumbers> Person_LinkedDrivingLicenceNumbers(EntityAliases personAliases, int personId, int riskClaimId, bool _trace)
		{

			List<int> drivingLicences = (from d in _db.Person2DrivingLicense.AsNoTracking()
										 where d.Person_Id == personId
										 && d.RiskClaim_Id == riskClaimId
										 select d.DrivingLicense.Id).ToList();

			List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

			return (from p2d in _db.Person2DrivingLicense.AsNoTracking()
					where peopleId.Contains(p2d.Person_Id)
					&& !drivingLicences.Contains(p2d.DrivingLicense.Id)
					&& (p2d.RiskClaim_Id != riskClaimId || p2d.RiskClaim_Id == null)
					&& (p2d.LinkConfidence == (int)LinkConfidence.Confirmed || p2d.LinkConfidence == (int)LinkConfidence.Unconfirmed)
					&& p2d.ADARecordStatus == (byte)ADARecordStatus.Current
					select new Person_LinkedDrivingLicenceNumbers()
					{
						Confirmed = p2d.LinkConfidence == (int)LinkConfidence.Confirmed,
						DrivingLicenceNumber = p2d.DrivingLicense.DriverNumber
					}).ToList();
		}

	}
}
