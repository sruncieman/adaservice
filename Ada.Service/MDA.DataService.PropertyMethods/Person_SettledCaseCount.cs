﻿using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using MDA.Common.Helpers;
using MDA.Common.Debug;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        //public int Person_SettledCaseCount(int incidentId, int personId, string matchType, int category, bool? isPotentialClaimant, string periodSkip, string periodCount, string cacheKey, MessageCache messageCache, bool _trace)
        //{
        //    bool includeConfirmed;
        //    bool includeUnconfirmed;
        //    bool includeTentative;
        //    bool includeThis;

        //    ProcessMatchType(matchType, out includeThis, out includeConfirmed, out includeUnconfirmed, out includeTentative, _trace);


        //    DateTime enddate = DateTime.Now.AddMonths(-monthSkip).Date;
        //    DateTime startdate = enddate.AddMonths(-monthCount).Date;

        //    var count = db.uspPersonSettledCaseCount(personId, incidentId, includeThis, includeConfirmed, includeUnconfirmed, includeTentative, startdate, enddate, category, isPotentialClaimant).FirstOrDefault();

        //    return (count == null) ? 0 : (int)count;

        //    ////bool LookForConfirmed = string.Compare(matchType, "confirmed", true) == 0;
        //    ////bool LookForUnConfirmed = string.Compare(matchType, "unconfirmed", true) == 0;

        //    //DateTime enddate = DateTime.Now.AddMonths(-monthSkip).Date;
        //    //DateTime startdate = enddate.AddMonths(-monthCount).Date;

        //    //List<string> settlementStrings = MDA.Common.Helpers.LinqHelper.GetSettlementCategoryStrings(category);

        //    //List<int> peopleId = FindAllPersonAliases(db, personId, matchType, _trace);

        //    //int count = 0;

        //    //foreach (var s in settlementStrings)
        //    //{
        //    //    count += (from i2po in _db.Incident2PersonOutcome
        //    //              where i2po.MannerOfResolution == s
        //    //              && peopleId.Contains(i2po.Person_Id)
        //    //              && i2po.RecordStatus == (byte)ADARecordStatus.Current
        //    //              && i2po.Incident.IncidentDate >= startdate && i2po.Incident.IncidentDate <= enddate
        //    //              select i2po).Count();
        //    //}

        //    //return count;
        //}

        public List<uspPersonSettledCaseCountMessages_Result> Person_SettledCaseCount(int incidentId, int personId,
                                                int category, bool? isPotentialClaimant, string periodSkip, string periodCount,
                                                EntityAliases personAliases, bool _trace)
        {
            //bool includeConfirmed;
            //bool includeUnconfirmed;
            //bool includeTentative;
            //bool includeThis;

            //ProcessMatchType(matchType, out includeThis, out includeConfirmed, out includeUnconfirmed, out includeTentative, _trace);

            PropertyDateRange dateRange = new PropertyDateRange(null, periodSkip, periodCount);

            if (_trace)
                ADATrace.WriteLine(string.Format("Date Range: StartDate[{0}] : EndDate[{1}]", dateRange.StartDate.ToString(), dateRange.EndDate.ToString()));

            //enddate = dateRange.EndDate; // incidentDate.AddMonths(+monthSkip).Date;
            //startdate = dateRange.StartDate; // incidentDate.AddMonths(-monthCount).Date;

            return _db.uspPersonSettledCaseCountMessages(personId, incidentId, personAliases.IncludeThis,
                        personAliases.IncludeConfirmed, personAliases.IncludeUnconfirmed, personAliases.IncludeTentative,
                        dateRange.StartDate, dateRange.EndDate, category, isPotentialClaimant).ToList();

            //if (recs != null && recs.Count > 0)
            //{
            //    if ( includeThis || includeConfirmed )
            //    {
            //        // First 5 values are fixed entity values
            //        object[] fixedValues = messageCache.GetFixedEntityInputValues(cacheKey);
            //        string formatString = messageCache.GetMediumThenInMessage(cacheKey);

            //        foreach (var row in recs)
            //        {
            //            try
            //            {
            //                string s = string.Format(formatString, fixedValues[0], fixedValues[1], fixedValues[2], fixedValues[3], fixedValues[4],
            //                    row.PartyTypeText, row.SubPartyText, row.IncidentDate, row.ClaimType, row.KeoghsEliteReference,
            //                    row.I2P_FiveGrading, row.MannerOfResolution, row.MOR_FiveGrading);

            //                messageCache.AddMediumOutputMessage(cacheKey, s, null);
            //            }
            //            catch (System.FormatException)
            //            {
            //                messageCache.AddMediumOutputMessage(cacheKey, "Person_SettledCaseCount Format Error", null);
            //            }
            //        }
            //    }
            //    else if ( includeUnconfirmed )
            //    {
            //        // First 2 values are fixed entity values
            //        object[] fixedValues = messageCache.GetFixedEntityInputValues(cacheKey);
            //        string formatString = messageCache.GetMediumThenInMessage(cacheKey);

            //        foreach (var row in recs)
            //        {
            //            try
            //            {
            //                string s = string.Format(formatString, fixedValues[0], fixedValues[1], row.FirstName, row.LastName, row.DateOfBirth,
            //                    row.PartyTypeText, row.SubPartyText, row.IncidentDate, row.ClaimType, row.KeoghsEliteReference,
            //                    row.I2P_FiveGrading, row.MannerOfResolution, row.MOR_FiveGrading);

            //                messageCache.AddMediumOutputMessage(cacheKey, s, null);
            //            }
            //            catch (System.FormatException)
            //            {
            //                messageCache.AddMediumOutputMessage(cacheKey, "Person_SettledCaseCount Format Error", null);
            //            }
            //        }
            //    }

            //    return recs.Count();
            //}

            //return 0;
        }

    }
}
