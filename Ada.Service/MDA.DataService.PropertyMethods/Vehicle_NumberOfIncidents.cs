﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.Common.Helpers;
using LinqKit;
using RiskEngine.Model;
using MDA.Common.Debug;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<Vehicle_NumberOfIncidents_Result> Vehicle_NumberOfIncidents(int excludeIncidentId, DateTime incidentDate, int baseRiskClaimId, int vehicleId, EntityAliases vehicleAliases, string incidentSource, string incidentType, string periodSkip, string periodCount, string searchVehicleLinkType, string indexVehicleLinkType, bool _trace)
        {

            if (indexVehicleLinkType == searchVehicleLinkType)
            {

                List<int> vehicleAliasIds = _dataServices.FindAllVehicleAliases(vehicleId, vehicleAliases, _trace);

                DateTime enddate; // = DateTime.Now.AddMonths(-monthSkip).Date;
                DateTime startdate; // = enddate.AddMonths(-monthCount).Date;

                PropertyDateRange dateRange = new PropertyDateRange(incidentDate, periodSkip, periodCount);

                enddate = dateRange.EndDate; // incidentDate.AddMonths(+monthSkip).Date;
                startdate = dateRange.StartDate; // incidentDate.AddMonths(-monthCount).Date;

                if (_trace)
                    ADATrace.WriteLine(string.Format("Date Range: StartDate[{0}] : EndDate[{1}] : IncidentDate[{2}]", startdate.ToString(), enddate.ToString(), incidentDate.ToString()));

                string[] incidentSources = incidentSource.Split('|');
                string[] incidentTypes = incidentType.Split('|');

                var incidents = (from i2v in _db.Incident2Vehicle
                                 join i in _db.Incidents on i2v.Incident_Id equals i.Id
                                 join v in _db.Vehicles on i2v.Vehicle_Id equals v.Id
                                 where vehicleAliasIds.Contains(i2v.Vehicle_Id)
                                 && i2v.ADARecordStatus == (byte)ADARecordStatus.Current
                                 && i.ADARecordStatus == (byte)ADARecordStatus.Current
                                 && v.ADARecordStatus == (byte)ADARecordStatus.Current
                                 && (i2v.BaseRiskClaim_Id != baseRiskClaimId)
                                 && i.IncidentDate >= startdate && i.IncidentDate <= enddate
                                 && i2v.Incident_Id != excludeIncidentId
                                 //&& i2v.Incident2VehicleLinkType_Id == vehicleLinkType_Id
                                 select new Vehicle_NumberOfIncidents_Query()
                                 {
                                     Vehicle = v,
                                     Incident = i,
                                     //Incident2Person = i2p,
                                     Incident2Vehicle = i2v,
                                     RiskClaim = i2v.RiskClaim,
                                     //Person = v2p.Person,
                                     Involvement = i2v.Incident2VehicleLinkType.Text,
                                 });

                var predicate1 = PredicateBuilder.False<Vehicle_NumberOfIncidents_Query>();

                bool pred1used = false;

                foreach (string s in incidentSources)
                {
                    if (string.Compare(s, "CUE", true) == 0)
                    {
                        predicate1 = predicate1.Or(i => i.Incident.Source == "CUE");
                        pred1used = true;
                    }
                    else if (string.Compare(s, "ADA", true) == 0)
                    {
                        predicate1 = predicate1.Or(i => i.Incident.Source == "ADA");
                        pred1used = true;
                    }
                    else if (string.Compare(s, "ACTIVE", true) == 0)
                    {
                        predicate1 = predicate1.Or(i => i.Incident.Source != "Keoghs CMS");
                        pred1used = true;
                    }
                    else if (string.Compare(s, "INFO", true) == 0)
                    {
                        predicate1 = predicate1.Or(i => i.Incident.Source == "Keoghs InfoOnly");
                        pred1used = true;
                    }
                }

                var predicate2 = PredicateBuilder.False<Vehicle_NumberOfIncidents_Query>();

                bool pred2used = false;

                foreach (string s in incidentTypes)
                {
                    if (string.Compare(s, "RTC", true) == 0)
                    {
                        predicate2 = predicate2.Or(i => i.Incident.IncidentType_Id == (int)MDA.Common.Enum.ClaimType.Motor);
                        pred2used = true;
                    }
                }

                if (pred1used)
                    incidents = incidents.AsExpandable().Where(predicate1);

                if (pred2used)
                    incidents = incidents.AsExpandable().Where(predicate2);

                var incidentList = incidents.ToList();

                var incidentList2 = incidentList.Distinct(new CompareVehicleNumberOfIncidents_Query()).ToList();

                List<Vehicle_NumberOfIncidents_Result> res = (from x in incidentList2
                                                              select new Vehicle_NumberOfIncidents_Result()
                                                              {
                                                                  IncidentDate = x.Incident.IncidentDate,
                                                                  //Insurer = x.Incident2Person.Insurer,
                                                                  ClaimRef = x.RiskClaim.ClientClaimRefNumber,
                                                                  //DriverDetails = x.Person.FirstName + " " + x.Person.LastName + " " + x.Person.DateOfBirth.ToString(),
                                                                  Involvement = x.Involvement,
                                                                  //VehicleDetails = x.Vehicle.VehicleRegistration + " " + x.Vehicle.VehicleMake + " " + x.Vehicle.Model
                                                              }).ToList();
                return res;

            }

            return null;

        }
    
    }

    /// <summary>
    /// A class to allow us to weed out duplicate INCIDENTS in this records set.  Called in a DISTINCT()
    /// </summary>
    class CompareVehicleNumberOfIncidents_Query : IEqualityComparer<Vehicle_NumberOfIncidents_Query>
    {
        public bool Equals(Vehicle_NumberOfIncidents_Query x, Vehicle_NumberOfIncidents_Query y)
        {
            return x.Incident.Id == y.Incident.Id;
        }
        public int GetHashCode(Vehicle_NumberOfIncidents_Query codeh)
        {
            return codeh.Incident.Id.GetHashCode();
        }
    }
}
