﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using MDA.Common.Helpers;
using MDA.Common.Debug;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<uspAddressNumberOpenCasesMessages_Result> Person_TelephoneNumberOfOpenCases(int personId, int riskClaimId, ListOfInts peopleAliasIds, EntityAliases personAliases, string linkType, bool? isPotentialClaimant, bool? stagedContrived, string periodSkip, string periodCount, bool _trace)
        {


            var telephoneId = (from t in _db.Person2Telephone
                               where t.Person_Id == personId
                               && t.RiskClaim_Id == riskClaimId
                               select t.Telephone_Id).ToList();

            // Get people attached telephone who are not on this claim/incident or an alias of a person on this claim/incident
            List<int> peopleNotOnClaim = (from p2t in _db.Person2Telephone
                                          where telephoneId.Contains(p2t.Telephone_Id)
                                          && p2t.RiskClaim_Id != riskClaimId
                                          && !peopleAliasIds.Contains(p2t.Person_Id)
                                          && p2t.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select p2t.Person_Id).ToList();

            #region Set start and end dates

            PropertyDateRange dateRange = new PropertyDateRange(null, periodSkip, periodCount);

            if (_trace)
                ADATrace.WriteLine(string.Format("Date Range: StartDate[{0}] : EndDate[{1}]", dateRange.StartDate.ToString(), dateRange.EndDate.ToString()));

            #endregion

            if (peopleNotOnClaim.Count() > 0)
            {

                int keoghsCase2IncidentLinkType = 1;

                if (linkType == "Info Only")
                {
                    keoghsCase2IncidentLinkType = 2;
                }


                List<uspAddressNumberOpenCasesMessages_Result> people = new List<uspAddressNumberOpenCasesMessages_Result>();

                foreach (var p in peopleNotOnClaim)
                {
                    List<uspAddressNumberOpenCasesMessages_Result> ret = _db.uspAddressNumberOpenCasesMessages(p, personAliases.IncludeThis, personAliases.IncludeConfirmed, personAliases.IncludeUnconfirmed, personAliases.IncludeTentative, dateRange.StartDate, dateRange.EndDate, keoghsCase2IncidentLinkType, isPotentialClaimant, stagedContrived).ToList();

                    foreach (var item in ret)
                    {
                        people.Add(item);
                    }

                }

                if (people.Count > 0)
                {
                    return people;
                }


            }

            return new List<uspAddressNumberOpenCasesMessages_Result>();
        }
    }
}
