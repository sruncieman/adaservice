﻿using System.Text;
using System.ServiceProcess;
using System.Threading;
using MDA.Common.Server;
using System;

namespace MDA.Pipeline.WindowsService
{
    /// <summary>
    /// This class implememts the actual WINDOWS SERVICE
    /// </summary>
    public partial class ADAWindowsService : ServiceBase
    {
        public MyServiceHost serviceHost = null;

        private ServiceConfigSettings configSettings = new ServiceConfigSettings();

        /// <summary>
        /// Constructor. Sets the service name and creates an event log
        /// </summary>
        public ADAWindowsService()
        {
            ServiceName = "ADA Service";

            InitializeComponent();
            if (!System.Diagnostics.EventLog.SourceExists("ADA Application"))
            {
                System.Diagnostics.EventLog.CreateEventSource("ADA Application", "ADA Log");
            }
            eventLog1.Source = "ADA Application";
            eventLog1.Log = "ADA Log";
        }

        private StringBuilder _nextEventMessage = new StringBuilder();

        /// <summary>
        /// Write Progress to event log.
        /// </summary>
        /// <param name="severity"></param>
        /// <param name="message"></param>
        /// <param name="eol"></param>
        /// <returns></returns>
        public int ShowProgress(ProgressSeverity severity, string message, bool eol = true)
        {
            _nextEventMessage.Append(message);

            if (eol)
            {
                switch (severity)
                {
                    case ProgressSeverity.Info:
                        eventLog1.WriteEntry(_nextEventMessage.ToString(), System.Diagnostics.EventLogEntryType.Information);
                        break;
                    case ProgressSeverity.Warning:
                        eventLog1.WriteEntry(_nextEventMessage.ToString(), System.Diagnostics.EventLogEntryType.Warning);
                        break;
                    case ProgressSeverity.Error:
                        eventLog1.WriteEntry(_nextEventMessage.ToString(), System.Diagnostics.EventLogEntryType.Error);
                        break;
                    case ProgressSeverity.Fatal:
                        eventLog1.WriteEntry(_nextEventMessage.ToString(), System.Diagnostics.EventLogEntryType.Error);
                        break;
                }

                if(severity == ProgressSeverity.Error || severity == ProgressSeverity.Fatal)
                    MDA.Pipeline.Service.PipelineEngine.SendEmail(severity, _nextEventMessage.ToString());

                _nextEventMessage.Clear();
            }

            return 0;
        }

        

        Thread pipelineManagerThread;
        Thread monitorFolderThread;
        Thread processBatchQueueThread;

        /// <summary>
        /// Callback from system when service starts. Create out custom WCF service host and create necessary threads
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("ADA Application Starting");

            configSettings = new ServiceConfigSettings();

            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            // Create a ServiceHost
            serviceHost = new MyServiceHost(configSettings, typeof(ADAService));

            // Open the ServiceHostBase to create listeners and start listening for messages.
            serviceHost.Open();

            serviceHost.ServiceEnabled = true;

            pipelineManagerThread = new Thread(new ParameterizedThreadStart(PipelineManager));
            pipelineManagerThread.Start(null);

            monitorFolderThread = new Thread(new ParameterizedThreadStart(MonitorFolder));

            processBatchQueueThread = new Thread(new ParameterizedThreadStart(ProcessBatchQueue));
            processBatchQueueThread.Start(null);
        }

        

        /// <summary>
        /// Callback from system when service stops. Close down the custom WCF service host
        /// </summary>
        protected override void OnStop()
        {
            eventLog1.WriteEntry("ADA Application Stopping");

            pipelineManagerThread.Abort();
            monitorFolderThread.Abort();

            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
        }


        /// <summary>
        /// THREAD.  Thread to monitor folders
        /// </summary>
        /// <param name="data"></param>
        private void MonitorFolder(object data)
        {
            int disableMonitoringCount = 0;
            while (true)
            {
                if (configSettings.EnableMonitoring)
                {
                    while (configSettings.EnableMonitoring == true)
                    {
                        if (!MDA.Pipeline.Service.PipelineEngine.PerformFolderMonitoringCheck(configSettings, ShowProgress))
                        {
                            disableMonitoringCount++;
                            ShowProgress(ProgressSeverity.Info, "Checking Monitored Folders FAILED attempt " + disableMonitoringCount, true);

                            Thread.Sleep(configSettings.MonitorErrorRetrySleepTime);
                        }
                        else
                        {
                            // Reset count
                            disableMonitoringCount = 0;
                        }

                        if (disableMonitoringCount == 10)
                        {
                            ShowProgress(ProgressSeverity.Error, "Checking Monitored Folders FAILED after 10 retries. Disabling service thread", true);
                            configSettings.EnableMonitoring = false;
                            return;
                        }
                        
                        Thread.Sleep(configSettings.MonitorSleepTime);
                    }
                }
                
            }
        }

        /// <summary>
        /// Thread to preload batch data and map
        /// </summary>
        /// <param name="data"></param>
        private void ProcessBatchQueue(object data)
        {
            while (true)
            {
                if (configSettings.ProcessBatches)
                {
                    int? batchId = null;

                    try
                    {
                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            batchId = MDA.Pipeline.Pipeline.Stage2_ProcessBatchQueue(ctx, configSettings, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowProgress(ProgressSeverity.Error, ex.Message, true);
                    }
                }
                else
                {
                    ShowProgress(ProgressSeverity.Info, "Batch loading disabled", true);
                    break;
                }

                Thread.Sleep(configSettings.ServiceSleepTime);
            }
        }


        /// <summary>
        /// THREAD.  Main thread that recovers and process the pipeline
        /// </summary>
        /// <param name="data"></param>
        private void PipelineManager(object data)
        {
            while (true)
            {
                if (serviceHost.ServiceEnabled)
                {
                    MDA.Pipeline.Service.PipelineEngine.RecoverPipeline(configSettings, ShowProgress);

                    if (!monitorFolderThread.IsAlive)
                        monitorFolderThread.Start(null);

                    while (true)
                    {
                        if (serviceHost.ServiceEnabled)
                        {
                            configSettings.ConfigChanged = false;

                            MDA.Pipeline.Service.PipelineEngine.RunPipeline(configSettings, ShowProgress);

                            while (MDA.Pipeline.Service.PipelineEngine.NothingToDo(configSettings, ShowProgress) && configSettings.ConfigChanged == false)
                            {
                                Thread.Sleep(configSettings.ServiceSleepTime);
                            }
                            
                        }
                        else
                            Thread.Sleep(configSettings.ServiceSleepTime);
                    }
                }
                else
                    Thread.Sleep(configSettings.ServiceSleepTime);
            }
        }
    }

}
