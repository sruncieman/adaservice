﻿using System.Collections.Generic;
using ADAServices.Interface;
using MDA.RiskService.Model;

namespace MDA.Pipeline.WindowsService
{
    public partial class Translator
    {


        public static RiskClientsResponse Translate(RiskClientsResult res)
        {
            List<ExRiskClient> clientList = new List<ExRiskClient>();

            foreach (var r in res.ClientList)
            {
                clientList.Add(Translate(r));
            }
            return new RiskClientsResponse()
            {
                ClientList = clientList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskWordsResponse Translate(RiskWordsResult res)
        {
            List<ExRiskWord> wordList = new List<ExRiskWord>();

            foreach (var r in res.WordList)
            {
                wordList.Add(Translate(r));
            }
            return new RiskWordsResponse()
            {
                WordList = wordList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskNotesResponse Translate(RiskNotesResult res)
        {
            List<ExRiskNote> noteList = new List<ExRiskNote>();

            foreach (var r in res.NoteList)
            {
                noteList.Add(Translate(r));
            }
            return new RiskNotesResponse()
            {
                NoteList = noteList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static GetRiskNoteResponse Translate(GetRiskNoteResult res)
        {
            return new GetRiskNoteResponse()
            {
                Error = res.Error,
                Result = res.Result,
                RiskNote = Translator.Translate(res.RiskNote)
            };
        }

        public static RiskNoteDecisionsResponse Translate(RiskNoteDecisionsResult res)
        {
            List<ExRiskNoteDecision> decisionList = new List<ExRiskNoteDecision>();

            foreach (var r in res.DecisionList)
            {
                decisionList.Add(Translate(r));
            }
            return new RiskNoteDecisionsResponse()
            {
                DecisionList = decisionList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskWordDeleteResponse Translate(RiskWordDeleteResult res)
        {

            return new RiskWordDeleteResponse()
            {
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskNoteCreateResponse Translate(RiskNoteCreateResult res)
        {

            return new RiskNoteCreateResponse()
            {
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskNoteUpdateResponse Translate(RiskNoteUpdateResult res)
        {

            return new RiskNoteUpdateResponse()
            {
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskDefaultDataResponse Translate(RiskDefaultDataResult res)
        {
            List<ExRiskDefaultData> defaultDataList = new List<ExRiskDefaultData>();

            foreach (var r in res.DefaultDataList)
            {
               defaultDataList.Add(Translate(r));
            }
            return new RiskDefaultDataResponse()
            {
                DefaultDataList = defaultDataList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskRolesResponse Translate(RiskRolesResult res)
        {
            List<ExRiskRole> roleList = new List<ExRiskRole>();

            foreach (var r in res.RoleList)
            {
                roleList.Add(Translate(r));
            }
            return new RiskRolesResponse()
            {
                RoleList = roleList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static InsurersClientsResponse Translate(InsurersClientsResult res)
        {
            List<ExInsurersClients> clientList = new List<ExInsurersClients>();

            foreach (var r in res.ClientList)
            {
                clientList.Add(Translate(r));
            }
            return new InsurersClientsResponse()
            {
                ClientList = clientList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskClientsParam Translate(RiskClientsRequest request)
        {
            return new RiskClientsParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

            };
        }

        public static RiskWordsParam Translate(RiskWordsRequest request)
        {
            return new RiskWordsParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                FilterClientId = request.FilterClientId

            };
        }

        public static RiskNotesParam Translate(RiskNotesRequest request)
        {
            return new RiskNotesParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                FilterRiskCliamId = request.FilterRiskClaimId

            };
        }

        public static GetRiskNoteParam Translate(GetRiskNoteRequest request)
        {
            return new GetRiskNoteParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                FilterRiskNoteId = request.FilterRiskNoteId

            };
        }

        public static RiskNoteDecisionsParam Translate(RiskNoteDecisionsRequest request)
        {
            return new RiskNoteDecisionsParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                FilterRiskClientId = request.FilterRiskClientId

            };
        }

        public static RiskWordDeleteParam Translate(RiskWordDeleteRequest request)
        {
            return new RiskWordDeleteParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskWordId = request.RiskWordId

            };
        }

        public static RiskNoteCreateParam Translate(RiskNoteCreateRequest request)
        {
            return new RiskNoteCreateParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                CreatedBy = request.CreatedBy,
                CreatedDate = request.CreatedDate,
                Decision_Id = request.Decision_Id,
                Deleted = request.Deleted,
                NoteDesc = request.NoteDesc,
                RiskClaim_Id = request.RiskClaim_Id,
                Visibilty = request.Visibilty,
                UserTypeId = request.UserTypeId,
                OriginalFile = request.OriginalFile,
                FileName = request.FileName,
                FileType = request.FileType,


            };
        }

        public static RiskNoteUpdateParam Translate(RiskNoteUpdateRequest request)
        {
            return new RiskNoteUpdateParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                Decision_Id = request.Decision_Id,
                Deleted = request.Deleted,
                NoteDesc = request.NoteDesc,
                Visibilty = request.Visibilty,
                RiskNoteId = request.RiskNoteId,
                UserTypeId = request.UserTypeId,
                UpdatedBy = request.UpdatedBy,
                UpdatedDate = request.UpdatedDate


            };
        }

        public static RiskDefaultDataParam Translate(RiskDefaultDataRequest request)
        {
            return new RiskDefaultDataParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                FilterClientId = request.FilterClientId

            };
        }

        public static RiskRolesParam Translate(RiskRolesRequest request)
        {
            return new RiskRolesParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
            };
        }

        public static InsurersClientsParam Translate(InsurersClientsRequest request)
        {
            return new InsurersClientsParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
            };
        }

        public static GetClaimScoreDataParam Translate(GetClaimScoreDataRequest request)
        {
            return new GetClaimScoreDataParam()
            {
                RiskClaimId = request.RiskClaimId,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static GetClaimScoreDataResponse Translate(GetClaimScoreDataResult result)
        {
            return new GetClaimScoreDataResponse()
            {
                ScoreNodes = result.ScoreNodes,
                Error = result.Error,
                RiskClaimRun = Translator.Translate(result.RiskClaimRun)
            };
        }

        public static BatchListParam Translate(BatchListRequest request)
        {
            return new BatchListParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                StatusFilter = request.StatusFilter
            };
        }

        public static ClaimsInBatchParam Translate(ClaimsInBatchRequest request)
        {
            return new ClaimsInBatchParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                BatchId = request.BatchId
            };
        }

        public static ClaimsInBatchResponse Translate(ClaimsInBatchResult result)
        {
            var res = new ClaimsInBatchResponse();

            res.Error = result.Error;
            res.ClaimsList = new List<ExRiskClaim>();

            foreach (var r in result.ClaimsList)
                res.ClaimsList.Add(Translator.Translate(r));

            return res;
        }

        public static BatchListResponse Translate(BatchListResult result)
        {
            var ret = new BatchListResponse()
            {
                Error = result.Error
            };

            ret.BatchList = new List<ExBatchDetails>();

            foreach (var r in result.BatchList)
                ret.BatchList.Add(Translate(r));

            return ret;
        }

        public static GetBatchProcessingResultsParam Translate(GetBatchProcessingResultsRequest request)
        {
            return new GetBatchProcessingResultsParam()
            {
                RiskBatchId = request.RiskBatchId,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
            };
        }

        public static GetBatchProcessingResultsResponse Translate(GetBatchProcessingResultsResult result)
        {
            return new GetBatchProcessingResultsResponse()
            {
                ProcessingResults = result.ProcessingResults,
                Error = result.Error
            };
        }

        public static ExRiskClient Translate(ERiskClient erc)
        {
            return new ExRiskClient()
            {
                Id = erc.Id,
                ClientName = erc.ClientName,
                AmberThreshold = erc.AmberThreshold,
                RedThreshold = erc.RedThreshold,
                ExpectedFileExtension = erc.ExpectedFileExtension,
                LevelOneReportDelay = erc.LevelOneReportDelay,
                LevelTwoReportDelay = erc.LevelTwoReportDelay
            };
        }

        public static ExRiskWord Translate(ERiskWord rw)
        {
            return new ExRiskWord()
            {
                Id = rw.Id,
                DateAdded = rw.DateAdded,
                FieldName = rw.FieldName,
                LookupWord = rw.LookupWord,
                ReplacementWord = rw.ReplacementWord,
                ReplaceWholeString = rw.ReplaceWholeString,
                RiskClient_Id = rw.RiskClient_Id,
                SearchType = rw.SearchType,
                TableName = rw.TableName,
            };
        }

        public static ExRiskNote Translate(ERiskNote rn)
        {
            return new ExRiskNote()
            {
                Id = rn.Id,
                BaseRiskClaim_Id = rn.BaseRiskClaim_Id,
                CreatedBy = rn.CreatedBy,
                CreatedDate = rn.CreatedDate,
                DecisionId = rn.DecisionId,
                Deleted = rn.Deleted,
                NoteDesc = rn.NoteDesc,
                RiskClaim_Id = rn.RiskClaim_Id,
                Visibilty = rn.Visibilty,
                Decision = rn.Decision,


            };
        }

        public static ExRiskNoteDecision Translate(ERiskNoteDecision rnd)
        {
            return new ExRiskNoteDecision()
            {
                Id = rnd.Id,
                ADARecordStatus = rnd.ADARecordStatus,
                Decision = rnd.Decision,
                RiskClient_Id = rnd.RiskClient_Id

            };
        }

        public static ExRiskDefaultData Translate(ERiskDefaultData rw)
        {
            return new ExRiskDefaultData()
            {
                Id = rw.Id,
                RiskClient_Id = rw.RiskClient_Id,
                ConfigurationValue = rw.ConfigurationValue,
                IsActive = rw.IsActive
            };
        }

        public static ExRiskRole Translate(ERiskRole erc)
        {
            return new ExRiskRole()
            {
                Id = erc.Id,
                RoleName  = erc.RoleName,
            };
        }

        public static ExInsurersClients Translate(EInsurersClient eic)
        {
            return new ExInsurersClients()
            {
                Id = eic.Id,
                ClientName = eic.ClientName,
            };
        }

        public static ExBatchDetails Translate(EBatchDetails result)
        {
            return new ExBatchDetails()
            {
                BatchId             = result.BatchId,
                BatchStatusId       = result.BatchStatusId,
                BatchStatusValue    = result.BatchStatusValue,
                Description         = result.Description,
                Reference           = result.Reference,
                Rejections          = result.Rejections,
                Status              = result.Status,
                UploadedBy          = result.UploadedBy,
                UploadedDate        = result.UploadedDate,
                TotalClaimsReceived = result.TotalClaimsReceived,
                TotalNewClaims      = result.TotalNewClaims,
                TotalModifiedClaims = result.TotalModifiedClaims,
                TotalClaimsLoaded   = result.TotalClaimsLoaded,
                TotalClaimsInError  = result.TotalClaimsInError,
                TotalClaimsSkipped  = result.TotalClaimsSkipped,
                LoadingDuration     = result.LoadingDuration
            };
        }

        public static ExRiskClaim Translate(ERiskClaim rc)
        {
            return new ExRiskClaim()
            {
                Id = rc.Id,
                RiskBatch_Id = rc.RiskBatch_Id,
                Incident_Id = rc.Incident_Id,
                MdaClaimRef = rc.MdaClaimRef,
                BaseRiskClaim_Id = rc.BaseRiskClaim_Id,
                ClaimStatus = rc.ClaimStatus,
                ClaimReadStatus = rc.ClaimReadStatus,
                ClientClaimRefNumber = rc.ClientClaimRefNumber,
                LevelOneRequestedCount = rc.LevelOneRequestedCount,
                LevelTwoRequestedCount = rc.LevelTwoRequestedCount,
                LevelOneRequestedWhen = rc.LevelOneRequestedWhen,
                LevelTwoRequestedWhen = rc.LevelTwoRequestedWhen,
                LevelOneRequestedWho = rc.LevelOneRequestedWho,
                LevelTwoRequestedWho = rc.LevelTwoRequestedWho,
                SourceReference = rc.SourceReference,
                CreatedDate = rc.CreatedDate,
                CreatedBy = rc.CreatedBy,
                TotalScore = rc.TotalScore,
                CleansingResults = rc.CleansingResults,
                ValidationResults = rc.ValidationResults,

            };
        }

        public static ExRiskClaimRun Translate(ERiskClaimRun rcr)
        {
            if (rcr == null) return null;

            return new ExRiskClaimRun()
            {
                Id = rcr.Id,
                RiskClaim_Id = rcr.RiskClaim_Id,
                TotalScore = rcr.TotalScore,
                TotalKeyAttractorCount = rcr.TotalKeyAttractorCount,
                ExecutedBy = rcr.ExecutedBy,
                ExecutedWhen = rcr.ExecutedWhen
            };
        }
    }
}
