﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceProcess;
using System.Configuration;
using System.Configuration.Install;
using ServiceProcess.Helpers;
using System.Threading;

namespace MDA.Pipeline.WindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the WINDOWS SERVICES
        /// </summary>
        static void Main(string[] args)
        {
            ServiceBase[] ServicesToRun;

            ServicesToRun = new ServiceBase[] 
			    { 
				    new ADAWindowsService() 
			    };

            //ServiceBase.Run(ServicesToRun);
            ServicesToRun.LoadServices(); //AND HERE
        }
    }
}
