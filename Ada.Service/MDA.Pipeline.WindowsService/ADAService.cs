﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceProcess;
using System.Configuration;
using System.Configuration.Install;
using ServiceProcess.Helpers;
using System.Threading;
using MDA.Pipeline.Service;
using MDA.Pipeline;
using System.Runtime.Serialization;
using MDA.Common;
using ADAServices.Factory;

namespace MDA.Pipeline.WindowsService
{
    /// <summary>
    /// This class implements the WEB SERVICES for the service
    /// </summary>
    public class ADAService : IADAService
    {
        /// <summary>
        /// Set new config values. If NULL passed return current settings
        /// </summary>
        /// <param name="remoteSettings">New config settings to use. Pass NULL to read current settings</param>
        /// <returns>The current (new) values</returns>
        private RemoteConfigSettings _SetConfigParameters(RemoteConfigSettings remoteSettings)
        {
            var x = OperationContext.Current.Host as MyServiceHost;

            ServiceConfigSettings cfg = x.ServiceSettings;

            //Check if we have been called with NULL. If we have just return the current settings
            if (remoteSettings == null)
            {
                return new RemoteConfigSettings()
                {
                    CreatingThreadLimit = cfg.CreatingThreadLimit,
                    EnableMonitoring    = cfg.EnableMonitoring,
                    EnableBackups       = cfg.EnableBackups,
                    EnableDedupe        = cfg.EnableDedupe,
                    EnableSync          = cfg.EnableSync,
                    ExternalThreadLimit = cfg.ExternalThreadLimit,
                    LoadingThreadLimit  = cfg.LoadingThreadLimit,
                    Process3rdParty     = cfg.Process3rdParty,
                    ProcessBatches      = cfg.ProcessBatches,
                    ProcessLoading      = cfg.ProcessLoading,
                    ProcessScoring      = cfg.ProcessScoring,
                    ReqBatchReports     = cfg.ReqBatchReports,
                    ScoringThreadLimit  = cfg.ScoringThreadLimit,
                    ServiceSleepTime    = cfg.ServiceSleepTime,
                    ThreadSleepTime     = cfg.ThreadSleepTime,
                    MonitorSleepTime    = cfg.MonitorSleepTime,

                    ServiceRunning       = x.ServiceEnabled
                };
            }
            else
            {
                cfg.CreatingThreadLimit = remoteSettings.CreatingThreadLimit;
                cfg.EnableMonitoring    = remoteSettings.EnableMonitoring;
                cfg.EnableBackups       = remoteSettings.EnableBackups;
                cfg.EnableDedupe        = remoteSettings.EnableDedupe;
                cfg.EnableSync          = remoteSettings.EnableSync;
                cfg.ExternalThreadLimit = remoteSettings.ExternalThreadLimit;
                cfg.LoadingThreadLimit  = remoteSettings.LoadingThreadLimit;
                cfg.Process3rdParty     = remoteSettings.Process3rdParty;
                cfg.ProcessBatches      = remoteSettings.ProcessBatches;
                cfg.ProcessLoading      = remoteSettings.ProcessLoading;
                cfg.ProcessScoring      = remoteSettings.ProcessScoring;
                cfg.ReqBatchReports     = remoteSettings.ReqBatchReports;
                cfg.ScoringThreadLimit  = remoteSettings.ScoringThreadLimit;
                cfg.ServiceSleepTime    = remoteSettings.ServiceSleepTime;
                cfg.ThreadSleepTime     = remoteSettings.ThreadSleepTime;
                cfg.MonitorSleepTime    = remoteSettings.MonitorSleepTime;
            }

            return remoteSettings;
        }

        /// <summary>
        /// Set new config values. If NULL passed return current settings
        /// </summary>
        /// <param name="remoteSettings">New config settings to use. Pass NULL to read current settings</param>
        /// <returns>The current (new) values</returns>
        public RemoteConfigSettings SetConfigParameters(RemoteConfigSettings remoteSettings)
        {
            return _SetConfigParameters(remoteSettings);
        }

        /// <summary>
        /// Suspend the service by setting the ServiceEnabled value in the custom host
        /// </summary>
        /// <returns>The current (new) values</returns>
        public RemoteConfigSettings SuspendService()
        {
            var x = OperationContext.Current.Host as MyServiceHost;

            x.ServiceEnabled = false;

            return _SetConfigParameters(null);
        }

        /// <summary>
        /// Start the service by setting the ServiceEnabled value in the custom host
        /// </summary>
        /// <param name="remoteSettings"></param>
        /// <returns>The current (new) values</returns>
        public RemoteConfigSettings StartService(RemoteConfigSettings remoteSettings)
        {
            var x = OperationContext.Current.Host as MyServiceHost;

            x.ServiceEnabled = true;

            return _SetConfigParameters(remoteSettings);
        }

        /// <summary>
        /// Return the current running metrics
        /// </summary>
        /// <returns>The current running metrics</returns>
        public RemoteRunningMetrics GetRunningMetrics()
        {
            var metrics = MDA.Pipeline.Service.PipelineEngine.GetRunningMetrics();

            return new RemoteRunningMetrics()
            {
                AverageLoadingTime            = metrics.AverageLoadingTime,
                AverageScoringTime            = metrics.AverageScoringTime,
                BatchesWaiting                = metrics.BatchesWaiting,
                BatchesError                  = metrics.BatchesError,
                BatchesLoading                = metrics.BatchesLoading,
                BatchesTotal                  = metrics.BatchesTotal,
                ClaimsBeingScored             = metrics.ClaimsBeingScored,
                ExternalErrors                = metrics.ExternalErrors,
                ExternalRetriesRequired       = metrics.ExternalRetriesRequired,
                ClaimsWaitingOnExternal       = metrics.ClaimsWaitingOnExternal,
                ClaimsWaitingToBeLoaded       = metrics.ClaimsWaitingToBeLoaded,
                ClaimsWaitingToBeScored       = metrics.ClaimsWaitingToBeScored
            };
        }

        /// <summary>
        /// Get all the claims in a batch
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ClaimsInBatchResponse GetClaimsInBatch(ClaimsInBatchRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetClaimsInBatch(Translator.Translate(request)));
        }

        /// <summary>
        /// Get the list of batches for a client
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public BatchListResponse GetClientBatchList(BatchListRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetClientBatchList(Translator.Translate(request)));
        }

        /// <summary>
        /// Get the score data for a particular claim
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetClaimScoreDataResponse GetClaimScoreData(GetClaimScoreDataRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetClaimScoreData(Translator.Translate(request)));
        }

        /// <summary>
        /// Get a list of clients
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RiskClientsResponse GetRiskClients(RiskClientsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskClients(Translator.Translate(request)));
        }

        /// <summary>
        /// Get a list of risk words
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RiskWordsResponse GetRiskWords(RiskWordsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskWords(Translator.Translate(request)));
        }

        /// <summary>
        /// Get a list of risk notes
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RiskNotesResponse GetRiskNotes(RiskNotesRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskNotes(Translator.Translate(request)));
        }

        /// <summary>
        /// Get a risk note
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetRiskNoteResponse GetRiskNote(GetRiskNoteRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskNote(Translator.Translate(request)));
        }

        /// <summary>
        /// Get a list of risk notes decisions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RiskNoteDecisionsResponse GetRiskNoteDecisions(RiskNoteDecisionsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskNoteDecisions(Translator.Translate(request)));
        }

        /// <summary>
        /// Delete risk word
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RiskWordDeleteResponse DeleteRiskWord(RiskWordDeleteRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().DeleteRiskWord(Translator.Translate(request)));
        }

        /// <summary>
        /// Create risk note
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RiskNoteCreateResponse CreateRiskNote(RiskNoteCreateRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().CreateRiskNote(Translator.Translate(request)));
        }

        /// <summary>
        /// Update risk note
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RiskNoteUpdateResponse UpdateRiskNote(RiskNoteUpdateRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().UpdateRiskNote(Translator.Translate(request)));
        }

        ///// <summary>
        ///// Get a list of risk default data
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //public RiskDefaultDataResponse GetRiskDefaultData(RiskDefaultDataRequest request)
        //{
        //    return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskDefaultData(Translator.Translate(request)));
        //}


        /// <summary>
        /// Get a list of roles
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RiskRolesResponse GetRiskRoles(RiskRolesRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskRoles(Translator.Translate(request)));
        }

        /// <summary>
        /// Get a list of InsurersClients
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public InsurersClientsResponse GetInsurersClients(InsurersClientsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetInsurersClients(Translator.Translate(request)));
        }

        /// <summary>
        /// Get the processing results for a given batch
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetBatchProcessingResultsResponse GetBatchProcessingResults(GetBatchProcessingResultsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetBatchProcessingResults(Translator.Translate(request)));
        }
    }

    
}
