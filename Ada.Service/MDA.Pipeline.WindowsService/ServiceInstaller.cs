﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration.Install;
using System.ServiceProcess;
using System.ComponentModel;

namespace MDA.Pipeline.WindowsService
{
    /// <summary>
    /// Class which registers the WINDOWS SERVICE with Windows.
    /// </summary>
    [RunInstaller(true)]
    public class ADAServiceInstaller : Installer
    {
        public ADAServiceInstaller()
        {
            ServiceProcessInstaller spi = new ServiceProcessInstaller();
            ServiceInstaller si = new ServiceInstaller();

            spi.Account = ServiceAccount.LocalSystem;
            spi.Username = null;
            spi.Password = null;

            si.DisplayName = "ADA Service";
            si.StartType = ServiceStartMode.Automatic;
            si.ServiceName = "ADA Service";

            this.Installers.Add(spi);
            this.Installers.Add(si);
        }
    }
}
