﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MDA.Common;

namespace MDA.Pipeline.WindowsService
{
    [DataContract]
    public class RiskClientsResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskClient> ClientList { get; set; }
    }

    [DataContract]
    public class RiskWordsResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskWord> WordList { get; set; }
    }

    [DataContract]
    public class RiskNotesResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskNote> NoteList { get; set; }
    }

    [DataContract]
    public class GetRiskNoteResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public ExRiskNote RiskNote { get; set; }
    }

    [DataContract]
    public class RiskNoteDecisionsResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskNoteDecision> DecisionList { get; set; }
    }


    [DataContract]
    public class RiskWordDeleteResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class RiskNoteCreateResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class RiskNoteUpdateResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class RiskDefaultDataResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskDefaultData> DefaultDataList { get; set; }
    }

    [DataContract]
    public class RiskClientsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterClientId { get; set; }

    }

    [DataContract]
    public class RiskWordsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterClientId { get; set; }

    }

    [DataContract]
    public class RiskNotesRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterRiskClaimId { get; set; }

    }

    [DataContract]
    public class GetRiskNoteRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterRiskNoteId { get; set; }

    }


    [DataContract]
    public class RiskNoteDecisionsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterRiskClientId { get; set; }

    }


    [DataContract]
    public class RiskWordDeleteRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskWordId { get; set; }

    }

    [DataContract]
    public class RiskNoteCreateRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string NoteDesc { get; set; }

        [DataMember]
        public int BaseRiskClaim_Id { get; set; }

        [DataMember]
        public int[] RiskClaim_Id { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public int Decision_Id { get; set; }

        [DataMember]
        public int Visibilty { get; set; }

        [DataMember]
        public bool Deleted { get; set; }

        [DataMember]
        public int? UserTypeId { get; set; }

        [DataMember]
        public byte[] OriginalFile { get; set; }

        [DataMember]
        public string FileType { get; set; }

        [DataMember]
        public string FileName { get; set; }

    }

    [DataContract]
    public class RiskNoteUpdateRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskNoteId { get; set; }

        [DataMember]
        public string NoteDesc { get; set; }

        [DataMember]
        public int? Decision_Id { get; set; }

        [DataMember]
        public byte Visibilty { get; set; }

        [DataMember]
        public bool Deleted { get; set; }

        [DataMember]
        public int? UserTypeId { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }

        [DataMember]
        public string UpdatedBy { get; set; }

    }

    [DataContract]
    public class RiskDefaultDataRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterClientId { get; set; }

    }

    [DataContract]
    public class RiskRolesResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskRole> RoleList { get; set; }
    }

    [DataContract]
    public class InsurersClientsResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExInsurersClients> ClientList { get; set; }
    }

    [DataContract]
    public class RiskRolesRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

    }

    [DataContract]
    public class InsurersClientsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

    }

    [DataContract]
    public class RemoteConfigSettings
    {
        [DataMember]
        public int ScoringThreadLimit { get; set; }
        [DataMember]
        public int ExternalThreadLimit { get; set; }
        [DataMember]
        public int LoadingThreadLimit { get; set; }
        [DataMember]
        public int CreatingThreadLimit { get; set; }

        [DataMember]
        public bool EnableMonitoring { get; set; }
        [DataMember]
        public bool EnableDedupe { get; set; }
        [DataMember]
        public bool EnableSync { get; set; }
        [DataMember]
        public bool EnableBackups { get; set; }
        [DataMember]
        public bool ReqBatchReports { get; set; }

        [DataMember]
        public bool ProcessBatches { get; set; }
        [DataMember]
        public bool ProcessLoading { get; set; }
        [DataMember]
        public bool Process3rdParty { get; set; }
        [DataMember]
        public bool ProcessScoring { get; set; }

        [DataMember]
        public int ServiceSleepTime { get; set; }
        [DataMember]
        public int ThreadSleepTime { get; set; }
        [DataMember]
        public int MonitorSleepTime { get; set; }

        [DataMember]
        public bool ServiceRunning { get; set; }
    }

    [DataContract]
    public class RemoteRunningMetrics
    {
        [DataMember]
        public int BatchesWaiting { get; set; }
        [DataMember]
        public int BatchesLoading { get; set; }
        [DataMember]
        public int BatchesTotal { get; set; }
        [DataMember]
        public int BatchesError { get; set; }
        [DataMember]
        public int ExternalErrors { get; set; }
        [DataMember]
        public int ExternalRetriesRequired { get; set; }

        [DataMember]
        public int ClaimsWaitingToBeLoaded { get; set; }

        [DataMember]
        public int ClaimsBeingScored { get; set; }
        [DataMember]
        public int ClaimsWaitingOnExternal { get; set; }

        [DataMember]
        public int ClaimsWaitingToBeScored { get; set; }

        [DataMember]
        public int AverageLoadingTime { get; set; }

        [DataMember]
        public int AverageScoringTime { get; set; }
    }

    [DataContract]
    public class ExRiskClient
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public int AmberThreshold { get; set; }

        [DataMember]
        public int RedThreshold { get; set; }

        [DataMember]
        public int LevelOneReportDelay { get; set; }

        [DataMember]
        public int LevelTwoReportDelay { get; set; }

        [DataMember]
        public string ExpectedFileExtension { get; set; }
    }

    [DataContract]
    public class ExRiskWord
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public string TableName { get; set; }

        [DataMember]
        public string FieldName { get; set; }

        [DataMember]
        public string LookupWord { get; set; }

        [DataMember]
        public string ReplacementWord { get; set; }

        [DataMember]
        public bool ReplaceWholeString { get; set; }

        [DataMember]
        public System.DateTime DateAdded { get; set; }

        [DataMember]
        public string SearchType { get; set; }

    }

    [DataContract]
    public class ExRiskNote
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int BaseRiskClaim_Id { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public int? DecisionId { get; set; }

        [DataMember]
        public string Decision { get; set; }

        [DataMember]
        public bool Deleted { get; set; }

        [DataMember]
        public string NoteDesc { get; set; }

        [DataMember]
        public int RiskClaim_Id { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int? Visibilty { get; set; }

    }

    [DataContract]
    public class ExRiskNoteDecision
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Decision { get; set; }

        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public int ADARecordStatus { get; set; }

    }

    [DataContract]
    public class ExRiskDefaultData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public string ConfigurationValue { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

    }

    [DataContract]
    public class ExRiskRole
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string RoleName { get; set; }

    }

    [DataContract]
    public class ExInsurersClients
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ClientName { get; set; }

    }

    [DataContract]
    public class ExRiskClaim
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RiskBatch_Id { get; set; }

        [DataMember]
        public int Incident_Id { get; set; }

        [DataMember]
        public string MdaClaimRef { get; set; }

        [DataMember]
        public int BaseRiskClaim_Id { get; set; }

        [DataMember]
        public int ClaimStatus { get; set; }

        [DataMember]
        public int ClaimReadStatus { get; set; }

        [DataMember]
        public string ClientClaimRefNumber { get; set; }

        [DataMember]
        public int LevelOneRequestedCount { get; set; }

        [DataMember]
        public int LevelTwoRequestedCount { get; set; }

        [DataMember]
        public Nullable<System.DateTime> LevelOneRequestedWhen { get; set; }

        [DataMember]
        public Nullable<System.DateTime> LevelTwoRequestedWhen { get; set; }

        [DataMember]
        public string LevelOneRequestedWho { get; set; }

        [DataMember]
        public string LevelTwoRequestedWho { get; set; }

        [DataMember]
        public string SourceReference { get; set; }

        //public string SourceEntityXML { get; set; }
        [DataMember]
        public System.DateTime CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public int TotalScore { get; set; }

        [DataMember]
        public MessageNode ValidationResults { get; set; }

        [DataMember]
        public MessageNode CleansingResults { get; set; }
    }

    [DataContract]
    public class ExEntityScoreMessageList
    {
        [DataMember]
        public string EntityHeader { get; set; }

        [DataMember]
        public List<string> Messages { get; set; }
    }

    [DataContract]
    public class ExRiskClaimRun
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RiskClaim_Id { get; set; }

        [DataMember]
        public string EntityType { get; set; }

        //public string ScoreEntityJson { get; set; }
        //public string ScoreEntityXml { get; set; }
        [DataMember]
        public int TotalScore { get; set; }

        [DataMember]
        public int TotalKeyAttractorCount { get; set; }

        //public string ReportOneVersion { get; set; }
        //public string ReportTwoVersion { get; set; }
        [DataMember]
        public string ExecutedBy { get; set; }

        [DataMember]
        public System.DateTime ExecutedWhen { get; set; }

        [DataMember]
        public List<ExEntityScoreMessageList> MessageList { get; set; }
    }

    [DataContract]
    public class ExBatchDetails
    {

        [DataMember]
        public int BatchId { get; set; }

        [DataMember]
        public string Reference { get; set; }

        [DataMember]
        public System.DateTime UploadedDate { get; set; }

        [DataMember]
        public string UploadedBy { get; set; }

        [DataMember]
        public int BatchStatusId { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string BatchStatusValue { get; set; }

        [DataMember]
        public string Description { get; set; }

        // | delimited string
        [DataMember]
        public string Rejections { get; set; }

        [DataMember]
        public int TotalClaimsReceived { get; set; }
        [DataMember]
        public int TotalNewClaims { get; set; }
        [DataMember]
        public int TotalModifiedClaims { get; set; }
        [DataMember]
        public int TotalClaimsLoaded { get; set; }
        [DataMember]
        public int TotalClaimsInError { get; set; }
        [DataMember]
        public int TotalClaimsSkipped { get; set; }
        [DataMember]
        public int LoadingDuration { get; set; }

    }

    [DataContract]
    public class BatchListRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int StatusFilter { get; set; }
    }

    [DataContract]
    public class BatchListResponse
    {
        [DataMember]
        public List<ExBatchDetails> BatchList { get; set; }

        [DataMember]
        public string Error { get; set; }
    }


    [DataContract]
    public class ClaimsInBatchRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int BatchId { get; set; }
    }

    [DataContract]
    public class ClaimsInBatchResponse
    {
        [DataMember]
        public List<ExRiskClaim> ClaimsList { get; set; }

        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class GetClaimScoreDataResponse
    {
        [DataMember]
        public ExRiskClaimRun RiskClaimRun { get; set; }

        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public MessageNode ScoreNodes { get; set; }
    }

    [DataContract]
    public class GetClaimScoreDataRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }


    }

    [DataContract]
    public class GetBatchProcessingResultsResponse
    {
        [DataMember]
        public MessageNode ProcessingResults { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class GetBatchProcessingResultsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskBatchId { get; set; }

    }

}
