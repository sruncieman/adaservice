﻿using System.ServiceModel;

namespace MDA.Pipeline.WindowsService
{
    /// <summary>
    /// This interface provided by the WEB SERVICES of the windows service
    /// </summary>
    [ServiceContract(Namespace = "http://MDA.Keoghs.co.uk")]
    public interface IADAService
    {
        [OperationContract]
        RemoteConfigSettings SetConfigParameters(RemoteConfigSettings settings);

        [OperationContract]
        RemoteRunningMetrics GetRunningMetrics();

        [OperationContract]
        RemoteConfigSettings SuspendService();

        [OperationContract]
        RemoteConfigSettings StartService(RemoteConfigSettings settings);

        [OperationContract]
        ClaimsInBatchResponse GetClaimsInBatch(ClaimsInBatchRequest request);

        [OperationContract]
        BatchListResponse GetClientBatchList(BatchListRequest batchListRequest); 

        [OperationContract]
        GetClaimScoreDataResponse GetClaimScoreData(GetClaimScoreDataRequest request);

        [OperationContract]
        GetBatchProcessingResultsResponse GetBatchProcessingResults(GetBatchProcessingResultsRequest request);

        [OperationContract]
        RiskClientsResponse GetRiskClients(RiskClientsRequest request);
    }

}
