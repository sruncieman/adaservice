﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MDA.Pipeline.WindowsService
{
    /// <summary>
    /// Class to HOST the WEB SERVICES. A custom host is being used so that we can embed our config settings and enable us to
    /// turn the service on and off
    /// </summary>
    public class MyServiceHost : ServiceHost
    {
        /// <summary>
        /// A classs to hold the config settings we read from the config file and then tweak through the web service
        /// </summary>
        public ServiceConfigSettings ServiceSettings { get; set; }

        private bool _serviceEnabled;

        /// <summary>
        /// A property to hold whether the service is currently enabled and running
        /// </summary>
        public bool ServiceEnabled
        {
            get
            {
                return _serviceEnabled;
            }

            set
            {
                if (value != _serviceEnabled)
                {
                    _serviceEnabled = value;
                    ServiceSettings.ConfigChanged = true;
                }
            }
        }

        /// <summary>
        /// Constructor which allows config to be passed in and defaults the service to disabled
        /// </summary>
        /// <param name="config"></param>
        /// <param name="t"></param>
        /// <param name="x"></param>
        public MyServiceHost(ServiceConfigSettings config, Type t, params Uri[] x)
            : base(t, x)
        {
            ServiceSettings = config;
            _serviceEnabled = false;
        }
    }
}
