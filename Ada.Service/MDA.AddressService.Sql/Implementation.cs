﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using MDA.Common.Debug;
using MDA.AddressService.Model;
using MDA.AddressService.Interface;
using MDA.Common.Server;

namespace MDA.AddressService.KeoghsPAF
{
    public class AddressService : IAddressService
    {
        private bool _trace;

        public AddressService(bool trace)
        {
            _trace = trace;
        }

        public Address CleanseAddress(Address address)
        {
            var cleansedAddress = new Address();

            if (_trace)
            {
                ADATrace.WriteLine(string.Format("Address Before: SBN[{0}], BLD[{1}], BNM[{2}], STR[{3}], LOC[{4}], TWN[{5}], PC[{6}]", address.SubBuilding, address.Building,
                    address.BuildingNumber, address.Street, address.Locality, address.Town, address.PostCode));
            }


            string connectionString = GetNewConnection();
            SqlDataReader dr = null;

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand("usp_GetAddress", conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@SubBuilding", SqlDbType.VarChar).Value    = address.SubBuilding ?? (object) DBNull.Value;
                        cmd.Parameters.AddWithValue("@Building", SqlDbType.VarChar).Value       = address.Building ?? (object)DBNull.Value;
                        cmd.Parameters.AddWithValue("@BuildingNumber", SqlDbType.VarChar).Value = address.BuildingNumber ?? (object)DBNull.Value;
                        cmd.Parameters.AddWithValue("@Street", SqlDbType.VarChar).Value         = address.Street ?? (object)DBNull.Value;
                        cmd.Parameters.AddWithValue("@Locality", SqlDbType.VarChar).Value       = address.Locality ?? (object)DBNull.Value;
                        cmd.Parameters.AddWithValue("@Town", SqlDbType.VarChar).Value           = address.Town ?? (object)DBNull.Value;
                        cmd.Parameters.AddWithValue("@PostCode", SqlDbType.VarChar).Value       = address.PostCode ?? (object)DBNull.Value;

                        dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        int rowCount = 0;
                        
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                //int pafValidated = Convert.ToInt32(dr[7]);

                                if (rowCount > 0)
                                        return address;
                                    
                                cleansedAddress.SubBuilding    = dr[0].ToString().Trim();
                                cleansedAddress.BuildingNumber = dr[1].ToString().Trim();
                                cleansedAddress.Building       = dr[2].ToString().Trim();
                                cleansedAddress.Street         = dr[3].ToString().Trim();
                                cleansedAddress.Locality       = dr[4].ToString().Trim();
                                cleansedAddress.Town           = dr[5].ToString().Trim();
                                cleansedAddress.PostCode       = dr[6].ToString().Trim();
                                //cleansedAddress.PAFValidation  = (bool?)dr[7];
                                cleansedAddress.PafUPRN        = dr[9].ToString().Trim();

                                if (dr[7] == DBNull.Value)
                                    cleansedAddress.PafValidation = 0;
                                else
                                    cleansedAddress.PafValidation = (int)dr[7];
                                    //cleansedAddress.PafValidation = ((bool)dr[7]) ? 1 : 0;

                                //if (string.IsNullOrEmpty(cleansedAddress.PAFUPRN))
                                //    cleansedAddress.PAFValidation = 0;

                                rowCount++;
                            }

                            if (_trace)
                            {
                                ADATrace.WriteLine(string.Format("After Cleanse : SBN[{0}], BLD[{1}], BNM[{2}], STR[{3}], LOC[{4}], TWN[{5}], PC[{6}], PafValid[{7}], UPRN[{8}]",
                                    cleansedAddress.SubBuilding, cleansedAddress.Building,
                                    cleansedAddress.BuildingNumber, cleansedAddress.Street, cleansedAddress.Locality,
                                    cleansedAddress.Town, cleansedAddress.PostCode, cleansedAddress.PafValidation, cleansedAddress.PafUPRN));
                            }

                            if (cleansedAddress.PafValidation != 0 && cleansedAddress.PafUPRN != null)
                                return cleansedAddress;
                            
                            return address;
                        }

                        else
                        {
                            if (_trace)
                            {
                                ADATrace.WriteLine(string.Format("After NOCLEAN : SBN[{0}], BLD[{1}], BNM[{2}], STR[{3}], LOC[{4}], TWN[{5}], PC[{6}]", address.SubBuilding, address.Building,
                                    address.BuildingNumber, address.Street, address.Locality, address.Town, address.PostCode));
                            }

                            address.PafValidation = 1;

                            return address;
                        }
                    }

                    catch (Exception ex)
                    {
                        if (_trace)
                        {
                            ADATrace.WriteLine(string.Format("ERROR NOCLEAN : SBN[{0}], BLD[{1}], BNM[{2}], STR[{3}], LOC[{4}], TWN[{5}], PC[{6}]", address.SubBuilding, address.Building,
                                address.BuildingNumber, address.Street, address.Locality, address.Town, address.PostCode));
                            ADATrace.WriteLine("EXCEPTION: " + ex.Message);
                        }

                        address.PafValidation = 0;


                        return address;
                    }

                    finally
                    {
                        if (dr != null)
                            dr.Close();

                        if (conn.State == ConnectionState.Open)
                            conn.Close();
                    }
                }
            }
        }

        private static string GetNewConnection()
        {
            return ConfigurationManager.ConnectionStrings["PAFDbContext"].ConnectionString;
        }
    }
}
