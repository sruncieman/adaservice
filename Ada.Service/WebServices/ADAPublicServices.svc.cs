﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using MDA.WCF.WebServices.Messages;
using ADA.Public.WebServices.WebServiceReference;

namespace ADA.Public.WebServices
{
    public class ADAPublicServices : IADAPublicServices
    {
        public ProcessClaimBatchResponse ProcessMotorClaimBatch(ProcessClaimBatchRequest request)
        {
           ADAWebServicesClient proxy = new ADAWebServicesClient();

           return proxy.ProcessClaimBatch(request);
        }

        public ProcessClaimResponse ProcessMotorClaim(ProcessClaimRequest request)
        {
            ADAWebServicesClient proxy = new ADAWebServicesClient();

            return proxy.ProcessClaim(request);
        }
    }
}
