﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using MDA.WCF.WebServices.Messages;
using ADA.Public.WebServices.WebServiceReference;

namespace ADA.Public.WebServices
{
    [ServiceContract]
    public interface IADAPublicServices
    {
        [OperationContract]
        ProcessClaimBatchResponse ProcessMotorClaimBatch(ProcessClaimBatchRequest request);

        [OperationContract]
        ProcessClaimResponse ProcessMotorClaim(ProcessClaimRequest request);
    }
}
