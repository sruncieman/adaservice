﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.Mapping.Eldon;
using MDA.MappingService.Eldon.Motor;
using MDA.Common.Server;
using MDA.Common;
using System.Xml.Serialization;
using Newtonsoft.Json;


namespace MDA.Mapping.Eldon
{
    class Program
    {
        static PipelineClaimBatch motorClaimBatch = new PipelineClaimBatch();
       
        private static int ProcessClaimIntoDb(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            motorClaimBatch.Claims.Add(claim);

            return 0;
        }

        private static void Main(string[] args)
        {
            string path = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Eldon\Resource\";

            CurrentContext ctx = new CurrentContext(0, 1, "Test");


            System.IO.FileStream strFile = new System.IO.FileStream(path + @"\Eldon Testing - 5 claims.txt", System.IO.FileMode.Open, System.IO.FileAccess.Read);

            Console.WriteLine("Start");

            ProcessingResults pr;

            new EldonFileMapping().ConvertToClaimBatch(ctx, strFile, null, ProcessClaimIntoDb, out pr);
            

            string xml = WriteToXml();

            WriteToJson(xml);

            WriteToXmlOriginal(motorClaimBatch);

        }

        private static void WriteToXmlOriginal(MDA.Pipeline.Model.PipelineClaimBatch batch)
        {
            string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Eldon\XmlOriginal\EldonFull.xml";
            var ser = new DataContractSerializer(batch.GetType());

            FileStream fs = new FileStream(strXmlFileFullSave, FileMode.Create);
            ser.WriteObject(fs, batch);

            fs.Close();
        }

        private static string WriteToXml()
        {
            string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Eldon\Xml\EldonFull.xml";

            string xml = null;

            var ms = new MemoryStream();

            var ser = new XmlSerializer(typeof(MDA.Pipeline.Model.PipelineClaimBatch));

            ser.Serialize(ms, motorClaimBatch);

            ms.Position = 0;

            var sr = new StreamReader(ms);

            xml = sr.ReadToEnd();

            TextWriter WriteFileStream = new StreamWriter(strXmlFileFullSave);
            ser.Serialize(WriteFileStream, motorClaimBatch);

            WriteFileStream.Close();
            return xml;
        }

        private static void WriteToJson(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            string json = JsonConvert.SerializeXmlNode(doc);

            System.IO.File.WriteAllText(@"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Eldon\Json\EldonFull.txt", json);
        }
    }
}


