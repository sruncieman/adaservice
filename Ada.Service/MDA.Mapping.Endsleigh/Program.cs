﻿using System;
using System.IO;
using System.Runtime.Serialization;
using MDA.Pipeline.Model;
using MDA.MappingService.Endsleigh.Motor;
using MDA.Common;
using MDA.Common.Server;
using System.Xml.Serialization;
using System.Xml;
using System.Collections.Generic;

namespace MDA.Mapping.Endsleigh
{

    class Program
    {
        static List<IPipelineClaim> motorClaimBatch = new List<IPipelineClaim>();

        private static int ProcessClaimIntoDb(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            motorClaimBatch.Add(claim);

            return 0;
        }

        static void Main(string[] args)
        {
            GetMotorClaimBatch claimBatch = new GetMotorClaimBatch();

            Stream mockStream = null;

            CurrentContext ctx = new CurrentContext(0, 1, "Test");

            ProcessingResults pr;

            new FileMapping().ConvertToClaimBatch(ctx, mockStream, null, ProcessClaimIntoDb, out pr);

            string xml = WriteToXml();
        }

        private static string WriteToXml()
        {
            string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Endsleigh\Xml\";
            string xml = null;

            foreach (var item in motorClaimBatch)
            {

                var ms = new MemoryStream();

                var ser = new XmlSerializer(typeof(MDA.Pipeline.Model.PipelineMotorClaim));

                ser.Serialize(ms, item);

                ms.Position = 0;

                var sr = new StreamReader(ms);

                xml = sr.ReadToEnd();

                TextWriter WriteFileStream = new StreamWriter(strXmlFileFullSave + item.ClaimNumber + ".xml");
                ser.Serialize(WriteFileStream, item);

                WriteFileStream.Close();

            }

            return xml;

        }
    }
}
