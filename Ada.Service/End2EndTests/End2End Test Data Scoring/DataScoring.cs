﻿using System;
using System.Linq;
using System.Data.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.WCF.WebServices;
using MDA.Common.FileModel;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Diagnostics;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.DataService;
using RiskEngine.Scoring.Entities;
using MDA.Common.Server;
using RiskEngine.Scoring.Model;

namespace End2EndTests
{
    public class Results
    {

        public static void Score(int riskClaimId, int score)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {
                int totalScore = (from x in ctx.db.RiskClaimRuns
                                  where x.RiskClaim_Id == riskClaimId
                                  select x.TotalScore).FirstOrDefault();

                Assert.AreEqual<int>(score, totalScore);
            }
        }


        public static void SettledCaseCount(int riskClaimId, int personId, int settledCaseCount, int settlementCategory, string matchType)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                    //{
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            //Assert.AreEqual<int>(settledCaseCount, person.v_SettledCaseCount("key", matchType, settlementCategory, null, 0, 84));
                        }
                    }
                    //}
                }
            }
        }

        public static void NumberOfCases(int riskClaimId, int personId, int numberOfCases, string matchType)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                    //{
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            Assert.AreEqual<int>(numberOfCases, person.v_NumberOfCases("key", matchType, "", true, "0m", "84m"));
                        }
                    }
                    //}
                }
            }
        }


        public static void MobileNumberOfCases(int riskClaimId, int personId, int numberOfCases, string matchType, string linkType)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                    //{
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            Assert.AreEqual<int>(numberOfCases, person.v_TelephoneNumberOfOpenCases("key", matchType, linkType, null, null, "0m", "84m"));
                        }
                    }
                    //}
                }
            }
        }

        public static void LinkedAddressCaseCount(int riskClaimId, int personId, string matchType, int numberOfCases, bool? isPotentialClaimant, string linkType, string incidentType, string claimType, string periodSkip, string periodCount)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            Assert.AreEqual<int>(numberOfCases, person.v_LinkedAddressCaseCount("key", matchType, isPotentialClaimant, linkType, incidentType, claimType, periodSkip, periodCount));
                        }
                    }
                }
            }
        }

        public static void AddressNumberOfCases(int riskClaimId, int numberOfCases, bool? isPotentialClaimant, string linkType, string incidentType, string claimType, string periodSkip, string periodCount)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {

                    foreach (var person in vehicle.VehiclePeople)
                    {

                        foreach (var address in person.PersonsAddresses)
                        {
                            address.ctx = ctx;
                            Assert.AreEqual<int>(numberOfCases, address.v_NumberOfCases("key", isPotentialClaimant, linkType, incidentType, claimType, periodSkip, periodCount));
                        }

                        foreach (var address in person.PersonsUnconfirmedAddresses)
                        {
                            address.ctx = ctx;
                            Assert.AreEqual<int>(numberOfCases, address.v_NumberOfCases("key", isPotentialClaimant, linkType, incidentType, claimType, periodSkip, periodCount));
                        }

                    }

                }
            }
        }

        public static void NumberOfIncidents(int riskClaimId, int personId, int numberOfIncidents, string matchType, string dateRange, string searchDate)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                    //{
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            Assert.AreEqual<int>(numberOfIncidents, person.v_NumberOfIncidents("key", matchType, "", "", true, "0m", dateRange)); //, searchDate));
                        }
                    }
                    //}
                }
            }
        }

        //public static void AddressNumberOfIncidents(int riskClaimId, int personId, int numberOfIncidents, string matchType, string dateRange)
        //{
        //    using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
        //    {

        //        FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchClaimForScoring(riskClaimId);

        //        var incident = x.incident;

        //        foreach (var vehicle in incident.IncidentVehicles)
        //        {
        //            //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
        //            //{
        //                foreach (var person in vehicle.VehiclePeople)
        //                {
        //                    if (person.Db_Id == personId)
        //                    {
        //                        foreach (var address in person.PersonsAddresses)
        //                        {
        //                            address.ctx = ctx;
        //                            Assert.AreEqual<int>(numberOfIncidents, address.v_NumberOfCases("key",true,"8","19","6m", "6m"));
        //                        }
        //                    }

        //                }
        //            //}
        //        }
        //    }
        //}

        public static void LinkedSettledCaseCount(int riskClaimId, int personId, int linkedSettledCaseCount, int settlementCategory, string matchType, int claimType)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                    //{
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            Assert.AreEqual<int>(linkedSettledCaseCount, person.v_LinkedSettledCaseCount("key", matchType, settlementCategory, claimType, true, "0m", "84m"));
                        }
                    }
                    //}
                }
            }
        }

        public static void AnyVehicleWithMultipleOccupancy(int riskClaimId, int limit, bool result)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                //{
                incident.ctx = ctx;
                Assert.AreEqual<bool>(result, incident.v_AnyVehiclesWithMultipleOccupancy("key", limit));
                //}
            }
        }

        public static void EmailUsageCount(int riskClaimId, int personId, string matchType, int count)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                    //{
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            Assert.AreEqual<int>(count, person.v_EmailUsageCount("key", matchType));
                        }
                    }
                    //}
                }
            }
        }

        public static void TelephoneNumberUsageCount(int riskClaimId, int personId, int count, bool sameAddress)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                    //{
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            Assert.AreEqual<int>(count, person.v_TelephoneNumberUsageCount("key", "MOBILE|LANDLINE", sameAddress));
                        }
                    }
                    //}
                }
            }
        }

        public static void BankAccountNumberUsageCount(int riskClaimId, int personId, string matchType, int count)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                    //{
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            Assert.AreEqual<int>(count, person.v_BankAccountUsageCount("key", matchType));
                        }
                    }
                    //}
                }
            }
        }

        public static void IsPersonKeyAttractor(int riskClaimId, int personId, bool keyAttractor, string matchType)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var vehicle in incident.IncidentVehicles)
                {
                    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                    //{
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        person.ctx = ctx;
                        if (person.Db_Id == personId)
                        {
                            person.SetRuleName("KAPersonConf");
                            Assert.AreEqual<bool?>(keyAttractor, person.v_IsKeyAttractor("key", matchType));
                        }
                    }
                    //}
                }
            }
        }

        //public static void IsTelephoneKeyAttractor(int riskClaimId, int personId, bool keyAttractor)
        //{
        //    FullClaimToScore x = new MDA.RiskService.DataService.RiskDataServices().FetchClaimForScoring(riskClaimId);

        //    var incident = x.incident;

        //    foreach (var vehicle in incident.IncidentVehicles)
        //    {
        //        using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
        //        {
        //            foreach (var person in vehicle.VehiclePeople)
        //            {
        //                person.db = db;
        //                if (person.Db_Id == personId)
        //                {
        //                    Assert.AreEqual<bool>(keyAttractor, person.v_IsTelephoneKeyAttractor("key"));
        //                }
        //            }
        //        }
        //    }
        //}

        public static void IsOrganisationKeyAttractor(int riskClaimId, int organisationId, bool keyAttractor, string matchType)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {
                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                foreach (var organisation in incident.IncidentOrganisations)
                {
                    organisation.ctx = ctx;
                    if (organisation.Db_Id == organisationId)
                    {
                        Assert.AreEqual<bool?>(keyAttractor, organisation.v_IsKeyAttractor("key", matchType));
                    }
                }
            }
        }

        public static void NumberOfDaysSincePolicyInception(int riskClaimId, int NumberOfDays)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                //{
                incident.ctx = ctx;
                Assert.AreEqual<int?>(NumberOfDays, incident.v_NumberOfDaysSincePolicyInception);
                //}
            }
        }

        public static void NumberOfDaysSincePolicyEnd(int riskClaimId, int numberOfDays)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                //{
                incident.ctx = ctx;
                Assert.AreEqual<int?>(numberOfDays, incident.v_NumberOfDaysSincePolicyEnd);
                //}
            }
        }

        public static void OccupancyCount(int riskClaimId, int vehicleId, int count)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                // {
                foreach (var vehicle in incident.IncidentVehicles)
                {
                    vehicle.ctx = ctx;
                    if (vehicle.Db_Id == vehicleId)
                    {
                        vehicle.Locked = false;
                        Assert.AreEqual<int?>(count, vehicle.v_OccupancyCount);
                    }
                }

                // }
            }
        }

        public static void VehicleNumberOfIncidents(int riskClaimId, int vehicleId, int count, string matchType, string vehicleLinkType)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                //{
                foreach (var vehicle in incident.IncidentVehicles)
                {
                    vehicle.ctx = ctx;
                    if (vehicle.Db_Id == vehicleId)
                    {
                        vehicle.Locked = false;
                        Assert.AreEqual<int?>(count, vehicle.v_NumberOfIncidents("key", matchType, "", "", "+180di", "179d", vehicleLinkType)); //,"Index"));
                    }
                }
                //}
            }
        }

        public static void IncidentOccupancyCount(int riskClaimId, int occupancyCount)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                //{
                incident.ctx = ctx;
                Assert.AreEqual<int?>(occupancyCount, incident.v_VehiclesOccupancyCount);
                //}
            }
        }

        public static void PoliceAttended(int riskClaimId, bool policeAttended)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                //{
                incident.ctx = ctx;
                Assert.AreEqual<bool?>(policeAttended, incident.v_PoliceAttended);
                //}
            }
        }

        public static void AmbulanceAttended(int riskClaimId, bool ambulanceAttended)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                //{
                incident.ctx = ctx;
                Assert.AreEqual<bool?>(ambulanceAttended, incident.v_AmbulanceAttended);
                //}
            }
        }

        public static void NumberOfClaimants(int riskClaimId, int numberOfClaimants)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                //{
                incident.ctx = ctx;
                Assert.AreEqual<int?>(numberOfClaimants, incident.v_NumberOfClaimants);
                //}
            }
        }

        public static void PassengerRisk(int riskClaimId, int ageRangeCount, int? incidentTime)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(riskClaimId);

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
                //{
                incident.ctx = ctx;
                Assert.AreEqual<int>(ageRangeCount, incident.v_AgeRangeCount("key", 0, 10));

                if (incident.v_IncidentDateTime != null)
                {
                    Assert.AreEqual<int?>(incidentTime, incident.v_IncidentDateTime.Value.Hour);

                }

                //}
            }

        }
    }
}
