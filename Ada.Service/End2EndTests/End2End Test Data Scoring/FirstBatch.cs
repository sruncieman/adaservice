﻿using System;
using System.Linq;
using System.Data.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.WCF.WebServices;
using MDA.Common.FileModel;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Diagnostics;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.DataService;
using RiskEngine.Scoring.Entities;
using ADAServices.Factory;
using ADAServices.Interface;
using MDA.Pipeline;

namespace End2EndTests
{
    [TestClass]
    public class FirstBatch
    {

        [TestMethod]
        public void AllTests()
        {
            ServiceConfigSettings configSettings = new ServiceConfigSettings();

            using (var ctx = new MDA.Common.Server.CurrentContext(46, 0, "System"))
            {
                MDA.Pipeline.Pipeline.Deduplicate(ctx, configSettings);
            }

            DataPopulation.PopulateTestData1();
            MotorClaimBatch testData = Common.SubmitFile("External Test 1.XML");
            KeoghsCase();
            ActiveCfsCase();
            ClaimsExperience();
            MediumVehicleOccupancy();
            ExcessiveVehicleOccupancy();
            TotalClaimants();
            Communications();
            KeyAttractors();
            PassengerRisks();
            //AddressCFSCase();
            PersonAddressCFSCase();
            MobileNumberOpenCases();
            PersonMobileCFSCase();
        }


        #region Person is linked to another address that is linked to a Person on a CFS case - within 6 months of index

        [TestMethod]
        public void PersonAddressCFSCase()
        {
            CLB00042_1();
            CLB00042_2();
            CLB00042_3();
            CLB00042_4();
            CLB00042_5();
            CLB00042_6();
        }

        [TestMethod]
        public void CLB00042_1()
        {
            Results.LinkedAddressCaseCount(168, 235, "This+Confirmed", 1, null, "Current", "8", "19", "+6mi", "12m");
            Results.Score(168, 100);
        }

        [TestMethod]
        public void CLB00042_2()
        {
            Results.LinkedAddressCaseCount(169, 236, "This+Confirmed", 1, true, "Current", "8", "-19", "+6mi", "12m");
            Results.Score(169, 100);
        }

        [TestMethod]
        public void CLB00042_3()
        {
            Results.LinkedAddressCaseCount(170, 237, "This+Confirmed", 1, false, "Current", "8", "-19", "+6mi", "12m");
            Results.Score(170, 100);
        }

        [TestMethod]
        public void CLB00042_4()
        {
            Results.LinkedAddressCaseCount(171, 238, "This+Confirmed", 1, null, "NotCurrent", "8", "19", "+6mi", "12m");
            Results.Score(171, 100);
        }

        [TestMethod]
        public void CLB00042_5()
        {
            Results.LinkedAddressCaseCount(172, 239, "This+Confirmed", 1, true, "NotCurrent", "8", "-19", "+6mi", "12m");
            Results.Score(172, 100);
        }

        [TestMethod]
        public void CLB00042_6()
        {
            Results.LinkedAddressCaseCount(173, 240, "This+Confirmed", 1, false, "NotCurrent", "8", "-19", "+6mi", "12m");
            Results.Score(173, 100);
        }

        #endregion

        //#region Address is linked to Person on a CFS Case - within 6 months of index

        //[TestMethod]
        //public void AddressCFSCase()
        //{
        //    CLB00041_1();
        //    CLB00041_2();
        //    CLB00041_3();
        //    CLB00041_4();
        //    CLB00041_5();
        //    CLB00041_6();
        //}

        //[TestMethod]
        //public void CLB00041_1() //CurrentAdd6MonthCFSSC
        //{
        //    //Results.AddressNumberOfCases(161, 1, null, "Current", "8", "19", "+6mi", "12m");
        //    Results.Score(161, 250);
        //}

        //[TestMethod]
        //public void CLB00041_2() //CurrentAdd6MonthCFSNotSCclaimant
        //{
        //    //Results.AddressNumberOfCases(162, 1, true, "Current", "8", "-19", "+6mi", "12m");
        //    Results.Score(162, 250);
        //}

        //[TestMethod]
        //public void CLB00041_3() //CurrentAdd6MonthCFSNotSCNotClaimant
        //{
        //    //Results.AddressNumberOfCases(163, 1, false, "Current", "8", "-19", "+6mi", "12m");
        //    Results.Score(163, 250);
        //}

        //[TestMethod]
        //public void CLB00041_4() //NotCurrentAdd6MonthCFSSC
        //{
        //    //Results.AddressNumberOfCases(164, 1, null, "NotCurrent", "8", "19", "+6mi", "12m");
        //    Results.Score(164, 250);
        //}

        //[TestMethod]
        //public void CLB00041_5() //NotCurrentAdd6MonthCFSNotSCclaimant
        //{
        //    //Results.AddressNumberOfCases(165, 1, true, "NotCurrent", "8", "-19", "+6mi", "12m");
        //    Results.Score(165, 250);
        //}

        //[TestMethod]
        //public void CLB00041_6() //NotCurrentAdd6MonthCFSNotSCNotClaimant
        //{
        //    //Results.AddressNumberOfCases(166, 1, false, "NotCurrent", "8", "-19", "+6mi", "12m");
        //    Results.Score(166, 250);
        //}

        //#endregion

        #region Keoghs Case with Dispute Resolution/Fraud Pleaded/Discontinued/Struck Out

        [TestMethod]
        public void KeoghsCase()
        {
            CLB0001_1();
            CLB0001_2();
            CLB0003_1();
            CLB0001_2();
            CLB0001_2a();
            //CLB0002_2();
            CLB0003_2();
            CLB0001_3();
            //CLB0002_3();
            CLB0002_3b();
            CLB0003_3();
            CLB0001_4();
            CLB0002_4();
            CLB0003_4();
            CLB0003_4b();
        }

        [TestMethod]
        public void CLB0001_1() // 1 Settled Case With Fraudulent Outcome (Confirmed Match)
        {
            Results.SettledCaseCount(59, 1, 1, 1, "This+Confirmed"); 
            Results.Score(59,355); // Expected 300 but additional 55 scored - Person is an unconfirmed match to a person AND the person is linked to more than 0 incidents not including the index incident between Today and 4 Months before or after the date of the index incident
            // Incident Date 01/01/2012 - FindPersonAliasesSQL: PersonId=1 : Unconfirmed=[59,60] - PersonId 60 linked to IncidentId 63 - Incident Date 08/11/2011 00:00:00 
        }

        [TestMethod]
        public void CLB0002_1() // 1 Settled Case With Discontinued Outcome (Confirmed Match)
        {
            Results.SettledCaseCount(60, 2, 1, 1, "This+Confirmed");
            Results.Score(60,300);
        }

        [TestMethod]
        public void CLB0003_1() // 1 Settled Case With Struck Out Outcome (Confirmed Match)
        {
            Results.SettledCaseCount(61, 3, 1, 1, "This+Confirmed");
            Results.Score(61, 355);  // Expected 300 but additional 55 scored - Person is an unconfirmed match to a person AND the person is linked to more than 0 incidents not including the index incident between Today and 4 Months before or after the date of the index incident
            // Incident Date 03/01/2012 - FindPersonAliasesSQL: PersonId=3 : Unconfirmed=[62] - PersonId 62 linked to IncidentId 65 - Incident Date 06/01/2012
        }

        [TestMethod]
        public void CLB0001_2()
        {
            Results.SettledCaseCount(62, 63, 1, 1, "unconfirmed");
            Results.Score(62, 360); 
        }

        [TestMethod]
        public void CLB0001_2a()
        {
            Results.SettledCaseCount(63, 60, 1, 1, "unconfirmed");
            Results.Score(63, 360); 
        }

        [TestMethod]
        public void CLB0002_2()
        {
            Results.SettledCaseCount(64, 61, 1, 1, "unconfirmed");
            Results.Score(64, 150);
        }

        [TestMethod]
        public void CLB0003_2()
        {
            Results.SettledCaseCount(65, 62, 1, 1, "unconfirmed");
            Results.Score(65, 305); // Expected 150 but additional 55 scored - Person is an unconfirmed match to a person AND the person is linked to more than 0 incidents not including the index incident between Today and 4 Months before or after the date of the index incident
            // Incident Date 06/01/2012 - FindPersonAliasesSQL: PersonId=62 : Unconfirmed=[3] - PersonId 3 linked to IncidentId 61 - Incident Date 03/01/2012
        }

        [TestMethod]
        public void CLB0001_3()
        {
            Results.LinkedSettledCaseCount(66, 5, 1, 2, "This+Confirmed",19);
            Results.Score(66, 390); 
        }

        //[TestMethod]
        //public void CLB0002_3() 
        //{
        //    Results.LinkedSettledCaseCount(66, 6, 1, 1, "This+Confirmed");
        //    Results.Score(66, 55); // Expected score 0 however Sarah Nazir is linked to a case AND case type is not Staged/Contrived AND the manner of resolution for another person associated with the case is Discontinued
        //}

        [TestMethod]
        public void CLB0002_3b()
        {
            //Results.LinkedAddressCaseCount(67, 16, "This+Confirmed", 1, null, "Current", "8", "-19", "-6mi", "12m");
            Results.Score(68,0); 
        }

        [TestMethod]
        public void CLB0003_3() 
        {
            Results.LinkedSettledCaseCount(68,11, 1, 1, "This+Confirmed",19);
            Results.Score(69, 380); 
        }

        [TestMethod]
        public void CLB0001_4()
        {
            Results.LinkedSettledCaseCount(69, 67, 1, 2, "unconfirmed",19);
            Results.Score(70, 360); 
        }

        [TestMethod]
        public void CLB0002_4()
        {
            Results.LinkedSettledCaseCount(70, 68, 1, 1, "unconfirmed",-19);
            Results.Score(71,295); 
        }

        [TestMethod]
        public void CLB0003_4()
        {
            Results.LinkedSettledCaseCount(71, 69, 1, 1, "unconfirmed",19);
            Results.Score(72, 320); 
        }

        [TestMethod]
        public void CLB0003_4b()
        {
            Results.LinkedSettledCaseCount(72, 69, 0, 1, "unconfirmed",-19);
            Results.Score(73, 320);
        }
        #endregion

        #region Active CFS Case

        [TestMethod]
        public void ActiveCfsCase()
        {
            CLB0004_1();
            CLB0004_2();
            CLB0004_3();
            CLB0004_4();
        }

        [TestMethod]
        public void CLB0004_1()
        {
            //Results.NumberOfCases(73, 18, 1, "This+Confirmed");
            //Results.Score(74, 300);
        }

        [TestMethod]
        public void CLB0004_2()
        {
            Results.NumberOfCases(74, 19, 1, "This+Confirmed");
            Results.Score(75, 485);
        }

        [TestMethod]
        public void CLB0004_3()
        {
            //Results.NumberOfCases(75, 66, 1, "unconfirmed");
            //Results.Score(76, 150);
        }

        [TestMethod]
        public void CLB0004_4()
        {
            Results.NumberOfCases(76, 67, 1, "unconfirmed");
            Results.Score(77, 360); 
        }

        #endregion

        #region Claims Experience 1 in less than 4 months/2-3 in Last Year/In 5 Years

        [TestMethod]
        public void ClaimsExperience()
        {
            CLB0005_1();
            CLB0005_2();
            CLB0006_1();
            CLB0007_1();
            //CLB0005_3();
            //CLB0005_4();
            CLB0006_2();
            CLB0007_2();
        }

        [TestMethod]
        public void CLB0005_1()
        {
            // Error of file conversion - no vehicle on claim so Hasan Tannerah not part of claim HM3/1604658/HT1

            //Results.NumberOfIncidents(77, 68, 1, "This+Confirmed", 4, "Index"); // 0 Incidents for confirmed matches
            //Results.NumberOfIncidents(77, 68, 1, "Unconfirmed", 4, "Index");
            //Results.Score(77, 55); // 100 Expected but 55 scored - Person is an unconfirmed match to a person AND the person is linked to more than 0 incidents not including the index incident between Today and 4 Months before or after the date of the index incident
            // Incident Date 25/11/2012 - FindPersonAliasesSQL: PersonId=68 : Unconfirmed=[71] - PersonId 71 linked to IncidentId 82 - Incident Date 25/11/2012
        }

        [TestMethod]
        public void CLB0005_2()
        {
            // Error of file conversion - no vehicle on claim so Sajid Hanif not part of claim H0001027234D

            //Results.NumberOfIncidents(78, 72, 0, "This+Confirmed", 4, "Index");
            //Results.Score(78, 55); // 100 Expected but 55 scored - Person is an unconfirmed match to a person AND the person is linked to more than 0 incidents not including the index incident between Today and 4 Months before or after the date of the index incident
            // Incident Date 04/03/2012 - FindPersonAliasesSQL: PersonId=69 : Unconfirmed=[72] - PersonId 72 linked to IncidentId 83 - Incident Date 04/03/2012
        }

        [TestMethod]
        public void CLB0006_1()
        {
            Results.NumberOfIncidents(80, 22, 2, "This+Confirmed", "12m", "Index"); 
            Results.Score(80, 355); // 250 Expected
            // 300 scored - NumberOfCases = 1 (1 Incident linked to a KeoghsCase where there is no outcome)
            // 55 scored - Person is an unconfirmed match to a person AND the person is linked to more than 0 incidents not including the index incident between Today and 4 Months before or after the date of the index incident
            // Incident Date 02/03/2013 - FindPersonAliasesSQL: PersonId=20 : Unconfirmed=[73] - Person 73 linked to IncidentId 84 - Incident Date 02/01/2013                         
        }

        [TestMethod]
        public void CLB0007_1()
        {
            Results.NumberOfIncidents(81, 23, 6, "This+Confirmed", "36m", "Index");
            Results.Score(81, 0); // 250 Expected
            
            // 6 in last 3 years
            // 16 - 31/03/2012 
            // 17 - 22/04/2010 
            // 18 - 20/12/2011 
            // 19 - 28/08/2011 
            // 20 - 28/10/2011 
            // 21 - 24/12/2009 
        }

        //[TestMethod]
        //public void CLB0005_3()
        //{
        //    Results.NumberOfIncidents(81, 71, 1, "unconfirmed", 4, "Index"); 
        //    Results.Score(81, 55);                        
        //}

        //[TestMethod]
        //public void CLB0005_4()
        //{
        //    Results.NumberOfIncidents(82, 72, 1, "unconfirmed",4,"Index");
        //    Results.Score(82, 55);
        //}

        [TestMethod]
        public void CLB0006_2()
        {
            //Results.NumberOfIncidents(83, 73, 1, "unconfirmed");
            Results.Score(84, 305); // 100 Expected
            // |UpdateScore: [PersonFraudRules].[UnconfActiveCase] : Adding [150] : RuleScore[150]
            // |UpdateScore: [PersonAllClaimsExperience].[Unconf1Claim4MonthsIndex] : Adding [55] : RuleScore[55]
        }

        [TestMethod]
        public void CLB0007_2()
        {
            //Results.NumberOfIncidents(84, 74, 1, "unconfirmed");
            Results.Score(85, 0); // 100 Expected
        }

        #endregion

        #region Occupancy 4+ either vehicle

        [TestMethod]
        public void MediumVehicleOccupancy()
        {
            CLB0009_1();
            CLB0009_2();
            CLB0009_3();
        }

        [TestMethod]
        public void CLB0009_1()
        {
            Results.OccupancyCount(89, 86, 4);
            Results.Score(89, 0);
        }

        [TestMethod]
        public void CLB0009_2()
        {
            Results.OccupancyCount(90, 86, 5);
            Results.Score(90, 180); // 55 Expected - > 4 People No Police/Ambulance = Score 125
        }

        [TestMethod]
        public void CLB0009_3()
        {
            Results.OccupancyCount(91, 86, 4);
            Results.Score(91, 180); // 55 Expected - > 4 People No Police/Ambulance = Score 125
        }

        #endregion

        #region Total Occupancy 15+

        [TestMethod]
        public void ExcessiveVehicleOccupancy()
        {
            CLB00010_1();
            CLB00010_2();
            CLB00010_3();
        }

        [TestMethod]
        public void CLB00010_1()
        {
            Results.OccupancyCount(92, 92, 15);
            Results.Score(92, 235); // (125 expected) 2 vehicles with more than 4 passenger 55 * 2 & > 4 People No Police/Ambulance = Score 125
        }

        [TestMethod]
        public void CLB00010_2()
        {
            Results.OccupancyCount(93, 92, 17);
            Results.Score(93, 180); // (125 expected) 1 vehicle with more than 4 passenger 55 & > 4 People No Police/Ambulance = Score 125
        }

        [TestMethod]
        public void CLB00010_3()
        {
            Results.OccupancyCount(94, 92, 17);
            Results.Score(94, 180); // (125 expected) 1 vehicle with more than 4 passenger 55 & > 4 People No Police/Ambulance = Score 125
        }

        #endregion

        #region Total Claimants 4+

        [TestMethod]
        public void TotalClaimants()
        {
            CLB00011_1();
            CLB00011_2();
            CLB00011_3();
        }

        [TestMethod]
        public void CLB00011_1()
        {
            Results.AnyVehicleWithMultipleOccupancy(95, 5, false);
            //Results.NumberOfClaimants(94, 5);
            Results.Score(95, 0);
        }

        [TestMethod]
        public void CLB00011_2()
        {
            Results.AnyVehicleWithMultipleOccupancy(96, 5, false);
            //Results.NumberOfClaimants(95, 5);
            Results.Score(96, 0);
        }

        [TestMethod]
        public void CLB00011_3()
        {
            Results.AnyVehicleWithMultipleOccupancy(97, 5, false);
            //Results.NumberOfClaimants(96, 8);
            Results.Score(97, 150); // 55 Expected - > 4 People No Police/Ambulance 
        }

        #endregion

        #region Communications/Accounts used by multiples - Email quoted by >2/Landline quoted by >2/Mobile quoted by >2/Account quoted by  >2

        [TestMethod]
        public void Communications()
        {
            CLB00012_1();
            CLB00012_2();
            CLB00012_3();
            CLB00012_4();
            CLB00013_1();
            CLB00013_2();
            CLB00013_3();
            CLB00013_4();
            CLB00013_5();
            CLB00014_1();
            CLB00014_2();
            //CLB00016_1(); // MapFromExcel currently putting Bank Account details against Organisation instead of Person
            //CLB00016_2();
            //CLB00016_3();
            //CLB00016_4();
            //CLB00016_5();

        }

        [TestMethod]
        public void CLB00012_1()
        {
            Results.EmailUsageCount(98, 164, "This+Confirmed+unconfirmed", 4);
            Results.Score(98, 125);
        }

        [TestMethod]
        public void CLB00012_2()
        {
            Results.EmailUsageCount(99, 165, "This+Confirmed+unconfirmed", 3);
            Results.Score(99, 125); 
        }

        [TestMethod]
        public void CLB00012_3()
        {
            Results.EmailUsageCount(100, 166, "This+Confirmed+unconfirmed", 2);
            Results.Score(100, 125);
        }

        [TestMethod]
        public void CLB00012_4()
        {
            Results.EmailUsageCount(101, 167, "This+Confirmed+unconfirmed", 0); // Expected 3 but currently only counting links between P2E not O2E
            Results.Score(101, 0);
        }

        [TestMethod]
        public void CLB00013_1()
        {
            Results.TelephoneNumberUsageCount(102, 171, 4, false);
            Results.Score(102, 0); // Expected 125 but current rule only looks for mobiles not landlines
        }

        [TestMethod]
        public void CLB00013_2()
        {
            Results.TelephoneNumberUsageCount(103, 172, 1,true);
            Results.Score(103, 0);
        }

        [TestMethod]
        public void CLB00013_3()
        {
            Results.TelephoneNumberUsageCount(104, 173,2,true);
            Results.Score(104, 0); // Expected 125 but current rule only looks for mobiles not landlines
        }

        [TestMethod]
        public void CLB00013_4()
        {
            Results.TelephoneNumberUsageCount(105, 174,0,true); // Expected 3 but currently only counting links between P2T not O2T
            Results.Score(105, 0);
        }

        [TestMethod]
        public void CLB00013_5()
        {
            Results.TelephoneNumberUsageCount(106, 175, 3,true);
            Results.Score(106, 0);
        }

        [TestMethod]
        public void CLB00014_1()
        {
            Results.TelephoneNumberUsageCount(107, 176,4,true);
            Results.Score(107, 75);
        }

        [TestMethod]
        public void CLB00014_2()
        {
            Results.TelephoneNumberUsageCount(108, 177,0,true); // Expected 3 but currently only counting links between P2T not O2T
            Results.Score(108, 0);
        }

        //[TestMethod]
        //public void CLB00016_1()
        //{
        //    Results.BankAccountNumberUsageCount(108, 178, "This+Confirmed+unconfirmed", 1);
        //    Results.Score(108, 290);
        //}

        //[TestMethod]
        //public void CLB00016_2()
        //{
        //    Results.BankAccountNumberUsageCount(109, 179, "This+Confirmed+unconfirmed", 1);
        //    Results.Score(109, 0);
        //}

        //[TestMethod]
        //public void CLB00016_3()
        //{
        //    Results.BankAccountNumberUsageCount(110, 180, "This+Confirmed+unconfirmed", 1);
        //    Results.Score(110, 290);
        //}

        //[TestMethod]
        //public void CLB00016_4()
        //{
        //    Results.BankAccountNumberUsageCount(111, 23, "This+Confirmed+unconfirmed", 1);
        //    Results.Score(111, 0);
        //}

        //[TestMethod]
        //public void CLB00016_5()
        //{
        //    Results.BankAccountNumberUsageCount(112, 181, "This+Confirmed+unconfirmed", 1);
        //    Results.Score(112, 0);
        //}
        #endregion

        #region Address in High Risk Postcode - *** property not defined ***

        //[TestMethod]
        //public void HighRiskPostcode()
        //{
        //    CLB00017_1();
        //    CLB00017_2();
        //    CLB00017_3();
        //    CLB00017_4();
        //    CLB00017_5();
        //    CLB00017_6();
        //    CLB00017_7();
        //    CLB00017_8();
        //    CLB00017_9();
        //}

        //[TestMethod]
        //public void CLB00017_1()
        //{
        //    Results.Score(113, 100);
        //}

        //[TestMethod]
        //public void CLB00017_2()
        //{
        //    Results.Score(114, 100);
        //}

        //[TestMethod]
        //public void CLB00017_3()
        //{
        //    Results.Score(115, 100);
        //}

        //[TestMethod]
        //public void CLB00017_4()
        //{
        //    Results.Score(116, 100);
        //}

        //[TestMethod]
        //public void CLB00017_5()
        //{
        //    Results.Score(117, 100);
        //}

        //[TestMethod]
        //public void CLB00017_6()
        //{
        //    Results.Score(118, 55);
        //}

        //[TestMethod]
        //public void CLB00017_7()
        //{
        //    Results.Score(119, 55);
        //}

        //[TestMethod]
        //public void CLB00017_8()
        //{
        //    Results.Score(120, 55);
        //}

        //[TestMethod]
        //public void CLB00017_9()
        //{
        //    Results.Score(121, 0);
        //}

        #endregion

        #region Address Not Validated - *** property not defined ***

        //[TestMethod]
        //public void AddresNotValdidated()
        //{
        //    CLB00018_1();
        //    CLB00018_2();
        //    CLB00018_3();
        //    CLB00018_4();
        //    CLB00018_5();
        //    CLB00018_6();
        //    CLB00018_7();
        //}

        //[TestMethod]
        //public void CLB00018_1()
        //{
        //    Results.Score(122, 50);
        //}

        //[TestMethod]
        //public void CLB00018_2()
        //{
        //    Results.Score(123, 50);
        //}

        //[TestMethod]
        //public void CLB00018_3()
        //{
        //    Results.Score(124, 50);
        //}

        //[TestMethod]
        //public void CLB00018_4()
        //{
        //    Results.Score(125, 50);
        //}

        //[TestMethod]
        //public void CLB00018_5()
        //{
        //    Results.Score(126, 50);
        //}

        //[TestMethod]
        //public void CLB00018_6()
        //{
        //    Results.Score(127, 50);
        //}

        //[TestMethod]
        //public void CLB00018_7()
        //{
        //    Results.Score(128, 0);
        //}
        #endregion

        #region Key Attractors

        [TestMethod]
        public void KeyAttractors()
        {
            //CLB00019_1();
            //CLB00019_2();
            //CLB00019_3();
            //CLB00020_1();
            //CLB00020_2();
            //CLB00021_1();
            //CLB00025_1();
            //CLB00025_2();
        }

        //[TestMethod]
        //public void CLB00019_1()
        //{
        //    Results.Score(129, 300);
        //}

        //[TestMethod]
        //public void CLB00019_2()
        //{
        //    Results.Score(130, 250);
        //}

        //[TestMethod]
        //public void CLB00019_3()
        //{
        //    Results.Score(131, 250);
        //}

        //[TestMethod]
        //public void CLB00020_1()
        //{
        //    Results.Score(132, 300);
        //}

        //[TestMethod]
        //public void CLB00020_2()
        //{
        //    Results.Score(133, 250);
        //}

        //[TestMethod]
        //public void CLB00021_1()
        //{
        //    Results.Score(134, 300);
        //}

        //[TestMethod]
        //public void CLB00025_1()
        //{
        //    Results.Score(135, 300);
        //}

        //[TestMethod]
        //public void CLB00025_2()
        //{
        //    Results.Score(136, 300);
        //}

        #endregion

        #region Phones and Accounts to multiples - different address

        [TestMethod]
        public void PhoneAccountMultipleUsage()
        {
            CLB00026_1();
            CLB00027_1();
            CLB00028_1();
        }

        [TestMethod]
        public void CLB00026_1()
        {
            Results.Score(138, 260);
        }

        [TestMethod]
        public void CLB00027_1()
        {
            Results.Score(139, 280);
        }

        [TestMethod]
        public void CLB00028_1()
        {
            Results.Score(140, 250);
        }

        #endregion

        #region Cross Hire - *** property not defined ***

        //[TestMethod]
        //public void CrossHire()
        //{
        //    CLB00029_1();
        //    CLB00029_2();
        //    //CLB00030_3();
        //}

        //[TestMethod]
        //public void CLB00029_1()
        //{
        //    Results.Score(140, 280);
        //}

        //[TestMethod]
        //public void CLB00029_2()
        //{
        //    Results.Score(141, 280);
        //}

        //[TestMethod]
        //public void CLB00030_3()
        //{
        //    Results.Score(142, 0);
        //}

        #endregion

        #region Multiple Policies

        [TestMethod]
        public void MultiplePolicies()
        {
            //CLB00030_1();
            //CLB00030_2();
            CLB00030_3a();
            //CLB00030_4();
        }

        [TestMethod]
        public void CLB00030_1()
        {
            Results.Score(144, 260);
        }

        [TestMethod]
        public void CLB00030_2()
        {
            Results.Score(145, 260);
        }

        [TestMethod]
        public void CLB00030_3a()
        {
            Results.Score(146, 0);
        }

        [TestMethod]
        public void CLB00030_4()
        {
            Results.Score(147, 0);
        }

        #endregion

        #region Policy Inception Risks

        [TestMethod]
        public void PolicyInceptionRisks()
        {
            CLB00031_1();
            CLB00032_1();
            CLB00032_2();
            CLB00033_1();
        }

        [TestMethod]
        public void CLB00031_1()
        {
            Results.NumberOfDaysSincePolicyInception(148, 10);
            Results.Score(148, 150);
        }

        [TestMethod]
        public void CLB00032_1()
        {
            Results.NumberOfDaysSincePolicyInception(149, 35);
            Results.Score(149, 150);
        }

        [TestMethod]
        public void CLB00032_2()
        {
            Results.NumberOfDaysSincePolicyInception(150, 91);
            Results.Score(150, 0);
        }

        [TestMethod]
        public void CLB00033_1()
        {
            Results.NumberOfDaysSincePolicyEnd(151, 10);
            Results.Score(151, 250);
        }

        #endregion

        #region Passenger Risks

        [TestMethod]
        public void PassengerRisks()
        {
            CLB00034_1();
            CLB00036_1();
            //CLB00037_1();
            //CLB00037_2();
        }


        [TestMethod]
        public void CLB00034_1() // 4 People No Police/Ambulance
        {
            //Results.IncidentOccupancyCount(151, 4);
            Results.PoliceAttended(152, false);
            Results.AmbulanceAttended(152, false);
            Results.Score(152, 0); // Expected 125 but only 2 passengers other occupants are drivers
        }

        [TestMethod]
        public void CLB00036_1() // 2 Pass - 1 9 years old, acc 14:05
        {
            Results.PassengerRisk(153, 1, 0); // no accident time 
            Results.Score(153, 0); // Expected 55 
        }

        //[TestMethod]
        //public void CLB00037_1() // 2 Pass - 1 9 years old, no police/ambulance, no time quoted
        //{
        //    Results.PassengerRisk(153, 1, null);
        //    Results.PoliceAttended(153, false);
        //    Results.AmbulanceAttended(153, false);
        //    Results.Score(153, 40);
        //}

        //[TestMethod]
        //public void CLB00037_2() // 4 pass, 1 9 year old, acc 14:05, no police attendence
        //{
        //    Results.PassengerRisk(154, 1, 14);
        //    Results.PoliceAttended(154, false);
        //    Results.Score(154, 220);
        //}

        #endregion

        #region 90 Day Hire Exceeded - *** property not defined ***

        //[TestMethod]
        //public void HireExceeded()
        //{
        //    CLB00038_1();
        //    CLB00038_2();
        //}

        //[TestMethod]
        //public void CLB00038_1()
        //{
        //    Results.Score(155, 0);
        //}

        //[TestMethod]
        //public void CLB00038_2()
        //{
        //    Results.Score(156, 260);
        //}

        #endregion

        #region Risk Occupations (Taxi Driver, Courier) - *** property not defined ***

        //[TestMethod]
        //public void RiskOccupations()
        //{
        //    CLB00040_1();
        //    CLB00040_2();
        //    CLB00040_3();
        //    CLB00040_4();
        //}

        //[TestMethod]
        //public void CLB00040_1()
        //{
        //    Results.Score(157, 55);
        //}

        //[TestMethod]
        //public void CLB00040_2()
        //{
        //    Results.Score(158, 55);
        //}

        //[TestMethod]
        //public void CLB00040_3()
        //{
        //    Results.Score(159, 55);
        //}

        //[TestMethod]
        //public void CLB00040_4()
        //{
        //    Results.Score(160, 55);
        //}

        #endregion

        #region Mobile Number links to a Person on an active Keoghs Case

        [TestMethod]
        public void MobileNumberOpenCases()
        {
            CLB00043_1();
        }

        [TestMethod]
        public void CLB00043_1()
        {
            Results.MobileNumberOfCases(174, 241, 1, "This+Confirmed", "Instruction");
            Results.Score(174, 250); //[PersonMobileCFSCase].[MobileConfActiveCaseNotSC] : Adding [250] : RuleScore[250]
        }

        #endregion

        #region Mobile Number links to a Person on a closed Keoghs Case

        [TestMethod]
        public void PersonMobileCFSCase()
        {
            CLB00044_1();
            CLB00044_2();
            CLB00044_3();
            CLB00044_4();
            CLB00044_5();
        }

        [TestMethod]
        public void CLB00044_1()
        {
            Results.Score(175, 250);//[MobileConfActiveCaseNotSC] RuleScore[250]
        }

        [TestMethod]
        public void CLB00044_2()
        {
            Results.Score(176, 250); //[MobileCFSFraud] RuleScore[250]
        }

        [TestMethod]
        public void CLB00044_3()
        {
            Results.Score(177, 50); //[MobileCFSOtherSusFraudSC] RuleScore[250]
        }

        [TestMethod]
        public void CLB00044_4()
        {
            Results.Score(178, 50);//[MobileCFSOtherFraudSC] RuleScore[250]
        }

        [TestMethod]
        public void CLB00044_5()
        {
            Results.Score(179, 50); //[MobileCFSOtherFraudNotSC] RuleScore[250]
        }

        #endregion

    }
}