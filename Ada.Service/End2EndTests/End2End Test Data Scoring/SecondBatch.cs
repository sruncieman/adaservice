﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace End2EndTests.End2End_Test_Data_Scoring
{
    [TestClass]
    public class SecondBatch
    {

        #region People Matches on Claims

        [TestMethod]
        public void PeopleMatchesOnClaims()
        {
            DataPopulation.PopulateTestData2();
            CLB2001_1();
            CLB2001_2();
            CLB2001_3();
            CLB2001_4();

        }

        [TestMethod]
        public void CLB2001_1() 
        {
            Results.Score(58, 600);
        }

        [TestMethod]
        public void CLB2001_2() 
        {
            Results.Score(58, 450);
        }

        [TestMethod]
        public void CLB2001_3() 
        {
            Results.Score(58, 365);
        }

        [TestMethod]
        public void CLB2001_4() 
        {
            Results.Score(58, 550);
        }

        #endregion

        #region Address & People Matches on Claims Experience/Address Matches on Claims Experience


        [TestMethod]
        public void ClaimsExperience()
        {
            DataPopulation.PopulateTestData2();
            CLB2001_5();
            CLB2001_6();
            CLB2001_7();
            CLB2001_8();
            CLB2001_9();
            CLB2001_10();

        }

        [TestMethod]
        public void CLB2001_5() 
        {
            Results.Score(58, 280);
        }

        [TestMethod]
        public void CLB2001_6()
        {
            Results.Score(58, 430);
        }

        [TestMethod]
        public void CLB2001_7()
        {
            Results.Score(58, 375);
        }

        [TestMethod]
        public void CLB2001_8()
        {
            Results.Score(58, 75);
        }

        [TestMethod]
        public void CLB2001_9()
        {
            Results.Score(58, 75);
        }

        [TestMethod]
        public void CLB2001_10() 
        {
            Results.Score(58, 425);
        }

        #endregion

        #region Occupancy/Claimants and Claims Experience

        [TestMethod]
        public void OccupancyClaimantsClaimsExperience()
        {
            DataPopulation.PopulateTestData2();
            CLB2002_1();
            CLB2002_2();
            CLB2002_3();
            CLB2002_4();
            CLB2002_5();

        }

        [TestMethod]
        public void CLB2002_1()
        {
            Results.Score(58, 155);
        }

        [TestMethod]
        public void CLB2002_2()
        {
            Results.Score(58, 330);
        }

        [TestMethod]
        public void CLB2002_3()
        {
            Results.Score(58, 400);
        }

        [TestMethod]
        public void CLB2002_4()
        {
            Results.Score(58, 0);
        }

        [TestMethod]
        public void CLB2002_5()
        {
            Results.Score(58, 255);
        }

        #endregion

        #region Multiple Commuinications use / Claims Exp


        [TestMethod]
        public void CommunicationsClaimsExperience()
        {
            DataPopulation.PopulateTestData2();
            CLB2003_1();
            CLB2003_2();
            CLB2003_3();
            CLB2003_4();

        }

        [TestMethod]
        public void CLB2003_1()
        {
            Results.Score(58, 260);
        }

        [TestMethod]
        public void CLB2003_2() 
        {
            Results.Score(58, 305);
        }

        [TestMethod]
        public void CLB2003_3()
        {
            Results.Score(58, 0);
        }

        [TestMethod]
        public void CLB2003_4()
        {
            Results.Score(58, 0);
        }


        #endregion

        #region Key Attractor

        [TestMethod]
        public void KeyAttractor()
        {
            DataPopulation.PopulateTestData2();
            CLB2004_1();
            CLB2004_2();
            CLB2004_3();
            CLB2005_1();
            CLB2005_2();
            CLB2005_3();

        }

        [TestMethod]
        public void CLB2004_1()
        {
            Results.Score(58, 150);
        }

        [TestMethod]
        public void CLB2004_2()
        {
            Results.Score(58, 300);
        }

        [TestMethod]
        public void CLB2004_3()
        {
            Results.Score(58, 50);
        }

        [TestMethod]
        public void CLB2005_1()
        {
            Results.Score(58, 400);
        }

        [TestMethod]
        public void CLB2005_2()
        {
            Results.Score(58, 450);
        }

        [TestMethod]
        public void CLB2005_3()
        {
            Results.Score(58, 1175);
        }

        #endregion

        #region Cross Hire


        [TestMethod]
        public void CrossHire()
        {
            DataPopulation.PopulateTestData2();
            CLB2006_1();
            CLB2006_2();
            CLB2006_3();

        }

        [TestMethod]
        public void CLB2006_1()
        {
            Results.Score(58, 280);
        }

        [TestMethod]
        public void CLB2006_2()
        {
            Results.Score(58, 0);
        }

        [TestMethod]
        public void CLB2006_3()
        {
            Results.Score(58, 0);
        }


        #endregion

        #region Occupancy / Age of passengers

        [TestMethod]
        public void OccupancyAgePassengers()
        {
            DataPopulation.PopulateTestData2();
            CLB2007_1();
            CLB2007_2a();
            CLB2007_2b();

        }

        [TestMethod]
        public void CLB2007_1()
        {
            Results.Score(58, 55);
        }

        [TestMethod]
        public void CLB2007_2a()
        {
            Results.Score(58, 0);
        }

        [TestMethod]
        public void CLB2007_2b()
        {
            Results.Score(58, 220);
        }


        #endregion

        #region Claim vs Incpetion & Occupation

        [TestMethod]
        public void InceptionOccupation()
        {
            DataPopulation.PopulateTestData2();
            CLB2008_1();
            CLB2008_2();

        }

        [TestMethod]
        public void CLB2008_1()
        {
            Results.Score(58, 315);
        }

        [TestMethod]
        public void CLB2008_2()
        {
            Results.Score(58, 315);
        }

        #endregion

    }
}
