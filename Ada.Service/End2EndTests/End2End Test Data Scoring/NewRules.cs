﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace End2EndTests.End2End_Test_Data_Scoring
{
    [TestClass]
    public class NewRules
    {

        #region Match Claim

        [TestMethod]
        public void MatchClaim()
        {

            // Set riskClientId = 0 in Common.cs
            
            //DataPopulation.PopulateTestMatchClaim1();

            // Set riskClientId = 3 in Common.cs

            DataPopulation.PopulateTestMatchClaim2();

        }
        #endregion

        #region Vehicle Claims Experience
        [TestMethod]
        public void VehicleClaimsExperience()
        {
            DataPopulation.PopulateTestDataVehicleClaimsExperience();

            Results.Score(1, 0); // Incident Date - 23/06/2013

            //Results.VehicleNumberOfIncidents(2, 1, 1, "This+Confirmed", "Insured Vehicle"); // Incident Date - 23/02/2013
            //Results.Score(2, 75); // UpdateScore: [VehicleAllClaimsExperience].[ConfVeh1Claim6MonthsPost] : Adding [75] : RuleScore[75]

            Results.VehicleNumberOfIncidents(2, 1, 1, "This+Confirmed", "Insured Vehicle"); // Incident Date - 23/09/2013
            Results.Score(2, 45); // UpdateScore: [VehicleAllClaimsExperience].[ConfVeh1Claim6MonthsPre] : Adding [105] : RuleScore[105]

        }
        #endregion

        #region Person Key Attractors

        [TestMethod]
        public void KaConfUnconfTest()
        {
            DataPopulation.PopulateTestDataKA();

            Results.IsPersonKeyAttractor(2, 1, true, "This+Confirmed"); // Confirmed (Full Name/DOB/NI)
            Results.Score(2, 300); // This Person OR any Confirmed Matches for this Person is a key attractor

            Results.IsPersonKeyAttractor(3, 2, true, "Unconfirmed"); // Unconfirmed (Full Name/DOB/Tel)
            Results.Score(3, 355); // An unconfirmed match for this person is a key attractor

        }
        #endregion

        #region Tracesmart
        [TestMethod]
        public void Tracesmart()
        {
            DataPopulation.PopulateTestTracesmart();

            #region Financial Concerns

            // NOT YET IMPLEMENTED

            //Results.Score(1, 270);   //15_00BF602-1

            //Results.Score(2, 270);   //15_00BF602-2

            //Results.Score(3, 55);    //15_00BF602-3

            //Results.Score(4, 270);   //15_00BF602-3

            //Results.Score(5, 55);    //15_00BF602-6
            #endregion

            #region Gone Away

            Results.Score(6, 55);   //13_0000607-5

            #endregion

            #region IDV Check

            Results.Score(7, 0);    //15_0000602-6
            Results.Score(8, 250);  //13_0000607-5
            Results.Score(9, 150);  //13_0000607-2

            #endregion

            #region Person Details Not Validated

            Results.Score(10, 125);
            Results.Score(11, 125);
            Results.Score(12, 100);
            Results.Score(13, 100);
            Results.Score(14, 150);

            #endregion

            #region Sanctions

            Results.Score(15, 290);

            #endregion

            #region Telephone Not Valid or Inactive

            //Results.Score(16, 55);
            //Results.Score(17, 55);
            //Results.Score(18, 55);

            #endregion


        }
        #endregion

        [TestMethod]
        public void CLB00012_1()
        {
            Results.EmailUsageCount(1, 200206, "This+Confirmed+unconfirmed", 2);

        }


    }
}
