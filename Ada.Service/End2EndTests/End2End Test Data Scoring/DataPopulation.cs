﻿using System;
using System.Linq;
using System.Data.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.WCF.WebServices;
using MDA.Common.FileModel;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Diagnostics;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.DataService;
using RiskEngine.Scoring.Entities;
using MDA.Common.Server;

namespace End2EndTests
{
    public class DataPopulation
    {

        public static void PopulateTestTracesmart()
        {
            MotorClaimBatch FinancialConcerns1 = Common.SubmitFile("Financial Concerns test 1.xml");
            MotorClaimBatch FinancialConcerns2 = Common.SubmitFile("Financial Concerns test 2.xml");
            MotorClaimBatch FinancialConcerns3 = Common.SubmitFile("Financial Concerns test 3.xml");
            MotorClaimBatch FinancialConcerns3_1 = Common.SubmitFile("Financial Concerns test 3-1.xml");
            MotorClaimBatch FinancialConcerns6 = Common.SubmitFile("Financial Concerns test 6.xml");

            MotorClaimBatch GoneAway1 = Common.SubmitFile("Gone Away test 1.xml");

            MotorClaimBatch IDVCheck1 = Common.SubmitFile("IDV Check test 1.xml");
            MotorClaimBatch IDVCheck2 = Common.SubmitFile("IDV Check test 2.xml");
            MotorClaimBatch IDVCheck3 = Common.SubmitFile("IDV Check test 3.xml");

            MotorClaimBatch PersonDetailsNotValidated1 = Common.SubmitFile("Person Details Not Validated test 1.xml");
            MotorClaimBatch PersonDetailsNotValidated2 = Common.SubmitFile("Person Details Not Validated test 2.xml");
            MotorClaimBatch PersonDetailsNotValidated3 = Common.SubmitFile("Person Details Not Validated test 3.xml");
            MotorClaimBatch PersonDetailsNotValidated4 = Common.SubmitFile("Person Details Not Validated test 4.xml");
            MotorClaimBatch PersonDetailsNotValidated5 = Common.SubmitFile("Person Details Not Validated test 5.xml");

            MotorClaimBatch Sanctions = Common.SubmitFile("Sanctions test 1.xml");

            MotorClaimBatch TelephoneNotValidorInactive1 = Common.SubmitFile("Telephone Not Valid or Inactive test 1.xml");
            MotorClaimBatch TelephoneNotValidorInactive2 = Common.SubmitFile("Telephone Not Valid or Inactive test 6.xml");
            MotorClaimBatch TelephoneNotValidorInactive3 = Common.SubmitFile("Telephone Not Valid or Inactive Tracesmart test 6-1.xml");



        }

        public static void PopulateTestMatchClaim1()
        {
            MotorClaimBatch Claim1 = Common.SubmitFile("MatchClaim1.xml");
            PopulateIncidentClaimType(2, 0);
        }

        public static void PopulateTestMatchClaim2()
        {
            MotorClaimBatch Cliam2 = Common.SubmitFile("MatchClaim2.xml");
        }

        public static void PopulateTestAddressClaimsExperience()
        {
            MotorClaimBatch Address1 = Common.SubmitFile("Address Number of Incidents Test 1.xml");
            MotorClaimBatch Address2 = Common.SubmitFile("Address Number of Incidents Test 2.xml");
            MotorClaimBatch Address3 = Common.SubmitFile("Address Number of Incidents Test 3.xml");
        }

        public static void PopulateTestDataVehicleClaimsExperience()
        {
            MotorClaimBatch Incident1 = Common.SubmitFile("Vehicle Number of Incidents Test 1.xml");
            MotorClaimBatch Incident2 = Common.SubmitFile("Vehicle Number of Incidents Test 2.xml");
            //MotorClaimBatch Incident3 = Common.SubmitFile("Vehicle Number of Incidents Test 3.xml");
        }

        public static void PopulateTestDataKA()
        {
            MotorClaimBatch ibaseData1 = Common.SubmitFile("ManizaTest.XML"); // IM31/10639012G - Maniza Akhtar
            PopulatePersonKeyAttractor(1, "Key Attractor"); // Populate key attractor for Maniza Akhtar
            MotorClaimBatch ibaseData2 = Common.SubmitFile("Maniza Test Conf.XML"); // CLB0001/1 - Confirmed Match (Full Name/DOB/NI)
            MotorClaimBatch ibaseData3 = Common.SubmitFile("Maniza Test Unconf.XML"); // CLB0001/2 - Unconfirmed Match (Full Name/DOB/Tel)
        }

        public static void PopulateTestData1()
        {
            MotorClaimBatch ibaseData = Common.SubmitFile("iBase Data.XML");

            PopulateIncidentClaimType(2, 0);
            PopulateIncidentClaimType(3, 19);
            PopulateIncidentClaimType(4, 19);
            PopulateIncidentClaimType(5, 19);
            PopulateIncidentClaimType(6, 0);
            PopulateIncidentClaimType(7, 19);
            PopulateIncidentClaimType(8, 0);
            PopulateIncidentClaimType(9, 0);
            PopulateIncidentClaimType(10, 19);
            PopulateIncidentClaimType(11, 0);
            PopulateIncidentClaimType(12, 19);
            PopulateIncidentClaimType(13, 19);
            PopulateIncidentClaimType(14, 19);
            PopulateIncidentClaimType(15, 19);
            PopulateIncidentClaimType(16, 19);
            PopulateIncidentClaimType(17, 0);
            PopulateIncidentClaimType(18, 19);
            PopulateIncidentClaimType(19, 0);

            //PopulateIncident2PersonOutcome("Settled Best Terms - Suspect Fraud but Not Proven", 1, 2);
            PopulateKeoghsCase(1, "2", "304023");

            PopulateIncident2PersonOutcome("Discontinued", 2, 3);
            PopulateKeoghsCase(2, "3", "303471");

            PopulateIncident2PersonOutcome("Claim Struck-out", 3, 4);
            PopulateKeoghsCase(2, "4", "304154");

            PopulateIncident2PersonOutcome("Settled Best Terms - Using Fraud Arguments", 4, 5);
            //PopulateIncident2PersonOutcome("Settled Best Terms - Genuine Case/Claimant", 5, 5);
            PopulateKeoghsCase(2, "5", "303736");

            PopulateIncident2PersonOutcome("Not Pursued and not heard for 3 months", 6, 6);
            PopulateIncident2PersonOutcome("Discontinued", 7, 6);
            PopulateIncident2PersonOutcome("Technical Knockout", 8, 6);
            PopulateKeoghsCase(1, "6", "311673");

            PopulateIncident2PersonOutcome("Negotiated Settlement", 9, 7);
            PopulateIncident2PersonOutcome("Claim Struck-out", 10, 7);
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 11, 7);
            PopulateIncident2PersonOutcome("Case Struck Out", 12, 7);
            PopulateKeoghsCase(2, "7", "309653");

            //PopulateIncident2PersonOutcome("Statute Barred", 13, 8);
            PopulateIncident2PersonOutcome("Discontinued", 14, 8);
            PopulateKeoghsCase(1, "8", "311777");

            PopulateIncident2PersonOutcome("Negotiated Settlement", 15, 9);
            PopulateIncident2PersonOutcome("Negotiated Settlement", 16, 9);
            PopulateIncident2PersonOutcome("Negotiated Settlement", 17, 9);
            PopulateKeoghsCase(1, "9", "311816");

            PopulateKeoghsCase(1, "10", "310430");

            PopulateKeoghsCase(1, "11", "311510");

            PopulateKeoghsCase(2, "12", "311498");

            PopulateKeoghsCase(2, "13", "311542");

            PopulateKeoghsCase(1, "14", "311187");

            PopulateKeoghsCase(2, "15", "310787");

            PopulateKeoghsCase(2, "16", "309278");

            PopulateKeoghsCase(1, "17", "311836");

            PopulateKeoghsCase(1, "18", "311840");

            PopulateIncident2PersonOutcome("Negotiated Settlement", 19, 19);
            PopulateKeoghsCase(2, "19", "311244");

            PopulatePersonKeyAttractor(55, "Key Attractor");
            PopulateOrganisationKeyAttractor(65, "Key Attractor");


            //PopulateIncident2PersonOutcome("Negotiated Settlement", 49, 50);
            //PopulateKeoghsCase(2, "50", "311678");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 50, 51);
            //PopulateKeoghsCase(1, "51", "311144");
            //PopulateKeoghsCase(2, "52", "311359");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 52, 53);
            //PopulateKeoghsCase(2, "53", "311691");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 53, 54);
            //PopulateKeoghsCase(2, "54", "311900");
            //PopulateKeoghsCase(1, "55", "311531");
            //PopulateKeoghsCase(1, "56", "310592");
            //PopulateKeoghsCase(1, "57", "311685");
            //PopulateKeoghsCase(1, "58", "311685");
            //PopulateKeoghsCase(1, "59", "311685");
            //PopulateKeoghsCase(2, "60", "311758");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 60, 61);
            //PopulateKeoghsCase(2, "61", "311758");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 61, 62);
            //PopulateKeoghsCase(2, "62", "311758");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 62, 63);
            //PopulateKeoghsCase(2, "63", "311164");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 63, 64);
            //PopulateKeoghsCase(1, "64", "311753");
            //PopulateKeoghsCase(1, "65", "311642");
            //PopulateKeoghsCase(1, "66", "311137");
            //PopulateKeoghsCase(1, "67", "311834");
            //PopulateKeoghsCase(2, "68", "311899");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 68, 69);
            //PopulateKeoghsCase(2, "69", "311909");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 69, 70);
            //PopulateKeoghsCase(2, "70", "306362");
            //PopulateIncident2PersonOutcome("Negotiated Settlement", 70, 71);
            //PopulateKeoghsCase(1, "71", "305974");


            PopulateKeoghsCase2Incident(2, 2, 2);
            PopulateKeoghsCase2Incident(3, 3, 3);
            PopulateKeoghsCase2Incident(4, 4, 4);
            PopulateKeoghsCase2Incident(5, 5, 5);
            PopulateKeoghsCase2Incident(6, 6, 6);
            PopulateKeoghsCase2Incident(7, 7, 7);
            PopulateKeoghsCase2Incident(8, 8, 8);
            PopulateKeoghsCase2Incident(9, 9, 9);
            PopulateKeoghsCase2Incident(10, 10, 10);
            PopulateKeoghsCase2Incident(11, 11, 11);
            PopulateKeoghsCase2Incident(12, 12, 12);
            PopulateKeoghsCase2Incident(13, 13, 13);
            PopulateKeoghsCase2Incident(14, 14, 14);
            PopulateKeoghsCase2Incident(15, 15, 15);
            PopulateKeoghsCase2Incident(16, 16, 16);
            PopulateKeoghsCase2Incident(17, 17, 17);
            PopulateKeoghsCase2Incident(18, 18, 18);


            //PopulateKeoghsCase2Incident(48, 48, 47);
            //PopulateKeoghsCase2Incident(49, 49, 48);
            //PopulateKeoghsCase2Incident(50, 50, 49);
            //PopulateKeoghsCase2Incident(51, 51, 50);
            //PopulateKeoghsCase2Incident(52, 52, 51);
            //PopulateKeoghsCase2Incident(53, 53, 52);
            //PopulateKeoghsCase2Incident(54, 54, 53);
            //PopulateKeoghsCase2Incident(55, 55, 54);
            //PopulateKeoghsCase2Incident(56, 56, 55);
            //PopulateKeoghsCase2Incident(57, 57, 56);
            //PopulateKeoghsCase2Incident(58, 58, 57);
            //PopulateKeoghsCase2Incident(59, 59, 58);
            //PopulateKeoghsCase2Incident(60, 60, 59);
            //PopulateKeoghsCase2Incident(61, 61, 60);
            //PopulateKeoghsCase2Incident(62, 62, 61);
            //PopulateKeoghsCase2Incident(63, 63, 62);
            //PopulateKeoghsCase2Incident(64, 64, 63);
            //PopulateKeoghsCase2Incident(65, 65, 64);
            //PopulateKeoghsCase2Incident(66, 66, 65);
            //PopulateKeoghsCase2Incident(67, 67, 66);
            //PopulateKeoghsCase2Incident(68, 68, 67);
            //PopulateKeoghsCase2Incident(69, 69, 68);
            //PopulateKeoghsCase2Incident(70, 70, 69);
            //PopulateKeoghsCase2Incident(71, 71, 70);

            
        }

        public static void PopulateTestData2()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {
                //MotorClaimBatch ibaseData = Common.SubmitFile("iBase Data 2.XML");
                MotorClaimBatch testData = Common.SubmitFile("External Test 4.XML");
            }
        }


        public static void PopulateTestData3()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {
                MotorClaimBatch ibaseData = Common.SubmitFile("iBase Data 3.XML");
                MotorClaimBatch testData = Common.SubmitFile("External Test 3.XML");
            }
        }

        public static void PopulateIncident2PersonOutcome(string mannerOfResolution, int person, int incident)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {
                Incident2PersonOutcome i2pO = new Incident2PersonOutcome
                {
                    Id = 1,
                    ADARecordStatus = 0,
                    CreatedBy = "Fred",
                    CreatedDate = DateTime.Today,
                    Incident_Id = incident,
                    Person_Id = person,
                    MannerOfResolution = mannerOfResolution,

                };

                ctx.db.Incident2PersonOutcome.Add(i2pO);

                try
                {
                    ctx.db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public static void PopulateKeoghsCase(int caseStatus, string caseId, string eliteReference)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                KeoghsCase kC = new KeoghsCase
                {
                    KeoghsOffice_Id = 1,
                    Client_Id = 1,
                    CaseStatus_Id = caseStatus,
                    ADARecordStatus = 0,
                    CreatedBy = "Fred",
                    CreatedDate = DateTime.Today,
                    KeoghsCaseId = caseId,
                    KeoghsEliteReference = eliteReference
                };

                ctx.db.KeoghsCases.Add(kC);

                try
                {
                    ctx.db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public static void PopulateKeoghsCase2Incident(int incidentId, int caseId, int riskClaimId)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                KeoghsCase2Incident kC2i = new KeoghsCase2Incident
                {
                    KeoghsCase_Id = caseId,
                    Incident_Id = incidentId,
                    CaseIncidentLinkType_Id = 1,
                    RiskClaim_Id = riskClaimId,
                    ADARecordStatus = 0,
                    CreatedBy = "Fred",
                    CreatedDate = DateTime.Today,

                };

                ctx.db.KeoghsCase2Incident.Add(kC2i);

                try
                {
                    ctx.db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public static void PopulateIncidentClaimType(int incidentId, int claimType)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                var incidents = from x in ctx.db.Incidents
                                where x.Id == incidentId
                                select x;

                foreach (var incident in incidents)
                {
                    incident.ClaimType_Id = claimType;
                    incident.IncidentType_Id = 8;
                }


                try
                {
                    ctx.db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public static void PopulatePersonKeyAttractor(int personId, string keyAttractor)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                var people = from x in ctx.db.People
                             where x.Id == personId
                             select x;

                foreach (var person in people)
                {
                    person.KeyAttractor = keyAttractor;
                }

                try
                {
                    ctx.db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public static void PopulateOrganisationKeyAttractor(int organisationId, string keyAttractor)
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                var organisations = from x in ctx.db.Organisations
                                    where x.Id == organisationId
                                    select x;

                foreach (var organisation in organisations)
                {
                    organisation.KeyAttractor = keyAttractor;
                }

                try
                {
                    ctx.db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
