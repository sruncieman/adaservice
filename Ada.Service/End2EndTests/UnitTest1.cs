﻿   using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.WCF.WebServices;
using MDA.Common.FileModel;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Diagnostics;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.Common.Helpers;
using MDA.DataService;

namespace End2EndTests
{
    [TestClass]
    public class UnitTest1
    {
        public static void CheckCounts(MdaDbContext db, RecordCounts counts)
        {
            UnitTest1.CheckBankAccountCount(db, counts.BankAccountCount);
            UnitTest1.CheckAddressCount(db, counts.AddressCount);
            UnitTest1.CheckDrivingLicenseCount(db, counts.DrivingLicenseCount);
            UnitTest1.CheckEmailCount(db, counts.EmailCount);
            UnitTest1.CheckIncidentCount(db, counts.IncidentCount);
            UnitTest1.CheckNINumberCount(db, counts.NINumberCount);
            UnitTest1.CheckOrganisationCount(db, counts.OrganisationCount);
            UnitTest1.CheckPassportCount(db, counts.PassportCount);
            UnitTest1.CheckPaymentCardCount(db, counts.PaymentCardCount);
            UnitTest1.CheckPersonCount(db, counts.PersonCount);
            UnitTest1.CheckPolicyCount(db, counts.PolicyCount);
            UnitTest1.CheckTelephoneCount(db, counts.TelephoneCount);
            UnitTest1.CheckVehicleCount(db, counts.VehicleCount);
            UnitTest1.CheckWebSiteCount(db, counts.WebSiteCount);

            UnitTest1.CheckPerson2BankAccountCount(db, counts.Person2BankAccountCount);
            UnitTest1.CheckPerson2BankAccountCountAll(db, counts.Person2BankAccountCountAll);

            UnitTest1.CheckOrganisation2BankAccountCount(db, counts.Organisation2BankAccountCount);
            UnitTest1.CheckPerson2AddressCount(db, counts.Person2AddressCount);
            UnitTest1.CheckOrganisation2AddressCount(db, counts.Organisation2AddressCount);
            UnitTest1.CheckVehicle2AddressCount(db, counts.Vehicle2AddressCount);
            UnitTest1.CheckAddress2AddressCount(db, counts.Address2AddressCount);
            UnitTest1.CheckPerson2BBPinCount(db, counts.Person2BBPinCount);
            UnitTest1.CheckPerson2DrivingLicenseCount(db, counts.Person2DrivingLicenseCount);
            UnitTest1.CheckPerson2EmailCount(db, counts.Person2EmailCount);
            UnitTest1.CheckOrganisation2EmailCount(db, counts.Organisation2EmailCount);
            UnitTest1.CheckIncident2FraudRingCount(db, counts.Incident2FraudRingCount);
            UnitTest1.CheckIncident2PersonCount(db, counts.Incident2PersonCount);
            UnitTest1.CheckIncident2OrganisationCount(db, counts.Incident2OrganisationCount);
            UnitTest1.CheckIncident2IncidentCount(db, counts.Incident2IncidentCount);
            UnitTest1.CheckPerson2IPAddressCount(db, counts.Person2IPAddressCount);
            UnitTest1.CheckOrganisation2IPAddressCount(db, counts.Organisation2IPAddressCount);
            UnitTest1.CheckPerson2NINumberCount(db, counts.Person2NINumberCount);
            UnitTest1.CheckOrganisation2OrganisationCount(db, counts.Organisation2OrganisationCount);
            UnitTest1.CheckPerson2PassportCount(db, counts.Person2PassportCount);
            UnitTest1.CheckPerson2PaymentCardCount(db, counts.Person2PaymentCardCount);
            UnitTest1.CheckOrganisation2PaymentCardCount(db, counts.Organisation2PaymentCardCount);
            UnitTest1.CheckPerson2OrganisationCount(db, counts.Person2OrganisationCount);
            UnitTest1.CheckPerson2PersonCount(db, counts.Person2PersonCount);
            UnitTest1.CheckPerson2PolicyCount(db, counts.Person2PolicyCount);
            UnitTest1.CheckOrganisation2PolicyCount(db, counts.Organisation2PolicyCount);
            UnitTest1.CheckVehicle2PolicyCount(db, counts.Vehicle2PolicyCount);
            UnitTest1.CheckPolicy2BankAccountCount(db, counts.Policy2BankAccountCount);
            UnitTest1.CheckPolicy2PaymentCardCount(db, counts.Policy2PaymentCardCount);
            UnitTest1.CheckPerson2TelephoneCount(db, counts.Person2TelephoneCount);
            UnitTest1.CheckOrganisation2TelephoneCount(db, counts.Organisation2TelephoneCount);
            UnitTest1.CheckAddress2TelephoneCount(db, counts.Address2TelephoneCount);
            UnitTest1.CheckIncident2VehicleCount(db, counts.Incident2VehicleCount);
            UnitTest1.CheckVehicle2PersonCount(db, counts.Vehicle2PersonCount);
            UnitTest1.CheckVehicle2OrganisationCount(db, counts.Vehicle2OrganisationCount);
            UnitTest1.CheckVehicle2VehicleCount(db, counts.Vehicle2VehicleCount);
            UnitTest1.CheckPerson2WebSiteCount(db, counts.Person2WebSiteCount);
            UnitTest1.CheckOrganisation2WebSiteCount(db, counts.Organisation2WebSiteCount);
            UnitTest1.CheckIncident2PersonOrganisationCount(db, counts.Incident2PersonOrganisationCount);
        }
        public static void CheckClaim(MdaDbContext db, MDA.Common.FileModel.MotorClaim claim)
        {
            #region Policy

            var policy = (from x in db.Policies
                            where x.PolicyNumber == claim.Policy.PolicyNumber
                            && x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).FirstOrDefault();

            UnitTest1.CheckPolicy(claim.Policy, policy);

            #endregion

            #region Vehicle

            foreach (var v in claim.Vehicles)
            {
                var vehicle = (from x in db.Vehicles
                                where x.VIN == v.VIN
                                && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                select x).FirstOrDefault();

                UnitTest1.CheckVehicle(v, vehicle);

                #region Person 

                foreach (var p in v.People)
                {
                    var person = (from x in db.People
                                    where x.LastName == p.LastName 
                                    && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select x).FirstOrDefault();

                    UnitTest1.CheckPerson(p, person);

                    foreach (var a in p.Addresses)
                    {
                        var address = (from x in db.Addresses
                                        where x.PostCode == a.PostCode && x.BuildingNumber == a.BuildingNumber
                                        && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                        select x).First();

                        UnitTest1.CheckAddress(a, address);
                    }

                    foreach (var o in p.Organisations)
                    {
                        var organisation = (from x in db.Organisations
                                            where x.OrganisationName == o.OrganisationName
                                            && x.RegisteredNumber == o.RegisteredNumber
                                            && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select x).FirstOrDefault();

                        UnitTest1.CheckOrganisation(o,organisation);

                        foreach (var ov in o.Vehicles)
                        {
                            var orgVehicle = (from x in db.Vehicles
                                                where x.VIN == ov.VIN 
                                                && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).FirstOrDefault();

                            UnitTest1.CheckVehicle(ov, orgVehicle);
                        }
                    }
                }
                #endregion Person

            }
            #endregion Vehicle

            foreach (var o in claim.Organisations)
            {
                var organisation = (from x in db.Organisations
                                    where x.OrganisationName == o.OrganisationName
                                    && x.RegisteredNumber == o.RegisteredNumber
                                    && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select x).FirstOrDefault();

                UnitTest1.CheckOrganisation(o, organisation);

                foreach (var ov in o.Vehicles)
                {
                    var orgVehicle = (from x in db.Vehicles
                                      where x.VIN == ov.VIN
                                      && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                      select x).FirstOrDefault();

                    UnitTest1.CheckVehicle(ov, orgVehicle);
                }
            }
        }
        public static void CheckIncidentCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Incidents
                where x.ADARecordStatus == (byte)ADARecordStatus.Current && x.Id > 0
                select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckBankAccountCount(MdaDbContext db, int n)
        {
            var count = (from x in db.BankAccounts
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPaymentCardCount(MdaDbContext db, int n)
        {
            var count = (from x in db.PaymentCards
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current 
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckEmailCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Emails
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current 
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckNINumberCount(MdaDbContext db, int n)
        {
            var count = (from x in db.NINumbers
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current 
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckDrivingLicenseCount(MdaDbContext db, int n)
        {
            var count = (from x in db.DrivingLicenses
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current 
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPassportCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Passports
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current 
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckTelephoneCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Telephones
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current 
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckWebSiteCount(MdaDbContext db, int n)
        {
            var count = (from x in db.WebSites
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current 
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisationCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisations
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current 
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckIncident2FraudRingCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Incident2FraudRing
                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckIncident2IncidentCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Incident2Incident
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckIncident2OrganisationCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Incident2Organisation
                         where x.IsClaimLevelLink == true && x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckIncident2PersonCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Incident2Person
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckIncident2PersonOrganisationCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Incident2Organisation
                         where x.IsClaimLevelLink == false && x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckIncident2PersonOrganisationCountAll(MdaDbContext db, int n)
        {
            var count = (from x in db.Incident2Organisation
                         where x.IsClaimLevelLink == false
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckIncident2VehicleCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Incident2Vehicle
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPolicyCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Policies
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPolicy2BankAccountCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Policy2BankAccount
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPolicy2PaymentCardCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Policy2PaymentCard
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckVehicleCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Vehicles
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckVehicle2AddressCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Vehicle2Address
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckVehicle2OrganisationCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Vehicle2Organisation
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckVehicle2PersonCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Vehicle2Person
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckVehicle2PolicyCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Vehicle2Policy
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckVehicle2VehicleCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Vehicle2Vehicle
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPersonCount(MdaDbContext db, int n)
        {
            var count = (from x in db.People
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2AddressCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2Address
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2BankAccountCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2BankAccount
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2BankAccountCountAll(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2BankAccount
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2BBPinCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2BBPin
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2DrivingLicenseCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2DrivingLicense
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2EmailCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2Email
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2IPAddressCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2IPAddress
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2NINumberCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2NINumber
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2OrganisationCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2Organisation
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2OrganisationCountAll(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2Organisation
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2PassportCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2Passport
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2PaymentCardCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2PaymentCard
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2PersonCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2Person
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2PolicyCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2Policy
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2TelephoneCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2Telephone
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPerson2WebSiteCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Person2WebSite
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckAddressCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Addresses
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckAddress2AddressCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Address2Address
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckAddress2TelephoneCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Address2Telephone
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisationsCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisations
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisation2AddressCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisation2Address
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisation2BankAccountCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisation2BankAccount
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisation2EmailCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisation2Email
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisation2IPAddressCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisation2IPAddress
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisation2OrganisationCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisation2Organisation
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisation2PaymentCardCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisation2PaymentCard
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisation2PolicyCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisation2Policy
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisation2TelephoneCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisation2Telephone
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckOrganisation2WebSiteCount(MdaDbContext db, int n)
        {
            var count = (from x in db.Organisation2WebSite
                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                         select x).Count();

            Assert.AreEqual<int>(n, count);
        }
        public static void CheckPolicy(MDA.Common.FileModel.Policy xmlPolicy, MDA.DAL.Policy dbPolicy)
        {

            DateTime? xmlPolicyEndDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(xmlPolicy.PolicyEndDate);
            DateTime? xmlPolicyStartDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(xmlPolicy.PolicyStartDate);
            DateTime? dbPolicyEndDate = dbPolicy.PolicyEndDate;
            DateTime? dbPolicyStartDate = dbPolicy.PolicyStartDate;

            Assert.AreEqual<string>(dbPolicy.Broker, xmlPolicy.Broker);
            Assert.AreEqual<int>(dbPolicy.PolicyCoverType_Id, (int)xmlPolicy.CoverType);
            Assert.AreEqual<DateTime?>(dbPolicyEndDate, xmlPolicyEndDate);
            Assert.AreEqual<string>(dbPolicy.PolicyNumber, xmlPolicy.PolicyNumber);
            Assert.AreEqual<DateTime?>(dbPolicyStartDate, xmlPolicyStartDate);
            Assert.AreEqual<int>(dbPolicy.PolicyType_Id, (int)xmlPolicy.PolicyType);
            Assert.AreEqual<int>((int)dbPolicy.Premium, (int)xmlPolicy.Premium);
            Assert.AreEqual<string>(dbPolicy.PreviousFaultClaimsCount.ToString(), xmlPolicy.PreviousFaultClaimsCount.ToString());
            Assert.AreEqual<string>(dbPolicy.PreviousNoFaultClaimsCount.ToString(), xmlPolicy.PreviousNoFaultClaimsCount.ToString());
            Assert.AreEqual<string>(dbPolicy.InsurerTradingName, xmlPolicy.TradingName);
        }
        public static void CheckPaymentCard(MDA.Common.FileModel.PaymentCard xmlCard, MDA.DAL.PaymentCard dbCard)
        {
            Assert.AreEqual<string>(dbCard.BankName, xmlCard.BankName);
            Assert.AreEqual<string>(dbCard.PaymentCardNumber, xmlCard.PaymentCardNumber);
            Assert.AreEqual<int>(dbCard.PaymentCardType_Id, (int)xmlCard.PolicyPaymentType);
        }
        public static void CheckOrganisationPaymentCard(MDA.Common.FileModel.PaymentCard xmlPolicy, MDA.DAL.Organisation2PaymentCard dbPolicy)
        {
            Assert.AreEqual<string>(dbPolicy.PaymentCard.BankName, xmlPolicy.BankName);
            Assert.AreEqual<string>(dbPolicy.PaymentCard.PaymentCardNumber, xmlPolicy.PaymentCardNumber);
            Assert.AreEqual<int>(dbPolicy.PaymentCard.PaymentCardType_Id, (int)xmlPolicy.PolicyPaymentType);
        }
        public static void CheckVehicle(MDA.Common.FileModel.Vehicle xmlVehicle, MDA.DAL.Vehicle dbVehicle)
        {
            Assert.AreEqual<int>(dbVehicle.VehicleCategoryOfLoss_Id, (int)xmlVehicle.CategoryOfLoss);
            Assert.AreEqual<int>(dbVehicle.VehicleColour_Id, (int)xmlVehicle.Colour);
            Assert.AreEqual<string>(dbVehicle.EngineCapacity, xmlVehicle.EngineCapacity);
            Assert.AreEqual<int>(dbVehicle.VehicleFuel_Id, (int)xmlVehicle.Fuel);
            Assert.AreEqual<string>(dbVehicle.VehicleMake, xmlVehicle.Make);
            Assert.AreEqual<string>(dbVehicle.Model, xmlVehicle.Model);
            Assert.AreEqual<int>(dbVehicle.VehicleTransmission_Id, (int)xmlVehicle.Transmission);
            Assert.AreEqual<string>(dbVehicle.VIN, xmlVehicle.VIN);
            Assert.AreEqual<string>(dbVehicle.VehicleRegistration, xmlVehicle.VehicleRegistration.Replace(" ", ""));
            Assert.AreEqual<int>(dbVehicle.VehicleType_Id, (int)xmlVehicle.VehicleType);
        }
        public static void CheckVehicle(MDA.Common.FileModel.OrganisationVehicle xmlVehicle, MDA.DAL.Vehicle dbVehicle)
        {
            Assert.AreEqual<int>(dbVehicle.VehicleColour_Id, (int)xmlVehicle.Colour);
            Assert.AreEqual<string>(dbVehicle.EngineCapacity, xmlVehicle.EngineCapacity);
            Assert.AreEqual<int>(dbVehicle.VehicleFuel_Id, (int)xmlVehicle.Fuel);
            Assert.AreEqual<string>(dbVehicle.VehicleMake, xmlVehicle.Make);
            Assert.AreEqual<string>(dbVehicle.Model, xmlVehicle.Model);
            Assert.AreEqual<int>(dbVehicle.VehicleTransmission_Id, (int)xmlVehicle.Transmission);
            Assert.AreEqual<string>(dbVehicle.VIN, xmlVehicle.VIN);
            Assert.AreEqual<string>(dbVehicle.VehicleRegistration, xmlVehicle.VehicleRegistration);
            Assert.AreEqual<int>(dbVehicle.VehicleType_Id, (int)xmlVehicle.VehicleType);
        }
        public static void CheckPerson(MDA.Common.FileModel.Person xmlPerson, MDA.DAL.Person dbPerson)
        {
            if (!xmlPerson.DateOfBirth.HasValue)
                Assert.IsNull(dbPerson.DateOfBirth);
            else
                Assert.AreEqual<DateTime>(dbPerson.DateOfBirth.Value.Date, xmlPerson.DateOfBirth.Value.Date);

            Assert.AreEqual<string>(dbPerson.FirstName, xmlPerson.FirstName);
            Assert.AreEqual<string>(dbPerson.LastName, xmlPerson.LastName);
            Assert.AreEqual<int>(dbPerson.Gender_Id, (int)xmlPerson.Gender);
            Assert.AreEqual<string>(dbPerson.Nationality, xmlPerson.Nationality);
            Assert.AreEqual<string>(dbPerson.Occupation, xmlPerson.Occupation);
        }
        public static void CheckPersonEmail(MDA.Common.FileModel.Person xmlPerson, MDA.DAL.Email email)
        {
            Assert.AreEqual<string>(xmlPerson.EmailAddress, email.EmailAddress);
        }
        public static void CheckOrganisationEmail(MDA.Common.FileModel.Organisation xmlPEmail, MDA.DAL.Organisation2Email dbPEmail)
        {
            Assert.AreEqual<string>(dbPEmail.Email.EmailAddress, xmlPEmail.Email);
        }
        public static void CheckPersonTelephone(MDA.Common.FileModel.Person xmlPPhone, MDA.DAL.Person2Telephone dbPPhone)
        {
            //Assert.AreEqual<string>(dbPPhone.Telephone.TelephoneNumber, xmlPPhone.LandlineTelephone);
            //Assert.AreEqual<string>(dbPPhone.Telephone.TelephoneNumber, xmlPPhone.MobileTelephone); 
            //Assert.AreEqual<string>(dbPPhone.Telephone.TelephoneNumber, xmlPPhone.WorkTelephone);
        }
        public static void CheckOrganisationTelephone(MDA.Common.FileModel.Organisation xmlPPhone, MDA.DAL.Organisation2Telephone dbPPhone)
        {
            //Assert.AreEqual<string>(dbPPhone.Telephone.TelephoneNumber, xmlPPhone.LandlineTelephone);
            //Assert.AreEqual<string>(dbPPhone.Telephone.TelephoneNumber, xmlPPhone.MobileTelephone);
            //Assert.AreEqual<string>(dbPPhone.Telephone.TelephoneNumber, xmlPPhone.WorkTelephone);
        }
        public static void CheckAddress( MDA.Common.FileModel.Address xmlAddress, MDA.DAL.Address dbAddress )
        {

            Assert.AreEqual<string>(dbAddress.Building, xmlAddress.Building);
            Assert.AreEqual<string>(dbAddress.BuildingNumber, xmlAddress.BuildingNumber);
            Assert.AreEqual<string>(dbAddress.County, xmlAddress.County);
            Assert.AreEqual<string>(dbAddress.Locality, xmlAddress.Locality);
            Assert.AreEqual<string>(dbAddress.PostCode, xmlAddress.PostCode);
            Assert.AreEqual<string>(dbAddress.Street, xmlAddress.Street);
            Assert.AreEqual<string>(dbAddress.SubBuilding, xmlAddress.SubBuilding);
            Assert.AreEqual<string>(dbAddress.Town, xmlAddress.Town);
        }
        public static void CheckAddressLink(MDA.Common.FileModel.Address xmlAddress, MDA.DAL.Person2Address dbAddress)
        {
            Assert.AreEqual<string>(dbAddress.EndOfResidency.ToString(), xmlAddress.EndOfResidency.ToString());
            Assert.AreEqual<string>(dbAddress.StartOfResidency.ToString(), xmlAddress.StartOfResidency.ToString());
        }
        public static void CheckOrganisation(MDA.Common.FileModel.Organisation xmlOrganisation, MDA.DAL.Organisation dbOrganisation)
        {
            Assert.AreEqual<string>(dbOrganisation.MojCRMNumber, xmlOrganisation.MojCrmNumber);
            Assert.AreEqual<string>(dbOrganisation.OrganisationName, xmlOrganisation.OrganisationName);

            Assert.AreEqual<int>(dbOrganisation.OrganisationType_Id, (int)xmlOrganisation.OrganisationType);

            Assert.AreEqual<string>(dbOrganisation.RegisteredNumber, xmlOrganisation.RegisteredNumber);
            Assert.AreEqual<string>(dbOrganisation.VATNumber, xmlOrganisation.VatNumber);
        }
        public static void CheckBankAccount(MDA.Common.FileModel.BankAccount xmlBankAccount, MDA.DAL.BankAccount dbBankAccount)
        {
            Assert.AreEqual<string>(dbBankAccount.AccountNumber, xmlBankAccount.AccountNumber);
            Assert.AreEqual<string>(dbBankAccount.BankName, xmlBankAccount.BankName);
            Assert.AreEqual<string>(dbBankAccount.SortCode, xmlBankAccount.SortCode);
        }
        public static void CheckPolicyBankAccount(MDA.Common.FileModel.BankAccount xmlBankAccount, MDA.DAL.Policy2BankAccount dbBankAccount)
        {
            Assert.AreEqual<string>(dbBankAccount.DatePaymentDetailsTaken.ToString(), xmlBankAccount.DatePaymentDetailsTaken.ToString());
            Assert.AreEqual<int>(dbBankAccount.PolicyPaymentType_Id, (int)xmlBankAccount.PolicyPaymentType);
        }
        public static void CheckClaimInfo(MDA.Common.FileModel.ClaimInfo xmlClaimInfo, MDA.DAL.Incident2Person dbClaimInfo)
        {
            Assert.AreEqual<bool?>(dbClaimInfo.AmbulanceAttended, xmlClaimInfo.AmbulanceAttended);
            Assert.AreEqual<string>(dbClaimInfo.ClaimCode, xmlClaimInfo.ClaimCode);
            Assert.AreEqual<string>(dbClaimInfo.ClaimNotificationDate.ToString(), xmlClaimInfo.ClaimNotificationDate.ToString());
            Assert.AreEqual<int>(dbClaimInfo.ClaimStatus_Id, (int)xmlClaimInfo.ClaimStatus);
            Assert.AreEqual<string>(dbClaimInfo.IncidentCircs, xmlClaimInfo.IncidentCircumstances);
            Assert.AreEqual<string>(dbClaimInfo.IncidentLocation, xmlClaimInfo.IncidentLocation);
            //Assert.AreEqual<TimeSpan?>(dbClaimInfo.IncidentTime, xmlClaimInfo.IncidentTime);
            //Assert.AreEqual<int>(dbClaimInfo.MojStatus_Id, (int)xmlClaimInfo.MojStatus);
            Assert.AreEqual<int>((int)dbClaimInfo.PaymentsToDate, (int)xmlClaimInfo.PaymentsToDate);
            Assert.AreEqual<bool?>(dbClaimInfo.PoliceAttended, xmlClaimInfo.PoliceAttended);
            Assert.AreEqual<string>(dbClaimInfo.PoliceForce, xmlClaimInfo.PoliceForce);
            Assert.AreEqual<string>(dbClaimInfo.PoliceReference, xmlClaimInfo.PoliceReference);
            Assert.AreEqual<int>((int)dbClaimInfo.Reserve, (int)xmlClaimInfo.Reserve);
        }

    }
}
