﻿using System;
using System.Linq;
using System.Data.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.Common.FileModel;
using MDA.Common.Server;
using MDA.Common.Enum;
using MDA.DataService;
using MDA.DAL;
using MDA.RiskService.Model;
using System.Data.Entity;

namespace End2EndTests
{
    [TestClass]
    public class MobileTests
    {

        [TestMethod]
        public void AllMobileTests()
        {
            DataPopulation.PopulateTestData1();

            MBCLM000_0();
            MBCLM000_1();
            MBCLM000_2();
            MBCLM000_3();
        }

        [TestMethod]
        public void MBCLM000_0()
        {

            MobileClaimBatch claim1 = Common.SubmitMobileFile("MBCLM000_0.xml", "MobileTestBatch0");

            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                var riskBatch = RiskBatch(ctx, "MobileTestBatch0");
                Assert.AreEqual<int>((int)RiskBatchStatus.BatchLoadComplete, riskBatch.BatchStatus);
                Assert.AreEqual<string>("Mobile", riskBatch.BatchEntityType);

                var riskClaim = RiskClaim(ctx, "MobileTestBatch0", "MBCLM000_0");
                Assert.AreEqual<int>((int)RiskClaimStatus.Scored, riskClaim.ClaimStatus);

                var riskClaimRun = RiskClaimRun(ctx, "MobileTestBatch0", "MBCLM000_0");
                //Assert.AreEqual<int>(300, riskClaimRun.TotalScore); // [PersonFraudRules].[ConfActiveCase] : RuleScore[300]

                var incident = Incident(ctx, "MobileTestBatch0", "MBCLM000_0");
                Assert.AreEqual<int>((int)MDA.Common.Enum.IncidentType.MobilePhone, incident.IncidentType_Id);

                var handset = Handset(ctx, incident.Id);
                Assert.AreEqual<string>("IMEI0001", handset.HandsetIMEI);
                Assert.AreEqual<string>("Apple", handset.HandsetMake);
                Assert.AreEqual<string>("iPhone 5c", handset.HandsetModel);

                System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));
                var x = ctx.db.uspGetMatchingHandsetIMEI(null, handset.HandsetIMEI, p).ToList();
                Assert.AreEqual<int>(1, x.Count());

            }

        }
                
        [TestMethod]
        public void MBCLM000_1() 
        {

            MobileClaimBatch claim1 = Common.SubmitMobileFile("MBCLM000_1.xml", "MobileTestBatch1");

            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                var riskBatch = RiskBatch(ctx, "MobileTestBatch1");                
                Assert.AreEqual<int>((int)RiskBatchStatus.BatchLoadComplete, riskBatch.BatchStatus);
                Assert.AreEqual<string>("Mobile", riskBatch.BatchEntityType);

                var riskClaim = RiskClaim(ctx, "MobileTestBatch1", "MBCLM000_1");
                Assert.AreEqual<int>((int)RiskClaimStatus.Scored, riskClaim.ClaimStatus);
                
                var riskClaimRun = RiskClaimRun(ctx, "MobileTestBatch1", "MBCLM000_1");
                Assert.AreEqual<int>(0, riskClaimRun.TotalScore);

                var incident = Incident(ctx, "MobileTestBatch1", "MBCLM000_1");
                Assert.AreEqual<int>((int)MDA.Common.Enum.IncidentType.MobilePhone, incident.IncidentType_Id);

                var handset = Handset(ctx, incident.Id);
                Assert.AreEqual<string>("IMEI0001", handset.HandsetIMEI);
                Assert.AreEqual<string>("Apple", handset.HandsetMake);
                Assert.AreEqual<string>("iPhone 5c", handset.HandsetModel);

                System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));
                var x = ctx.db.uspGetMatchingHandsetIMEI(null, handset.HandsetIMEI, p).ToList();
                Assert.AreEqual<int>(1, x.Count());

            }

        }

        [TestMethod]
        public void MBCLM000_2()
        {

            MobileClaimBatch claim2 = Common.SubmitMobileFile("MBCLM000_2.xml", "MobileTestBatch2");

            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                var riskBatch = RiskBatch(ctx, "MobileTestBatch2");
                Assert.AreEqual<int>((int)RiskBatchStatus.BatchLoadComplete, riskBatch.BatchStatus);
                Assert.AreEqual<string>("Mobile", riskBatch.BatchEntityType);

                var riskClaim = RiskClaim(ctx, "MobileTestBatch2", "MBCLM000_2");
                Assert.AreEqual<int>((int)RiskClaimStatus.Scored, riskClaim.ClaimStatus);

                var riskClaimRun = RiskClaimRun(ctx, "MobileTestBatch2", "MBCLM000_2");
                Assert.AreEqual<int>(250, riskClaimRun.TotalScore); //[AddressCFSCase].[CurrentAddCFSOpenNotSC] : RuleScore[250]

                var incident = Incident(ctx, "MobileTestBatch2", "MBCLM000_2");
                Assert.AreEqual<int>((int)MDA.Common.Enum.IncidentType.MobilePhone, incident.IncidentType_Id);

                var handset = Handset(ctx, incident.Id);
                Assert.AreEqual<string>("IMEI0002", handset.HandsetIMEI);
                Assert.AreEqual<string>("Samsung", handset.HandsetMake);
                Assert.AreEqual<string>("Galaxy 5", handset.HandsetModel);

                System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));
                var x = ctx.db.uspGetMatchingHandsetIMEI(null, handset.HandsetIMEI, p).ToList();
                Assert.AreEqual<int>(1, x.Count());

            }

        }

        [TestMethod]
        public void MBCLM000_3()
        {

            MobileClaimBatch claim3 = Common.SubmitMobileFile("MBCLM000_3.xml", "MobileTestBatch3");

            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                var riskBatch = RiskBatch(ctx, "MobileTestBatch3");
                Assert.AreEqual<int>((int)RiskBatchStatus.BatchLoadComplete, riskBatch.BatchStatus);
                Assert.AreEqual<string>("Mobile", riskBatch.BatchEntityType);

                var riskClaim = RiskClaim(ctx, "MobileTestBatch3", "MBCLM000_3");
                Assert.AreEqual<int>((int)RiskClaimStatus.Scored, riskClaim.ClaimStatus);

                var riskClaimRun = RiskClaimRun(ctx, "MobileTestBatch3", "MBCLM000_3");
                Assert.AreEqual<int>(250, riskClaimRun.TotalScore); //[AddressCFSCase].[CurrentAddCFSOpenNotSC] : RuleScore[250]

                var incident = Incident(ctx, "MobileTestBatch3", "MBCLM000_3");
                Assert.AreEqual<int>((int)MDA.Common.Enum.IncidentType.MobilePhone, incident.IncidentType_Id);

                var handset = Handset(ctx, incident.Id);
                Assert.AreEqual<string>("IMEI0002", handset.HandsetIMEI);
                Assert.AreEqual<string>("Samsung", handset.HandsetMake);
                Assert.AreEqual<string>("Galaxy 5", handset.HandsetModel);

                System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));
                var x = ctx.db.uspGetMatchingHandsetIMEI(null, handset.HandsetIMEI, p).ToList();
                Assert.AreEqual<int>(1, x.Count());

            }

        }

        public RiskBatch RiskBatch(CurrentContext ctx, string clientBatchReference)
        {

            RiskBatch riskBatch = (from x in ctx.db.RiskBatches
                                   where x.ClientBatchReference == clientBatchReference
                                   select x).FirstOrDefault();

            return riskBatch;
                       
        }

        public RiskClaim RiskClaim(CurrentContext ctx, string clientBatchReference, string clientClaimRefNumber)
        {
            RiskClaim riskClaim = (from x in ctx.db.RiskClaims
                                   where x.RiskBatch.ClientBatchReference == clientBatchReference
                                   && x.ClientClaimRefNumber == clientClaimRefNumber
                                   select x).FirstOrDefault();

            return riskClaim;
                        
        }

        public RiskClaimRun RiskClaimRun(CurrentContext ctx, string clientBatchReference, string clientClaimRefNumber)
        {
            RiskClaimRun riskClaimRun = (from x in ctx.db.RiskClaimRuns
                                         where x.RiskClaim.RiskBatch.ClientBatchReference == clientBatchReference
                                         && x.RiskClaim.ClientClaimRefNumber == clientClaimRefNumber
                                         select x).FirstOrDefault();

            return riskClaimRun;

        }

        public Incident Incident(CurrentContext ctx, string clientBatchReference, string clientClaimRefNumber)
        {
            Incident incident = (from x in ctx.db.RiskClaims
                                   where x.RiskBatch.ClientBatchReference == clientBatchReference
                                   && x.ClientClaimRefNumber == clientClaimRefNumber
                                   select x.Incident).FirstOrDefault();

            return incident;

        }

        public MDA.DAL.Handset Handset(CurrentContext ctx, int incidentId)
        {
            MDA.DAL.Handset handset = (from x in ctx.db.Incident2Handset
                                       where x.Incident_Id == incidentId
                                       select x.Handset).FirstOrDefault();

            return handset;

        }

    }
}
