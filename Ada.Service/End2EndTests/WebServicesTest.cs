﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.WCF.WebServices.Messages;
using MDA.Common.FileModel;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Diagnostics;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.DataService;

namespace End2EndTests
{

    [TestClass]
    public partial class WebServiceTest
    {

        [TestMethod]
        public void TestMessageList()
        {
            MDA.WCF.WebServices.ADAWebServices proxy = new MDA.WCF.WebServices.ADAWebServices();

            var x = proxy.GetClaimsForClient(new ClaimsForClientRequest() { ClientId = 1, FilterUploadedBy = null, Page = 1, PageSize = 10, SortColumn = "IncidentDate" });
        }
    }
}
