﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.WCF.WebServices;
using MDA.Common.FileModel;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Diagnostics;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.DataService;
using MDA.Common.Server;

namespace End2EndTests
{
    public class RecordCounts
    {
        public int BankAccountCount = 0;
        public int AddressCount = 0;
        public int DrivingLicenseCount = 0;
        public int EmailCount = 0;
        public int IncidentCount = 0;
        public int NINumberCount = 0;
        public int OrganisationCount = 0;
        public int PassportCount = 0;
        public int PaymentCardCount = 0;
        public int PersonCount = 0;
        public int PolicyCount = 0;
        public int TelephoneCount = 0;
        public int VehicleCount = 0;
        public int WebSiteCount = 0;

        public int Person2BankAccountCount = 0;
        public int Person2BankAccountCountAll = 0;

        public int Organisation2BankAccountCount = 0;
        public int Person2AddressCount = 0;
        public int Organisation2AddressCount = 0;
        public int Vehicle2AddressCount = 0;
        public int Address2AddressCount = 0;
        public int Person2BBPinCount = 0;
        public int Person2DrivingLicenseCount = 0;
        public int Person2EmailCount = 0;
        public int Organisation2EmailCount = 0;
        public int Incident2FraudRingCount = 0;
        public int Incident2PersonCount = 0;
        public int Incident2OrganisationCount = 0;
        public int Incident2IncidentCount = 0;
        public int Person2IPAddressCount = 0;
        public int Organisation2IPAddressCount = 0;
        public int Person2NINumberCount = 0;
        public int Organisation2OrganisationCount = 0;
        public int Person2PassportCount = 0;
        public int Person2PaymentCardCount = 0;
        public int Organisation2PaymentCardCount = 0;
        public int Person2OrganisationCount = 0;
        public int Person2OrganisationCountAll = 0;
        public int Person2PersonCount = 0;
        public int Person2PolicyCount = 0;
        public int Organisation2PolicyCount = 0;
        public int Vehicle2PolicyCount = 0;
        public int Policy2BankAccountCount = 0;
        public int Policy2PaymentCardCount = 0;
        public int Person2TelephoneCount = 0;
        public int Organisation2TelephoneCount = 0;
        public int Address2TelephoneCount = 0;
        public int Incident2VehicleCount = 0;
        public int Vehicle2PersonCount = 0;
        public int Vehicle2OrganisationCount = 0;
        public int Vehicle2VehicleCount = 0;
        public int Person2WebSiteCount = 0;
        public int Organisation2WebSiteCount = 0;
        public int Incident2PersonOrganisationCountAll = 0;
        public int Incident2PersonOrganisationCount = 0;
    }

    [TestClass]
    public partial class ClaimSet_1
    {
        RecordCounts counts = new RecordCounts();

        [TestMethod]
        public void TestDatabasePopulation()
        {
            TestClaim1_1();
            TestClaim1_2();
            TestClaim1_3();
            TestClaim2_1();
            TestClaim3_1();
            TestClaim1_4();
            TestClaim4_1();
            TestClaim1_5();
            TestClaim4_2();
            TestClaim2_2();
            TestClaim1_6();
            TestClaim4_3();
            TestClaim4_4();
            TestClaim1_7();
            TestClaim4_5();
            TestClaim2_3();
            TestClaim3_2();
            TestClaim5_1();
            TestClaim5_2();

        }

        [TestMethod]
        public void TestClaim1_1()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 1-1.xml");

                var claim = batch.Claims[0];

                counts.BankAccountCount = 0;
                counts.AddressCount = 2;
                counts.DrivingLicenseCount = 1;
                counts.EmailCount = 1;
                counts.IncidentCount = 1;
                counts.NINumberCount = 1;
                counts.OrganisationCount = 1;
                counts.PassportCount = 1;
                counts.PaymentCardCount = 1;
                counts.PersonCount = 1;
                counts.PolicyCount = 1;
                counts.TelephoneCount = 4;
                counts.VehicleCount = 1;
                counts.WebSiteCount = 0;

                counts.Person2BankAccountCount = 0;
                counts.Person2BankAccountCountAll = 0;

                counts.Organisation2BankAccountCount = 0;
                counts.Person2AddressCount = 1;
                counts.Organisation2AddressCount = 1;
                counts.Vehicle2AddressCount = 0;
                counts.Address2AddressCount = 0;
                counts.Person2BBPinCount = 0;
                counts.Person2DrivingLicenseCount = 1;
                counts.Person2EmailCount = 1;
                counts.Organisation2EmailCount = 0;
                counts.Incident2FraudRingCount = 0;
                counts.Incident2PersonCount = 1;
                counts.Incident2OrganisationCount = 0;
                counts.Incident2IncidentCount = 0; ;
                counts.Person2IPAddressCount = 0;
                counts.Organisation2IPAddressCount = 0;
                counts.Person2NINumberCount = 1;
                counts.Organisation2OrganisationCount = 0;
                counts.Person2PassportCount = 1;
                counts.Person2PaymentCardCount = 1;
                counts.Organisation2PaymentCardCount = 0;

                counts.Person2OrganisationCount = 1;
                counts.Person2OrganisationCountAll = 1;

                counts.Person2PersonCount = 0;
                counts.Person2PolicyCount = 1;
                counts.Organisation2PolicyCount = 0;
                counts.Vehicle2PolicyCount = 1;
                counts.Policy2BankAccountCount = 0;
                counts.Policy2PaymentCardCount = 1;
                counts.Person2TelephoneCount = 4;
                counts.Organisation2TelephoneCount = 0;
                counts.Address2TelephoneCount = 0;
                counts.Incident2VehicleCount = 1;
                counts.Vehicle2PersonCount = 1;
                counts.Vehicle2OrganisationCount = 0;
                counts.Vehicle2VehicleCount = 0;
                counts.Person2WebSiteCount = 0;
                counts.Organisation2WebSiteCount = 0;

                counts.Incident2PersonOrganisationCountAll = 1;
                counts.Incident2PersonOrganisationCount = 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim1_2()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 1-2.xml");

                var claim = batch.Claims[0];

                counts.OrganisationCount += 1;
                counts.PersonCount += 1;
                counts.Person2AddressCount += 1;
                counts.Incident2PersonCount += 1;
                counts.Organisation2OrganisationCount += 1;

                counts.Person2OrganisationCount += 1;
                counts.Person2OrganisationCountAll += 1;

                counts.Vehicle2PersonCount += 1;

                counts.Incident2PersonOrganisationCountAll += 1;
                counts.Incident2PersonOrganisationCount += 1;


                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim1_3()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 1-3.xml");

                var claim = batch.Claims[0];

                counts.BankAccountCount += 1;
                counts.EmailCount += 1;
                counts.OrganisationCount += 1;
                counts.PersonCount += 1;
                counts.TelephoneCount += 3;
                counts.VehicleCount += 1;
                counts.WebSiteCount += 1;

                counts.Person2BankAccountCount += 1;
                counts.Person2BankAccountCountAll += 1;

                counts.Organisation2EmailCount += 1;
                counts.Incident2PersonCount += 1;

                counts.Person2OrganisationCountAll += 1;
                counts.Person2OrganisationCount += 0;       //one added one archived

                counts.Organisation2TelephoneCount += 3;
                counts.Incident2VehicleCount += 1;
                counts.Vehicle2PersonCount += 1;
                counts.Vehicle2OrganisationCount += 1;
                counts.Organisation2WebSiteCount += 1;

                counts.Incident2PersonOrganisationCountAll += 1;
                counts.Incident2PersonOrganisationCount += 0;   //one added one archived


                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim1_4()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 1-4.xml");

                var claim = batch.Claims[0];

                counts.BankAccountCount += 1;
                counts.Person2BankAccountCount += 1;
                counts.Person2BankAccountCountAll += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }


        [TestMethod]
        public void TestClaim1_5()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 1-5.xml");

                var claim = batch.Claims[0];

                counts.PersonCount += 1;
                counts.Person2PolicyCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim1_6()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 1-6.xml");

                var claim = batch.Claims[0];

                counts.BankAccountCount += 1;
                counts.Person2BankAccountCount += 1;
                counts.Person2BankAccountCountAll += 1;

                counts.Policy2BankAccountCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim1_7()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 1-7.xml");

                var IdCollection = (from rc in ctx.db.RiskClaims
                                    select rc.Id).ToList();

                var claim = batch.Claims[0];

                //counts.BankAccountCount -= 1;    not actually removed
                counts.PaymentCardCount += 1;
                counts.Person2BankAccountCount -= 1;
                counts.Person2BankAccountCountAll += 0;

                counts.Person2PaymentCardCount += 1;
                counts.Policy2BankAccountCount -= 1;
                counts.Policy2PaymentCardCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim2_1()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 2-1.xml");

                var claim = batch.Claims[0];

                counts.BankAccountCount += 1;
                counts.AddressCount += 1;
                counts.IncidentCount += 1;
                counts.OrganisationCount += 1;
                counts.PolicyCount += 1;
                counts.TelephoneCount += 1;
                counts.Organisation2BankAccountCount += 1;
                counts.Organisation2AddressCount += 1;
                counts.Incident2OrganisationCount += 1;
                counts.Organisation2PolicyCount += 1;
                counts.Policy2BankAccountCount += 1;
                counts.Organisation2TelephoneCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim2_2()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 2-2.xml");

                var claim = batch.Claims[0];

                counts.Organisation2PolicyCount -= 1;
                counts.Policy2BankAccountCount -= 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim2_3()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 2-3.xml");

                var claim = batch.Claims[0];

                counts.VehicleCount += 1;
                counts.Vehicle2OrganisationCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim3_1()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 3-1.xml");

                var claim = batch.Claims[0];

                counts.IncidentCount += 1;
                counts.OrganisationCount += 1;
                counts.PaymentCardCount += 1;
                counts.PolicyCount += 1;
                counts.Incident2OrganisationCount += 1;
                counts.Organisation2PaymentCardCount += 1;
                counts.Organisation2PolicyCount += 1;
                counts.Policy2PaymentCardCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim3_2()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 3-2.xml");

                var claim = batch.Claims[0];

                counts.PersonCount += 1;
                counts.VehicleCount += 1;
                counts.Incident2PersonCount += 1;
                counts.Person2PolicyCount += 1;
                counts.Vehicle2PolicyCount += 1;
                counts.Vehicle2PersonCount += 1;

                counts.Incident2VehicleCount += 1;      // not on spreadsheet but should be

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim4_1()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 4-1.xml");

                var claim = batch.Claims[0];

                counts.IncidentCount += 1;
                counts.PersonCount += 1;
                counts.PolicyCount += 1;
                counts.VehicleCount += 1;
                counts.Incident2PersonCount += 1;
                counts.Person2PersonCount += 1;
                counts.Vehicle2PolicyCount += 1;
                counts.Incident2VehicleCount += 1;
                counts.Vehicle2PersonCount += 1;
                counts.Vehicle2VehicleCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim4_2()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 4-2.xml");

                var claim = batch.Claims[0];

                counts.Person2PolicyCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim4_3()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 4-3.xml");

                var claim = batch.Claims[0];

                counts.BankAccountCount += 1;
                counts.Person2BankAccountCount += 1;
                counts.Person2BankAccountCountAll += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim4_4()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 4-4.xml");

                var claim = batch.Claims[0];

                // counts.BankAccountCount -= 1;      account does not get removed
                counts.PaymentCardCount += 1;

                counts.Person2BankAccountCount -= 1;    // the link becomes inactive
                counts.Person2BankAccountCountAll += 0;

                counts.Person2PaymentCardCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim4_5()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 4-5.xml");

                var claim = batch.Claims[0];

                counts.OrganisationCount += 1;
                counts.VehicleCount += 1;

                counts.Person2OrganisationCountAll += 1;
                counts.Person2OrganisationCount += 1;

                counts.Vehicle2PersonCount += 1;      // error
                counts.Vehicle2OrganisationCount += 1;

                counts.Incident2PersonOrganisationCountAll += 1;
                counts.Incident2PersonOrganisationCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim5_1()
        {
            using (CurrentContext ctx = new CurrentContext(End2EndTests.Common.TestClientId, Common.TestUserId, "Test"))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 5-1.xml");

                var claim = batch.Claims[0];

                counts.BankAccountCount += 1;
                counts.IncidentCount += 1;
                counts.OrganisationCount += 1;
                counts.PolicyCount += 1;
                counts.VehicleCount += 1;
                counts.Organisation2BankAccountCount += 1;
                counts.Incident2OrganisationCount += 1;
                counts.Vehicle2PolicyCount += 1;
                counts.Incident2VehicleCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

        [TestMethod]
        public void TestClaim5_2()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 5-2.xml");

                var claim = batch.Claims[0];

                counts.PersonCount += 1;
                counts.Incident2PersonCount += 1;
                counts.Person2PolicyCount += 1;

                UnitTest1.CheckCounts(ctx.db, counts);
                // UnitTest1.CheckClaim(db, claim);
            }
        }

    }
}
