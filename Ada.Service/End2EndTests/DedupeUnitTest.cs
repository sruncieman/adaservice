﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.Common.FileModel;
using MDA.Common.Server;
using System.Linq;
using MDA.Pipeline;


namespace End2EndTests
{
    [TestClass]
    public class DedupeUnitTest
    {
        [TestMethod]
        public void LoadData()
        {
            MotorClaimBatch batchOne = Common.SubmitFileAsUser("External Test 1.XML", 4, 0);

            ChangeAdaRecordStatus();

            MotorClaimBatch batchTwo = Common.SubmitFileAsUser("External Test 1.XML", 4, 0);
        }

        private void ChangeAdaRecordStatus()
        {
            string[] TableNames = 
            {
                "Address",
                "Address2Address",
                "Address2Telephone",
                "BankAccount",
                "BBPin",
                "DrivingLicense",
                "Email",
                "FraudRing",
                "Incident",
                "Incident2FraudRing",
                "Incident2Incident",
                "Incident2Organisation",
                "Incident2OrganisationOutcome",
                "Incident2Person",
                "Incident2PersonOutcome",
                "Incident2Vehicle",
                "IPAddress",
                "KeoghsCase",
                "KeoghsCase2Incident",
                "NINumber",
                "Organisation",
                "Organisation2Address",
                "Organisation2BankAccount",
                "Organisation2Email",
                "Organisation2IPAddress",
                "Organisation2Organisation",
                "Organisation2PaymentCard",
                "Organisation2Policy",
               "Organisation2Telephone",
               "Organisation2WebSite",
               "Passport",
               "PaymentCard",
               "Person",
               "Person2Address",
               "Person2BankAccount",
               "Person2BBPin",
               "Person2DrivingLicense",
               "Person2Email",
               "Person2IPAddress",
               "Person2NINumber",
               "Person2Organisation",
               "Person2Passport",
               "Person2PaymentCard",
               "Person2Person",
               "Person2Policy",
               "Person2Telephone",
               "Person2WebSite",
               "Policy",
               "Policy2BankAccount",
               "Policy2PaymentCard",
               "Telephone",
               "Vehicle",
               "Vehicle2Address",
              "Vehicle2Organisation",
              "Vehicle2Person",
              "Vehicle2Policy",
              "Vehicle2Vehicle",
              "WebSite",
            };

            using (var ctx = new MDA.Common.Server.CurrentContext(0, 0, "System"))
            {
                foreach( string tbl in TableNames)
                {
                    string sql = "UPDATE " + tbl + " SET ADARecordStatus = 10 WHERE ADARecordStatus = 0";

                    ctx.db.Database.ExecuteSqlCommand(sql);
                }
            }
        }

        [TestMethod]
        public void DedupeTest()
        {
            ServiceConfigSettings configSettings = new ServiceConfigSettings();

            //MotorClaimBatch batchOne = Common.SubmitFileAsUser("External Test 1.XML", 0, 0);

            //ChangeAdaRecordStatus();

            //MotorClaimBatch batchTwo = Common.SubmitFileAsUser("External Test 1.XML", 0, 0);

            using (var ctx = new MDA.Common.Server.CurrentContext(0, 0, "System"))
            {
                MDA.Pipeline.Pipeline.Deduplicate(ctx, configSettings);

            }

        }
    }
}
