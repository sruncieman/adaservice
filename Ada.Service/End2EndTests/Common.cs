﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.Common.Enum;
using MDA.DataService;
using MDA.WCF.WebServices;
using MDA.WCF.WebServices.Messages;
using MDA.Common.FileModel;

namespace End2EndTests
{
    public class Common
    {
        public const string PathToXmlFiles = @"..\..\..\End2EndTests\XMLInputFiles\";
        public const int TestClientId = 46;
        public const int TestUserId = 0;
        public const string TestUserName = "keoAdmin@keoghs.co.uk";

        public static MotorClaimBatch SubmitFile(string filePath)
        {
            MDA.WCF.WebServices.ADAWebServices proxy = new MDA.WCF.WebServices.ADAWebServices();

            string xx = Directory.GetCurrentDirectory();

            FileStream fs = File.OpenRead(PathToXmlFiles + filePath);

            fs.Seek(0, SeekOrigin.Begin);

            XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            DataContractSerializer ser = new DataContractSerializer(typeof(MotorClaimBatch));

            MotorClaimBatch batch = (MDA.Common.FileModel.MotorClaimBatch)ser.ReadObject(reader, true);

            string clientBatchReference = Path.GetRandomFileName();

            var x = proxy.ProcessClaimBatch(new ProcessClaimBatchRequest() 
            { 
                ClientId = TestClientId, 
                UserId = TestUserId, 
                BatchEntityType = "Motor", 
                Batch = batch, 
                ClientBatchReference = clientBatchReference, 
                Who = TestUserName 
            });

            return batch;
        }

        public static MobileClaimBatch SubmitMobileFile(string filePath, string batchReference)
        {
            MDA.WCF.WebServices.ADAWebServices proxy = new MDA.WCF.WebServices.ADAWebServices();

            string xx = Directory.GetCurrentDirectory();

            FileStream fs = File.OpenRead(PathToXmlFiles + filePath);

            fs.Seek(0, SeekOrigin.Begin);

            XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            DataContractSerializer ser = new DataContractSerializer(typeof(MobileClaimBatch));

            MobileClaimBatch batch = (MDA.Common.FileModel.MobileClaimBatch)ser.ReadObject(reader, true);

            string clientBatchReference = batchReference;

            var x = proxy.ProcessMobileClaimBatch(new ProcessMobileClaimBatchRequest()
            {
                ClientId = TestClientId,
                UserId = TestUserId,
                BatchEntityType = "Mobile",
                Batch = batch,
                ClientBatchReference = clientBatchReference,
                Who = TestUserName
            });

            return batch;
        }

        public static MotorClaimBatch SubmitFileAsUser(string filePath, int clientId, int userId)
        {
            MDA.WCF.WebServices.ADAWebServices proxy = new MDA.WCF.WebServices.ADAWebServices();

            string xx = Directory.GetCurrentDirectory();

            FileStream fs = File.OpenRead(PathToXmlFiles + filePath);

            fs.Seek(0, SeekOrigin.Begin);

            XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            DataContractSerializer ser = new DataContractSerializer(typeof(MotorClaimBatch));

            MotorClaimBatch batch = (MDA.Common.FileModel.MotorClaimBatch)ser.ReadObject(reader, true);

            string clientBatchReference = Path.GetRandomFileName();

            var x = proxy.ProcessClaimBatch(new ProcessClaimBatchRequest() 
            { 
                ClientId = clientId, 
                UserId = userId, 
                Batch = batch, 
                BatchEntityType = "Motor", 
                ClientBatchReference = clientBatchReference, 
                Who = TestUserName 
            });

            return batch;
        }
    }
}
