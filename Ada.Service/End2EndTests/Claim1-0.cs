﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.WCF.WebServices;
using MDA.Common.FileModel;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Diagnostics;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.DataService;
using MDA.Common.Server;

namespace End2EndTests
{
    [TestClass]
    public class ClaimSet_0
    {

        //Test

        [TestMethod] 
        public void TestDatabasePopulation2()
        {
            TestClaim1_0();
            TestClaim0_1();
            TestClaim0_2();
        }

        [TestMethod]
        public void TestClaim1_0()
        {

            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {
                MotorClaimBatch batch = Common.SubmitFile("Claim 1-0.xml");

                //var IdCollection = (from rc in ctx.db.RiskClaims
                //                    select rc.Id).ToList();


                int currentId = (from rc in ctx.db.RiskClaims
                                 select rc.Id).ToList().Last();

                foreach (MDA.Common.FileModel.MotorClaim claim in batch.Claims)
                {

                    #region Check 3 email records created
                    var emailsCount = (from x in ctx.db.Emails
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(3, emailsCount);
                    #endregion

                    #region Check for 10 telephone records
                    var teleCount = (from x in ctx.db.Telephones
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                     select x).Count();

                    Assert.AreEqual<int>(10, teleCount);
                    #endregion

                    #region Check for 2 Bank Accounts
                    var bankCount = (from x in ctx.db.BankAccounts
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                     select x).Count();

                    Assert.AreEqual<int>(2, bankCount);
                    #endregion

                    #region Check for 2 Payment Cards
                    var cardCount = (from x in ctx.db.PaymentCards
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                     select x).Count();

                    Assert.AreEqual<int>(2, cardCount);
                    #endregion

                    #region Check for 3 Addresses
                    var addressCount = (from x in ctx.db.Addresses
                                        where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                        select x).Count();

                    Assert.AreEqual<int>(3, addressCount);
                    #endregion



                    #region Incident

                    #region Check for 1 incident
                    var incidents = (from x in ctx.db.Incidents
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current && x.Id > 0
                                     select x);

                    Assert.AreEqual<int>(1, incidents.Count());

                    var incident = incidents.First();

                    #endregion

                    #region Check for zero Incident2FraudRing
                    var incFraudCount = (from x in ctx.db.Incident2FraudRing
                                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select x).Count();

                    Assert.AreEqual<int>(0, incFraudCount);
                    #endregion

                    #region check for zero incident 2 incident
                    var inc2IncCount = (from x in ctx.db.Incident2Incident
                                        where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                        select x).Count();

                    Assert.AreEqual<int>(0, inc2IncCount);
                    #endregion

                    #region Check for 2 Incident 2 Organsation
                    var incOrgCount = (from x in ctx.db.Incident2Organisation
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(2, incOrgCount);
                    #endregion

                    #region Check for 1 Incident 2 Person
                    var incPerCount = (from x in ctx.db.Incident2Person
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(1, incPerCount);
                    #endregion

                    #region checked for 1 Incident 2 Vehicle
                    var incVehCount = (from x in ctx.db.Incident2Vehicle
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(1, incVehCount);
                    #endregion


                    #endregion

                    #region Policy

                    #region Check for 1 Policy
                    var polCount = (from x in ctx.db.Policies
                                    where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select x).Count();

                    Assert.AreEqual<int>(1, polCount);
                    #endregion

                    #region Check Policy Fields
                    var policy = (from x in ctx.db.Policies
                                  where x.Id == 1
                                  && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                  select x).FirstOrDefault();

                    UnitTest1.CheckPolicy(claim.Policy, policy);
                    #endregion

                    #region Check for 1 Policy 2 Bank Link
                    var Pol2BankLinks = (from x in ctx.db.Policy2BankAccount
                                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select x);

                    Assert.AreEqual<int>(1, Pol2BankLinks.Count());
                    #endregion

                    #region Check Policy 2 Bank link connects correct 2 records
                    var Pol2PersonBankLink = Pol2BankLinks.First();

                    var PersonBankAccount = (from x in ctx.db.BankAccounts where x.BankName == "Person1Bank" select x).First();

                    Assert.AreEqual<int>(Pol2PersonBankLink.Policy_Id, policy.Id);
                    Assert.AreEqual<int>(Pol2PersonBankLink.BankAccount_Id, PersonBankAccount.Id);
                    #endregion

                    #region Check for 1 Policy 2 Card link
                    var polPayCardLinks = (from x in ctx.db.Policy2PaymentCard
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x);

                    Assert.AreEqual<int>(1, polPayCardLinks.Count());
                    #endregion

                    #region Check Policy 2 Card link connects correct 2 records
                    var Pol2PersonCardLink = polPayCardLinks.First();

                    var PersonCard = (from x in ctx.db.PaymentCards where x.BankName == "Bank1-Person" select x).First();

                    Assert.AreEqual<int>(Pol2PersonCardLink.Policy_Id, policy.Id);
                    Assert.AreEqual<int>(Pol2PersonCardLink.PaymentCard_Id, PersonCard.Id);
                    #endregion

                    #endregion

                    #region Vehicles

                    #region Check for 3 Vehicles
                    var vehCount = (from x in ctx.db.Vehicles
                                    where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select x).Count();

                    Assert.AreEqual<int>(3, vehCount);
                    #endregion

                    foreach (var v in claim.Vehicles)
                    {
                        #region Check vehicle fields
                        var vehicle = (from x in ctx.db.Vehicles
                                       where x.VIN == v.VIN
                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).FirstOrDefault();

                        UnitTest1.CheckVehicle(v, vehicle);
                        #endregion

                        #region Check for zero Vehicle 2 Address links
                        var vehAddCount = (from x in ctx.db.Vehicle2Address
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                        Assert.AreEqual<int>(0, vehAddCount);
                        #endregion

                        #region Check for 2 vehicle to Org links
                        var vehOrgCount = (from x in ctx.db.Vehicle2Organisation
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                        Assert.AreEqual<int>(2, vehOrgCount);
                        #endregion

                        #region Check for 1 Vehicle 2 person link
                        var vehPerCount = (from x in ctx.db.Vehicle2Person
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                        Assert.AreEqual<int>(1, vehPerCount);
                        #endregion

                        #region Check for 1 Vehicle 2 Policy link
                        var vehPolCount = (from x in ctx.db.Vehicle2Policy
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                        Assert.AreEqual<int>(1, vehPolCount);
                        #endregion

                        #region Check for zero vehicle 2 vehicle links
                        var veh2VehCount = (from x in ctx.db.Vehicle2Vehicle
                                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select x).Count();

                        Assert.AreEqual<int>(0, veh2VehCount);
                        #endregion

                    #endregion

                        #region People

                        #region check for correct number people created (1)
                        var peopleCount = (from x in ctx.db.People
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                        Assert.AreEqual<int>(v.People.Count(), peopleCount);
                        #endregion

                        foreach (var p in v.People)
                        {
                            DateTime DoB = p.DateOfBirth.Value.Date;

                            #region Check the Person fields
                            var person = (from x in ctx.db.People
                                          where x.FirstName == p.FirstName && x.LastName == p.LastName && x.DateOfBirth == DoB
                                          && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select x).FirstOrDefault();

                            UnitTest1.CheckPerson(p, person);
                            #endregion

                            #region Check for 1 Person 2 Address links
                            var perAddCount = (from x in ctx.db.Person2Address
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(1, perAddCount);
                            #endregion

                            #region Check only 1 email record exists
                            var emails = (from x in ctx.db.Emails
                                          where x.EmailAddress == p.EmailAddress && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select x);

                            Assert.AreEqual<int>(1, emails.Count());
                            #endregion

                            #region Check email link created
                            var Person2Email = (from x in ctx.db.Person2Email
                                                where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x);

                            Assert.AreEqual<int>(1, Person2Email.Count());

                            Assert.AreEqual<int>(Person2Email.First().Person_Id, person.Id);
                            Assert.AreEqual<int>(Person2Email.First().Email_Id, emails.First().Id);
                            #endregion

                            #region Check Payment Card
                            var peoplePaymentCardCount = (from x in ctx.db.PaymentCards where x.BankName == "Bank1-Person" select x).Count();

                            Assert.AreEqual<int>(1, peoplePaymentCardCount);

                            var paymentCard = (from x in ctx.db.PaymentCards where x.BankName == "Bank1-Person" select x).First();

                            UnitTest1.CheckPaymentCard(p.PaymentCard, paymentCard);

                            var person2CardLinks = from x in ctx.db.Person2PaymentCard where x.Person_Id == person.Id select x;

                            Assert.AreEqual<int>(1, person2CardLinks.Count());

                            var person2CardLink = person2CardLinks.First();

                            Assert.AreEqual<int>(paymentCard.Id, person2CardLink.PaymentCard_Id);
                            Assert.AreEqual<int>(person.Id, person2CardLink.Person_Id);
                            #endregion

                            #region Check BankAccount

                            var peopleAccountCount = (from x in ctx.db.BankAccounts where x.BankName == "Person1Bank" select x).Count();

                            Assert.AreEqual<int>(1, peopleAccountCount);

                            var account = (from x in ctx.db.BankAccounts where x.BankName == "Person1Bank" select x).First();

                            UnitTest1.CheckBankAccount(p.BankAccount, account);

                            var person2AccountLinks = from x in ctx.db.Person2BankAccount where x.Person_Id == person.Id select x;

                            Assert.AreEqual<int>(1, person2AccountLinks.Count());

                            var person2AccountLink = person2AccountLinks.First();

                            Assert.AreEqual<int>(account.Id, person2AccountLink.BankAccount_Id);
                            Assert.AreEqual<int>(person.Id, person2AccountLink.Person_Id);
                            #endregion

                            #region Check 4 telephones for this person
                            var personTelephones = (from x in ctx.db.Person2Telephone
                                                    where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                    select x);

                            Assert.AreEqual<int>(4, personTelephones.Count());

                            var PersonPhone1 = from x in ctx.db.Telephones
                                               join y in ctx.db.Person2Telephone on x.Id equals y.Telephone_Id
                                               where x.TelephoneType_Id == 1 &&
                                                     x.TelephoneNumber == p.LandlineTelephone &&
                                                     y.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x;

                            Assert.AreEqual<int>(1, PersonPhone1.Count());

                            var PersonPhone2 = from x in ctx.db.Telephones
                                               join y in ctx.db.Person2Telephone on x.Id equals y.Telephone_Id
                                               where x.TelephoneType_Id == 2 &&
                                                     x.TelephoneNumber == p.MobileTelephone &&
                                                     y.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x;

                            Assert.AreEqual<int>(1, PersonPhone2.Count());

                            var PersonPhone3 = from x in ctx.db.Telephones
                                               join y in ctx.db.Person2Telephone on x.Id equals y.Telephone_Id
                                               where x.TelephoneType_Id == 0 &&
                                                     x.TelephoneNumber == p.WorkTelephone &&
                                                     y.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x;

                            Assert.AreEqual<int>(0, PersonPhone3.Count());

                            var PersonPhone4 = from x in ctx.db.Telephones
                                               join y in ctx.db.Person2Telephone on x.Id equals y.Telephone_Id
                                               where x.TelephoneType_Id == 0 &&
                                                     x.TelephoneNumber == p.OtherTelephone &&
                                                     y.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x;

                            Assert.AreEqual<int>(0, PersonPhone4.Count());

                            #endregion

                            #region Check Incident 2 person link
                            var i2PersonLinks = (from x in ctx.db.Incident2Person
                                                 where x.Person_Id == person.Id && x.Incident_Id == incident.Id
                                                 && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                 select x);


                            #endregion

                            #region Check Driving Licence

                            var PersonDrivingLicenceCount = (from x in ctx.db.DrivingLicenses
                                                             where x.DriverNumber == "FIR11111111ST1FI"
                                                             && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                             select x);

                            Assert.AreEqual<int>(1, PersonDrivingLicenceCount.Count());

                            var Person2DrivingLicenceLinks = from x in ctx.db.Person2DrivingLicense
                                                             where x.Person_Id == person.Id
                                                             select x;

                            Assert.AreEqual<int>(1, Person2DrivingLicenceLinks.Count());

                            #endregion

                            #region Check Passport

                            var PersonPassportCount = (from x in ctx.db.Passports
                                                       where x.PassportNumber == "111111112"
                                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x);

                            Assert.AreEqual<int>(1, PersonPassportCount.Count());

                            var Person2PassportLinks = from x in ctx.db.Person2Passport
                                                       where x.Person_Id == person.Id
                                                       select x;

                            Assert.AreEqual<int>(1, Person2PassportLinks.Count());

                            #endregion

                            #region Check NI Number

                            var PersonNINumberCount = (from x in ctx.db.NINumbers
                                                       where x.NINumber1 == "FI111111S"
                                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x);

                            Assert.AreEqual<int>(1, PersonNINumberCount.Count());

                            var Person2NINumberLinks = from x in ctx.db.Person2NINumber
                                                       where x.Person_Id == person.Id
                                                       select x;

                            Assert.AreEqual<int>(1, Person2NINumberLinks.Count());

                            #endregion

                            #region Check for 1 person to Org links
                            var perOrgCount = (from x in ctx.db.Person2Organisation
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(1, perOrgCount);
                            #endregion

                            #region Check for 0 person to person links
                            var perPerCount = (from x in ctx.db.Person2Person
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(0, perPerCount);
                            #endregion

                            #region Check for 1 person to policy links
                            var perPolCount = (from x in ctx.db.Person2Policy
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(1, perPolCount);
                            #endregion

                            #region Check for 0 person to web site links
                            var perWebCount = (from x in ctx.db.Person2WebSite
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(0, perWebCount);
                            #endregion


                        #endregion

                            #region Addresses

                            #region Check Address Fields
                            foreach (var a in p.Addresses)
                            {
                                var address = (from x in ctx.db.Addresses
                                               where x.PostCode == a.PostCode && x.BuildingNumber == a.BuildingNumber
                                               && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).FirstOrDefault();

                                UnitTest1.CheckAddress(a, address);
                            }
                            #endregion

                            #region Check for 0 Address 2 Address links
                            var add2AddCount = (from x in ctx.db.Address2Address
                                                where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).Count();

                            Assert.AreEqual<int>(0, add2AddCount);
                            #endregion

                            #region Check for 0 Address 2 Telephone links
                            var add2TelCount = (from x in ctx.db.Address2Telephone
                                                where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).Count();

                            Assert.AreEqual<int>(0, add2TelCount);
                            #endregion

                            #endregion

                            #region Organisation

                            var PersonOrganisationCount = (from x in ctx.db.Organisations
                                                           where x.RegisteredNumber == "11111111"
                                                           && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                           select x);

                            Assert.AreEqual<int>(1, PersonOrganisationCount.Count());

                            #endregion
                        }

                        #region Organisation

                        foreach (var o in claim.Organisations)
                        {

                            #region Check Organisation Fields
                            var organisation = (from x in ctx.db.Organisations
                                                where x.RegisteredNumber == "123456"
                                                && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).FirstOrDefault();

                            UnitTest1.CheckOrganisation(o, organisation);
                            #endregion

                            #region Check 2 Organisations created
                            var oCount = (from x in ctx.db.Organisations
                                          where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select x).Count();

                            Assert.AreEqual<int>(2, oCount);
                            #endregion

                            #region Check for 2 Organisation 2 Address links
                            var o2Address = (from x in ctx.db.Organisation2Address
                                             where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                             select x).Count();

                            Assert.AreEqual<int>(2, o2Address);
                            #endregion

                            #region Check for 1 Organisation 2 Bank Account links
                            var o2BankAccount = (from x in ctx.db.Organisation2BankAccount
                                                 where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                 select x).Count();

                            Assert.AreEqual<int>(1, o2BankAccount);
                            #endregion

                            #region Check for 2 Organisation 2 Email links
                            var o2Email = (from x in ctx.db.Organisation2Email
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                            Assert.AreEqual<int>(2, o2Email);
                            #endregion

                            #region Check for 1 Organisation 2 Organisation links
                            var o2Org = (from x in ctx.db.Organisation2Organisation
                                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select x).Count();

                            Assert.AreEqual<int>(0, o2Org);
                            #endregion

                            #region Check for 1 Organisation 2 Payment Card links
                            var o2PaymentCard = (from x in ctx.db.Organisation2PaymentCard
                                                 where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                 select x).Count();

                            Assert.AreEqual<int>(1, o2PaymentCard);
                            #endregion

                            #region Check for 1 Organisation 2 Policy links
                            var o2Policy = (from x in ctx.db.Organisation2Policy
                                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select x).Count();

                            Assert.AreEqual<int>(0, o2Policy);
                            #endregion

                            #region Check for 6 Organisation 2 Telephone links
                            var o2Telephone = (from x in ctx.db.Organisation2Telephone
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(6, o2Telephone);
                            #endregion

                            #region Check for 1 Organisation 2 Web Site links
                            var o2Website = (from x in ctx.db.Organisation2WebSite
                                             where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                             select x).Count();

                            Assert.AreEqual<int>(1, o2Website);
                            #endregion


                        }

                        #endregion

                    }
                }
            }

        }

        [TestMethod]
        public void TestClaim0_1()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 0-1.xml");

                int currentId = (from rc in ctx.db.RiskClaims
                                 select rc.Id).ToList().Last();

                #region Check for 1 Withdrawn Vehicle 2 Policy Link
                var vehicle2PolicyCount = (from x in ctx.db.Vehicle2Policy
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                           select x).Count();

                Assert.AreEqual<int>(1, vehicle2PolicyCount);
                #endregion

                #region Check for 1 Withdrawn Person 2 Policy Link
                var person2PolicyCount = (from x in ctx.db.Person2Policy
                                          where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                          select x).Count();

                Assert.AreEqual<int>(1, person2PolicyCount);
                #endregion

                #region Check for 1 Withdrawn Organisation 2 Policy Link
                var Organisation2PolicyCount = (from x in ctx.db.Organisation2Policy
                                                where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                                select x).Count();

                Assert.AreEqual<int>(0, Organisation2PolicyCount);
                #endregion

                #region Check for 1 Withdrawn Bank Account 2 Policy Link
                var BankAccount2PolicyCount = (from x in ctx.db.Policy2BankAccount
                                               where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                               select x).Count();

                Assert.AreEqual<int>(1, BankAccount2PolicyCount);
                #endregion

                #region Check for 1 Withdrawn Payment Card 2 Policy Link
                var PaymentCard2PolicyCount = (from x in ctx.db.Policy2PaymentCard
                                               where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                               select x).Count();

                Assert.AreEqual<int>(1, PaymentCard2PolicyCount);
                #endregion

                foreach (MDA.Common.FileModel.MotorClaim claim in batch.Claims)
                {

                    #region Check 3 email records created
                    var emailsCount = (from x in ctx.db.Emails
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(3, emailsCount);
                    #endregion

                    #region Check for 10 telephone records
                    var teleCount = (from x in ctx.db.Telephones
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                     select x).Count();

                    Assert.AreEqual<int>(10, teleCount);
                    #endregion

                    #region Check for 2 Bank Accounts
                    var bankCount = (from x in ctx.db.BankAccounts
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                     select x).Count();

                    Assert.AreEqual<int>(2, bankCount);
                    #endregion

                    #region Check for 2 Payment Cards
                    var cardCount = (from x in ctx.db.PaymentCards
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                     select x).Count();

                    Assert.AreEqual<int>(2, cardCount);
                    #endregion

                    #region Check for 3 Addresses
                    var addressCount = (from x in ctx.db.Addresses
                                        where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                        select x).Count();

                    Assert.AreEqual<int>(3, addressCount);
                    #endregion



                    #region Incident

                    #region Check for 1 incident
                    var incidents = (from x in ctx.db.Incidents
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current && x.Id > 0
                                     select x);

                    Assert.AreEqual<int>(1, incidents.Count());

                    var incident = incidents.First();

                    #endregion

                    #region Check for zero Incident2FraudRing
                    var incFraudCount = (from x in ctx.db.Incident2FraudRing
                                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select x).Count();

                    Assert.AreEqual<int>(0, incFraudCount);
                    #endregion

                    #region check for zero incident 2 incident
                    var inc2IncCount = (from x in ctx.db.Incident2Incident
                                        where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                        select x).Count();

                    Assert.AreEqual<int>(0, inc2IncCount);
                    #endregion

                    #region Check for 1 Incident 2 Organsation
                    var incOrgCount = (from x in ctx.db.Incident2Organisation
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(1, incOrgCount);
                    #endregion

                    #region Check for zero Incident 2 Person
                    var incPerCount = (from x in ctx.db.Incident2Person
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(0, incPerCount);
                    #endregion

                    #region checked for zero Incident 2 Vehicle
                    var incVehCount = (from x in ctx.db.Incident2Vehicle
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(0, incVehCount);
                    #endregion


                    #endregion

                    #region Policy

                    #region Check for 1 Policy
                    var polCount = (from x in ctx.db.Policies
                                    where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select x).Count();

                    Assert.AreEqual<int>(1, polCount);
                    #endregion

                    #region Check Policy Fields
                    var policy = (from x in ctx.db.Policies
                                  where x.Id == 1
                                  && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                  select x).FirstOrDefault();

                    //UnitTest1.CheckPolicy(claim.Policy, policy);
                    #endregion

                    #region Check for zero Policy 2 Bank Link
                    var Pol2BankLinks = (from x in ctx.db.Policy2BankAccount
                                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select x);

                    Assert.AreEqual<int>(0, Pol2BankLinks.Count());
                    #endregion

                    #region Check for zero Policy 2 Card link
                    var polPayCardLinks = (from x in ctx.db.Policy2PaymentCard
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x);

                    Assert.AreEqual<int>(0, polPayCardLinks.Count());
                    #endregion

                    #endregion

                    #region Vehicles

                    #region Check for 3 Vehicles
                    var vehCount = (from x in ctx.db.Vehicles
                                    where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select x).Count();

                    Assert.AreEqual<int>(3, vehCount);
                    #endregion

                    foreach (var v in claim.Vehicles)
                    {
                        #region Check vehicle fields
                        var vehicle = (from x in ctx.db.Vehicles
                                       where x.VIN == v.VIN
                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).FirstOrDefault();

                        UnitTest1.CheckVehicle(v, vehicle);
                        #endregion

                        #region Check for 1 Vehicle 2 Address links
                        var vehAddCount1 = (from x in ctx.db.Vehicle2Address
                                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select x).Count();

                        Assert.AreEqual<int>(0, vehAddCount1);

                        vehAddCount1 = (from x in ctx.db.Vehicle2Address
                                        where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                        select x).Count();

                        Assert.AreEqual<int>(0, vehAddCount1);
                        #endregion

                        #region Check for 3 vehicle to Org links

                        var vehOrgCount = (from x in ctx.db.Vehicle2Organisation
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                        Assert.AreEqual<int>(1, vehOrgCount);

                        vehOrgCount = (from x in ctx.db.Vehicle2Organisation
                                       where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                       select x).Count();

                        Assert.AreEqual<int>(1, vehOrgCount);

                        vehOrgCount = (from x in ctx.db.Vehicle2Organisation
                                       where x.ADARecordStatus == (byte)ADARecordStatus.UpdateRemoved
                                       select x).Count();

                        Assert.AreEqual<int>(0, vehOrgCount);
                        #endregion

                        #region Check for 1 withdrawn Vehicle 2 person link
                        var vehPerCount = (from x in ctx.db.Vehicle2Person
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                           select x).Count();

                        Assert.AreEqual<int>(1, vehPerCount);
                        #endregion

                        #region Check for 1 withdrawn Vehicle 2 Policy link
                        var vehPolCount = (from x in ctx.db.Vehicle2Policy
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                           select x).Count();

                        Assert.AreEqual<int>(1, vehPolCount);
                        #endregion

                        #region Check for zero vehicle 2 vehicle links
                        var veh2VehCount = (from x in ctx.db.Vehicle2Vehicle
                                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select x).Count();

                        Assert.AreEqual<int>(0, veh2VehCount);
                        #endregion

                    #endregion

                        #region People

                        #region check for correct number people created (1)
                        var peopleCount = (from x in ctx.db.People
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                        Assert.AreEqual<int>(v.People.Count(), peopleCount);
                        #endregion

                        foreach (var p in v.People)
                        {
                            DateTime DoB = p.DateOfBirth.Value.Date;

                            #region Check the Person fields
                            var person = (from x in ctx.db.People
                                          where x.FirstName == p.FirstName && x.LastName == p.LastName && x.DateOfBirth == DoB
                                          && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select x).FirstOrDefault();

                            UnitTest1.CheckPerson(p, person);
                            #endregion

                            #region Check for 1 withdraw Person 2 Address links and 0 current
                            var perAddCount = (from x in ctx.db.Person2Address
                                               where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                               select x).Count();

                            Assert.AreEqual<int>(1, perAddCount);

                            perAddCount = (from x in ctx.db.Person2Address
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                            Assert.AreEqual<int>(0, perAddCount);
                            #endregion

                            #region Check 3 current email record exists
                            var emails = (from x in ctx.db.Emails
                                          where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select x);

                            Assert.AreEqual<int>(3, emails.Count());
                            #endregion

                            #region Check 1 withdrawn email links created
                            var Person2Email = (from x in ctx.db.Person2Email
                                                where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x);

                            Assert.AreEqual<int>(0, Person2Email.Count());

                            Person2Email = (from x in ctx.db.Person2Email
                                            where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                            select x);

                            Assert.AreEqual<int>(1, Person2Email.Count());

                            #endregion

                            #region Check Payment Card
                            var person2CardLinks = from x in ctx.db.Person2PaymentCard
                                                   where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                   select x;

                            Assert.AreEqual<int>(0, person2CardLinks.Count());

                            person2CardLinks = from x in ctx.db.Person2PaymentCard
                                               where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                               select x;

                            Assert.AreEqual<int>(1, person2CardLinks.Count());
                            #endregion

                            #region Check BankAccount
                            var person2AccountLinks = from x in ctx.db.Person2BankAccount
                                                      where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                      select x;

                            Assert.AreEqual<int>(0, person2AccountLinks.Count());

                            person2AccountLinks = from x in ctx.db.Person2BankAccount
                                                  where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                                  select x;

                            Assert.AreEqual<int>(1, person2AccountLinks.Count());
                            #endregion

                            #region Check zero telephones for this person
                            var personTelephones = (from x in ctx.db.Person2Telephone
                                                    where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                    select x);

                            Assert.AreEqual<int>(0, personTelephones.Count());

                            personTelephones = (from x in ctx.db.Person2Telephone
                                                where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                                select x);

                            Assert.AreEqual<int>(4, personTelephones.Count());

                            #endregion

                            #region Check Incident 2 person link
                            var i2PersonLinks = (from x in ctx.db.Incident2Person
                                                 where x.Person_Id == person.Id && x.Incident_Id == incident.Id
                                                 && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                 select x);

                            Assert.AreEqual<int>(0, i2PersonLinks.Count());

                            i2PersonLinks = (from x in ctx.db.Incident2Person
                                             where x.Person_Id == person.Id && x.Incident_Id == incident.Id
                                             && x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                             select x);

                            Assert.AreEqual<int>(1, i2PersonLinks.Count());
                            #endregion

                            #region Check Driving Licence

                            var PersonDrivingLicenceCount = (from x in ctx.db.DrivingLicenses
                                                             where x.DriverNumber == "FIR11111111ST1FI"
                                                             && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                             select x);

                            Assert.AreEqual<int>(1, PersonDrivingLicenceCount.Count());

                            var Person2DrivingLicenceLinks = from x in ctx.db.Person2DrivingLicense
                                                             where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                             select x;

                            Assert.AreEqual<int>(0, Person2DrivingLicenceLinks.Count());

                            Person2DrivingLicenceLinks = from x in ctx.db.Person2DrivingLicense
                                                         where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                                         select x;

                            Assert.AreEqual<int>(1, Person2DrivingLicenceLinks.Count());
                            #endregion

                            #region Check Passport

                            var PersonPassportCount = (from x in ctx.db.Passports
                                                       where x.PassportNumber == "111111112"
                                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x);

                            Assert.AreEqual<int>(1, PersonPassportCount.Count());

                            var Person2PassportLinks = from x in ctx.db.Person2Passport
                                                       where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x;

                            Assert.AreEqual<int>(0, Person2PassportLinks.Count());

                            Person2PassportLinks = from x in ctx.db.Person2Passport
                                                   where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                                   select x;

                            Assert.AreEqual<int>(1, Person2PassportLinks.Count());

                            #endregion

                            #region Check NI Number

                            var PersonNINumberCount = (from x in ctx.db.NINumbers
                                                       where x.NINumber1 == "FI111111S"
                                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x);

                            Assert.AreEqual<int>(1, PersonNINumberCount.Count());

                            var Person2NINumberLinks = from x in ctx.db.Person2NINumber
                                                       where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x;

                            Assert.AreEqual<int>(0, Person2NINumberLinks.Count());

                            Person2NINumberLinks = from x in ctx.db.Person2NINumber
                                                   where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                                   select x;

                            Assert.AreEqual<int>(1, Person2NINumberLinks.Count());

                            #endregion

                            #region Check for zero person to Org links
                            var perOrgCount = (from x in ctx.db.Person2Organisation
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(0, perOrgCount);

                            perOrgCount = (from x in ctx.db.Person2Organisation
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                           select x).Count();

                            Assert.AreEqual<int>(1, perOrgCount);
                            #endregion

                            #region Check for zero person to person links
                            var perPerCount = (from x in ctx.db.Person2Person
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(0, perPerCount);
                            #endregion

                            #region Check for zero person to policy links
                            var perPolCount = (from x in ctx.db.Person2Policy
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(0, perPolCount);

                            perPolCount = (from x in ctx.db.Person2Policy
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                           select x).Count();

                            Assert.AreEqual<int>(1, perPolCount);
                            #endregion

                            #region Check for zero person to web site links
                            var perWebCount = (from x in ctx.db.Person2WebSite
                                               select x).Count();

                            Assert.AreEqual<int>(0, perWebCount);
                            #endregion


                        #endregion

                            #region Addresses

                            #region Check Address Fields
                            foreach (var a in p.Addresses)
                            {
                                var address = (from x in ctx.db.Addresses
                                               where x.PostCode == a.PostCode && x.BuildingNumber == a.BuildingNumber
                                               && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).FirstOrDefault();

                                UnitTest1.CheckAddress(a, address);
                            }
                            #endregion

                            #region Check for 0 Address 2 Address links
                            var add2AddCount = (from x in ctx.db.Address2Address
                                                where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).Count();

                            Assert.AreEqual<int>(0, add2AddCount);
                            #endregion

                            #region Check for 0 Address 2 Telephone links
                            var add2TelCount = (from x in ctx.db.Address2Telephone
                                                where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).Count();

                            Assert.AreEqual<int>(0, add2TelCount);
                            #endregion

                            #endregion
                        }

                        #region Organisation

                        foreach (var o in claim.Organisations)
                        {

                            #region Check Organisation Fields
                            var organisation = (from x in ctx.db.Organisations
                                                where x.RegisteredNumber == "123456"
                                                && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).FirstOrDefault();

                            UnitTest1.CheckOrganisation(o, organisation);
                            #endregion

                            #region Check 2 Organisations created
                            var oCount = (from x in ctx.db.Organisations
                                          where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select x).Count();

                            Assert.AreEqual<int>(2, oCount);
                            #endregion

                            #region Check for 2 Organisation 2 Address links
                            // is this correct?

                            var o2Address = (from x in ctx.db.Organisation2Address
                                             where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                             select x).Count();

                            Assert.AreEqual<int>(1, o2Address);

                            o2Address = (from x in ctx.db.Organisation2Address
                                         where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                         select x).Count();

                            Assert.AreEqual<int>(1, o2Address);
                            #endregion

                            #region Check for 1 Organisation 2 Bank Account links
                            var o2BankAccount = (from x in ctx.db.Organisation2BankAccount
                                                 where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                 select x).Count();

                            Assert.AreEqual<int>(1, o2BankAccount);
                            #endregion

                            #region Check for 2 Organisation 2 Email links
                            var o2Email = (from x in ctx.db.Organisation2Email
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                            Assert.AreEqual<int>(1, o2Email);

                            o2Email = (from x in ctx.db.Organisation2Email
                                       where x.ADARecordStatus == (byte)ADARecordStatus.UpdateRemoved    // is this correct
                                       select x).Count();

                            Assert.AreEqual<int>(1, o2Email);
                            #endregion

                            #region Check for 2 Organisation 2 Organisation links
                            var o2Org = (from x in ctx.db.Organisation2Email
                                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select x).Count();

                            Assert.AreEqual<int>(1, o2Org);
                            #endregion

                            #region Check for 1 Organisation 2 Payment Card links
                            var o2PaymentCard = (from x in ctx.db.Organisation2PaymentCard
                                                 where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                 select x).Count();

                            Assert.AreEqual<int>(1, o2PaymentCard);
                            #endregion

                            #region Check for 1 Organisation 2 Policy links
                            var o2Policy = (from x in ctx.db.Organisation2Policy
                                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select x).Count();

                            Assert.AreEqual<int>(0, o2Policy);
                            #endregion

                            #region Check for 3 Organisation 2 Telephone links
                            var o2Telephone = (from x in ctx.db.Organisation2Telephone
                                               where x.Organisation_Id == 1 && x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                               select x).Count();

                            Assert.AreEqual<int>(3, o2Telephone);

                            o2Telephone = (from x in ctx.db.Organisation2Telephone
                                           where x.Organisation_Id == 2 && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                            Assert.AreEqual<int>(3, o2Telephone);
                            #endregion

                            #region Check for 1 Organisation 2 Web Site links
                            var o2Website = (from x in ctx.db.Organisation2WebSite
                                             where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                             select x).Count();

                            Assert.AreEqual<int>(1, o2Website);
                            #endregion


                        }

                        #endregion

                    }


                }
            }
        }

        [TestMethod]
        public void TestClaim0_2()
        {
            using (CurrentContext ctx = new CurrentContext(Common.TestClientId, Common.TestUserId, Common.TestUserName))
            {

                MotorClaimBatch batch = Common.SubmitFile("Claim 0-2.xml");

                int currentId = (from rc in ctx.db.RiskClaims
                                 select rc.Id).ToList().Last();

                #region Check for 1 Withdrawn Vehicle 2 Policy Link
                var vehicle2PolicyCount = (from x in ctx.db.Vehicle2Policy
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                           select x).Count();

                Assert.AreEqual<int>(1, vehicle2PolicyCount);
                #endregion

                #region Check for 1 Deleted Person 2 Policy Link
                var person2PolicyCount = (from x in ctx.db.Person2Policy
                                          where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                          select x).Count();

                Assert.AreEqual<int>(1, person2PolicyCount);
                #endregion

                #region Check for 1 Withdrawn Organisation 2 Policy Link
                var Organisation2PolicyCount = (from x in ctx.db.Organisation2Policy
                                                where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                                select x).Count();

                Assert.AreEqual<int>(0, Organisation2PolicyCount);
                #endregion

                #region Check for 1 Deleted Bank Account 2 Policy Link
                var BankAccount2PolicyCount = (from x in ctx.db.Policy2BankAccount
                                               where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                               select x).Count();

                Assert.AreEqual<int>(1, BankAccount2PolicyCount);
                #endregion

                #region Check for 1 Deleted Payment Card 2 Policy Link
                var PaymentCard2PolicyCount = (from x in ctx.db.Policy2PaymentCard
                                               where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                               select x).Count();

                Assert.AreEqual<int>(1, PaymentCard2PolicyCount);
                #endregion

                foreach (MotorClaim claim in batch.Claims)
                {
                    #region Check 3 email records created
                    var emailsCount = (from x in ctx.db.Emails
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(3, emailsCount);
                    #endregion

                    #region Check for 10 telephone records
                    var teleCount = (from x in ctx.db.Telephones
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                     select x).Count();

                    Assert.AreEqual<int>(10, teleCount);
                    #endregion

                    #region Check for 2 Bank Accounts
                    var bankCount = (from x in ctx.db.BankAccounts
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                     select x).Count();

                    Assert.AreEqual<int>(2, bankCount);
                    #endregion

                    #region Check for 2 Payment Cards
                    var cardCount = (from x in ctx.db.PaymentCards
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                     select x).Count();

                    Assert.AreEqual<int>(2, cardCount);
                    #endregion

                    #region Check for 3 Addresses
                    var addressCount = (from x in ctx.db.Addresses
                                        where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                        select x).Count();

                    Assert.AreEqual<int>(3, addressCount);
                    #endregion



                    #region Incident

                    #region Check for 1 incident
                    var incidents = (from x in ctx.db.Incidents
                                     where x.ADARecordStatus == (byte)ADARecordStatus.Current && x.Id > 0
                                     select x);

                    Assert.AreEqual<int>(1, incidents.Count());

                    var incident = incidents.First();

                    #endregion

                    #region Check for zero Incident2FraudRing
                    var incFraudCount = (from x in ctx.db.Incident2FraudRing
                                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select x).Count();

                    Assert.AreEqual<int>(0, incFraudCount);
                    #endregion

                    #region check for zero incident 2 incident
                    var inc2IncCount = (from x in ctx.db.Incident2Incident
                                        where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                        select x).Count();

                    Assert.AreEqual<int>(0, inc2IncCount);
                    #endregion

                    #region Check for 1 Incident 2 Organsation
                    var incOrgCount = (from x in ctx.db.Incident2Organisation
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(1, incOrgCount);
                    #endregion

                    #region Check for zero Incident 2 Person
                    var incPerCount = (from x in ctx.db.Incident2Person
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(0, incPerCount);
                    #endregion

                    #region checked for zero Incident 2 Vehicle
                    var incVehCount = (from x in ctx.db.Incident2Vehicle
                                       where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).Count();

                    Assert.AreEqual<int>(0, incVehCount);
                    #endregion


                    #endregion

                    #region Policy

                    #region Check for 1 Policy
                    var polCount = (from x in ctx.db.Policies
                                    where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select x).Count();

                    Assert.AreEqual<int>(1, polCount);
                    #endregion

                    #region Check Policy Fields
                    var policy = (from x in ctx.db.Policies
                                  where x.Id == 1
                                  && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                  select x).FirstOrDefault();

                    //UnitTest1.CheckPolicy(claim.Policy, policy);
                    #endregion

                    #region Check for zero Policy 2 Bank Link
                    var Pol2BankLinks = (from x in ctx.db.Policy2BankAccount
                                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select x);

                    Assert.AreEqual<int>(0, Pol2BankLinks.Count());
                    #endregion

                    #region Check for zero Policy 2 Card link
                    var polPayCardLinks = (from x in ctx.db.Policy2PaymentCard
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x);

                    Assert.AreEqual<int>(0, polPayCardLinks.Count());
                    #endregion

                    #endregion

                    #region Vehicles

                    #region Check for 3 Vehicles
                    var vehCount = (from x in ctx.db.Vehicles
                                    where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select x).Count();

                    Assert.AreEqual<int>(3, vehCount);
                    #endregion

                    foreach (var v in claim.Vehicles)
                    {
                        #region Check vehicle fields
                        var vehicle = (from x in ctx.db.Vehicles
                                       where x.VIN == v.VIN
                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).FirstOrDefault();

                        UnitTest1.CheckVehicle(v, vehicle);
                        #endregion

                        #region Check for 1 Vehicle 2 Address links
                        var vehAddCount1 = (from x in ctx.db.Vehicle2Address
                                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select x).Count();

                        Assert.AreEqual<int>(0, vehAddCount1);

                        vehAddCount1 = (from x in ctx.db.Vehicle2Address
                                        where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                        select x).Count();

                        Assert.AreEqual<int>(0, vehAddCount1);
                        #endregion

                        #region Check for 3 vehicle to Org links

                        var vehOrgCount = (from x in ctx.db.Vehicle2Organisation
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                        Assert.AreEqual<int>(1, vehOrgCount);

                        vehOrgCount = (from x in ctx.db.Vehicle2Organisation
                                       where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                       select x).Count();

                        Assert.AreEqual<int>(0, vehOrgCount);

                        vehOrgCount = (from x in ctx.db.Vehicle2Organisation
                                       where x.ADARecordStatus == (byte)ADARecordStatus.UpdateRemoved
                                       select x).Count();

                        Assert.AreEqual<int>(0, vehOrgCount);
                        #endregion

                        #region Check for 1 deleted Vehicle 2 person link
                        var vehPerCount = (from x in ctx.db.Vehicle2Person
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                           select x).Count();

                        Assert.AreEqual<int>(1, vehPerCount);
                        #endregion

                        #region Check for 1 withdrawn Vehicle 2 Policy link
                        var vehPolCount = (from x in ctx.db.Vehicle2Policy
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientWithdrawn
                                           select x).Count();

                        Assert.AreEqual<int>(1, vehPolCount);
                        #endregion

                        #region Check for zero vehicle 2 vehicle links
                        var veh2VehCount = (from x in ctx.db.Vehicle2Vehicle
                                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select x).Count();

                        Assert.AreEqual<int>(0, veh2VehCount);
                        #endregion

                    #endregion

                        #region People

                        #region check for correct number people created (1)
                        var peopleCount = (from x in ctx.db.People
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                        Assert.AreEqual<int>(v.People.Count(), peopleCount);
                        #endregion

                        foreach (var p in v.People)
                        {
                            DateTime DoB = p.DateOfBirth.Value.Date;

                            #region Check the Person fields
                            var person = (from x in ctx.db.People
                                          where x.FirstName == p.FirstName && x.LastName == p.LastName && x.DateOfBirth == DoB
                                          && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select x).FirstOrDefault();

                            UnitTest1.CheckPerson(p, person);
                            #endregion

                            #region Check for 1 deleted Person 2 Address links and 0 current
                            var perAddCount = (from x in ctx.db.Person2Address
                                               where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                               select x).Count();

                            Assert.AreEqual<int>(1, perAddCount);

                            perAddCount = (from x in ctx.db.Person2Address
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                            Assert.AreEqual<int>(0, perAddCount);
                            #endregion

                            #region Check 3 current email record exists
                            var emails = (from x in ctx.db.Emails
                                          where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select x);

                            Assert.AreEqual<int>(3, emails.Count());
                            #endregion

                            #region Check 1 deleted email links created
                            var Person2Email = (from x in ctx.db.Person2Email
                                                where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x);

                            Assert.AreEqual<int>(0, Person2Email.Count());

                            Person2Email = (from x in ctx.db.Person2Email
                                            where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                            select x);

                            Assert.AreEqual<int>(1, Person2Email.Count());

                            #endregion

                            #region Check Payment Card
                            var person2CardLinks = from x in ctx.db.Person2PaymentCard
                                                   where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                   select x;

                            Assert.AreEqual<int>(0, person2CardLinks.Count());

                            person2CardLinks = from x in ctx.db.Person2PaymentCard
                                               where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                               select x;

                            Assert.AreEqual<int>(1, person2CardLinks.Count());
                            #endregion

                            #region Check BankAccount
                            var person2AccountLinks = from x in ctx.db.Person2BankAccount
                                                      where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                      select x;

                            Assert.AreEqual<int>(0, person2AccountLinks.Count());

                            person2AccountLinks = from x in ctx.db.Person2BankAccount
                                                  where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                                  select x;

                            Assert.AreEqual<int>(1, person2AccountLinks.Count());
                            #endregion

                            #region Check zero telephones for this person
                            var personTelephones = (from x in ctx.db.Person2Telephone
                                                    where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                    select x);

                            Assert.AreEqual<int>(0, personTelephones.Count());

                            personTelephones = (from x in ctx.db.Person2Telephone
                                                where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                                select x);

                            Assert.AreEqual<int>(4, personTelephones.Count());

                            #endregion

                            #region Check Incident 2 person link
                            var i2PersonLinks = (from x in ctx.db.Incident2Person
                                                 where x.Person_Id == person.Id && x.Incident_Id == incident.Id
                                                 && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                 select x);

                            Assert.AreEqual<int>(0, i2PersonLinks.Count());

                            i2PersonLinks = (from x in ctx.db.Incident2Person
                                             where x.Person_Id == person.Id && x.Incident_Id == incident.Id
                                             && x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                             select x);

                            Assert.AreEqual<int>(1, i2PersonLinks.Count());
                            #endregion

                            #region Check Driving Licence

                            var PersonDrivingLicenceCount = (from x in ctx.db.DrivingLicenses
                                                             where x.DriverNumber == "FIR11111111ST1FI"
                                                             && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                             select x);

                            Assert.AreEqual<int>(1, PersonDrivingLicenceCount.Count());

                            var Person2DrivingLicenceLinks = from x in ctx.db.Person2DrivingLicense
                                                             where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                             select x;

                            Assert.AreEqual<int>(0, Person2DrivingLicenceLinks.Count());

                            Person2DrivingLicenceLinks = from x in ctx.db.Person2DrivingLicense
                                                         where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                                         select x;

                            Assert.AreEqual<int>(1, Person2DrivingLicenceLinks.Count());
                            #endregion

                            #region Check Passport

                            var PersonPassportCount = (from x in ctx.db.Passports
                                                       where x.PassportNumber == "111111112"
                                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x);

                            Assert.AreEqual<int>(1, PersonPassportCount.Count());

                            var Person2PassportLinks = from x in ctx.db.Person2Passport
                                                       where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x;

                            Assert.AreEqual<int>(0, Person2PassportLinks.Count());

                            Person2PassportLinks = from x in ctx.db.Person2Passport
                                                   where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                                   select x;

                            Assert.AreEqual<int>(1, Person2PassportLinks.Count());

                            #endregion

                            #region Check NI Number

                            var PersonNINumberCount = (from x in ctx.db.NINumbers
                                                       where x.NINumber1 == "FI111111S"
                                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x);

                            Assert.AreEqual<int>(1, PersonNINumberCount.Count());

                            var Person2NINumberLinks = from x in ctx.db.Person2NINumber
                                                       where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                       select x;

                            Assert.AreEqual<int>(0, Person2NINumberLinks.Count());

                            Person2NINumberLinks = from x in ctx.db.Person2NINumber
                                                   where x.Person_Id == person.Id && x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                                   select x;

                            Assert.AreEqual<int>(1, Person2NINumberLinks.Count());

                            #endregion

                            #region Check for zero person to Org links
                            var perOrgCount = (from x in ctx.db.Person2Organisation
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(0, perOrgCount);

                            perOrgCount = (from x in ctx.db.Person2Organisation
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                           select x).Count();

                            Assert.AreEqual<int>(1, perOrgCount);
                            #endregion

                            #region Check for zero person to person links
                            var perPerCount = (from x in ctx.db.Person2Person
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(0, perPerCount);
                            #endregion

                            #region Check for zero person to policy links
                            var perPolCount = (from x in ctx.db.Person2Policy
                                               where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).Count();

                            Assert.AreEqual<int>(0, perPolCount);

                            perPolCount = (from x in ctx.db.Person2Policy
                                           where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                           select x).Count();

                            Assert.AreEqual<int>(1, perPolCount);
                            #endregion

                            #region Check for zero person to web site links
                            var perWebCount = (from x in ctx.db.Person2WebSite
                                               select x).Count();

                            Assert.AreEqual<int>(0, perWebCount);
                            #endregion


                        #endregion

                            #region Addresses

                            #region Check Address Fields
                            foreach (var a in p.Addresses)
                            {
                                var address = (from x in ctx.db.Addresses
                                               where x.PostCode == a.PostCode && x.BuildingNumber == a.BuildingNumber
                                               && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                               select x).FirstOrDefault();

                                UnitTest1.CheckAddress(a, address);
                            }
                            #endregion

                            #region Check for 0 Address 2 Address links
                            var add2AddCount = (from x in ctx.db.Address2Address
                                                where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).Count();

                            Assert.AreEqual<int>(0, add2AddCount);
                            #endregion

                            #region Check for 0 Address 2 Telephone links
                            var add2TelCount = (from x in ctx.db.Address2Telephone
                                                where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).Count();

                            Assert.AreEqual<int>(0, add2TelCount);
                            #endregion

                            #endregion
                        }

                        #region Organisation

                        foreach (var o in claim.Organisations)
                        {

                            #region Check Organisation Fields
                            var organisation = (from x in ctx.db.Organisations
                                                where x.RegisteredNumber == "123456"
                                                && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select x).FirstOrDefault();

                            UnitTest1.CheckOrganisation(o, organisation);
                            #endregion

                            #region Check 2 Organisations created
                            var oCount = (from x in ctx.db.Organisations
                                          where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select x).Count();

                            Assert.AreEqual<int>(2, oCount);
                            #endregion

                            #region Check for 2 Organisation 2 Address links
                            // is this correct?

                            var o2Address = (from x in ctx.db.Organisation2Address
                                             where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                             select x).Count();

                            Assert.AreEqual<int>(1, o2Address);

                            o2Address = (from x in ctx.db.Organisation2Address
                                         where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                         select x).Count();

                            Assert.AreEqual<int>(1, o2Address);
                            #endregion

                            #region Check for 1 Organisation 2 Bank Account links
                            var o2BankAccount = (from x in ctx.db.Organisation2BankAccount
                                                 where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                 select x).Count();

                            Assert.AreEqual<int>(1, o2BankAccount);
                            #endregion

                            #region Check for 2 Organisation 2 Email links
                            var o2Email = (from x in ctx.db.Organisation2Email
                                           where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                            Assert.AreEqual<int>(1, o2Email);

                            o2Email = (from x in ctx.db.Organisation2Email
                                       where x.ADARecordStatus == (byte)ADARecordStatus.UpdateRemoved    // is this correct
                                       select x).Count();

                            Assert.AreEqual<int>(1, o2Email);
                            #endregion

                            #region Check for 2 Organisation 2 Organisation links
                            var o2Org = (from x in ctx.db.Organisation2Email
                                         where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                         select x).Count();

                            Assert.AreEqual<int>(1, o2Org);
                            #endregion

                            #region Check for 1 Organisation 2 Payment Card links
                            var o2PaymentCard = (from x in ctx.db.Organisation2PaymentCard
                                                 where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                 select x).Count();

                            Assert.AreEqual<int>(1, o2PaymentCard);
                            #endregion

                            #region Check for 1 Organisation 2 Policy links
                            var o2Policy = (from x in ctx.db.Organisation2Policy
                                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select x).Count();

                            Assert.AreEqual<int>(0, o2Policy);
                            #endregion

                            #region Check for 3 Organisation 2 Telephone links
                            var o2Telephone = (from x in ctx.db.Organisation2Telephone
                                               where x.Organisation_Id == 1 && x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                               select x).Count();

                            Assert.AreEqual<int>(3, o2Telephone);

                            o2Telephone = (from x in ctx.db.Organisation2Telephone
                                           where x.Organisation_Id == 2 && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select x).Count();

                            Assert.AreEqual<int>(3, o2Telephone);
                            #endregion

                            #region Check for 1 Organisation 2 Web Site links
                            var o2Website = (from x in ctx.db.Organisation2WebSite
                                             where x.ADARecordStatus == (byte)ADARecordStatus.ClientDeleted
                                             select x).Count();

                            Assert.AreEqual<int>(1, o2Website);
                            #endregion


                        }

                        #endregion

                    }


                }
            }
        }

    }
}
