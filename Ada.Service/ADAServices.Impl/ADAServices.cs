﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization;
using MDA.Common;
using MDA.Common.Server;
using MDA.Pipeline;
using System.Configuration;
using MDA.RiskService;
using MDA.RiskService.Model;
using MDA.DAL;
using ADAServices.Interface;
using MDA.ReportingService;
using System.Web.Security;
using Newtonsoft.Json;
using MDA.RiskService.Interface;
using System.Xml;
using System.Net.Mail;


namespace ADAServices.Impl
{
    public class ADAServices : IADAServices
    {
        private string tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];

        public void UploadBatchOfClaimsFile(Stream sourceStream, string batchEntityType, string fileName, string mimeType, int clientId,
            int userId, string clientBatchReference, string who)
        {
            string filePath;
            string clientFolder;
            string extension;

            GetFilePath(fileName, clientId, out filePath, out clientFolder, out extension);

            using (FileStream targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                //read from the input stream in 65000 byte chunks
                const int bufferLen = 65000;
                byte[] buffer = new byte[bufferLen];
                int count = 0;
                while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                {
                    // save to output stream
                    targetStream.Write(buffer, 0, count);
                }
                targetStream.Close();
                sourceStream.Close();
            }

            using (CurrentContext ctx = new CurrentContext(clientId, userId, who))
            {
                ProcessingResults results;

                var riskBatchId = Pipeline.Stage1_ProcessClaimBatchFile(ctx, batchEntityType, filePath, fileName, clientBatchReference, out results);
            }

            bool deleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);

            if (deleteUploadedFiles)
                File.Delete(filePath);
        }

        private void GetFilePath(string fileName, int clientId, out string filePath, out string clientFolder,
            out string extension)
        {
            extension = Path.GetExtension(fileName);

            string fileNameMinusExt = Path.GetFileNameWithoutExtension(fileName); // fileName.Replace(extension, "");

            //extension = GetFileExtension(fileName);

            string tempClientFolder = tempFolder + "\\" + clientId.ToString();

            if (!Directory.Exists(tempClientFolder))
                Directory.CreateDirectory(tempClientFolder);

            clientFolder = tempClientFolder;

            filePath = string.Format(@"{0}\{1}-{2}{3}", tempClientFolder, fileNameMinusExt,
                DateTime.Now.ToString("ddMMyyyyHHmmss"), extension);
        }

        public ProcessClaimBatchResult ProcessClaimBatch(ProcessClaimBatchParam param)
        {
            FileStream writer = null;
            ProcessClaimBatchResult result = new ProcessClaimBatchResult();

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                IRiskServices riskServices = new RiskServices(ctx);

                if (param.Who == null)
                {
                    param.Who = riskServices.GetUserName(param.UserId);
                    ctx.Who = param.Who;
                }

                try
                {
                    string clientName = riskServices.GetRiskClient(param.ClientId).ClientName;

                    string fileName = string.Format(@"{0}.xml", Guid.NewGuid());
                    string filePath = tempFolder + @"\" + clientName + "-" + fileName;

                    writer = new FileStream(filePath, FileMode.Create);

                    // Creates the xml File and serializes claim model to xml to be picked up later.
                    DataContractSerializer ser = new DataContractSerializer(typeof (MDA.Common.FileModel.MotorClaimBatch));
                    ser.WriteObject(writer, param.Batch);

                    writer.Close();

                    ProcessingResults results;

                    var RiskBatchId = Pipeline.Stage1_ProcessClaimBatchFile(ctx, param.BatchEntityType, filePath, fileName, param.ClientBatchReference, out results);

                    result.Results = results;

                    bool deleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);

                    if (deleteUploadedFiles)
                        File.Delete(filePath);
                }

                catch (Exception ex)
                {
                    result.Results.Message.AddError("An error occured processing the file. " + ex.Message);
                }

                finally
                {
                    if (writer != null)
                        writer.Close();

                    //ADATrace.SwitchToDefaultLogFile();
                }

                return result;
            }
        }

        public ProcessMobileClaimBatchResult ProcessMobileClaimBatch(ProcessMobileClaimBatchParam param)
        {
            FileStream writer = null;
            ProcessMobileClaimBatchResult result = new ProcessMobileClaimBatchResult();

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                IRiskServices riskServices = new RiskServices(ctx);

                try
                {
                    string clientName = riskServices.GetRiskClient(param.ClientId).ClientName;

                    string fileName = string.Format(@"{0}.xml", Guid.NewGuid());
                    string filePath = tempFolder + @"\" + clientName + "-" + fileName;

                    writer = new FileStream(filePath, FileMode.Create);

                    DataContractSerializer ser = new DataContractSerializer(typeof(MDA.Common.FileModel.MobileClaimBatch));
                    ser.WriteObject(writer, param.Batch);

                    writer.Close();

                    ProcessingResults results;

                    var RiskBatchId = Pipeline.Stage1_ProcessClaimBatchFile(ctx, param.BatchEntityType, filePath, fileName, param.ClientBatchReference, out results);

                    result.Results = results;

                    bool deleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);

                    if (deleteUploadedFiles)
                        File.Delete(filePath);
                }

                catch (Exception ex)
                {
                    result.Results.Message.AddError("An error occured processing the file. " + ex.Message);
                }

                finally
                {
                    if (writer != null)
                        writer.Close();

                    //ADATrace.SwitchToDefaultLogFile();
                }

                return result;
            }
        }

        public ProcessClaimResult ProcessClaim(ProcessClaimParam param)
        {
            FileStream writer = null;
            ProcessClaimResult result = new ProcessClaimResult();

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                if (param.Who == null)
                {
                    param.Who = riskServices.GetUserName(param.UserId);
                    ctx.Who = param.Who;
                }

                try
                {
                    string clientName = riskServices.GetRiskClient(param.ClientId).ClientName;

                    string fileName = string.Format(@"{0}.xml", Guid.NewGuid());
                    string filePath = tempFolder + @"\" + clientName + "-" + fileName;

                    writer = new FileStream(filePath, FileMode.Create);

                    MDA.Common.FileModel.MotorClaimBatch claimBatch = new MDA.Common.FileModel.MotorClaimBatch();
                    claimBatch.Claims.Add(param.Claim);

                    DataContractSerializer ser = new DataContractSerializer(typeof (MDA.Common.FileModel.MotorClaimBatch));
                    ser.WriteObject(writer, claimBatch);

                    writer.Close();

                    ProcessingResults results;

                    var riskBatchId = Pipeline.Stage1_ProcessClaimBatchFile(ctx, param.BatchEntityType, filePath, fileName, param.ClientBatchReference, out results);

                    result.Results = results;
                    if (riskBatchId != null)
                        result.RiskClaim_Id = riskServices.GetSingleClaimRiskClaimId((int)riskBatchId);

                    bool deleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);

                    if (deleteUploadedFiles)
                        File.Delete(filePath);
                }
                catch (Exception ex)
                {
                    result.Results.Message.AddError("An error occured processing the file. " + ex.Message);
                }
                finally
                {
                    if (writer != null)
                        writer.Close();

                    //ADATrace.SwitchToDefaultLogFile();
                }

                return result;
            }
        }

        public ClaimsForClientResult GetClaimsForClient(ClaimsForClientParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                using (var transaction = ctx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    ClaimsForClientResult res = new ClaimsForClientResult();
                    //res.ClaimsList = new List<ExRiskClaimDetails>();

                    int totalRows = -1;

                    var l = riskServices.GetRiskClaimRunDetailsForClient(param.ClientId, param.TeamId,
                        param.FilterUserId,
                        param.FilterUploadedBy, param.FilterBatchRef, param.FilterClaimNumber, param.FilterStartDate,
                        param.FilterEndDate, param.FilterHighRisk, param.FilterMediumRisk, param.FilterLowRisk,
                        param.FilterKeoghsCFS, param.FilterPreviousVersion, param.FilterError, param.FilterStatusUnrequestedReports,
                        param.FilterStatusAvailableReports, param.FilterStatusPendingReports, param.FilterStatusRead, param.FilterStatusUnread,
                        param.FilterStatusUnreadScoreChanges, param.FilterStatusReserveChange, param.FilterNoReportsRequested, param.FilterLevel1ReportsRequested,
                        param.FilterLevel2ReportsRequested, param.FilterCallbackReportsRequested, param.FilterNotes, param.FilterIntel, param.FilterDecisionIds, param.FilterDecisions, param.SortColumn,
                        param.SortAscending, param.Page, param.PageSize, out totalRows);

                    foreach (var i in l)
                        res.ClaimsList.Add(Translator.Translator.Translate(i));

                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }
            }
        }
    

    //public ClaimsForUserResult GetClaimsForUser(ClaimsForUserParam param)
    //    {
    //        using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
    //        {//TODO -  no user id
    //        try
    //        {

    //                IRiskServices riskServices = new RiskServices(ctx);

    //                ctx.Who = riskServices.GetUserName(param.UserId);

    //            ClaimsForUserResult res = new ClaimsForUserResult();

    //            int totalRows = -1;

    //            var l = riskServices.GetClaimEditDetailsForUser(param.UserId);

    //            //var l = riskServices.GetRiskClaimRunDetailsForClient(param.riskClientId, param.TeamId, param.UserId,
    //            //                    param.FilterUploadedBy, param.FilterBatchRef, param.FilterClaimNumber, param.FilterStartDate,
    //            //                    param.FilterEndDate, param.FilterHighRisk, param.FilterMediumRisk, param.FilterLowRisk, param.FilterNoReportsRequested, param.FilterLevel1ReportsRequested,
    //            //                    param.FilterLevel2ReportsRequested, param.FilterCallbackReportsRequested, param.SortColumn,
    //            //                    param.SortAscending, param.Page, param.PageSize, out totalRows);

    //            foreach (var i in l)
    //                res.ClaimsList.Add(Translator.Translator.Translate(i));

    //            res.TotalRows = totalRows;
                
    //            res.Result = 0;

    //            return res;
    //        }
    //        catch (Exception ex)
    //        {
    //            return new ClaimsForUserResult() { Result = -1, Error = ex.Message };
    //        }
    //    }
    //    }

        //public SingleClaimForUserResult GetSingleClaimForUser(SingleClaimForUserParam param)
        //{
        //    using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
        //    {
        //    try
        //    {

        //            IRiskServices riskServices = new RiskServices(ctx);

        //            ctx.Who = riskServices.GetUserName(param.UserId);

        //        SingleClaimForUserResult res = new SingleClaimForUserResult();

        //        var l = riskServices.GetSingleClaimEditDetailsForUser(param.UserId, param.EditClaimId);

        //        res.Claim = l;

        //        return res;
        //    }

        //    catch (Exception ex)
        //    {
        //        return new SingleClaimForUserResult() { Result = -1, Error = ex.Message };
        //    }
        //}
        //}



        public RegisterNewClientResult RegisterNewClient(RegisterNewClientParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    var existingRiskClient = riskServices.FindRiskClientByName(param.ClientName);

                    if (existingRiskClient != null)
                    {
                        return new RegisterNewClientResult()
                        {
                            Result = -1,
                            ClientId = -1,
                            Error = "Client Name already exists"
                        };
                    }

                    var newRiskClient = riskServices.CreatNewRiskClient(param.ClientName);

                    return new RegisterNewClientResult()
                    {
                        ClientId = newRiskClient.Id,
                        Result = 0,
                        Error = null
                    };
                }
                catch (Exception ex)
                {
                    return new RegisterNewClientResult() { ClientId = -1, Result = -1, Error = ex.Message };
                }
            }
        }

        public RequestLevelOneReportResult RequestLevelOneReport(RequestLevelOneReportParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    int success = riskServices.RequestLevelOneReport(param.RiskClaimId, param.UserId);

                    return new RequestLevelOneReportResult() { Result = success, Error = null };
                }
                catch (Exception ex)
                {
                    return new RequestLevelOneReportResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RequestLevelTwoReportResult RequestLevelTwoReport(RequestLevelTwoReportParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    int success = riskServices.RequestLevelTwoReport(param.RiskClaimId, param.UserId);

                    return new RequestLevelTwoReportResult() { Result = success, Error = null };
                }
                catch (Exception ex)
                {
                    return new RequestLevelTwoReportResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RequestCallbackResult RequestCallback(RequestCallbackParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    int success = riskServices.RequestCallback(param.RiskClaimId, param.UserId);

                    return new RequestCallbackResult() { Result = success, Error = null };
                }
                catch (Exception ex)
                {
                    return new RequestCallbackResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public ClearCallbackRequestResult ClearCallbackRequest(ClearCallbackRequestParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    int success = riskServices.ClearCallbackRequest(param.RiskClaimId, param.UserId);

                    return new ClearCallbackRequestResult() { Result = success, Error = null };
                }
                catch (Exception ex)
                {
                    return new ClearCallbackRequestResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public SetClaimReadStatusResult SetClaimReadStatus(SetClaimReadStatusParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    int CurrentClaimReadStatus;
                    int success = riskServices.SetClaimReadStatus(param.RiskClaimId, param.RiskClaimReadStatus, param.UserId, out CurrentClaimReadStatus);

                    return new SetClaimReadStatusResult() { Result = success, Error = null, CurrentReadStatus = CurrentClaimReadStatus };
                }
                catch (Exception ex)
                {
                    return new SetClaimReadStatusResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public GetBatchStatusResult GetBatchStatus(GetBatchStatusParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    var rb = riskServices.GetRiskBatch(param.ClientId, param.ClientBatchReference);

                    if (rb != null)
                    {
                        ERiskBatch riskBatch = Translator.Translator.Translate(rb);

                        return new GetBatchStatusResult() { RiskBatch = riskBatch, Error = null };
                    }
                    return new GetBatchStatusResult() { RiskBatch = null, Error = "Batch Not Found" };
                }
                catch (Exception ex)
                {
                    return new GetBatchStatusResult() { RiskBatch = null, Error = ex.Message };
                }
            }
        }

        public GetBatchProcessingResultsResult GetBatchProcessingResults(GetBatchProcessingResultsParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    MessageNode root = new MessageNode("Processing Results");

                    var rb = riskServices.GetRiskBatch(param.RiskBatchId);
                    if (rb != null)
                    {
                        var mr = ((rb.MappingResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(rb.MappingResults).Message : null);
                        var vr = ((rb.VerificationResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(rb.VerificationResults).Message : null);

                        if (mr != null)
                        {
                            var n = new MessageNode("Mapping Results");
                            root.AddNode(n);

                            n.AddNode(mr);
                        }

                        if (vr != null)
                        {
                            var n = new MessageNode("Verification Results");
                            root.AddNode(n);

                            n.AddNode(vr);
                        }

                        var rc = riskServices.GetClaimsInBatch(param.RiskBatchId);

                        var nch = new MessageNode("Detailed Claim Results");
                        root.AddNode(nch);

                        foreach (var c in rc)
                        {
                            var nc = new MessageNode("Claim : " + c.ClientClaimRefNumber);
                            nch.AddNode(nc);

                            var cr = ((c.CleansingResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(c.CleansingResults).Message : null);
                            var vrr = ((c.ValidationResults != null) ? JsonConvert.DeserializeObject<ProcessingResults>(c.ValidationResults).Message : null);

                            if (cr != null)
                            {
                                var n = new MessageNode("Cleansing Results");
                                nc.AddNode(n);

                                n.AddNode(cr);
                            }

                            if (vrr != null)
                            {
                                var n = new MessageNode("Validation Results");
                                nc.AddNode(n);

                                n.AddNode(vrr);
                            }
                        }

                        return new GetBatchProcessingResultsResult() { ProcessingResults = root, Error = null };
                    }
                    return new GetBatchProcessingResultsResult() { ProcessingResults = null, Error = "Batch Not Found" };
                }
                catch (Exception ex)
                {
                    return new GetBatchProcessingResultsResult() { ProcessingResults = null, Error = ex.Message };
                }
            }
        }

        public IsClientBatchReferenceUniqueResult IsClientBatchReferenceUnique(IsClientBatchReferenceUniqueParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    var rb = riskServices.GetRiskBatch(param.ClientId, param.ClientBatchReference);

                    if (rb != null)
                    {
                        return new IsClientBatchReferenceUniqueResult() { IsUnique = false, Error = null };
                    }
                    return new IsClientBatchReferenceUniqueResult() { IsUnique = true, Error = null };
                }
                catch (Exception ex)
                {
                    return new IsClientBatchReferenceUniqueResult() { IsUnique = false, Error = ex.Message };
                }
            }
        }


        public IsClientNameUniqueResult IsClientNameUnique(IsClientNameUniqueParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    var rb = riskServices.FindRiskClientByName(param.ClientName);

                    if (rb != null)
                    {
                        return new IsClientNameUniqueResult() { IsUnique = false, Error = null };
                    }
                    return new IsClientNameUniqueResult() { IsUnique = true, Error = null };
                }
                catch (Exception ex)
                {
                    return new IsClientNameUniqueResult() { IsUnique = false, Error = ex.Message };
                }
            }
        }

        public IsPasswordUniqueResult IsPasswordUnique(IsPasswordUniqueParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    string hash = FormsAuthentication.HashPasswordForStoringInConfigFile(param.Password.Trim(), "md5");

                    var rb = riskServices.IsPasswordUnique(param.Username, hash);

                    if (rb == false)
                    {
                        return new IsPasswordUniqueResult() { IsUnique = false, Error = null };
                    }
                    return new IsPasswordUniqueResult() { IsUnique = true, Error = null };
                }
                catch (Exception ex)
                {
                    return new IsPasswordUniqueResult() { IsUnique = false, Error = ex.Message };
                }
            }
        }

        public IsUserNameUniqueResult IsUserNameUnique(IsUserNameUniqueParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    var rb = riskServices.FindRiskUserByName(param.UserName);

                    if (rb != null)
                    {
                        return new IsUserNameUniqueResult() { IsUnique = false, Error = null };
                    }
                    return new IsUserNameUniqueResult() { IsUnique = true, Error = null };
                }
                catch (Exception ex)
                {
                    return new IsUserNameUniqueResult() { IsUnique = false, Error = ex.Message };
                }
            }
        }

        //public Byte[] GetReportBytes(int riskClaimId)
        //{
        //    bool dummy = Convert.ToBoolean(ConfigurationManager.AppSettings["ReturnDummyReports"]);

        //    if (dummy)
        //    {
        //        FileStream fs = new FileStream("c:\\temp\\DummyBatchReport.pdf", FileMode.Open);

        //        return fs;
        //    }
        //    else
        //    {
        //        string path = "/ADALevelOne/" + ConfigurationManager.AppSettings["CurrentADAOneVersion"] + "/ADAOne";

        //        ReportServices rs = new ReportServices();
        //        return rs.RunReport(riskClaimId.ToString(), path);
        //    }
        //}

        public Stream GetLevel1ReportStream(GetLevel1ReportStreamParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReturnDummyReports"]))
                {
                    string path = ConfigurationManager.AppSettings["Level1DummyReportPath"];

                    return new FileStream(path, FileMode.Open);
                }
                else
                {
                    string path = "/ADALevelOne/" + ConfigurationManager.AppSettings["CurrentADAOneVersion"] + "/ADAOne";

                    var r = new RiskServices(ctx).GetLevel1Report(param.RiskClaimId);

                    if (r != null)
                        return new MemoryStream(r);

                    return new MemoryStream(GenerateLevel1Report(new GenerateLevel1ReportParam() { RiskClaimId = param.RiskClaimId }));
                }
            }
        }

        public Stream GetLevel2ReportStream(GetLevel2ReportStreamParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReturnDummyReports"]))
                {
                    string path = ConfigurationManager.AppSettings["Level2DummyReportPath"];

                    return new FileStream(path, FileMode.Open);
                }
                else
                {
                    string path = "/ADALevelTwo/" + ConfigurationManager.AppSettings["CurrentADATwoVersion"] + "/ADATwo";

                    var r = new RiskServices(ctx).GetLevel2Report(param.RiskClaimId);

                    if (r != null)
                        return new MemoryStream(r);

                    return new MemoryStream(GenerateLevel2Report(new GenerateLevel2ReportParam() { RiskClaimId = param.RiskClaimId }));
                }
            }
        }

        public Stream GetBatchReportStream(GetBatchReportStreamParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReturnDummyReports"]))
                {
                    string path = ConfigurationManager.AppSettings["BatchDummyReportPath"];

                    return new FileStream(path, FileMode.Open);
                }
                else
                {
                    var r = new RiskServices(ctx).GetBatchReport(param.RiskBatchId);

                    if (r != null)
                        return new MemoryStream(r);

                    return new MemoryStream(GenerateBatchReport(new GenerateBatchReportParam() { RiskBatchId = param.RiskBatchId }));
                }
            }
        }

        public byte[] GenerateBatchReport(GenerateBatchReportParam param)
        {
            string path = "/ADABatchReport/";

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                RiskBatch rb = riskServices.GetRiskBatch(param.RiskBatchId);
                RiskClient rc = riskServices.GetRiskClient(rb.RiskClient_Id);

                var r = new ReportServices().RunBatchReport(param.RiskBatchId.ToString(), rc.AmberThreshold, rc.RedThreshold, path, "pdf");

                riskServices.SaveBatchReportIntoRiskBatchRecord(param.RiskBatchId, r);

                return r;
            }
        }

        public Stream GetCueReportStream(GetCueReportStreamParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReturnDummyReports"]))
                {
                    string path = ConfigurationManager.AppSettings["BatchDummyReportPath"];

                    return new FileStream(path, FileMode.Open);
                }
                else
                {
                    var r = new RiskServices(ctx).GetRiskReportFromRiskExternalServiceRecord(param.RiskClaimId, 3);

                    if (r != null)
                        return new MemoryStream(r);

                    return null;
                }
            }
        }

        public void GenerateAnyBatchReportsOutstanding(GenerateAnyBatchReportsOutstandingParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                var batches = riskServices.GetRiskBatchesNeedingReport();

                foreach (var rb in batches)
                    GenerateBatchReport(new GenerateBatchReportParam() { RiskBatchId = rb.Id });
            }
        }

        public byte[] GenerateLevel1Report(GenerateLevel1ReportParam param)
        {
            string path = "/ADALevel1Report/";

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                RiskClaim rc = riskServices.GetRiskClaim(param.RiskClaimId);
                //RiskClient rc = riskServices.GetClientRecord(rb.RiskClient_Id);

                var r = new ReportServices().RunReport(param.RiskClaimId.ToString(), path);

                riskServices.SaveLevel1ReportIntoRiskClaimRecord(param.RiskClaimId, r);

                return r;
            }
        }


        public byte[] GenerateLevel2Report(GenerateLevel2ReportParam param)
        {
            string path = "/ADALevel2Report/";

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                RiskClaim rc = riskServices.GetRiskClaim(param.RiskClaimId);

                var r = new ReportServices().RunReport(param.RiskClaimId.ToString(), path);

                riskServices.SaveLevel2ReportIntoRiskClaimRecord(param.RiskClaimId, r);

                return r;
            }
        }

        #region File Editor Mehods
        public BatchListResult GetClientBatchList(BatchListParam param)
        {
            //string[] batchStatus = { "Bad", "Loading", "Loaded" };

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                IRiskServices riskServices = new RiskServices(ctx);

                var listBatch = riskServices.GetBatchesForClient(param.ClientId);

                var newBatchList = new List<EBatchDetails>();

                foreach (var batchRecord in listBatch)
                {
                    EBatchDetails batch = new EBatchDetails()
                    {
                        BatchId             = batchRecord.Id,

                        BatchStatusId       = batchRecord.BatchStatus,

                        Reference           = batchRecord.BatchReference,

                        Status              = Translator.Translator.BatchStatusToText(batchRecord.BatchStatus),

                        UploadedBy          = batchRecord.CreatedBy,
                        UploadedDate        = batchRecord.CreatedDate,
                        TotalClaimsReceived = batchRecord.TotalClaimsReceived,
                        TotalNewClaims      = batchRecord.TotalNewClaims,
                        TotalModifiedClaims = batchRecord.TotalModifiedClaims,
                        TotalClaimsLoaded   = batchRecord.TotalClaimsLoaded,
                        TotalClaimsInError  = batchRecord.TotalClaimsInError,
                        TotalClaimsSkipped  = batchRecord.TotalClaimsSkipped,
                        LoadingDuration     = batchRecord.LoadingDurationInMs
                    };

                    newBatchList.Add(batch);
                }

                return new BatchListResult() { BatchList = newBatchList };
            }
        }

        public ClaimsInBatchResult GetClaimsInBatch(ClaimsInBatchParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                ClaimsInBatchResult response = new ClaimsInBatchResult();
                response.ClaimsList = new List<ERiskClaim>();

                var l = riskServices.GetClaimsInBatch(param.BatchId);

                foreach (var i in l)
                {
                    var latestScoreRun = riskServices.GetLatestRiskClaimRun(i.Id);

                    int latestScore = (latestScoreRun == null) ? 0 : latestScoreRun.TotalScore;

                    response.ClaimsList.Add(Translator.Translator.Translate(i, latestScore));
                }
                return response;
            }
        }

        public GetClaimScoreDataResult GetClaimScoreData(GetClaimScoreDataParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                RiskClaimRun rcr = riskServices.GetLatestRiskClaimRun(param.RiskClaimId);

                MessageNode scoreNodes = riskServices.GetScoresAsNodeTree(rcr);

                return new GetClaimScoreDataResult()
                {
                    RiskClaimRun = Translator.Translator.Translate(rcr),
                    Error = "",
                    ScoreNodes = scoreNodes
                };
            }
        }

        #endregion

        public GetRiskUserInfoResult GetRiskUserInfo(GetRiskUserInfoParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                var rc = riskServices.GetUserDetails(param.UserId, param.ClientId);

                return new GetRiskUserInfoResult()
                {
                    UserInfo = Translator.Translator.Translate(rc)
                };
            }
        }

        #region membership

        public int GetPasswordFailuresSinceLastSuccess(GetPasswordFailuresSinceLastSuccessParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                return riskServices.GetPasswordFailuresSinceLastSuccess(param.UserName);
            }
        }

        public bool IsUserInRole(IsUserInRoleParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                return riskServices.IsUserInRole(param.UserName, param.RoleName);
            }
        }

        public string[] GetRolesForUser(GetRolesForUserParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                return riskServices.GetRolesForUser(param.UserName);
            }
        }

        public bool ValidateUser(ValidateUserParam param)
        {
            if (string.IsNullOrEmpty(param.Password.Trim()) || string.IsNullOrEmpty(param.UserName.Trim())) return false; 
            
            string hash = FormsAuthentication.HashPasswordForStoringInConfigFile(param.Password.Trim(), "md5");

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                IRiskServices riskServices = new RiskServices(ctx);

                return riskServices.ValidateUser(param.UserName, hash, param.IPAddress);
            }
        }

        public void CreateUser(CreateUserParam param)
        {
            string hash = FormsAuthentication.HashPasswordForStoringInConfigFile(param.Password.Trim(), "md5");

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.CreateUser(param.UserName, hash, param.Email, param.FirstName, param.LastName, 
                                        param.TelephoneNumber, param.ClientId, param.RoleId, param.TeamId,param.Status,param.Autoloader, param.UserTypeId);
            }
        }

        public void CreateClient(CreateClientParam param)
        {

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.CreateClient(param.ClientName, param.StatusId, param.FileExtensions, param.AmberThreshold, param.RedThreshold, param.InsurersClientsId);
            }
        }

        public void CreateRiskWord(CreateRiskWordParam param)
        {

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.CreateRiskWord(param.RiskClientId, param.TableName, param.FieldName, param.LookupWord, param.ReplacementWord, param.ReplaceWholeString, param.DateAdded, param.SearchType);
            }
        }

        public void CreateRiskDefaultData(CreateRiskDefaultDataParam param)
        {

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.CreateRiskDefaultData(param.ConfigurationValue, param.IsActive, param.RiskClient_Id, param.RiskConfigurationDescriptionId);
            }
        }

        public void CreateRiskNoteDecision(CreateRiskNoteDecisionParam param)
        {

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.CreateRiskNoteDecision(param.Decision, param.IsActive, param.RiskClient_Id);
            }
        }

        public void DeleteRiskDefaultData(DeleteRiskDefaultDataParam param)
        {

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.DeleteRiskDefaultData(param.Id);
            }
        }

        public void DeleteRiskNoteDecision(DeleteRiskNoteDecisionParam param)
        {

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.DeleteRiskNoteDecision(param.Id);
            }
        }

        public void UpdateClient(UpdateClientParam param)
        {

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.UpdateClient(param.Id ,param.ClientName, param.StatusId, param.FileExtensions, param.AmberThreshold, param.RedThreshold, param.InsurersClientsId, param.RecordReserveChanges, param.BatchPriority);
            }
        }

        public void UpdateUser(UpdateUserParam param)
        {
            string hash = FormsAuthentication.HashPasswordForStoringInConfigFile(param.Password.Trim(), "md5");

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.UpdateUser(param.UserName, hash, param.Email, param.FirstName, param.LastName,
                                        param.TelephoneNumber, param.ClientId, param.RoleId, param.TeamId, param.Status, param.Autoloader, param.UserTypeId);
            }
        }

        public void CreateRiskUser2Client(CreateRiskUser2ClientParam param)
        {

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.CreateRiskUser2Client(param.SelectedClientId, param.SelectedUserId);
            }
        }

        public void DeleteRiskUser2Client(DeleteRiskUser2ClientParam param)
        {

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                riskServices.DeleteRiskUser2Client(param.SelectedClientId, param.SelectedUserId);
            }
        }

        public bool ChangePassword(ChangePasswordParam param)
        {
            ValidateUserParam usr = new ValidateUserParam()
            {
                UserName = param.UserName,
                ClientId = param.ClientId,
                Password = param.OldPassword,
                UserId = param.UserId,
                Who = param.Who
            };

            if (string.IsNullOrEmpty(param.NewPassword.Trim()) || param.UserId < 0) 
                return false;

            string newHashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(param.NewPassword.Trim(), "md5");

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);



                return riskServices.ChangePassword(param.UserName, newHashPassword);
            }
        }

        public int GetUserId(GetUserIdParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                return riskServices.GetUserId(param.UserName);
            }
        }


        #endregion

        #region WebSite Methods

        public RiskClientsResult GetRiskClients(RiskClientsParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskClientsResult res = new RiskClientsResult();
                    res.ClientList = new List<ERiskClient>();

                    //RiskClient client = riskServices.GetRiskClient(param.ClientId);

                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    int totalRows = -1;

                    //ctx.SourceRef = "";

                    //All clients
                    if (param.FilterClientId == -1)
                    {
                        var l = riskServices.GetAllRiskClients();

                        foreach (var i in l)
                            res.ClientList.Add(Translator.Translator.Translate(i));
                    }
                    else // Individual client
                    {
                        res.ClientList.Add(Translator.Translator.Translate(riskServices.GetRiskClient(param.ClientId)));
                    }

                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new RiskClientsResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RiskWordsResult GetRiskWords(RiskWordsParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskWordsResult res = new RiskWordsResult();
                    res.WordList = new List<ERiskWord>();

                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    int totalRows = -1;

                    //ctx.SourceRef = "";


                    var l = riskServices.GetAllRiskWords();

                    foreach (var i in l)
                        res.WordList.Add(Translator.Translator.Translate(i));
                    


                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new RiskWordsResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RiskNotesResult GetRiskNotes(RiskNotesParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskNotesResult res = new RiskNotesResult();
                    res.NoteList = new List<ERiskNote>();

                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    int totalRows = -1;

                    //ctx.SourceRef = "";


                    var l = riskServices.GetAllRiskNotes(param.FilterRiskCliamId, param.UserId);

                    foreach (var i in l)
                        res.NoteList.Add(i);



                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new RiskNotesResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public GetRiskNoteResult GetRiskNote(GetRiskNoteParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    GetRiskNoteResult res = new GetRiskNoteResult();
                    
                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    int totalRows = -1;

                    //ctx.SourceRef = "";


                    res.RiskNote = riskServices.GetRiskNote(param.FilterRiskNoteId);

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new GetRiskNoteResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RiskNoteDecisionsResult GetRiskNoteDecisions(RiskNoteDecisionsParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskNoteDecisionsResult res = new RiskNoteDecisionsResult();
                    res.DecisionList = new List<ERiskNoteDecision>();

                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    int totalRows = -1;

                    //ctx.SourceRef = "";


                    var l = riskServices.GetAllRiskNoteDecisions(param.FilterRiskClientId);

                    foreach (var i in l)
                        res.DecisionList.Add(Translator.Translator.Translate(i));



                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new RiskNoteDecisionsResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RiskWordDeleteResult DeleteRiskWord(RiskWordDeleteParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    RiskWordDeleteResult res = new RiskWordDeleteResult();

                    IRiskServices riskServices = new RiskServices(ctx);

                    riskServices.DeleteRiskWord(param.RiskWordId);

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    
                    return new RiskWordDeleteResult() { Result = -1, Error = ex.Message };
                }

            }

        }

        public RiskNoteCreateResult CreateRiskNote(RiskNoteCreateParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    RiskNoteCreateResult res = new RiskNoteCreateResult();

                    IRiskServices riskServices = new RiskServices(ctx);
                                       

                    riskServices.CreateRiskNote(param.ClientId, param.CreatedBy, param.CreatedDate, param.Decision_Id, param.Deleted, param.NoteDesc, param.RiskClaim_Id, param.UserId, param.Visibilty, param.UserTypeId, param.OriginalFile, param.FileName, param.FileType);

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {

                    return new RiskNoteCreateResult() { Result = -1, Error = ex.Message };
                }

            }

        }

        public RiskNoteUpdateResult UpdateRiskNote(RiskNoteUpdateParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    RiskNoteUpdateResult res = new RiskNoteUpdateResult();

                    IRiskServices riskServices = new RiskServices(ctx);


                    riskServices.UpdateRiskNote(param.RiskNoteId, param.Decision_Id, param.Deleted, param.NoteDesc, param.Visibilty,param.UserTypeId, param.UpdatedBy, param.UpdatedDate);

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {

                    return new RiskNoteUpdateResult() { Result = -1, Error = ex.Message };
                }

            }

        }

        public RiskDefaultDataResult GetRiskDefaultData(RiskDefaultDataParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskDefaultDataResult res = new RiskDefaultDataResult();
                    res.DefaultDataList = new List<ERiskDefaultData>();

                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    int totalRows = -1;

                    //ctx.SourceRef = "";


                    var l = riskServices.GetRiskDefaultData();

                    foreach (var i in l)
                        res.DefaultDataList.Add(Translator.Translator.Translate(i));



                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new RiskDefaultDataResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public WebsiteSaveTemplateFunctionsResult SaveTemplateFunctions(WebsiteSaveTemplateFunctionsParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    riskServices.SaveTemplateFunctions(param.Who, param.Template_Id, 1, param.TemplateFunctionsJson, param.ListTemplateFunctionsJson);


                    WebsiteSaveTemplateFunctionsResult res = new WebsiteSaveTemplateFunctionsResult();

                    

                    return res;
                }
                catch (Exception ex)
                {
                    return new WebsiteSaveTemplateFunctionsResult() { Result = -1, Error = ex.Message };
                }
            }
        }
        
        public WebsiteLoadTemplateFunctionsResult LoadTemplateFunctions(WebsiteLoadTemplateFunctionsParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    WebsiteLoadTemplateFunctionsResult res = new WebsiteLoadTemplateFunctionsResult();

                    res.TemplateFunctionsJson = riskServices.LoadTemplateFunctions(param.Who, param.Template_Id, param.ClientId, param.UserId);

                    return res;
                }
                catch (Exception ex)
                {
                    return new WebsiteLoadTemplateFunctionsResult() { Result = -1, Error = ex.Message };
                }
            }
        }
        
        public GetAssignedRiskClientsForUserResult GetAssignedRiskClientsForUser(GetAssignedRiskClientsForUserParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    GetAssignedRiskClientsForUserResult res = new GetAssignedRiskClientsForUserResult();

                    res.ClientList = new List<ERiskClient>();


                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    int totalRows = -1;

                    var l = riskServices.GetAssignedRiskClientsForUser(param.FilterUserId);

                    foreach (var i in l)
                        res.ClientList.Add(Translator.Translator.Translate(i));

                    //res.ClientList.Add(Translator.Translator.Translate(riskServices.GetAssignedRiskClientsForUser(param.UserId)));

                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new GetAssignedRiskClientsForUserResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RiskRolesResult GetRiskRoles(RiskRolesParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskRolesResult res = new RiskRolesResult();
                    res.RoleList = new List<ERiskRole>();

                    //List<RiskRole> roles = riskServices.GetRoles();

                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    ctx.SourceRef = "";

                    var l = riskServices.GetRoles();

                    foreach (var i in l)
                        res.RoleList.Add(Translator.Translator.Translate(i));


                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new RiskRolesResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public InsurersClientsResult GetInsurersClients(InsurersClientsParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    InsurersClientsResult res = new InsurersClientsResult();
                    res.ClientList = new List<EInsurersClient>();

                    //List<RiskRole> roles = riskServices.GetRoles();

                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    ctx.SourceRef = "";

                    var l = riskServices.GetAllInsurersClients();

                    foreach (var i in l)
                        res.ClientList.Add(Translator.Translator.Translate(i));


                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new InsurersClientsResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RiskUsersResult GetRiskUsers(RiskUsersParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskUsersResult res = new RiskUsersResult();
                    res.UsersList = new List<ERiskUser>();

                    //RiskClient client = riskServices.GetRiskClient(param.ClientId);

                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    //ctx.SourceRef = "";

                    res.UsersList.AddRange(riskServices.GetAllRiskUsers());
                    
                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new RiskUsersResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RiskUsersLockedResult GetRiskUsersLocked(RiskUsersLockedParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskUsersLockedResult res = new RiskUsersLockedResult();

                    res.LockedUserList = riskServices.GetLockedUsers(param.ClientId, param.Who, param.UserId);

                    return res;
                }

                catch (Exception ex)
                {
                    return new RiskUsersLockedResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public RiskUserUnlockResult UnlockRiskUser(RiskUserUnlockParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskUserUnlockResult res = new RiskUserUnlockResult();

                    int resultId = riskServices.UnlockRiskUser(param.ClientId, param.Who, param.UserId, param.UnlockUserId);

                    return res;
                }

                catch (Exception ex)
                {
                    return new RiskUserUnlockResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public GeneratePasswordResetTokenResult GeneratePasswordResetToken(GeneratePasswordResetTokenParam param)
        {
            using (CurrentContext ctx = new CurrentContext(0, 0, param.UserName))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);
                    
                    GeneratePasswordResetTokenResult res = new GeneratePasswordResetTokenResult();

                    res.Token = param.Token;
                    res.Result = riskServices.GeneratePasswordResetToken(param.UserName, param.Token, param.TokenExpiration);

                    string messageText = string.Format("We received a request to reset your password for your ADA account: {0}", param.UserName);


                    //string resetLink = Url.Action("ResetPassword", "Account", new { rt = token }, "http");

                    string linkText = "<br/><br/>Click the <a href=\"" + param.ResetLink + "\">link</a> to set a new password<br/><br/>If you didnt request a password reset please ignore.<br/>This link will expire in 24 hours.";

                    string text = "<html> <head> </head>" +
                    " <body style= \" font-size:12px; font-family: Montserrat, sans-serif\">" +
                    messageText + linkText +
                    "</body></html>";

                    string subject = "Reset your password for ADA";
                    string body = text;
                    string from = "donotreply@ada.keoghs.co.uk";


                    MailMessage message = new MailMessage(from, param.UserName);
                    message.Subject = subject;
                    message.Body = body;
                    message.IsBodyHtml = true;
                    SmtpClient client = new SmtpClient();
                    client.Port = 25;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Host = ConfigurationManager.AppSettings["SmtpServer"];


                    // Attempt to send the email
                    try
                    {
                        client.Send(message);
                    }
                    catch (Exception e)
                    {
                        //ModelState.AddModelError("", "Issue sending email: " + e.Message);
                    }

                    return res;
                }

                catch (Exception ex)
                {
                    return new GeneratePasswordResetTokenResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public ValidatePasswordResetTokenResult ValidatePasswordResetToken(ValidatePasswordResetTokenParam param)
        {
            using (CurrentContext ctx = new CurrentContext(0, 0, ""))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ValidatePasswordResetTokenResult res = new ValidatePasswordResetTokenResult();

                    res.UserName = riskServices.ValidatePasswordResetToken(param.Token);

                    if (res.UserName == null)
                    {
                        res.Result = 0;
                    }
                    else
                    {
                        res.Result = 1;  // Valid
                    }

                    return res;
                }

                catch (Exception ex)
                {
                    return new ValidatePasswordResetTokenResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public WebsiteBatchListResult GetBatchList(WebsiteBatchListParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    WebsiteBatchListResult res = new WebsiteBatchListResult();
                    res.BatchList = new List<EWebsiteBatchDetails>();
                    //

                    int totalRows = -1;

                    var l = riskServices.GetBatchesForClient(param.FilterClientId, param.TeamId, param.FilterUserId, param.SortColumn, param.SortAscending, param.Page, param.PageSize, out totalRows);

                    foreach (var i in l)
                        res.BatchList.Add(Translator.Translator.Translate(i));


                    //Check if any loaded batches are being scored

                    foreach (var item in res.BatchList)
                    {
                        if (item.BatchStatusId == 5)
                            item.BatchStatusAsString = riskServices.CheckScoring(item.BatchId);
                    }


                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;

                }
                catch (Exception ex)
                {
                    return new WebsiteBatchListResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public WebSearchResultsResult FindSearchResultDetails(WebSearchResultsParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    WebSearchResultsResult res = new WebSearchResultsResult();

                    res.SearchResultsList = new List<Interface.ESearchDetails>();

                    int totalRows = -1;

                    List<MDA.RiskService.Model.ESearchDetails> l = new List<MDA.RiskService.Model.ESearchDetails>();
                    
                    l = riskServices.GetWebSearchResults(param.ClientId, param.UserId, param.Who, param.FilterFirstName, param.FilterSurname, param.FilterDob, param.FilterAddressLine, param.FilterAddressPostcode, param.FilterVehicleRegNumber,  param.Page, param.PageSize, out totalRows);

                    foreach (var i in l)
                        res.SearchResultsList.Add(Translator.Translator.Translate(i));

                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }

                catch (Exception ex)
                {
                    return new WebSearchResultsResult() { Result = -1, Error = ex.Message };
                }
            }
        }
        
        public WebsiteSingleClaimsListResult GetSingleClaims(WebsiteSingleClaimsListParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    WebsiteSingleClaimsListResult res = new WebsiteSingleClaimsListResult();
                    res.WebsiteSingleClaimsList = new List<EWebsiteSingleClaimsDetails>();

                    int totalRows = -1;

                    var l = riskServices.GetSingleClaimsForClient(param.ClientId, param.TeamId, param.UserId,
                                                                  param.SortColumn, param.SortAscending, param.Page,
                                                                  param.PageSize, out totalRows);


                    foreach (var i in l)
                        res.WebsiteSingleClaimsList.Add(Translator.Translator.Translate(i));

                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }

                catch (Exception ex)
                {
                    return new WebsiteSingleClaimsListResult() { Result = -1, Error = ex.Message };
                }
            }
        }
        
        public WebsiteFetchSingleClaimDetailResult FetchSingleClaimDetail(WebsiteFetchSingleClaimDetailParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    WebsiteFetchSingleClaimDetailResult res = new WebsiteFetchSingleClaimDetailResult();

                    var singleClaim = riskServices.FetchSingleClaimDetail(param.ClientId, param.UserId,
                        param.EditClaimId);

                    res.claimDetail = Translator.Translator.Translate(singleClaim);
                   
                    return res;

                }

                catch (Exception ex)
                {
                    return new WebsiteFetchSingleClaimDetailResult() {Result = -1, Error = ex.Message};
                }
            }
        }

        public WebsiteSaveSingleClaimResult SaveSingleClaim(WebsiteSaveSingleClaimParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    int editClaimId = -1;

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    WebsiteSaveSingleClaimResult res = new WebsiteSaveSingleClaimResult();

                    riskServices.SaveSingleClaim(param.EditClaimId, param.ClaimToSave, param.ClaimType,
                                                 param.ClientId, param.UserId, param.IncidentDate, param.ModifiedBy,
                                                 param.ModifiedDate, param.RiskClaimNumber, param.Status, out editClaimId);

                    res.EditClaimId = editClaimId;

                    return res;

                }

                catch (Exception ex)
                {
                    return new WebsiteSaveSingleClaimResult() { Result = -1, Error = ex.Message };
                }
            }
        }
        
        public WebsiteGetTotalSingleClaimsForClientResult GetTotalSingleClaimForClient(WebsiteGetTotalSingleClaimsForClientParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    int totalRows = -1;

                    IRiskServices riskServices = new RiskServices(ctx);

                    //ctx.Who = riskServices.GetUserName(param.UserId);

                    WebsiteGetTotalSingleClaimsForClientResult res = new WebsiteGetTotalSingleClaimsForClientResult();

                    riskServices.GetTotalSingleClaimsForClient(param.ClientId, out totalRows);

                    res.TotalRows = totalRows;

                    return res;

                }

                catch (Exception ex)
                {
                    return new WebsiteGetTotalSingleClaimsForClientResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public WebsiteDeleteSingleClaimResult DeleteSingleClaim(WebsiteDeleteSingleClaimParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    WebsiteDeleteSingleClaimResult res = new WebsiteDeleteSingleClaimResult();

                    riskServices.DeleteSingleClaim(param.ClientId, param.UserId, param.EditClaimId);

                    //res.TotalRows = totalRows;

                    return res;

                }

                catch (Exception ex)
                {
                    return new WebsiteDeleteSingleClaimResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public WebsiteFilterBatchNumbersResult FilterBatchNumbers(WebsiteFilterBatchNumbersParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    WebsiteFilterBatchNumbersResult res = new WebsiteFilterBatchNumbersResult();

                    List<string> batchNumbers = riskServices.GetFilteredClientBatchRefs(param.Role, param.ClientId, param.UserId, param.SelectedClientId);

                    res.BatchNumbersList.AddRange(batchNumbers);

                    //List<ExBatchNumbers> batches = new List<ExBatchNumbers>();

                    //foreach (var eRiskBatchNumbers in batchNumbers)
                    //    batches.Add(new ExBatchNumbers {BatchNumber = eRiskBatchNumbers.BatchNumber});

                    //res.BatchNumbersList = batches;

                    return res;
                }

                catch (Exception ex)
                {
                    return new WebsiteFilterBatchNumbersResult() {Result = -1, Error = ex.Message};
                }
            }
        }

        public RiskClientsResult GetRoles(RiskClientsParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    ctx.Who = riskServices.GetUserName(param.UserId);

                    RiskClientsResult res = new RiskClientsResult();
                    res.ClientList = new List<ERiskClient>();

                    RiskClient client = riskServices.GetRiskClient(param.ClientId);

                    if (string.IsNullOrEmpty(param.Who))
                        ctx.Who = riskServices.GetUserName(param.UserId);

                    int totalRows = -1;

                    //ctx.SourceRef = "";

                    // All clients
                    //if (param.FilterClientId == -1)
                    //{
                    //    var l = riskServices.GetAllRiskClients();

                    //    foreach (var i in l)
                    //        res.ClientList.Add(Translator.Translator.Translate(i));
                    //}
                    //else // Individual client
                    //{
                        res.ClientList.Add(Translator.Translator.Translate(riskServices.GetRiskClient(param.ClientId)));
                    //}

                    res.TotalRows = totalRows;

                    res.Result = 0;

                    return res;
                }
                catch (Exception ex)
                {
                    return new RiskClientsResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public WebsiteRiskRoleCreateResult CreateRiskRole(WebsiteRiskRoleCreateParam param)
        {

            try
            {
                WebsiteRiskRoleCreateResult res = new WebsiteRiskRoleCreateResult();

                using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
                {

                    IRiskServices riskServices = new RiskServices(ctx);
                    
                    riskServices.CreateRiskRole(param.ClientId, param.UserId, param.Who, param.TemplateName);

                    res.Result = 0;

                    return res;
                }
            }

            catch (Exception ex)
            {
                return new WebsiteRiskRoleCreateResult() { Result = -1, Error = ex.Message };
            }
        }
        
        public WebsiteRiskRoleEditResult EditRiskRole(WebsiteRiskRoleEditParam param)
        {
            try
            {
                WebsiteRiskRoleEditResult res = new WebsiteRiskRoleEditResult();

                using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    riskServices.EditRiskRole(param.ClientId, param.UserId, param.Who, param.TemplateName, param.Profile_Id);

                    res.Result = 0;

                    return res;
                }
            }

            catch (Exception ex)
            {
                return new WebsiteRiskRoleEditResult() { Result = -1, Error = ex.Message };
            }
        }

        public WebsiteRiskRoleDeleteResult DeleteRiskRole(WebsiteRiskRoleDeleteParam param)
        {
            try
            {
                WebsiteRiskRoleDeleteResult res = new WebsiteRiskRoleDeleteResult();

                using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
                {

                    IRiskServices riskServices = new RiskServices(ctx);

                    int result = riskServices.DeleteRiskRole(param.ClientId, param.UserId, param.Who, param.Profile_Id);

                    res.Result = result;
                    return res;
                }
            }

            catch (Exception ex)
            {
                return new WebsiteRiskRoleDeleteResult() { Result = -1, Error = ex.Message };
            }
        }

        public FindUserNameResult FindUserName(FindUserNameParam param)
        {
            try
            {
                FindUserNameResult res = new FindUserNameResult();

                using (CurrentContext ctx = new CurrentContext(0, 0, ""))
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    int result = riskServices.FindUserName(param.UserName);
                    
                    res.Result = result;
                    return res;
                }
            }

            catch (Exception ex)
            {
                return new FindUserNameResult() { Result = -1, Error = ex.Message };
            }
        }


        public GetListOfBatchPriorityClientsResult GetListOfBatchPriorityClients(GetListOfBatchPriorityClientsParam param)
        {
            try {

                GetListOfBatchPriorityClientsResult res = new GetListOfBatchPriorityClientsResult();

                using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    res.ListRiskClientBatchPriority = riskServices.GetRiskClientBatchPriority(param.ClientId, param.Who, param.UserId);

                    res.Result = 0;

                    return res;
                }

            }

            catch (Exception ex)
            {
                return new GetListOfBatchPriorityClientsResult() { Result = -1, Error = ex.Message };
            }


        }

        
        

        #region CUE
        public WebsiteCueSearchResult SubmitCueSearch(WebsiteCueSearchParam param)
        {
            WebsiteCueSearchResult res = new WebsiteCueSearchResult();

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    DateTime earliestRequestDate = new DateTime();

                    string sname = ConfigurationManager.AppSettings["CueServiceNameInDb"].ToString();
                    int serviceId = riskServices.GetServiceId(sname);

                    // Create the in-memory copy of all the AppSetting values
                    var config = new MDA.Pipeline.ServiceConfigSettings();

                    // Get the RiskBatch that this claim belongs to
                    var riskBatch = riskServices.GetRiskClaimParentBatch(param.RiskClaim_Id);
                    var riskClaim = riskServices.GetRiskClaim(param.RiskClaim_Id);

                    // Save locally these 2 field values
                    var _submitDirect = riskBatch.SubmitDirect;
                    var _scoreDirect = riskBatch.ScoreDirect;

                    // Now update the Batch so that these 2 values are TRUE. This HIDES the batch from the Windows Service
                    riskServices.SetRiskBatchDirectFlags(riskBatch.Id, true, true);

                    // Set the same 2 flags in the context so that the pipeline ONLY looks for batches that have this value set to TRUE
                    ctx.ScoreDirect = true;
                    ctx.SubmitDirect = true;

                    // Now for each person, create an External Service Request and reset the status to make external services get called
                    foreach (var personId in param.ListPerson_Id)
                    {
                        // Duplicate Unique Key will cause ref integrity to cause error unless removed first
                        riskServices.RemoveExistingServicesRequest(param.RiskClaim_Id, serviceId, personId);
                        
                        riskServices.InsertRiskExternalServicesRequest(param.RiskClaim_Id, serviceId, personId, param.Who);

                        riskServices.SetRiskClaimStatus(param.RiskClaim_Id, RiskClaimStatus.QueuedForThirdParty);
                    }

                    // Now call the pipeline to process THIS batch, which has these new external request records
                    if (config.Process3rdParty)
                    {
                        MDA.Pipeline.Pipeline.Stage4_ProcessClaimQueueThirdParties(ctx, config, riskBatch.Id);
                    }
                    // Now score them
                    //TODO: Call pipeline.Stage5_ScoreClaimQueue(ctx, config, batchId)

                    // Now reset risk claim status back to RiskClaimStatus.Scored
                    riskServices.SetRiskClaimStatus(param.RiskClaim_Id, RiskClaimStatus.Scored);

                    // Now reset the flags back to what they were before we started
                    riskServices.SetRiskBatchDirectFlags(riskBatch.Id, _submitDirect, _scoreDirect);
                    

                    byte[] reportPDF = null;

                    var resr = riskServices.GetExternalRequestRecordsAsListString(param.RiskClaim_Id, serviceId);

                    MDA.CueService.Model.Results result = new MDA.CueService.Model.Results();

                    result.ListResultInfo = new List<Dictionary<string,MDA.CueService.Model.Result>>();
                   
                    result.ClaimNumber = riskClaim.ClientClaimRefNumber;

                    // Find earliest requestedDate
                    foreach (var personId in param.ListPerson_Id)
                    {
                        var extRecord = riskServices.GetExternalRequestRecord(param.RiskClaim_Id, serviceId, personId);

                        if(extRecord.RequestedWhen > earliestRequestDate)
                            earliestRequestDate = extRecord.RequestedWhen;
                    }

                    result.SearchDate = earliestRequestDate;

                    foreach (var item in resr)
                    {
                        #region XML Serialized
                        //using (XmlReader reader = XmlReader.Create(new StringReader(item)))
                        //{
                        //    var formatter = new DataContractSerializer(typeof(MDA.CueService.Model.Result));
                        //    var q = (MDA.CueService.Model.Result)formatter.ReadObject(reader);
                        //    result.ListResultInfo.Add(q);
                        //}
                        #endregion
                       
                        var dictResults = JsonConvert.DeserializeObject<Dictionary<string, MDA.CueService.Model.Result>>(item);

                        result.ListResultInfo.Add(dictResults);
                    }

                    riskServices.SaveCueReportIntoRiskExternalTable(param.RiskClaim_Id, serviceId, reportPDF, result);
                }

                catch (Exception ex)
                {
                    return new WebsiteCueSearchResult() { Result = -1, Error = ex.Message };
                }
            }

            return res;
        }


        public WebsiteCueReportResult GetCueReport(WebsiteCueReportParam param)
        {
             WebsiteCueReportResult res = new WebsiteCueReportResult();

             ECueReportDetails reportModel = new ECueReportDetails();

             var lstResultInfo = new List<Dictionary<string, ECUE_ReportModel>>();

             using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
             {
                 try
                 {
                     IRiskServices riskServices = new RiskServices(ctx);

                     string sname = ConfigurationManager.AppSettings["CueServiceNameInDb"].ToString();
                     int serviceId = riskServices.GetServiceId(sname);

                     var reportString = riskServices.GetRiskReportStructure(param.RiskClaim_Id, serviceId);

                     var reportStructure = JsonConvert.DeserializeObject<MDA.CueService.Model.Results>(reportString);

                     reportModel = Translator.Translator.Translate(reportStructure);
                     
                     res.CueReportDetails = reportModel;
                 }
                 catch (Exception ex)
                 {
                     return new WebsiteCueReportResult() { Result = -1, Error = ex.Message };
                 }
             }
                
            return res;
        }

        public WebsiteCueInvolvementsResult GetCueInvolvements(WebsiteCueInvolvementsParam param)
        {
            WebsiteCueInvolvementsResult res = new WebsiteCueInvolvementsResult();
            
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                try
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    var currentClaim = riskServices.FetchCueInvolvements(param.RiskClaim_Id, param.ClientId);

                    res.ClaimNumber = currentClaim.ClaimNumber;
                    res.IncidentDate = currentClaim.IncidentDate;
                    res.UploadedDate = currentClaim.UploadedDate;

                    foreach (var involvement in currentClaim.CueInvolvements)
                    {
                        res.CueInvolvements.Add(involvement);
                    }
                }

                catch (Exception ex)
                {
                    return new WebsiteCueInvolvementsResult() { Result = -1, Error = ex.Message };
                }
            }

            return res;
        }

        #endregion


        #endregion

        #region Security

        public bool CheckUserBelongsToClient(CheckUserBelongsToClientParam param)
        {
            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                ctx.Who = riskServices.GetUserName(param.UserId);

                return riskServices.CheckUserBelongsToClient(param.ClientId, param.UserId);
            }
        }

        public bool CheckUserHasAccessToReport(CheckUserHasAccessToReportParam param)
        {
            bool hasAccessToBatchReport = true;

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {

                IRiskServices riskServices = new RiskServices(ctx);

                ctx.Who = riskServices.GetUserName(param.LoggedInUserId);

                if (param.ClientId != 0) // Keoghs Client (0) Can access all reports.
                {
                    hasAccessToBatchReport = riskServices.CheckUserHasAccessToReport(param.RiskBatchId, param.LoggedInUserId, param.ClientId);
                }

                return hasAccessToBatchReport;
            }
        }

        #endregion

        public SendElmahErrorResult SendElmahEmail(SendElmahErrorParam param)
        {
            using (CurrentContext ctx = new CurrentContext(0, 0, ""))
            {
                try
                {
                    SendElmahErrorResult res = new SendElmahErrorResult();

                    string subject = "ADA Website Error (Reported By ELMAH)";
                    string body = param.Error;
                    string from = ConfigurationManager.AppSettings["ElmahFrom"].ToString();
                    string to = ConfigurationManager.AppSettings["ElmahTo"].ToString();


                    MailMessage message = new MailMessage(from, to);
                    message.Subject = subject;
                    message.Body = body;
                    message.IsBodyHtml = true;
                    SmtpClient client = new SmtpClient();
                    client.Port = 25;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Host = ConfigurationManager.AppSettings["SmtpServer"];

                    // Attempt to send the email
                    try
                    {
                        client.Send(message);
                    }
                    catch (Exception e)
                    {
                        //ModelState.AddModelError("", "Issue sending email: " + e.Message);
                    }
                    return res;
                }

                catch (Exception ex)
                {
                    return new SendElmahErrorResult() { Result = -1, Error = ex.Message };
                }
            }
        }

        public GetBatchQueueResult GetBatchQueue(GetBatchQueueParam param)
        {
            List<MDA.RiskService.Model.EBatchQueue> ListBatchQueue = new List<MDA.RiskService.Model.EBatchQueue>();

            GetBatchQueueResult BatchQueueResult = new GetBatchQueueResult();
            BatchQueueResult.BatchQueue = new List<Interface.EBatchQueue>();

            GetBatchQueueResult res = new GetBatchQueueResult();

            using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
            {
                IRiskServices riskServices = new RiskServices(ctx);

                ListBatchQueue = riskServices.GetRiskBatchQueue();

                foreach (var batch in ListBatchQueue)
                {
                    BatchQueueResult.BatchQueue.Add(new Interface.EBatchQueue { BatchId = batch.BatchId, BatchRef = batch.BatchRef, ClaimsReceieved = batch.ClaimsReceieved, Client = batch.Client, Uploaded = batch.Uploaded });
                }

                return BatchQueueResult;
            }
        }

        public SaveBatchPriorityClientsResult SaveBatchPriorityClients(SaveBatchPriorityClientsParam param)
        {
            try
            {
                SaveBatchPriorityClientsResult res = new SaveBatchPriorityClientsResult();

                using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    res.Result = riskServices.SaveBatchPriorityClients(param.ClientId, param.UserId, param.Who, param.ClientBatchPriorityArray);

                    return res;
                }
            }

            catch (Exception ex)
            {
                return new SaveBatchPriorityClientsResult() { Result = -1, Error = ex.Message };
            }
        }

        public SaveOverriddenBatchPriorityResult SaveOverriddenBatchPriority(SaveOverriddenBatchPriorityParam param)
        {
            try
            {
                SaveOverriddenBatchPriorityResult res = new SaveOverriddenBatchPriorityResult();
                using (CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, param.Who))
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    res.Result = riskServices.SaveOverriddenBatchPriority(param.ClientId, param.UserId, param.Who, param.OverriddenBatchPriorityArray);


                    return res;
                }
            }

            catch (Exception ex)
            {
                return new SaveOverriddenBatchPriorityResult() { Result = -1, Error = ex.Message };
            }
        }
    }
}
