﻿CREATE TABLE [Extract].[Claim] (
    [RowID]                    INT             IDENTITY (1, 1) NOT NULL,
    [ClaimHashbyte]            VARBINARY (MAX) NOT NULL,
    [IncludeRecord]            BIT             CONSTRAINT [DF_CreditHirePreLitigated_Claim_IncludeRecord] DEFAULT ((0)) NOT NULL,
    [ExcludeRecord]            BIT             CONSTRAINT [DF_CreditHirePreLitigated_Claim_ExcludeRecord] DEFAULT ((0)) NOT NULL,
    [CreationModificationDate] SMALLDATETIME   NOT NULL,
    [RecordSourceID]           INT             NOT NULL,
    [ProductTypeID]            INT             NOT NULL,
    [MatterNumber]             INT             NOT NULL,
    [Insurer]                  VARCHAR (50)    NULL,
    [IncidentDate]             SMALLDATETIME   NULL,
    [ClaimStatus]              VARCHAR (50)    NULL,
    [InsurerClientReference]   VARCHAR (50)    NULL,
    [IncidentCircumstances]    VARCHAR (1024)  NULL,
    [CreditHire]               BIT             NULL,
    [ProceedingsIssued]        BIT             NULL,
	[CFS]					   BIT             NULL
    CONSTRAINT [PK_Extract.Claim_RowID_C] PRIMARY KEY CLUSTERED ([RowID] ASC),
    CONSTRAINT [FK_Extract.Claim_Ref.Configuration_ProductTypeID] FOREIGN KEY ([ProductTypeID]) REFERENCES [Ref].[Configuration] ([RowID]),
    CONSTRAINT [FK_Extract.Claim_Ref.Configuration_RecordSourceID] FOREIGN KEY ([RecordSourceID]) REFERENCES [Ref].[Configuration] ([RowID])
);

