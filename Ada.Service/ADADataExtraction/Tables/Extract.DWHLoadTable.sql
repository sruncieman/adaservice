﻿CREATE TABLE [Extract].[DWHLoadTable] (
    [LoadID]     INT      NOT NULL,
    [LoadDay]    DATETIME NULL,
    [LoadStart]  DATETIME NULL,
    [LoadFinish] DATETIME NULL,
    [ErrorCount] INT      NULL,
    CONSTRAINT [PK_Extract.DWHLoadTable_LoadID] PRIMARY KEY CLUSTERED ([LoadID] ASC)
);

