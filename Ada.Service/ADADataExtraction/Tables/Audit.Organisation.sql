﻿CREATE TABLE [Audit].[Organisation] (
    [RowID]                    INT             IDENTITY (1, 1) NOT NULL,
    [DMLAction]                VARCHAR (10)    NOT NULL,
    [ExecutionDateTime]        SMALLDATETIME   NOT NULL,
    [OrganisationRowID]        INT             NOT NULL,
    [OrganisationHashbyte]     VARBINARY (MAX) NOT NULL,
    [CreationModificationDate] SMALLDATETIME   NOT NULL,
    [ProductTypeID]            INT             NOT NULL,
    [MatterNumber]             INT             NOT NULL,
    [OrganisationID]           SMALLINT        NULL,
    [PersonID]                 SMALLINT        NULL,
    [OrganisationInvolvement]  VARCHAR (50)    NULL,
    [CompanyName]              VARCHAR (255)   NULL,
    [Telephone1]               VARCHAR (50)    NULL,
    [Address]                  VARCHAR (225)   NULL,
    [PostCode]                 VARCHAR (20)    NULL,
    CONSTRAINT [PK_Audit.Organisation_RowID_C] PRIMARY KEY CLUSTERED ([RowID] ASC)
);

