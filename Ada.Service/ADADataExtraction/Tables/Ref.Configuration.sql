﻿CREATE TABLE [Ref].[Configuration] (
    [RowID]                      INT           IDENTITY (1, 1) NOT NULL,
    [ConfigurationDescriptionID] INT           NULL,
    [Value]                      VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Ref.Configuration_RowID_C] PRIMARY KEY CLUSTERED ([RowID] ASC),
    CONSTRAINT [FK_Ref.Configuration_Ref.ConfigurationDescription_ConfigurationDescriptionID] FOREIGN KEY ([ConfigurationDescriptionID]) REFERENCES [Ref].[ConfigurationDescription] ([RowID])
);

