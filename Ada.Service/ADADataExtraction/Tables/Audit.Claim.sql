﻿CREATE TABLE [Audit].[Claim] (
    [RowID]                    INT             IDENTITY (1, 1) NOT NULL,
    [DMLAction]                VARCHAR (10)    NOT NULL,
    [ExecutionDateTime]        SMALLDATETIME   NOT NULL,
    [ClaimRowID]               INT             NOT NULL,
    [ClaimHashbyte]            VARBINARY (MAX) NOT NULL,
    [IncludeRecord]            BIT             NOT NULL,
    [ExcludeRecord]            BIT             NOT NULL,
    [CreationModificationDate] SMALLDATETIME   NOT NULL,
    [RecordSourceID]           INT             NOT NULL,
    [ProductTypeID]            INT             NOT NULL,
    [MatterNumber]             INT             NOT NULL,
    [Insurer]                  VARCHAR (50)    NULL,
    [IncidentDate]             SMALLDATETIME   NULL,
    [ClaimStatus]              VARCHAR (50)    NULL,
    [InsurerClientReference]   VARCHAR (50)    NULL,
    [IncidentCircumstances]    VARCHAR (1024)  NULL,
    [CreditHire]               BIT             NULL,
    [ProceedingsIssued]        BIT             NULL,
	[CFS]					   BIT             NULL
    CONSTRAINT [PK_Audit.Claim_RowID_C] PRIMARY KEY CLUSTERED ([RowID] ASC)
);

