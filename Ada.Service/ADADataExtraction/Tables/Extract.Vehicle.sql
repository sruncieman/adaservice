﻿CREATE TABLE [Extract].[Vehicle] (
    [RowID]                    INT             IDENTITY (1, 1) NOT NULL,
    [VehicleHashbyte]          VARBINARY (MAX) NOT NULL,
    [CreationModificationDate] SMALLDATETIME   NOT NULL,
    [ProductTypeID]            INT             NOT NULL,
    [MatterNumber]             INT             NOT NULL,
    [VehicleID]                SMALLINT        NULL,
    [OrganisationID]           SMALLINT        NULL,
    [VehicleInvolvement]       VARCHAR (50)    NULL,
    [VehicleRegistration]      VARCHAR (20)    NULL,
    [VehicleMake]              VARCHAR (50)    NULL,
    [VehicleModel]             VARCHAR (50)    NULL,
    [DateHireStarted]          SMALLDATETIME   NULL,
    [DateHireEnded]            SMALLDATETIME   NULL,
    CONSTRAINT [PK_Extract.Vehicle_RowID_C] PRIMARY KEY CLUSTERED ([RowID] ASC),
    CONSTRAINT [FK_Extract.Vehicle_Ref.Configuration_ProductTypeID] FOREIGN KEY ([ProductTypeID]) REFERENCES [Ref].[Configuration] ([RowID])
);

