﻿CREATE TABLE [Ref].[ConfigurationDescription] (
    [RowID]                    INT           IDENTITY (1, 1) NOT NULL,
    [ConfigurationDescription] VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_Ref.ConfigurationDescription_RowID_C] PRIMARY KEY CLUSTERED ([RowID] ASC)
);

