﻿CREATE TABLE [Extract].[Person] (
    [RowID]                    INT             IDENTITY (1, 1) NOT NULL,
    [PersonHashbyte]           VARBINARY (MAX) NOT NULL,
    [CreationModificationDate] SMALLDATETIME   NOT NULL,
    [ProductTypeID]            INT             NOT NULL,
    [MatterNumber]             INT             NOT NULL,
    [VehicleID]                SMALLINT        NULL,
    [PersonID]                 SMALLINT        NULL,
    [PersonInvolvement]        VARCHAR (50)    NULL,
    [Salutation]               VARCHAR (20)    NULL,
    [FirstName]                VARCHAR (100)   NULL,
    [LastName]                 VARCHAR (100)   NULL,
    [DateOfBirth]              SMALLDATETIME   NULL,
    [Gender]                   VARCHAR (50)    NULL,
    [NINumber]                 VARCHAR (30)    NULL,
    [LandlineTelephone]        VARCHAR (50)    NULL,
    [MobileTelephone]          VARCHAR (50)    NULL,
    [Address]                  VARCHAR (225)   NULL,
    [PostCode]                 VARCHAR (20)    NULL,
    CONSTRAINT [PK_Extract.Person_RowID_C] PRIMARY KEY CLUSTERED ([RowID] ASC),
    CONSTRAINT [FK_Extract.Person_Ref.Configuration_ProductTypeID] FOREIGN KEY ([ProductTypeID]) REFERENCES [Ref].[Configuration] ([RowID])
);

