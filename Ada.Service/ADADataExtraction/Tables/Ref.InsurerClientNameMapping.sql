﻿CREATE TABLE [Ref].[InsurerClientNameMapping] (
    [RowID]                  INT          IDENTITY (1, 1) NOT NULL,
    [ExtractClientName]      VARCHAR (50) NOT NULL,
    [FriendlyFileClientName] VARCHAR (50) NOT NULL,
    [ProductTypeID]          INT          NOT NULL,
    [Active]                 BIT          NOT NULL,
    CONSTRAINT [PK_Ref.InsurerClientNameMapping_RowID_C] PRIMARY KEY CLUSTERED ([RowID] ASC),
    CONSTRAINT [FK_Ref.InsurerClientNameMapping.Configuration_ProductTypeID] FOREIGN KEY ([ProductTypeID]) REFERENCES [Ref].[Configuration] ([RowID])
);

