﻿CREATE TABLE [Audit].[Vehicle] (
    [RowID]                    INT             IDENTITY (1, 1) NOT NULL,
    [DMLAction]                VARCHAR (10)    NOT NULL,
    [ExecutionDateTime]        SMALLDATETIME   NOT NULL,
    [VehicleRowID]             INT             NOT NULL,
    [VehicleHashbyte]          VARBINARY (MAX) NOT NULL,
    [CreationModificationDate] SMALLDATETIME   NOT NULL,
    [ProductTypeID]            INT             NOT NULL,
    [MatterNumber]             INT             NOT NULL,
    [VehicleID]                SMALLINT        NULL,
    [OrganisationID]           INT             NULL,
    [VehicleInvolvement]       VARCHAR (50)    NULL,
    [VehicleRegistration]      VARCHAR (20)    NULL,
    [VehicleMake]              VARCHAR (50)    NULL,
    [VehicleModel]             VARCHAR (50)    NULL,
    [DateHireStarted]          SMALLDATETIME   NULL,
    [DateHireEnded]            SMALLDATETIME   NULL,
    CONSTRAINT [PK_Audit.Vehicle_RowID_C] PRIMARY KEY CLUSTERED ([RowID] ASC)
);

