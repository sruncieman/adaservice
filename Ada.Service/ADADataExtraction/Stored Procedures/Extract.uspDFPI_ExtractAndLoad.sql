﻿CREATE PROCEDURE [Extract].[uspDFPI_ExtractAndLoad]
/**************************************************************************************************/
-- ObjectName:		[Extract].[uspDFPI_ExtractAndLoad]
--
-- Description:		Extract the Credit Hire Pre Litigated Matters from VF and load locally.
--				
--
-- Steps:	
--		
-- RevisionHistory:
--		Date				Author			Modification
--		2015-02-02			Paul Allen		Created
/**************************************************************************************************/
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
SET DATEFORMAT DMY

-----------------------------------------
--1.)SetUpAnyTempObjectsAndVariables
-----------------------------------------
DECLARE  @Now				SMALLDATETIME	= GETDATE()
		,@RecordSourceID	INT				= 4 --VFStage
		,@ProductTypeID		INT				= 2 --DFPI

IF OBJECT_ID('tempdb..#Claim') IS NOT NULL
	DROP TABLE #Claim

IF OBJECT_ID('tempdb..#Vehicle') IS NOT NULL
	DROP TABLE #Vehicle

IF OBJECT_ID('tempdb..#Person') IS NOT NULL
	DROP TABLE #Person

IF OBJECT_ID('tempdb..#Organisation') IS NOT NULL
	DROP TABLE #Organisation

IF OBJECT_ID('tempdb..#OrganisationUpdate') IS NOT NULL
	DROP TABLE #OrganisationUpdate

CREATE TABLE #Claim (ID INT IDENTITY(1,1),MatterNumber INT NOT NULL, Hashbyte VARBINARY(MAX), Insurer VARCHAR(50), IncidentDate SMALLDATETIME, ClaimStatus VARCHAR(50), InsurerClientReference VARCHAR(50), IncidentCircumstances VARCHAR(1024), CreditHire BIT, ProceedingsIssued BIT, CFS BIT, MatterClosed BIT)
CREATE TABLE #Vehicle (ID INT IDENTITY(1,1),MatterNumber INT NOT NULL, Hashbyte VARBINARY(MAX), VehicleID SMALLINT, OrganisationID SMALLINT, VehicleInvolvement VARCHAR(50), VehicleRegistration VARCHAR(20), VehicleMake VARCHAR(50), VehicleModel VARCHAR(50), DateHireStarted SMALLDATETIME, DateHireEnded SMALLDATETIME, GroupNo SMALLINT, ClaimantGroupNo SMALLINT)
CREATE TABLE #Person (ID INT IDENTITY(1,1),MatterNumber INT NOT NULL, Hashbyte VARBINARY(MAX), VehicleID SMALLINT, PersonID SMALLINT, PersonInvolvement VARCHAR(50), Salutation VARCHAR(20), FirstName VARCHAR(100), LastName VARCHAR(100), DateOfBirth SMALLDATETIME, Gender VARCHAR(50), NINumber VARCHAR(30), LandlineTelephone VARCHAR(50), MobileTelephone VARCHAR(50), Address VARCHAR(225), PostCode VARCHAR(20), GroupNo SMALLINT, ClaimStatus VARCHAR(100))
CREATE TABLE #Organisation (ID INT IDENTITY(1,1),MatterNumber INT NOT NULL, Hashbyte VARBINARY(MAX), OrganisationID SMALLINT, PersonID SMALLINT, OrganisationInvolvement VARCHAR(50), CompanyName VARCHAR(255), Telephone1 VARCHAR(50), Address VARCHAR(225), PostCode VARCHAR(20), GroupNo SMALLINT)
CREATE TABLE #OrganisationUpdate (ID INT IDENTITY(1,1), MatterNumber INT NOT NULL, OrganisationInvolvement VARCHAR(50), Telephone1 VARCHAR(50), Address VARCHAR(225), PostCode VARCHAR(20), GroupNo SMALLINT)

--------------------------
--2.)ExtractTheClaimData
--------------------------
INSERT INTO #Claim (MatterNumber, Insurer, IncidentDate, ClaimStatus, InsurerClientReference, IncidentCircumstances, CreditHire, ProceedingsIssued, CFS, MatterClosed)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(Insurer,50) AS VARCHAR(50)))),'"','') Insurer
		,CASE WHEN ISDATE(LTRIM(RTRIM(IncidentDate))) = 1 AND CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(IncidentDate)),103) AS DATETIME) BETWEEN '01-JAN-1900' AND '01-JUN-2079' THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(IncidentDate)),103) AS SMALLDATETIME) END + CASE WHEN ISDATE(REPLACE(ISNULL(IncidentTime,'00:00'),'.',':')) = 1 THEN REPLACE(ISNULL(IncidentTime,'00:00'),'.',':') ELSE '00:00' END IncidentDate
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(ClaimStatus,50) AS VARCHAR(50)))),'"','') ClaimStatus
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(InsurerClientReference,50) AS VARCHAR(50)))),'"','') InsurerClientReference
		,REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(CAST(LEFT(IncidentCircumstances,1024) AS VARCHAR(1024)))),'"',''), CHAR(13),''),CHAR(10),''),CHAR(13) + CHAR(10), '') IncidentCircumstances
		,CAST(CASE WHEN LEFT(CreditHire,1) = 'Y' THEN 1 ELSE 0 END AS BIT) CreditHire
		,CAST(CASE WHEN LEFT(ProceedingsIssued,1) = 'Y' THEN 1 ELSE 0 END AS BIT) ProceedingsIssued
		,CAST(CASE WHEN LEFT(CFS,1) = 'Y' THEN 1 ELSE 0 END AS BIT) CFS
		,CAST(CASE WHEN MatterClosed = 'COMP' THEN 1 ELSE 0 END AS BIT) MatterClosed
FROM OPENQUERY  (
				VF_Stage,
				'SELECT  MD.Matter_Code MatterNumber
						,INS.INS01 Insurer
						,ACC.ACC01 IncidentDate
						,ACC.ACC03 IncidentTime
						,NULL ClaimStatus
						,INS.INS13 InsurerClientReference
						,ACI.ACI01 IncidentCircumstances
						,RTA.RTA01 CreditHire
						,GCI.GCI10 ProceedingsIssued
						,GCI.GCI09 CFS
						,MD.Mstone_Code MatterClosed
				FROM dbo.Matdb MD
				LEFT JOIN dbo.UDDetail_ACC ACC ON MD.Mt_Int_Code = ACC.Owner_Code AND MD.Mt_Type = ACC.Parent_Code AND ACC.PO_Type_Char = ''C''
				LEFT JOIN dbo.UDDetail_INS INS ON MD.Mt_int_Code = INS.Owner_Code AND MD.Mt_Type = INS.Parent_Code AND INS.PO_Type_Char = ''C''
				LEFT JOIN dbo.UDDetail_RTA RTA ON MD.Mt_int_Code = RTA.Owner_Code AND MD.Mt_Type = RTA.Parent_Code AND RTA.PO_Type_Char = ''C''
				LEFT JOIN dbo.UDDETAIL_GCI GCI ON MD.Mt_Int_Code = GCI.Owner_Code AND MD.Mt_Type = GCI.Parent_Code AND GCI.PO_Type_Char = ''C''
				LEFT JOIN dbo.UDDETAIL_ACI ACI ON MD.Mt_Int_Code = ACI.Owner_Code AND MD.Mt_Type = ACI.Parent_Code AND ACI.PO_Type_Char = ''C''
				WHERE MD.Mt_Type = ''DFPI'''
				)
ORDER BY REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') 

--------------------------
--3.)ExtractTheVehicleData
--------------------------
INSERT INTO #Vehicle (MatterNumber, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, DateHireStarted, DateHireEnded, GroupNo, ClaimantGroupNo)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST((CASE WHEN RelationCode IN ('FILE_INVEHICLE','FILE_DEFVEHICLE') THEN 'InsuredVehicle'  WHEN RelationCode IN ('FILE_CLVEHICLE') THEN 'ThirdPartyVehicle' WHEN RelationCode IN ('FILE_HIREVEHICLE') THEN 'HireVehicle' ELSE '' END) AS VARCHAR(50)) VehicleInvolvement
		,REPLACE(REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleRegistration,20) AS VARCHAR(20)))),'"',''),' ','') VehicleRegistration
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleMake,50) AS VARCHAR(50)))),'"','') VehicleMake
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleModel,50) AS VARCHAR(50)))),'"','') VehicleModel
		,CASE WHEN ISDATE(LTRIM(RTRIM(DateHireStarted))) =1 AND CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireStarted)),103) AS DATETIME) BETWEEN '01-JAN-1900' AND '01-JUN-2079' THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireStarted)),103) AS SMALLDATETIME) ELSE NULL END DateHireStarted
		,CASE WHEN ISDATE(LTRIM(RTRIM(DateHireEnded))) =1 AND CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireEnded)),103) AS DATETIME) BETWEEN '01-JAN-1900' AND '01-JUN-2079' THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireEnded)),103) AS SMALLDATETIME) ELSE NULL END DateHireEnded
		,REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','')  GroupNo
		,REPLACE(LTRIM(RTRIM(CAST(REPLACE(ISNULL(ClaimantGroupNo,1),'GRP,CLAI','') AS SMALLINT))),'"','')  ClaimantGroupNo
FROM OPENQUERY  (
				VF_Stage,
				'SELECT  md.Matter_Code AS MatterNumber
						,md.Mt_Int_Code
						,md.Mt_Int_Code AS Owner_Code
						,md.Mt_Type AS Parent_Code
						,l.From_Subcode_Group_No GroupNo
						,l.To_Owner_Code AS Entity_Int_Code
						,R.Relation_Code RelationCode 
						,[VEH01] VehicleRegistration
						,[VEH02] VehicleMake
						,[VEH03] VehicleModel
						,[HIR04] DateHireStarted
						,[HIR05] DateHireEnded
						,[HCC24] ClaimantGroupNo
				FROM dbo.Matdb MD
				INNER JOIN dbo.Link L ON MD.Mt_Int_Code = L.From_Owner_Code
				INNER JOIN dbo.Entity E	ON L.From_Owner_Code = E.Entity_Int_Code AND E.Sort_Field <> ''''	
				INNER JOIN dbo.Relation	R ON L.Relation_Int_Code = R.Relation_Int_Code
				--JoinOnToTheRequiredTables
				INNER JOIN dbo.UDDetail_VEH VEH ON VEH.Owner_Code = l.To_Owner_Code AND VEH.PO_Type_Char = L.To_Owner_Type_Char
				LEFT JOIN [dbo].[UDDETAIL_HIR] HIR ON HIR.Owner_Code = MD.Mt_Int_Code AND R.Relation_Code = ''FILE_HIREVEHICLE'' AND HIR.PO_Type_Char = ''C'' AND HIR.Group_No = l.From_Subcode_Group_No
				LEFT JOIN [dbo].[UDDETAIL_HCC] HCC ON HCC.Owner_Code = MD.Mt_Int_Code AND R.Relation_Code = ''FILE_HIREVEHICLE'' AND HCC.PO_Type_Char = ''C'' AND HCC.Group_No = l.From_Subcode_Group_No
				WHERE md.Mt_Type = ''DFPI''
				AND R.Relation_Code IN (''FILE_CLVEHICLE'',''FILE_DEFVEHICLE'',''FILE_HIREVEHICLE'',''FILE_INVEHICLE'')'
				)	
GROUP BY REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','')
		,CAST((CASE WHEN RelationCode IN ('FILE_INVEHICLE','FILE_DEFVEHICLE') THEN 'InsuredVehicle'  WHEN RelationCode IN ('FILE_CLVEHICLE') THEN 'ThirdPartyVehicle' WHEN RelationCode IN ('FILE_HIREVEHICLE') THEN 'HireVehicle' ELSE '' END) AS VARCHAR(50)) 
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleRegistration,20) AS VARCHAR(20)))),'"','')
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleMake,50) AS VARCHAR(50)))),'"','')
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleModel,50) AS VARCHAR(50)))),'"','')
		,CASE WHEN ISDATE(LTRIM(RTRIM(DateHireStarted))) =1 AND CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireStarted)),103) AS DATETIME) BETWEEN '01-JAN-1900' AND '01-JUN-2079' THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireStarted)),103) AS SMALLDATETIME) ELSE NULL END
		,CASE WHEN ISDATE(LTRIM(RTRIM(DateHireEnded))) =1 AND CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireEnded)),103) AS DATETIME) BETWEEN '01-JAN-1900' AND '01-JUN-2079' THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireEnded)),103) AS SMALLDATETIME) ELSE NULL END
		,REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','') 
		,REPLACE(LTRIM(RTRIM(CAST(REPLACE(ISNULL(ClaimantGroupNo,1),'GRP,CLAI','') AS SMALLINT))),'"','') 
ORDER BY REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','')

---ThereShoudOnlyEverBeOneInsuredVehicle... So if we have two delete the one which lacks details
DELETE VEH
FROM #Vehicle AS VEH
INNER JOIN (
			SELECT   ID
					,ROW_NUMBER() OVER (PARTITION BY MatterNumber ORDER BY CASE WHEN VehicleRegistration IS NOT NULL THEN 1 ELSE 0 END + CASE WHEN VehicleMake IS NOT NULL THEN 1 ELSE 0 END + CASE WHEN VehicleModel IS NOT NULL THEN 1 ELSE 0 END DESC) RowNumber
			FROM #Vehicle
			WHERE VehicleInvolvement = 'InsuredVehicle'
			) DEL ON DEL.ID = VEH.ID AND DEL.RowNumber !=1

-----------------------------------------
--4.)ExtractThePersonData
-----------------------------------------
INSERT INTO #Person (MatterNumber, PersonInvolvement, Salutation, FirstName, LastName, DateOfBirth, Gender, NINumber, LandlineTelephone, MobileTelephone, Address, PostCode, GroupNo)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST(CASE WHEN REPLACE(LTRIM(RTRIM(PersonInvolvement)),'"','') = 'FILE_INSDCONTACT' THEN 'Insured'  WHEN ISNULL(IsDriver,'') = 'YES' THEN 'InsuredDriver' WHEN REPLACE(LTRIM(RTRIM(PersonInvolvement)),'"','') = 'FILE_CLCONTACT' THEN 'Claimant' END AS VARCHAR(50)) PersonInvolvement
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(Salutation,20) AS VARCHAR(20)))),'"','') Salutation
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(FirstName,100) AS VARCHAR(100)))),'"','') FirstName
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(LastName,100) AS VARCHAR(100)))),'"','') LastName
		,CASE WHEN ISDATE(LTRIM(RTRIM(DateOfBirth))) =1 AND CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateOfBirth)),103) AS DATETIME) BETWEEN '01-JAN-1900' AND '01-JUN-2079' THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateOfBirth)),103) AS SMALLDATETIME) ELSE NULL END DateOfBirth
		,CASE WHEN REPLACE(LTRIM(RTRIM(CAST(LEFT(Gender,50) AS VARCHAR(50)))),'"','') NOT IN ('Male','Female') THEN 'Unknown' ELSE REPLACE(LTRIM(RTRIM(CAST(LEFT(Gender,50) AS VARCHAR(50)))),'"','') END Gender
		,REPLACE(REPLACE(LTRIM(RTRIM(CAST(LEFT(NINumber,30) AS VARCHAR(50)))),'"',''),' ','') NINumber
		,REPLACE(REPLACE(LTRIM(RTRIM(CAST(LEFT(Landline,50) AS VARCHAR(50)))),'"',''),' ','') Landline
		,REPLACE(REPLACE(LTRIM(RTRIM(CAST(LEFT(Mobile,50) AS VARCHAR(50)))),'"',''),' ','') Mobile
		,CAST(LEFT(UPPER(ISNULL(REPLACE(LTRIM(RTRIM(Address1)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address2)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address3)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address4)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address5)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address6)),'"','')  + ' ','')) ,225) AS VARCHAR(225)) Address
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(REPLACE(PostCode,' ',''),20) AS VARCHAR(20)))),'"','') PostCode
		,REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','')  GroupNo
FROM OPENQUERY	(
				VF_Stage,
				'SELECT  md.Matter_Code AS MatterNumber
						,md.Mt_Int_Code
						,ISNULL(mai.Mt_Add_In_Int_Code, md.Mt_Int_Code) AS Owner_Code
						,ISNULL(mai.Add_In_Code, md.Mt_Type) AS Parent_Code
						,l.From_Subcode_Group_No AS GroupNo
						,l.To_Owner_Code AS Entity_Int_Code
						,R.Relation_Code PersonInvolvement 
						,NAM03 Salutation
						,NAM02 FirstName
						,NAM01 LastName
						,NAM15 Gender
						,NAM04 Address1
						,NAM05 Address2
						,NAM06 Address3
						,NAM07 Address4
						,NAM08 Address5
						,NAM09 Address6
						,NAM10 Postcode
						,CON05 EmailAddress
						,CON01 Landline
						,CON03 Mobile
						,PSI01 DateOfBirth
						,PSI12 NINumber
						,FAD.FAD23 IsDriver --All Null
				FROM dbo.Matdb MD
				LEFT JOIN dbo.Matdb_Add_In MAI ON MD.Mt_Int_Code = MAI.Mt_Int_Code
				INNER JOIN dbo.Link L ON MD.Mt_Int_Code = L.From_Owner_Code
				INNER JOIN dbo.Entity E	ON L.To_Owner_Code = E.Entity_Int_Code AND E.Sort_Field <> ''''	
				INNER JOIN dbo.Relation	R ON L.Relation_Int_Code = R.Relation_Int_Code
				--JoinOnToTheRequiredTables
				LEFT JOIN [dbo].[UDDETAIL_NAM] NAM ON NAM.Owner_Code = l.To_Owner_Code AND NAM.PO_Type_Char = L.To_Owner_Type_Char AND NAM.Parent_Code = E.Entity_Type_Code
				LEFT JOIN [dbo].[UDDETAIL_CON] CON ON CON.Owner_Code = l.To_Owner_Code AND CON.PO_Type_Char = L.To_Owner_Type_Char AND CON.Parent_Code = E.Entity_Type_Code
				LEFT JOIN [dbo].[UDDETAIL_PSI] PSI ON PSI.Owner_Code = l.To_Owner_Code AND PSI.PO_Type_Char = L.To_Owner_Type_Char AND PSI.Parent_Code = E.Entity_Type_Code
				LEFT JOIN [dbo].[UDDETAIL_FAD] FAD ON FAD.Owner_Code = l.To_Owner_Code AND FAD.PO_Type_Char = L.To_Owner_Type_Char AND FAD.Parent_Code = E.Entity_Type_Code
				WHERE ISNULL(mai.Add_In_Code, md.Mt_Type) = ''DFPI''
				AND R.Relation_Code IN (''FILE_CLCONTACT'',''FILE_INSDCONTACT'')'
				)
ORDER BY REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') 

UPDATE PER
SET ClaimStatus = VF.ClaimStatus
FROM #Person PER
INNER JOIN	(
			SELECT	 REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
					,REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','')  GroupNo
					,REPLACE(LTRIM(RTRIM(CAST(LEFT(ClaimStatus,100) AS VARCHAR(100)))),'"','') ClaimStatus
			FROM OPENQUERY	(VF_Stage,
							'SELECT	 md.Matter_Code MatterNumber
									,Group_No GroupNo
									,DRC.DRC01 ClaimStatus
							FROM [dbo].[UDDETAIL_DRC] DRC
							INNER JOIN dbo.Matdb MD ON MD.Mt_Int_Code = DRC.Owner_Code
							WHERE Parent_Code = ''DFPI''
							AND DRC.DRC01 IS NOT NULL'
							)
			) VF ON VF.MatterNumber = PER.MatterNumber AND VF.GroupNo = PER.GroupNo
WHERE PER.PersonInvolvement = 'Claimant'

-----------------------------------------
--5.)ExtractTheOrganisationData
-----------------------------------------
--GetTheBaseDetails
INSERT INTO #Organisation (MatterNumber, OrganisationInvolvement, CompanyName, GroupNo)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST(CASE REPLACE(LTRIM(RTRIM(OrganisationInvolvement)),'"','') WHEN 'FILE_CLHIRECOMPANY' THEN 'CHO' WHEN 'FILE_HIRECOMPANY' THEN 'CHO' WHEN 'FILE_CLAIMSOLICITORS' THEN 'ClaimantSolicitor' WHEN 'FILE_CLAIMINSURER' THEN 'ClaimantInsurer' WHEN 'FILE_CLAIMBROKER' THEN 'ClaimantBroker' WHEN 'FILE_ACCIDENTMANAGEMENT' THEN 'Repairer' WHEN 'FILE_GENERAL' THEN 'GeneralParty' WHEN 'FILE_SCUENGINEER' THEN 'Engineer' WHEN 'FILE_RECOVERYSTORAGE' THEN 'RecoverySalvageAgent' END AS VARCHAR(50)) OrganisationInvolvement
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(CompanyName,225) AS VARCHAR(225)))),'"','') CompanyName
		,REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','')  GroupNo
FROM OPENQUERY	(
				VF_Stage,
				'SELECT  md.Matter_Code AS MatterNumber
						,md.Mt_Int_Code
						,ISNULL(mai.Mt_Add_In_Int_Code, md.Mt_Int_Code) AS Owner_Code
						,ISNULL(mai.Add_In_Code, md.Mt_Type) AS Parent_Code
						,l.From_Subcode_Group_No AS GroupNo
						,l.To_Owner_Code AS Entity_Int_Code
						,R.Relation_Code OrganisationInvolvement 
						,AD.[ADD02] CompanyName
				FROM dbo.Matdb MD
				LEFT JOIN dbo.Matdb_Add_In MAI ON MD.Mt_Int_Code = MAI.Mt_Int_Code
				INNER JOIN dbo.Link L ON MD.Mt_Int_Code = L.From_Owner_Code
				INNER JOIN dbo.Entity E	ON L.To_Owner_Code = E.Entity_Int_Code AND E.Sort_Field <> ''''	
				INNER JOIN dbo.Relation	R ON L.Relation_Int_Code = R.Relation_Int_Code
				--JoinOnToTheRequiredTables
				LEFT JOIN [dbo].[UDDETAIL_ADD] AD ON AD.Owner_Code = l.To_Owner_Code AND AD.PO_Type_Char = L.To_Owner_Type_Char AND AD.Parent_Code = E.Entity_Type_Code
				WHERE ISNULL(mai.Add_In_Code, md.Mt_Type) = ''DFPI''
				AND R.Relation_Code IN (''FILE_CLAIMBROKER'',''FILE_CLAIMINSURER'',''FILE_CLAIMSOLICITORS'',''FILE_CLHIRECOMPANY'',''FILE_GENERAL'',''FILE_RECOVERYSTORAGE'',''FILE_SCUENGINEER'',''FILE_HIRECOMPANY'',''FILE_ACCIDENTMANAGEMENT'')'
				)
ORDER BY REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"',''), CAST(CASE REPLACE(LTRIM(RTRIM(OrganisationInvolvement)),'"','') WHEN 'FILE_CLHIRECOMPANY' THEN 'CHO' WHEN 'FILE_HIRECOMPANY' THEN 'CHO' WHEN 'FILE_CLAIMSOLICITORS' THEN 'ClaimantSolicitor' WHEN 'FILE_CLAIMINSURER' THEN 'ClaimantInsurer' WHEN 'FILE_CLAIMBROKER' THEN 'ClaimantBroker' WHEN 'FILE_ACCIDENTMANAGEMENT' THEN 'Repairer' WHEN 'FILE_GENERAL' THEN 'GeneralParty' WHEN 'FILE_SCUENGINEER' THEN 'Engineer' WHEN 'FILE_RECOVERYSTORAGE' THEN 'RecoverySalvageAgent' END AS VARCHAR(50)), REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','') 

--GetTheRestOfTheDataWeNeed
INSERT INTO #OrganisationUpdate (MatterNumber, OrganisationInvolvement, Telephone1, Address, PostCode, GroupNo)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST(CASE REPLACE(LTRIM(RTRIM(OrganisationInvolvement)),'"','') WHEN 'FILE_CLHIRECOMPOFFICE' THEN 'CHO' WHEN 'FILE_CLAIMSOLSOFFICE' THEN 'ClaimantSolicitor' WHEN 'FILE_CLAIMINSROFFICE' THEN 'ClaimantInsurer' WHEN 'FILE_CLAIMBROKERSOFFICE' THEN 'ClaimantBroker' WHEN 'FILE_GENERALOFFICE' THEN 'GeneralParty'  END AS VARCHAR(50)) OrganisationInvolvement
		,REPLACE(REPLACE(LTRIM(RTRIM(CAST(LEFT(Telephone1,50) AS VARCHAR(50)))),'"',''),' ','') Telephone1
		,CAST(LEFT(UPPER(ISNULL(REPLACE(LTRIM(RTRIM(Address1)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address2)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address3)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address4)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address5)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address6)),'"','')  + ' ','')) ,225) AS VARCHAR(225)) Address
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(REPLACE(PostCode,' ',''),20) AS VARCHAR(20)))),'"','') PostCode
		,REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','')  GroupNo
FROM OPENQUERY	(
				VF_Stage,
				'SELECT  md.Matter_Code AS MatterNumber
						,md.Mt_Int_Code
						,ISNULL(mai.Mt_Add_In_Int_Code, md.Mt_Int_Code) AS Owner_Code
						,ISNULL(mai.Add_In_Code, md.Mt_Type) AS Parent_Code
						,l.From_Subcode_Group_No AS GroupNo
						,l.To_Owner_Code AS Entity_Int_Code
						,R.Relation_Code OrganisationInvolvement 
						,OFFI.OFF04 Address1
						,OFFI.OFF05 Address2
						,OFFI.OFF06 Address3
						,OFFI.OFF07 Address4
						,OFFI.OFF08 Address5
						,OFFI.OFF09 Address6
						,OFFI.OFF10 Postcode
						,CON.CON02 Telephone1
				FROM dbo.Matdb MD
				LEFT JOIN dbo.Matdb_Add_In MAI ON MD.Mt_Int_Code = MAI.Mt_Int_Code
				INNER JOIN dbo.Link L ON MD.Mt_Int_Code = L.From_Owner_Code
				INNER JOIN dbo.Entity E	ON L.To_Owner_Code = E.Entity_Int_Code AND E.Sort_Field <> ''''	
				INNER JOIN dbo.Relation	R ON L.Relation_Int_Code = R.Relation_Int_Code
				--JoinOnToTheRequiredTables
				LEFT JOIN [dbo].[UDDETAIL_OFF] OFFI ON OFFI.Owner_Code = l.To_Owner_Code AND OFFI.PO_Type_Char = L.To_Owner_Type_Char AND OFFI.Parent_Code = E.Entity_Type_Code
				LEFT JOIN [dbo].[UDDETAIL_CON] CON ON CON.Owner_Code = l.To_Owner_Code AND CON.PO_Type_Char = L.To_Owner_Type_Char AND CON.Parent_Code = E.Entity_Type_Code
				WHERE ISNULL(mai.Add_In_Code, md.Mt_Type) = ''DFPI''
				AND R.Relation_Code IN (''FILE_CLAIMBROKERSOFFICE'',''FILE_CLAIMINSROFFICE'',''FILE_CLHIRECOMPOFFICE'',''FILE_GENERALOFFICE'',''FILE_CLAIMSOLSOFFICE'')'
				)
ORDER BY REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"',''), CAST(CASE REPLACE(LTRIM(RTRIM(OrganisationInvolvement)),'"','') WHEN 'FILE_CLHIRECOMPOFFICE' THEN 'CHO' WHEN 'FILE_CLAIMSOLSOFFICE' THEN 'ClaimantSolicitor' WHEN 'FILE_CLAIMINSROFFICE' THEN 'ClaimantInsurer' WHEN 'FILE_CLAIMBROKERSOFFICE' THEN 'ClaimantBroker' WHEN 'FILE_GENERALOFFICE' THEN 'GeneralParty'  END AS VARCHAR(50)), REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','') 

INSERT INTO #OrganisationUpdate (MatterNumber, OrganisationInvolvement, Telephone1, Address, PostCode, GroupNo)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST(CASE REPLACE(LTRIM(RTRIM(OrganisationInvolvement)),'"','') WHEN 'FILE_HIRECOMPANY' THEN 'CHO' WHEN 'FILE_ACCIDENTMANAGEMENT' THEN 'Repairer' WHEN 'FILE_SCUENGINEER' THEN 'Engineer' WHEN 'FILE_RECOVERYSTORAGE' THEN 'RecoverySalvageAgent' END AS VARCHAR(50)) OrganisationInvolvement
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(Telephone1,50) AS VARCHAR(50)))),'"','') Telephone1
		,CAST(LEFT(UPPER(ISNULL(REPLACE(LTRIM(RTRIM(Address1)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address2)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address3)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address4)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address5)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address6)),'"','')  + ' ','')) ,225) AS VARCHAR(225)) Address
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(REPLACE(PostCode,' ',''),20) AS VARCHAR(20)))),'"','') PostCode
		,REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','')  GroupNo
FROM OPENQUERY	(
				VF_Stage,
				'SELECT  md.Matter_Code AS MatterNumber
						,md.Mt_Int_Code
						,ISNULL(mai.Mt_Add_In_Int_Code, md.Mt_Int_Code) AS Owner_Code
						,ISNULL(mai.Add_In_Code, md.Mt_Type) AS Parent_Code
						,l.From_Subcode_Group_No AS GroupNo
						,l.To_Owner_Code AS Entity_Int_Code
						,R.Relation_Code OrganisationInvolvement 
						,OFFI.OFF04 Address1
						,OFFI.OFF05 Address2
						,OFFI.OFF06 Address3
						,OFFI.OFF07 Address4
						,OFFI.OFF08 Address5
						,OFFI.OFF09 Address6
						,OFFI.OFF10 Postcode
						,CON.CON02 Telephone1
				FROM dbo.Matdb MD
				LEFT JOIN dbo.Matdb_Add_In MAI ON MD.Mt_Int_Code = MAI.Mt_Int_Code
				INNER JOIN dbo.Link L ON MD.Mt_Int_Code = L.From_Owner_Code
				INNER JOIN dbo.Entity E	ON L.To_Owner_Code = E.Entity_Int_Code AND E.Sort_Field <> ''''	
				INNER JOIN dbo.Relation	R ON L.Relation_Int_Code = R.Relation_Int_Code
				INNER JOIN dbo.Link L1 ON  L1.From_Owner_Code = L.To_Owner_Code
				INNER JOIN dbo.Relation	R1 ON L1.Relation_Int_Code = R1.Relation_Int_Code AND R1.Relation_Code = ''COMM_SEARCHOFFICE''
				INNER JOIN dbo.Entity E1 ON L1.To_Owner_Code = E1.Entity_Int_Code AND E1.Sort_Field <> ''''
				INNER JOIN [dbo].[UDDETAIL_OFF] OFFI ON OFFI.Owner_Code = l1.To_Owner_Code AND OFFI.PO_Type_Char = L1.To_Owner_Type_Char AND OFFI.Parent_Code = E1.Entity_Type_Code
				INNER JOIN [dbo].[UDDETAIL_CON] CON ON CON.Owner_Code = l1.To_Owner_Code AND CON.PO_Type_Char = L1.To_Owner_Type_Char AND CON.Parent_Code = E1.Entity_Type_Code
				WHERE ISNULL(mai.Add_In_Code, md.Mt_Type) = ''DFPI''
				AND R.Relation_Code IN (''FILE_HIRECOMPANY'',''FILE_ACCIDENTMANAGEMENT'',''FILE_SCUENGINEER'',''FILE_RECOVERYSTORAGE'')'
				)
ORDER BY REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"',''), CAST(CASE REPLACE(LTRIM(RTRIM(OrganisationInvolvement)),'"','') WHEN 'FILE_HIRECOMPANY' THEN 'CHO' WHEN 'FILE_ACCIDENTMANAGEMENT' THEN 'Repairer' WHEN 'FILE_SCUENGINEER' THEN 'Engineer' WHEN 'FILE_RECOVERYSTORAGE' THEN 'RecoverySalvageAgent' END AS VARCHAR(50)), REPLACE(LTRIM(RTRIM(CAST(ISNULL(GroupNo,1) AS SMALLINT))),'"','') 

UPDATE ORG
SET  Telephone1	= OU.Telephone1
	,Address	= OU.Address
	,PostCode	= OU.PostCode
FROM #Organisation ORG
INNER JOIN #OrganisationUpdate OU ON OU.MatterNumber = ORG.MatterNumber AND OU.OrganisationInvolvement = ORG.OrganisationInvolvement AND OU.GroupNo = ORG.GroupNo 

------------------------------------
--NowUpdateTheRelationships
------------------------------------
--SetTheClaimStatus
UPDATE CLA
SET ClaimStatus = CASE WHEN MatterClosed = 1 OR HasAOutcome = 1 THEN 'Negotiated Settlement' ELSE NULL END
FROM #Claim CLA
LEFT JOIN (
			SELECT	 ROW_NUMBER() OVER (PARTITION BY MatterNumber ORDER BY CAST(CASE WHEN ClaimStatus IS NOT NULL THEN 1 ELSE 0 END AS BIT) ASC) RowID 
					,MatterNumber
					,CAST(CASE WHEN ClaimStatus IS NOT NULL THEN 1 ELSE 0 END AS BIT) HasAOutcome
			FROM #Person
			WHERE PersonInvolvement = 'Claimant'
			GROUP BY MatterNumber
					,CAST(CASE WHEN ClaimStatus IS NOT NULL THEN 1 ELSE 0 END AS BIT)
			) UPD ON UPD.MatterNumber = CLA.MatterNumber AND RowID = 1
 
--SetTheIDValues
UPDATE VEH
SET VehicleID = Data.VehicleID
FROM #Vehicle VEH
INNER JOIN	(
			SELECT ID, ROW_NUMBER() OVER (PARTITION BY MatterNumber ORDER BY VehicleInvolvement, GroupNo) VehicleID
			FROM #Vehicle
			) Data ON Data.ID = VEH.ID

UPDATE PER
SET PersonID = Data.PersonID
FROM #Person PER
INNER JOIN	(
			SELECT ID, ROW_NUMBER() OVER (PARTITION BY MatterNumber ORDER BY PersonInvolvement) PersonID
			FROM #Person
			) Data ON Data.ID = PER.ID

UPDATE ORG
SET OrganisationID = Data.OrganisationID
FROM #Organisation ORG
INNER JOIN	(
			SELECT ID, ROW_NUMBER() OVER (PARTITION BY MatterNumber ORDER BY OrganisationInvolvement, GroupNo) OrganisationID
			FROM #Organisation
			) Data ON Data.ID = ORG.ID

--AddSomeKeys
ALTER TABLE #Vehicle ALTER COLUMN VehicleID SMALLINT NOT NULL
ALTER TABLE #Person ALTER COLUMN PersonID SMALLINT NOT NULL
ALTER TABLE #Organisation ALTER COLUMN OrganisationID SMALLINT NOT NULL

ALTER TABLE #Vehicle ADD CONSTRAINT PK_#Vehicle_MatterNumber_VehicleID PRIMARY KEY (MatterNumber, VehicleID)
ALTER TABLE #Person ADD CONSTRAINT PK_#Person_MatterNumber_PersonID PRIMARY KEY (MatterNumber, PersonID)
ALTER TABLE #Organisation ADD CONSTRAINT PK_#Organisation_MatterNumber_OrganisationID PRIMARY KEY (MatterNumber, OrganisationID)

--LinkTheDatasetsTogether
UPDATE PER
SET VehicleID = CASE 
					WHEN Per.PersonInvolvement IN ('InsuredDriver','Insured') AND VEH.VehicleInvolvement = 'InsuredVehicle' THEN VEH.VehicleID
					WHEN Per.PersonInvolvement IN ('Claimant') AND VEH.VehicleInvolvement = 'ThirdPartyVehicle' THEN VEH.VehicleID
				END
FROM #Person PER
INNER JOIN #Vehicle VEH ON VEH.MatterNumber = Per.MatterNumber
WHERE CASE WHEN Per.PersonInvolvement IN ('InsuredDriver','Insured') AND VEH.VehicleInvolvement = 'InsuredVehicle' THEN VEH.VehicleID WHEN Per.PersonInvolvement IN ('Claimant') AND VEH.VehicleInvolvement = 'ThirdPartyVehicle' THEN VEH.VehicleID END IS NOT NULL

UPDATE ORG
SET PersonID = CASE WHEN Per.PersonInvolvement IN ('Claimant') AND ORG.OrganisationInvolvement IN ('ClaimantInsurer','ClaimantSolicitor','ClaimantBroker','ClaimantAlternativeRepresentative') AND Per.GroupNo = ORG.GroupNo THEN PER.PersonID END
FROM #Organisation ORG
INNER JOIN #Person PER ON PER.MatterNumber = ORG.MatterNumber 
WHERE CASE WHEN Per.PersonInvolvement IN ('Claimant') AND ORG.OrganisationInvolvement IN ('ClaimantInsurer','ClaimantSolicitor','ClaimantBroker','ClaimantAlternativeRepresentative') AND Per.GroupNo = ORG.GroupNo THEN PER.PersonID END IS NOT NULL

UPDATE VEH
SET OrganisationID = CASE WHEN VEH.VehicleInvolvement IN ('HireVehicle') AND ORG.OrganisationInvolvement IN ('CHO') AND VEH.GroupNo = ORG.GroupNo THEN ORG.OrganisationID END
FROM #Vehicle VEH
INNER JOIN #Organisation ORG ON ORG.MatterNumber = VEH.MatterNumber
WHERE CASE WHEN VEH.VehicleInvolvement IN ('HireVehicle') AND ORG.OrganisationInvolvement IN ('CHO') AND VEH.GroupNo = ORG.GroupNo THEN ORG.OrganisationID END IS NOT NULL

UPDATE VEH
SET OrganisationID = CASE WHEN VEH.OrganisationID IS NULL AND VEH.VehicleInvolvement IN ('HireVehicle') AND ORG.OrganisationInvolvement IN ('CHO') THEN ORG.OrganisationID  END
FROM #Vehicle VEH
INNER JOIN #Organisation ORG ON ORG.MatterNumber = VEH.MatterNumber
WHERE CASE WHEN VEH.OrganisationID IS NULL AND VEH.VehicleInvolvement IN ('HireVehicle') AND ORG.OrganisationInvolvement IN ('CHO') THEN ORG.OrganisationID END IS NOT NULL
AND VEH.OrganisationID IS NULL

UPDATE ORG
SET PersonID = PER.PersonID
FROM #Organisation ORG
INNER JOIN #Vehicle VEH ON VEH.OrganisationID = ORG.OrganisationID AND VEH.MatterNumber = ORG.MatterNumber
INNER JOIN #Person PER ON PER.GroupNo = VEH.ClaimantGroupNo AND PER.MatterNumber = VEH.MatterNumber
WHERE OrganisationInvolvement = 'CHO'
AND PER.PersonInvolvement = 'Claimant'

--ThereAreInstancesWhereAPersonDoesNotBelongToAVehicleSoWeHaveToCreateThisAtMatterLevel
DECLARE @NoPersonToVehicleLink TABLE (MatterNumber INT, PersonID INT, NewVehicleID INT)
INSERT INTO @NoPersonToVehicleLink (MatterNumber, PersonID, NewVehicleID)
SELECT PER.MatterNumber, PersonID, ISNULL(NewVehicleID,1)
FROM #Person PER
LEFT JOIN	(
			SELECT MatterNumber, MAX(ISNULL(VehicleID,0)) + 1 NewVehicleID
			FROM #Vehicle
			GROUP BY MatterNumber
			) VEH ON VEH.MatterNumber = PER.MatterNumber
WHERE VehicleID IS NULL
ORDER BY PER.MatterNumber, PersonID

INSERT INTO #Vehicle (MatterNumber, VehicleInvolvement, VehicleID)
SELECT MatterNumber, 'NoVehicleInvolved', NewVehicleID
FROM @NoPersonToVehicleLink
GROUP BY MatterNumber, NewVehicleID

UPDATE PER
SET VehicleID = VEH.NewVehicleID
FROM #Person PER
INNER JOIN @NoPersonToVehicleLink VEH ON PER.MatterNumber = VEH.MatterNumber AND PER.PersonID = VEH.PersonID

--InTheCaseOfhireVehiclesWeCanEndupWithOrganisationsThatAreTheSame,WeNeedToCleanThisLit
--GetTheDuplicates
DECLARE @DuplicateOrgs TABLE (RowID INT, MatterNumber INT, OrganisationID INT)
INSERT INTO @DuplicateOrgs (RowID, MatterNumber, OrganisationID )
SELECT ORG.ID, ORG.MatterNumber, ORG.OrganisationID
FROM #Organisation ORG
INNER JOIN	(
			SELECT MatterNumber, PersonID
			FROM #Organisation
			WHERE OrganisationInvolvement = 'CHO'
			GROUP BY MatterNumber, PersonID, OrganisationInvolvement, Telephone1, Address, PostCode
			HAVING COUNT(1) > 1
			) Duplicates ON Duplicates.MatterNumber = ORG.MatterNumber AND Duplicates.PersonID = ORG.PersonID 
WHERE ORG.OrganisationInvolvement = 'CHO'
ORDER BY ORG.MatterNumber

--UpdateTheVehicleIDLink
UPDATE VEH
SET OrganisationID = OrgIDToKeep
FROM #Vehicle VEH
INNER JOIN @DuplicateOrgs DO ON DO.MatterNumber = VEH.MatterNumber AND VEH.OrganisationID = DO.OrganisationID
INNER JOIN (
			SELECT MatterNumber, MIN(OrganisationID) OrgIDToKeep
			FROM @DuplicateOrgs
			GROUP BY MatterNumber
			) DO1 ON DO1.MatterNumber = DO.MatterNumber
INNER JOIN (
			SELECT MatterNumber, OrganisationID
			FROM @DuplicateOrgs
			) DO2 ON DO2.MatterNumber = VEH.MatterNumber AND DO2.OrganisationID = VEH.OrganisationID
WHERE VEH.OrganisationID != OrgIDToKeep

--DeleteTheOrgs
DELETE ORG
FROM #Organisation ORG
INNER JOIN (
			SELECT MatterNumber, MIN(OrganisationID) OrgIDToKeep
			FROM @DuplicateOrgs
			GROUP BY MatterNumber
			) DO1 ON DO1.MatterNumber = ORG.MatterNumber
INNER JOIN (
			SELECT MatterNumber, OrganisationID
			FROM @DuplicateOrgs
			) DO2 ON DO2.MatterNumber = ORG.MatterNumber AND DO2.OrganisationID = ORG.OrganisationID
WHERE ORG.OrganisationID != OrgIDToKeep
AND ORG.OrganisationInvolvement = 'CHO'

------------------------------
--SetTheHashByte
------------------------------
UPDATE #Claim
SET Hashbyte = HASHBYTES('MD5',CAST(ISNULL(MatterNumber,'') AS VARCHAR(20))+CONVERT(VARCHAR(20),ISNULL(IncidentDate,''),103)+ISNULL(Insurer,'')+ISNULL(ClaimStatus,'')+ISNULL(InsurerClientReference,'')+ISNULL(IncidentCircumstances,'')+CAST(ISNULL(CreditHire,'') AS VARCHAR(1))+CAST(ISNULL(ProceedingsIssued,'') AS VARCHAR(1))+CAST(ISNULL(CFS,'') AS VARCHAR(1)))

UPDATE #Vehicle
SET Hashbyte = HASHBYTES('MD5',CAST(ISNULL(MatterNumber,'') AS VARCHAR(20))+ISNULL(VehicleInvolvement,'')+ISNULL(VehicleRegistration,'')+ISNULL(VehicleMake,'')+ISNULL(VehicleModel,'')+CONVERT(VARCHAR(20),ISNULL(DateHireStarted,''),103)+CONVERT(VARCHAR(20),ISNULL(DateHireEnded,''),103)+CAST(ISNULL(GROUPNO,'') AS VARCHAR(3)))

UPDATE #Person
SET Hashbyte = HASHBYTES('MD5',CAST(ISNULL(MatterNumber,'') AS VARCHAR(20))+ISNULL(PersonInvolvement,'')+ISNULL(Salutation,'')+ISNULL(FirstName,'')+ISNULL(LastName,'')+CONVERT(VARCHAR(20),ISNULL(DateOfBirth,''),103)+ISNULL(Gender,'')+ISNULL(Salutation,'')+ISNULL(NINumber,'')+ISNULL(LandlineTelephone,'')+ISNULL(MobileTelephone,'')+ISNULL(Address,'')+ISNULL(PostCode,''))

UPDATE #Organisation
SET Hashbyte = HASHBYTES('MD5',CAST(ISNULL(MatterNumber,'') AS VARCHAR(20))+ISNULL(OrganisationInvolvement,'')+ISNULL(CompanyName,'')+ISNULL(Telephone1,'')+ISNULL(Address,'')+ISNULL(PostCode,'')+CAST(ISNULL(GroupNo,'') AS VARCHAR(3)))

-----------------------------
--AddAnyRecordModificationsToTheTables
--OutputTheseToAnAuditTable
-----------------------------
--DFPI_Claim
MERGE [Extract].[Claim] AS [Target]
USING	(
		SELECT Hashbyte, MatterNumber, Insurer, IncidentDate, ClaimStatus, InsurerClientReference, IncidentCircumstances, CreditHire, ProceedingsIssued, CFS
		FROM #Claim TC
		) AS Source ON Source.Hashbyte = [Target].ClaimHashbyte
WHEN NOT MATCHED BY TARGET THEN 
	INSERT (ClaimHashbyte, CreationModificationDate, RecordSourceID, ProductTypeID, MatterNumber, Insurer, IncidentDate, ClaimStatus, InsurerClientReference, IncidentCircumstances, CreditHire, ProceedingsIssued,CFS)
	VALUES (Source.Hashbyte, @Now, @RecordSourceID, @ProductTypeID, Source.MatterNumber, Source.Insurer, Source.IncidentDate, Source.ClaimStatus, Source.InsurerClientReference, Source.IncidentCircumstances, Source.CreditHire, Source.ProceedingsIssued, Source.CFS)
WHEN NOT MATCHED BY SOURCE AND TARGET.[ProductTypeID] = @ProductTypeID THEN
	DELETE
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.ClaimHashbyte,Deleted.ClaimHashbyte), ISNULL(Inserted.IncludeRecord,Deleted.IncludeRecord), ISNULL(Inserted.ExcludeRecord,Deleted.ExcludeRecord), ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.RecordSourceID,Deleted.RecordSourceID), ISNULL(Inserted.ProductTypeID,Deleted.ProductTypeID), ISNULL(Inserted.MatterNumber,Deleted.MatterNumber), ISNULL(Inserted.Insurer,Deleted.Insurer), ISNULL(Inserted.IncidentDate,Deleted.IncidentDate), ISNULL(Inserted.ClaimStatus,Deleted.ClaimStatus), ISNULL(Inserted.InsurerClientReference,Deleted.InsurerClientReference), ISNULL(Inserted.IncidentCircumstances,Deleted.IncidentCircumstances), ISNULL(Inserted.CreditHire,Deleted.CreditHire), ISNULL(Inserted.ProceedingsIssued,Deleted.ProceedingsIssued), ISNULL(Inserted.CFS,Deleted.CFS)
INTO [Audit].[Claim] (DMLAction, ExecutionDateTime, ClaimRowID, ClaimHashbyte, IncludeRecord, ExcludeRecord, CreationModificationDate, RecordSourceID, ProductTypeID, MatterNumber, Insurer, IncidentDate, ClaimStatus, InsurerClientReference, IncidentCircumstances, CreditHire, ProceedingsIssued ,CFS);

--DFPI_Vehicle
MERGE [Extract].[Vehicle] AS [Target]
USING	(
		SELECT Hashbyte, MatterNumber, VehicleID, OrganisationID, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, DateHireStarted, DateHireEnded 
		FROM #Vehicle TV
		) AS Source ON Source.Hashbyte = [Target].VehicleHashbyte
WHEN NOT MATCHED BY TARGET THEN 
	INSERT (VehicleHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, VehicleID, OrganisationID, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, DateHireStarted, DateHireEnded)
	VALUES (Source.Hashbyte, @Now, @ProductTypeID, Source.MatterNumber, Source.VehicleID, Source.OrganisationID, Source.VehicleInvolvement, Source.VehicleRegistration, Source.VehicleMake, Source.VehicleModel, Source.DateHireStarted, Source.DateHireEnded)
WHEN NOT MATCHED BY SOURCE AND TARGET.[ProductTypeID] = @ProductTypeID THEN
	DELETE
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.VehicleHashbyte,Deleted.VehicleHashbyte), ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.ProductTypeID,Deleted.ProductTypeID), ISNULL(Inserted.MatterNumber,Deleted.MatterNumber), ISNULL(Inserted.VehicleID,Deleted.VehicleID), ISNULL(Inserted.OrganisationID,Deleted.OrganisationID), ISNULL(Inserted.VehicleInvolvement,Deleted.VehicleInvolvement), ISNULL(Inserted.VehicleRegistration,Deleted.VehicleRegistration), ISNULL(Inserted.VehicleMake,Deleted.VehicleMake), ISNULL(Inserted.VehicleModel,Deleted.VehicleModel), ISNULL(Inserted.DateHireStarted,Deleted.DateHireStarted), ISNULL(Inserted.DateHireEnded,Deleted.DateHireEnded)
INTO [Audit].[Vehicle] (DMLAction, ExecutionDateTime, VehicleRowID, VehicleHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, VehicleID, OrganisationID, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, DateHireStarted, DateHireEnded);

--DFPI_Person
MERGE [Extract].[Person] AS [Target]
USING	(
		SELECT Hashbyte, MatterNumber, VehicleID, PersonID, PersonInvolvement, Salutation, FirstName, LastName, DateOfBirth, Gender, NINumber, LandlineTelephone, MobileTelephone, Address, PostCode
		FROM #Person TP
		) AS Source ON Source.Hashbyte = [Target].PersonHashbyte
WHEN NOT MATCHED BY TARGET THEN 
	INSERT (PersonHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, VehicleID, PersonID, PersonInvolvement, Salutation, FirstName, LastName, DateOfBirth, Gender, NINumber, LandlineTelephone, MobileTelephone, Address, PostCode)
	VALUES (Source.Hashbyte, @Now, @ProductTypeID, Source.MatterNumber, Source.VehicleID, Source.PersonID, Source.PersonInvolvement, Source.Salutation, Source.FirstName, Source.LastName, Source.DateOfBirth, Source.Gender, Source.NINumber, Source.LandlineTelephone, Source.MobileTelephone, Source.Address, Source.PostCode)
WHEN NOT MATCHED BY SOURCE AND TARGET.[ProductTypeID] = @ProductTypeID THEN
	DELETE
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.PersonHashbyte,Deleted.PersonHashbyte),  ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.ProductTypeID,Deleted.ProductTypeID), ISNULL(Inserted.MatterNumber,Deleted.MatterNumber), ISNULL(Inserted.VehicleID,Deleted.VehicleID), ISNULL(Inserted.PersonID,Deleted.PersonID), ISNULL(Inserted.PersonInvolvement,Deleted.PersonInvolvement), ISNULL(Inserted.Salutation,Deleted.Salutation), ISNULL(Inserted.FirstName,Deleted.FirstName), ISNULL(Inserted.LastName,Deleted.LastName), ISNULL(Inserted.DateOfBirth,Deleted.DateOfBirth), ISNULL(Inserted.Gender,Deleted.Gender), ISNULL(Inserted.NINumber,Deleted.NINumber), ISNULL(Inserted.LandlineTelephone,Deleted.LandlineTelephone), ISNULL(Inserted.MobileTelephone,Deleted.MobileTelephone), ISNULL(Inserted.Address,Deleted.Address), ISNULL(Inserted.PostCode,Deleted.PostCode)
INTO [Audit].[Person] (DMLAction, ExecutionDateTime, PersonRowID, PersonHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, VehicleID, PersonID, PersonInvolvement, Salutation, FirstName, LastName, DateOfBirth, Gender, NINumber, LandlineTelephone, MobileTelephone, Address, PostCode);

--DFPI_Organisation
MERGE [Extract].[Organisation] AS [Target]
USING	(
		SELECT Hashbyte, MatterNumber, OrganisationID, PersonID, OrganisationInvolvement, CompanyName, Telephone1, Address, PostCode 
		FROM #Organisation TOR
		) AS Source ON Source.Hashbyte = [Target].OrganisationHashbyte
WHEN NOT MATCHED BY TARGET THEN 
	INSERT (OrganisationHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, OrganisationID, PersonID, OrganisationInvolvement, CompanyName, Telephone1, Address, PostCode)
	VALUES (Source.Hashbyte, @Now, @ProductTypeID, Source.MatterNumber, Source.OrganisationID, Source.PersonID, Source.OrganisationInvolvement, Source.CompanyName, Source.Telephone1, Source.Address, Source.PostCode)
WHEN NOT MATCHED BY SOURCE AND TARGET.[ProductTypeID] = @ProductTypeID THEN
	DELETE
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.OrganisationHashbyte,Deleted.OrganisationHashbyte), ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.ProductTypeID,Deleted.ProductTypeID), ISNULL(Inserted.MatterNumber,Deleted.MatterNumber), ISNULL(Inserted.OrganisationID,Deleted.OrganisationID), ISNULL(Inserted.PersonID,Deleted.PersonID), ISNULL(Inserted.OrganisationInvolvement,Deleted.OrganisationInvolvement), ISNULL(Inserted.CompanyName,Deleted.CompanyName), ISNULL(Inserted.Telephone1,Deleted.Telephone1), ISNULL(Inserted.Address,Deleted.Address), ISNULL(Inserted.PostCode,Deleted.PostCode)
INTO [Audit].[Organisation] (DMLAction, ExecutionDateTime, OrganisationRowID, OrganisationHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, OrganisationID, PersonID, OrganisationInvolvement, CompanyName, Telephone1, Address, PostCode);

------------------------------------
--Tidy
------------------------------------
IF OBJECT_ID('tempdb..#Claim') IS NOT NULL
	DROP TABLE #Claim

IF OBJECT_ID('tempdb..#Vehicle') IS NOT NULL
	DROP TABLE #Vehicle

IF OBJECT_ID('tempdb..#Person') IS NOT NULL
	DROP TABLE #Person

IF OBJECT_ID('tempdb..#Organisation') IS NOT NULL
	DROP TABLE #Organisation

IF OBJECT_ID('tempdb..#OrganisationUpdate') IS NOT NULL
	DROP TABLE #OrganisationUpdate