﻿CREATE PROCEDURE [Extract].[uspAceExtractAndLoad]
AS

--AddAuditing, ControlRunAudit
DECLARE @Now DATETIME = GETDATE()

--------------------------------
--SetTheHasbytesValues
--------------------------------
UPDATE [Import].[Claim_ACE]
SET [ClaimHashbyte] = HASHBYTES('MD5',ISNULL(ClaimNumber,'')+ ISNULL(Title,'')+ ISNULL(CustomerForname,'')+ ISNULL(CustomerSurname,'')+ ISNULL(ClaimHistoryCounter,'')+ ISNULL(PolicyNumber,'')+ ISNULL(InsuredHandsetMake,'')+ ISNULL(InsuredHandsetModel,'')+ ISNULL(InsuredHandsetIMEI,'')+ CONVERT(VARCHAR,ISNULL(HandsetPrice,''))+ ISNULL(UpdatedProductMake,'')+ ISNULL(UpdatedProductModel,'')+ ISNULL(UpdatedProductIMEI,'')+ ISNULL(CustomerAddressLine1,'')+ ISNULL(CustomerAddressLine2,'')+ ISNULL(CustomerAddressLine3,'')+ ISNULL(CustomerAddressLine4,'')+ ISNULL(S6PostCode,'')+ ISNULL(CustomerEmail,'')+ ISNULL(AlternateEmail,'')+ ISNULL(MobileNumber,'')+ ISNULL(IncidentType,'')+ ISNULL(CauseOfLoss,'')+ ISNULL(IncidentLocation,'')+ ISNULL(DeviceFault,'')+ CONVERT(VARCHAR(20),ISNULL(IncidentDate,''),103)+ CONVERT(VARCHAR(20),ISNULL(ClaimNotificationDate,''),103)+ CONVERT(VARCHAR(20),ISNULL(PoliceNotifiedDate,''),103)+ ISNULL(CrimeRefNumber,'')+ ISNULL(PoliceStation,'')+ ISNULL(BypassFraud,'')+ ISNULL(ReplacementPhoneMake,'')+ ISNULL(ReplacementPhoneModel,'')+ ISNULL(ReplacementPhoneIMEI,'')+ ISNULL(ReplacementPhoneGrade,'')+ ISNULL(DeliveryAddressLine1,'')+ ISNULL(DeliveryAddressLine2,'')+ ISNULL(DeliveryAddressLine3,'')+ ISNULL(DeliveryAddressLine4,'')+ ISNULL(DeliveryPostCode,'')+ ISNULL(PickUpAddress1,'')+ ISNULL(PickUpAddress2,'')+ ISNULL(PickUpAddress3,'')+ ISNULL(PickUpAddress4,'')+ ISNULL(PickUpCity,'')+ ISNULL(PickUpPostCode,'')+ ISNULL(JiffyBagAddress1,'')+ ISNULL(JiffyBagAddress2,'')+ ISNULL(JiffyBagAddress3,'')+ ISNULL(JiffyBagAddress4,'')+ ISNULL(JiffyBagCity,'')+ ISNULL(JiffyBagPostCode,'')+ CONVERT(VARCHAR,ISNULL(TotalClaimCost,''))+ CONVERT(VARCHAR,ISNULL(TotalLessExcessCost,''))+ ISNULL(ClaimStatus,''));

UPDATE [Import].[Policy_ACE]
SET [PolicyHashbyte] = HASHBYTES('MD5',ISNULL(Policy,'')+ CONVERT(VARCHAR(20),ISNULL(StartDate,''),103)+ CONVERT(VARCHAR,ISNULL(GrossPremium,''))+ ISNULL(FirstName,'')+ ISNULL(Surname,'')+ CONVERT(VARCHAR(20),ISNULL(DateOfBirth,''),103)+ ISNULL(CompanyName,'')+ ISNULL(AddressLine1,'')+ ISNULL(AddressLine2,'')+ ISNULL(AddressLine3,'')+ ISNULL(AddressLine4,'')+ ISNULL(PostCode,'')+ CONVERT(VARCHAR(20),ISNULL(CancelDate,''),103)+ ISNULL(ProductInsured,'')+ ISNULL(MobileNumber,'')+ ISNULL(IMEI,'')+ ISNULL(CustomerType,''));

UPDATE [Extract].[Claim_ACE]
SET IncludeRecord = 0
WHERE IncludeRecord !=0

UPDATE [Extract].[Claim_ACE]
SET ExcludeRecord = 0
WHERE ExcludeRecord !=0

---------------------------
--MergeTheDeltas
---------------------------
MERGE [Extract].[Claim_ACE] AS TARGET
USING   (
		SELECT	ClaimNumber, Title, CustomerForname, CustomerSurname, ClaimHistoryCounter, PolicyNumber, InsuredHandsetMake, InsuredHandsetModel, InsuredHandsetIMEI, HandsetPrice, UpdatedProductMake, UpdatedProductModel, UpdatedProductIMEI, CustomerAddressLine1, CustomerAddressLine2, CustomerAddressLine3, CustomerAddressLine4, S6PostCode, CustomerEmail, AlternateEmail, MobileNumber, IncidentType, CauseOfLoss, IncidentLocation, DeviceFault, IncidentDate, ClaimNotificationDate, PoliceNotifiedDate, CrimeRefNumber, PoliceStation, BypassFraud, ReplacementPhoneMake, ReplacementPhoneModel, ReplacementPhoneIMEI, ReplacementPhoneGrade, DeliveryAddressLine1, DeliveryAddressLine2, DeliveryAddressLine3, DeliveryAddressLine4, DeliveryPostCode, PickUpAddress1, PickUpAddress2, PickUpAddress3, PickUpAddress4, PickUpCity, PickUpPostCode, JiffyBagAddress1, JiffyBagAddress2, JiffyBagAddress3, JiffyBagAddress4, JiffyBagCity, JiffyBagPostCode, TotalClaimCost, TotalLessExcessCost, ClaimStatus, ClaimHashbyte
		FROM	(
				SELECT ROW_NUMBER() OVER (PARTITION BY ClaimNumber ORDER BY RowID DESC) RowNumber, ClaimNumber, Title, CustomerForname, CustomerSurname, ClaimHistoryCounter, PolicyNumber, InsuredHandsetMake, InsuredHandsetModel, InsuredHandsetIMEI, HandsetPrice, UpdatedProductMake, UpdatedProductModel, UpdatedProductIMEI, CustomerAddressLine1, CustomerAddressLine2, CustomerAddressLine3, CustomerAddressLine4, S6PostCode, CustomerEmail, AlternateEmail, MobileNumber, IncidentType, CauseOfLoss, IncidentLocation, DeviceFault, IncidentDate, ClaimNotificationDate, PoliceNotifiedDate, CrimeRefNumber, PoliceStation, BypassFraud, ReplacementPhoneMake, ReplacementPhoneModel, ReplacementPhoneIMEI, ReplacementPhoneGrade, DeliveryAddressLine1, DeliveryAddressLine2, DeliveryAddressLine3, DeliveryAddressLine4, DeliveryPostCode, PickUpAddress1, PickUpAddress2, PickUpAddress3, PickUpAddress4, PickUpCity, PickUpPostCode, JiffyBagAddress1, JiffyBagAddress2, JiffyBagAddress3, JiffyBagAddress4, JiffyBagCity, JiffyBagPostCode, TotalClaimCost, TotalLessExcessCost, ClaimStatus, ClaimHashbyte
				FROM [Import].[Claim_ACE] DATA
				) Data
		WHERE RowNumber = 1
		) AS Source ON Source.ClaimHashbyte = [Target].ClaimHashbyte
WHEN NOT MATCHED BY TARGET THEN 
	INSERT (ClaimHashbyte, IncludeRecord, ExcludeRecord, ControlRunID, CreationModificationDate, ClaimNumber, Title, CustomerForname, CustomerSurname, ClaimHistoryCounter, PolicyNumber, InsuredHandsetMake, InsuredHandsetModel, InsuredHandsetIMEI, HandsetPrice, UpdatedProductMake, UpdatedProductModel, UpdatedProductIMEI, CustomerAddressLine1, CustomerAddressLine2, CustomerAddressLine3, CustomerAddressLine4, S6PostCode, CustomerEmail, AlternateEmail, MobileNumber, IncidentType, CauseOfLoss, IncidentLocation, DeviceFault, IncidentDate, ClaimNotificationDate, PoliceNotifiedDate, CrimeRefNumber, PoliceStation, BypassFraud, ReplacementPhoneMake, ReplacementPhoneModel, ReplacementPhoneIMEI, ReplacementPhoneGrade, DeliveryAddressLine1, DeliveryAddressLine2, DeliveryAddressLine3, DeliveryAddressLine4, DeliveryPostCode, PickUpAddress1, PickUpAddress2, PickUpAddress3, PickUpAddress4, PickUpCity, PickUpPostCode, JiffyBagAddress1, JiffyBagAddress2, JiffyBagAddress3, JiffyBagAddress4, JiffyBagCity, JiffyBagPostCode, TotalClaimCost, TotalLessExcessCost, ClaimStatus)
	VALUES (ClaimHashbyte, 1, 0, NULL, @Now, ClaimNumber, Title, CustomerForname, CustomerSurname, ClaimHistoryCounter, PolicyNumber, InsuredHandsetMake, InsuredHandsetModel, InsuredHandsetIMEI, HandsetPrice, UpdatedProductMake, UpdatedProductModel, UpdatedProductIMEI, CustomerAddressLine1, CustomerAddressLine2, CustomerAddressLine3, CustomerAddressLine4, S6PostCode, CustomerEmail, AlternateEmail, MobileNumber, IncidentType, CauseOfLoss, IncidentLocation, DeviceFault, IncidentDate, ClaimNotificationDate, PoliceNotifiedDate, CrimeRefNumber, PoliceStation, BypassFraud, ReplacementPhoneMake, ReplacementPhoneModel, ReplacementPhoneIMEI, ReplacementPhoneGrade, DeliveryAddressLine1, DeliveryAddressLine2, DeliveryAddressLine3, DeliveryAddressLine4, DeliveryPostCode, PickUpAddress1, PickUpAddress2, PickUpAddress3, PickUpAddress4, PickUpCity, PickUpPostCode, JiffyBagAddress1, JiffyBagAddress2, JiffyBagAddress3, JiffyBagAddress4, JiffyBagCity, JiffyBagPostCode, TotalClaimCost, TotalLessExcessCost, ClaimStatus)
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.ClaimHashbyte,Deleted.ClaimHashbyte), ISNULL(Inserted.IncludeRecord,Deleted.IncludeRecord), ISNULL(Inserted.ExcludeRecord,Deleted.ExcludeRecord), ISNULL(Inserted.ControlRunID,Deleted.ControlRunID), ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.ClaimNumber,Deleted.ClaimNumber), ISNULL(Inserted.Title,Deleted.Title), ISNULL(Inserted.CustomerForname,Deleted.CustomerForname), ISNULL(Inserted.CustomerSurname,Deleted.CustomerSurname), ISNULL(Inserted.ClaimHistoryCounter,Deleted.ClaimHistoryCounter), ISNULL(Inserted.PolicyNumber,Deleted.PolicyNumber), ISNULL(Inserted.InsuredHandsetMake,Deleted.InsuredHandsetMake), ISNULL(Inserted.InsuredHandsetModel,Deleted.InsuredHandsetModel), ISNULL(Inserted.InsuredHandsetIMEI,Deleted.InsuredHandsetIMEI), ISNULL(Inserted.HandsetPrice,Deleted.HandsetPrice), ISNULL(Inserted.UpdatedProductMake,Deleted.UpdatedProductMake), ISNULL(Inserted.UpdatedProductModel,Deleted.UpdatedProductModel), ISNULL(Inserted.UpdatedProductIMEI,Deleted.UpdatedProductIMEI), ISNULL(Inserted.CustomerAddressLine1,Deleted.CustomerAddressLine1), ISNULL(Inserted.CustomerAddressLine2,Deleted.CustomerAddressLine2), ISNULL(Inserted.CustomerAddressLine3,Deleted.CustomerAddressLine3), ISNULL(Inserted.CustomerAddressLine4,Deleted.CustomerAddressLine4), ISNULL(Inserted.S6PostCode,Deleted.S6PostCode), ISNULL(Inserted.CustomerEmail,Deleted.CustomerEmail), ISNULL(Inserted.AlternateEmail,Deleted.AlternateEmail), ISNULL(Inserted.MobileNumber,Deleted.MobileNumber), ISNULL(Inserted.IncidentType,Deleted.IncidentType), ISNULL(Inserted.CauseOfLoss,Deleted.CauseOfLoss), ISNULL(Inserted.IncidentLocation,Deleted.IncidentLocation), ISNULL(Inserted.DeviceFault,Deleted.DeviceFault), ISNULL(Inserted.IncidentDate,Deleted.IncidentDate), ISNULL(Inserted.ClaimNotificationDate,Deleted.ClaimNotificationDate), ISNULL(Inserted.PoliceNotifiedDate,Deleted.PoliceNotifiedDate), ISNULL(Inserted.CrimeRefNumber,Deleted.CrimeRefNumber), ISNULL(Inserted.PoliceStation,Deleted.PoliceStation), ISNULL(Inserted.BypassFraud,Deleted.BypassFraud), ISNULL(Inserted.ReplacementPhoneMake,Deleted.ReplacementPhoneMake), ISNULL(Inserted.ReplacementPhoneModel,Deleted.ReplacementPhoneModel), ISNULL(Inserted.ReplacementPhoneIMEI,Deleted.ReplacementPhoneIMEI), ISNULL(Inserted.ReplacementPhoneGrade,Deleted.ReplacementPhoneGrade), ISNULL(Inserted.DeliveryAddressLine1,Deleted.DeliveryAddressLine1), ISNULL(Inserted.DeliveryAddressLine2,Deleted.DeliveryAddressLine2), ISNULL(Inserted.DeliveryAddressLine3,Deleted.DeliveryAddressLine3), ISNULL(Inserted.DeliveryAddressLine4,Deleted.DeliveryAddressLine4), ISNULL(Inserted.DeliveryPostCode,Deleted.DeliveryPostCode), ISNULL(Inserted.PickUpAddress1,Deleted.PickUpAddress1), ISNULL(Inserted.PickUpAddress2,Deleted.PickUpAddress2), ISNULL(Inserted.PickUpAddress3,Deleted.PickUpAddress3), ISNULL(Inserted.PickUpAddress4,Deleted.PickUpAddress4), ISNULL(Inserted.PickUpCity,Deleted.PickUpCity), ISNULL(Inserted.PickUpPostCode,Deleted.PickUpPostCode), ISNULL(Inserted.JiffyBagAddress1,Deleted.JiffyBagAddress1), ISNULL(Inserted.JiffyBagAddress2,Deleted.JiffyBagAddress2), ISNULL(Inserted.JiffyBagAddress3,Deleted.JiffyBagAddress3), ISNULL(Inserted.JiffyBagAddress4,Deleted.JiffyBagAddress4), ISNULL(Inserted.JiffyBagCity,Deleted.JiffyBagCity), ISNULL(Inserted.JiffyBagPostCode,Deleted.JiffyBagPostCode), ISNULL(Inserted.TotalClaimCost,Deleted.TotalClaimCost), ISNULL(Inserted.TotalLessExcessCost,Deleted.TotalLessExcessCost), ISNULL(Inserted.ClaimStatus,Deleted.ClaimStatus)
INTO [Audit].[Claim_ACE] (DMLAction, ExecutionDateTime, ClaimRowID, ClaimHashbyte, IncludeRecord, ExcludeRecord, ControlRunID, CreationModificationDate, ClaimNumber, Title, CustomerForname, CustomerSurname, ClaimHistoryCounter, PolicyNumber, InsuredHandsetMake, InsuredHandsetModel, InsuredHandsetIMEI, HandsetPrice, UpdatedProductMake, UpdatedProductModel, UpdatedProductIMEI, CustomerAddressLine1, CustomerAddressLine2, CustomerAddressLine3, CustomerAddressLine4, S6PostCode, CustomerEmail, AlternateEmail, MobileNumber, IncidentType, CauseOfLoss, IncidentLocation, DeviceFault, IncidentDate, ClaimNotificationDate, PoliceNotifiedDate, CrimeRefNumber, PoliceStation, BypassFraud, ReplacementPhoneMake, ReplacementPhoneModel, ReplacementPhoneIMEI, ReplacementPhoneGrade, DeliveryAddressLine1, DeliveryAddressLine2, DeliveryAddressLine3, DeliveryAddressLine4, DeliveryPostCode, PickUpAddress1, PickUpAddress2, PickUpAddress3, PickUpAddress4, PickUpCity, PickUpPostCode, JiffyBagAddress1, JiffyBagAddress2, JiffyBagAddress3, JiffyBagAddress4, JiffyBagCity, JiffyBagPostCode, TotalClaimCost, TotalLessExcessCost, ClaimStatus);

--ThisNeedsToBeDifferent;
MERGE [Extract].[Policy_ACE] AS TARGET
USING	(
		SELECT Policy, StartDate, GrossPremium, FirstName, Surname, DateOfBirth, CompanyName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, PostCode, CancelDate, ProductInsured, MobileNumber, IMEI, CustomerType, PolicyHashbyte
		FROM	(
				SELECT ROW_NUMBER() OVER (PARTITION BY Policy ORDER BY RowID DESC) RowNumber, Policy, StartDate, GrossPremium, FirstName, Surname, DateOfBirth, CompanyName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, PostCode, CancelDate, ProductInsured, MobileNumber, IMEI, CustomerType, PolicyHashbyte
				FROM [Import].[Policy_ACE]
				) DATA
		WHERE RowNumber = 1
		) AS Source ON Source.Policy = [Target].Policy
WHEN MATCHED AND Source.PolicyHashbyte != TARGET.PolicyHashbyte THEN UPDATE
SET  PolicyHashbyte				= SOURCE.PolicyHashbyte
	,ControlRunID				= NULL
	,CreationModificationDate	= @Now
	,Policy						= SOURCE.Policy
	,StartDate					= SOURCE.StartDate
	,GrossPremium				= SOURCE.GrossPremium
	,FirstName					= SOURCE.FirstName
	,Surname					= SOURCE.Surname
	,DateOfBirth				= SOURCE.DateOfBirth
	,CompanyName				= SOURCE.CompanyName
	,AddressLine1				= SOURCE.AddressLine1
	,AddressLine2				= SOURCE.AddressLine2
	,AddressLine3				= SOURCE.AddressLine3
	,AddressLine4				= SOURCE.AddressLine4
	,PostCode					= SOURCE.PostCode
	,CancelDate					= SOURCE.CancelDate
	,ProductInsured				= SOURCE.ProductInsured
	,MobileNumber				= SOURCE.MobileNumber
	,IMEI						= SOURCE.IMEI
	,CustomerType				= SOURCE.CustomerType
WHEN NOT MATCHED THEN 
		INSERT (PolicyHashbyte, ControlRunID, CreationModificationDate, Policy, StartDate, GrossPremium, FirstName, Surname, DateOfBirth, CompanyName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, PostCode, CancelDate, ProductInsured, MobileNumber, IMEI, CustomerType)
		VALUES (SOURCE.PolicyHashbyte, NULL, @Now, SOURCE.Policy, SOURCE.StartDate, SOURCE.GrossPremium, SOURCE.FirstName, SOURCE.Surname, SOURCE.DateOfBirth, SOURCE.CompanyName, SOURCE.AddressLine1, SOURCE.AddressLine2, SOURCE.AddressLine3, SOURCE.AddressLine4, SOURCE.PostCode, SOURCE.CancelDate, SOURCE.ProductInsured, SOURCE.MobileNumber, SOURCE.IMEI, SOURCE.CustomerType)
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.PolicyHashbyte,Deleted.PolicyHashbyte), ISNULL(Inserted.ControlRunID,Deleted.ControlRunID), ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.Policy,Deleted.Policy), ISNULL(Inserted.StartDate,Deleted.StartDate), ISNULL(Inserted.GrossPremium,Deleted.GrossPremium), ISNULL(Inserted.FirstName,Deleted.FirstName), ISNULL(Inserted.Surname,Deleted.Surname), ISNULL(Inserted.DateOfBirth,Deleted.DateOfBirth), ISNULL(Inserted.CompanyName,Deleted.CompanyName), ISNULL(Inserted.AddressLine1,Deleted.AddressLine1), ISNULL(Inserted.AddressLine2,Deleted.AddressLine2), ISNULL(Inserted.AddressLine3,Deleted.AddressLine3), ISNULL(Inserted.AddressLine4,Deleted.AddressLine4), ISNULL(Inserted.PostCode,Deleted.PostCode), ISNULL(Inserted.CancelDate,Deleted.CancelDate), ISNULL(Inserted.ProductInsured,Deleted.ProductInsured), ISNULL(Inserted.MobileNumber,Deleted.MobileNumber), ISNULL(Inserted.IMEI,Deleted.IMEI), ISNULL(Inserted.CustomerType,Deleted.CustomerType)
INTO [Audit].[Policy_ACE] (DMLAction, ExecutionDateTime, PolicyRowID, PolicyHashbyte, ControlRunID, CreationModificationDate, Policy, StartDate, GrossPremium, FirstName, Surname, DateOfBirth, CompanyName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, PostCode, CancelDate, ProductInsured, MobileNumber, IMEI, CustomerType);