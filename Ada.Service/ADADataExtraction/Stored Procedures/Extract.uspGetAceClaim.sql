﻿CREATE PROCEDURE [Extract].[uspGetAceClaim] @Year INT, @Month TINYINT
AS

SELECT ClaimNumber, Title, CustomerForname, CustomerSurname, ClaimHistoryCounter, PolicyNumber, InsuredHandsetMake, InsuredHandsetModel, InsuredHandsetIMEI, HandsetPrice, UpdatedProductMake, UpdatedProductModel, UpdatedProductIMEI, CustomerAddressLine1, CustomerAddressLine2, CustomerAddressLine3, CustomerAddressLine4, S6PostCode, CustomerEmail, AlternateEmail, MobileNumber, IncidentType, CauseOfLoss, IncidentLocation, DeviceFault, IncidentDate, ClaimNotificationDate, PoliceNotifiedDate, CrimeRefNumber, PoliceStation, BypassFraud, ReplacementPhoneMake, ReplacementPhoneModel, ReplacementPhoneIMEI, ReplacementPhoneGrade, DeliveryAddressLine1, DeliveryAddressLine2, DeliveryAddressLine3, DeliveryAddressLine4, DeliveryPostCode, PickUpAddress1, PickUpAddress2, PickUpAddress3, PickUpAddress4, PickUpCity, PickUpPostCode, JiffyBagAddress1, JiffyBagAddress2, JiffyBagAddress3, JiffyBagAddress4, JiffyBagCity, JiffyBagPostCode, TotalClaimCost, TotalLessExcessCost, ClaimStatus
FROM [Extract].[Claim_ACE] CL
WHERE EXISTS (SELECT TOP 1 1 FROM [Extract].[Policy_ACE] PL WHERE PL.Policy = CL.PolicyNumber)
AND [IncludeRecord] = 1
AND [ExcludeRecord] = 0
AND (DATEPART(YEAR,ClaimNotificationDate) = @Year OR @Year = 0)
AND (DATEPART(MONTH,ClaimNotificationDate) = @Month OR @Month = 0)
ORDER BY ClaimNotificationDate ASC