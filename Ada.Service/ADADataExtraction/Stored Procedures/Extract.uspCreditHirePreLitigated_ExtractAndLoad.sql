﻿CREATE PROCEDURE [Extract].[uspCreditHirePreLitigated_ExtractAndLoad]
/**************************************************************************************************/
-- ObjectName:		[Extract].[uspCreditHirePreLitigated_ExtractAndLoad]
--
-- Description:		Extract the Credit Hire Pre Litigated Matters from VF and load locally.
--				
--
-- Steps:	
--		
-- RevisionHistory:
--		Date				Author			Modification
--		2014-10-13			Paul Allen		Created
/**************************************************************************************************/
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
SET DATEFORMAT DMY

-----------------------------------------
--1.)SetUpAnyTempObjectsAndVariables
-----------------------------------------
DECLARE  @Now				SMALLDATETIME	= GETDATE()
		,@RecordSourceID	INT				= 4 --VFStage
		,@ProductTypeID		INT				= 3 --CreditHire

IF OBJECT_ID('tempdb..#Claim') IS NOT NULL
	DROP TABLE #Claim

IF OBJECT_ID('tempdb..#Vehicle') IS NOT NULL
	DROP TABLE #Vehicle

IF OBJECT_ID('tempdb..#Person') IS NOT NULL
	DROP TABLE #Person

IF OBJECT_ID('tempdb..#Organisation') IS NOT NULL
	DROP TABLE #Organisation

IF OBJECT_ID('tempdb..#OrganisationUpdate') IS NOT NULL
	DROP TABLE #OrganisationUpdate

CREATE TABLE #Claim (ID INT IDENTITY(1,1),MatterNumber INT NOT NULL, Hashbyte VARBINARY(MAX), Insurer VARCHAR(50), IncidentDate SMALLDATETIME, ClaimStatus VARCHAR(50), InsurerClientReference VARCHAR(50), IncidentCircumstances VARCHAR(1024))
CREATE TABLE #Vehicle (ID INT IDENTITY(1,1),MatterNumber INT NOT NULL, Hashbyte VARBINARY(MAX), VehicleID SMALLINT, OrganisationID SMALLINT, VehicleInvolvement VARCHAR(50), VehicleRegistration VARCHAR(20), VehicleMake VARCHAR(50), VehicleModel VARCHAR(50), DateHireStarted SMALLDATETIME, DateHireEnded SMALLDATETIME, GroupNo SMALLINT)
CREATE TABLE #Person (ID INT IDENTITY(1,1),MatterNumber INT NOT NULL, Hashbyte VARBINARY(MAX), VehicleID SMALLINT, PersonID SMALLINT, PersonInvolvement VARCHAR(50), Salutation VARCHAR(20), FirstName VARCHAR(100), LastName VARCHAR(100), DateOfBirth SMALLDATETIME, Gender VARCHAR(50), NINumber VARCHAR(30), LandlineTelephone VARCHAR(50), MobileTelephone VARCHAR(50), Address VARCHAR(225), PostCode VARCHAR(20))
CREATE TABLE #Organisation (ID INT IDENTITY(1,1),MatterNumber INT NOT NULL, Hashbyte VARBINARY(MAX), OrganisationID SMALLINT, PersonID SMALLINT, OrganisationInvolvement VARCHAR(50), CompanyName VARCHAR(255), Telephone1 VARCHAR(50), Address VARCHAR(225), PostCode VARCHAR(20), GroupNo SMALLINT)
CREATE TABLE #OrganisationUpdate (ID INT IDENTITY(1,1), MatterNumber INT NOT NULL, OrganisationInvolvement VARCHAR(50), Telephone1 VARCHAR(50), Address VARCHAR(225), PostCode VARCHAR(20), GroupNo SMALLINT)

-----------------------------------------
--2.)ExtractTheClaimData
-----------------------------------------
INSERT INTO #Claim (MatterNumber, Insurer, IncidentDate, ClaimStatus, InsurerClientReference, IncidentCircumstances)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(Insurer,50) AS VARCHAR(50)))),'"','') Insurer
		,CASE WHEN ISDATE(LTRIM(RTRIM(IncidentDate))) = 1 THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(IncidentDate)),103) AS SMALLDATETIME) ELSE NULL END IncidentDate
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(ClaimStatus,50) AS VARCHAR(50)))),'"','') ClaimStatus
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(InsurerClientReference,50) AS VARCHAR(50)))),'"','') InsurerClientReference
		,REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(CAST(LEFT(IncidentCircumstances,1024) AS VARCHAR(1024)))),'"',''), CHAR(13),''),CHAR(10),''),CHAR(13) + CHAR(10), '') IncidentCircumstances
FROM OPENQUERY  (
				VF_Stage,
				'SELECT	 MD.Matter_Code MatterNumber
						,AD.ADD02 Insurer
						,MAT.MAT03 IncidentDate
						,DIS.DIS02 ClaimStatus
						,INR.INR12 InsurerClientReference
						,MAT.MAT21 IncidentCircumstances
				FROM dbo.Matdb MD
				LEFT JOIN dbo.Matdb_Add_In mai ON MD.Mt_Int_Code =mai.Mt_Int_Code and mai.Add_In_Code =''AICHBASE''
				INNER JOIN dbo.Link L ON MD.Mt_Int_Code = L.From_Owner_Code 
				INNER JOIN dbo.Relation r ON l.Relation_Int_Code = r.Relation_Int_Code
				LEFT JOIN UDDETAIL_INR inr ON mai.Mt_Add_In_Int_Code =	inr.Owner_Code and mai.Add_In_Code = inr.Parent_Code
				LEFT JOIN UDDETAIL_MAT mat ON mai.Mt_Add_In_Int_Code =	mat.Owner_Code and mai.Add_In_Code = mat.Parent_Code
				LEFT JOIN dbo.UDDetail_ADD ad ON md.Entity_Int_Code = ad.Owner_Code AND ad.PO_Type_Char = ''E''
				LEFT JOIN UDDETAIL_DIS dis ON mai.Mt_Add_In_Int_Code =	dis.Owner_Code and mai.Add_In_Code = dis.Parent_Code
				WHERE MD.Mt_Type = ''LGEN''
				AND r.Relation_Code IN (''FILE_ALLCLIENTS'')'
				)
ORDER BY MatterNumber

-----------------------------------------
--3.)ExtractTheVehicleData
-----------------------------------------
--InsuredVehicle
INSERT INTO #Vehicle (MatterNumber, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, GroupNo)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST('InsuredVehicle' AS VARCHAR(50)) VehicleInvolvement
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleRegistration,20) AS VARCHAR(20)))),'"','') VehicleRegistration
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleMake,50) AS VARCHAR(50)))),'"','') VehicleMake
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleModel,50) AS VARCHAR(50)))),'"','') VehicleModel
		,CAST(1 AS SMALLINT) GroupNo
FROM OPENQUERY  (
				VF_Stage,
				'SELECT	 MD.Matter_Code MatterNumber
						,INS.INS22 VehicleRegistration
						,INS.INS23 VehicleMake
						,INS.INS24 VehicleModel
				FROM dbo.Matdb MD
				LEFT JOIN dbo.Matdb_Add_In mai ON MD.Mt_Int_Code =mai.Mt_Int_Code and mai.Add_In_Code =''AICHBASE''
				INNER JOIN dbo.Link L ON MD.Mt_Int_Code = L.From_Owner_Code AND ISNULL(l.To_Owner_Code,'''') <>''''
				INNER JOIN dbo.Relation r ON l.Relation_Int_Code = r.Relation_Int_Code
				LEFT JOIN dbo.UDDETAIL_INS ins	ON	mai.Mt_Add_In_Int_Code= ins.Owner_Code AND mai.Add_In_Code= ins.Parent_Code 
				WHERE MD.Mt_Type = ''LGEN''
				AND r.Relation_Code IN (''CHCINSURED_INSURED'')'
				)	
ORDER BY MatterNumber

--ThirdPartyVehicle
INSERT INTO #Vehicle (MatterNumber, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, GroupNo)
SELECT	 REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST('ThirdPartyVehicle' AS VARCHAR(50)) VehicleInvolvement
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleRegistration,20) AS VARCHAR(20)))),'"','') VehicleRegistration
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleMake,50) AS VARCHAR(50)))),'"','') VehicleMake
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleModel,50) AS VARCHAR(50)))),'"','') VehicleModel
		,CAST(1 AS SMALLINT) GroupNo
FROM OPENQUERY  (
				VF_Stage,
				'SELECT	 MD.Matter_Code MatterNumber
						,CLA.CLA22 VehicleRegistration
						,CLA.CLA23 VehicleMake
						,CLA.CLA24 VehicleModel
				FROM dbo.Matdb md 
				LEFT JOIN dbo.Matdb_Add_In mai	ON	md.Mt_Int_Code = mai.Mt_Int_Code AND Add_In_Code IN (''AICHBASE'')
				INNER JOIN dbo.Link l ON md.Mt_Int_Code = l.From_Owner_Code AND l.From_Subcode_Group_Name IN (''CLAI'','''')
				INNER JOIN dbo.Entity e	ON l.To_Owner_Code = e.Entity_Int_Code AND e.Sort_Field <> ''''	
				INNER JOIN dbo.Relation	r ON l.Relation_Int_Code = r.Relation_Int_Code
				INNER JOIN dbo.UDDETAIL_CLA cla ON mai.Mt_Add_In_Int_Code =cla.Owner_Code AND mai.Add_In_Code = cla.Parent_Code
				WHERE MD.Mt_Type = ''LGEN''
				AND r.Relation_Code IN (''CHCLAIMANT_CLAIMANT'')'
				)
ORDER BY MatterNumber

--HireVehicle
INSERT INTO #Vehicle (MatterNumber, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, DateHireStarted, DateHireEnded, GroupNo)
SELECT	 REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST('HireVehicle' AS VARCHAR(50)) VehicleInvolvement
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleRegistration,20) AS VARCHAR(20)))),'"','') VehicleRegistration
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleMake,50) AS VARCHAR(50)))),'"','') VehicleMake
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(VehicleModel,50) AS VARCHAR(50)))),'"','') VehicleModel
		,CASE WHEN ISDATE(LTRIM(RTRIM(DateHireStarted))) =1 THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireStarted)),103) AS SMALLDATETIME) ELSE NULL END DateHireStarted
		,CASE WHEN ISDATE(LTRIM(RTRIM(DateHireEnded))) =1 THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateHireEnded)),103) AS SMALLDATETIME) ELSE NULL END DateHireEnded
		,REPLACE(LTRIM(RTRIM(CAST(GroupNo AS SMALLINT))),'"','')  GroupNo
FROM OPENQUERY	(
				VF_Stage,
				'SELECT	 MD.Matter_Code MatterNumber
						,VEH.VEH01 VehicleRegistration
						,VEH.VEH02 VehicleMake
						,VEH.VEH03 VehicleModel
						,HVI.HVI09 DateHireStarted
						,HVI.HVI10 DateHireEnded
						,hvi.Group_No GroupNo
				FROM dbo.Matdb md 
				LEFT JOIN dbo.Matdb_Add_In mai ON md.Mt_Int_Code = mai.Mt_Int_Code AND Add_In_Code IN (''AICHBASE'')
				INNER JOIN dbo.Link l ON md.Mt_Int_Code = l.From_Owner_Code AND l.From_Subcode_Group_Name IN (''GHVI'' ) 
				INNER JOIN dbo.Entity e	ON l.To_Owner_Code = e.Entity_Int_Code 
				INNER JOIN dbo.Relation	r ON l.Relation_Int_Code = r.Relation_Int_Code
				LEFT JOIN dbo.UDDetail_HVI hvi ON mai.Mt_Add_In_Int_Code = hvi.Owner_Code AND L.From_Subcode_Group_No = hvi.Group_No AND mai.Add_In_Code = hvi.Parent_Code
				LEFT JOIN dbo.UDDETAIL_VEH veh ON l.To_Owner_Code = veh.Owner_Code AND veh.PO_Type_Char =''E''
				WHERE MD.Mt_Type = ''LGEN''
				AND r.Relation_Code IN (''CHHVEHICLE_VEHICLE'')'
				)
ORDER BY MatterNumber, GroupNo

-----------------------------------------
--4.)ExtractThePersonData
-----------------------------------------
INSERT INTO #Person (MatterNumber, PersonInvolvement, Salutation, FirstName, LastName, DateOfBirth, Gender, NINumber, LandlineTelephone, MobileTelephone, Address, PostCode)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST(CASE REPLACE(LTRIM(RTRIM(PersonInvolvement)),'"','') WHEN 'CHINCON_CONTACT' THEN 'Insured' WHEN 'CHDRCON_CONTACT' THEN 'InsuredDriver' WHEN 'CHCLCON_CONTACT' THEN 'Claimant' END AS VARCHAR(50)) PersonInvolvement
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(Salutation,20) AS VARCHAR(20)))),'"','') Salutation
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(FirstName,100) AS VARCHAR(100)))),'"','') FirstName
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(LastName,100) AS VARCHAR(100)))),'"','') LastName
		,CASE WHEN ISDATE(LTRIM(RTRIM(DateOfBirth))) =1 THEN CAST(CONVERT(VARCHAR(50),LTRIM(RTRIM(DateOfBirth)),103) AS SMALLDATETIME) ELSE NULL END DateOfBirth
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(Gender,50) AS VARCHAR(50)))),'"','') Gender
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(NINumber,30) AS VARCHAR(50)))),'"','') NINumber
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(Landline,50) AS VARCHAR(50)))),'"','') Landline
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(Mobile,50) AS VARCHAR(50)))),'"','') Mobile
		,CAST(LEFT(UPPER(ISNULL(REPLACE(LTRIM(RTRIM(Address1)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address2)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address3)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address4)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address5)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address6)),'"','')  + ' ','')) ,225) AS VARCHAR(225)) Address
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(REPLACE(PostCode,' ',''),20) AS VARCHAR(20)))),'"','') PostCode
FROM OPENQUERY	(
				VF_Stage,
				'SELECT  md.Matter_Code MatterNumber
						,R.Relation_Code PersonInvolvement
						,Nam.NAM03 Salutation
						,Nam.NAM02 FirstName
						,Nam.NAM01 LastName
						,PSI.PSI01 DateOfBirth
						,Nam.NAM15 Gender
						,PSI.PSI12 NINumber
						,CON.CON01 Landline
						,CON.CON03 Mobile
						,NAM.NAM04 Address1
						,NAM.NAM05 Address2
						,NAM.NAM06 Address3
						,NAM.NAM07 Address4
						,NAM.NAM08 Address5
						,NAM.NAM09 Address6
						,NAM.NAM10 PostCode
				FROM dbo.Matdb md 
				INNER JOIN dbo.Matdb_Add_In mai ON md.Mt_Int_Code = mai.Mt_Int_Code AND Add_In_Code IN (''AICHBASE'')
				INNER JOIN dbo.Link l ON md.Mt_Int_Code = l.From_Owner_Code 
				INNER JOIN dbo.Relation r ON l.Relation_Int_Code = r.Relation_Int_Code
				LEFT JOIN dbo.UDDetail_Nam nam ON  l.To_Owner_Code = nam.Owner_Code AND Nam.PO_Type_Char = ''E''
				LEFT JOIN dbo.UDDETAIL_PSI PSI ON  l.To_Owner_Code = PSI.Owner_Code AND PSI.PO_Type_Char = ''E''
				LEFT JOIN dbo.UDDETAIL_CON CON ON  l.To_Owner_Code = CON.Owner_Code AND CON.PO_Type_Char = ''E'' 
				WHERE MD.Mt_Type = ''LGEN''
				AND r.Relation_Code IN (''CHINCON_CONTACT'',''CHDRCON_CONTACT'',''CHCLCON_CONTACT'')'
				)
ORDER BY MatterNumber

-----------------------------------------
--5.)ExtractTheOrganisationData
-----------------------------------------
--GetTheBaseDetails
INSERT INTO #Organisation (MatterNumber, OrganisationInvolvement, CompanyName, GroupNo)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST(CASE REPLACE(LTRIM(RTRIM(OrganisationInvolvement)),'"','') WHEN 'CHCHO_CHO' THEN 'CHO' WHEN 'CHCLSOLICITOR_SOLICITOR' THEN 'ClaimantSolicitor' WHEN 'AICHBASE_CLAIMINSURER' THEN 'ClaimantInsurer' WHEN 'AICHBASE_CLAIMBROKER' THEN 'ClaimantBroker' WHEN 'AICHBASE_CLAIMALTREP' THEN 'ClaimantAlternariveRepresentative' WHEN 'CHREPAIRER_REPAIRER' THEN 'Repairer' WHEN 'AICHBASE_GENPARTY' THEN 'GeneralParty' WHEN 'AICHBASE_ENGINEER' THEN 'Engineer' WHEN 'AICHBASE_RECSTOR' THEN 'RecoverySalvageAgent' END AS VARCHAR(50)) OrganisationInvolvement
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(CompanyName,225) AS VARCHAR(225)))),'"','') CompanyName
		,REPLACE(LTRIM(RTRIM(CAST(GroupNo AS SMALLINT))),'"','')  GroupNo
FROM OPENQUERY	(
				VF_Stage,
				'SELECT  md.Matter_Code MatterNumber
						,R.Relation_Code OrganisationInvolvement
						,AD.ADD02 CompanyName
						,L.From_Subcode_Group_No GroupNo
				FROM dbo.Matdb md 
				INNER JOIN dbo.Matdb_Add_In mai ON md.Mt_Int_Code = mai.Mt_Int_Code AND Add_In_Code IN (''AICHBASE'')
				INNER JOIN dbo.Link l ON md.Mt_Int_Code = l.From_Owner_Code 
				INNER JOIN dbo.Relation r ON l.Relation_Int_Code = r.Relation_Int_Code
				LEFT JOIN dbo.UDDetail_ADD AD ON  l.To_Owner_Code = AD.Owner_Code AND AD.PO_Type_Char = ''E''
				LEFT JOIN dbo.UDDetail_COM COM ON  l.To_Owner_Code = COM.Owner_Code AND COM.PO_Type_Char = ''E''
				WHERE MD.Mt_Type = ''LGEN''
				AND r.Relation_Code IN (''CHCHO_CHO'',''CHCLSOLICITOR_SOLICITOR'',''AICHBASE_CLAIMINSURER'',''AICHBASE_CLAIMBROKER'',''AICHBASE_CLAIMALTREP'',''CHREPAIRER_REPAIRER'',''AICHBASE_GENPARTY'',''AICHBASE_ENGINEER'',''AICHBASE_RECSTOR'')'
				)
ORDER BY MatterNumber, OrganisationInvolvement, GroupNo

--GetTheRestOfTheDataWeNeed
INSERT INTO #OrganisationUpdate (MatterNumber, OrganisationInvolvement, Telephone1, Address, PostCode, GroupNo)
SELECT   REPLACE(LTRIM(RTRIM(CAST(MatterNumber AS INT))),'"','') MatterNumber
		,CAST(CASE REPLACE(LTRIM(RTRIM(OrganisationInvolvement)),'"','') WHEN 'CHOOFF_OFFICE' THEN 'CHO' WHEN 'CHCLSOLOFFI_OFFICE' THEN 'ClaimantSolicitor' WHEN 'AICHBASE_CLAIMINSUREROFFI' THEN 'ClaimantInsurer' WHEN 'AICHBASE_CLAIMBROKEROFF' THEN 'ClaimantBroker' WHEN 'AICHBASE_CLAIMALTREPOFFI' THEN 'ClaimantAlternativeRepresentative' WHEN 'CHRPOFFI_OFFICE' THEN 'Repairer' WHEN 'AICHBASE_GENPAROFFI' THEN 'GeneralParty' WHEN 'AICHBASE_ENGINEEROFFI' THEN 'Engineer' WHEN 'AICHBASE_RECSTOROFFI' THEN 'RecoverySalvageAgent' END AS VARCHAR(50)) OrganisationInvolvement
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(Telephone1,50) AS VARCHAR(50)))),'"','') Telephone1
		,CAST(LEFT(UPPER(ISNULL(REPLACE(LTRIM(RTRIM(Address1)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address2)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address3)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address4)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address5)),'"','')  + ' ','') + ISNULL(REPLACE(LTRIM(RTRIM(Address6)),'"','')  + ' ','')) ,225) AS VARCHAR(225)) Address
		,REPLACE(LTRIM(RTRIM(CAST(LEFT(REPLACE(PostCode,' ',''),20) AS VARCHAR(20)))),'"','') PostCode
		,REPLACE(LTRIM(RTRIM(CAST(GroupNo AS SMALLINT))),'"','')  GroupNo
FROM OPENQUERY	(
				VF_Stage,
				'SELECT  md.Matter_Code MatterNumber
						,R.Relation_Code OrganisationInvolvement
						,CON.CON02 Telephone1
						,OFFI.OFF04 Address1
						,OFFI.OFF05 Address2
						,OFFI.OFF06 Address3
						,OFFI.OFF07 Address4
						,OFFI.OFF08 Address5
						,OFFI.OFF09 Address6
						,OFFI.OFF10 PostCode
						,L.From_Subcode_Group_No GroupNo
				FROM dbo.Matdb md 
				INNER JOIN dbo.Matdb_Add_In mai ON md.Mt_Int_Code = mai.Mt_Int_Code AND Add_In_Code IN (''AICHBASE'')
				INNER JOIN dbo.Link l ON md.Mt_Int_Code = l.From_Owner_Code 
				INNER JOIN dbo.Relation r ON l.Relation_Int_Code = r.Relation_Int_Code
				LEFT JOIN dbo.UDDETAIL_CON CON ON  l.To_Owner_Code = CON.Owner_Code AND CON.PO_Type_Char = ''E''
				LEFT JOIN dbo.UDDETAIL_OFF OFFI ON  l.To_Owner_Code = OFFI.Owner_Code AND OFFI.PO_Type_Char = ''E''
				WHERE MD.Mt_Type = ''LGEN''
				AND r.Relation_Code IN (''CHOOFF_OFFICE'',''CHCLSOLOFFI_OFFICE'',''AICHBASE_CLAIMINSUREROFFI'',''AICHBASE_CLAIMBROKEROFF'',''AICHBASE_CLAIMALTREPOFFI'',''CHRPOFFI_OFFICE'',''AICHBASE_GENPAROFFI'',''AICHBASE_ENGINEEROFFI'',''AICHBASE_RECSTOROFFI'')'
				)
ORDER BY MatterNumber, GroupNo

UPDATE ORG
SET  Telephone1	= OU.Telephone1
	,Address	= OU.Address
	,PostCode	= OU.PostCode
FROM #Organisation ORG
INNER JOIN #OrganisationUpdate OU ON OU.MatterNumber = ORG.MatterNumber AND OU.OrganisationInvolvement = ORG.OrganisationInvolvement AND OU.GroupNo = ORG.GroupNo 

------------------------------------
--NowUpdateTheRelationships
------------------------------------
--SetTheIDValues
UPDATE VEH
SET VehicleID = Data.VehicleID
FROM #Vehicle VEH
INNER JOIN	(
			SELECT ID, ROW_NUMBER() OVER (PARTITION BY MatterNumber ORDER BY VehicleInvolvement, GroupNo) VehicleID
			FROM #Vehicle
			) Data ON Data.ID = VEH.ID

UPDATE PER
SET PersonID = Data.PersonID
FROM #Person PER
INNER JOIN	(
			SELECT ID, ROW_NUMBER() OVER (PARTITION BY MatterNumber ORDER BY PersonInvolvement) PersonID
			FROM #Person
			) Data ON Data.ID = PER.ID

UPDATE ORG
SET OrganisationID = Data.OrganisationID
FROM #Organisation ORG
INNER JOIN	(
			SELECT ID, ROW_NUMBER() OVER (PARTITION BY MatterNumber ORDER BY OrganisationInvolvement, GroupNo) OrganisationID
			FROM #Organisation
			) Data ON Data.ID = ORG.ID

--AddSomeKeys
ALTER TABLE #Vehicle ALTER COLUMN VehicleID SMALLINT NOT NULL
ALTER TABLE #Person ALTER COLUMN PersonID SMALLINT NOT NULL
ALTER TABLE #Organisation ALTER COLUMN OrganisationID SMALLINT NOT NULL

ALTER TABLE #Vehicle ADD CONSTRAINT PK_#Vehicle_MatterNumber_VehicleID PRIMARY KEY (MatterNumber, VehicleID)
ALTER TABLE #Person ADD CONSTRAINT PK_#Person_MatterNumber_PersonID PRIMARY KEY (MatterNumber, PersonID)
ALTER TABLE #Organisation ADD CONSTRAINT PK_#Organisation_MatterNumber_OrganisationID PRIMARY KEY (MatterNumber, OrganisationID)

--LinkTheDatasetsTogether
UPDATE PER
SET VehicleID = CASE 
					WHEN Per.PersonInvolvement IN ('InsuredDriver','Insured') AND VEH.VehicleInvolvement = 'InsuredVehicle' THEN VEH.VehicleID
					WHEN Per.PersonInvolvement IN ('Claimant') AND VEH.VehicleInvolvement = 'ThirdPartyVehicle' THEN VEH.VehicleID
				END
FROM #Person PER
INNER JOIN #Vehicle VEH ON VEH.MatterNumber = Per.MatterNumber
WHERE CASE WHEN Per.PersonInvolvement IN ('InsuredDriver','Insured') AND VEH.VehicleInvolvement = 'InsuredVehicle' THEN VEH.VehicleID WHEN Per.PersonInvolvement IN ('Claimant') AND VEH.VehicleInvolvement = 'ThirdPartyVehicle' THEN VEH.VehicleID END IS NOT NULL

UPDATE ORG
SET PersonID = CASE WHEN Per.PersonInvolvement IN ('Claimant') AND ORG.OrganisationInvolvement IN ('ClaimantInsurer','ClaimantSolicitor','ClaimantBroker','ClaimantAlternativeRepresentative','CHO') THEN PER.PersonID END
FROM #Organisation ORG
INNER JOIN #Person PER ON PER.MatterNumber = ORG.MatterNumber
WHERE CASE WHEN Per.PersonInvolvement IN ('Claimant') AND ORG.OrganisationInvolvement IN ('ClaimantInsurer','ClaimantSolicitor','ClaimantBroker','ClaimantAlternativeRepresentative','CHO') THEN PER.PersonID END IS NOT NULL

UPDATE VEH
SET OrganisationID = CASE WHEN VEH.VehicleInvolvement IN ('HireVehicle') AND ORG.OrganisationInvolvement IN ('CHO') AND VEH.GroupNo = ORG.GroupNo THEN ORG.OrganisationID END
FROM #Vehicle VEH
INNER JOIN #Organisation ORG ON ORG.MatterNumber = VEH.MatterNumber
WHERE CASE WHEN VEH.VehicleInvolvement IN ('HireVehicle') AND ORG.OrganisationInvolvement IN ('CHO') AND VEH.GroupNo = ORG.GroupNo THEN ORG.OrganisationID END IS NOT NULL

UPDATE VEH
SET OrganisationID = CASE WHEN VEH.OrganisationID IS NULL AND VEH.VehicleInvolvement IN ('HireVehicle') AND ORG.OrganisationInvolvement IN ('CHO') THEN ORG.OrganisationID  END
FROM #Vehicle VEH
INNER JOIN #Organisation ORG ON ORG.MatterNumber = VEH.MatterNumber
WHERE CASE WHEN VEH.OrganisationID IS NULL AND VEH.VehicleInvolvement IN ('HireVehicle') AND ORG.OrganisationInvolvement IN ('CHO') THEN ORG.OrganisationID END IS NOT NULL
AND VEH.OrganisationID IS NULL

------------------------------
--SetTheHashByte
------------------------------
UPDATE #Claim
SET Hashbyte = HASHBYTES('MD5',CAST(ISNULL(MatterNumber,'') AS VARCHAR(20))+CONVERT(VARCHAR(20),ISNULL(IncidentDate,''),103)+ISNULL(Insurer,'')+ISNULL(ClaimStatus,'')+ISNULL(InsurerClientReference,'')+ISNULL(IncidentCircumstances,''))

UPDATE #Vehicle
SET Hashbyte = HASHBYTES('MD5',CAST(ISNULL(MatterNumber,'') AS VARCHAR(20))+ISNULL(VehicleInvolvement,'')+ISNULL(VehicleRegistration,'')+ISNULL(VehicleMake,'')+ISNULL(VehicleModel,'')+CONVERT(VARCHAR(20),ISNULL(DateHireStarted,''),103)+CONVERT(VARCHAR(20),ISNULL(DateHireEnded,''),103)+CAST(ISNULL(GROUPNO,'') AS VARCHAR(3)))

UPDATE #Person
SET Hashbyte = HASHBYTES('MD5',CAST(ISNULL(MatterNumber,'') AS VARCHAR(20))+ISNULL(PersonInvolvement,'')+ISNULL(Salutation,'')+ISNULL(FirstName,'')+ISNULL(LastName,'')+CONVERT(VARCHAR(20),ISNULL(DateOfBirth,''),103)+ISNULL(Gender,'')+ISNULL(Salutation,'')+ISNULL(NINumber,'')+ISNULL(LandlineTelephone,'')+ISNULL(MobileTelephone,'')+ISNULL(Address,'')+ISNULL(PostCode,''))

UPDATE #Organisation
SET Hashbyte = HASHBYTES('MD5',CAST(ISNULL(MatterNumber,'') AS VARCHAR(20))+ISNULL(OrganisationInvolvement,'')+ISNULL(CompanyName,'')+ISNULL(Telephone1,'')+ISNULL(Address,'')+ISNULL(PostCode,'')+CAST(ISNULL(GroupNo,'') AS VARCHAR(3)))

-----------------------------
--AddAnyRecordModificationsToTheTables
--OutputTheseToAnAuditTable
-----------------------------
--CreditHirePreLitigated_Claim
MERGE [Extract].[Claim] AS [Target]
USING	(
		SELECT Hashbyte, MatterNumber, Insurer, IncidentDate, ClaimStatus, InsurerClientReference, IncidentCircumstances
		FROM #Claim TC
		) AS Source ON Source.Hashbyte = [Target].ClaimHashbyte
WHEN NOT MATCHED BY TARGET THEN 
	INSERT (ClaimHashbyte, CreationModificationDate, RecordSourceID, ProductTypeID, MatterNumber, Insurer, IncidentDate, ClaimStatus, InsurerClientReference, IncidentCircumstances)
	VALUES (Source.Hashbyte, @Now, @RecordSourceID, @ProductTypeID, Source.MatterNumber, Source.Insurer, Source.IncidentDate, Source.ClaimStatus, Source.InsurerClientReference, Source.IncidentCircumstances)
WHEN NOT MATCHED BY SOURCE AND TARGET.[ProductTypeID] = @ProductTypeID THEN
	DELETE
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.ClaimHashbyte,Deleted.ClaimHashbyte), ISNULL(Inserted.IncludeRecord,Deleted.IncludeRecord), ISNULL(Inserted.ExcludeRecord,Deleted.ExcludeRecord), ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.RecordSourceID,Deleted.RecordSourceID), ISNULL(Inserted.ProductTypeID,Deleted.ProductTypeID), ISNULL(Inserted.MatterNumber,Deleted.MatterNumber), ISNULL(Inserted.Insurer,Deleted.Insurer), ISNULL(Inserted.IncidentDate,Deleted.IncidentDate), ISNULL(Inserted.ClaimStatus,Deleted.ClaimStatus), ISNULL(Inserted.InsurerClientReference,Deleted.InsurerClientReference), ISNULL(Inserted.IncidentCircumstances,Deleted.IncidentCircumstances)
INTO [Audit].[Claim] (DMLAction, ExecutionDateTime, ClaimRowID, ClaimHashbyte, IncludeRecord, ExcludeRecord, CreationModificationDate, RecordSourceID, ProductTypeID, MatterNumber, Insurer, IncidentDate, ClaimStatus, InsurerClientReference, IncidentCircumstances);

--CreditHirePreLitigated_Vehicle
MERGE [Extract].[Vehicle] AS [Target]
USING	(
		SELECT Hashbyte, MatterNumber, VehicleID, OrganisationID, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, DateHireStarted, DateHireEnded 
		FROM #Vehicle TV
		) AS Source ON Source.Hashbyte = [Target].VehicleHashbyte
WHEN NOT MATCHED BY TARGET THEN 
	INSERT (VehicleHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, VehicleID, OrganisationID, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, DateHireStarted, DateHireEnded)
	VALUES (Source.Hashbyte, @Now, @ProductTypeID, Source.MatterNumber, Source.VehicleID, Source.OrganisationID, Source.VehicleInvolvement, Source.VehicleRegistration, Source.VehicleMake, Source.VehicleModel, Source.DateHireStarted, Source.DateHireEnded)
WHEN NOT MATCHED BY SOURCE AND TARGET.[ProductTypeID] = @ProductTypeID THEN
	DELETE
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.VehicleHashbyte,Deleted.VehicleHashbyte), ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.ProductTypeID,Deleted.ProductTypeID), ISNULL(Inserted.MatterNumber,Deleted.MatterNumber), ISNULL(Inserted.VehicleID,Deleted.VehicleID), ISNULL(Inserted.OrganisationID,Deleted.OrganisationID), ISNULL(Inserted.VehicleInvolvement,Deleted.VehicleInvolvement), ISNULL(Inserted.VehicleRegistration,Deleted.VehicleRegistration), ISNULL(Inserted.VehicleMake,Deleted.VehicleMake), ISNULL(Inserted.VehicleModel,Deleted.VehicleModel), ISNULL(Inserted.DateHireStarted,Deleted.DateHireStarted), ISNULL(Inserted.DateHireEnded,Deleted.DateHireEnded)
INTO [Audit].[Vehicle] (DMLAction, ExecutionDateTime, VehicleRowID, VehicleHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, VehicleID, OrganisationID, VehicleInvolvement, VehicleRegistration, VehicleMake, VehicleModel, DateHireStarted, DateHireEnded);

--CreditHirePreLitigated_Person
MERGE [Extract].[Person] AS [Target]
USING	(
		SELECT Hashbyte, MatterNumber, VehicleID, PersonID, PersonInvolvement, Salutation, FirstName, LastName, DateOfBirth, Gender, NINumber, LandlineTelephone, MobileTelephone, Address, PostCode
		FROM #Person TP
		) AS Source ON Source.Hashbyte = [Target].PersonHashbyte
WHEN NOT MATCHED BY TARGET THEN 
	INSERT (PersonHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, VehicleID, PersonID, PersonInvolvement, Salutation, FirstName, LastName, DateOfBirth, Gender, NINumber, LandlineTelephone, MobileTelephone, Address, PostCode)
	VALUES (Source.Hashbyte, @Now, @ProductTypeID, Source.MatterNumber, Source.VehicleID, Source.PersonID, Source.PersonInvolvement, Source.Salutation, Source.FirstName, Source.LastName, Source.DateOfBirth, Source.Gender, Source.NINumber, Source.LandlineTelephone, Source.MobileTelephone, Source.Address, Source.PostCode)
WHEN NOT MATCHED BY SOURCE AND TARGET.[ProductTypeID] = @ProductTypeID THEN
	DELETE
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.PersonHashbyte,Deleted.PersonHashbyte),  ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.ProductTypeID,Deleted.ProductTypeID), ISNULL(Inserted.MatterNumber,Deleted.MatterNumber), ISNULL(Inserted.VehicleID,Deleted.VehicleID), ISNULL(Inserted.PersonID,Deleted.PersonID), ISNULL(Inserted.PersonInvolvement,Deleted.PersonInvolvement), ISNULL(Inserted.Salutation,Deleted.Salutation), ISNULL(Inserted.FirstName,Deleted.FirstName), ISNULL(Inserted.LastName,Deleted.LastName), ISNULL(Inserted.DateOfBirth,Deleted.DateOfBirth), ISNULL(Inserted.Gender,Deleted.Gender), ISNULL(Inserted.NINumber,Deleted.NINumber), ISNULL(Inserted.LandlineTelephone,Deleted.LandlineTelephone), ISNULL(Inserted.MobileTelephone,Deleted.MobileTelephone), ISNULL(Inserted.Address,Deleted.Address), ISNULL(Inserted.PostCode,Deleted.PostCode)
INTO [Audit].[Person] (DMLAction, ExecutionDateTime, PersonRowID, PersonHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, VehicleID, PersonID, PersonInvolvement, Salutation, FirstName, LastName, DateOfBirth, Gender, NINumber, LandlineTelephone, MobileTelephone, Address, PostCode);

--CreditHirePreLitigated_Organisation
MERGE [Extract].[Organisation] AS [Target]
USING	(
		SELECT Hashbyte, MatterNumber, OrganisationID, PersonID, OrganisationInvolvement, CompanyName, Telephone1, Address, PostCode 
		FROM #Organisation TOR
		) AS Source ON Source.Hashbyte = [Target].OrganisationHashbyte
WHEN NOT MATCHED BY TARGET THEN 
	INSERT (OrganisationHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, OrganisationID, PersonID, OrganisationInvolvement, CompanyName, Telephone1, Address, PostCode)
	VALUES (Source.Hashbyte, @Now, @ProductTypeID, Source.MatterNumber, Source.OrganisationID, Source.PersonID, Source.OrganisationInvolvement, Source.CompanyName, Source.Telephone1, Source.Address, Source.PostCode)
WHEN NOT MATCHED BY SOURCE AND TARGET.[ProductTypeID] = @ProductTypeID THEN
	DELETE
OUTPUT $ACTION, @Now, ISNULL(Inserted.RowID,Deleted.RowID), ISNULL(Inserted.OrganisationHashbyte,Deleted.OrganisationHashbyte), ISNULL(Inserted.CreationModificationDate,Deleted.CreationModificationDate), ISNULL(Inserted.ProductTypeID,Deleted.ProductTypeID), ISNULL(Inserted.MatterNumber,Deleted.MatterNumber), ISNULL(Inserted.OrganisationID,Deleted.OrganisationID), ISNULL(Inserted.PersonID,Deleted.PersonID), ISNULL(Inserted.OrganisationInvolvement,Deleted.OrganisationInvolvement), ISNULL(Inserted.CompanyName,Deleted.CompanyName), ISNULL(Inserted.Telephone1,Deleted.Telephone1), ISNULL(Inserted.Address,Deleted.Address), ISNULL(Inserted.PostCode,Deleted.PostCode)
INTO [Audit].[Organisation] (DMLAction, ExecutionDateTime, OrganisationRowID, OrganisationHashbyte, CreationModificationDate, ProductTypeID, MatterNumber, OrganisationID, PersonID, OrganisationInvolvement, CompanyName, Telephone1, Address, PostCode);

------------------------------------
--Tidy
------------------------------------
IF OBJECT_ID('tempdb..#Claim') IS NOT NULL
	DROP TABLE #Claim

IF OBJECT_ID('tempdb..#Vehicle') IS NOT NULL
	DROP TABLE #Vehicle

IF OBJECT_ID('tempdb..#Person') IS NOT NULL
	DROP TABLE #Person

IF OBJECT_ID('tempdb..#Organisation') IS NOT NULL
	DROP TABLE #Organisation

IF OBJECT_ID('tempdb..#OrganisationUpdate') IS NOT NULL
	DROP TABLE #OrganisationUpdate