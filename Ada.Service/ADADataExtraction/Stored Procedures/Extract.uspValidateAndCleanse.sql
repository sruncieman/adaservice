﻿CREATE PROCEDURE [Extract].[uspValidateAndCleanse]
/**************************************************************************************************/
-- ObjectName:		[dbo].[uspCreditHirePreLitigated_ValidateAndCleanse]
--
-- Description:		
--				
--
-- Steps:	
--		
-- RevisionHistory:
--		Date				Author			Modification
--		2014-10-13			Paul Allen		Created
/**************************************************************************************************/
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

DECLARE @LastExecutionDate VARCHAR(20) = (SELECT CONVERT(VARCHAR(20),CAST([Value] AS SMALLDATETIME),120) FROM [Ref].[Configuration] WHERE [ConfigurationDescriptionID] = 1)
DECLARE @Matters TABLE (MatterNumber INT NOT NULL)

------------------------------------
--ApplyIncludeRecordFlagToMainClaimEntity
------------------------------------
DELETE @Matters
INSERT INTO @Matters (MatterNumber)
SELECT DISTINCT MatterNumber FROM [Audit].[Claim] WHERE [ExecutionDateTime] > @LastExecutionDate UNION
SELECT DISTINCT MatterNumber FROM [Audit].[Vehicle] WHERE [ExecutionDateTime] > @LastExecutionDate UNION
SELECT DISTINCT MatterNumber FROM [Audit].[Person] WHERE [ExecutionDateTime] > @LastExecutionDate UNION
SELECT DISTINCT MatterNumber FROM [Audit].[Organisation] WHERE [ExecutionDateTime] > @LastExecutionDate

UPDATE C
SET [IncludeRecord] = CASE WHEN M.MatterNumber IS NOT NULL THEN 1 ELSE 0 END
FROM [Extract].[Claim] C
LEFT JOIN @Matters M ON M.MatterNumber = C.MatterNumber
WHERE [IncludeRecord] != CASE WHEN M.MatterNumber IS NOT NULL THEN 1 ELSE 0 END

--------------------------------------
--ValidateAndCleanseRecords
--------------------------------------
UPDATE [Extract].[Claim]
SET [ExcludeRecord] = CASE WHEN NULLIF([Insurer],'NONBILLABLE CLIENT') IS NULL OR [InsurerClientReference] IN ('Not Co-op','Not insured by Co-op') THEN 1 ELSE 0 END
WHERE [ExcludeRecord] != CASE WHEN NULLIF([Insurer],'NONBILLABLE CLIENT') IS NULL OR [InsurerClientReference] IN ('Not Co-op','Not insured by Co-op') THEN 1 ELSE 0 END

UPDATE [Extract].[Claim]
SET [ExcludeRecord] = 1
WHERE NULLIF([IncidentDate],'01-JAN-1900') IS NULL 
AND [ExcludeRecord] != 1

--ForDFPIOnly
UPDATE [Extract].[Claim]
SET [ExcludeRecord] = CASE WHEN ([CreditHire] = 1 AND [ProceedingsIssued] =1 AND [ProductTypeID] = 2 AND [CFS] = 0) THEN 0 ELSE 1 END
WHERE [ProductTypeID] = 2
AND [ExcludeRecord] != CASE WHEN ([CreditHire] = 1 AND [ProceedingsIssued] =1  AND [ProductTypeID] = 2 AND [CFS] = 0) THEN 0 ELSE 1 END

