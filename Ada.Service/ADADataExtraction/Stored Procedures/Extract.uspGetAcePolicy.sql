﻿
CREATE PROCEDURE Extract.uspGetAcePolicy @Year INT, @Month TINYINT
AS

SELECT Policy, StartDate, GrossPremium, FirstName, Surname, DateOfBirth, CompanyName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, PostCode, CancelDate, ProductInsured, MobileNumber, IMEI, CustomerType
FROM [Extract].[Policy_ACE] PL 
WHERE EXISTS	(
				SELECT TOP 1 1 
				FROM [Extract].[Claim_ACE] CL
				WHERE CL.PolicyNumber = PL.Policy
				AND CL.[IncludeRecord] = 1
				AND CL.[ExcludeRecord] = 0
				AND (DATEPART(YEAR,ClaimNotificationDate) = @Year OR @Year = 0)
				AND (DATEPART(MONTH,ClaimNotificationDate) = @Month OR @Month = 0)
				)