﻿CREATE PROCEDURE [Extract].[uspRunJobCheck]
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

--Need7ZipOnProduction
DECLARE  @Today				DATETIME		= CAST(CONVERT(VARCHAR(20),GETDATE(),111) AS DATETIME)
		,@JobName			VARCHAR(100)	= 'ADA_ExtractVFMatters'
		,@EmailRecipients	VARCHAR(1000)	= (SELECT EmailDistribution FROM [MDAControl].[dbo].[DBControlEmailDistribution] WHERE EmailDistributionID = 1)
		,@Subject			VARCHAR(1000)
		,@Body				VARCHAR(1000)
		,@RunNameID			INT				= 131

--HaveWePerformedALoadToday:IfSoExit
IF EXISTS (SELECT TOP 1 1 FROM [MDAControl].[dbo].[DBControlRun] WHERE CAST(CONVERT(VARCHAR(20),[RunStartTime],111) AS DATETIME) = @Today AND [RunNameID] = @RunNameID )
BEGIN
	RAISERROR('TheJobADA_ExtractVFMattersHasBeenRunforToday..',16,1)
	RETURN 
END
 
--IfLoadFinishIsPopulatedAndErrorCountis0weCanProceed:CreateARunRecord 
IF EXISTS (SELECT TOP 1 1 FROM [Extract].[DWHLoadTable] WHERE LoadDay = @Today AND LoadFinish IS NOT NULL AND ErrorCount = 0)
BEGIN
	--CreateControlRunRecord
	INSERT INTO [MDAControl].[dbo].[DBControlRun] ([RunNameID]) VALUES (@RunNameID)

	--SendEmailThatProcessHasStarted
	SELECT   @Subject	= (SELECT '*** '+@@SERVERNAME+':['+@JobName+'] SQL Server Agent Job - STARTED ***' )
			,@Body		= (SELECT 'The SQL Server Job ['+@JobName+'] on server '+ @@SERVERNAME +' has started.')

	EXECUTE msdb.dbo.sp_send_dbmail
				 @profile_name = 'SVC ADA SQL',
				 @recipients=@EmailRecipients,
				 @subject=@Subject,
				 @Body = @Body
	RETURN
END

--IfWeHaveAErrorCountWeCantProceed:SendEmail,DisableJob,ManualInterventionRequired:CreateARunRecord
IF EXISTS (SELECT TOP 1 1 FROM [Extract].[DWHLoadTable] WHERE LoadDay = @Today AND LoadFinish IS NOT NULL AND ErrorCount != 0)
BEGIN
	--CreateControlRunRecord
	INSERT INTO [MDAControl].[dbo].[DBControlRun] ([RunNameID], [RunEndTime], [Error], [ErrorDescription]) 
	VALUES (@RunNameID, GETDATE(), 1, 'FatalErrorWithDWHJob-JobDisabled-ManualInterventionRequired' )

	--SendEmailThatProcessHasCanNotBeAttempted
	SELECT   @Subject	= (SELECT '*** '+@@SERVERNAME+':['+@JobName+'] SQL Server Agent Job - DWH Fatal Error ***' )
			,@Body		= (SELECT 'The SQL Server Job ['+@JobName+'] on server '+ @@SERVERNAME +' has failed due to a DWH Refresh Error. This job has been disabled and manual intervention is required.')

	EXECUTE msdb.dbo.sp_send_dbmail
				 @profile_name = 'SVC ADA SQL',
				 @recipients=@EmailRecipients,
				 @subject=@Subject,
				 @importance='HIGH',
				 @Body = @Body

	--DisableJob
	EXECUTE msdb.dbo.sp_update_job
				@job_name = @JobName,
				@enabled = 0

	RAISERROR('FatalErrorWithDWHJob-JobDisabled-ManualInterventionRequired...',16,1)
	RETURN
END

--IfLoadfinishIsNotPopulatedAndErrorCountNot>=0:ExitAndCheckOnNextSchedule
RAISERROR('DWHHasNotFinishedProcessingForToday-TryingLater...',16,1)
RETURN