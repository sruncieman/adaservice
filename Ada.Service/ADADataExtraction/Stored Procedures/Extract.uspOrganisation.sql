﻿
CREATE PROCEDURE [Extract].[uspOrganisation]
/**************************************************************************************************/
-- ObjectName:		[dbo].[uspCreditHirePreLitigated_Organisation_Output]
--
-- Description:						
--
-- Steps:	
--		
-- RevisionHistory:
--		Date				Author			Modification
--		2014-11-04			Paul Allen		Created
/**************************************************************************************************/
(
 @ExtractClientName		VARCHAR(50)
,@ProductTypeID			INT
)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

SELECT DISTINCT O.MatterNumber, O.OrganisationID, O.PersonID, O.OrganisationInvolvement, O.CompanyName, O.Telephone1, O.Address, O.PostCode
FROM [Extract].[Organisation] O
INNER JOIN [Extract].[Claim] C ON C.MatterNumber = O.MatterNumber
WHERE C.[Insurer] = @ExtractClientName
AND C.[ProductTypeID] = @ProductTypeID
AND C.[IncludeRecord] =1
AND C.[ExcludeRecord] != 1