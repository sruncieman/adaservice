﻿CREATE PROCEDURE [Extract].[uspGetLastDWHLoadAndPopulate]
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON 

TRUNCATE TABLE [Extract].[DWHLoadTable]

INSERT INTO [Extract].[DWHLoadTable] (LoadID, LoadDay, LoadStart, LoadFinish, ErrorCount)
SELECT	(SELECT TOP 1 Load_ID FROM VF_Stage.[Keoghs_DW].[dbo].[LOAD_CONTROL] WHERE Load_Start >= LoadStart ORDER BY Load_Start ASC) LoadID,
		LoadDay,
		LoadStart,
		DATEADD(Second,(DATEPART(hh, TimeVal) * 60 * 60) + (DATEPART(mi, TimeVal) * 60) + DATEPART(s, TimeVal),LoadStart) LoadEnd,
		Errors
FROM	(
			SELECT	CONVERT(DATE,CONVERT(VARCHAR(10),run_date+1)) LoadDay, 
					CONVERT(DATETIME,CONVERT(VARCHAR(10),run_date)+ ' ' + STUFF(STUFF(STUFF(run_time, 1, 0, REPLICATE('0', 6 - LEN(run_time))),3,0,':'),6,0,':')) LoadStart,
					run_duration,
					STUFF(STUFF(STUFF(run_duration, 1, 0, REPLICATE('0', 6 - LEN(run_duration))),3,0,':'),6,0,':') TimeVal,
					(SELECT	SUM(CASE WHEN ErrorId = 0 THEN 0 ELSE 1 END) Errors
					FROM	VF_Stage.keoghs_stage.dbo.Process_Log
					WHERE	ProcessStart > CONVERT(Varchar(50),(YEAR(GETDATE()) *10000) + MONTH(GETDATE())*100 + DAY(GETDATE()-1)) + ' 21:00:00') Errors
			FROM	VF_Stage.[msdb].[dbo].[sysjobhistory]
			WHERE	instance_id =	(SELECT	MAX(Instance_Id) Instance_ID
									FROM	VF_Stage.[msdb].[dbo].[sysjobhistory]
									WHERE	step_name = 'SSIS Keoghs_DW_025_Extract VF')
					AND job_id =	(SELECT	job_id
									FROM	VF_Stage.[msdb].[dbo].[sysjobs]
									WHERE	name = 'Keoghs_DW_010_Main_ETL Job')
					AND run_status = 1
		) msdb

EXECUTE [Extract].[uspRunJobCheck]