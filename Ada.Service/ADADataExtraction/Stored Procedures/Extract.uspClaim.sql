﻿CREATE PROCEDURE [Extract].[uspClaim]
/**************************************************************************************************/
-- ObjectName:		[Extract].[uspClaim]
--
-- Description:						
--
-- Steps:	
--		
-- RevisionHistory:
--		Date				Author			Modification
--		2014-10-13			Paul Allen		Created
/**************************************************************************************************/
(
 @ExtractClientName		VARCHAR(50)
,@ProductTypeID			INT
)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

SELECT MatterNumber, Insurer, IncidentDate, ClaimStatus, InsurerClientReference, IncidentCircumstances
FROM [Extract].[Claim]
WHERE [Insurer] = @ExtractClientName
AND [ProductTypeID] = @ProductTypeID
AND [IncludeRecord] =1
AND [ExcludeRecord] != 1