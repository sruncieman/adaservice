﻿
CREATE PROCEDURE [Extract].[uspPerson]
/**************************************************************************************************/
-- ObjectName:		[dbo].[uspCreditHirePreLitigated_Person_Output]
--
-- Description:						
--
-- Steps:	
--		
-- RevisionHistory:
--		Date				Author			Modification
--		2014-11-04			Paul Allen		Created
/**************************************************************************************************/
(
 @ExtractClientName		VARCHAR(50)
,@ProductTypeID			INT
)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON


SELECT DISTINCT P.MatterNumber, P.VehicleID, P.PersonID, P.PersonInvolvement, P.Salutation, P.FirstName, P.LastName, P.DateOfBirth, P.Gender, P.NINumber, P.LandlineTelephone, P.MobileTelephone, P.Address, P.PostCode
FROM [Extract].[Person] P
INNER JOIN [Extract].[Claim] C ON C.MatterNumber = P.MatterNumber
WHERE C.[Insurer] = @ExtractClientName
AND C.[ProductTypeID] = @ProductTypeID
AND C.[IncludeRecord] =1
AND C.[ExcludeRecord] != 1