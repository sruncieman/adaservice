﻿CREATE PROCEDURE [Extract].[uspGetAceBatchList] @IsSeedProcess TINYINT
AS

IF @IsSeedProcess = 255
BEGIN
	SELECT DATEPART(YEAR,ClaimNotificationDate) [Year], DATEPART(MONTH,ClaimNotificationDate) [Month]
	FROM [Extract].[Claim_ACE]
	WHERE ClaimNotificationDate < GETDATE()
	AND ClaimNotificationDate >= '01-MAY-2010'
	AND IncludeRecord = 1
	AND ExcludeRecord = 0
	GROUP BY DATEPART(YEAR,ClaimNotificationDate), DATEPART(MONTH,ClaimNotificationDate)
	ORDER BY DATEPART(YEAR,ClaimNotificationDate) ASC, DATEPART(MONTH,ClaimNotificationDate) ASC
END
ELSE
BEGIN
	SELECT TOP 1 0 [Year], 0 [Month]
	FROM [Extract].[Claim_ACE]
	WHERE IncludeRecord = 1
	AND ExcludeRecord = 0
END