﻿
CREATE PROCEDURE [Extract].[uspVehicle]
/**************************************************************************************************/
-- ObjectName:		[dbo].[uspCreditHirePreLitigated_Vehicle_Output]
--
-- Description:						
--
-- Steps:	
--		
-- RevisionHistory:
--		Date				Author			Modification
--		2014-11-04			Paul Allen		Created
/**************************************************************************************************/
(
 @ExtractClientName		VARCHAR(50)
,@ProductTypeID			INT
)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

SELECT DISTINCT V.MatterNumber, V.VehicleID, V.OrganisationID, V.VehicleInvolvement, V.VehicleRegistration, V.VehicleMake, V.VehicleModel, V.DateHireStarted, V.DateHireEnded
FROM [Extract].[Vehicle] V
INNER JOIN [Extract].[Claim] C ON C.MatterNumber = V.MatterNumber
WHERE C.[Insurer] = @ExtractClientName
AND C.[ProductTypeID] = @ProductTypeID
AND C.[IncludeRecord] =1
AND C.[ExcludeRecord] != 1