﻿CREATE PROCEDURE [Extract].[UspGetACESSISParameters]
AS

SELECT CAST((SELECT TOP 1 Value FROM [Ref].[Configuration] WHERE ConfigurationDescriptionID = 4) AS BIT) IsSeedProcess
	  ,CAST((SELECT TOP 1 Value FROM [Ref].[Configuration] WHERE ConfigurationDescriptionID = 5) AS VARCHAR(300)) ACESourceFileDirectory
	  ,CAST((SELECT TOP 1 Value FROM [Ref].[Configuration] WHERE ConfigurationDescriptionID = 6) AS VARCHAR(300)) ACEOutputFileDirectory
	  ,CAST((SELECT TOP 1 Value FROM [Ref].[Configuration] WHERE ConfigurationDescriptionID = 7) AS VARCHAR(300)) ACESourceFileArchiveDirectory
	  ,CAST((SELECT TOP 1 Value FROM [Ref].[Configuration] WHERE ConfigurationDescriptionID = 8) AS INT) ACESourceFileArchiveRetentionPeriod
	  ,CAST((SELECT TOP 1 Value FROM [Ref].[Configuration] WHERE ConfigurationDescriptionID = 9) AS VARCHAR(300)) ACESourceClaimFileLike
	  ,CAST((SELECT TOP 1 Value FROM [Ref].[Configuration] WHERE ConfigurationDescriptionID = 10) AS VARCHAR(300)) ACESourcePolicyFileLike