﻿using System;
using System.Collections.Generic;
using System.Threading;
using MDA.RiskService;
using System.Configuration;
using MDA.DAL;
using ADAServices.Factory;
using ADAServices.Interface;
using MDA.Common.Server;
using MDA.RiskService.Interface;
using MDA.RiskService.Model;
using System.IO;
using System.Net.Mail;
using System.Diagnostics;
using MDA.Common.Enum;


namespace MDA.Pipeline.Service
{
    public class PipelineEngine
    {
        static DateTime lastOvernightSync = new DateTime(2010, 1, 1);

        private static void RequestbatchReport(int batchId)
        {
            ADAServicesFactory.CreateADAServices().GenerateBatchReport(new GenerateBatchReportParam() { RiskBatchId = batchId });
        }

        private static int ProcessBatchStream(CurrentContext ctx, Stream stream, string fileName, string mimeType, string batchEntityType, string clientBatchRef)
        {
            if (stream != null)
            {
                ADAServicesFactory.CreateADAServices().UploadBatchOfClaimsFile(stream, batchEntityType, fileName, mimeType, ctx.RiskClientId, ctx.RiskUserId, clientBatchRef, ctx.Who);
            }

            return 0;
        }

        public static bool PerformFolderMonitoringCheck(ServiceConfigSettings config, Func<ProgressSeverity, string, bool, int> ShowProgress)
        {
            try
            {
                using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                {
                    MDA.Pipeline.Pipeline.Stage0_PerformFolderMonitoringCheck(ctx, config, ProcessBatchStream);

                    return true;
                }
            }
            catch(Exception ex)
            {
                string err = ex.Message;
                while( ex.InnerException != null )
                {
                    ex = ex.InnerException;
                    err = err + " INNER: " + ex.Message;
                }

                if (ShowProgress != null)
                    ShowProgress(ProgressSeverity.Error, err, true);
            }

            return false;
        }

        public static void SendEmail(ProgressSeverity severity, string message)
        {
            string myHost = System.Net.Dns.GetHostName();
            if (!myHost.Contains("LT-"))
            {
                MailMessage mail = new MailMessage("ADAMonitoring@keoghs.co.uk", ConfigurationManager.AppSettings["ErrorRecipients"]);
                SmtpClient client = new SmtpClient();
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = ConfigurationManager.AppSettings["SmtpServer"];

                mail.Subject = "ADA Error";
                mail.Body = string.Format("An ADA Error has occurred on server {0} with a Severity Level of {1}", myHost, severity.ToString());
                mail.Body += Environment.NewLine + Environment.NewLine;
                mail.Body += string.Format("Error Message: {0}", message);

                client.Send(mail);
            }
        }


        private static void Sync(CurrentContext ctx, ServiceConfigSettings config, SyncOrDedupeType syncOrDedupeType)
        {
            if (config.EnableSync)
            {
                ctx.ShowProgress(ProgressSeverity.Info, "Sync with IBASE : Starting", true);

                Stopwatch sw = new Stopwatch();
                sw.Start();                

                IRiskServices riskServices = new RiskServices(ctx);

                if (!riskServices.IsNextBatchToProcessForInternalClient())
                {
                    MDA.Pipeline.Pipeline.Synchronise(ctx, config, true);

                    riskServices.SaveSyncronisationTiming(sw.ElapsedMilliseconds, syncOrDedupeType);

                    ctx.ShowProgress(ProgressSeverity.Info, "Sync with IBASE : Complete", true);
                }
                else
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Sync with IBASE : Skip - Next Batch Is Internal Client", true);
                }

            }
            else
                ctx.ShowProgress(ProgressSeverity.Info, "Sync with IBASE : Disabled", true);


            //if (config.EnableDedupe)
            //{
            //    ctx.ShowProgress(ProgressSeverity.Info, "De-Duplicate ADA : Starting", true);

            //    Stopwatch sw = new Stopwatch();
            //    sw.Start();

            //    MDA.Pipeline.Pipeline.Deduplicate(ctx, config);

            //    IRiskServices riskServices = new RiskServices(ctx);

            //    riskServices.SaveDeduplicationTiming(sw.ElapsedMilliseconds, syncOrDedupeType);


            //    ctx.ShowProgress(ProgressSeverity.Info, "De-Duplicate ADA : Complete", true);
            //}
            //else
            //    ctx.ShowProgress(ProgressSeverity.Info, "De-Duplicate ADA : Disabled", true);

            //if (config.EnableSync)
            //{
            //    ctx.ShowProgress(ProgressSeverity.Info, "Sync with IBASE : Starting", true);

            //    IRiskServices riskServices = new RiskServices(ctx);

            //    if (!riskServices.IsNextBatchToProcessForInternalClient())
            //    {
            //        MDA.Pipeline.Pipeline.Synchronise(ctx, config, false);

            //        ctx.ShowProgress(ProgressSeverity.Info, "Sync with IBASE : Complete", true);
            //    }
            //    else
            //    {
            //        ctx.ShowProgress(ProgressSeverity.Info, "Sync with IBASE : Skip - Next Batch Is Internal Client", true);
            //    }

                
            //}
            //else
            //    ctx.ShowProgress(ProgressSeverity.Info, "Sync with IBASE : Disabled", true);
        }

        private static void Dedupe(CurrentContext ctx, ServiceConfigSettings config, SyncOrDedupeType syncOrDedupeType)
        {
            if (config.EnableDedupe)
            {
                IRiskServices riskServices = new RiskServices(ctx);

                //Check if we should dedupe
                bool boolRunDeduplication = riskServices.CheckToRunDeduplication();

                if (boolRunDeduplication)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "De-Duplicate ADA : Starting", true);

                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    MDA.Pipeline.Pipeline.Deduplicate(ctx, config);

                    riskServices.SaveDeduplicationTiming(sw.ElapsedMilliseconds, syncOrDedupeType);

                    ctx.ShowProgress(ProgressSeverity.Info, "De-Duplicate ADA : Complete", true);
                }
            }
            else
                ctx.ShowProgress(ProgressSeverity.Info, "De-Duplicate ADA : Disabled", true);
        }

        private static void RunReports(CurrentContext ctx, ServiceConfigSettings config)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["RunReports"]))
            {
                ctx.ShowProgress(ProgressSeverity.Info, "Run Reports : Starting", true);
               
                try
                {
                    //ctx.db.uspRunReports();

                    string sql = "exec uspRunReports ";

                    ctx.db.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, sql);


                }
                catch (Exception ex)
                {

                    ctx.ShowProgress(ProgressSeverity.Info, "Run Reports : Error " + ex.InnerException , true);
                }

                ctx.ShowProgress(ProgressSeverity.Info, "Run Reports : Complete", true);


            }
            else
                ctx.ShowProgress(ProgressSeverity.Info, "Run Reports : Disabled", true);

        }

        public static RunningMetrics GetRunningMetrics()
        {
            using (CurrentContext ctx = new CurrentContext(0, 0, "System", null))
            {
                return new RiskServices(ctx).GetRunningMetrics();
            }
        }

        public static void RecoverPipeline(ServiceConfigSettings config, Func<ProgressSeverity, string, bool, int> ShowProgress)
        {
            ShowProgress(ProgressSeverity.Info, "Claim Processing Recovery Starting...", true);

            try
            {

                    int? batchId = null;

                    //IRiskServices riskServices = new RiskServices(ctx);

                    while(true)
                    {
                        ShowProgress(ProgressSeverity.Info, "Recovery: Check for scoring", true);
                        if (config.ProcessScoring)
                        {
                            using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                            {
                                while (config.ScoringMultithreaded ? MDA.Pipeline.Pipeline.Stage5_ScoreClaimQueueMultithreaded(ctx, config, batchId) != null : MDA.Pipeline.Pipeline.Stage5_ScoreClaimQueueSingleProcess(ctx, config, batchId) != null)
                                {
                                    ShowProgress(ProgressSeverity.Info, "Recovery: Finished Scoring batch", true);
                                }
                            }
                        }
                        else
                            ShowProgress(ProgressSeverity.Info, "Recovery: Scoring disabled", true);

                        ShowProgress(ProgressSeverity.Info, "Recovery: Check for 3rd party calls", true);
                        if (config.Process3rdParty)
                        {
                            using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                            {
                                batchId = MDA.Pipeline.Pipeline.Stage4_ProcessClaimQueueThirdParties(ctx, config, batchId);

                                if (batchId == null)
                                    break;
                            }
                        }
                        else
                        {
                            ShowProgress(ProgressSeverity.Info, "Recovery: 3rd party disabled", true);
                            break;
                        }
                    }

                    if (!config.ProcessBatches && !config.ProcessLoading && !config.Process3rdParty && config.ProcessScoring)
                    {
                        // Do Nothing
                    }
                    else{

                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            ShowProgress(ProgressSeverity.Info, "Recovery: CleanUpPartiallyCreatedBatchClaims", true);
                            MDA.Pipeline.Pipeline.Stage2_CleanUpPartiallyCreatedBatchClaims(ctx);
                        }

                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            ShowProgress(ProgressSeverity.Info, "Recovery: RecoverBatchesWithClaimsBeingLoaded", true);
                            MDA.Pipeline.Pipeline.Stage3_RecoverBatchesWithClaimsBeingLoaded(ctx, ShowProgress);
                        }

                        List<RiskBatch> batches = new List<RiskBatch>();

                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            ShowProgress(ProgressSeverity.Info, "Recovery: ProcessBatchesWithClaimsBeingLoaded", true);
                            batches = new RiskServices(ctx).GetListOfBatchesWithClaimsBeingLoaded();
                        }

                        foreach (var riskBatch in batches)
                        {
                            ShowProgress(ProgressSeverity.Info, "Recovery: Check for Batches to load", true);
                            if (config.ProcessLoading)
                            {
                                using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                                {
                                    MDA.Pipeline.Pipeline.Stage3_LoadBatchClaimsIntoDatabase(ctx, config, riskBatch.Id);
                                }
                            }
                            else
                                ShowProgress(ProgressSeverity.Info, "Recovery: Claims loading disabled", true);

                            ShowProgress(ProgressSeverity.Info, "Recovery: Check for 3rd party calls", true);
                            if (config.Process3rdParty)
                            {
                                using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                                {
                                    MDA.Pipeline.Pipeline.Stage4_ProcessClaimQueueThirdParties(ctx, config, riskBatch.Id);
                                }
                            }
                            else
                                ShowProgress(ProgressSeverity.Info, "Recovery: 3rd party disabled", true);
                        }

                        foreach (var riskBatch in batches)
                        {
                            ShowProgress(ProgressSeverity.Info, "Recovery: Check for scoring", true);
                            if (config.ProcessScoring)
                            {
                                using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                                {
                                    if (config.ScoringMultithreaded)
                                        MDA.Pipeline.Pipeline.Stage5_ScoreClaimQueueMultithreaded(ctx, config, riskBatch.Id);
                                    else
                                        MDA.Pipeline.Pipeline.Stage5_ScoreClaimQueueSingleProcess(ctx, config, riskBatch.Id);
                                }
                            }
                            else
                                ShowProgress(ProgressSeverity.Info, "Recovery: Scoring disabled", true);
                        }

                        #region SYNC
                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            Sync(ctx, config, SyncOrDedupeType.Recovery);
                        }
                        #endregion

                        #region DEDUPE
                        // A check is performed to determine whether dedupe needs to run first. 
                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            Dedupe(ctx, config, SyncOrDedupeType.Recovery);
                        }
                        #endregion

                        if (config.EnableBackups)
                        {
                            using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                            {
                                ShowProgress(ProgressSeverity.Info, "Initial Full Backup Starting (if required)", true);
                                MDA.Pipeline.Pipeline.PerformInitialFullBackup(ctx, config);
                                ShowProgress(ProgressSeverity.Info, "Initial Full Backup Complete", true);
                            }
                        }
                        else
                            ShowProgress(ProgressSeverity.Info, "Initial Full Backup - Disabled", true);

                        lastOvernightSync = DateTime.Today;
                    }
                
            }
            catch(Exception ex)
            {
                while (ex != null)
                {
                    ShowProgress(ProgressSeverity.Error, "Exception: " + ex.Message, true);
                    ex = ex.InnerException;
                }
               // Console.WriteLine("Hit a key....");
               // Console.ReadKey();
            }
        }


        //public static int? RunBatchQueue(Func<ProgressSeverity, string, bool, int> ShowProgress)
        //{
        //    int? batchId = null;

        //    ShowProgress("Starting Batch Process...", true);

        //    using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
        //    {
        //        IRiskServices riskServices = new RiskServices(ctx);

        //        try
        //        {
        //            batchId = MDA.Pipeline.Pipeline.ProcessBatchQueue(ctx, config, null, ShowProgress);
        //        }
        //        catch(Exception ex)
        //        {
        //        }
        //    }

        //    return batchId;
        //}

        public static void RunPipeline(ServiceConfigSettings config, Func<ProgressSeverity, string, bool, int> ShowProgress)
        {
            DateTime lastOvernightSync = new DateTime(2010, 1, 1);

            ShowProgress(ProgressSeverity.Info, "Claim Processing Starting...", true);
            

                //IRiskServices riskServices = new RiskServices(ctx);

                try
                {
                    int? batchId = null;

                    ShowProgress(ProgressSeverity.Info, "Normal processing - Starting", true);

                    //#region Batches

                    //ShowProgress(ProgressSeverity.Info, "Check for new batches", true);
                    //if (config.ProcessBatches)
                    //{
                    //    using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                    //    {
                    //        batchId = MDA.Pipeline.Pipeline.Stage2_ProcessBatchQueue(ctx, config, null);
                    //    }
                    //}
                    //else
                    //    ShowProgress(ProgressSeverity.Info, "Batch loading disabled", true);

                    //#endregion

                    #region Loading

                    ShowProgress(ProgressSeverity.Info, "Check Claims to processs", true);
                    if (config.ProcessLoading)
                    {
                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            MDA.Pipeline.Pipeline.Stage3_LoadBatchClaimsIntoDatabase(ctx, config, batchId);
                        }
                    }
                        
                    else
                        ShowProgress(ProgressSeverity.Info, "Claims loading disabled", true);

                    #endregion

                    #region 3rd party

                    ShowProgress(ProgressSeverity.Info, "Check for 3rd party calls", true);
                    if (config.Process3rdParty)
                     {
                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            MDA.Pipeline.Pipeline.Stage4_ProcessClaimQueueThirdParties(ctx, config, batchId);
                        }
                    }                       
                    else
                        ShowProgress(ProgressSeverity.Info, "3rd party disabled", true);

                    #endregion

                    #region Scoring

                    ShowProgress(ProgressSeverity.Info, "Check for scoring", true);
                    if (config.ProcessScoring)
                      {
                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            if (config.ScoringMultithreaded)
                                MDA.Pipeline.Pipeline.Stage5_ScoreClaimQueueMultithreaded(ctx, config, batchId);
                            else
                                MDA.Pipeline.Pipeline.Stage5_ScoreClaimQueueSingleProcess(ctx, config, batchId);
                        }
                    }                        
                    else
                        ShowProgress(ProgressSeverity.Info, "Scoring disabled", true);

                    #endregion

                    #region Redo external calls when one comes online then rescore

                    ShowProgress(ProgressSeverity.Info, "Check for 3rd party retry calls", true);
                    if (config.Process3rdParty)
                    {
                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            MDA.Pipeline.Pipeline.ReProcessClaimQueueThirdParties(ctx, config);
                        }
                    } 
                    else
                        ShowProgress(ProgressSeverity.Info, "3rd party disabled", true);

                    ShowProgress(ProgressSeverity.Info, "Check for re-scoring", true);
                    if (config.ProcessScoring)
                    {
                        using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            if (config.ScoringMultithreaded)
                                MDA.Pipeline.Pipeline.Stage5_ScoreClaimQueueMultithreaded(ctx, config, batchId);
                            else
                                MDA.Pipeline.Pipeline.Stage5_ScoreClaimQueueSingleProcess(ctx, config, batchId);
                        }
                    }                         
                    else
                        ShowProgress(ProgressSeverity.Info, "Scoring disabled", true);

                    #endregion

                    if (config.ReqBatchReports)
                    {
                        ShowProgress(ProgressSeverity.Info, "Requesting Batch Report", true);
                        if (batchId != null)
                            RequestbatchReport((int)batchId);
                        ShowProgress(ProgressSeverity.Info, "Batch Report Complete", true);
                    }
                    else
                    {
                        ShowProgress(ProgressSeverity.Info, "Requesting Batch Report Disabled", true);
                    }

                    #region Mobile Reports
                    using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                    {
                        RunReports(ctx, config);
                    }
                    #endregion

                    #region SYNC
                    using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                    {
                        Sync(ctx, config, SyncOrDedupeType.Pipeline);
                    }
                    #endregion

                    #region DEDUPE
                    // A check is performed to determine whether dedupe needs to run first. 
                    using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
                    {
                        Dedupe(ctx, config, SyncOrDedupeType.Pipeline);
                    }
                    #endregion

                    ShowProgress(ProgressSeverity.Info, "Normal processing - Completed", true);
                }
                catch (Exception ex)
                {
                    while (ex != null)
                    {
                        ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
                        ex = ex.InnerException;
                    }

                    ShowProgress(ProgressSeverity.Error, "Exception: Pause, Sleeping 60 seconds)", true);

                    Thread.Sleep(60 * 1000);
                }
       
        }

        public static bool NothingToDo(ServiceConfigSettings config, Func<ProgressSeverity, string, bool, int> ShowProgress)
        {
            bool NothingToDo = true;

            using (CurrentContext ctx = new CurrentContext(0, 0, "System", ShowProgress))
            {
                IRiskServices riskServices = new RiskServices(ctx);

                try
                {
                    NothingToDo = ((!config.ProcessLoading || !riskServices.IsAClaimToLoad(ctx.SubmitDirect)) &&
                                        (!config.ProcessScoring || !riskServices.IsAClaimToScore(ctx.ScoreDirect)) &&
                                        (!config.Process3rdParty || !riskServices.IsAClaimToProcessThirdParties(ctx.SubmitDirect)) &&
                                        (!config.ProcessBatches || !riskServices.IsABatchToProcess(ctx.SubmitDirect)));

                    if (NothingToDo)
                    {
                        if (DateTime.Now.Hour > 3 && DateTime.Today > lastOvernightSync.Date)
                        {
                            ShowProgress(ProgressSeverity.Info, "Overnight processing : Starting", true);

                            #region SYNC and DEDUPE
                            Sync(ctx, config, SyncOrDedupeType.Overnight);
                            // A check is performed to determine whether dedupe needs to run first. 
                            Dedupe(ctx, config, SyncOrDedupeType.Overnight);
                            Sync(ctx, config, SyncOrDedupeType.Overnight);
                            #endregion

                            lastOvernightSync = DateTime.Today;

                            ShowProgress(ProgressSeverity.Info, "Overnight processing : Complete", true);
                        }
                    }
                }
                catch(Exception)
                {
                    throw;
                }
            }
            return NothingToDo;
        }


        
    }

}
