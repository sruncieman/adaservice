﻿//using RiskEngine.Scoring.Entities;
using System.Runtime.Serialization;
using RiskEngine.Model;
//using RiskEngine.Interfaces;
using System.Collections.Generic;

namespace RiskEngine.Model
{

    [CollectionDataContract(ItemName = "M", Namespace = XmlScores.Namespace)]
    public class ListOfMsgs : List<string> { }

    [CollectionDataContract(ItemName = "C", Namespace = XmlScores.Namespace)]
    public class ListOfStringData : List<string> { }

    [CollectionDataContract(ItemName = "R", Namespace = XmlScores.Namespace)]
    public class ListOfListOfStringData : List<ListOfStringData> { }

    /// <summary>
    /// Class used to populate RiskResult Dictionary collection. The output of each Rule execution is one of these
    /// </summary>
    [DataContract(Namespace = XmlScores.Namespace)]
    public class RiskResult
    {
        [DataMember(Name = "Score")]
        public int Score { get; set; }
        [DataMember(Name = "ML")]
        public ListOfMsgs MessageLow { get; set; }
        [DataMember(Name = "MM")]
        public ListOfMsgs MessageMedium { get; set; }
        [DataMember(Name = "MH")]
        public ListOfMsgs MessageHigh { get; set; }

        [DataMember(Name = "DL")]
        public ListOfListOfStringData DataLow { get; set; }
        [DataMember(Name = "DM")]
        public ListOfListOfStringData DataMedium { get; set; }
        [DataMember(Name = "DH")]
        public ListOfListOfStringData DataHigh { get; set; }

        [DataMember(Name = "RSId")]
        public int RuleSetId { get; set; }

        public RiskResult()
        {
            Score = 0;
            MessageLow = new ListOfMsgs();  //string.Empty;
            MessageMedium = new ListOfMsgs(); ////new  string.Empty;
            MessageHigh = new ListOfMsgs(); // string.Empty;

            DataLow = new ListOfListOfStringData();
            DataMedium = new ListOfListOfStringData();
            DataHigh = new ListOfListOfStringData();

            RuleSetId = 0;
        }
    }


}
