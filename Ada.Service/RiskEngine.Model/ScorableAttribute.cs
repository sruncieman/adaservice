﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskEngine.Model
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ScoreableItemAttribute : System.Attribute
    {
        private bool _isCollection = false;
        private string _rulePath;

        /// <summary>
        /// 
        /// </summary>
        public bool IsCollection
        {
            get { return _isCollection; }
            set { _isCollection = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string RulePath
        {
            get { return _rulePath; }
            set { _rulePath = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public ScoreableItemAttribute()
        {
        }
    }
}
