﻿
using System;
using System.Collections.Generic;
using System.Linq;
using RiskEngine.DAL;

namespace RiskEngine.Model
{

    /// <summary>
    /// Business Object that maps to a RuleScore entity
    /// </summary>
    public class RuleScoreData
    {
        public int RuleScoreId { get; set; }
        public int RootRuleSetId { get; set; }
        public string RuleName { get; set; }
        public int ThenScore { get; set; }
        public int ElseScore { get; set; }
        public string ThenMessageLow { get; set; }
        public string ElseMessageLow { get; set; }
        public string ThenMessageMedium { get; set; }
        public string ElseMessageMedium { get; set; }
        public string ThenMessageHigh { get; set; }
        public string ElseMessageHigh { get; set; }
        public string ThenError { get; set; }
        public string ElseError { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public RuleScoreData()
        {
        }

        public RuleScoreData(RuleScoreEntity rse)
        {
            RuleScoreId       = rse.RuleScoreId;
            RootRuleSetId     = rse.RootRuleSetId;
            RuleName          = rse.RuleName;
            ThenScore         = rse.ThenScore;
            ElseScore         = rse.ElseScore;
            ThenMessageLow    = rse.ThenMessageLow;
            ElseMessageLow    = rse.ElseMessageLow;
            ThenMessageMedium = rse.ThenMessageMedium;
            ElseMessageMedium = rse.ElseMessageMedium;
            ThenMessageHigh   = rse.ThenMessageHigh;
            ElseMessageHigh   = rse.ElseMessageHigh;
            ThenError         = rse.ThenError;
            ElseError         = rse.ElseError;
            CreatedBy         = rse.CreatedBy;
            CreatedDate       = rse.CreatedDate;
            ModifiedBy        = rse.ModifiedBy;
            ModifiedDate      = rse.ModifiedDate;
        }
    }

}

