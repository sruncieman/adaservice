﻿
using System;
using System.Collections.Generic;
using System.Linq;
using RiskEngine.DAL;

namespace RiskEngine.Model
{
    /// <summary>
    /// Business Object that maps to a RuleGroup entity
    /// </summary>
    public class RuleGroupData 
    {
        public int RuleGroupId { get; set; }
        public int RootRuleGroupId { get; set; }
        public int ClientId { get; set; }
        public string RuleGroupName { get; set; }        
        public string EntityClassType { get; set; }
        public int Status { get; set; }
        public int Version { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public List<RuleSetData> RuleSets { get; set; }
        public List<RuleScoreData> RuleScores { get; set; }
        public List<RuleVariableData> RuleVariables { get; set; }

        public List<RuleSetData> DefRuleSets { get; set; }
        public List<RuleScoreData> DefRuleScores { get; set; }
        public List<RuleVariableData> DefRuleVariables { get; set; }

        public RuleGroupData()
        {
        }

        public RuleGroupData(RuleGroupEntity rge)
        {
            ClientId        = rge.RuleClientId;
            Status          = rge.Status;
            RuleGroupId     = rge.RuleGroupId;
            RootRuleGroupId = rge.RootRuleGroupId;
            RuleGroupName   = rge.RuleGroupName;
            EntityClassType = rge.EntityClassType;
            Version         = rge.Version;

            DefRuleScores    = new List<RuleScoreData>();
            DefRuleVariables = new List<RuleVariableData>();
            DefRuleSets      = new List<RuleSetData>();
            RuleScores       = new List<RuleScoreData>();
            RuleSets         = new List<RuleSetData>();
            RuleVariables    = new List<RuleVariableData>();
        }
    }

    
}

