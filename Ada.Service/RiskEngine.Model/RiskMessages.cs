﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common;

namespace RiskEngine.Model
{
    public static class XmlScores
    {
        public const string Namespace = "http://keo/ScoresV1";
    }

    public static class XmlMessages
    {
        public const string Namespace = "http://keo/MsgsV1";
    }

    //[DataContract(Namespace = XmlMessages.Namespace)]
    public class EntityResultMessages
    {
        //[DataMember(Name="RSN")]
        public string RuleSetName { get; set; }

        //[DataMember(Name = "LML")]
        public ListOfStrings LowMessageLines { get; set; }
        //[DataMember(Name = "MML")]
        public ListOfStrings MediumMessageLines { get; set; }
        //[DataMember(Name = "HML")]
        public ListOfStrings HighMessageLines { get; set; }
    }


    ///// <summary>
    ///// Structure to populate to get a risk performed
    ///// </summary>
    //public class CalculateRiskRequest //: ICalculateRiskRequest
    //{
    //    /// <summary>
    //    /// RuleGroup to execute
    //    /// </summary>
    //    public RuleGroupData RuleGroupData { get; set; }

    //    /// <summary>
    //    /// Entity to process
    //    /// </summary>
    //    public IRiskEntity Entity { get; set; } // eg ClaimRisk
    //}

    [CollectionDataContract(ItemName = "Item", Namespace = XmlScores.Namespace)]
    public class ListOfInts : List<int> { }

    [CollectionDataContract(ItemName = "Item", Namespace = XmlScores.Namespace)]
    public class ListOfStrings: List<string> { }

    [CollectionDataContract(ItemName = "Results", Namespace = XmlScores.Namespace)]
    public class DictionaryOfStringString : Dictionary<string, string> { }

    [CollectionDataContract(ItemName = "MResults", Namespace = XmlScores.Namespace)]
    public class DictionaryOfMethodResultRecords : Dictionary<string, MethodResultRecord> { }

    [CollectionDataContract(ItemName = "RGResults", Namespace = XmlScores.Namespace)]
    public class DictionaryOfRiskResults : Dictionary<string, RiskResult> { }

    [CollectionDataContract(ItemName = "EResults", Namespace = XmlScores.Namespace)]
    public class DictionaryOfDictionaryOfRiskResults : Dictionary<string, DictionaryOfRiskResults> { }

    [CollectionDataContract(ItemName = "Item", Namespace = XmlMessages.Namespace)]
    public class ListOfEntityResultMessages : List<EntityResultMessages> { }


    /// <summary>
    /// Structure to populate to get a risk performed
    /// </summary>
    public class CalculateRiskRequest //: ICalculateRiskRequest
    {
        /// <summary>
        /// RuleGroup to execute
        /// </summary>
        public RuleGroupData RuleGroupData { get; set; }

        /// <summary>
        /// Entity to process
        /// </summary>
        public IRiskEntity Entity { get; set; } // eg ClaimRisk
    }

    [DataContract(Namespace = XmlScores.Namespace)]
    public class CalculateRiskResponse // This structure holds a collection of all the results from scoring one entity against multiple rulesets
    {
        /// <summary>
        /// Results. A dictionary keyed on RuleSetName
        /// </summary>
        [DataMember]
        public DictionaryOfDictionaryOfRiskResults Results { get; set; }

        /// <summary>
        /// List of errors found across the Rulegroup process
        /// </summary>
        [DataMember]
        public ListOfStrings Errors { get; set; }

        /// <summary>
        /// Total score across rulegroup
        /// </summary>
        [DataMember(Name = "RGS")]
        public int RuleGroupScore { get; set; }

        /// <summary>
        /// Total score across rulegroup
        /// </summary>
        [DataMember(Name = "RGKAC")]
        public int RuleGroupKeyAttractorCount { get; set; }

        [DataMember(Name = "MRH")]
        public DictionaryOfStringString MethodResultHistory;

        /// <summary>
        /// Default contructor.  Sets score to zero and adds two empty return lists
        /// </summary>
        public CalculateRiskResponse()
        {
            RuleGroupScore = 0;
            RuleGroupKeyAttractorCount = 0;
            Results = new DictionaryOfDictionaryOfRiskResults(); // new Dictionary<string, Dictionary<string, RiskResult>>();
            Errors = new ListOfStrings(); // new List<string>();
        }


        /// <summary>
        /// Support method to return all the Messages in a list
        /// </summary>
        /// <returns></returns>
        public ListOfEntityResultMessages GetScoreMessages()
        {
            ListOfEntityResultMessages ret = new ListOfEntityResultMessages();

            foreach (KeyValuePair<string, DictionaryOfRiskResults> s in Results)
            {
                DictionaryOfRiskResults rr = s.Value;

                if (s.Value.Count != 0)
                {
                    EntityResultMessages cms = new EntityResultMessages();

                    cms.RuleSetName = s.Key;
                    cms.LowMessageLines = new ListOfStrings();
                    cms.MediumMessageLines = new ListOfStrings();
                    cms.HighMessageLines = new ListOfStrings();

                    bool changed = false;

                    foreach (KeyValuePair<string, RiskResult> t in rr)
                    {
                        if (t.Value.MessageLow != null && t.Value.MessageLow.Count > 0)
                        {
                            cms.LowMessageLines.AddRange(t.Value.MessageLow);
                            changed = true;
                        }

                        if (t.Value.MessageMedium != null && t.Value.MessageMedium.Count > 0)
                        {
                            cms.MediumMessageLines.AddRange(t.Value.MessageMedium);
                            changed = true;
                        }

                        if (t.Value.MessageHigh != null && t.Value.MessageHigh.Count > 0)
                        {
                            cms.HighMessageLines.AddRange(t.Value.MessageHigh);
                            changed = true;
                        }
                    }

                    if (changed)
                        ret.Add(cms);
                }
            }

            return ret;
        }

        /// <summary>
        /// A support method that returns all the Scores of this entity as a TreeNode
        /// </summary>
        /// <param name="rootString"></param>
        /// <returns></returns>
        public MessageNode ScoresAsNodes(string rootString, bool includeMethodResultHistory = false)
        {
            MessageNode root = new MessageNode(rootString);

            root.Nodes.Add(new MessageNode("RuleGroup Score [" + RuleGroupScore + "]"));

            foreach (KeyValuePair<string, DictionaryOfRiskResults> s in Results)
            {
                DictionaryOfRiskResults rr = s.Value;

                if (s.Value.Count == 0)
                {
                    MessageNode n0 = new MessageNode(s.Key + "- No Result");

                    root.Nodes.Add(n0);
                }
                else
                {
                    MessageNode n0 = new MessageNode(s.Key);

                    MessageNode n1 = new MessageNode("Low");
                    MessageNode n2 = new MessageNode("Medium");
                    MessageNode n3 = new MessageNode("High");
                    MessageNode n4 = new MessageNode("RuleScore");
                    MessageNode n5 = new MessageNode("RuleSetId");

                    int score = 0;

                    foreach (KeyValuePair<string, RiskResult> t in rr)
                    {
                        foreach (var x in t.Value.MessageLow)
                            n1.Nodes.Add(new MessageNode(t.Key + ": " + x));

                        foreach (var x in t.Value.MessageMedium)
                            n2.Nodes.Add(new MessageNode(t.Key + ": " + x));

                        foreach (var x in t.Value.MessageHigh)
                            n3.Nodes.Add(new MessageNode(t.Key + ": " + x));

                        n4.Nodes.Add(new MessageNode(t.Key + ": " + t.Value.Score));
                        n5.Nodes.Add(new MessageNode(t.Key + ": " + t.Value.RuleSetId));

                        score += t.Value.Score;
                    }

                    n0.Text = n0.Text + " - Score [" + score.ToString() + "]";

                    n0.Nodes.Add(n1);
                    n0.Nodes.Add(n2);
                    n0.Nodes.Add(n3);
                    n0.Nodes.Add(n4);
                    n0.Nodes.Add(n5);

                    root.Nodes.Add(n0);
                }
            }

            if (Errors.Count > 0)
            {
                MessageNode e0 = new MessageNode("Errors");

                foreach (var e in Errors)
                {
                    e0.Nodes.Add(new MessageNode(e));
                }

                root.Nodes.Add(e0);
            }

            if ( includeMethodResultHistory )
            {
                MessageNode e0 = new MessageNode("Methods Called");

                foreach (var e in MethodResultHistory)
                {
                    e0.Nodes.Add(new MessageNode(e.Key + " = " + e.Value));
                }

                root.Nodes.Add(e0);
            }
            return root;

        }
    }
}
