﻿using System;
using System.Collections.Generic;
using RiskEngine.Model;
using System.Data.Entity;
using MDA.Common.Server;
using MDA.Common;

namespace RiskEngine.Model
{
    public interface IRiskEntity : ICloneable
    {


        #region Properties
        int Db_Id { get; set; }

        DbContext db { get; set; }

        CurrentContext ctx { get; set; }

        CalculateRiskResponse _ScoreResults { get; set; }


        bool Trace { get; set; }

        bool Locked { get; set; }

        /// <summary>
        /// List of INPUT data for scores
        /// </summary>
        List<RuleScoreData> RuleScoreData { get; set; }

        /// <summary>
        /// List of INPUT data for variables
        /// </summary>
        List<RuleVariableData> RuleVariableData { get; set; }

        /// <summary>
        /// The name of the ruleset being executed
        /// </summary>
        string RuleSetName { get; set; }

        string ExecutingRuleName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int RuleSetId { get; set; }

        /// <summary>
        /// OUTPUT Results.  Keyed on RuleSetName
        /// </summary>
        DictionaryOfRiskResults Results { get; set; }

        /// <summary>
        /// List of errors found across the RuleSet.
        /// </summary>
        ListOfStrings Errors { get; set; }

        //MyNode ScoresAsNodes();

        /// <summary>
        /// Score of all children added together
        /// </summary>
        int TotalChildScore { get; set; }

        /// <summary>
        /// Score across all rules
        /// </summary>
        int TotalEntityScore { get; set; }

        /// <summary>
        /// Score across all rules
        /// </summary>
        int UserKACount { get; set; }

       // int TotalAutoKACount { get; set; }

        int AddUpEntityRuleSetScores();

        DictionaryOfMethodResultRecords MethodResultHistory { get; set; }

        #endregion

        #region Methods

        bool SetRuleName(string ruleName);

        void AssignThenScore();
        void AssignThenLow();
        void AssignThenMedium();
        void AssignThenHigh();

        void AssignElseScore();
        void AssignElseLow();
        void AssignElseMedium();
        void AssignElseHigh();

        void AssignThenAll();
        void AssignElseAll();
        void AssignThenMessages();
        void AssignElseMessages();

        void LogError(string error);

        void UpdateKeyAttractorCount(int increment);
        void SetKeyAttractorCount(int value);
        int GetKeyAttractorCount();

        int GetScore();
        int GetTotalScore();

        void SetScore(int score);
        void UpdateScore(int increment);

        void AddLowMessage(string message);
        void AddMediumMessage(string message);
        void AddHighMessage(string message);

        void AddLowMessages(ListOfMsgs messageList);
        void AddMediumMessages(ListOfMsgs messageList);
        void AddHighMessages(ListOfMsgs messageList);

        void AddLowMessageData(ListOfStringData data);
        void AddMediumMessageData(ListOfStringData data);
        void AddHighMessageData(ListOfStringData data);

        void SetLowMessageData(ListOfListOfStringData data);
        void SetMediumMessageData(ListOfListOfStringData data);
        void SetHighMessageData(ListOfListOfStringData data);

        ListOfMsgs GetThenLowMessagesFromCache(string key);
        ListOfMsgs GetThenMediumMessagesFromCache(string key);
        ListOfMsgs GetThenHighMessagesFromCache(string key);
        ListOfMsgs GetElseLowMessagesFromCache(string key);
        ListOfMsgs GetElseMediumMessagesFromCache(string key);
        ListOfMsgs GetElseHighMessagesFromCache(string key);

        string GetThenLowInMessageFromCache(string key);
        string GetThenMediumInMessageFromCache(string key);
        string GetThenHighInMessageFromCache(string key);
        string GetElseLowInMessageFromCache(string key);
        string GetElseMediumInMessageFromCache(string key);
        string GetElseHighInMessageFromCache(string key);

        void AssignThenCacheData(string key);
        void AssignElseCacheData(string key);

        void AssignThenCacheLowData(string key);
        void AssignThenCacheMediumData(string key);
        void AssignThenCacheHighData(string key);
        void AssignElseCacheLowData(string key);
        void AssignElseCacheMediumData(string key);
        void AssignElseCacheHighData(string key);

        void AssignThenCacheLowMessages(string key);
        void AssignThenCacheMediumMessages(string key);
        void AssignThenCacheHighMessages(string key);

        void AssignThenCacheMessages(string key);
        void AssignElseCacheLowMessages(string key);
        void AssignElseCacheMediumMessages(string key);
        void AssignElseCacheHighMessages(string key);

        void AssignElseCacheMessages(string key);
        void AssignScoreAndThenCacheMessages(string key);
        void AssignScoreAndElseCacheMessages(string key);

        void AssignThenLowCacheDataIntoFormattedMessages(string key);
        void AssignThenMediumCacheDataIntoFormattedMessages(string key);
        void AssignThenHighCacheDataIntoFormattedMessages(string key);
        void AssignElseLowCacheDataIntoFormattedMessages(string key);
        void AssignElseMediumCacheDataIntoFormattedMessages(string key);
        void AssignElseHighCacheDataIntoFormattedMessages(string key);

        string MyString(string varName);
        int MyInteger(string varName);
        double MyDouble(string varName);
        DateTime MyDateTime(string varName);
        bool MyBoolean(string varName);
        decimal MyDecimal(string varName);
        char MyChar(string varName);

        int GetThenScore();
        int GetElseScore();

        string GetThenError();
        string GetElseError();

        string GetThenMessageLow();
        string GetElseMessageLow();
        string GetThenMessageMedium();
        string GetElseMessageMedium();
        string GetThenMessageHigh();
        string GetElseMessageHigh();

        ListOfListOfStringData GetThenLowDataFromCache(string key);
        ListOfListOfStringData GetThenMediumDataFromCache(string key);
        ListOfListOfStringData GetThenHighDataFromCache(string key);
        ListOfListOfStringData GetElseLowDataFromCache(string key);
        ListOfListOfStringData GetElseMediumDataFromCache(string key);
        ListOfListOfStringData GetElseHighDataFromCache(string key);

        #endregion

        string DisplayText { get; set; }
        string ReportHeading1 { get; set; }
        string ReportHeading2 { get; set; }

        MessageNode GetScoresAsNodeTree(bool includeChildren);

       // int? AutoKACount { get; set; }

        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        void AssignUsedProperties(IRiskEntity dest);

        void PopulateMessageCacheInputMessagesFromRulesDb(string cacheKey);
    }
}
