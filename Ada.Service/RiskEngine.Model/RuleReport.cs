﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskEngine.Model
{
    public class ReportRulesData
    {

        public int ClientId { get; set; }
        public string ClientName { get; set; }

        public string RuleGroupEntityClassType { get; set; }
        public int RuleGroupRootRuleGroupId { get; set; }
        public int RuleGroupId { get; set; }
        public string RuleGroupName { get; set; }
        public int RuleGroupStatus { get; set; }
        public int RuleGroupVersion { get; set; }

        public string RuleSetAssemblyPath { get; set; }
        public string RuleSetCreatedBy { get; set; }
        public DateTime RuleSetCreatedDate { get; set; }
        public string RuleSetEntityClassType { get; set; }
        public string RuleSetName { get; set; }
        public int RuleSetRootRuleSetId { get; set; }
        public int RuleSetId { get; set; }
        public int? RuleSetStatus { get; set; }
        public int RuleSetVersion { get; set; }

        public string RuleName { get; set; }
        public string RuleActive { get; set; }
        public string RuleDescription { get; set; }
        public string RuleCondition { get; set; }
        public string RulePriority { get; set; }
        public string RuleReevaluationBehaviour { get; set; }
        public string RuleThenActions { get; set; }
        public string RuleElseActions { get; set; }

    }
}
