﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskEngine.Model
{
    public class ScoringException : Exception
    {
        public int ReturnCode { get; set; }

        public ScoringException(int code)
        {
            ReturnCode = code;
        }

        public ScoringException(string message, int code)
            : base(message)
        {
            ReturnCode = code;
        }

        public ScoringException(string message, int code, Exception inner)
            : base(message, inner)
        {
            ReturnCode = code;
        }
    }
}
