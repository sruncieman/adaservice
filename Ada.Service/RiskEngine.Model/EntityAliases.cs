﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskEngine.Model
{
    public class EntityAliases
    {
        public Boolean IncludeThis { get; set; }
        public Boolean IncludeConfirmed { get; set; }
        public Boolean IncludeUnconfirmed { get; set; }
        public Boolean IncludeTentative { get; set; }

        public string MatchType { get; set; }

        public ListOfInts ConfirmedAliases { get; set; }
        public ListOfInts UnconfirmedAliases { get; set; }
        public ListOfInts TentativeAliases { get; set; }

        public EntityAliases(string matchType, ListOfInts confirmedAliases, ListOfInts unconfirmedAliases, ListOfInts tentativeAliases)
        {
            IncludeConfirmed = false;
            IncludeUnconfirmed = false;
            IncludeTentative = false;
            IncludeThis = false;

            MatchType = matchType;

            string[] parts = matchType.Split('+');

            foreach (var part in parts)
            {
                string m = part.ToUpper();

                if (m == "CONFIRMED") IncludeConfirmed = true;
                if (m == "UNCONFIRMED") IncludeUnconfirmed = true;
                if (m == "TENTATIVE") IncludeTentative = true;
                if (m == "THIS") IncludeThis = true;
            }

            ConfirmedAliases = confirmedAliases;
            UnconfirmedAliases = unconfirmedAliases;
            TentativeAliases = tentativeAliases;
        }
    }
}
