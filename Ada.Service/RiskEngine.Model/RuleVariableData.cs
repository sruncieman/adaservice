﻿
using System;
using System.Collections.Generic;
using System.Linq;
using RiskEngine.DAL;

namespace RiskEngine.Model
{
    /// <summary>
    /// Business Object that maps to a RuleVariable entity
    /// </summary>
    public class RuleVariableData
    {
        public int RuleVariableId { get; set; }
        public string VariableName { get; set; }
        public string VariableValue { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public RuleVariableData()
        {
        }

        public RuleVariableData(RuleVariableEntity rve)
        {
            RuleVariableId = rve.RuleVariableId;
            CreatedBy      = rve.CreatedBy;
            CreatedDate    = rve.CreatedDate;
            ModifiedBy     = rve.ModifiedBy;
            ModifiedDate   = rve.ModifiedDate;
            VariableName   = rve.VariableName;
            VariableValue  = rve.VariableValue;
        }
    }

}

