﻿
using System;
using System.Globalization;
using System.Collections.Generic;
using System.IO;
using System.Workflow.Activities.Rules;
using System.Workflow.ComponentModel.Serialization;
using System.Xml;
using RiskEngine.DAL;

namespace RiskEngine.Model
{
    /// <summary>
    /// Business Object that maps to a RuleSet entity
    /// </summary>
    public class RuleSetData : IComparable<RuleSetData>
    {
        public int RuleSetId { get; set; }
        public int RootRuleSetId { get; set; }
        public string Name { get; set; }
        public string AssemblyPath { get; set; }
        public string EntityClassType { get; set; }
        public bool Dirty { get; set; }
        //public Type Activity { get; set; }

        public int Version { get; set; }
        private RuleSet ruleSet { get; set; }
        public string RuleSetXML { get; set; }
        public int? Status { get; set; }
        //public DateTime? ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        //public string ModifiedBy { get; set; }

        //public bool IsDefaultRuleSet { get; set; }   //used in dialog

        private WorkflowMarkupSerializer serializer = new WorkflowMarkupSerializer();

        public RuleSetData()
        {
        }

        public RuleSetData(RuleSetEntity rse)
        {
            RuleSetId       = rse.RuleSetId;
            RootRuleSetId   = rse.RootRuleSetId;
            AssemblyPath    = rse.AssemblyPath;
            CreatedBy       = rse.CreatedBy;
            CreatedDate     = rse.CreatedDate;
            EntityClassType = rse.EntityClassType;
            Version         = rse.Version;
            //ModifiedBy    = rse.ModifiedBy;
            //ModifiedDate  = rse.ModifiedDate;
            Name            = rse.Name;
            RuleSetXML      = rse.RuleSetXML;
            Status          = rse.Status;

            Dirty = false;
        }

        public RuleSet RuleSet
        {
            get
            {
                if (ruleSet == null)
                {
                    ruleSet = this.DeserializeRuleSet(RuleSetXML);
                }
                return ruleSet;
            }
            set
            {
                ruleSet = value;
            }
        }

        private RuleSet DeserializeRuleSet(string ruleSetXmlDefinition)
        {
            if (!String.IsNullOrEmpty(ruleSetXmlDefinition))
            {
                StringReader stringReader = new StringReader(ruleSetXmlDefinition);
                XmlTextReader reader = new XmlTextReader(stringReader);
                return serializer.Deserialize(reader) as RuleSet;
            }
            else
            {
                return null;
            }
        }

        public RuleSetData Clone()
        {
            RuleSetData newData = new RuleSetData();

            newData.RuleSetId      = RuleSetId;
            newData.RootRuleSetId  = RootRuleSetId;
            newData.Version        = Version;
            newData.RuleSetXML     = RuleSetXML;
            newData.RuleSet        = RuleSet.Clone();
            newData.Status         = Status;
            //newData.ModifiedDate = ModifiedDate;
            newData.CreatedDate    = CreatedDate;

            return newData;
        }

        #region IComparable<RuleSetData> Members

        public int CompareTo(RuleSetData other)
        {
            if (other != null)
            {
                int nameComparison = String.CompareOrdinal(this.Name, other.Name);
                if (nameComparison != 0)
                    return nameComparison;

                int majorVersionComparison = this.Version - other.Version;
                if (majorVersionComparison != 0)
                    return majorVersionComparison;

                //int minorVersionComparison = this.MinorVersion - other.MinorVersion;
                //if (minorVersionComparison != 0)
                //    return minorVersionComparison;

                return 0;
            }
            else
            {
                return 1;
            }
        }

        #endregion
    }

    public class RuleSetDataExport : IComparable<RuleSetData>
    {
        public int RuleSetId { get; set; }
        public int RootRuleSetId { get; set; }
        public string Name { get; set; }
        public string AssemblyPath { get; set; }
        public string EntityClassType { get; set; }
        public bool Dirty { get; set; }
        public Type Activity { get; set; }

        public int Version { get; set; }
        //private RuleSet ruleSet { get; set; }
        public string RuleSetXML { get; set; }
        public int? Status { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        //public bool IsDefaultRuleSet { get; set; }   //used in dialog

        private WorkflowMarkupSerializer serializer = new WorkflowMarkupSerializer();

        public RuleSetDataExport()
        {
        }

        public RuleSetDataExport(RuleSetEntity rse)
        {
            RuleSetId = rse.RuleSetId;
            RootRuleSetId = rse.RootRuleSetId;
            AssemblyPath = rse.AssemblyPath;
            CreatedBy = rse.CreatedBy;
            CreatedDate = rse.CreatedDate;
            EntityClassType = rse.EntityClassType;
            Version = rse.Version;
            //ModifiedBy = rse.ModifiedBy;
            //ModifiedDate = rse.ModifiedDate;
            Name = rse.Name;
            RuleSetXML = rse.RuleSetXML;
            Status = rse.Status;

            Dirty = false;
        }

        //public RuleSet RuleSet
        //{
        //    get
        //    {
        //        if (ruleSet == null)
        //        {
        //            ruleSet = this.DeserializeRuleSet(RuleSetXML);
        //        }
        //        return ruleSet;
        //    }
        //    set
        //    {
        //        ruleSet = value;
        //    }
        //}

        private RuleSet DeserializeRuleSet(string ruleSetXmlDefinition)
        {
            if (!String.IsNullOrEmpty(ruleSetXmlDefinition))
            {
                StringReader stringReader = new StringReader(ruleSetXmlDefinition);
                XmlTextReader reader = new XmlTextReader(stringReader);
                return serializer.Deserialize(reader) as RuleSet;
            }
            else
            {
                return null;
            }
        }

        public RuleSetData Clone()
        {
            RuleSetData newData = new RuleSetData();

            newData.RuleSetId = RuleSetId;
            newData.RootRuleSetId = RootRuleSetId;
            newData.Version = Version;
            newData.RuleSetXML = RuleSetXML;
            //newData.RuleSet = RuleSet.Clone();
            newData.Status = Status;
            //newData.ModifiedDate = ModifiedDate;
            newData.CreatedDate = CreatedDate;

            return newData;
        }

        #region IComparable<RuleSetData> Members

        public int CompareTo(RuleSetData other)
        {
            if (other != null)
            {
                int nameComparison = String.CompareOrdinal(this.Name, other.Name);
                if (nameComparison != 0)
                    return nameComparison;

                int majorVersionComparison = this.Version - other.Version;
                if (majorVersionComparison != 0)
                    return majorVersionComparison;

                //int minorVersionComparison = this.MinorVersion - other.MinorVersion;
                //if (minorVersionComparison != 0)
                //    return minorVersionComparison;

                return 0;
            }
            else
            {
                return 1;
            }
        }

        #endregion
    }
}
