﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace RiskEngine.Model
{
    [DataContract]
    public class MethodResultRecord
    {
        [DataMember]
        public object Result { get; set; }

        public MessageCache MessageCache { get; set; }
    }

    public class MessageCacheEntry
    {
        public string ThenLowLevelMessageIn { get; set; }
        public string ThenMediumLevelMessageIn { get; set; }
        public string ThenHighLevelMessageIn { get; set; }

        public string ElseLowLevelMessageIn { get; set; }
        public string ElseMediumLevelMessageIn { get; set; }
        public string ElseHighLevelMessageIn { get; set; }

        //public List<object> FixedEntityInputValues { get; set; }

        public ListOfMsgs ThenLowLevelMessagesOut { get; set; }
        public ListOfMsgs ThenMediumLevelMessagesOut { get; set; }
        public ListOfMsgs ThenHighLevelMessagesOut { get; set; }

        public ListOfMsgs ElseLowLevelMessagesOut { get; set; }
        public ListOfMsgs ElseMediumLevelMessagesOut { get; set; }
        public ListOfMsgs ElseHighLevelMessagesOut { get; set; }

        public ListOfListOfStringData ThenLowLevelData { get; set; }
        public ListOfListOfStringData ThenMediumLevelData { get; set; }
        public ListOfListOfStringData ThenHighLevelData { get; set; }

        public ListOfListOfStringData ElseLowLevelData { get; set; }
        public ListOfListOfStringData ElseMediumLevelData { get; set; }
        public ListOfListOfStringData ElseHighLevelData { get; set; }

        public MessageCacheEntry()
        {
            ThenLowLevelMessagesOut    = new ListOfMsgs();
            ThenMediumLevelMessagesOut = new ListOfMsgs();
            ThenHighLevelMessagesOut   = new ListOfMsgs();

            ElseLowLevelMessagesOut    = new ListOfMsgs();
            ElseMediumLevelMessagesOut = new ListOfMsgs();
            ElseHighLevelMessagesOut    = new ListOfMsgs();

            //FixedEntityInputValues = new List<object>();

            ThenLowLevelData    = new ListOfListOfStringData();
            ThenMediumLevelData = new ListOfListOfStringData();
            ThenHighLevelData   = new ListOfListOfStringData();

            ElseLowLevelData    = new ListOfListOfStringData();
            ElseMediumLevelData = new ListOfListOfStringData();
            ElseHighLevelData   = new ListOfListOfStringData();

        }
    }

    public class MessageCache
    {
        private Dictionary<string, MessageCacheEntry> MessageLines { get; set; }

        public MessageCache()
        {
            MessageLines = new Dictionary<string, MessageCacheEntry>();
        }

        //public object[] GetFixedEntityInputValues( string key )
        //{
        //    if (MessageLines.ContainsKey(key))
        //    {
        //        MessageCacheEntry x = MessageLines[key];

        //        return x.FixedEntityInputValues.ToArray();
        //    }

        //    return null;
        //}

        //public void AddFixedEntityInputValues( string key, object v )
        //{
        //    MessageCacheEntry x;

        //    if (MessageLines.ContainsKey(key))
        //    {
        //        x = MessageLines[key];

        //        x.FixedEntityInputValues.Add(v);
        //    }
        //    else
        //    {
        //        x = new MessageCacheEntry();

        //        x.FixedEntityInputValues.Add(v);

        //        MessageLines.Add(key, x);
        //    }
        //}

        public void AddMessageCacheEntry(string key, MessageCacheEntry cacheEntry)
        {
            if (MessageLines.ContainsKey(key))
                MessageLines.Remove(key);

            MessageLines.Add(key, cacheEntry);
        }

        public void AddLowOutputData(string key, ListOfStringData thenData, ListOfStringData elseData)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];

                if (thenData != null && thenData.Count > 0) x.ThenLowLevelData.Add(thenData);
                if (elseData != null && elseData.Count > 0) x.ElseLowLevelData.Add(elseData);
            }
            else
            {
                x = new MessageCacheEntry();

                if (thenData != null && thenData.Count > 0) x.ThenLowLevelData.Add(thenData);
                if (elseData != null && elseData.Count > 0) x.ElseLowLevelData.Add(elseData);

                MessageLines.Add(key, x);
            }
        }

        public void AddMediumOutputData(string key, ListOfStringData thenData, ListOfStringData elseData)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];

                if (thenData != null && thenData.Count > 0) x.ThenMediumLevelData.Add(thenData);
                if (elseData != null && elseData.Count > 0) x.ElseMediumLevelData.Add(elseData);
            }
            else
            {
                x = new MessageCacheEntry();

                if (thenData != null && thenData.Count > 0) x.ThenMediumLevelData.Add(thenData);
                if (elseData != null && elseData.Count > 0) x.ElseMediumLevelData.Add(elseData);

                MessageLines.Add(key, x);
            }
        }

        public void AddHighOutputData(string key, ListOfStringData thenData, ListOfStringData elseData)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];

                if (thenData != null && thenData.Count > 0) x.ThenHighLevelData.Add(thenData);
                if (elseData != null && elseData.Count > 0) x.ElseHighLevelData.Add(elseData);
            }
            else
            {
                x = new MessageCacheEntry();

                if (thenData != null && thenData.Count > 0) x.ThenHighLevelData.Add(thenData);
                if (elseData != null && elseData.Count > 0) x.ElseHighLevelData.Add(elseData);

                MessageLines.Add(key, x);
            }
        }

        public void AddLowOutputMessage(string key, string thenMsg, string elseMsg)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];
                if (!string.IsNullOrEmpty(thenMsg)) x.ThenLowLevelMessagesOut.Add(thenMsg);
                if (!string.IsNullOrEmpty(elseMsg)) x.ElseLowLevelMessagesOut.Add(elseMsg);
            }
            else
            {
                x = new MessageCacheEntry();

                if (!string.IsNullOrEmpty(thenMsg)) x.ThenLowLevelMessagesOut.Add(thenMsg);
                if (!string.IsNullOrEmpty(elseMsg)) x.ElseLowLevelMessagesOut.Add(elseMsg);

                MessageLines.Add(key, x);
            }
        }

        public void AddMediumOutputMessage(string key, string thenMsg, string elseMsg)
        {
            MessageCacheEntry x;


            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];
                if (!string.IsNullOrEmpty(thenMsg)) x.ThenMediumLevelMessagesOut.Add(thenMsg.Replace(" .", "."));
                if (!string.IsNullOrEmpty(elseMsg)) x.ElseMediumLevelMessagesOut.Add(elseMsg.Replace(" .", "."));
            }
            else
            {
                x = new MessageCacheEntry();

                if (!string.IsNullOrEmpty(thenMsg)) x.ThenMediumLevelMessagesOut.Add(thenMsg.Replace(" .", "."));
                if (!string.IsNullOrEmpty(elseMsg)) x.ElseMediumLevelMessagesOut.Add(elseMsg.Replace(" .", "."));

                MessageLines.Add(key, x);
            }
        }

        public void AddHighOutputMessage(string key, string thenMsg, string elseMsg)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];
                if (!string.IsNullOrEmpty(thenMsg)) x.ThenHighLevelMessagesOut.Add(thenMsg);
                if (!string.IsNullOrEmpty(elseMsg)) x.ElseHighLevelMessagesOut.Add(elseMsg);
            }
            else
            {
                x = new MessageCacheEntry();

                if (!string.IsNullOrEmpty(thenMsg)) x.ThenHighLevelMessagesOut.Add(thenMsg);
                if (!string.IsNullOrEmpty(elseMsg)) x.ElseHighLevelMessagesOut.Add(elseMsg);

                MessageLines.Add(key, x);
            }
        }

        #region Message IN methods
        public void SetLowThenInMessage(string key, string v)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];

                x.ThenLowLevelMessageIn = v;
            }
            else
            {
                x = new MessageCacheEntry();

                x.ThenLowLevelMessageIn = v;

                MessageLines.Add(key, x);
            }
        }

        public void SetMediumThenInMessage(string key, string v)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];

                x.ThenMediumLevelMessageIn = v;
            }
            else
            {
                x = new MessageCacheEntry();

                x.ThenMediumLevelMessageIn = v;

                MessageLines.Add(key, x);
            }
        }

        public void SetHighThenInMessage(string key, string v)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];

                x.ThenHighLevelMessageIn = v;
            }
            else
            {
                x = new MessageCacheEntry();

                x.ThenHighLevelMessageIn = v;

                MessageLines.Add(key, x);
            }
        }

        public void SetLowElseInMessage(string key, string v)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];

                x.ElseLowLevelMessageIn = v;
            }
            else
            {
                x = new MessageCacheEntry();

                x.ElseLowLevelMessageIn = v;

                MessageLines.Add(key, x);
            }
        }

        public void SetMediumElseInMessage(string key, string v)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];

                x.ElseMediumLevelMessageIn = v;
            }
            else
            {
                x = new MessageCacheEntry();

                x.ElseMediumLevelMessageIn = v;

                MessageLines.Add(key, x);
            }
        }

        public void SetHighElseInMessage(string key, string v)
        {
            MessageCacheEntry x;

            if (MessageLines.ContainsKey(key))
            {
                x = MessageLines[key];

                x.ElseHighLevelMessageIn = v;
            }
            else
            {
                x = new MessageCacheEntry();

                x.ElseHighLevelMessageIn = v;

                MessageLines.Add(key, x);
            }
        }

        public string GetLowThenInMessage(string key)
        {
            if (!MessageLines.ContainsKey(key) || string.IsNullOrEmpty(MessageLines[key].ThenLowLevelMessageIn))
                return string.Empty;

            return MessageLines[key].ThenLowLevelMessageIn;
        }
        public string GetMediumThenInMessage(string key)
        {
            if (!MessageLines.ContainsKey(key) || string.IsNullOrEmpty(MessageLines[key].ThenMediumLevelMessageIn))
                return string.Empty;

            return MessageLines[key].ThenMediumLevelMessageIn;
        }
        public string GetHighThenInMessage(string key)
        {
            if (!MessageLines.ContainsKey(key) || string.IsNullOrEmpty(MessageLines[key].ThenHighLevelMessageIn))
                return string.Empty;

            return MessageLines[key].ThenHighLevelMessageIn;
        }

        public string GetLowElseInMessage(string key)
        {
            if (!MessageLines.ContainsKey(key) || string.IsNullOrEmpty(MessageLines[key].ElseLowLevelMessageIn))
                return string.Empty;

            return MessageLines[key].ElseLowLevelMessageIn;
        }
        public string GetMediumElseInMessage(string key)
        {
            if (!MessageLines.ContainsKey(key) || string.IsNullOrEmpty(MessageLines[key].ElseMediumLevelMessageIn))
                return string.Empty;

            return MessageLines[key].ElseMediumLevelMessageIn;
        }
        public string GetHighElseInMessage(string key)
        {
            if (!MessageLines.ContainsKey(key) || string.IsNullOrEmpty(MessageLines[key].ElseHighLevelMessageIn))
                return string.Empty;

            return MessageLines[key].ElseHighLevelMessageIn;
        }
        #endregion

        #region Message OUT methods
        public ListOfMsgs GetLowThenMessages(string key)
        {
            if (!MessageLines.ContainsKey(key) ) //|| string.IsNullOrEmpty(MessageLines[key].ThenLowLevelMessage))
                return new ListOfMsgs();

            return MessageLines[key].ThenLowLevelMessagesOut;
        }
        public ListOfMsgs GetMediumThenMessages(string key)
        {
            if (!MessageLines.ContainsKey(key) )//|| string.IsNullOrEmpty(MessageLines[key].ThenMediumLevelMessages))
                return new ListOfMsgs();

            return MessageLines[key].ThenMediumLevelMessagesOut;
        }
        public ListOfMsgs GetHighThenMessages(string key)
        {
            if (!MessageLines.ContainsKey(key) )//|| string.IsNullOrEmpty(MessageLines[key].ThenHighLevelMessages))
                return new ListOfMsgs();

            return MessageLines[key].ThenHighLevelMessagesOut;
        }

        public ListOfMsgs GetLowElseMessages(string key)
        {
            if (!MessageLines.ContainsKey(key) )//|| string.IsNullOrEmpty(MessageLines[key].ElseLowLevelMessages))
                return new ListOfMsgs();

            return MessageLines[key].ElseLowLevelMessagesOut;
        }
        public ListOfMsgs GetMediumElseMessages(string key)
        {
            if (!MessageLines.ContainsKey(key) )//|| string.IsNullOrEmpty(MessageLines[key].ElseMediumLevelMessages))
                return new ListOfMsgs();

            return MessageLines[key].ElseMediumLevelMessagesOut;
        }
        public ListOfMsgs GetHighElseMessages(string key)
        {
            if (!MessageLines.ContainsKey(key) )//|| string.IsNullOrEmpty(MessageLines[key].ElseHighLevelMessages))
               return new ListOfMsgs();

            return MessageLines[key].ElseHighLevelMessagesOut;
        }
        #endregion

        #region DATA Methods
        public ListOfListOfStringData GetThenLowDataFromCache(string key)
        {
            if (!MessageLines.ContainsKey(key) || MessageLines[key].ThenLowLevelData.Count == 0)
                return new ListOfListOfStringData();

            return MessageLines[key].ThenLowLevelData;
        }
        public ListOfListOfStringData GetThenMediumDataFromCache(string key)
        {
            if (!MessageLines.ContainsKey(key) || MessageLines[key].ThenMediumLevelData.Count == 0)
                return new ListOfListOfStringData(); ;

            return MessageLines[key].ThenMediumLevelData;
        }
        public ListOfListOfStringData GetThenHighDataFromCache(string key)
        {
            if (!MessageLines.ContainsKey(key) || MessageLines[key].ThenHighLevelData.Count == 0)
                return new ListOfListOfStringData(); ;

            return MessageLines[key].ThenHighLevelData;
        }
        public ListOfListOfStringData GetElseLowDataFromCache(string key)
        {
            if (!MessageLines.ContainsKey(key) || MessageLines[key].ElseLowLevelData.Count == 0)
                return new ListOfListOfStringData(); ;

            return MessageLines[key].ElseLowLevelData;
        }
        public ListOfListOfStringData GetElseMediumDataFromCache(string key)
        {
            if (!MessageLines.ContainsKey(key) || MessageLines[key].ElseMediumLevelData.Count == 0)
                return new ListOfListOfStringData(); ;

            return MessageLines[key].ElseMediumLevelData;
        }
        public ListOfListOfStringData GetElseHighDataFromCache(string key)
        {
            if (!MessageLines.ContainsKey(key) || MessageLines[key].ElseHighLevelData.Count == 0)
                return new ListOfListOfStringData(); ;

            return MessageLines[key].ElseHighLevelData;
        }
        #endregion
    }
}
