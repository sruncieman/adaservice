﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.Serialization;
using ADAServices.Interface;
using MDA.MappingService.Liberty.Motor.Model;

namespace MDA.Mapping.LibertyBatchUtility
{
    class Program
    {


         //[STAThread]
        static void Main(string[] args)
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());



            
            List<IncidentXsdIncident> incidents = new List<IncidentXsdIncident>();

            //string path = @"C:\Temp\Liberty XML Files Batch 2\JonsSampleBatch\";
            //string path = @"C:\Temp\Liberty XML Files Batch 2\Batch 2\";
            string path = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Liberty\ClaimToBatch";

            foreach (var file in Directory.GetFiles(path, "LSM*", SearchOption.AllDirectories))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(IncidentXsd));
                StreamReader reader = new StreamReader(file);

                var i = (IncidentXsd)serializer.Deserialize(reader);

                incidents.Add(i.Incident);

                reader.Close();
            }


            int totalCount = incidents.Count();

            var countOpen = incidents.Count(x => x.Status.ToString() == "Open");
            var countClosed = incidents.Count(x => x.Status.ToString() == "Closed");


            var minIncidentDt = incidents.Select(x => x.dtIncident).Min();
            var maxIncidentDt = incidents.Select(x => x.dtIncident).Max();

            int monthDiff = MonthDifference(Convert.ToDateTime(maxIncidentDt), Convert.ToDateTime(minIncidentDt));

            //var batchAmount = totalCount/monthDiff;
            var batchAmount = 100000000;
           
            int counter = 0;

            string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Liberty\Xml\LibertyMergedFile.xml";

            for (int i = 0; i < totalCount; i += batchAmount)
            {
                var batch = incidents.Skip(i).Take(batchAmount).ToList();

                counter++;

                var ms = new MemoryStream();

                var ser = new XmlSerializer(typeof(List<IncidentXsdIncident>));

                ser.Serialize(ms, batch);

                ms.Position = 0;

                WriteToXmlFile(ms, strXmlFileFullSave, ser, batch);

                // Load directly into db
                //ADAServices.Factory.ADAServicesFactory.CreateADAServices().UploadBatchOfClaimsFile(ms, "Batch" + counter, "xml", 5, 17, "Batch" + counter, "BatchUtility");
                                                                                     
            }
        }

        private static void WriteToXmlFile(MemoryStream ms, string strXmlFileFullSave, XmlSerializer ser, List<IncidentXsdIncident> batch)
        {
            var sr = new StreamReader(ms);

            TextWriter WriteFileStream = new StreamWriter(strXmlFileFullSave);
            ser.Serialize(WriteFileStream, batch);

            WriteFileStream.Close();
        }


        public static int MonthDifference(DateTime lValue, DateTime rValue)
        {
            return (lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year);
        }
    }
}

