﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.KeoghsCH.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class Organisation
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int MatterNum;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int OrganisationId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? PersonId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string OrgInvolvement;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CompanyName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Telephone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Postcode;
    }

}
