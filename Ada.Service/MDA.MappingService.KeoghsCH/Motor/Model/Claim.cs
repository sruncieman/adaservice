﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.KeoghsCH.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class Claim
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int MatterNum;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insurer;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime ?IncidentDt;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimStatus;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsurerClaimRef;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IncidentCircumstances;
    }
}
