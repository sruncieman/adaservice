﻿CREATE FUNCTION [dbo].[fnGetMatchingClaimTSql_20140512] 
(
	@Id INT,
	@UseRiskClaimId BIT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)

DECLARE @From NVARCHAR(1000) ='
FROM dbo.Incident AS INC WITH(NOLOCK)'

DECLARE @JoinRiskClaim NVARCHAR(1000) ='
LEFT JOIN dbo.KeoghsCase2Incident _KC21 WITH(NOLOCK) ON _KC21.Incident_ID = INC.ID AND _KC21.ADARecordStatus = 0 
LEFT JOIN dbo.Incident2Person _I2P WITH(NOLOCK) ON _I2P.Incident_ID = INC.ID AND _I2P.ADARecordStatus = 0 
LEFT JOIN Incident2Vehicle _I2V WITH(NOLOCK) ON _I2V.Incident_ID = INC.ID AND _I2V.ADARecordStatus = 0 
INNER JOIN @XML.nodes(''//Claim'') XML(Data) ON COALESCE(_KC21.BaseRiskClaim_Id, _I2P.BaseRiskClaim_Id, _I2V.BaseRiskClaim_Id) = CONVERT(varchar(50), XML.Data.query(''data(RiskClaim_ID)''))'

DECLARE @JoinClaimRef NVARCHAR(1000) ='
INNER JOIN dbo.KeoghsCase2Incident AS KC2I WITH(NOLOCK) ON KC2I.Incident_Id = INC.ID AND KC2I.ADARecordStatus = 0 
INNER JOIN dbo.KeoghsCase KC WITH(NOLOCK) ON KC.ID = KC2I.KeoghsCase_Id AND KC.ADARecordStatus = 0 
INNER JOIN @XML.nodes(''//Claim'') XKC(Data)'

DECLARE @JoinPerson	NVARCHAR(1000) ='
INNER JOIN dbo.Incident2Person I2P WITH(NOLOCK) ON I2P.Incident_Id = INC.ID AND I2P.ADARecordStatus = 0 AND I2P.LinkConfidence = 0 '

DECLARE @JoinPersonXML	NVARCHAR(1000) ='
INNER JOIN @XML.nodes(''//Claim'') XI2P(Data)'

DECLARE @JoinIncidentDate NVARCHAR(1000) ='
INNER JOIN @XML.nodes(''//Claim'') XINC(Data)'

DECLARE @JoinVehicle NVARCHAR(1000) ='
INNER JOIN Incident2Vehicle I2V WITH(NOLOCK) ON I2V.Incident_Id = INC.Id AND I2V.ADARecordStatus = 0 AND I2V.LinkConfidence = 0 '

DECLARE @JoinVehicleAndPeople NVARCHAR(1000) ='
--INNER JOIN dbo.Vehicle2Person V2P WITH(NOLOCK) ON V2P.Vehicle_Id = I2V.Vehicle_Id AND V2P.Person_Id = I2P.Person_Id AND V2P.ADARecordStatus = 0'

DECLARE @WherePeopleIn NVARCHAR(1000) ='
AND I2P.Person_Id IN (SELECT PerID FROM #PerAll PA WHERE PA.PerID = I2P.Person_Id) '

DECLARE @WhereVehicleIn NVARCHAR(1000) ='
AND I2V.Vehicle_Id IN (SELECT VecID FROM #VecAll VA WHERE VA.VecID = I2V.Vehicle_Id) '

DECLARE @HavingPeopleCount NVARCHAR(1000) ='
AND COUNT(DISTINCT I2P.Person_Id) >='

DECLARE @HavingVehicleCount NVARCHAR(1000) ='
AND COUNT(DISTINCT I2V.Vehicle_Id) >='

SELECT @SQL = 
		'SELECT DISTINCT INC.Id, '
		+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
		+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
		+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType'
		+ @From 
		+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.ClientClaimReference ), 0) + ISNULL(CONVERT(INT,MRC.ClientID ), 0) > 0 THEN 
		+ @JoinClaimRef 
			+ dbo.fnFormatOnClause(
			CASE WHEN ClientClaimReference = 1 THEN 'AND KC.ClientReference = CONVERT(varchar(50), XKC.Data.query(''data(ClientClaimReference)'')) ' ELSE '' END
			+ CASE WHEN ClientID = 1 THEN 'AND KC.Client_Id = CONVERT(varchar(50), XKC.Data.query(''data(ClientID)'')) ' ELSE '' END
			)	
			ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.IncidentTime), 0) + ISNULL(CONVERT(INT,MRC.IncidentLocation), 0) + ISNULL(CONVERT(INT,MRC.PeopleMatch), 0)> 0 THEN 
		+ @JoinPerson ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.IncidentTime), 0) + ISNULL(CONVERT(INT,MRC.IncidentLocation), 0) > 0 THEN 
		+ @JoinPersonXML
		+ dbo.fnFormatOnClause(
			CASE WHEN IncidentTime = 1 THEN 'AND I2P.IncidentTime  = CONVERT(varchar(50), XI2P.Data.query(''data(IncidentTime)'')) ' ELSE '' END
			+ CASE WHEN IncidentLocation = 1 THEN 'AND I2P.IncidentLocation = CONVERT(varchar(50), XI2P.Data.query(''data(IncidentLocation)'')) ' ELSE '' END
			)	
			ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.IncidentDate), 0) > 0 THEN
		+  @JoinIncidentDate 
		+ dbo.fnFormatOnClause(
			CASE WHEN IncidentDate = 1 THEN 'AND INC.IncidentDate = CONVERT(varchar(50), XINC.Data.query(''data(IncidentDate)'')) ' ELSE '' END
			)	
			ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN 
		+ @JoinVehicle ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,CONVERT(BIT,MRC.VehicleMatch)), 0)+ ISNULL(CONVERT(INT,CONVERT(BIT,MRC.PeopleMatch)), 0) > 1 THEN 
		+ @JoinVehicleAndPeople ELSE '' END 
		+ dbo.fnFormatAddCarriageReturn('WHERE INC.ADARecordStatus = 0 ') 
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) + ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN

			CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 THEN @WherePeopleIn ELSE '' END 
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN @WhereVehicleIn ELSE '' END
			
		ELSE '' END
		+ dbo.fnFormatAddCarriageReturn('GROUP BY INC.Id')
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) + ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN
		+ dbo.fnFormatAddCarriageReturn('HAVING') 
		+ dbo.fnFormatWhereClause(
			CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 THEN @HavingPeopleCount + CAST(MRC.PeopleMatch AS VARCHAR(5)) + ' ' ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN @HavingVehicleCount + CAST(MRC.VehicleMatch AS VARCHAR(5)) + ' ' ELSE '' END
			)
		ELSE '' END
		FROM dbo.MatchingRulesClaim MRC WITH(NOLOCK)   
		WHERE RuleNo = @Id
		
RETURN @SQL

END