﻿



CREATE FUNCTION [dbo].[fnGetMatchingVehicleToProcess]
(	
	@XML XML
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 
		MRV.*, HasRiskClaim_Id
	FROM  dbo.MatchingRulesVehicles MRV WITH(NOLOCK) 
	INNER JOIN(
		SELECT  
			CONVERT(BIT, MAX(LEN(XVEH.Data.value('@RiskClaim_Id', 'VARCHAR(50)')))) HasRiskClaim_Id,
			MAX(LEN(XVEH.Data.value('@VehicleMake', 'VARCHAR(50)'))) VehicleMake,
			MAX(LEN(XVEH.Data.value('@Model', 'VARCHAR(50)'))) Model,
			MAX(LEN(XVEH.Data.value('@VehicleRegistration', 'VARCHAR(50)'))) VehicleRegistration,
			MAX(LEN(XVEH.Data.value('@Colour', 'VARCHAR(50)'))) Colour,
			MAX(LEN(XVEH.Data.value('@VIN', 'VARCHAR(50)'))) VIN,
			MAX(LEN(XAD.Data.value('@BuildingNumber', 'VARCHAR(50)'))) BuildingNumber,
			MAX(LEN(XAD.Data.value('@PostCode', 'VARCHAR(50)'))) PostCode,
			MAX(LEN(XAD.Data.value('@PafUPRN', 'VARCHAR(50)'))) PafUPRN

		FROM
			@XML.nodes('//Data/Vehicle ') XVEH(Data) 	
		INNER JOIN
			@XML.nodes('//Data/Vehicle/Addresses/Address') XAD(Data) 	
			ON  1 = 1
		) _XML

	ON
		ISNULL(MRV.VehicleMake,  0) <= _XML.VehicleMake
		AND ISNULL(MRV.Model,  0) <= _XML.Model
		AND ISNULL(MRV.VehicleRegistration,  0) <= _XML.VehicleRegistration
		AND ISNULL(MRV.Colour,  0) <= _XML.Colour
		AND ISNULL(MRV.VIN,  0) <= _XML.VIN
		AND ISNULL(MRV.BuildingNumber,  0) <= _XML.BuildingNumber
		AND ISNULL(MRV.PostCode,  0) <= _XML.PostCode
		AND ISNULL(MRV.PafUPRN,  0) <= _XML.PafUPRN
)