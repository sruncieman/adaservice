﻿CREATE FUNCTION [dbo].[fnGetXmlMediumDataOccupation]
(
	@RiskClaim_Id INT,
	@Person_Id INT
)
RETURNS  VARCHAR(500)
AS
BEGIN
	

	DECLARE @Val VARCHAR(500) = ''
	DECLARE @Sep CHAR(2) = '; '
	DECLARE @Len INT = 0
	
	SELECT 	@Val += CASE WHEN MD.DataMed7 IS NULL THEN '' ELSE  REPLACE(MD.DataMed7,',',';') END + @Sep--+ CASE WHEN MD.DataMed6 = 'True'  THEN ' [C]' ELSE ' [U]' END + @Sep  END 
	FROM dbo.rpt_Level_One_Report_DataMed AS MD
	WHERE md.RiskClaim_Id = @RiskClaim_Id
    AND MD.Per_dbId = @Person_Id
	AND MD.localName = 'PersonRisk' 
	AND MD.Rule_Key IN ('Person_AliasInformation_Result')
	ORDER BY MD.Rule_Key 
	
	/*
	SELECT 	@Val += CASE WHEN MD.DataMed5 IS NULL THEN '' ELSE  MD.DataMed5  END + @Sep 
	FROM 
    dbo.fnGetXmlMediumData(@RiskClaim_Id) AS MD
	WHERE 
    MD.Per_Id = @Person_Id 
    AND MD.localName = 'PersonRisk' 
    AND MD.Key_ IN('AliasInfo')
	*/
	

	IF LEN(@Val) > 0 SET @Len = 1
	
	IF @Val IS NOT NULL
		SET @Val = NULLIF(LEFT(@Val, LEN(@Val) - @Len), '')
	RETURN @Val

END