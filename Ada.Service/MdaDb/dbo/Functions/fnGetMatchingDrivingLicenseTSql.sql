﻿


CREATE FUNCTION [dbo].[fnGetMatchingDrivingLicenseTSql] 
(
	@Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)
DECLARE @RiskClaim_Id BIT = (SELECT RiskClaim_Id FROM dbo.MatchingRulesDrivingLicense WHERE RuleNo = @Id)

IF (@RiskClaim_Id IS NULL)
	SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  DrivingLicense AS A 
	WHERE (DriverNumber = @DriverNumber)'
FROM dbo.MatchingRulesDrivingLicense WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	  
	FROM  DrivingLicense AS A 
	INNER JOIN Person2DrivingLicense AS P2DL ON A.Id = P2DL.DrivingLicense_Id AND P2DL.AdaRecordStatus = 0
	WHERE  ' 
	+ dbo.fnFormatWhereClause(CASE WHEN DriverNumber = 1 THEN ' AND DriverNumber = @DriverNumber' ELSE '' END + CASE WHEN RiskClaim_Id = 1 THEN ' AND BaseRiskClaim_Id = @RiskClaim_Id' ELSE '' END )

FROM dbo.MatchingRulesDrivingLicense  WHERE RuleNo = @Id

RETURN @SQL

END