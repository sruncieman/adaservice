﻿

CREATE FUNCTION [dbo].[fnGetMatchingAddressTSql_20140205] 
(
	@Id INT,
	@UseRiskClaimId BIT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN



DECLARE @SQL NVARCHAR(MAX)


IF (@UseRiskClaimId = 0)
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  Address AS A  WITH(NOLOCK)
	WHERE '
		+ dbo.fnFormatWhereClause(
		CASE WHEN SubBuilding = 1 THEN ' AND SubBuilding = @SubBuilding' ELSE '' END 
		+ CASE WHEN Building = 1 THEN ' AND Building = @Building' ELSE '' END 
		+ CASE WHEN BuildingNumber = 1 THEN ' AND BuildingNumber = @BuildingNumber' ELSE '' END 
		+ CASE WHEN Street = 1 THEN ' AND Street = @Street' ELSE '' END 
		+ CASE WHEN Locality = 1 THEN ' AND Locality = @Locality' ELSE '' END 
		+ CASE WHEN Town = 1 THEN ' AND Town = @Town' ELSE '' END 
		+ CASE WHEN PostCode = 1 THEN ' AND PostCode = REPLACE(@PostCode, '' '', '''')' ELSE '' END 
		+ ' AND A.ADARecordStatus = 0 AND A.RecordStatus = 0'
	)
FROM dbo.MatchingRulesAddress WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  Address AS A  WITH(NOLOCK)
	INNER JOIN
	(
	SELECT BaseRiskClaim_Id, Address_Id  FROM Person2Address WHERE BaseRiskClaim_Id = @RiskClaim_Id
	UNION
	SELECT BaseRiskClaim_Id, Address_Id  FROM dbo.Organisation2Address WHERE BaseRiskClaim_Id = @RiskClaim_Id
	) AS U
	 ON U.Address_Id = A.Id
	
	WHERE  '
	+ dbo.fnFormatWhereClause(
		CASE WHEN SubBuilding = 1 THEN ' AND SubBuilding = @SubBuilding' ELSE '' END 
		+ CASE WHEN Building = 1 THEN ' AND Building = @Building' ELSE '' END 
		+ CASE WHEN BuildingNumber = 1 THEN ' AND BuildingNumber = @BuildingNumber' ELSE '' END 
		+ CASE WHEN Street = 1 THEN ' AND Street = @Street' ELSE '' END 
		+ CASE WHEN Locality = 1 THEN ' AND Locality = @Locality' ELSE '' END 
		+ CASE WHEN Town = 1 THEN ' AND Town = @Town' ELSE '' END 
		+ CASE WHEN PostCode = 1 THEN ' AND PostCode = REPLACE(@PostCode, '' '', '''')' ELSE '' END 

		
		
			 
	)

FROM dbo.MatchingRulesAddress WHERE RuleNo = @Id

RETURN @SQL

END