﻿
CREATE FUNCTION [dbo].[fnGetXmlMediumDataLinkedTelephones]
(
	@RiskClaim_Id INT,
	@Person_Id INT
)
RETURNS  VARCHAR(500)
AS
BEGIN
	

	DECLARE @Val VARCHAR(500) = ''
	DECLARE @Sep CHAR(2) = '; '
	DECLARE @Len INT = 0

	SELECT 	@Val += CASE WHEN MD.DataMed1 IS NULL THEN '' ELSE  MD.DataMed1  END + CASE WHEN MD.DataMed2 = 'True' THEN ' [C]' ELSE ' [U]' END + @Sep 
	FROM dbo.rpt_Level_One_Report_DataMed AS MD
	WHERE md.RiskClaim_Id = @RiskClaim_Id
    AND MD.Per_dbId = @Person_Id
	AND MD.localName = 'PersonRisk' 
	AND MD.Rule_Key IN ('Person_LinkedTelephones')
	ORDER BY MD.Rule_Key 

	
	IF LEN(@Val) > 0 SET @Len = 1
	
	IF @Val IS NOT NULL
		SET @Val = NULLIF(LEFT(@Val, LEN(@Val) - @Len), '')

	RETURN @Val

END