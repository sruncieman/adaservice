﻿
CREATE FUNCTION [dbo].[fn_Split] 
( 
@RowData nvarchar(MAX),
@SplitOn nvarchar(5)
) 
	RETURNS @ReturnValue TABLE 
	(Id INT, Data NVARCHAR(MAX)) 
	AS
	BEGIN
	Declare @Counter int
	Set @Counter = 1 
	While (Charindex(@SplitOn,@RowData)>0) 
	Begin 
	Insert Into @ReturnValue (id, data) 
	Select @Counter, Data = 
	ltrim(rtrim(Substring(@RowData,1,Charindex(@SplitOn,@RowData)-1)))
	Set @RowData = 
	Substring(@RowData,Charindex(@SplitOn,@RowData)+1,len(@RowData)) 
	Set @Counter = @Counter + 1 
	End 
	Insert Into @ReturnValue (id, data) 
	Select @Counter, Data = ltrim(rtrim(@RowData)) 
	Return 
END