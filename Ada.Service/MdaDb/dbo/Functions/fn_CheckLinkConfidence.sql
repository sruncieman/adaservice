﻿
CREATE FUNCTION [dbo].[fn_CheckLinkConfidence]
(
	 @PersonId INT,
	 @IncludeConfirmed BIT ,
	 @IncludeUnconfirmed BIT,
	 @IncludeTentative BIT
)
RETURNS INT
AS
BEGIN

	DECLARE @Result INT
	DECLARE @Include INT = CASE WHEN @IncludeConfirmed = 1 THEN 0 ELSE  CASE WHEN  @IncludeUnconfirmed = 1 THEN 1 ELSE CASE WHEN  @IncludeTentative = 1 THEN 2  END  END END
	
	IF NOT EXISTS (SELECT * FROM Person2Person WHERE (Person1_Id = @PersonId) OR (Person2_Id = @PersonId) )
		SET @Result = @PersonId
	ELSE
	BEGIN
		IF EXISTS (SELECT * FROM Person2Person WHERE (Person1_Id = @PersonId AND LinkConfidence >= @Include) OR (Person2_Id >= @PersonId AND LinkConfidence = @Include) )
			SET @Result = @PersonId
		ELSE
			SET @Result = -1
	
	END
	
	RETURN @Result

END