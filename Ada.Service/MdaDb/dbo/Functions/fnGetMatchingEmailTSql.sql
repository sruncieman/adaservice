﻿


CREATE FUNCTION [dbo].[fnGetMatchingEmailTSql] 
(
	@Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)
DECLARE @RiskClaim_Id BIT = (SELECT RiskClaim_Id FROM dbo.MatchingRulesEmail WHERE RuleNo = @Id)

IF (@RiskClaim_Id IS NULL)
	SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  dbo.Email AS A 
	WHERE (EmailAddress = @EmailAddress)'
FROM dbo.MatchingRulesEmail WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	  
	FROM  Email AS A 
	INNER JOIN Person2Email AS P2E ON A.Id = P2E.Email_Id AND P2E.AdaRecordStatus = 0
	WHERE  ' 
	+
	dbo.fnFormatWhereClause(
	CASE WHEN EmailAddress = 1 THEN ' AND EmailAddress = @EmailAddress' ELSE '' END + 
	CASE WHEN RiskClaim_Id = 1 THEN ' AND BaseRiskClaim_Id = @RiskClaim_Id' ELSE '' END )

FROM dbo.MatchingRulesEmail  WHERE RuleNo = @Id

RETURN @SQL

END