﻿
CREATE FUNCTION [dbo].[fnGetMatchingClaimTSql_20140616] 
(
	@Id INT,
	@UseRiskClaimId BIT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)

DECLARE @From NVARCHAR(1000) ='
FROM dbo.Incident AS INC WITH(NOLOCK)'

DECLARE @JoinRiskClaim NVARCHAR(1000) ='
LEFT JOIN dbo.KeoghsCase2Incident _KC21 WITH(NOLOCK) ON _KC21.Incident_ID = INC.ID AND _KC21.ADARecordStatus = 0 
LEFT JOIN dbo.Incident2Person _I2P WITH(NOLOCK) ON _I2P.Incident_ID = INC.ID AND _I2P.ADARecordStatus = 0 
LEFT JOIN Incident2Vehicle _I2V WITH(NOLOCK) ON _I2V.Incident_ID = INC.ID AND _I2V.ADARecordStatus = 0 
INNER JOIN @XML.nodes(''//Claim'') XML(Data) ON COALESCE(_KC21.BaseRiskClaim_Id, _I2P.BaseRiskClaim_Id, _I2V.BaseRiskClaim_Id) = CONVERT(varchar(MAX), XML.Data.query(''data(RiskClaim_ID)''))'

DECLARE @JoinClaimRef NVARCHAR(1000) ='
INNER JOIN dbo.KeoghsCase2Incident AS KC2I WITH(NOLOCK) ON KC2I.Incident_Id = INC.ID AND KC2I.ADARecordStatus = 0 
INNER JOIN dbo.KeoghsCase KC WITH(NOLOCK) ON KC.ID = KC2I.KeoghsCase_Id AND KC.ADARecordStatus = 0 
INNER JOIN @XML.nodes(''//Claim'') XKC(Data)'

DECLARE @JoinPerson	NVARCHAR(1000) ='
INNER JOIN dbo.Incident2Person I2P WITH(NOLOCK) ON I2P.Incident_Id = INC.ID AND I2P.ADARecordStatus = 0 AND I2P.LinkConfidence = 0 '

DECLARE @JoinPersonXML	NVARCHAR(1000) ='
INNER JOIN @XML.nodes(''//Claim'') XI2P(Data)'

DECLARE @JoinIncidentDate NVARCHAR(1000) ='
INNER JOIN @XML.nodes(''//Claim'') XINC(Data)'

DECLARE @JoinVehicle NVARCHAR(1000) ='
INNER JOIN Incident2Vehicle I2V WITH(NOLOCK) ON I2V.Incident_Id = INC.Id AND I2V.ADARecordStatus = 0 AND I2V.LinkConfidence = 0 '

DECLARE @JoinPeopleIn NVARCHAR(1000) ='
INNER JOIN #PerAll PA ON PA.PerID = I2P.Person_Id AND PA.LinkConfidence =0'

DECLARE @JoinVehicleIn NVARCHAR(1000) ='
INNER JOIN #VecAll VA ON VA.VecID = I2V.Vehicle_Id AND VA.LinkConfidence =0'

DECLARE @JoinPeopleUnconfirmedIn NVARCHAR(1000) ='
INNER JOIN #PerAll PAU ON PAU.PerID = I2P.Person_Id AND PAU.LinkConfidence =1'

DECLARE @JoinVehicleUnconfirmedIn NVARCHAR(1000) ='
INNER JOIN #VecAll VAU ON VAU.VecID = I2V.Vehicle_Id AND VAU.LinkConfidence =1'

DECLARE @HavingPeopleCount NVARCHAR(1000) ='
AND COUNT(DISTINCT PA.Id) >='

DECLARE @HavingVehicleCount NVARCHAR(1000) ='
AND COUNT(DISTINCT VA.Id) >='

DECLARE @HavingPeopleUnconfirmedCount NVARCHAR(1000) ='
AND COUNT(DISTINCT PAU.Id) >='

DECLARE @HavingVehicleUnconfirmedCount NVARCHAR(1000) ='
AND COUNT(DISTINCT VAU.Id) >='

SELECT @SQL = 
		'SELECT DISTINCT INC.Id, '
		+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
		+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
		+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType'
		+ @From 
		+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.ClientClaimReference ), 0) + ISNULL(CONVERT(INT,MRC.ClientID ), 0) > 0 THEN 
		+ @JoinClaimRef 
			+ dbo.fnFormatOnClause(
			CASE WHEN ClientClaimReference = 1 THEN 'AND KC.ClientReference = CONVERT(varchar(MAX), XKC.Data.query(''data(ClientClaimReference)'')) ' ELSE '' END
			+ CASE WHEN ClientID = 1 THEN 'AND KC.Client_Id = CONVERT(varchar(MAX), XKC.Data.query(''data(ClientID)'')) ' ELSE '' END
			)	
			ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.IncidentTime), 0) + ISNULL(CONVERT(INT,MRC.IncidentLocation), 0) + ISNULL(CONVERT(INT,MRC.IncidentCircumstances), 0) + ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) + ISNULL(CONVERT(INT,MRC.PeopleUnconfirmedMatch), 0) > 0 THEN 
		+ @JoinPerson ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.IncidentTime), 0) + ISNULL(CONVERT(INT,MRC.IncidentLocation), 0) + ISNULL(CONVERT(INT,MRC.IncidentCircumstances), 0) > 0 THEN 
		+ @JoinPersonXML
		+ dbo.fnFormatOnClause(
			CASE WHEN IncidentTime = 1 THEN 'AND I2P.IncidentTime  = CONVERT(varchar(MAX), XI2P.Data.query(''data(IncidentTime)'')) ' ELSE '' END
			+ CASE WHEN IncidentLocation = 1 THEN 'AND I2P.IncidentLocation = CONVERT(varchar(MAX), XI2P.Data.query(''data(IncidentLocation)'')) ' ELSE '' END
			+ CASE WHEN IncidentCircumstances = 1 THEN 'AND I2P.IncidentCircs = CONVERT(varchar(MAX), XI2P.Data.query(''data(IncidentCircumstances)'')) ' ELSE '' END
			)	
			ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.IncidentDate), 0) > 0 THEN
		+  @JoinIncidentDate 
		+ dbo.fnFormatOnClause(
			CASE WHEN IncidentDate = 1 THEN 'AND INC.IncidentDate = CONVERT(varchar(MAX), XINC.Data.query(''data(IncidentDate)'')) ' ELSE '' END
			)	
			ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) +  ISNULL(CONVERT(INT,MRC.VehicleUnconfirmedMatch), 0) > 0 THEN 
		+ @JoinVehicle ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 THEN 
		+ @JoinPeopleIn ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN 
		+ @JoinVehicleIn ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleUnconfirmedMatch), 0) > 0 THEN 
		+ @JoinPeopleUnconfirmedIn ELSE '' END
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleUnconfirmedMatch), 0) > 0 THEN 
		+ @JoinVehicleUnconfirmedIn ELSE '' END
		+ dbo.fnFormatAddCarriageReturn('WHERE INC.ADARecordStatus = 0 ') 
		+ dbo.fnFormatAddCarriageReturn('GROUP BY INC.Id')
		+ CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) + ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) + ISNULL(CONVERT(INT,MRC.PeopleUnconfirmedMatch), 0) + ISNULL(CONVERT(INT,MRC.VehicleUnconfirmedMatch), 0) > 0 THEN
		+ dbo.fnFormatAddCarriageReturn('HAVING') 
		+ dbo.fnFormatWhereClause(
			CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 THEN @HavingPeopleCount + CAST(MRC.PeopleMatch AS VARCHAR(5)) + ' ' ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN @HavingVehicleCount + CAST(MRC.VehicleMatch AS VARCHAR(5)) + ' ' ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleUnconfirmedMatch), 0) > 0 THEN @HavingPeopleUnconfirmedCount + CAST(MRC.PeopleUnconfirmedMatch AS VARCHAR(5)) + ' ' ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleUnconfirmedMatch), 0) > 0 THEN @HavingVehicleUnconfirmedCount+ CAST(MRC.VehicleUnconfirmedMatch AS VARCHAR(5)) + ' ' ELSE '' END
			)
		ELSE '' END
		FROM dbo.MatchingRulesClaim MRC WITH(NOLOCK)   
		WHERE RuleNo = @Id
		
RETURN @SQL

END