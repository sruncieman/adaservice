﻿

CREATE FUNCTION [dbo].[fnFormatOnClause] 
(
	@OnClause  NVARCHAR(1000)
)
RETURNS NVARCHAR(1000)
AS
BEGIN
	DECLARE @Result NVARCHAR(1000)
	
	IF (LEN(@OnClause) > 3)
		SET @Result = ' ON ' + RIGHT(@OnClause, LEN(@OnClause) - 3)
	ELSE
		SET @Result = ''
	
	
	RETURN @Result

END