﻿
CREATE FUNCTION fn_Tracesmart_Bitmask
(	
	@Mask INT
)
RETURNS TABLE 
AS
RETURN 
(
		SELECT 
		CONVERT(BIT, @Mask & 1)			AS Tracesmart_Passport,
		CONVERT(BIT, @Mask & 2)			AS Tracesmart_Telephone,
		CONVERT(BIT, @Mask & 4)			AS Tracesmart_Driving,
		CONVERT(BIT, @Mask & 8)			AS Tracesmart_Birth,
		CONVERT(BIT, @Mask & 16)		AS Tracesmart_SmartLink,
		CONVERT(BIT, @Mask & 32)		AS Tracesmart_NI,
		CONVERT(BIT, @Mask & 64)		AS Tracesmart_CardNumber,
		CONVERT(BIT, @Mask & 128)		AS Tracesmart_BankAccount,
		CONVERT(BIT, @Mask & 256)		AS Tracesmart_Mobile,
		CONVERT(BIT, @Mask & 512)		AS Tracesmart_Crediva,
		CONVERT(BIT, @Mask & 1024)		AS Tracesmart_CreditActive,
		CONVERT(BIT, @Mask & 2048)		AS Tracesmart_NHS,
		CONVERT(BIT, @Mask & 4096)		AS Tracesmart_Cardavs,
		CONVERT(BIT, @Mask & 8192)		AS Tracesmart_MPan,
		CONVERT(BIT, @Mask & 16384)		AS Tracesmart_Address,
		CONVERT(BIT, @Mask & 32768)		AS Tracesmart_DeathScreen,
		CONVERT(BIT, @Mask & 65536)		AS Tracesmart_DoB,
		CONVERT(BIT, @Mask & 131072)	AS Tracesmart_Sanction,
		CONVERT(BIT, @Mask & 262144)	AS Tracesmart_Insolvency,
		CONVERT(BIT, @Mask & 524288)	AS Tracesmart_CCJ
	
)