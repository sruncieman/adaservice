﻿

CREATE FUNCTION [dbo].[fnGetMatchingPersonTSql] 
(
	@Id INT,
	@UseRiskClaimId BIT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX)

	DECLARE @From NVARCHAR(1000) ='
	FROM Person PER'

	DECLARE @JoinPerson NVARCHAR(1000) ='
	INNER JOIN @XML.nodes(''//Person'') XPER(Data)'

	DECLARE @JoinRiskClaim NVARCHAR(1000) ='
	LEFT OUTER JOIN Person2Policy AS P2PER ON PER.Id = P2PER.Person_Id AND P2PER.AdaRecordStatus = 0
	LEFT OUTER JOIN Incident2Person AS I2PER ON PER.Id = I2PER.Person_Id AND I2PER.AdaRecordStatus = 0
	LEFT OUTER JOIN Vehicle2Person AS V2PER ON PER.Id = V2PER.Person_Id AND V2PER.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Person'') XRIS(Data) ON COALESCE(I2PER.BaseRiskClaim_Id, V2PER.BaseRiskClaim_Id, P2PER.BaseRiskClaim_Id) = XRIS.Data.value(''@RiskClaim_Id'', ''INT'')'

	DECLARE @JoinNINumber NVARCHAR(1000) =  '
	INNER JOIN Person2NINumber AS P2NI WITH(NOLOCK) ON PER.Id = P2NI.Person_Id AND P2NI.AdaRecordStatus = 0
	INNER JOIN NINumber AS NI WITH(NOLOCK) ON P2NI.NINumber_Id = NI.Id  AND NI.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Person/NINumbers/NINumber'') XNI(Data) ON NI.NINumber = XNI.Data.value(''@NINumber'', ''VARCHAR(50)'')'

	DECLARE @JoinDriver NVARCHAR(1000) =  '
	INNER JOIN Person2DrivingLicense AS P2DL WITH(NOLOCK) ON PER.Id = P2DL.Person_Id AND P2DL.AdaRecordStatus = 0 
	INNER JOIN DrivingLicense AS DL WITH(NOLOCK) ON P2DL.DrivingLicense_Id = DL.Id AND DL.AdaRecordStatus = 0 
	INNER JOIN @XML.nodes(''//Person/DrivingLicenses/DrivingLicense'') XDL(Data) ON DL.DriverNumber = XDL.Data.value(''@DriverNumber'', ''VARCHAR(50)'')'

	DECLARE @JoinPassport NVARCHAR(1000) =  '
	INNER JOIN Person2Passport AS P2Pas WITH(NOLOCK) ON PER.Id = P2Pas.Person_Id AND P2Pas.AdaRecordStatus = 0 
	INNER JOIN Passport AS Pas WITH(NOLOCK) ON P2Pas.Passport_Id = Pas.Id  AND Pas.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Person/Passports/Passport'') XPAS(Data) ON Pas.PassportNumber = XPAS.Data.value(''@PassportNumber'', ''VARCHAR(50)'')'

	DECLARE @JoinPaymentCard NVARCHAR(1000) =  '
	INNER JOIN Person2PaymentCard AS P2Pay WITH(NOLOCK) ON PER.Id = P2Pay.Person_Id AND P2Pay.AdaRecordStatus = 0 
	INNER JOIN PaymentCard AS Pay WITH(NOLOCK) ON P2Pay.PaymentCard_Id = Pay.Id AND Pay.AdaRecordStatus = 0 
	INNER JOIN @XML.nodes(''//Person/PaymentCards/PaymentCard'') XPAY(Data) ON Pay.PaymentCardNumber = XPAY.Data.value(''@PaymentCardNumber'', ''VARCHAR(50)'')'

	DECLARE @JoinAccount NVARCHAR(1000) =  '
	INNER JOIN Person2BankAccount AS P2BA WITH(NOLOCK) ON PER.Id = P2BA.Person_Id AND P2BA.AdaRecordStatus = 0 
	INNER JOIN BankAccount AS Acc WITH(NOLOCK) ON P2BA.BankAccount_Id = Acc.Id AND Acc.AdaRecordStatus = 0 
	INNER JOIN @XML.nodes(''//Person/BankAccounts/BankAccount'') XACC(Data) ON Acc.AccountNumber =  XACC.Data.value(''@AccountNumber'', ''VARCHAR(50)'')'

	DECLARE @JoinPolicy NVARCHAR(1000) =  '
	INNER JOIN Person2Policy AS P2POL ON PER.Id = P2POL.Person_Id  AND P2POL.AdaRecordStatus = 0
	INNER JOIN Policy AS POL ON P2POL.Policy_Id = POL.Id AND POL.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Person/Policies/Policy'') XPOL(Data) ON POL.PolicyNumber =  XPOL.Data.value(''@PolicyNumber'', ''VARCHAR(50)'')'

	DECLARE @JoinAddress NVARCHAR(1000) =  '
	INNER JOIN Person2Address P2A WITH(NOLOCK) ON PER.Id = P2A.Person_Id  AND P2A.AdaRecordStatus = 0
	INNER JOIN Address AS _ADD WITH(NOLOCK) ON P2A.Address_ID = _ADD.Id  AND _ADD.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Person/Addresses/Address'') XADD(Data)'

	DECLARE @JoinTelephone NVARCHAR(1000) =  '
	INNER JOIN Person2Telephone AS P2Tel WITH(NOLOCK) ON PER.Id = P2Tel.Person_Id  AND P2Tel.AdaRecordStatus = 0
	INNER JOIN Telephone AS Tel WITH(NOLOCK) ON P2Tel.Telephone_Id = Tel.Id  AND Tel.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Person/Telephones/Telephone'') XTEL(Data) ON Tel.TelephoneNumber = XTEL.Data.value(''@TelephoneNumber'', ''VARCHAR(50)'')'

	DECLARE @JoinVehicle NVARCHAR(1000) =  '
	INNER JOIN Vehicle2Person AS V2Per2 WITH(NOLOCK) ON PER.Id = V2Per2.Person_Id  AND V2Per2.AdaRecordStatus = 0
	INNER JOIN Vehicle AS Veh WITH(NOLOCK) ON V2Per2.Vehicle_Id = Veh.Id  AND Veh.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Person/Vehicles/Vehicle'') XVEH(Data)' 

	DECLARE @JoinOrganisation NVARCHAR(1000) =  '
	INNER JOIN Person2Organisation AS P2ORG WITH(NOLOCK) ON PER.Id = P2ORG.Person_Id  AND P2ORG.AdaRecordStatus = 0 AND P2ORG.Person2OrganisationLinkType_Id IN (1,2,3,4,5,6,7,8,9,21,23,25,26,27)
	INNER JOIN Organisation AS ORG WITH(NOLOCK) ON P2ORG.Organisation_Id = ORG.Id AND ORG.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Person/Organisations/Organisation'') XORG(Data)'
	
	DECLARE @JoinEmail NVARCHAR(1000) =  '
	INNER JOIN Person2Email AS P2E WITH(NOLOCK) ON PER.Id = P2E.Person_Id AND P2E.AdaRecordStatus = 0
	INNER JOIN dbo.Email AS EMI WITH(NOLOCK) ON  P2E.Email_Id = EMI.ID AND EMI.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Person/EmailAddresses/EmailAddress'') XEMI(Data) ON EMI.EmailAddress = XEMI.Data.value(''@EmailAddress'', ''VARCHAR(50)'')'


	SELECT
		@SQL =
		'SELECT PER.Id, '
		+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
		+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
		+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType'
		+ @From 
		+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT, FirstName), 0) + ISNULL(CONVERT(INT, LastName), 0) +  ISNULL(CONVERT(INT, DateOfBirth), 0)  > 0 THEN  
			@JoinPerson 
			+ dbo.fnFormatOnClause(CASE WHEN FirstName = 1 THEN 'AND PER.FirstName = XPER.Data.value(''@FirstName'', ''VARCHAR(50)'') ' ELSE '' END
			+	CASE WHEN LastName = 1 THEN 'AND PER.LastName = XPER.Data.value(''@LastName'', ''VARCHAR(50)'') ' ELSE '' END
			+	CASE WHEN  DateOfBirth  = 1 THEN 'AND CONVERT(DATE, PER.DateOfBirth, 103)  =  CONVERT(DATE, XPER.Data.value(''@DateOfBirth'', ''VARCHAR(50)''), 103) ' ELSE '' END)
		ELSE '' END  
		+ CASE WHEN NINumber = 1 THEN @JoinNINumber ELSE '' END 
		+ CASE WHEN DriverNumber  = 1 THEN @JoinDriver ELSE '' END 
		+ CASE WHEN PassportNumber  = 1 THEN @JoinPassport ELSE '' END 
  		+ CASE WHEN PaymentCardNumber = 1 THEN @JoinPaymentCard ELSE '' END 
		+ CASE WHEN AccountNumber = 1 THEN @JoinAccount ELSE '' END 
		+ CASE WHEN PolicyNumber = 1 THEN @JoinPolicy ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT, SubBuilding), 0) + ISNULL(CONVERT(INT, Building), 0) +  ISNULL(CONVERT(INT, BuildingNumber), 0) +  ISNULL(CONVERT(INT, Street), 0) +  ISNULL(CONVERT(INT, Locality), 0) +  ISNULL(CONVERT(INT, Town), 0) +  ISNULL(CONVERT(INT, PostCode), 0) + ISNULL(CONVERT(INT, PafUPRN), 0)> 0 
		THEN @JoinAddress
		+ dbo.fnFormatOnClause(
			  CASE WHEN SubBuilding = 1 THEN 'AND _ADD.SubBuilding = XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN Building = 1 THEN 'AND _ADD.Building = XADD.Data.value(''@Building'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN BuildingNumber = 1 THEN 'AND _ADD.BuildingNumber = XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN Street = 1 THEN 'AND _ADD.Street = XADD.Data.value(''@Street'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN Locality = 1 THEN 'AND _ADD.Locality = XADD.Data.value(''@Locality'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN Town = 1 THEN 'AND _ADD.Town = XADD.Data.value(''@Town'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN PostCode = 1 THEN 'AND _ADD.PostCode = XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN PafUPRN = 1 THEN 'AND NULLIF(_ADD.PafUPRN,'''') = XADD.Data.value(''@PafUPRN'', ''VARCHAR(50)'') ' ELSE '' END
		)
		ELSE '' END 
		+ CASE WHEN TelephoneNumber = 1 THEN @JoinTelephone ELSE '' END 
		
		
		+ CASE WHEN ISNULL(CONVERT(INT, VehicleRegistration), 0) +  ISNULL(CONVERT(INT, VehicleVIN), 0) > 0 
		THEN @JoinVehicle 
		+ dbo.fnFormatOnClause(
				CASE WHEN VehicleRegistration = 1 THEN 'AND VEH.VehicleRegistration = XVEH.Data.value(''@VehicleRegistration'', ''VARCHAR(50)'') ' ELSE '' END
				+ CASE WHEN VehicleVIN = 1 THEN 'AND VEH.VIN = XVEH.Data.value(''@VehicleVIN'', ''VARCHAR(50)'') ' ELSE '' END
		)
		ELSE '' END 
		

		+ CASE WHEN ISNULL(CONVERT(INT, OrganisationName), 0) + ISNULL(CONVERT(INT, RegisteredNumber), 0) +  ISNULL(CONVERT(INT, VATNumber), 0) > 0  
		THEN @JoinOrganisation
			+ dbo.fnFormatOnClause(
				CASE WHEN OrganisationName = 1 THEN 'AND ORG.OrganisationName = XORG.Data.value(''@OrganisationName'', ''VARCHAR(50)'') ' ELSE '' END
				+ CASE WHEN RegisteredNumber = 1 THEN 'AND ORG.RegisteredNumber = XORG.Data.value(''@RegisteredNumber'', ''VARCHAR(50)'') ' ELSE '' END
				+ CASE WHEN VATNumber = 1 THEN 'AND ORG.VATNumber = XORG.Data.value(''@VATNumber'', ''VARCHAR(50)'') ' ELSE '' END
		)
		ELSE '' END 
	+ CASE WHEN EmailAddress = 1 THEN @JoinEmail ELSE '' END 
	
	FROM     MatchingRulesPerson
	WHERE RuleNo = @Id

	RETURN @SQL

END