﻿
CREATE FUNCTION [dbo].[fnGetMatchingOrganisationTSql] 
(
	@Id INT,
	@UseRiskClaimId BIT,
	@RCid VARCHAR(10)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX)

	DECLARE @From NVARCHAR(1000) ='
	FROM dbo.Organisation ORG'
	
	DECLARE @JoinOrganisation NVARCHAR(1000) ='
	INNER JOIN @XML.nodes(''//Organisation'') XO(Data)'

	DECLARE @JoinRiskClaim NVARCHAR(1000) ='
	INNER JOIN Incident2Organisation AS INC2O ON ORG.Id = INC2O.Organisation_Id AND INC2O.AdaRecordStatus = 0 AND INC2O.BaseRiskClaim_Id = ' + @RCid + '
	--INNER JOIN @XML.nodes(''//Organisation'') XORG(Data) ON INC2O.BaseRiskClaim_Id = XORG.Data.value(''@RiskClaim_Id'', ''INT'')'

	DECLARE @JoinEmail NVARCHAR(1000) ='
	INNER JOIN Organisation2Email AS O2EMA ON ORG.Id = O2EMA.Organisation_Id AND O2EMA.AdaRecordStatus = 0
	INNER JOIN Email AS EMA ON O2EMA.Email_Id = EMA.Id AND EMA.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Organisation/EmailAddresses/EmailAddress'') XEMA(Data) ON EMA.EmailAddress = XEMA.Data.value(''@EmailAddress'', ''VARCHAR(100)'')'                    
	                               
	DECLARE @JoinWebSite NVARCHAR(1000) ='           
	INNER JOIN Organisation2WebSite AS O2WEB ON ORG.Id = O2WEB.Organisation_Id AND O2WEB.AdaRecordStatus = 0 
	INNER JOIN WebSite AS WEB ON O2WEB.WebSite_Id = WEB.Id AND WEB.AdaRecordStatus = 0     
	INNER JOIN @XML.nodes(''//Organisation/Websites/Website'') XWEB(Data) ON WEB.URL = XWEB.Data.value(''@URL'', ''VARCHAR(100)'')'             
          
	DECLARE @JoinIPAddress NVARCHAR(1000) ='    
	INNER JOIN Organisation2IPAddress AS O2ADD ON ORG.Id = O2ADD.Organisation_Id  AND O2ADD.AdaRecordStatus = 0
	INNER JOIN IPAddress AS IPA ON O2ADD.IPAddress_Id = IPA.Id AND IPA.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Organisation/IPAddresses/IPAddress'') XIPA(Data) ON IPA.IPAddress = XIPA.Data.value(''@IPAddress'', ''VARCHAR(50)'')'             

	DECLARE @JoinTelephone NVARCHAR(1000) ='
	INNER JOIN Organisation2Telephone AS O2TEL ON ORG.Id = O2TEL.Organisation_Id  AND O2TEL.AdaRecordStatus = 0
	INNER JOIN Telephone AS TEL ON O2TEL.Telephone_Id = TEL.Id  AND TEL.AdaRecordStatus = 0                 
	INNER JOIN @XML.nodes(''//Organisation/TelephoneNumbers/TelephoneNumber'') XTEL(Data) ON TEL.TelephoneNumber = XTEL.Data.value(''@TelephoneNumber'', ''VARCHAR(50)'')'             

	DECLARE @JoinBankAccount NVARCHAR(1000) ='
	INNER JOIN Organisation2BankAccount AS O2BAN ON ORG.Id = O2BAN.Organisation_Id  AND O2BAN.AdaRecordStatus = 0
	INNER JOIN BankAccount AS BAN ON O2BAN.BankAccount_Id = BAN.Id     AND BAN.AdaRecordStatus = 0   
	INNER JOIN @XML.nodes(''//Organisation/BankAccounts/BankAccount'') XBAN(Data) ON BAN.AccountNumber = XBAN.Data.value(''@AccountNumber'', ''VARCHAR(50)'')'             
 
	DECLARE @JoinPaymentCard NVARCHAR(1000) ='                      
	INNER JOIN  Organisation2PaymentCard AS O2PER ON ORG.Id = O2PER.Organisation_Id  AND O2PER.AdaRecordStatus = 0
	INNER JOIN PaymentCard AS PAY ON O2PER.PaymentCard_Id = PAY.Id     AND PAY.AdaRecordStatus = 0            
	INNER JOIN @XML.nodes(''//Organisation/PaymentCards/PaymentCard'') XPAY(Data) ON PAY.PaymentCardNumber = XPAY.Data.value(''@PaymentCardNumber'', ''VARCHAR(50)'')'             
	                
	DECLARE @JoinAddress NVARCHAR(1000) ='                    
	INNER JOIN Organisation2Address AS O2ADD ON ORG.Id = O2ADD.Organisation_Id AND O2ADD.AdaRecordStatus = 0 AND (O2ADD.BaseRiskClaim_Id = ' + ISNULL(@RCid,'''''') + ' OR ' + ISNULL(@RCid,'''''') + ' ='''')
	--INNER JOIN @XML.nodes(''//Organisation'') XORG1(Data) ON O2ADD.BaseRiskClaim_Id = XORG1.Data.value(''@RiskClaim_Id'', ''INT'')
	INNER JOIN Address AS _ADD ON O2ADD.Address_Id = _ADD.Id AND _ADD.AdaRecordStatus = 0
	INNER JOIN @XML.nodes(''//Organisation/Addresses/Address'') XADD(Data)
	'            
          
	SELECT
		@SQL =
		'SELECT DISTINCT ORG.Id, '
		+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
		+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
		--+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType'
		+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType'
		+ @From 
		--+ CASE WHEN RiskClaim_Id = 1 THEN @JoinRiskClaim ELSE '' END 
		+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT, OrganisationName), 0) + ISNULL(CONVERT(INT, RegisteredNumber), 0) +  ISNULL(CONVERT(INT, VATNumber), 0)  > 0 THEN  
			@JoinOrganisation 
			+ dbo.fnFormatOnClause(CASE WHEN OrganisationName = 1 THEN 'AND REPLACE(ORG.OrganisationName,''Limited'',''Ltd'') = REPLACE(XO.Data.value(''@OrganisationName'', ''VARCHAR(50)''),''Limited'',''Ltd'') ' ELSE '' END
			+	CASE WHEN RegisteredNumber = 1 THEN 'AND ORG.RegisteredNumber = XO.Data.value(''@RegisteredNumber'', ''VARCHAR(50)'') ' ELSE '' END
			+	CASE WHEN VATNumber = 1 THEN 'AND ORG.VATNumber = XO.Data.value(''@VATNumber'', ''VARCHAR(50)'') ' ELSE '' END)
		ELSE '' END  

		+ CASE WHEN EmailAddress = 1 THEN @JoinEmail ELSE '' END 
		
		+ CASE WHEN Website  = 1 THEN @JoinWebSite ELSE '' END 
		
		+ CASE WHEN IPAddress = 1 THEN @JoinIPAddress ELSE '' END 

		+ CASE WHEN TelephoneNumber = 1 THEN @JoinTelephone ELSE '' END 
		
		+ CASE WHEN AccountNumber = 1 THEN @JoinBankAccount ELSE '' END 

		+ CASE WHEN PaymentCardNumber = 1 THEN @JoinPaymentCard ELSE '' END 
  

		+ CASE WHEN ISNULL(CONVERT(INT, SubBuilding), 0) + ISNULL(CONVERT(INT, Building), 0) +  ISNULL(CONVERT(INT, BuildingNumber), 0) +  ISNULL(CONVERT(INT, Street), 0) +  ISNULL(CONVERT(INT, Locality), 0) +  ISNULL(CONVERT(INT, Town), 0) +  ISNULL(CONVERT(INT, PostCode), 0) +  ISNULL(CONVERT(INT, PafUPRN), 0) > 0 
		THEN @JoinAddress
		+ dbo.fnFormatOnClause(
			CASE WHEN PafUPRN = 1 THEN 'AND NULLIF(_ADD.PafUPRN,'''') = XADD.Data.value(''@PafUPRN'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN PostCode = 1 THEN 'AND _ADD.PostCode = XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN Building = 1 THEN 'AND _ADD.Building = XADD.Data.value(''@Building'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN SubBuilding = 1 THEN 'AND _ADD.SubBuilding = XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN Street = 1 THEN 'AND _ADD.Street = XADD.Data.value(''@Street'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN BuildingNumber = 1 THEN 'AND _ADD.BuildingNumber = XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN Town = 1 THEN 'AND _ADD.Town = XADD.Data.value(''@Town'', ''VARCHAR(50)'') ' ELSE '' END
			+ CASE WHEN Locality = 1 THEN 'AND _ADD.Locality = XADD.Data.value(''@Locality'', ''VARCHAR(50)'') ' ELSE '' END)
		ELSE '' END
		
	FROM     dbo.MatchingRulesOrganisation
	WHERE RuleNo = @Id

	RETURN @SQL

END