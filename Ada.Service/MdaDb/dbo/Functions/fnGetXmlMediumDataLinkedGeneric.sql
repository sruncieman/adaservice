﻿
CREATE FUNCTION [dbo].[fnGetXmlMediumDataLinkedGeneric]
(
	@RiskClaim_Id INT,
	@Person_Id INT,
	@LocalName VARCHAR(50),
	@RuleKey VARCHAR(50)
)
RETURNS  VARCHAR(500)
AS
BEGIN
	

	DECLARE @Val VARCHAR(500) = ''
	DECLARE @Sep CHAR(2) = '; '
	DECLARE @Len INT = 0

	SELECT 	@Val += CASE WHEN MD.DataMed1 IS NULL THEN '' ELSE  REPLACE(MD.DataMed1,'  ','')  END  + CASE WHEN MD.DataMed2 = 'true' THEN ' [C]' ELSE ' [U]' END + @Sep 
	FROM dbo.rpt_Level_One_Report_DataMed AS MD
	WHERE md.RiskClaim_Id = @RiskClaim_Id
    AND MD.Per_dbId = @Person_Id
    AND MD.localName = @LocalName
    AND MD.Rule_Key = @RuleKey
    ORDER BY MD.Rule_Key 

	
	IF LEN(@Val) > 0 SET @Len = 1
	
	IF @Val IS NOT NULL
		SET @Val = NULLIF(LEFT(@Val, LEN(@Val) - @Len), '')

	RETURN @Val

END