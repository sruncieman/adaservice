﻿






CREATE FUNCTION [dbo].[fn_GetOrganisationTree_20140801]
(
	 @Id INT,
	 @includeConfirmed BIT,
	 @includeUnconfirmed BIT,
	 @includeTentative BIT,
	 @includeThis BIT
)
RETURNS 
@T TABLE 
(
	Id INT
)
AS
BEGIN
	
DECLARE @LinkConfidence INT

	IF @includeTentative = 1  
		SET	@LinkConfidence = 2
	ELSE IF @includeUnconfirmed = 1  
		SET	@LinkConfidence = 1
	ELSE IF @includeConfirmed = 1  
		SET	@LinkConfidence = 0
		

	DECLARE @ObjectLevel INT = 1

	DECLARE @Tree TABLE(ObjectLevel INT, ObjectId INT, LinkConfidence INT)

	INSERT INTO @Tree
	SELECT @ObjectLevel, Organisation1_Id, LinkConfidence FROM dbo.Organisation2Organisation WHERE Organisation1_Id = @Id AND LinkConfidence <= @LinkConfidence 
	UNION ALL
	SELECT @ObjectLevel, Organisation2_Id, LinkConfidence FROM dbo.Organisation2Organisation WHERE Organisation2_Id = @Id AND LinkConfidence <= @LinkConfidence 


	WHILE(@@Rowcount > 0)
	BEGIN
		SET @ObjectLevel += 1
		INSERT INTO @Tree
			SELECT @ObjectLevel ObjectLevel, Organisation1_Id, LinkConfidence FROM dbo.Organisation2Organisation
			WHERE Organisation2_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Organisation1_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence 
			UNION ALL
			SELECT @ObjectLevel ObjectLevel, Organisation2_Id, LinkConfidence FROM dbo.Organisation2Organisation
			WHERE Organisation1_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Organisation2_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence 
	END
	
	
	IF @includeThis = 1
	INSERT INTO @T(Id)
	SELECT @Id
	
	INSERT INTO @T(Id)
	SELECT DISTINCT ObjectId FROM @Tree WHERE ISNULL(LinkConfidence, @LinkConfidence) = @LinkConfidence AND ObjectId != @Id

	
	
	RETURN 
END