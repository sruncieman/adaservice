﻿

CREATE FUNCTION [dbo].[fnGetMatchingBBPinTSql] 
(
	@Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN


DECLARE @SQL NVARCHAR(MAX)
DECLARE @RiskClaim_Id BIT = (SELECT RiskClaim_Id FROM dbo.MatchingRulesBBPin WHERE RuleNo = @Id)

IF (@RiskClaim_Id IS NULL)
	SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  BBPin AS A 
	WHERE ' 
	+ dbo.fnFormatWhereClause(CASE WHEN BBPin = 1 THEN ' AND BBPin = @BBPin' ELSE '' END + CASE WHEN DisplayName = 1 THEN ' AND DisplayName = @DisplayName' ELSE '' END )
FROM dbo.MatchingRulesBBPin WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  BBPin AS A 
	INNER JOIN Person2BBPin AS P2BB ON A.Id = P2BB.BBPin_Id AND P2BB.ADARecordStatus = 0
	WHERE ' 
	+ dbo.fnFormatWhereClause(CASE WHEN BBPin = 1 THEN ' AND BBPin = @BBPin' ELSE '' END + CASE WHEN DisplayName = 1 THEN ' AND DisplayName = @DisplayName' ELSE '' END + CASE WHEN RiskClaim_Id = 1 THEN ' AND BaseRiskClaim_Id = @RiskClaim_Id' ELSE '' END)
FROM dbo.MatchingRulesBBPin WHERE RuleNo = @Id




RETURN @SQL

END