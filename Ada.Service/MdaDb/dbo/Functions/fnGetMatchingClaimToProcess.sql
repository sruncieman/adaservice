﻿
CREATE FUNCTION [dbo].[fnGetMatchingClaimToProcess]
(	
	@XML XML
)
RETURNS TABLE 
AS
RETURN 
(
SELECT   MRC.RuleNo
		,MRC.ClientClaimReference
		,MRC.ClientID
		,MRC.IncidentDate
		,MRC.IncidentTime
		,MRC.IncidentLocation
		,MRC.PeopleMatch
		,MRC.VehicleMatch
		,MRC.[PeopleUnconfirmedMatch]
		,MRC.[VehicleUnconfirmedMatch]
		,MRC.[IncidentCircumstances]
		,MRC.PriorityGroup
		,MRC.MatchType
		,MRC.RiskClaimFlag
		,_XML.HasRiskClaim_ID
FROM dbo.MatchingRulesClaim MRC
INNER JOIN	(
			SELECT	 CONVERT(BIT,MAX(LEN(CONVERT(varchar(MAX), CR.Data.query('data(RiskClaim_ID)'))))) HasRiskClaim_ID
					,MAX(LEN(CONVERT(varchar(MAX), CR.Data.query('data(ClientClaimReference)')))) ClientClaimReference
					,MAX(LEN(CONVERT(varchar(MAX), CR.Data.query('data(ClientID)')))) ClientID
					,MAX(LEN(CONVERT(varchar(MAX), CR.Data.query('data(IncidentDate)')))) IncidentDate
					,MAX(LEN(CONVERT(varchar(MAX), CR.Data.query('data(IncidentTime)')))) IncidentTime
					,MAX(LEN(CONVERT(varchar(MAX), CR.Data.query('data(IncidentLocation)')))) IncidentLocation
					,MAX(VC.VehicleCount) VehicleCount
					,MAX(PEO.PersonCount) PersonCount
					,MAX(LEN(CONVERT(varchar(MAX), CR.Data.query('data(IncidentCircumstances)')))) IncidentCircumstances
			FROM @XML.nodes('//Data/Claim') CR(Data)
			INNER JOIN	(
						SELECT  COUNT(CONVERT(varchar(MAX), VEH.Data.query('data(.)'))) VehicleCount
						FROM @XML.nodes('//Data/Claim/Vehicles/Vehicle') VEH(Data)
						WHERE ISNUMERIC(CONVERT(varchar(MAX), VEH.Data.query('data(.)'))) = 1
						) VC ON 1=1
			INNER JOIN	(
						SELECT  COUNT(CONVERT(varchar(MAX), PEO.Data.query('data(.)'))) PersonCount
						FROM @XML.nodes('//Data/Claim/People/Person') PEO(Data)
						WHERE ISNUMERIC(CONVERT(varchar(MAX), PEO.Data.query('data(.)'))) = 1
						) PEO ON 1=1
			
			) _XMl
ON ISNULL(MRC.ClientClaimReference, 0) <= _XML.ClientClaimReference
AND ISNULL(MRC.ClientID, 0) <= _XML.ClientID
AND ISNULL(MRC.IncidentDate, 0) <= _XML.IncidentDate
AND ISNULL(MRC.IncidentTime, 0) <= _XML.IncidentTime
AND ISNULL(MRC.IncidentLocation, 0) <= _XML.IncidentLocation
AND ISNULL(MRC.VehicleMatch, 0) <= VehicleCount
AND ISNULL(MRC.PeopleMatch, 0) <= PersonCount
AND ISNULL(MRC.[PeopleUnconfirmedMatch],0) <= PersonCount
AND ISNULL(MRC.[VehicleUnconfirmedMatch],0) <= VehicleCount
AND ISNULL(MRC.[IncidentCircumstances],0) <= _XML.IncidentCircumstances

)