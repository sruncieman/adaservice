﻿

CREATE FUNCTION [dbo].[fnGetMatchingVehicleTSql] 
(
	@Id INT,
	@UseRiskClaimId BIT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN


DECLARE @SQL NVARCHAR(MAX)

DECLARE @From NVARCHAR(1000) ='
	FROM  Vehicle AS VEH WITH(NOLOCK)'

DECLARE @JoinVehicle NVARCHAR(1000) ='
	INNER JOIN @XML.nodes(''//Vehicle'') XVEH(Data)'

DECLARE @JoinRiskClaim NVARCHAR(1000) ='
	LEFT JOIN Incident2Vehicle AS I2V WITH (NOLOCK) ON VEH.Id = I2V.Vehicle_Id AND I2V.AdaRecordStatus = 0
    LEFT JOIN dbo.Vehicle2Organisation AS V2O WITH (NOLOCK) ON VEH.Id = V2O.Vehicle_Id AND V2O.AdaRecordStatus = 0
   
    INNER JOIN @XML.nodes(''//Vehicle'') XI2V(Data) 
    
    ON ISNULL(I2V.BaseRiskClaim_Id, V2O.BaseRiskClaim_Id)   = XI2V.Data.value(''@RiskClaim_Id'', ''INT'')'

--DECLARE @JoinColour NVARCHAR(1000) ='
--	INNER JOIN VehicleColour AS VC WITH(NOLOCK) ON VEH.VehicleColour_Id = VC.Id 
--	INNER JOIN @XML.nodes(''//Vehicle'') XVC(Data) ON VC.Colour = XVC.Data.value(''@Colour'', ''VARCHAR(50)'')'

DECLARE @JoinAddress NVARCHAR(1000) ='
	LEFT JOIN Vehicle2Person AS V2P WITH (NOLOCK) ON VEH.Id = V2P.Vehicle_Id AND V2P.AdaRecordStatus = 0
	LEFT JOIN Person2Address AS P2A WITH (NOLOCK) ON V2P.Person_Id = P2A.Person_Id AND P2A.AdaRecordStatus = 0
	LEFT JOIN Address AS AD WITH (NOLOCK) ON P2A.Address_Id = AD.Id AND AD.AdaRecordStatus = 0
	--LEFT JOIN dbo.Vehicle2Address AS V2A WITH (NOLOCK) ON  VEH.Id = Vehicle_Id 
	--LEFT JOIN Address AS AD1 WITH (NOLOCK) ON V2A.Address_Id = AD1.Id
	INNER JOIN @XML.nodes(''//Vehicle/Addresses/Address'') XAD(Data) '
	SELECT
		@SQL =
		'SELECT VEH.Id, '
		+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
		+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
		--+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType'
		+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType'
		+ @From 
		--+ CASE WHEN RiskClaim_Id = 1 THEN @JoinRiskClaim ELSE '' END 
		+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT, VehicleRegistration), 0) + ISNULL(CONVERT(INT, VIN), 0) +  ISNULL(CONVERT(INT, VehicleMake), 0)  +  ISNULL(CONVERT(INT, Model), 0) > 0 THEN  
		@JoinVehicle 
			+ dbo.fnFormatOnClause(
				CASE WHEN VehicleRegistration = 1 THEN 'AND VEH.VehicleRegistration = XVEH.Data.value(''@VehicleRegistration'', ''VARCHAR(50)'') ' ELSE '' END
				+ CASE WHEN VIN = 1 THEN 'AND VEH.VIN = XVEH.Data.value(''@VIN'', ''VARCHAR(50)'') ' ELSE '' END
				+ CASE WHEN VehicleMake = 1 THEN 'AND VEH.VehicleMake = XVEH.Data.value(''@VehicleMake'', ''VARCHAR(50)'') ' ELSE '' END
				+ CASE WHEN Model = 1 THEN 'AND VEH.Model = XVEH.Data.value(''@Model'', ''VARCHAR(50)'') ' ELSE '' END
				+ CASE WHEN Colour = 1 THEN 'AND VEH.VehicleColour_Id  = XVEH.Data.value(''@Colour'', ''VARCHAR(50)'') ' ELSE '' END)
		ELSE '' END  
		--+ CASE WHEN Colour = 1 THEN @JoinColour ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT, BuildingNumber), 0) + ISNULL(CONVERT(INT, PostCode), 0) + ISNULL(CONVERT(INT, PafUPRN), 0) > 0 THEN  
		@JoinAddress 
			+ dbo.fnFormatOnClause(
				CASE WHEN BuildingNumber = 1 THEN 'AND AD.BuildingNumber  = XAD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') ' ELSE '' END
				+ CASE WHEN PostCode = 1 THEN 'AND AD.PostCode   = XAD.Data.value(''@PostCode'', ''VARCHAR(50)'') ' ELSE '' END
				+ CASE WHEN PafUPRN = 1 THEN 'AND NULLIF(AD.PafUPRN,'''')   = XAD.Data.value(''@PafUPRN'', ''VARCHAR(50)'') ' ELSE '' END
				)
		ELSE '' END  
		
	FROM  MatchingRulesVehicles MRV WITH(NOLOCK)   
	WHERE RuleNo = @Id
	
RETURN @SQL

END