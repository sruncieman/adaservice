﻿



CREATE VIEW [dbo].[vPersonLinkedSettledCaseMessages]
AS
SELECT  SA.Text Salutation, P.FirstName, P.LastName, P.DateOfBirth, PT.PartyTypeText, SPT.SubPartyText, I2P.FiveGrading AS Incident2Person_FiveGrading, 
                      I2PO.FiveGrading AS Incident2PersonOutcome_FiveGrading, I2PO.MannerOfResolution, I.IncidentDate, CT.ClaimType, KC.KeoghsEliteReference, P.Id AS Person_Id, 
                      I.Id AS Incident_Id, I.ClaimType_Id, OC.CategoryTypeId
FROM         dbo.Person AS P INNER JOIN
                      dbo.Incident2Person AS I2P ON P.Id = I2P.Person_Id INNER JOIN
                      dbo.Incident AS I ON I2P.Incident_Id = I.Id LEFT OUTER JOIN
                      dbo.Incident2PersonOutcome AS I2PO ON I2P.Person_Id = I2PO.Person_Id AND I.ID = I2PO.Incident_ID/*PA:Added20140225,AddedAddittionalFilterOnIncident*/LEFT OUTER JOIN 
                      dbo.KeoghsCase2Incident AS KC2I ON KC2I.Incident_Id = I.Id AND I2P.Incident_Id = I2P.Incident_Id LEFT OUTER JOIN
                      dbo.KeoghsCase AS KC ON KC2I.KeoghsCase_Id = KC.Id INNER JOIN
                      dbo.PartyType AS PT ON I2P.PartyType_Id = PT.Id INNER JOIN
                      dbo.SubPartyType AS SPT ON I2P.SubPartyType_Id = SPT.Id INNER JOIN
                      dbo.ClaimType AS CT ON I.ClaimType_Id = CT.Id LEFT JOIN /*PA:20140214,ChangedFromInnerToLeftJoin*/
                      dbo.OutcomeCategories AS OC ON I2PO.MannerOfResolution = OC.CategoryText INNER JOIN
                      dbo.Salutation SA ON P.Salutation_Id = SA.Id