﻿

CREATE VIEW [dbo].[vOrganisation2Organisation]

AS 

	SELECT 
			CASE WHEN Organisation1_Id > Organisation2_Id THEN Organisation1_Id ELSE Organisation2_Id END ParentId,
			CASE WHEN Organisation1_Id > Organisation2_Id THEN Organisation2_Id ELSE Organisation1_Id END ChildID,
			LinkConfidence
	FROM dbo.Organisation2Organisation
	  WHERE OrganisationLinkType_Id = 7 AND ADARecordStatus = 0