﻿
CREATE VIEW [dbo].[vVehicle2Vehicle]

AS 

	SELECT 
			CASE WHEN Vehicle1_Id > Vehicle2_Id THEN Vehicle1_Id ELSE Vehicle2_Id END ParentId,
			CASE WHEN Vehicle1_Id > Vehicle2_Id THEN Vehicle2_Id ELSE Vehicle1_Id END ChildID,
			LinkConfidence
	FROM dbo.Vehicle2Vehicle
	  WHERE  ADARecordStatus = 0