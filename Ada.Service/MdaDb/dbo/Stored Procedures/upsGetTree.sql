﻿


CREATE PROCEDURE [dbo].[upsGetTree]

	@tblTree AS tblTree READONLY,
	@includeConfirmed BIT,
	@includeUnconfirmed BIT,
	@includeTentative BIT,
	@includeThis BIT
	
	
AS

	DECLARE @LinkConfidence INT

	IF @includeTentative = 1  
		SET	@LinkConfidence = 2
	ELSE IF @includeUnconfirmed = 1  
		SET	@LinkConfidence = 1
	ELSE IF @includeConfirmed = 1  
		SET	@LinkConfidence = 0

	 DECLARE @T AS tblTree

	 
	 DECLARE @Lvl INT = (SELECT MAX(lvl) +1 FROM @tblTree)

	INSERT INTO @T
	 SELECT @Lvl, Person2_Id, LinkConfidence FROM dbo.Person2Person 
	 WHERE Person1_Id IN (SELECT ID FROM @tblTree WHERE Lvl = @Lvl -1) AND Person2_Id NOT IN(SELECT ID FROM @tblTree) AND  LinkConfidence <= @LinkConfidence 
	UNION ALL
	 SELECT @Lvl, Person1_Id, LinkConfidence FROM dbo.Person2Person 
	 WHERE Person2_Id IN (SELECT ID FROM @tblTree WHERE Lvl = @Lvl -1) AND Person1_Id NOT IN(SELECT ID FROM @tblTree) AND  LinkConfidence <= @LinkConfidence 
 
	IF(@@Rowcount > 0)
	 BEGIN
			INSERT INTO @T
			 SELECT * FROM @tblTree
		 
		 EXEC upsGetTree @T, @includeConfirmed, @includeUnconfirmed, @includeTentative, @includeThis 
	END
	ELSE
	BEGIN 
		IF @includeThis = 1
			SELECT ID FROM @tblTree WHERE ISNULL(LinkConfidence, @LinkConfidence) = @LinkConfidence
		ELSE
			SELECT ID FROM @tblTree WHERE ISNULL(LinkConfidence, @LinkConfidence) = @LinkConfidence AND Lvl != (SELECT MIN(Lvl) FROM @tblTree)
	END