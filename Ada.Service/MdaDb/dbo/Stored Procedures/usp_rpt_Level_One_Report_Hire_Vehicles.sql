﻿CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Hire_Vehicles]

	@RiskClaim_Id INT,
	@Organisation_Id INT

AS

DECLARE @Msg VARCHAR(50) = 'Not Provided'

SELECT   DISTINCT HV.RiskClaim_Id
		,HV.Inc_DbId
		,HV.Parent_Vehicle_Id
		,HV.Per_DbId
		,HV.Org_DbId
		,HV.Vehicle_Id
		,HV.ParentName
		,HV.LocalName
		,HV.CategoryOfLoss
		,HV.CategoryOfLoss_Text
		,ISNULL(NULLIF(HV.Colour_Text,'Unknown'),@Msg) Colour
		,HV.Colour_Text
		,HV.EngineCapacity
		,HV.Fuel
		,HV.Fuel_Text
		,HV.IsKeyAttractor
		,ISNULL(HV.Make,@Msg) Make
		,ISNULL(HV.Model,@Msg) Model
		,HV.OccupancyCount
		,HV.Transmission
		,HV.Transmission_Text
		,HV.VehicleIncidentLink
		,HV.VehicleIncidentLink_Text
		,ISNULL(HV.VehicleRegistration,@Msg) VehicleRegistration
		,HV.VehicleType
		,HV.VehicleType_Text
		,HV.VIN
		,ISNULL(HV.OrganisationName,@Msg) OrganisationName
		,HV.HireStartDate
		,HV.HireEndDate
		,HV.MessageHeader
		,'Hire Vehicle' + ' - ' + ISNULL(HV.VehicleRegistration,@Msg) +' , ' + Org.ReportHeader + ', ' + P.ReportHeader + ', ' + Veh.ReportHeader ReportHeader
FROM dbo.rpt_Level_One_Report_Hire_Vehicles HV
LEFT JOIN (
		  SELECT RiskClaim_Id, Org_DbId, Per_DbId, ReportHeader
		  FROM dbo.rpt_Level_One_Report_Organisation
		  WHERE RiskClaim_Id = @RiskClaim_Id 
		  AND Org_DbId = @Organisation_Id
		  GROUP BY RiskClaim_Id, Org_DbId, Per_DbId, ReportHeader
		  ) Org ON Org.RiskClaim_Id = HV.RiskClaim_Id AND Org.Org_DbId = HV.Org_DbId
LEFT JOIN	(
			SELECT RiskClaim_Id, Vehicle_Id, Person_Id, REPLACE(ReportHeader,'Involvement Unknown - ', 'Involvement Unknown - Details Unknown') ReportHeader
			FROM dbo.rpt_Level_One_Report_Person
			WHERE RiskClaim_Id = @RiskClaim_Id
			GROUP BY RiskClaim_Id, Vehicle_Id, Person_Id, REPLACE(ReportHeader,'Involvement Unknown - ', 'Involvement Unknown - Details Unknown')
			) P ON P.RiskClaim_Id = HV.RiskClaim_Id AND P.Person_Id = org.Per_DbId
LEFT JOIN	(
			SELECT RiskClaim_Id, Vehicle_Id, ReportHeader
			FROM dbo.rpt_Level_One_Report_Vehicles
			WHERE RiskClaim_Id = @RiskClaim_Id 
			GROUP BY RiskClaim_Id, Vehicle_Id, ReportHeader
			) Veh ON Veh.RiskClaim_Id = HV.RiskClaim_Id AND Veh.Vehicle_Id = P.Vehicle_Id
WHERE HV.RiskClaim_Id = @RiskClaim_Id 
AND HV.Org_DbId = @Organisation_Id


/****** Object:  UserDefinedFunction [dbo].[fnGetXmlMediumDataOccupation]    Script Date: 02/11/2014 13:01:10 ******/
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'usp_rpt_Level_One_Report_Hire_Vehicles';

