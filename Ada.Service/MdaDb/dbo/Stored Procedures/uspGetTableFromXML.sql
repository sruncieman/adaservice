﻿CREATE PROCEDURE uspGetTableFromXML




 @Xml XML,

 @FROM NVARCHAR(MAX) = '//a:IncidentVehicles/a:VehicleRisk',

 @XMLNAMESPACES NVARCHAR(200) =  'DEFAULT ''http://www.keoghs.co.uk/ScoresV1'', ''http://www.w3.org/2001/XMLSchema-instance'' as i, ''http://schemas.datacontract.org/2004/07/RiskEngine.Scoring.Entities'' as a'
 



AS




DECLARE @Fields NVARCHAR(MAX) = ''

DECLARE @Sql NVARCHAR(MAX)




SET @SQL =' 

;WITH XMLNAMESPACES (' + @XMLNAMESPACES + ')
 
SELECT 

  @Fields +=  CHAR(13) + CHAR(9) +   CHAR(9) +  ''TBL.COL.query(''''data(a:'' + TBL.COL.value(''fn:local-name(.)'', ''nvarchar(50)'') 
 
  + '')'''') AS '' + TBL.COL.value(''fn:local-name(.)'', ''nvarchar(50)'') + '', ''

FROM @Xml.nodes(''' + @FROM + '/*'') AS TBL(COL)
 
'




PRINT @SQL




EXEC sp_executesql @Sql, N'@Xml XML, @Fields NVARCHAR(MAX) OUTPUT', @Xml = @Xml, @Fields = @Fields OUTPUT




PRINT @Fields
 



SET @Fields = REPLACE(@Fields, 'data(a:_ScoreResults', 'data(a:_ScoreResults/Score')

SET @Fields = SUBSTRING(@Fields, 1, LEN(@Fields) -1)

SET @Sql = ';WITH XMLNAMESPACES (' + @XMLNAMESPACES + ')' + CHAR(13) + CHAR(9) +  ' SELECT ' + @Fields  +  CHAR(13) + CHAR(9) + ' FROM @Xml.nodes(''' + @FROM + ''') AS TBL(COL)'
 



PRINT @SQL

;




EXEC sp_executesql @Sql, N'@Xml XML', @Xml = @Xml