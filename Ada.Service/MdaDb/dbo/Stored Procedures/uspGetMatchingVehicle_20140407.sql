﻿



CREATE PROCEDURE [dbo].[uspGetMatchingVehicle_20140407]


	@XML XML,
	@FieldsFlag BIT OUTPUT 
	
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--XML DEFINITION NEEDED TO ACCOUNT FOR MISSING TAGS
DECLARE @XML_DEF VARCHAR(MAX) = '<Vehicle RiskClaim_Id="" VehicleMake="" Model="" VehicleRegistration="" Colour="" VIN=""><Addresses><Address PafUPRN="" BuildingNumber="" PostCode="" /></Addresses></Vehicle>'
DECLARE @XML_Check XML = '<Data>' +  REPLACE(CONVERT(VARCHAR(MAX), @XML), 'UNKNOWN', '') +  @XML_DEF + '</Data>'

SET @FieldsFlag = 0 --RETURN VALUE 0 IF NOT ENOUGHT DATA SUPPLIED
DECLARE @T AS TABLE(TID INT NOT NULL IDENTITY(1,1), RuleNo INT, PriorityGroup INT, RiskClaimFlag INT, HasRiskClaim_Id BIT)
DECLARE @Ret TABLE(ID INT, RuleNo INT, PriorityGroup INT, MatchType INT)

--CREATING TABLE OF POSSABLE RULES TO MATCH
INSERT INTO @T(RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id)
SELECT RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id FROM fnGetMatchingVehicleToProcess (@XML_Check) ORDER BY PriorityGroup ,RuleNo
DECLARE @ROWCOUNT INT = @@ROWCOUNT	
IF @ROWCOUNT = 0  --NOT ENOUGHT DATA SUPPLIED EXIT PROCEDURE
	GOTO MatchEnd
ELSE
	SET @FieldsFlag = 1
	
DECLARE @RuleNo INT
DECLARE @PriorityGroup INT
DECLARE @RiskClaimFlag TINYINT
DECLARE @ExecWithRcId BIT
DECLARE @ExecWithOutRcId BIT
DECLARE @Count INT = 1
DECLARE @SQL NVARCHAR(MAX)

--LOOP EACH RULE, EXIT WHEN A MATCH IS FOUND
WHILE @Count <= @ROWCOUNT
BEGIN

	SELECT
		@RuleNo = RuleNo,	
		@PriorityGroup = PriorityGroup,	
		@RiskClaimFlag = RiskClaimFlag,
		@ExecWithRcId = CASE WHEN HasRiskClaim_Id = 1 AND RiskClaimFlag > 0 THEN 1 ELSE 0 END,
		@ExecWithOutRcId = CASE WHEN RiskClaimFlag = 0 OR RiskClaimFlag = 2 THEN 1 ELSE 0 END
	FROM @T WHERE TID = @Count
		
	--PROCESS ALL TENTATIVE (Risk Claim Flag) UNION SAME RULE GROUPS 
	IF @RiskClaimFlag = 0
	BEGIN
		
		DECLARE @GrpCount INT = @Count
		DECLARE @GrpCountEnd INT = (SELECT MAX(TID) FROM @T WHERE PriorityGroup = (SELECT PriorityGroup FROM @T WHERE TID = @Count) AND RiskClaimFlag = 0 AND TID >= @Count)
		SET @SQL = dbo.fnGetMatchingVehicleTSql(@RuleNo, 0)  + CHAR(10) + 'WHERE  (VEH.ADARecordStatus = 0) '
		
		WHILE @GrpCount < @GrpCountEnd
		BEGIN
			SET @GrpCount += 1
			SELECT @RuleNo = RuleNo FROM @T WHERE TID = @GrpCount
			SET @SQL +=   CHAR(10) + 'UNION ALL' + CHAR(10) + dbo.fnGetMatchingVehicleTSql(@RuleNo, 0)  + CHAR(10) + 'WHERE  (VEH.ADARecordStatus = 0) '

		END
			PRINT '################################# RiskClaimFlag = 0 START #######################################'
			PRINT ''
			PRINT @SQL
			PRINT ''
			PRINT '################################# RiskClaimFlag = 0 END #########################################'
		INSERT INTO @Ret
		EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
		IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
		
		SET @Count = @GrpCountEnd
		
	END
	ELSE
	BEGIN
		--PROCESS ALL CONF (Risk Claim Flag) If we have a RC_ID 
		IF @ExecWithRcId = 1
		BEGIN
			SET @SQL = dbo.fnGetMatchingVehicleTSql(@RuleNo, 1)  + CHAR(10) + 'WHERE  (VEH.ADARecordStatus = 0) '
			PRINT ''
			PRINT '------------------------------------WithRcId-------------------------------------------'

			PRINT @SQL
			PRINT ''
			PRINT ''
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
				
		END
		
		--PROCESS ALL UN-CONF (Risk Claim Flag) 
		IF @ExecWithOutRcId = 1
		BEGIN
			PRINT ''
			PRINT '----------------------------------WithOutRcId------------------------------------------'

			SET @SQL = dbo.fnGetMatchingVehicleTSql(@RuleNo, 0)  + CHAR(10) + 'WHERE  (VEH.ADARecordStatus = 0) '
			PRINT @SQL
			PRINT ''
			PRINT ''
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
			
		END

	END
	
SET @Count += 1

END 

MatchEnd:
	
SELECT ID, MIN(RuleNo) AS RuleNo, PriorityGroup, MatchType FROM @Ret 
GROUP BY ID, PriorityGroup, MatchType 
ORDER BY RuleNo DESC, ID ASC