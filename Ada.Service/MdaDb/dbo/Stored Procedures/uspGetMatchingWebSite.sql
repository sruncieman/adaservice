﻿
CREATE PROCEDURE [dbo].[uspGetMatchingWebSite]
	
	@RiskClaim_Id INT = NULL,
	@URL VARCHAR(50) = NULL,
	@FieldsFlag BIT OUTPUT 

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @FieldsFlag = 1

SELECT ID, 0 RuleNo, 1 PriorityGroup, 0 MatchType FROM dbo.WebSite WHERE ADARecordStatus = 0 AND URL = @URL