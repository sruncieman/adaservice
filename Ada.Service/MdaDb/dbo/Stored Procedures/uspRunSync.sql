﻿
CREATE PROCEDURE [dbo].[uspRunSync]
@SyncSuccess SMALLINT OUTPUT
AS

/*
 0 - Complete, 
-1 - Busy, try again later, 
-2 - Sync Error needs manual intervention, 
-3 - Migration Error needs manual intervention
*/

EXECUTE MDAControl.[Sync].[uspRunSync] @Success = @SyncSuccess OUTPUT