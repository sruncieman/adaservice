﻿
CREATE PROCEDURE uspGetPotentialClaimant

 @PartyType INT	= NULL,
 @SubPartyType INT = NULL
 
AS

SELECT     TOP (1000) ID, PartyType, SubPartyType, Flag, RecordStatus, ADARecordStatus
FROM         PotentialClaimant  

WHERE 
		(PartyType	= @PartyType OR @PartyType IS NULL)
AND		(SubPartyType = @SubPartyType OR @SubPartyType IS NULL)