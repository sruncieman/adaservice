﻿
CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Vehicles]

	@RiskClaim_Id INT

AS

DECLARE @Msg VARCHAR(50) = 'Not Provided'

SELECT	 RiskClaim_Id
		,Vehicle_Id
		,CategoryOfLoss
		,CategoryOfLoss_Text
		,Colour
		,ISNULL(REPLACE(Colour_Text,'UnKnown','Unknown'),@Msg) Colour_Text
		,EngineCapacity
		,Fuel
		,Fuel_Text
		,IsKeyAttractor
		,ISNULL(Make,@Msg) Make
		,ISNULL(Model,@Msg) Model
		,OccupancyCount
		,Transmission
		,Transmission_Text
		,VehicleIncidentLink
		,VehicleIncidentLink_Text
		,ISNULL(VehicleRegistration,@Msg) VehicleRegistration
		,VehicleType
		,VehicleType_Text
		,VIN
		,SortOrder
		,MessageHeader
		,ReportHeader
FROM dbo.rpt_Level_One_Report_Vehicles 
WHERE RiskClaim_Id = @RiskClaim_Id