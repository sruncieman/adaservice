﻿CREATE PROCEDURE [dbo].[uspPersonNumberOpenCases]
                @personId int,
                @includeThisPersonId bit,
                @includeConfirmed bit,
                @includeUnconfirmed bit,
                @includeTentative bit,
                @startDate smallDateTime,
                @endDate smallDateTime,
                @caseIncidentLinkType int,
                @IsPotentialClaimant BIT  = NULL
AS

IF @IsPotentialClaimant = 0

	SELECT COUNT(DISTINCT(Id)) AS OpenCases FROM(
        SELECT 
			  I2C.Id, SubPartyType_Id, PartyType_Id
			  FROM Incident2Person AS I2P
			  INNER JOIN KeoghsCase2Incident AS I2C ON I2P.Incident_Id = I2C.Incident_Id
			  INNER JOIN Incident AS I ON I2P.Incident_Id = I.Id
			  INNER JOIN KeoghsCase KC ON KC.Id = I2c.KeoghsCase_Id
			  LEFT OUTER JOIN Incident2PersonOutcome AS I2P_1 ON I2P.Incident_Id = I2P_1.Incident_Id AND I2P.Person_Id = I2P_1.Person_Id
	          
		WHERE (I2P_1.Id IS NULL AND CaseIncidentLinkType_Id = @caseIncidentLinkType
			  and I.IncidentDate >= @startDate
			  and I.IncidentDate <= @endDate
			  and I2P.ADArecordStatus = 0
			  and I.ADArecordStatus = 0 
			  and I.IncidentType_Id = 8
			  and kc.CaseStatus_Id = 2
			  and I2P.Person_Id in (SELECT Id FROM dbo.fn_GetPersonTree(@personId, @includeConfirmed ,@includeUnconfirmed, @includeTentative, @includeThisPersonId)))
		) A
			WHERE  
			  (PartyType_Id = 3 AND SubPartyType_Id IN (0, 1, 3)) OR (PartyType_Id IN (5 ,9))
			  
ELSE IF @IsPotentialClaimant = 1

	SELECT COUNT(DISTINCT(Id)) AS OpenCases FROM(
        SELECT 
			  I2C.Id, SubPartyType_Id, PartyType_Id
			  FROM Incident2Person AS I2P
			  INNER JOIN KeoghsCase2Incident AS I2C ON I2P.Incident_Id = I2C.Incident_Id
			  INNER JOIN Incident AS I ON I2P.Incident_Id = I.Id
			  INNER JOIN KeoghsCase KC ON KC.Id = I2c.KeoghsCase_Id
			  LEFT OUTER JOIN Incident2PersonOutcome AS I2P_1 ON I2P.Incident_Id = I2P_1.Incident_Id AND I2P.Person_Id = I2P_1.Person_Id
	          
		WHERE (I2P_1.Id IS NULL AND CaseIncidentLinkType_Id = @caseIncidentLinkType
			  and I.IncidentDate >= @startDate
			  and I.IncidentDate <= @endDate
			  and I2P.ADArecordStatus = 0
			  and I.ADArecordStatus = 0 
			  and I.IncidentType_Id = 8
			  and kc.CaseStatus_Id = 2
			  and I2P.Person_Id in (SELECT Id FROM dbo.fn_GetPersonTree(@personId, @includeConfirmed ,@includeUnconfirmed, @includeTentative, @includeThisPersonId)))
		) A
			WHERE  
			  (PartyType_Id != 3 AND SubPartyType_Id NOT IN (0, 1, 3)) OR (PartyType_Id NOT IN (5 ,9))

ELSE

	 SELECT 
          COUNT(DISTINCT(I2C.Id)) AS OpenCases  FROM Incident2Person AS I2P
          INNER JOIN KeoghsCase2Incident AS I2C ON I2P.Incident_Id = I2C.Incident_Id
          INNER JOIN Incident AS I ON I2P.Incident_Id = I.Id
          INNER JOIN KeoghsCase KC ON KC.Id = I2c.KeoghsCase_Id
          LEFT OUTER JOIN Incident2PersonOutcome AS I2P_1 ON I2P.Incident_Id = I2P_1.Incident_Id AND I2P.Person_Id = I2P_1.Person_Id
          WHERE (I2P_1.Id IS NULL AND CaseIncidentLinkType_Id = @caseIncidentLinkType
          and I.IncidentDate >= @startDate
          and I.IncidentDate <= @endDate
          and I2P.ADArecordStatus = 0
          and I.ADArecordStatus = 0 
          and I.IncidentType_Id = 8
          and kc.CaseStatus_Id = 2
		  and I2P.Person_Id in (SELECT Id FROM dbo.fn_GetPersonTree(@personId, @includeConfirmed ,@includeUnconfirmed, @includeTentative, @includeThisPersonId)))