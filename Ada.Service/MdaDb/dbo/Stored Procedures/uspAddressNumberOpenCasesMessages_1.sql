﻿
CREATE PROCEDURE [dbo].[uspAddressNumberOpenCasesMessages]
 /**************************************************************************************************/
-- ObjectName:		MDA.dbo.uspAddressNumberOpenCasesMessages
--
-- Description:		Fraud Risk Address Rules, modified from uspAddressNumberOpenCasesMessages 
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-10-03			Paul Allen		Created, TFS Item 8303
/**************************************************************************************************/
(
 @personId					INT
,@includeThisPersonId		BIT
,@includeConfirmed			BIT
,@includeUnconfirmed		BIT
,@includeTentative			BIT
,@startDate					SMALLDATETIME
,@endDate					SMALLDATETIME
,@caseIncidentLinkType		INT
,@IsPotentialClaimant		BIT  = NULL
,@StagedContrived			BIT  
)
AS
SET NOCOUNT ON

DECLARE @ClaimTypeID TABLE (ClaimTypeID INT)
DECLARE @PotentialClaimantTable TABLE (IsPotentialClaimant INT)

IF @StagedContrived = 1
BEGIN
	INSERT INTO @ClaimTypeID (ClaimTypeID) VALUES (19)
END
ELSE
BEGIN
	INSERT INTO @ClaimTypeID (ClaimTypeID)
	SELECT Id FROM dbo.ClaimType WHERE ID != 19
END

IF @IsPotentialClaimant IS NULL
BEGIN
	INSERT INTO @PotentialClaimantTable (IsPotentialClaimant)
	SELECT [Flag]
	FROM [dbo].[PotentialClaimant]
	GROUP BY [Flag]
END
ELSE
BEGIN
	INSERT INTO @PotentialClaimantTable (IsPotentialClaimant)
	SELECT @IsPotentialClaimant
END



SELECT	 PT.PartyTypeText
		,SPT.SubPartyText
		,I.IncidentDate
		,REPLACE(CT.ClaimType, 'RTA - ', '') ClaimType
		,KC.KeoghsEliteReference
		,I2P.FiveGrading
		,CASE WHEN @includeUnconfirmed = 1 THEN  S.Text ELSE NULL END AS Salutation
		,CASE WHEN @includeUnconfirmed = 1 THEN  P.FirstName ELSE NULL END AS FirstName
		,CASE WHEN @includeUnconfirmed = 1 THEN  P.LastName ELSE NULL END AS LastName
		,CASE WHEN @includeUnconfirmed = 1 THEN  P.DateOfBirth ELSE NULL END AS DateOfBirth
FROM Incident2Person AS I2P
INNER JOIN KeoghsCase2Incident AS I2C ON I2P.Incident_Id = I2C.Incident_Id
INNER JOIN Incident AS I ON I2P.Incident_Id = I.Id
INNER JOIN KeoghsCase KC ON KC.Id = I2c.KeoghsCase_Id
LEFT OUTER JOIN Incident2PersonOutcome AS I2P_1 ON I2P.Incident_Id = I2P_1.Incident_Id AND I2P.Person_Id = I2P_1.Person_Id
INNER JOIN PartyType PT ON I2P.PartyType_Id = PT.Id
INNER JOIN SubPartyType SPT ON I2P.SubPartyType_Id = SPT.Id
INNER JOIN ClaimType CT ON I.ClaimType_Id = CT.Id
INNER JOIN Person P ON I2P.Person_Id = P.Id
INNER JOIN Salutation S ON P.Salutation_Id = S.Id
INNER JOIN [dbo].[PotentialClaimant] PC ON PC.PartyType = PT.Id AND PC.SubPartyType = SPT.Id
WHERE I2P_1.Id IS NULL 
AND CaseIncidentLinkType_Id = @caseIncidentLinkType
AND I.IncidentDate >= @startDate
AND I.IncidentDate <= @endDate
AND I2P.ADArecordStatus = 0
AND I.ADArecordStatus = 0 
AND I.IncidentType_Id = 8
AND kc.CaseStatus_Id = 2
AND I2P.Person_Id in (SELECT Id FROM dbo.fn_GetPersonTree(@personId, @includeConfirmed ,@includeUnconfirmed, @includeTentative, @includeThisPersonId))
AND EXISTS (SELECT TOP 1 1 FROM @ClaimTypeID A WHERE A.ClaimTypeID = I.ClaimType_Id)
AND EXISTS (SELECT TOP 1 1 FROM @PotentialClaimantTable B WHERE B.IsPotentialClaimant = PC.Flag)