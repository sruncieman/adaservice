﻿CREATE PROCEDURE [dbo].[uspCreateExternalRequests]
    @RiskBatchId INT,
    @RequestedBy VARCHAR(50),
    @RequestedWhen SMALLDATETIME
AS
    DECLARE @RiskExternalServiceId INT
	DECLARE @RiskExternalServiceMask INT

	-------------------Start of code for Tracesmart---------------------------

	--Read Mask value and service ID for Tracesmart
    SELECT @RiskExternalServiceId = Id, @RiskExternalServiceMask = MaskValue 
	FROM RiskExternalServices res 
	WHERE res.ServiceName = 'Tracesmart'

	--Create External Records
    INSERT INTO [dbo].[RiskExternalServicesRequests] ([RiskClaim_Id], [RiskExternalService_Id], [Unique_Id], [RequestedBy], [RequestedWhen], [IsRepeatCall])
    SELECT  rc.Id AS RiskClaim_Id, 
            @RiskExternalServiceId AS RiskExternalService_Id, 
            p2a.Id AS Unique_Id,
            @RequestedBy AS RequestedBy, 
            @RequestedWhen AS RequestedWhen,
			case when p2a.TracesmartIKey is null then 0 else 1 end as IsRepeatCall
            FROM RiskBatch rb
            JOIN RiskClaim rc ON rc.RiskBatch_Id = rb.Id
            JOIN Person2Address p2a on p2a.RiskClaim_Id = rc.Id
            WHERE rb.Id = @RiskBatchId
			    AND (rc.SuppliedClaimStatus = 0 or rc.SuppliedClaimStatus = 1 or rc.SuppliedClaimStatus = 4)
				AND rc.ExternalServicesRequestsRequired & @RiskExternalServiceMask = @RiskExternalServiceMask 
				AND p2a.ADARecordStatus = 0 AND p2a.AddressLinkType_Id = 3 AND p2a.LinkConfidence = 0 

	--Clear the service BIT in  the RiskClaim mask
    UPDATE riskclaim SET ExternalServicesRequestsRequired = ExternalServicesRequestsRequired & ~(@RiskExternalServiceMask)
    WHERE riskclaim.RiskBatch_Id = @RiskBatchId

	--------------------End of code for Tracesmart----------------------------

	--------------------Start of code for Comp House---------------------------

	----Read Mask value and service ID for Comp House
	--SELECT @RiskExternalServiceId = Id, @RiskExternalServiceMask = MaskValue 
	--FROM RiskExternalServices res 
	--WHERE res.ServiceName = 'Companies House'

	----Create External Records
 --   INSERT INTO [dbo].[RiskExternalServicesRequests] ([RiskClaim_Id], [RiskExternalService_Id], [Unique_Id], [RequestedBy], [RequestedWhen], [IsRepeatCall])
 --   SELECT  rc.Id AS RiskClaim_Id, 
 --           @RiskExternalServiceId AS RiskExternalService_Id, 
 --           i2o.Id AS Unique_Id,
 --           @RequestedBy AS RequestedBy, 
 --           @RequestedWhen AS RequestedWhen,
	--		0 as IsRepeatCall
 --           FROM RiskBatch rb
 --           JOIN RiskClaim rc ON rc.RiskBatch_Id = rb.Id
 --           JOIN Incident2Organisation i2o on i2o.RiskClaim_Id = rc.Id
 --           WHERE rb.Id = @RiskBatchId 
 --             AND (rc.SuppliedClaimStatus = 0 or rc.SuppliedClaimStatus = 1 or rc.SuppliedClaimStatus = 4)
	--			AND rc.ExternalServicesRequestsRequired & @RiskExternalServiceMask = @RiskExternalServiceMask 		

	----Clear the service BIT in  the RiskClaim mask
    --UPDATE riskclaim SET ExternalServicesRequestsRequired = ExternalServicesRequestsRequired & ~(@RiskExternalServiceMask)
    --WHERE riskclaim.RiskBatch_Id = @RiskBatchId

	----------------End of code for Comp House--------------------------

	----------------Start of code for Cue-------------------------------

	----Read Mask value and service ID for Cue
	--SELECT @RiskExternalServiceId = Id, @RiskExternalServiceMask = MaskValue 
		--FROM RiskExternalServices res 
		--WHERE res.ServiceName = 'Cue'

	----Create External Records
	--INSERT INTO [dbo].[RiskExternalServicesRequests] ([RiskClaim_Id], [RiskExternalService_Id], [Unique_Id], [RequestedBy], [RequestedWhen], [IsRepeatCall])
	--SELECT  rc.Id AS RiskClaim_Id, 
 --           @RiskExternalServiceId AS RiskExternalService_Id, 
 --           p2a.Id AS Unique_Id,
 --           @RequestedBy AS RequestedBy, 
 --           @RequestedWhen AS RequestedWhen,
	--		0
 --           FROM RiskBatch rb
 --           JOIN RiskClaim rc ON rc.RiskBatch_Id = rb.Id
 --           JOIN Person2Address p2a on p2a.RiskClaim_Id = rc.Id
 --           WHERE rb.Id = @RiskBatchId
	--		    AND (rc.SuppliedClaimStatus = 0 or rc.SuppliedClaimStatus = 1 or rc.SuppliedClaimStatus = 4)
	--			AND rc.ExternalServicesRequestsRequired & @RiskExternalServiceMask = @RiskExternalServiceMask 
	--			AND p2a.ADARecordStatus = 0 AND p2a.AddressLinkType_Id = 3 AND p2a.LinkConfidence = 0 

	----Clear the service BIT in  the RiskClaim mask
 --   UPDATE riskclaim SET ExternalServicesRequestsRequired = ExternalServicesRequestsRequired & ~(@RiskExternalServiceMask)
 --   WHERE riskclaim.RiskBatch_Id = @RiskBatchId

	  ------------------End of code for Cue----------------------
RETURN 0
