﻿

CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Person_Dynamic_Linked_Data]
(@RiskClaim_Id INT
,@Vehicle_Id INT)

AS

SELECT B.ColumnName, B.Value
FROM	(
		SELECT   CAST(LinkedNINumber AS VARCHAR(100)) [Linked NI Numbers]
				,CAST(LinkedDrivingLicenceNumber AS VARCHAR(100)) [Linked Driving Licence Numbers]
				,CAST(LinkedPassportNumber AS VARCHAR(100)) [Linked Passport Numbers]
				,CAST(LinkedTaxiDriverLicenceNumber AS VARCHAR(100)) [Linked Taxi Driver Licence Numbers]
				,CAST(LinkedEmailAddress AS VARCHAR(100)) [Linked Email Addresses]
				,CAST(LinkedBankAccount AS VARCHAR(100)) [Linked Bank Accounts]
		FROM dbo.rpt_Level_One_Report_Person
		WHERE RiskClaim_Id = @RiskClaim_Id
		AND Vehicle_Id = @Vehicle_Id
		) A
UNPIVOT (Value FOR ColumnName IN (
								 [Linked NI Numbers]
								,[Linked Driving Licence Numbers]
								,[Linked Passport Numbers]
								,[Linked Taxi Driver Licence Numbers]
								,[Linked Email Addresses]
								,[Linked Bank Accounts]
								)
		) AS B