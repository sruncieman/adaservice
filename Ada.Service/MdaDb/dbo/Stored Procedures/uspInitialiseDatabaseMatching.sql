﻿
CREATE PROCEDURE [dbo].[uspInitialiseDatabaseMatching]


AS

DELETE FROM  dbo.OutcomeCategories
INSERT [dbo].[OutcomeCategories] (CategoryTypeId, CategoryType, CategoryText, RecordStatus, ADARecordStatus)
SELECT CategoryTypeId, CategoryType, CategoryText, RecordStatus, ADARecordStatus
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[OutcomeCategories]
 
TRUNCATE TABLE [dbo].[UKAreaCodes]
INSERT [dbo].[UKAreaCodes] ([AreaCode], [OfcomDefinition], [TraditionalBTDefinition]) 
SELECT [AreaCode], [OfcomDefinition], [TraditionalBTDefinition]
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[UKAreaCodes]
 
TRUNCATE TABLE [dbo].[MatchingRulesAccount] 
INSERT [dbo].[MatchingRulesAccount]  (RuleNo, RiskClaim_Id, AccountNumber, SortCode, BankName, PriorityGroup, MatchType)
SELECT RuleNo, RiskClaim_Id, AccountNumber, SortCode, BankName, PriorityGroup, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesAccount]
 
TRUNCATE TABLE [dbo].[MatchingRulesWebsite]
INSERT INTO [dbo].[MatchingRulesWebsite]
SELECT RuleNo, RiskClaim_Id, URL, PriorityGroup, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesWebsite]

TRUNCATE TABLE [dbo].[MatchingRulesTelephone]
INSERT INTO dbo.MatchingRulesTelephone (RuleNo, RiskClaim_Id, TelephoneNumber, TelephoneType, MatchType)
SELECT RuleNo, RiskClaim_Id, TelephoneNumber, TelephoneType, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesTelephone]

TRUNCATE TABLE [dbo].[MatchingRulesPolicy] 
INSERT INTO [dbo].[MatchingRulesPolicy] (RuleNo, RiskClaim_Id, PolicyNumber, MatchType)
SELECT RuleNo, RiskClaim_Id, PolicyNumber, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesPolicy]

TRUNCATE TABLE [dbo].[MatchingRulesPaymentCard]
INSERT INTO [dbo].[MatchingRulesPaymentCard] (RuleNo, RiskClaim_Id, PaymentCardNumber, SortCode, BankName, ExpiryDate, PriorityGroup, MatchType)
SELECT RuleNo, RiskClaim_Id, PaymentCardNumber, SortCode, BankName, ExpiryDate, PriorityGroup, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesPaymentCard]

TRUNCATE TABLE [dbo].[MatchingRulesPassport]
INSERT INTO [dbo].[MatchingRulesPassport] (RuleNo, RiskClaim_Id, PassportNumber, PriorityGroup, MatchType)
SELECT RuleNo, RiskClaim_Id, PassportNumber, PriorityGroup, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesPassport]
 
TRUNCATE TABLE [dbo].[MatchingRulesNINumber]
INSERT INTO [dbo].[MatchingRulesNINumber] (RuleNo, RiskClaim_Id, NINumber, PriorityGroup, MatchType)
SELECT RuleNo, RiskClaim_Id, NINumber, PriorityGroup, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesNINumber]

TRUNCATE TABLE [dbo].[MatchingRulesIPAddress]
INSERT INTO [dbo].[MatchingRulesIPAddress] (RuleNo, IPAddress, PriorityGroup, MatchType, RiskClaim_Id)
SELECT RuleNo, IPAddress, PriorityGroup, MatchType, RiskClaim_Id
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesIPAddress] 

TRUNCATE TABLE [dbo].[MatchingRulesIncident]
INSERT [dbo].[MatchingRulesIncident] (RuleNo, RiskClaim_Id, IncidentDate, IncidentTime, FirstName, LastName, DateOfBirth, VehicleRegistration, OrganisationName, RegisteredNumber, VATNumber, PriorityGroup, MatchType)
SELECT RuleNo, RiskClaim_Id, IncidentDate, IncidentTime, FirstName, LastName, DateOfBirth, VehicleRegistration, OrganisationName, RegisteredNumber, VATNumber, PriorityGroup, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesIncident] 

TRUNCATE TABLE [dbo].[MatchingRulesEmail]
INSERT INTO dbo.MatchingRulesEmail (RuleNo, RiskClaim_Id, EmailAddress, PriorityGroup, MatchType)
SELECT RuleNo, RiskClaim_Id, EmailAddress, PriorityGroup, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesEmail] 

TRUNCATE TABLE [dbo].[MatchingRulesDrivingLicense]
INSERT INTO [dbo].[MatchingRulesDrivingLicense] (RuleNo, RiskClaim_Id, DriverNumber, PriorityGroup, MatchType)
SELECT RuleNo, RiskClaim_Id, DriverNumber, PriorityGroup, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesDrivingLicense] 

TRUNCATE TABLE [dbo].[MatchingRulesBBPin]
INSERT [dbo].[MatchingRulesBBPin] (RuleNo, RiskClaim_Id, BBPin, DisplayName, PriorityGroup, MatchType)
SELECT RuleNo, RiskClaim_Id, BBPin, DisplayName, PriorityGroup, MatchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].[MatchingRulesBBPin] 

TRUNCATE TABLE dbo.MatchingRulesClaim
INSERT INTO dbo.MatchingRulesClaim (RuleNo, ClientClaimReference, ClientID, IncidentDate, IncidentTime, IncidentLocation, PeopleMatch, VehicleMatch, [PeopleUnconfirmedMatch], [VehicleUnconfirmedMatch], [IncidentCircumstances], PriorityGroup, MatchType, RiskClaimFlag)
SELECT RuleNo, ClientClaimReference, ClientID, IncidentDate, IncidentTime, IncidentLocation, PeopleMatch, VehicleMatch, [PeopleUnconfirmedMatch], [VehicleUnconfirmedMatch], [IncidentCircumstances], PriorityGroup, MatchType, RiskClaimFlag
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].MatchingRulesClaim 
 
TRUNCATE TABLE  [dbo].[PotentialClaimant]
INSERT INTO [dbo].[PotentialClaimant] (PartyType, SubPartyType, Flag, RecordStatus, ADARecordStatus)
SELECT PartyType, SubPartyType, Flag, RecordStatus, ADARecordStatus
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].dbo.PotentialClaimant

TRUNCATE TABLE  [dbo].[RiskWordFilter]
INSERT INTO [dbo].[RiskWordFilter] (RiskClient_Id, TableName, FieldName, LookupWord, ReplacementWord, ReplaceWholeString, DateAdded, SearchType)
SELECT RiskClient_Id, TableName, FieldName, LookupWord, ReplacementWord, ReplaceWholeString, DateAdded, SearchType
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].dbo.[RiskWordFilter]

TRUNCATE TABLE dbo.MatchingRulesAddress
INSERT INTO dbo.MatchingRulesAddress (RuleNo, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode, OrganisationName, PriorityGroup, MatchType, RiskClaimFlag)
SELECT RuleNo, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode, OrganisationName, PriorityGroup, MatchType, RiskClaimFlag
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].MatchingRulesAddress

TRUNCATE TABLE dbo.MatchingRulesOrganisation
INSERT INTO dbo.MatchingRulesOrganisation (RuleNo, OrganisationName, RegisteredNumber, VATNumber, EmailAddress, Website, IPAddress, TelephoneNumber, AccountNumber, PaymentCardNumber, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode, PafUPRN, PriorityGroup, MatchType, RiskClaimFlag)
SELECT RuleNo, OrganisationName, RegisteredNumber, VATNumber, EmailAddress, Website, IPAddress, TelephoneNumber, AccountNumber, PaymentCardNumber, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode,PafUPRN, PriorityGroup, MatchType, RiskClaimFlag
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].MatchingRulesOrganisation

TRUNCATE TABLE dbo.MatchingRulesPerson
INSERT INTO dbo.MatchingRulesPerson (RuleNo, FirstName, LastName, DateOfBirth, NINumber, DriverNumber, PassportNumber, PaymentCardNumber, AccountNumber, PolicyNumber, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode, TelephoneNumber, VehicleRegistration, VehicleVIN, OrganisationName, RegisteredNumber, EmailAddress, VATNumber, PafUPRN, PriorityGroup, MatchType, RiskClaimFlag)
SELECT RuleNo, FirstName, LastName, DateOfBirth, NINumber, DriverNumber, PassportNumber, PaymentCardNumber, AccountNumber, PolicyNumber, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode, TelephoneNumber, VehicleRegistration, VehicleVIN, OrganisationName, RegisteredNumber, EmailAddress, VATNumber, PafUPRN, PriorityGroup, MatchType, RiskClaimFlag
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].MatchingRulesPerson

TRUNCATE TABLE dbo.MatchingRulesVehicles
INSERT INTO dbo.MatchingRulesVehicles (RuleNo, VehicleRegistration, VIN, VehicleMake, Model, Colour, PostCode, BuildingNumber, PafUPRN, PriorityGroup, MatchType, RiskClaimFlag)
SELECT RuleNo, VehicleRegistration, VIN, VehicleMake, Model, Colour, PostCode, BuildingNumber, PafUPRN, PriorityGroup, MatchType, RiskClaimFlag
FROM [UKBOLINTEL-TST3].[MDA-MasterGen].[dbo].MatchingRulesVehicles