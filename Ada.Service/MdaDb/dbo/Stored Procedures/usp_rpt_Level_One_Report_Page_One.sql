﻿
CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Page_One]

	@RiskClaim_Id INT

AS

SET NOCOUNT ON

SELECT   RiskClaimRun.ExecutedWhen AS UploadedDate
		,DATEADD(dd, 1, RiskClaimRun.ExecutedWhen) AS ScoredDate
		,RiskBatch.ClientBatchReference AS BatchRef
		,RiskClaim.ClientClaimRefNumber AS ClaimNo
		,CASE WHEN TotalScore >= RiskClaimRun.RedThreshold THEN 'High' WHEN TotalScore >= RiskClaimRun.AmberThreshold AND TotalScore < RiskClaimRun.RedThreshold THEN 'Medium' WHEN TotalScore >= 0 AND TotalScore < RiskClaimRun.AmberThreshold THEN 'Low' ELSE 'NoScore' END AS RiskRaiting
		,CASE WHEN TotalScore >= RiskClaimRun.RedThreshold THEN 'Red' WHEN TotalScore >= RiskClaimRun.AmberThreshold AND TotalScore < RiskClaimRun.RedThreshold THEN 'Peru' WHEN TotalScore >= 0 AND TotalScore < RiskClaimRun.AmberThreshold THEN 'Green' ELSE 'Red' END AS RiskColour
		,CASE WHEN ExternalServicesRequired & 2 != 0 AND RESR.RiskExternalService_Id =2 AND RESR.ServicesCalled =1 THEN 'Yes' ELSE 'No' END AS CompaniesHouse
		,CASE WHEN ExternalServicesRequired & 1 != 0 AND RESR.RiskExternalService_Id =1 AND RESR.ServicesCalled =1 THEN 'Yes' ELSE 'No' END AS Tracesmart
		,CASE WHEN ExternalServicesRequired & 4 != 0 THEN 'Yes' ELSE 'No' END AS CUE
		,RiskClaimRun.Id
		,RiskClaimRun.RiskClaim_Id
		,RiskClaimRun.EntityType
		,RiskClaimRun.ScoreEntityEncodeFormat
		,RiskClaimRun.ScoreEntityXml
		,RiskClaimRun.SummaryMessagesEncodeFormat 
		,RiskClaimRun.SummaryMessagesXml
		,RiskClaimRun.TotalScore
		,RiskClaimRun.TotalKeyAttractorCount
		,RiskClaimRun.ReportOneVersion
		,RiskClaimRun.ReportTwoVersion
		,RiskClaimRun.ExecutedBy
		,RiskClaimRun.ExecutedWhen
		,RiskClaimRun.LatestVersion
		,ISNULL(RiskClaim.LevelOneRequestedWhen,GETDATE()) LevelOneRequestedWhen
		,ISNULL(RiskClaim.LevelOneRequestedWho,'Automated User') LevelOneRequestedWho
		,RiskClaim.CreatedDate
		,RiskClient.ClientName
FROM dbo.RiskClaimRun 
INNER JOIN dbo.RiskClaim ON RiskClaimRun.RiskClaim_Id = RiskClaim.Id
INNER JOIN dbo.RiskBatch RiskBatch ON RiskBatch.ID = RiskClaim.RiskBatch_Id
LEFT JOIN dbo.RiskClient RiskClient ON RiskClient.ID = RiskBatch.RiskClient_Id
LEFT JOIN (
			SELECT RiskClaim_Id, RiskExternalService_Id, CAST(SUM(ServicesCalledMask) AS BIT) ServicesCalled
			FROM dbo.RiskExternalServicesRequests
			WHERE RiskClaim_Id = @RiskClaim_Id
			GROUP BY RiskClaim_Id, RiskExternalService_Id
			) RESR ON RESR.RiskClaim_Id = RiskClaim.Id
LEFT JOIN dbo.RiskExternalServicesRequests RESR2 ON RESR2.RiskClaim_Id = RiskClaim.Id AND RESR2.RiskExternalService_Id = 2 --Companies House
WHERE RiskClaimRun.RiskClaim_Id  = @RiskClaim_Id