﻿


CREATE PROCEDURE [dbo].[uspGetMatchingClaim_Old]

	@VehicleList VARCHAR(254), 
	@Date AS DATE

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


	SELECT TOP 1 Incident_Id FROM
		(
		SELECT  I2V.Incident_Id, COUNT(DISTINCT I2V.Vehicle_Id) VehicleCount
		FROM  Incident2Vehicle I2V
		INNER JOIN
		Incident I ON I2V.Incident_Id = I.Id 
		WHERE I2V.ADARecordStatus = 0 
			AND I.ADARecordStatus = 0 
			AND I2V.Vehicle_Id IN(SELECT Item FROM dbo.SplitStrings_XML(@VehicleList, ', ')) 
			AND I.IncidentDate = @Date
		GROUP BY I2V.Incident_Id
		HAVING COUNT(DISTINCT I2V.Vehicle_Id) > 1
		) AS A ORDER BY VehicleCount DESC























--SELECT ID AS VehicleID, COUNT(*)  AS VehicleCount FROM (
	--SELECT V.Id, I.Source, ROW_NUMBER() OVER(PARTITION BY I.Source, V.Id ORDER BY I.Source) AS RN
	--FROM         Incident2Vehicle I2V INNER JOIN
	--					  Incident I ON I2V.Incident_Id = I.Id INNER JOIN
	--					  Vehicle V ON I2V.Vehicle_Id = V.Id
	--WHERE I.IncidentDate < @Date AND V.Id IN(SELECT Item FROM dbo.SplitStrings_XML(@VehicleList, ', '))
--) A
--GROUP BY ID, RN
--HAVING COUNT(*) > 1