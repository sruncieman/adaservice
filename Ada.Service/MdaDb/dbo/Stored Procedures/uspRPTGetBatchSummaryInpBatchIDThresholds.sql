﻿CREATE PROCEDURE [dbo].[uspRPTGetBatchSummaryInpBatchIDThresholds]
(
 @BatchId		INT	
,@RedClaim		INT	
,@AmberClaim	INT	
)
AS
SET NOCOUNT ON

DECLARE @PersonsInBatch INT
DECLARE @T TABLE (BatchID INT, TotalIDV INT, NewIDV INT, UpdatedIDV INT, RecordsExcluded INT)

SELECT @PersonsInBatch = ISNULL(COUNT(1),0)
FROM dbo.RiskBatch RB
INNER JOIN dbo.RiskClaim RC ON RB.Id = RC.RiskBatch_Id
INNER JOIN dbo.RiskClaimRun RCR ON RCR.RiskClaim_Id = RC.ID
INNER JOIN dbo.Incident I ON I.Id = RC.Incident_Id
INNER JOIN dbo.Incident2Person I2P ON I2P.Incident_Id = I.Id
WHERE RB.Id = @BatchId
AND RCR.LatestVersion = 1

INSERT INTO @T (BatchID,  TotalIDV, NewIDV, UpdatedIDV, RecordsExcluded)
SELECT   RB.Id BatchID 
		,SUM(CASE WHEN IsRepeatCall IS NOT NULL THEN 1 ELSE 0 END) TotalIDV
		,ISNULL(SUM(CASE WHEN IsRepeatCall = 0 THEN 1 ELSE 0 END),0) NewIDV
		,ISNULL(SUM(CASE WHEN IsRepeatCall = 1 THEN 1 ELSE 0 END),0) UpdatedIDV
		,ISNULL(SUM(CASE WHEN CallStatus = -6 THEN 1 ELSE 0 END),0) RecordsExcluded
FROM dbo.RiskBatch RB
INNER JOIN dbo.RiskClaim RC ON RB.Id = RC.RiskBatch_Id
INNER JOIN dbo.RiskClaimRun RCR ON RCR.RiskClaim_Id = RC.ID
INNER JOIN dbo.RiskExternalServicesRequests RESR ON RESR.RiskClaim_Id = RC.ID
WHERE RB.Id = @BatchID
AND RCR.LatestVersion = 1
AND RESR.RiskExternalService_Id = 1
AND ISNULL(ServicesCalledMask,0) > 0
GROUP BY RB.Id 

SELECT	 RB.ID BatchID
		,RB.TotalClaimsReceived
		,RB.TotalNewClaims
		,RB.TotalModifiedClaims
		,RB.TotalClaimsSkipped
		,RB.TotalClaimsInError
		,RB.TotalClaimsLoaded
		,ISNULL(RB.TotalClaimsLoaded - (IQ.RedClaims + IQ.AmberClaims + IQ.GreenClaims),0)  TotalClaimsWithKeoghs
		,IQ.TotalPreliminaryClaims
		,ISNULL(IQ.TotalReserve,0) TotalReserve
		,ISNULL(IQ.RedClaims,0) RedClaims
		,ISNULL(IQ.RedReserves,0) RedReserves
		,ISNULL(IQ.AmberClaims,0) AmberClaims
		,ISNULL(IQ.AmberReserves,0) AmberReserves
		,ISNULL(IQ.GreenClaims,0) GreenClaims
		,ISNULL(IQ.GreenReserves,0) GreenReserves
		,@PersonsInBatch PersonsInBatch
		,ISNULL(T.TotalIDV,0) TotalIDV
		,ISNULL(T.NewIDV,0) NewIDV
		,ISNULL(T.UpdatedIDV,0) UpdatedIDV
		,ISNULL(T.RecordsExcluded,0) + (@PersonsInBatch - (ISNULL(T.NewIDV,0) + ISNULL(T.UpdatedIDV,0)))  RecordsExcluded
FROM dbo.RiskBatch RB
LEFT JOIN (
			SELECT	 RB.Id
					,SUM(CASE WHEN RCR.FinalScoreStatus = 0 THEN 1 ELSE 0 END) TotalPreliminaryClaims
					,ISNULL(SUM(I.Reserve),0) TotalReserve
					,SUM(CASE WHEN RCR.TotalScore >= @RedClaim THEN 1 ELSE 0 END) RedClaims
					,SUM(CASE WHEN RCR.TotalScore >= @RedClaim THEN I.Reserve ELSE 0 END) RedReserves
					,SUM(CASE WHEN RCR.TotalScore >= @AmberClaim AND RCR.TotalScore < @RedClaim THEN 1 ELSE 0 END) AmberClaims
					,SUM(CASE WHEN RCR.TotalScore >= @AmberClaim AND RCR.TotalScore < @RedClaim THEN I.Reserve ELSE 0 END) AmberReserves
					,SUM(CASE WHEN RCR.TotalScore < @AmberClaim THEN 1 ELSE 0 END) GreenClaims
					,SUM(CASE WHEN RCR.TotalScore < @AmberClaim THEN I.Reserve ELSE 0 END) GreenReserves
			FROM dbo.RiskBatch RB
			INNER JOIN dbo.RiskClaim RC ON RB.Id = RC.RiskBatch_Id
			INNER JOIN dbo.RiskClaimRun RCR ON RCR.RiskClaim_Id = RC.ID
			INNER JOIN dbo.Incident I ON I.Id  = RC.Incident_Id
			WHERE RB.Id = @BatchID
			AND RCR.LatestVersion = 1
			GROUP BY RB.Id
			) IQ ON IQ.Id = RB.ID
LEFT JOIN @T T ON T.BatchID = RB.ID
WHERE RB.Id = @BatchID