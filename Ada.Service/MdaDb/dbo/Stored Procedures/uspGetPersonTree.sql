﻿CREATE PROCEDURE [dbo].[uspGetPersonTree]

	 @Id INT,
	 @includeConfirmed BIT,
	 @includeUnconfirmed BIT,
	 @includeTentative BIT,
	 @includeThis BIT

AS

	SELECT ID FROM dbo.fn_GetPersonTree(@Id, @includeConfirmed, @includeUnconfirmed, @includeTentative, @includeThis)