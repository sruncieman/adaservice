﻿


 
 CREATE PROCEDURE [dbo].[uspPersonSettledCaseCountMessages]
 
	 @PersonId INT,
	 @IncidentID INT,
	 @includeThisPersonId BIT,
	 @IncludeConfirmed BIT ,
	 @IncludeUnconfirmed BIT,
	 @IncludeTentative BIT,
	 @StartDate SMALLDATETIME,
	 @EndDate SMALLDATETIME,
	 @OutcomeCategoryId INT,
  	 @IsPotentialClaimant BIT  = NULL
 
 AS

--IF @IsPotentialClaimant = 0
--		SELECT COUNT(*) OUT_Count FROM (
--			SELECT MOR.Id, MOR.Person_Id, MOR.Incident_Id FROM Incident2PersonOutcome MOR 
--			INNER JOIN OutcomeCategories OC 
--			ON MOR.MannerOfResolution = OC.CategoryText
--			WHERE Person_Id IN(SELECT Id FROM dbo.fn_GetPersonTree (@personId, @includeConfirmed ,@includeUnconfirmed, @includeTentative, @includeThisPersonId))
									
--		) A
--		INNER JOIN
--		Incident2Person B
--		ON A.Incident_Id = B.Incident_Id 
--		AND A.Person_Id = B.Person_Id
--		WHERE 
--			(PartyType_Id = 3 AND SubPartyType_Id IN (0, 1, 3))
--			OR (PartyType_Id IN (5 ,9))

--ELSE IF @IsPotentialClaimant = 1
--		SELECT COUNT(*) OUT_Count FROM (
--			SELECT MOR.Id, MOR.Person_Id, MOR.Incident_Id FROM Incident2PersonOutcome MOR 
--			INNER JOIN OutcomeCategories OC 
--			ON MOR.MannerOfResolution = OC.CategoryText
--			WHERE Person_Id IN(SELECT Id FROM dbo.fn_GetPersonTree (@personId, @includeConfirmed ,@includeUnconfirmed, @includeTentative, @includeThisPersonId) )
									
		--) A
		--INNER JOIN
		--Incident2Person B
		--ON A.Incident_Id = B.Incident_Id 
		--AND A.Person_Id = B.Person_Id
		--WHERE 
		--	(PartyType_Id != 3 AND SubPartyType_Id NOT IN (0, 1, 3))
		--	OR (PartyType_Id NOT IN (5 ,9))
			
--ELSE



SELECT 
          PT.PartyTypeText, SPT.SubPartyText, I.IncidentDate, REPLACE(CT.ClaimType, 'RTA - ', '') ClaimType, KC.KeoghsEliteReference, I2P.FiveGrading AS  I2P_FiveGrading,
          MOR.MannerOfResolution, MOR.FiveGrading AS  MOR_FiveGrading
          ,CASE WHEN @includeUnconfirmed = 1 THEN  S.Text ELSE NULL END AS Salutation,
          CASE WHEN @includeUnconfirmed = 1 THEN  P.FirstName ELSE NULL END AS FirstName,
          CASE WHEN @includeUnconfirmed = 1 THEN  P.LastName ELSE NULL END AS LastName,
          CASE WHEN @includeUnconfirmed = 1 THEN  P.DateOfBirth ELSE NULL END AS DateOfBirth

	FROM Incident2PersonOutcome MOR 
	INNER JOIN OutcomeCategories OC 
		ON MOR.MannerOfResolution = OC.CategoryText
	INNER JOIN 
	 Incident2Person AS I2P
	 ON MOR.Person_Id = I2p.Person_Id
	 AND MOR.Incident_Id = I2p.Incident_Id
	INNER JOIN Person P
	ON MOR.Person_Id = P.Id
	INNER JOIN PartyType PT
	ON I2P.PartyType_Id = PT.Id
	INNER JOIN SubPartyType SPT
	ON I2P.SubPartyType_Id = SPT.Id
	INNER JOIN Incident I 
	ON I2p.Incident_Id = I.Id
	AND MOR.Incident_Id = I.Id
	INNER JOIN ClaimType CT
	ON I.ClaimType_Id = CT.Id
	INNER JOIN Salutation S 
	ON P.Salutation_Id = S.Id
	INNER JOIN KeoghsCase2Incident AS I2C 
	ON I2P.Incident_Id = I2C.Incident_Id
	AND MOR.Incident_Id = I2C.Incident_Id
	INNER JOIN KeoghsCase KC 
			ON KC.Id = I2c.KeoghsCase_Id
	
	
	
	WHERE MOR.Person_Id IN(SELECT Id FROM dbo.fn_GetPersonTree(@personId, @includeConfirmed ,@includeUnconfirmed, @includeTentative, @includeThisPersonId) 
	) AND MOR.Incident_ID  != @IncidentID AND OC.CategoryTypeId = @OutcomeCategoryId