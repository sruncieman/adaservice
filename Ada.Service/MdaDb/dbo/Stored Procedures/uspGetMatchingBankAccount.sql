﻿
CREATE PROCEDURE [dbo].[uspGetMatchingBankAccount]

 @RiskClaim_Id INT = NULL,
 @AccountNumber VARCHAR(50) = NULL,
 @SortCode VARCHAR(50) = NULL,
 @BankName VARCHAR(50)  = NULL,
 @FieldsFlag BIT OUTPUT 

AS



SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET @FieldsFlag = 0


DECLARE @T AS TABLE(TID INT NOT NULL IDENTITY(1,1), RuleNo INT, PriorityGroup INT)
DECLARE @Ret TABLE(ID INT, RuleNo INT, PriorityGroup INT, MatchType INT)

INSERT INTO @T(RuleNo, PriorityGroup)
SELECT RuleNo, PriorityGroup
FROM dbo.MatchingRulesAccount
WHERE	ISNULL( RiskClaim_Id, 0)  <= ISNULL(LEN(CONVERT(VARCHAR(10), @RiskClaim_Id)), 0 )
	AND	ISNULL(AccountNumber, 0 ) <= ISNULL(LEN(@AccountNumber), 0 )
	AND	ISNULL(SortCode, 0 ) <= ISNULL(LEN(@SortCode), 0)
	AND	ISNULL(BankName, 0 ) <= ISNULL(LEN(@BankName), 0)
ORDER BY PriorityGroup
DECLARE @ROWCOUNT INT = @@ROWCOUNT	

DECLARE @SQL NVARCHAR(MAX) 
DECLARE @EXEC_SQL NVARCHAR(MAX) = ''
DECLARE @PriorityGroup INT
DECLARE @RuleNo INT

IF @ROWCOUNT = 0 
	GOTO MatchEnd
ELSE
	SET @FieldsFlag = 1
	
	DECLARE @Count INT = 1

WHILE @Count <= @ROWCOUNT
BEGIN

SELECT @RuleNo = RuleNo, @PriorityGroup = PriorityGroup FROM @T WHERE TID = @Count
SET @SQL = dbo.fnGetMatchingBankAccountTSql(@RuleNo)

	SET @Count += 1
	
	SET @EXEC_SQL += @SQL + CHAR(10) + 'AND (A.ADARecordStatus = 0) '
	
	IF EXISTS (SELECT * FROM @T WHERE TID = @Count AND PriorityGroup = @PriorityGroup) 
	BEGIN
		SET @EXEC_SQL += CHAR(10) + 'UNION ALL ' + CHAR(10)
	END
	ELSE
	BEGIN
		PRINT @EXEC_SQL
		PRINT ''

		PRINT '-----------------------------------------------------------------------------------'
		INSERT INTO @Ret(ID, RuleNo, PriorityGroup, MatchType)
		
		EXEC sp_executesql @EXEC_SQL ,
			N'@RiskClaim_Id INT,  @AccountNumber VARCHAR(50), @SortCode VARCHAR(50), @BankName VARCHAR(50)',
			@RiskClaim_Id = @RiskClaim_Id, @AccountNumber = @AccountNumber, @SortCode = @SortCode, @BankName = @BankName	
		
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
		SET @EXEC_SQL = ''
	END
END


MatchEnd:

IF EXISTS (SELECT * FROM @Ret WHERE MatchType = 0)
	SELECT TOP 1 ID, RuleNo, PriorityGroup, MatchType FROM @Ret ORDER BY ID
ELSE
	--SELECT DISTINCT ID, RuleNo, PriorityGroup, MatchType FROM @Ret
	SELECT ID, MIN(RuleNo) AS RuleNo, PriorityGroup, MatchType FROM @Ret GROUP BY ID, PriorityGroup, MatchType