﻿CREATE PROCEDURE [dbo].[spClientLoadRowCount]
	@BatchNo INT,
	@Client INT
AS
BEGIN

	INSERT INTO CH1_Analysis(ClientID,tableName,[RowCount],BatchNo)
	SELECT DISTINCT @Client,tbl.name, ption.rows ,@BatchNo 
	FROM MDA.sys.partitions ption
		Inner join MDA.sys.tables tbl
			on ption.object_id = tbl.object_id
			and tbl.type ='U'
		
END