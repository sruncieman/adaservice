﻿/****** Object:  StoredProcedure [dbo].[usp_rpt_Level_One_Report_Person_Organisation_Dynamic_Linked_Data]    Script Date: 02/11/2014 16:51:05 ******/
	
CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Person_Organisation_Dynamic_Linked_Data]
(@RiskClaim_Id INT
,@Person_ID INT = NULL
,@Organisation_ID INT = NULL)

AS

DECLARE  @ParentName VARCHAR(20) = CASE WHEN @Person_ID IS NOT NULL THEN 'PersonRisk' ELSE 'Incident' END

SELECT B.ColumnName, B.Value
FROM	(
		SELECT   CAST(LinkedEmail AS VARCHAR(500)) [Linked Email(s)]
				,CAST(LinkedTradingName AS VARCHAR(500)) [Linked Trading Name(s)]
				,CAST(LinkedFormerName AS VARCHAR(500)) [Linked Former Name(s)]

		FROM dbo.rpt_Level_One_Report_Organisation
		WHERE RiskClaim_Id = @RiskClaim_Id 
		AND (Per_Dbid = @Person_ID OR @Person_ID IS NULL)
		AND (Org_DbId = @Organisation_ID OR @Organisation_ID IS NULL)
		AND ParentName = @ParentName
		) A
UNPIVOT (Value FOR ColumnName IN (
								 [Linked Email(s)]
								,[Linked Trading Name(s)]
								,[Linked Former Name(s)]
								)
		) AS B