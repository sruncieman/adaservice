﻿
CREATE PROCEDURE [dbo].[uspGetMatchingPaymentCard]
	
	@RiskClaim_Id INT,
	@PaymentCardNumber VARCHAR(16),
	@SortCode   VARCHAR(8),
	@BankName  VARCHAR(50),
	@ExpiryDate  VARCHAR(4),
	@FieldsFlag BIT OUTPUT

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @FieldsFlag = 1


SET NOCOUNT ON
SET @FieldsFlag = 0


DECLARE @T AS TABLE(TID INT NOT NULL IDENTITY(1,1), RuleNo INT, PriorityGroup INT)
DECLARE @Ret TABLE(ID INT, RuleNo INT, PriorityGroup INT, MatchType INT)

INSERT INTO @T(RuleNo, PriorityGroup)
SELECT RuleNo, PriorityGroup
FROM dbo.MatchingRulesPaymentCard
WHERE	ISNULL( RiskClaim_Id, 0)  <= ISNULL(LEN(CONVERT(VARCHAR(10), @RiskClaim_Id)), 0 )
	AND	ISNULL(PaymentCardNumber, 0 ) <= ISNULL(LEN(@PaymentCardNumber), 0 )
	AND	ISNULL(SortCode, 0 ) <= ISNULL(LEN(@SortCode), 0)
	AND	ISNULL(BankName, 0 ) <= ISNULL(LEN(@BankName), 0)
	AND	ISNULL(ExpiryDate, 0 ) <= ISNULL(LEN(@ExpiryDate), 0)
ORDER BY PriorityGroup
DECLARE @ROWCOUNT INT = @@ROWCOUNT	

DECLARE @SQL NVARCHAR(MAX) 
DECLARE @EXEC_SQL NVARCHAR(MAX) = ''
DECLARE @PriorityGroup INT
DECLARE @RuleNo INT

IF @ROWCOUNT = 0 
	GOTO MatchEnd
ELSE
	SET @FieldsFlag = 1
	
	DECLARE @Count INT = 1

WHILE @Count <= @ROWCOUNT
BEGIN

SELECT @RuleNo = RuleNo, @PriorityGroup = PriorityGroup FROM @T WHERE TID = @Count
SET @SQL = dbo.fnGetMatchingPaymentCardTSql(@RuleNo)

	SET @Count += 1
	
	SET @EXEC_SQL += @SQL + CHAR(10) + 'AND (A.ADARecordStatus = 0) '
	
	IF EXISTS (SELECT * FROM @T WHERE TID = @Count AND PriorityGroup = @PriorityGroup) 
	BEGIN
		SET @EXEC_SQL += CHAR(10) + 'UNION ALL ' + CHAR(10)
	END
	ELSE
	BEGIN
		PRINT @EXEC_SQL
		PRINT ''

		PRINT '-----------------------------------------------------------------------------------'
		INSERT INTO @Ret(ID, RuleNo, PriorityGroup, MatchType)
		
		EXEC sp_executesql @EXEC_SQL ,
			N'@RiskClaim_Id INT,  @PaymentCardNumber VARCHAR(16), @SortCode   VARCHAR(8), @BankName  VARCHAR(50), @ExpiryDate  VARCHAR(4)',
			@RiskClaim_Id = @RiskClaim_Id, @PaymentCardNumber = @PaymentCardNumber, @SortCode  = @SortCode, @BankName  = @BankName, @ExpiryDate  = @ExpiryDate
		
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
		SET @EXEC_SQL = ''
	END
END


MatchEnd:


IF EXISTS (SELECT * FROM @Ret WHERE MatchType = 0)
	SELECT TOP 1 ID, RuleNo, PriorityGroup, MatchType FROM @Ret ORDER BY ID
ELSE
	--SELECT DISTINCT ID, RuleNo, PriorityGroup, MatchType FROM @Ret
	SELECT ID, MIN(RuleNo) AS RuleNo, PriorityGroup, MatchType FROM @Ret GROUP BY ID, PriorityGroup, MatchType