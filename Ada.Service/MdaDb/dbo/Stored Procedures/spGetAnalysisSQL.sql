﻿
CREATE PROCEDURE [dbo].[spGetAnalysisSQL](@ClientID INT)
AS
BEGIN

	DECLARE @SQLstring Nvarchar(MAX)
	
	DECLARE @lastBatch INT ,@PenultimateBatch INT
	DECLARE @batch TABLE(Client INT ,BatchNo INT)

	INSERT INTO @batch(Client, BatchNo)
	SELECT DISTINCT ClientID,BatchNo
	FROM [CH1_Analysis]
	WHERE ClientID = @ClientID
	
	SELECT @lastBatch = MAX(BatchNo)FROM @batch
	SELECT @PenultimateBatch =MAX(BatchNo) FROM @batch WHERE BatchNo <> @LastBatch
	
	SET @SQLstring='SELECT [Tablename],[ClientID],' +CHAR(13)

	SELECT @SQLString += 'SUM(Case when BatchNo = '+ CAST(BatchNo as VARCHAR)  
					  +' THEN [RowCount] END) as Batch_' + CAST(BatchNo as VARCHAR) + ', ' +CHAR(13)
				  
	FROM @Batch

	IF @PenultimateBatch >0
	BEGIN
		SELECT @SQLString += 'SUM(Case when BatchNo = '+ CAST(@lastBatch as VARCHAR(30))  
						  +' THEN [RowCount] END) - SUM(Case when BatchNo = '+ CAST(@PenultimateBatch as VARCHAR(30))  
						  +' THEN [RowCount] END) as [Rows Added]' +CHAR(13)
						  
		SET @SQLstring = LEFT(@SQLString,LEN(@SQLString)-1) +CHAR(13) 
					 + 'FROM [MDA].[dbo].[CH1_Analysis] ' + CHAR(13)
					 + 'WHERE clientID = ' + CAST(@ClientID as VARCHAR) +CHAR(13) 
					 + 'GROUP BY [Tablename],[ClientID]'
	END	
	ELSE
	BEGIN
		SET @SQLstring = LEFT(@SQLString,LEN(@SQLString)-3) +CHAR(13) 
					 + 'FROM [MDA].[dbo].[CH1_Analysis] ' + CHAR(13)
					 + 'WHERE clientID = ' + CAST(@ClientID as VARCHAR) +CHAR(13) 
					 + 'GROUP BY [Tablename],[ClientID]'
	
	
	END		  

	EXEC sp_ExecuteSQL @SQLstring
END