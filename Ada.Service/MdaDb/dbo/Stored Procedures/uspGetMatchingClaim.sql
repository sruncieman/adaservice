﻿
CREATE PROCEDURE [dbo].[uspGetMatchingClaim]
	@XML XML,
	@FieldsFlag BIT OUTPUT 
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
 
IF @XML IS NULL
BEGIN
	SELECT CAST(NULL AS INT) ID, CAST(NULL AS INT)  RuleNo, CAST(NULL AS INT)  PriorityGroup, CAST(NULL AS INT)  MatchType 
	RETURN
END
 
--DECLARE @XML XML = '<Claim><RiskClaim_ID></RiskClaim_ID><ClientClaimReference></ClientClaimReference><ClientID></ClientID><IncidentDate></IncidentDate><IncidentTime></IncidentTime><IncidentLocation></IncidentLocation><Vehicles><Vehicle></Vehicle></Vehicles><People><Person></Person></People><IncidentCircumstances></IncidentCircumstances></Claim>'
--DECLARE	@FieldsFlag BIT 

--XML DEFINITION NEEDED TO ACCOUNT FOR MISSING TAGS
DECLARE @XML_DEF VARCHAR(MAX) ='<Claim><RiskClaim_ID></RiskClaim_ID><ClientClaimReference></ClientClaimReference><ClientID></ClientID><IncidentDate></IncidentDate><IncidentTime></IncidentTime><IncidentLocation></IncidentLocation><Vehicles><Vehicle></Vehicle></Vehicles><People><Person></Person></People><IncidentCircumstances></IncidentCircumstances></Claim>'
DECLARE @XML_Check XML = '<Data>' +  REPLACE(CONVERT(VARCHAR(MAX), @XML), 'UNKNOWN', '') +  @XML_DEF + '</Data>'
SET @FieldsFlag = 0 --RETURN VALUE 0 IF NOT ENOUGHT DATA SUPPLIED
DECLARE @T AS TABLE(TID INT NOT NULL IDENTITY(1,1), RuleNo INT, PriorityGroup INT, RiskClaimFlag INT, HasRiskClaim_Id BIT)
DECLARE @Ret TABLE(ID INT, RuleNo INT, PriorityGroup INT, MatchType INT)

--CREATING TABLE OF POSSABLE RULES TO MATCH
INSERT INTO @T(RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id)
SELECT RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id FROM dbo.fnGetMatchingClaimToProcess(@XML_Check) ORDER BY PriorityGroup ,RuleNo
DECLARE @ROWCOUNT INT = @@ROWCOUNT	
IF @ROWCOUNT = 0  --NOT ENOUGHT DATA SUPPLIED EXIT PROCEDURE
	GOTO MatchEnd
ELSE
	SET @FieldsFlag = 1
----------------------------------------------
--GetThePeopleAndVehicleIDsAndConfirmedLinks
----------------------------------------------
--GetAllThePeopleIDsFromTheXMLAndConfirmedLinks
DECLARE @Per TABLE (ID INT IDENTITY(1,1), PerID INT)

IF (SELECT OBJECT_ID('tempdb..#PerAll')) IS NOT NULL
	DROP TABLE #PerAll
CREATE TABLE #PerAll  (ID INT IDENTITY(1,1), PerID INT, LinkConfidence TINYINT)

INSERT INTO @Per (PerID)
SELECT CONVERT(VARCHAR(50), PEO.Data.query('data(.)')) PersonID
FROM @XML.nodes('//Claim/People/Person') PEO(Data)
DECLARE @PerCount INT = @@ROWCOUNT
DECLARE @C INT = 1

IF @PerCount > 0
BEGIN
	DECLARE @PerID INT
	WHILE @C <= @PerCount
		BEGIN
			SELECT @PerID = PerID FROM @Per WHERE ID = @C
			INSERT INTO #PerAll (PerID, LinkConfidence)
			SELECT ID, 0
			FROM dbo.fn_GetPersonTree(@PerID,1,0,0,1)
			UNION ALL 
			SELECT ID, 1
			FROM dbo.fn_GetPersonTree(@PerID,0,1,0,0)

			SELECT @C +=1
		END
END

--GetAllTheVecIDsFromTheXMLAndConfirmedLinks
DECLARE @Vec TABLE (ID INT IDENTITY(1,1), VecID INT)

IF (SELECT OBJECT_ID('tempdb..#VecAll')) IS NOT NULL
	DROP TABLE #VecAll
CREATE TABLE #VecAll (ID INT IDENTITY(1,1), VecID INT, LinkConfidence TINYINT)

INSERT INTO @Vec (VecID)
SELECT CONVERT(varchar(50), VEH.Data.query('data(.)')) VehicleID
FROM @XML.nodes('//Claim/Vehicles/Vehicle') VEH(Data)
DECLARE @VecCount INT = @@ROWCOUNT
SELECT @C = 1

IF @VecCount > 0
BEGIN
	DECLARE  @VecID INT
	WHILE @C <= @VecCount
		BEGIN
			SELECT @VecID = VecID FROM @Vec WHERE ID = @C
			INSERT INTO #VecAll (VecID, LinkConfidence)
			SELECT ID, 0
			FROM [dbo].[fn_GetVehicleTree] (@VecID,1,0,0,1)
			UNION ALL
			SELECT ID, 1
			FROM [dbo].[fn_GetVehicleTree] (@VecID,0,1,0,0)

			SELECT @C +=1
		END
END

--LOOP EACH RULE, EXIT WHEN A MATCH IS FOUND
DECLARE @RuleNo INT
DECLARE @PriorityGroup INT
DECLARE @RiskClaimFlag TINYINT
DECLARE @ExecWithRcId BIT
DECLARE @ExecWithOutRcId BIT
DECLARE @Count INT = 1
DECLARE @SQL NVARCHAR(MAX)

WHILE @Count <= @ROWCOUNT
BEGIN

	SELECT	@RuleNo			= RuleNo,	
			@PriorityGroup	= PriorityGroup,	
			@RiskClaimFlag	= RiskClaimFlag,
			@ExecWithRcId	= CASE WHEN HasRiskClaim_Id = 1 AND RiskClaimFlag > 0 THEN 1 ELSE 0 END,
			@ExecWithOutRcId= CASE WHEN RiskClaimFlag = 0 OR RiskClaimFlag = 2 THEN 1 ELSE 0 END
	FROM @T 
	WHERE TID = @Count
	
	--PROCESS ALL TENTATIVE (Risk Claim Flag) UNION SAME RULE GROUPS 
	IF @RiskClaimFlag = 0
	BEGIN
		
		DECLARE @GrpCount INT = @Count
		DECLARE @GrpCountEnd INT = (SELECT MAX(TID) FROM @T WHERE PriorityGroup = (SELECT PriorityGroup FROM @T WHERE TID = @Count) AND RiskClaimFlag = 0 AND TID >= @Count)
		SET @SQL = dbo.fnGetMatchingClaimTSql(@RuleNo, 0) 
		
		WHILE @GrpCount < @GrpCountEnd
		BEGIN
			SET @GrpCount += 1
			SELECT @RuleNo = RuleNo FROM @T WHERE TID = @GrpCount
			SET @SQL +=   CHAR(10) + 'UNION ALL' + CHAR(10) + dbo.fnGetMatchingClaimTSql(@RuleNo, 0)

		END
			PRINT '################################# RiskClaimFlag = 0 START #######################################'
			PRINT ''
			PRINT @SQL
			PRINT ''
			PRINT '################################# RiskClaimFlag = 0 END #########################################'
		INSERT INTO @Ret
		EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
		IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
		
		SET @Count = @GrpCountEnd
		
	END
	ELSE
	BEGIN
		--PROCESS ALL CONF (Risk Claim Flag) If we have a RC_ID 
		IF @ExecWithRcId = 1
		BEGIN
			SET @SQL = dbo.fnGetMatchingClaimTSql(@RuleNo, 1)
			PRINT ''
			PRINT '------------------------------------WithRcId-------------------------------------------'
			PRINT @SQL
			PRINT ''
			PRINT ''
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
				
		END
	
		--PROCESS ALL UN-CONF (Risk Claim Flag) 
		IF @ExecWithOutRcId = 1
		BEGIN
			PRINT ''
			PRINT '----------------------------------WithOutRcId------------------------------------------'

			SET @SQL = dbo.fnGetMatchingClaimTSql(@RuleNo, 0)
			PRINT @SQL
			PRINT ''
			PRINT ''
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
			
		END

	END
	
SET @Count += 1

END 

MatchEnd:
	
SELECT ID, MIN(RuleNo) AS RuleNo, PriorityGroup, MatchType FROM @Ret 
GROUP BY ID, PriorityGroup, MatchType 
ORDER BY RuleNo DESC