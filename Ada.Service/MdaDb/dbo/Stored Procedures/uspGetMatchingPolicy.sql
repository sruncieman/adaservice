﻿
CREATE PROCEDURE [dbo].[uspGetMatchingPolicy]
	
	@RiskClaim_Id INT = NULL,
	@PolicyNumber VARCHAR(50) = NULL,
	@FieldsFlag BIT OUTPUT 

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @FieldsFlag = 1

SELECT ID, 0 RuleNo, 1 PriorityGroup, 0 MatchType FROM dbo.Policy WHERE ADARecordStatus = 0 AND PolicyNumber = NULLIF(@PolicyNumber,'')