﻿CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Person_Tracesmart]

	@RiskClaim_Id INT,
	@Person_Id INT

AS


DECLARE @Message_0 VARCHAR(50) = 'Status Not Available'	
DECLARE @Message_1 VARCHAR(50) = 'Status Pass'	
DECLARE @Message_2 VARCHAR(50) = 'Status Unknown'	
DECLARE	@Message_3 VARCHAR(50) = 'Status Fail'		

SELECT  DISTINCT
	FullName + ' DOB: '  + CONVERT(VARCHAR(10), DateOfBirth, 103) AS NameAndDOB,
	dbo.fnFormatAddress (A.SubBuilding, NULL, A.BuildingNumber, A.Street, A.Locality, A.Town, A.County, A.Postcode) AS Address_,
	@Person_Id AS Person_Id,
	
	'Unknown' AS AddressSources,

	
	2 AS AddressSourcesCheck,
	'Unknown' AS AddressGoneAway,
	2 AS AddressGoneAwayCheck,
	'Verification Count: ?'   AS VerificationExperianDOB,
	
	ExperianDOB AS VerificationExperianDOBCheck,
	'Verification Count: ?'  AS VerificationTracemartDOB,
	2 VerificationTracemartDOBCheck,
	
	'Land Line ' +
	CASE LandlineValidated 
		WHEN 0 THEN @Message_0
		WHEN 1 THEN @Message_1
		WHEN 2 THEN @Message_2
		WHEN 3 THEN @Message_3
	END 
	AS LandlineStatus,
	LandlineValidated AS LandlineStatusCheck,
	
	
	'Mobile ' +
	CASE  MobileStatus
		WHEN 0 THEN @Message_0
		WHEN 1 THEN @Message_1
		WHEN 2 THEN @Message_2
		WHEN 3 THEN @Message_3
	END 
	AS MobileStatus,
	MobileStatus AS MobileStatusCheck,
	
	
	@Message_2 AS MobileCurrentLocation,
	MobileCurrentLocation AS  MobileCurrentLocationCheck,
	@Message_2 AS SanctionDate,
	2 SanctionDateCheck,
	@Message_2 AS SanctionSource,
	2 AS SanctionSourceCheck,
	@Message_2 AS CCJDate,
	2 CCJDateCheck,
	@Message_2 AS CCJType,
	2 AS CCJTypeCheck,
	@Message_2 AS CCJAmount,
	2 CCJAmountCheck,
	@Message_2 AS CCJEndDate,
	2 CCJEndDateCheck,
 	@Message_2 AS InsolvencyStartDate,
	2 InsolvencyStartDateCheck,
	@Message_2 AS InsolvencyStatus,
	2 AS InsolvencyStatusCheck,
	CreditIvaCheck AS IDVCheck,
	2 AS ElectoralRoll,
	2 AS TelephoneDirectory,
	2 AS CreditActive,
	ExperianDOB AS DOBVerification,
	MobileStatus AS MobileVerification,
	2 PEPAndSanction,
	2 Mortality,
	2 CIFAS,
	2 GoneAway,
	2 CCJs,
	2 Bankruptcy,
	CreditIvaCheck	AS IVA		

FROM  rpt_Level_One_Report_Person P
INNER JOIN 
dbo.rpt_Level_One_Report_Addresses A
ON A.Person_ID = @Person_Id
AND A.RiskClaim_Id = @RiskClaim_Id
AND AddressLinkType = 'Current Address'
INNER JOIN  
dbo.rpt_Level_One_Report_Person_Tracesmart TS
ON TS.Person_ID = @Person_Id
AND TS.RiskClaim_Id = @RiskClaim_Id

WHERE
P.RiskClaim_Id = @RiskClaim_Id AND P.Person_Id = @Person_Id