﻿CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Addresses]

	@RiskClaim_Id INT,
	@Person_ID INT


AS

SELECT * FROM rpt_Level_One_Report_Addresses WHERE RiskClaim_Id = @RiskClaim_Id AND Person_ID = @Person_ID
AND ParentName = 'PersonRisk'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'usp_rpt_Level_One_Report_Addresses';

