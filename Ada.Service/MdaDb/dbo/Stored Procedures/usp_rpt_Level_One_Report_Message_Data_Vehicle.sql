﻿CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Message_Data_Vehicle]

	@RiskClaim_Id INT,
	@Vehicle_Id INT

AS

SELECT * FROM rpt_Level_One_Report_DataMed WHERE RiskClaim_Id = @RiskClaim_Id AND Veh_DbId = @Vehicle_Id AND Rule_Set_Key = 'VehicleAllClaimsExperience'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'usp_rpt_Level_One_Report_Message_Data_Vehicle';

