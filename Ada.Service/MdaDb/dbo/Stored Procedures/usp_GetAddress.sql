﻿
CREATE PROCEDURE [dbo].[usp_GetAddress]

	@SubBuilding VARCHAR(50),
	@Building VARCHAR(50),
	@BuildingNumber VARCHAR(50),
	@Street VARCHAR(500),
	@Locality VARCHAR(50),
	@Town VARCHAR(50),
	@PostCode VARCHAR(10),
	@Debug BIT = 0
	
	

AS

SET NOCOUNT ON

DECLARE @HN VARCHAR(4)	
DECLARE @Str VARCHAR(100)
IF @SubBuilding IS NULL SET @SubBuilding = ''
IF @Building IS NULL SET @Building = ''
IF @BuildingNumber  IS NULL SET @BuildingNumber = ''
IF @Street  IS NULL SET @Street  = ''
IF @Locality  IS NULL SET @Locality = ''
IF @Town IS NULL SET @Town = ''
IF @PostCode IS NULL SET @PostCode = ''

DECLARE @PafAddress TABLE 
(	SubBuildingName VARCHAR(50),
	BuildingNumber VARCHAR(4),
	Building VARCHAR(50),
	Street  VARCHAR(50), 
	Locality VARCHAR(50),
	Town  VARCHAR(30),
	PostCode VARCHAR(7),
	QStr VARCHAR(500),
	QStr2 VARCHAR(500),
	QStr3 VARCHAR(500),
	QStr4 VARCHAR(500),
	PafValidation BIT,
	Flag INT
 
)
	DECLARE	@Address VARCHAR(600) = @SubBuilding + @Building +  @BuildingNumber + @Street + @Locality  + @Town 
	SET @PostCode = REPLACE(@PostCode, ' ', '')
		
	SET @Address =  REPLACE(REPLACE(@Address, ' ', ''), ',', '')
	 
	IF (LEN(@PostCode) < 1) 
	BEGIN
		SET @PostCode = dbo.fn_GetPostCodeFromString(@Address)
		IF (LEN(@PostCode) > 4)
			SET @Address =  REPLACE(@Address, @PostCode, '')
	END

--	IF NOT EXISTS(SELECT * FROM PafIndex WITH (NOLOCK) WHERE (Postcode = @PostCode))
--	BEGIN
		
	
--		SELECT *, DIFFERENCE (LTRIM(RTRIM(P.HoNo)) + LTRIM(RTRIM(TF.Thoroughfare)) + LTRIM(RTRIM(ThoroughfareDescriptor)) + LTRIM(RTRIM(PostTown)),  @Street) 
--		FROM PafIndex P WITH (NOLOCK)
--		INNER JOIN Town AS T WITH (NOLOCK) ON TownKey = T.ID 
--		INNER JOIN Thoroughfare AS TF ON P.Thoroughfare = TF.ID 
--		INNER JOIN  ThoroughfareDescriptor AS TD WITH (NOLOCK) ON P.ThorfareDesc = TD.ID 
--		WHERE
--		@PostCode = LEFT(PostCode, LEN(@PostCode)) 
--		--AND  DIFFERENCE (LTRIM(RTRIM(TF.Thoroughfare)) + LTRIM(RTRIM(DependentLocality)) + LTRIM(RTRIM(PostTown)),  @Str ) = 4
--SELECT @Str
--		---
--	END


	IF (LEN(@PostCode) > 4)
	BEGIN
		INSERT INTO @PafAddress (SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, QStr, QStr2, QStr3, QStr4, PafValidation, Flag)
		SELECT 
			LTRIM(RTRIM(SBN.SubBuildingName)) AS SubBuildingName,
			NULLIF(CONVERT(VARCHAR(4), P.HoNo), '0') AS BuildingNumber,
			LTRIM(RTRIM(BN.BuildingName)) AS Building, 
			LTRIM(RTRIM(TF.Thoroughfare)) + SPACE(1) + LTRIM(RTRIM(TD.ThoroughfareDescriptor)) AS Street, 
			LTRIM(RTRIM(DependentLocality)) AS Locality,
			LTRIM(RTRIM(T.PostTown)) PostTown, 
			P.PostCode2 AS PostCode, 
			
			REPLACE(ISNULL(SBN.SubBuildingName, ''), ' ', '') +   REPLACE(ISNULL(BN.BuildingName, ''), ' ', '') + CASE WHEN P.HoNo = 0 THEN '' ELSE CONVERT(VARCHAR(4), P.HoNo)   END + REPLACE(ISNULL(TF.Thoroughfare, ''), ' ', '') QStr,
			REPLACE(ISNULL(BN.BuildingName, ''), ' ', '') + CASE WHEN P.HoNo = 0 THEN '' ELSE CONVERT(VARCHAR(4), P.HoNo)   END + REPLACE(ISNULL(TF.Thoroughfare, ''), ' ', '') QStr2,
			REPLACE(ISNULL(SBN.SubBuildingName, ''), ' ', '') +  CASE WHEN P.HoNo = 0 THEN '' ELSE CONVERT(VARCHAR(4), P.HoNo)   END + REPLACE(ISNULL(TF.Thoroughfare, ''), ' ', '') QStr3,
			CASE WHEN P.HoNo = 0 THEN '' ELSE CONVERT(VARCHAR(4), P.HoNo)   END + REPLACE(ISNULL(TF.Thoroughfare, ''), ' ', '') QStr4,
			NULL,
			NULL
		FROM    PafIndex AS P WITH (NOLOCK) 
			LEFT OUTER JOIN SubBuildingName AS SBN ON P.SubBuiName = SBN.ID LEFT OUTER JOIN
			Thoroughfare AS TF ON P.Thoroughfare = TF.ID LEFT OUTER JOIN
			Town AS T WITH (NOLOCK) ON P.TownKey = T.ID LEFT OUTER JOIN
			ThoroughfareDescriptor AS TD WITH (NOLOCK) ON P.ThorfareDesc = TD.ID LEFT OUTER JOIN
			BuildingName AS BN WITH (NOLOCK) ON P.BuilName = BN.ID
		WHERE     (P.Postcode = @PostCode)



			UPDATE   @PafAddress
			SET PafValidation = 1, Flag = 1
			WHERE 
				ISNULL(SubBuildingName, '') = @SubBuilding AND
				ISNULL(Building, '') = @Building AND
				ISNULL(BuildingNumber, '') = @BuildingNumber AND
				ISNULL(Street, '') = @Street AND
				ISNULL(Locality, '') = @Locality AND
				ISNULL(Town, '')  = @Town AND
				ISNULL(PostCode, '') = @PostCode
			
			
			
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
			UPDATE   @PafAddress
			SET PafValidation = 1, Flag = 20
			WHERE PATINDEX('%' + @Address + '%',QStr ) > 0 
			
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
			UPDATE   @PafAddress
			SET PafValidation = 1, Flag = 30
			WHERE PATINDEX(QStr2 + '%', @Address) > 0   AND LEN(QStr2) > 2
			
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
			UPDATE   @PafAddress
			SET PafValidation = 1, Flag = 40
			WHERE PATINDEX('%' + @Address + '%',QStr2 ) > 0 
	
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
			UPDATE   @PafAddress
			SET PafValidation = 1, Flag = 50
			WHERE PATINDEX(QStr3 + '%', @Address) > 0  AND LEN(QStr3) > 2 
			
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
			UPDATE   @PafAddress
			SET PafValidation = 1, Flag = 60
			WHERE PATINDEX('%' + @Address + '%',QStr3 ) > 0 
	
			--IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
			--UPDATE   @PafAddress
			--SET PafValidation = 1, Flag = 70
			--WHERE PATINDEX('%' + QStr4 + '%', @Address) > 0 
			
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
			UPDATE   @PafAddress
			SET PafValidation = 1, Flag = 80
			WHERE PATINDEX('%' + @Address + '%',QStr4 ) > 0 
		
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
			UPDATE   @PafAddress
			SET PafValidation = 1, Flag = 90
			WHERE PATINDEX( QStr4 + '%',@Address ) > 0  AND LEN(QStr4) > 2
		
			--IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
			--UPDATE   @PafAddress
			--SET PafValidation = 1, Flag = 100
			--WHERE BuildingNumber = @Address
			
SET @HN = LEFT(@Address, 4)
SET @Str = @Address
		 
		 WHILE PATINDEX('%[^0-9]%', @HN) > 0
		   BEGIN
				SET @HN = STUFF(@HN, PATINDEX('%[^0-9]%', @HN), 1, '')
		   END
					
		 WHILE PATINDEX('%[0-9]%', @Str) > 0
		   BEGIN
				SET @Str = STUFF(@Str, PATINDEX('%[0-9]%', @Str), 1, '')
		   END	
	--Fuzzy
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
				UPDATE   @PafAddress
				SET PafValidation = 1, Flag = 200
				WHERE BuildingNumber = @HN AND DIFFERENCE ( @Str , Street) = 4
				
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
				UPDATE   @PafAddress
				SET PafValidation = 1, Flag = 300
				WHERE BuildingNumber = @HN AND DIFFERENCE ( @Str , Street) = 3
				
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
				UPDATE   @PafAddress
				SET PafValidation = 1, Flag = 400
				WHERE BuildingNumber = @HN AND DIFFERENCE ( @Str , Street) = 2
				

			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
				UPDATE   @PafAddress
				SET PafValidation = 1, Flag = 500
				WHERE BuildingNumber = @HN
			
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
				UPDATE   @PafAddress
				SET PafValidation = 1, Flag = 600
				WHERE REPLACE(Building, ' ', '') = LEFT(@Address, LEN(REPLACE(Building, ' ', '')))
				
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
				UPDATE   @PafAddress
				SET PafValidation = 1, Flag = 700
				WHERE REPLACE(SubBuildingName, ' ', '') = LEFT(@Address, LEN(REPLACE(SubBuildingName, ' ', '')))
				
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
				UPDATE   @PafAddress
				SET PafValidation = 1, Flag = 800
				WHERE DIFFERENCE ( LEFT(@Address, LEN(Building)) , Building) = 4
				
			IF NOT EXISTS (SELECT * FROM  @PafAddress WHERE PafValidation = 1)
				UPDATE   @PafAddress
				SET PafValidation = 1, Flag = 900
				WHERE DIFFERENCE ( LEFT(@Address, LEN(SubBuildingName)) , SubBuildingName) = 4
			 
		
	END
	If (@Debug = 0)
		SELECT DISTINCT SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, Flag FROM @PafAddress WHERE PafValidation = 1
	ELSE
		SELECT DISTINCT SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, Flag FROM @PafAddress