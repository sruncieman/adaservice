﻿
/****** Object:  StoredProcedure [dbo].[usp_rpt_Level_One_Report_Incident_And_Policy]    Script Date: 02/11/2014 09:43:02 ******/
CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Incident_And_Policy]

	@RiskClaim_Id INT = 58 --93

AS

/*
	TRUNCATE TABLE rpt_Level_One_Report_Incident_And_Policy
	TRUNCATE TABLE rpt_Level_One_Report_Vehicles
	TRUNCATE TABLE rpt_Level_One_Report_Person
	TRUNCATE TABLE rpt_Level_One_Report_Addresses
	TRUNCATE TABLE rpt_Level_One_Report_Messages
	TRUNCATE TABLE rpt_Level_One_Report_DataMed
	TRUNCATE TABLE rpt_Level_One_Report_Organisation
	TRUNCATE TABLE rpt_Level_One_Report_Hire_Vehicles
*/

DECLARE	@Tracesmart_CallStatus INT
DECLARE	@CompaniesHouse_CallStatus INT
DECLARE	@Tracesmart_ServicesCalledMask INT
DECLARE	@CompaniesHouse_ServicesCalledMask INT
DECLARE @GrayedOut INT = 0
DECLARE @GreenTick INT = 1
DECLARE @YellowExclamation INT = 2
DECLARE @RedCross INT = 3
DECLARE @LatestVersion INT = (SELECT LatestVersion FROM dbo.RiskClaimRun WHERE RiskClaim_Id = @RiskClaim_Id)

IF NOT EXISTS (SELECT * FROM rpt_Level_One_Report_Incident_And_Policy WHERE RiskClaim_Id = @RiskClaim_Id AND LatestVersion = @LatestVersion)
BEGIN
IF EXISTS (SELECT * FROM rpt_Level_One_Report_Incident_And_Policy WHERE RiskClaim_Id = @RiskClaim_Id)
BEGIN
	PRINT 'Removing Old Risk Claim'
	--Remove old report data
	DELETE FROM rpt_Level_One_Report_Incident_And_Policy WHERE RiskClaim_Id = @RiskClaim_Id
	DELETE FROM rpt_Level_One_Report_Vehicles WHERE RiskClaim_Id = @RiskClaim_Id
	DELETE FROM rpt_Level_One_Report_Person WHERE RiskClaim_Id = @RiskClaim_Id
	DELETE FROM rpt_Level_One_Report_Addresses WHERE RiskClaim_Id = @RiskClaim_Id
	DELETE FROM rpt_Level_One_Report_Messages WHERE RiskClaim_Id = @RiskClaim_Id
	DELETE FROM rpt_Level_One_Report_DataMed WHERE RiskClaim_Id = @RiskClaim_Id
	DELETE FROM rpt_Level_One_Report_Organisation WHERE RiskClaim_Id = @RiskClaim_Id
	DELETE FROM rpt_Level_One_Report_Hire_Vehicles WHERE RiskClaim_Id = @RiskClaim_Id
	DELETE FROM dbo.rpt_Level_One_Report_Person_Tracesmart WHERE RiskClaim_Id = @RiskClaim_Id
END

DECLARE @XML XML =  dbo.fnRiskClaimXml(@RiskClaim_Id)

PRINT 'INSERT INTO rpt_Level_One_Report_Incident_And_Policy'

;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO rpt_Level_One_Report_Incident_And_Policy
SELECT	@RiskClaim_Id AS RiskClaim_Id,
		@LatestVersion AS LatestVersion,
		GETDATE() CreateDate,
		GETDATE() LastRunDate,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_ITt)')), '')  AS IncidentType,
	    NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_CN)')), '')  AS ClaimNumber,
	    NULLIF(CONVERT(varchar(50), TBL.COL.query('data(Policy/v_CS)')), '')  AS ClaimStatus,
	    NULLIF(CONVERT(varchar(50), TBL.COL.query('data(Policy/v_ID)')), '')  AS IncidentDate,
	    NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_IDT)')), '')  AS IncidentDateTime,
	    REPLACE(NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_CCode)')), ''),'&amp;','&')  AS ClaimCode,
	    NULL  AS Reserve,   
	    NULL AS PaymentsToDate,
	    REPLACE(NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Loc)')), ''),'&amp;','&') AS Location,
	    REPLACE( NULLIF(CONVERT(varchar(500), TBL.COL.query('data(v_Circ)')), ''),'&amp;','&') AS Circumstances,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(Policy/v_PN)')), '')  AS PolicyNumber,
	    NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PSD)')), '')  AS PolicyStartDate,
	    NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PED)')), '')  AS PolicyEndDate,
		ISNULL(NULLIF(CONVERT(varchar(50), TBL.COL.query('data(Policy/v_B)')), ''), 'Not Provided')  AS Broker_,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(Policy/v_PTt)')), '')  AS PolicyType,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(Policy/v_CTt)')), '')  AS CoverType,
		NULLIF(CONVERT(varchar(100), TBL.COL.query('data(RH1)')), '')  AS MessageHeader,
	    NULLIF(CONVERT(varchar(100), TBL.COL.query('data(RH2)')), '')  AS ReportHeader
FROM @Xml.nodes('//ClaimScoreResults/Incident') AS TBL(COL)	

;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
UPDATE   IP
SET		 Reserve = NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Res)')), '')
		,PaymentsToDate = NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PTD)')), '')    
FROM @Xml.nodes('//PersonRisk') AS TBL(COL)
INNER JOIN dbo.rpt_Level_One_Report_Incident_And_Policy IP ON IP.RiskClaim_Id = CONVERT(VARCHAR(50), TBL.COL.query('data(RCId)'))

PRINT 'INSERT INTO rpt_Level_One_Report_Vehicles'
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO rpt_Level_One_Report_Vehicles
SELECT	@RiskClaim_Id AS RiskClaim_Id,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(Veh_DbId)')), '')  AS Vehicle_Id,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_COL)')), '')  AS CategoryOfLoss,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_COLt)')), '')  AS CategoryOfLoss_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_C)')), '')  AS Colour,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Ct)')), '')  AS Colour_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_EC)')), '')  AS EngineCapacity,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_F)')), '')  AS Fuel,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Ft)')), '')  AS Fuel_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_IsKA)')), '')  AS IsKeyAttractor,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Ma)')), '')  AS Make,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Mo)')), '')  AS Model,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_OC)')), '')  AS OccupancyCount,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_T)')), '')  AS Transmission,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Tt)')), '')  AS Transmission_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VIL)')), '')  AS VehicleIncidentLink,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VILt)')), '')  AS VehicleIncidentLink_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VR)')), '')  AS VehicleRegistration,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VT)')), '')  AS VehicleType,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VTt)')), '')  AS VehicleType_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VIN)')), '')  AS VIN,
		CASE CONVERT(varchar(50), TBL.COL.query('data(v_VILt)'))
			WHEN 'Insured Vehicle' THEN 1
			WHEN 'Third Party Vehicle' THEN 2
			WHEN 'Witness Vehicle' THEN 3
			WHEN 'No Vehicle Involved' THEN 4
			WHEN 'Unknown' THEN 5
		END AS SortOrder,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(RH1)')), '')  AS MessageHeader,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(RH2)')), '')  AS ReportHeader   
FROM @Xml.nodes('//ClaimScoreResults/Incident/Vehicles/VehicleRisk') AS TBL(COL)
	
PRINT 'INSERT INTO rpt_Level_One_Report_DataMed'
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO rpt_Level_One_Report_DataMed
SELECT	@RiskClaim_Id AS RiskClaim_Id,
		CASE TBL.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') 
			WHEN 'PersonRisk' THEN TBL.COL.value('(../../../../../../../../Per_DbId[1])','VARCHAR(10)')
			WHEN 'AddressRisk' THEN TBL.COL.value('(../../../../../../../../../../Per_DbId[1])','VARCHAR(10)')
		END AS Per_Id,
		CASE TBL.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') 
			WHEN 'VehicleRisk' THEN TBL.COL.value('(../../../../../../../../Veh_DbId[1])','VARCHAR(10)')
			WHEN 'PersonRisk' THEN TBL.COL.value('(../../../../../../../../../../Veh_DbId[1])','VARCHAR(10)')
		END AS Veh_Id,	
		CASE TBL.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') 
			WHEN 'AddressRisk' THEN TBL.COL.value('(../../../../../Add_DbId[1])','VARCHAR(10)')
		END AS Add_Id,
		TBL.COL.value('(../../../../../../../../Org_DbId[1])','VARCHAR(10)') AS Org_DbId,
		TBL.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') LocalName,
		TBL.COL.value('(../../../Key[1])','VARCHAR(50)') AS Rule_Key,
		TBL.COL.value('(../../../../../Key[1])','VARCHAR(50)') AS Rule_Set_Key,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[1]')), '') AS DataMed1,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[2]')), '') AS DataMed2,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[3]')), '') AS DataMed3,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[4]')), '') AS DataMed4,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[5]')), '') AS DataMed5,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[6]')), '') AS DataMed6,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[7]')), '') AS DataMed7,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[8]')), '') AS DataMed8,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[9]')), '') AS DataMed9,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[10]')), '') AS DataMed10,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[11]')), '') AS DataMed11,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[12]')), '') AS DataMed12,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[13]')), '') AS DataMed13,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[14]')), '') AS DataMed14,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[15]')), '') AS DataMed15,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[16]')), '') AS DataMed16,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[17]')), '') AS DataMed17,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[18]')), '') AS DataMed18,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[19]')), '') AS DataMed19,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[20]')), '') AS DataMed20,
		TBL.COL.value('(../../../../../../../../RH1[1])','VARCHAR(100)') AS MessageHeader,
		TBL.COL.value('(../../../../../../../../RH2[1])','VARCHAR(100)') AS ReportHeader
FROM @Xml.nodes('//RGResults/Value/DM/R') AS TBL(COL)	
WHERE LTRIM(RTRIM(
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[1]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[2]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[3]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[4]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[5]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[6]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[7]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[8]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[9]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[10]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[11]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[12]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[13]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[14]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[15]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[16]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[17]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[18]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[19]')) +
		CONVERT(varchar(MAX), TBL.COL.query('data(C)[20]'))
		)) != ''

---------------------------------------------------------------
--Keoghs Intelligence Linked Data
---------------------------------------------------------------
--Person_LinkedAddresses
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:FullAddress)')) ,'') FullAddress
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Person_LinkedAddresses_Result') AS TBL(COL)

--Person_AliasInformation_Result
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, LocalName, Rule_Key, DataMed1, DataMed2, DataMed3, DataMed4, DataMed5, DataMed6, DataMed7, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Per_DbId)')) ,'')	Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:DateOfBirth)')),'')	DateOfBirth
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:FirstName)')),'')		FirstName
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:LastName)')),'')		LastName
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Person_Id)')),'')		Person_Id_
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Salutation)')),'')	Salutation
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Occupation)')) ,'') Occupation
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Person_AliasInformation_Result') AS TBL(COL)

--Person_LinkedTelephones
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Telephone)')) ,'') Telephone
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Person_LinkedTelephones') AS TBL(COL)

--Person_LinkedBankAccounts
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:BankAccounts)')) ,'') BankAccounts
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Person_LinkedBankAccounts') AS TBL(COL)

--Person_LinkedDrivingLicenceNumbers
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:DrivingLicenceNumber)')) ,'') DrivingLicenceNumber
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Person_LinkedDrivingLicenceNumbers') AS TBL(COL)

--Person_LinkedEmails
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:EmailAddress)')) ,'') EmailAddress
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Person_LinkedEmails') AS TBL(COL)

--Person_LinkedNINumbers
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:NINumber)')) ,'') NINumber
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Person_LinkedNINumbers') AS TBL(COL)

--Person_LinkedPassportNumbers
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:PassportNumber)')) ,'') PassportNumber
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Person_LinkedPassportNumbers') AS TBL(COL)

--Organisation_FormallyKnownAs
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, Org_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Org_DbId)')) ,'') Org_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:FormerName)')) ,'') FormerName
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Organisation_FormallyKnownAs') AS TBL(COL)

--Organisation_LinkedEmails
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, Org_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Org_DbId)')) ,'') Org_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:EmailAddress)')) ,'') EmailAddress
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed 
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Organisation_LinkedEmails') AS TBL(COL)

--Organisation_LinkedAddress
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, Org_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Org_DbId)')) ,'') Org_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:FullAddress)')) ,'') FullAddress
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed 
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Organisation_LinkedAddress') AS TBL(COL)

--Organisation_LinkedTelephones
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, Org_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Org_DbId)')) ,'') Org_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Telephone)')) ,'') Telephone
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Organisation_LinkedTelephones') AS TBL(COL)

--Organisation_LinkedWebSites
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, Org_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Org_DbId)')) ,'') Org_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:WebSiteURL)')) ,'') WebSiteURL
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader
FROM @Xml.nodes('//a:Organisation_LinkedWebSites') AS TBL(COL)

--Organisation_TradingNames
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://schemas.datacontract.org/2004/07/MDA.DataService.Model' AS a, 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_DataMed (RiskClaim_Id, Per_DbId, Veh_DbId, Org_DbId, LocalName, Rule_Key, DataMed1, DataMed2, MessageHeader, ReportHeader)
SELECT
	   @RiskClaim_Id RiskClaim_Id
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../Per_DbId)')) ,'') Person_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../../../Veh_DbId)')) ,'') Vehicle_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../Org_DbId)')) ,'') Org_Id 
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(../..)')) ,'') LocalName
	  ,NULLIF(CONVERT(varchar(50), TBL.COL.query('fn:local-name(.)')) ,'') Rule_Key
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:TradingName)')) ,'') TradingName
	  ,NULLIF(CONVERT(varchar(max), TBL.COL.query('data(a:Confirmed)')) ,'') Confirmed 
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH1)')) ,'')  MessageHeader
	  ,NULLIF(CONVERT(varchar(500), TBL.COL.query('data(../../RH2)')) ,'')  ReportHeader 
FROM @Xml.nodes('//a:Organisation_TradingNames') AS TBL(COL)
	
PRINT 'INSERT INTO rpt_Level_One_Report_Person'
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO rpt_Level_One_Report_Person
SELECT @RiskClaim_Id AS RiskClaim_Id,
		NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))), '') AS Person_Id,
		NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(../../Veh_DbId)'))), '') AS Vehicle_Id,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_ConA)')), '') AS ConfirmedAliases,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_GENt)')), '') AS Gender,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_SALt)')), '') AS Salutation,
		NULLIF(CASE WHEN CONVERT(varchar(50), TBL.COL.query('data(v_FN)'))	= '' THEN '' ELSE CONVERT(varchar(50), TBL.COL.query('data(v_FN)'))	+ ' ' END +
		CASE WHEN CONVERT(varchar(50), TBL.COL.query('data(v_MN)'))	= '' THEN '' ELSE CONVERT(varchar(50), TBL.COL.query('data(v_MN)'))	 + ' ' END +
		CASE WHEN CONVERT(varchar(50), TBL.COL.query('data(v_LN)'))	= '' THEN '' ELSE UPPER(CONVERT(varchar(50), TBL.COL.query('data(v_LN)')))	END,'') AS FullName,
		--CONVERT(DATE, CONVERT(date, CONVERT(varchar(50), TBL.COL.query('data(v_DoB)')))) AS DateOfBirth,
		CONVERT(DATE,NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_DoB)')),'')) AS DateOfBirth,
		ISNULL(NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_MOB)')), ''), 'Not Provided')  AS MobileNumber, 
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_NI)')), '') AS NINumber,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PTt)')), '') AS PartyType,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_SPTt)')), '') AS SubPartyType,
		dbo.fnGetXmlMediumDataAliasInfo(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))))  AS PossibleAlias,
		dbo.fnGetXmlMediumDataOccupation(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)')))) AS Occupation,
		dbo.fnGetXmlMediumDataLinkedGeneric(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))),'PersonRisk','Person_LinkedAddresses_Result') AS OtherLinkedAddresses,
		dbo.fnGetXmlMediumDataLinkedGeneric(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))),'PersonRisk','Person_LinkedTelephones') AS LinkedTelephone,
		CASE CONVERT(varchar(50), TBL.COL.query('data(v_PTt)'))
			WHEN 'Policyholder' THEN 1
			WHEN 'Paid Policy' THEN 2
			WHEN 'Insured' THEN 
				CASE CONVERT(varchar(50), TBL.COL.query('data(v_SPTt)'))
					WHEN 'Driver' THEN 3			
					WHEN 'Passenger' THEN 4
					WHEN 'Unknown' THEN 5
					WHEN 'Witness' THEN 6
				END
			WHEN 'Third Party' THEN 
				CASE CONVERT(varchar(50), TBL.COL.query('data(v_SPTt)'))
					WHEN 'Driver' THEN 7			
					WHEN 'Passenger' THEN 8
					WHEN 'Unknown' THEN 9
					WHEN 'Witness' THEN 10
				END
			ELSE
				CASE CONVERT(varchar(50), TBL.COL.query('data(v_SPTt)'))
					WHEN 'Unknown' THEN 11
					WHEN 'Claimant' THEN 12
					WHEN 'Witness' THEN 13
					ELSE 14
				END
		END  SortOrder,
		NULLIF(CONVERT(varchar(100), TBL.COL.query('data(RH1)')), '') AS MessageHeader,
		NULLIF(CONVERT(varchar(100), TBL.COL.query('data(RH2)')), '') AS ReportHeader,
		NULLIF(CASE WHEN LEN(CONVERT(varchar(50), TBL.COL.query('data(v_LAND)'))) > 0 THEN CONVERT(varchar(50), TBL.COL.query('data(v_LAND)')) + ' [L]' ELSE '' END +
		CASE WHEN LEN(CONVERT(varchar(50), TBL.COL.query('data(v_MOB)'))) > 0  THEN '; ' + CONVERT(varchar(50), TBL.COL.query('data(v_MOB)')) + ' [M]' ELSE '' END + 
		CASE WHEN LEN(CONVERT(varchar(50), TBL.COL.query('data(v_TELEO)'))) > 0 THEN '; ' + CONVERT(varchar(50), TBL.COL.query('data(v_TELEO)')) + ' [O]' ELSE '' END +
		CASE WHEN LEN(CONVERT(varchar(50), TBL.COL.query('data(v_FAX)'))) > 0 THEN '; ' + CONVERT(varchar(50), TBL.COL.query('data(v_FAX)')) + ' [F]' ELSE '' END,'')  AS Telephone,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_NAT)')), '') AS Nationality,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_DLN)')), '') AS DrivingLicenceNumber,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PASS)')), '') AS PassportNumber,
		NULLIF(REPLACE(CONVERT(varchar(50), TBL.COL.query('data(v_OCC)')),',',';'), '') AS OccupationClaim,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_EMAIL)')), '') AS EmailAddress,
		NULLIF(CASE WHEN LEN(CONVERT(varchar(50), TBL.COL.query('data(v_BASC)'))) > 0 THEN CONVERT(varchar(50), TBL.COL.query('data(v_BASC)')) ELSE '' END +
		CASE WHEN LEN(CONVERT(varchar(50), TBL.COL.query('data(v_BAAN)'))) > 0  THEN ' ' + CONVERT(varchar(50), TBL.COL.query('data(v_BAAN)')) ELSE '' END + 
		CASE WHEN LEN(CONVERT(varchar(50), TBL.COL.query('data(v_BAN)'))) > 0 THEN ' ' + CONVERT(varchar(50), TBL.COL.query('data(v_BAN)')) ELSE '' END,'') AS BankAccount,
		dbo.fnGetXmlMediumDataLinkedGeneric(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))),'PersonRisk','Person_LinkedNINumbers') AS LinkedNINumber,
		dbo.fnGetXmlMediumDataLinkedGeneric(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))),'PersonRisk','Person_LinkedDrivingLicenceNumbers') AS LinkedDrivingLicenceNumber,
		dbo.fnGetXmlMediumDataLinkedGeneric(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))),'PersonRisk','Person_LinkedPassportNumbers') AS LinkedPassportNumber,
		dbo.fnGetXmlMediumDataLinkedGeneric(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))),'PersonRisk','Person_LinkedTaxiDrivingLicenceNumbers') AS LinkedTaxiDriverLicenceNumber,
		dbo.fnGetXmlMediumDataLinkedGeneric(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))),'PersonRisk','Person_LinkedEmails') AS LinkedEmailAddress,
		dbo.fnGetXmlMediumDataLinkedGeneric(@RiskClaim_Id, CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))),'PersonRisk','Person_LinkedBankAccounts') AS LinkedBankAccount,
		null LinkedRelativesAssociates
FROM @Xml.nodes('//PersonRisk') AS TBL(COL)	

UPDATE A
SET Telephone =  SUBSTRING(Telephone,3,LEN(Telephone) - 2)
FROM dbo.rpt_Level_One_Report_Person A
WHERE RiskClaim_Id = @RiskClaim_Id
AND LEFT(Telephone,2) = '; '
	
PRINT 'INSERT INTO rpt_Level_One_Report_Hire_Vehicles'
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_Hire_Vehicles
SELECT	@RiskClaim_Id AS RiskClaim_Id,
		CONVERT(VARCHAR(MAX), TBL.COL.query('data(../../../../../../../../Inc_DbId)')) AS Inc_DbId,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../../../../../Veh_DbId)')), '')  AS Parent_Vehicle_Id,
		CONVERT(VARCHAR(MAX), TBL.COL.query('data(../../../../Per_DbId)')) AS Per_DbId,
		CONVERT(VARCHAR(MAX), TBL.COL.query('data(../../Org_DbId)')) AS Org_DbId,
		NULLIF(CONVERT(varchar(50)  , TBL.COL.query('data(Veh_DbId)')), '')  AS Vehicle_Id,
		TBL.COL.value('fn:local-name(../..[1])', 'nvarchar(50)') ParentName, 
		TBL.COL.value('fn:local-name(.[1])', 'nvarchar(50)') LocalName,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_COL)')), '')  AS CategoryOfLoss,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_COLt)')), '')  AS CategoryOfLoss_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_C)')), '')  AS Colour,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Ct)')), '')  AS Colour_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_EC)')), '')  AS EngineCapacity,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_F)')), '')  AS Fuel,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Ft)')), '')  AS Fuel_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_IsKA)')), '')  AS IsKeyAttractor,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Ma)')), '')  AS Make,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Mo)')), '')  AS Model,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_OC)')), '')  AS OccupancyCount,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_T)')), '')  AS Transmission,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Tt)')), '')  AS Transmission_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VIL)')), '')  AS VehicleIncidentLink,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VILt)')), '')  AS VehicleIncidentLink_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VR)')), '')  AS VehicleRegistration,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VT)')), '')  AS VehicleType,
		NULLIF(CONVERT(varchar(50)   , TBL.COL.query('data(v_VTt)')), '')  AS VehicleType_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VIN)')), '')  AS VIN,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../v_ON)')), '') AS OrganisationName,
		NULLIF(CONVERT(Smalldatetime, NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../v_HSsd)')),''), 103),'') AS HireStartDate,
		NULLIF(CONVERT(Smalldatetime, NULLIF(CONVERT(varchar(50), TBL.COL.query('data(../../v_Hed)')),''), 103),'') AS HireEndDate,
		NULLIF(CONVERT(varchar(100), TBL.COL.query('data(RH1)')), '') AS MessageHeader,
		NULLIF(CONVERT(varchar(100), TBL.COL.query('data(RH2)')), '') AS ReportHeader
FROM @Xml.nodes('//OrganisationVehicleRisk') AS TBL(COL)	

PRINT 'INSERT INTO rpt_Level_One_Report_Addresses'
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_Addresses
SELECT @RiskClaim_Id AS RiskClaim_Id,
	   NULLIF(CONVERT(int, CONVERT(varchar(50) , TBL.COL.query('data(../../Per_DbId)'))), '')  AS Person_ID,
	   NULLIF(CONVERT(int, CONVERT(varchar(50) , TBL.COL.query('data(../../Org_DbId)'))), '')  AS Org_ID,
	   TBL.COL.value('fn:local-name(../..[1])', 'nvarchar(50)') ParentName, 
	   TBL.COL.value('fn:local-name(.[1])', 'nvarchar(50)') LocalName, 
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_ALT)')), '')  AS AddressLinkType,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_BN)')), '')  AS BuildingNumber,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_C)')), '')  AS County,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(Addr_DbId)')), '')  AS Db_Id,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_EoR)')), '')  AS EndOfResidency,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_IsKA)')), '')  AS IsKeyAttractor,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_L)')), '')  AS Locality,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_MC)')), '')  AS MosaicCode,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PafV)')), '')  AS PafValidation,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PC)')), '')  AS Postcode,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PCKA)')), '')  AS PostCodeKeyAttractor,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PT)')), '')  AS PropertyType,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_SoR)')), '')  AS StartOfResidency,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_S)')), '')  AS Street,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_SB)')), '')  AS SubBuilding,
       NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_T)')), '')  AS Town,
	   NULLIF(CONVERT(int, CONVERT(varchar(50) , TBL.COL.query('data(../../../../Veh_DbId)'))), '')  AS Vehicle_ID,
	   NULLIF(CONVERT(varchar(100), TBL.COL.query('data(../../RH1)')), '') AS MessageHeader,
	   NULLIF(CONVERT(varchar(100), TBL.COL.query('data(../../RH2)')), '') AS ReportHeader,
	   NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_B)')), '')  AS BuildingName
FROM @Xml.nodes('//AddressRisk') AS TBL(COL)

PRINT 'INSERT INTO rpt_Level_One_Report_Organisation'
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_Organisation
SELECT	@RiskClaim_Id AS RiskClaim_Id,
		CONVERT(VARCHAR(MAX), TBL.COL.query('data(../../Inc_DbId)')) AS Inc_DbId,
		NULLIF(CONVERT(VARCHAR(MAX), TBL.COL.query('data(../../Per_DbId)')),'') AS Per_DbId,
		TBL.COL.value('fn:local-name(../..[1])', 'nvarchar(50)') ParentName, 
		TBL.COL.value('fn:local-name(.[1])', 'nvarchar(50)') LocalName, 
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_IsKA)')), '') AS IsKeyAttractor,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(Org_DbId)')), '') AS Org_DbId,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_ON)')), '') AS OrganisationName,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_OPLT)')), '') AS OrganisationPersonLinkType,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_OPLTt)')), '') AS OrganisationPersonLinkType_Text,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(v_OrgAddrs)')), '') AS OrganisationsAddresses,
		NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(v_OrgVehs)')), '') AS OrganisationsVehicles,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_OT)')), '') AS OrganisationType,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_OTt)')), '') AS OrganisationType_Text,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_PTD)')), '') AS PaymentsToDate,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_RN)')), '') AS RegisteredNumber,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_Res)')), '') AS Reserve,
		NULLIF(CONVERT(varchar(50), TBL.COL.query('data(v_VN)')), '') AS VatNumber,
		CASE WHEN LEN(CONVERT(varchar(20), TBL.COL.query('data(v_T1)'))) > 0 THEN CONVERT(varchar(20), TBL.COL.query('data(v_T1)') ) ELSE '' END + 
		CASE WHEN LEN(CONVERT(varchar(20), TBL.COL.query('data(v_T2)'))) > 0 THEN '; ' + CONVERT(varchar(20), TBL.COL.query('data(v_T2)') ) ELSE '' END  +
		CASE WHEN LEN(CONVERT(varchar(20), TBL.COL.query('data(v_T3)'))) > 0 THEN '; ' + CONVERT(varchar(20), TBL.COL.query('data(v_T3)') ) ELSE '' END  AS Telephone,
		NULLIF(CONVERT(varchar(100), TBL.COL.query('data(RH1)')), '') AS MessageHeader,
	    NULLIF(CONVERT(varchar(100), TBL.COL.query('data(RH2)')), '') AS ReportHeader,
	    NULL LinkedName,
		dbo.fnGetXmlMediumDataOrgLinkedGeneric(@RiskClaim_Id, NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Org_DbId)'))),''), NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(../../Per_DbId)'))),''),'OrganisationRisk','Organisation_TradingNames') AS LinkedAddress,
		dbo.fnGetXmlMediumDataOrgLinkedGeneric(@RiskClaim_Id, NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Org_DbId)'))),''), NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(../../Per_DbId)'))),''),'OrganisationRisk','Organisation_LinkedAddresses') AS LinkedTelephones,
		dbo.fnGetXmlMediumDataOrgLinkedGeneric(@RiskClaim_Id, NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Org_DbId)'))),''), NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(../../Per_DbId)'))),''),'OrganisationRisk','Organisation_LinkedWebSites') AS LinkedWebsites,
		NULLIF(CONVERT(varchar(100), TBL.COL.query('data(v_EMAIL)')), '') Email,
		NULLIF(CONVERT(varchar(100), TBL.COL.query('data(v_WSITE)')), '') Website,
		dbo.fnGetXmlMediumDataOrgLinkedGeneric(@RiskClaim_Id, NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Org_DbId)'))),''), NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(../../Per_DbId)'))),''),'OrganisationRisk','Organisation_TradingNames') AS LinkedTradingName,
		dbo.fnGetXmlMediumDataOrgLinkedGeneric(@RiskClaim_Id, NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Org_DbId)'))),''), NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(../../Per_DbId)'))),''),'OrganisationRisk','Organisation_FormallyKnownAs') AS LinkedFormerName,
		dbo.fnGetXmlMediumDataOrgLinkedGeneric(@RiskClaim_Id, NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Org_DbId)'))),''), NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(../../Per_DbId)'))),''),'OrganisationRisk','Organisation_LinkedEmails') AS LinkedEmail
FROM @Xml.nodes('//OrganisationRisk') AS TBL(COL)	
	
PRINT 'INSERT INTO rpt_Level_One_Report_Messages:LowMessages'
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_Messages
SELECT  @RiskClaim_Id AS RiskClaim_Id,
		RH.COL.value('(../../../../../../../../Per_DbId[1])','varchar(max)') AS Per_DbId,
		RH.COL.value('(../../../../../../../../Veh_DbId[1])','varchar(max)') AS Veh_DbId,
		RH.COL.value('(../../../../../../../../Add_DbId[1])','varchar(max)') AS Add_DbId,
		RH.COL.value('(../../../../../../../../Org_DbId[1])','varchar(max)') AS Org_DbId,
		RH.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') LocalName, 
		RH.COL.value('(../../../Key[1])','varchar(max)') AS Rule_Key,
		RH.COL.value('(../../../../../Key[1])','varchar(max)') AS Rule_Set_Key,
		CONVERT(VARCHAR(100), RH.COL.query('data(//v_CN)')) AS ClaimNumber,
		RH.COL.value('(../../../../../../../../RH1[1])','varchar(max)') AS MessageHeader,
		'Low' AS MessageType,
		CONVERT(VARCHAR(MAX), RH.COL.query('data(.)')) AS [Message],
		RH.COL.value('(../../../../../../../../RH1[1])','varchar(100)') AS MessageHeader,
		RH.COL.value('(../../../../../../../../RH2[1])','varchar(100)') AS ReportHeader
FROM @Xml.nodes('//RGResults/Value/ML/M') AS RH(COL)

PRINT 'INSERT INTO rpt_Level_One_Report_Messages:MediumMessages'
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_Messages
SELECT	@RiskClaim_Id AS RiskClaim_Id,
		RH.COL.value('(../../../../../../../../Per_DbId[1])','varchar(max)') AS Per_DbId,
		RH.COL.value('(../../../../../../../../Veh_DbId[1])','varchar(max)') AS Veh_DbId,
		RH.COL.value('(../../../../../../../../Add_DbId[1])','varchar(max)') AS Add_DbId,
		RH.COL.value('(../../../../../../../../Org_DbId[1])','varchar(max)') AS Org_DbId,
		RH.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') LocalName, 
		RH.COL.value('(../../../Key[1])','varchar(max)') AS Rule_Key,
		RH.COL.value('(../../../../../Key[1])','varchar(max)') AS Rule_Set_Key,
		CONVERT(VARCHAR(100), RH.COL.query('data(//v_CN)')) AS ClaimNumber,
		RH.COL.value('(../../../../../../../../RH1[1])','varchar(max)') AS Header,
		'Medium' AS MessageType,
		CONVERT(VARCHAR(MAX), RH.COL.query('data(.)')) AS [Message],
		RH.COL.value('(../../../../../../../../RH1[1])','varchar(100)') AS MessageHeader,
		RH.COL.value('(../../../../../../../../RH2[1])','varchar(100)') AS ReportHeader
FROM @Xml.nodes('//RGResults/Value/MM/M') AS RH(COL)

PRINT 'INSERT INTO rpt_Level_One_Report_Messages:HighMessages'	
;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
INSERT INTO dbo.rpt_Level_One_Report_Messages
SELECT	@RiskClaim_Id AS RiskClaim_Id,
		RH.COL.value('(../../../../../../../../Per_DbId[1])','varchar(max)') AS Per_DbId,
		RH.COL.value('(../../../../../../../../Veh_DbId[1])','varchar(max)') AS Veh_DbId,
		RH.COL.value('(../../../../../../../../Add_DbId[1])','varchar(max)') AS Add_DbId,
		RH.COL.value('(../../../../../../../../Org_DbId[1])','varchar(max)') AS Org_DbId,
		RH.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') LocalName, 
		RH.COL.value('(../../../Key[1])','varchar(max)') AS Rule_Key,
		RH.COL.value('(../../../../../Key[1])','varchar(max)') AS Rule_Set_Key,
		CONVERT(VARCHAR(100), RH.COL.query('data(//v_CN)')) AS ClaimNumber,
		RH.COL.value('(../../../../../../../../RH1[1])','varchar(max)') AS Header,
		'High' AS MessageType,
		CONVERT(VARCHAR(MAX), RH.COL.query('data(.)')) AS [Message],
		RH.COL.value('(../../../../../../../../RH1[1])','varchar(100)') AS MessageHeader,
		RH.COL.value('(../../../../../../../../RH2[1])','varchar(100)') AS ReportHeader
FROM @Xml.nodes('//RGResults/Value/MH/M') AS RH(COL)

/*
--------------------------------------------------------------------------
--TracemartDevelopment
--------------------------------------------------------------------------		
--public const int Tracesmart_Use_Passport     = 0x00000001;       // 0000 0000 0000 0000 0000 0000 0000 0001
--public const int Tracesmart_Use_Telephone    = 0x00000002;       // 0000 0000 0000 0000 0000 0000 0000 0010
--public const int Tracesmart_Use_Driving      = 0x00000004;       // 0000 0000 0000 0000 0000 0000 0000 0100
--public const int Tracesmart_Use_Birth        = 0x00000008;       // 0000 0000 0000 0000 0000 0000 0000 1000
--public const int Tracesmart_Use_SmartLink    = 0x00000010;       // 0000 0000 0000 0000 0000 0000 0001 0000
--public const int Tracesmart_Use_NI           = 0x00000020;       // 0000 0000 0000 0000 0000 0000 0010 0000
--public const int Tracesmart_Use_CardNumber   = 0x00000040;       // 0000 0000 0000 0000 0000 0000 0100 0000
--public const int Tracesmart_Use_BankAccount  = 0x00000080;       // 0000 0000 0000 0000 0000 0000 1000 0000
--public const int Tracesmart_Use_Mobile       = 0x00000100;       // 0000 0000 0000 0000 0000 0001 0000 0000
--public const int Tracesmart_Use_Crediva      = 0x00000200;       // 0000 0000 0000 0000 0000 0010 0000 0000
--public const int Tracesmart_Use_CreditActive = 0x00000400;       // 0000 0000 0000 0000 0000 0100 0000 0000
--public const int Tracesmart_Use_NHS          = 0x00000800;       // 0000 0000 0000 0000 0000 1000 0000 0000
--public const int Tracesmart_Use_Cardavs      = 0x00001000;       // 0000 0000 0000 0000 0001 0000 0000 0000
--public const int Tracesmart_Use_MPan         = 0x00002000;       // 0000 0000 0000 0000 0010 0000 0000 0000
--public const int Tracesmart_Use_Address      = 0x00004000;       // 0000 0000 0000 0000 0100 0000 0000 0000
--public const int Tracesmart_Use_DeathScreen  = 0x00008000;       // 0000 0000 0000 0000 1000 0000 0000 0000
--public const int Tracesmart_Use_DoB          = 0x00010000;       // 0000 0000 0000 0001 0000 0000 0000 0000
--public const int Tracesmart_Use_Sanction     = 0x00020000;       // 0000 0000 0000 0010 0000 0000 0000 0000
--public const int Tracesmart_Use_Insolvency   = 0x00040000;       // 0000 0000 0000 0100 0000 0000 0000 0000
--public const int Tracesmart_Use_CCJ          = 0x00080000;       // 0000 0000 0000 1000 0000 0000 0000 0000

--BitMasks
--CONVERT(BIT, @Mask & 1)		AS Tracesmart_Passport,
--CONVERT(BIT, @Mask & 2)		AS Tracesmart_Telephone,
--CONVERT(BIT, @Mask & 4)		AS Tracesmart_Driving,
--CONVERT(BIT, @Mask & 8)		AS Tracesmart_Birth,
--CONVERT(BIT, @Mask & 16)		AS Tracesmart_SmartLink,
--CONVERT(BIT, @Mask & 32)		AS Tracesmart_NI,
--CONVERT(BIT, @Mask & 64)		AS Tracesmart_CardNumber,
--CONVERT(BIT, @Mask & 128)		AS Tracesmart_BankAccount,
--CONVERT(BIT, @Mask & 256)		AS Tracesmart_Mobile,
--CONVERT(BIT, @Mask & 512)		AS Tracesmart_Crediva,
--CONVERT(BIT, @Mask & 1024)	AS Tracesmart_CreditActive,
--CONVERT(BIT, @Mask & 2048)	AS Tracesmart_NHS,
--CONVERT(BIT, @Mask & 4096)	AS Tracesmart_Cardavs,
--CONVERT(BIT, @Mask & 8192)	AS Tracesmart_MPan,
--CONVERT(BIT, @Mask & 16384)	AS Tracesmart_Address,
--CONVERT(BIT, @Mask & 32768)	AS Tracesmart_DeathScreen,
--CONVERT(BIT, @Mask & 65536)	AS Tracesmart_DoB,
--CONVERT(BIT, @Mask & 131072)	AS Tracesmart_Sanction,
--CONVERT(BIT, @Mask & 262144)	AS Tracesmart_Insolvency,
--CONVERT(BIT, @Mask & 524288)	AS Tracesmart_CCJ

--CallStatus
--ResultsFalse = -5,
--NoResults = -4,
--Aborted = -3,
--P2ALinkNotFound = -2,
--PersonNotFound = -1,
--Created = 0,
--Success = 1,
--FailedNeedsRetry = 2,
--UnavailableNeedsRetry = 3

SELECT @Tracesmart_CallStatus = CallStatus, @Tracesmart_ServicesCalledMask = ServicesCalledMask
FROM RiskExternalServicesRequests
WHERE RiskClaim_Id = @RiskClaim_Id AND RiskExternalService_Id = 1

;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)

INSERT INTO dbo.rpt_Level_One_Report_Person_Tracesmart
SELECT 
	@RiskClaim_Id AS RiskClaim_Id,
	NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(Per_DbId)'))), '') AS Person_Id,
	NULLIF(CONVERT(int, CONVERT(varchar(20), TBL.COL.query('data(../../Veh_DbId)'))), '') AS Vehicle_Id,
	@Tracesmart_CallStatus AS Tracesmart_CallStatus, 
	@Tracesmart_ServicesCalledMask AS Tracesmart_ServicesCalledMask,
	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 65536) = 0 THEN @GrayedOut	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_EDob)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS ExperianDOB,

	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 512) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_CredC)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS CreditIvaCheck,

	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 128) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_BAV)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS BankAccountValidated,

	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 4) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_DLV)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS DrivingLicenseValidated,
	
	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 32) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_NIV)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS NINumberValidated,

	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 1) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_PassMV)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS PassportMRZValid,

	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 1) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_PassDoBV)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS PassportDOBValid,

	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 1) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_PGV)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS PassportGenderValid,

	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 4096) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_PCNV)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS PaymentCardNumberValid,
	
	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 2) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_LLV)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS LandlineValidated,

	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 2) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_LLS)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS LandlineStatus,
	
	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 256) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_MobCL)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END		
	END AS MobileCurrentLocation,

	CASE	
		WHEN @Tracesmart_CallStatus IS NULL THEN @GrayedOut --Not Required = Grayed Out
		WHEN @Tracesmart_CallStatus != 1 THEN @YellowExclamation	--Possable Error = Yellow !
		WHEN (@Tracesmart_ServicesCalledMask & 256) = @GrayedOut THEN 0	--Bitmask set to 0 - Not Required = Grayed Out 				
		ELSE 
			CASE CONVERT(VARCHAR(20), TBL.COL.query('data(v_MobS)'))
				WHEN 0 THEN @RedCross --Not Passed = Red Cross
				WHEN 1 THEN @GreenTick --Passed = Green Tick
				ELSE @YellowExclamation --NULL Data = Yellow !
			END	
		END AS MobileStatus
FROM @Xml.nodes('//PersonRisk') AS TBL(COL)	
*/		
			
END
ELSE
	UPDATE dbo.rpt_Level_One_Report_Incident_And_Policy SET LastRunDate = GETDATE() WHERE RiskClaim_Id = @RiskClaim_Id

SELECT	 RCR.RiskClaim_Id
		,RC.ClientClaimRefNumber ClaimNumber
		,RCL.ClientName
		,ISNULL(RC.LevelOneRequestedWho,'Automated User') RequestedBy
		,ISNULL(RC.LevelOneRequestedWhen,GETDATE()) ReportProducedOn
		,CASE WHEN RCR.TotalScore >= RCR.RedThreshold THEN 'High' WHEN RCR.TotalScore >= RCR.AmberThreshold AND RCR.TotalScore < RCR.RedThreshold THEN 'Medium' WHEN RCR.TotalScore >= 0 AND RCR.TotalScore < RCR.AmberThreshold THEN 'Low' ELSE 'NoScore' END AS RiskRaiting
        ,CASE WHEN RCR.TotalScore >= RCR.RedThreshold THEN 'Red' WHEN RCR.TotalScore >= RCR.AmberThreshold AND RCR.TotalScore < RCR.RedThreshold THEN 'Peru' WHEN RCR.TotalScore >= 0 AND RCR.TotalScore < RCR.AmberThreshold THEN 'Green' ELSE 'Red' END AS RiskColour		
FROM RiskClaimRun  RCR
INNER JOIN RiskClaim RC ON RCR.RiskClaim_Id = RC.Id
INNER JOIN dbo.RiskBatch RB ON RB.ID = RC.RiskBatch_Id
INNER JOIN dbo.RiskClient RCL ON RCL.ID = RB.RiskClient_Id
WHERE RCR.RiskClaim_Id = @RiskClaim_Id
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '***** Object:  StoredProcedure [dbo].[usp_rpt_Level_One_Report_Incident_And_Policy]    Script Date: 02/11/2014 09:43:02 *****', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'usp_rpt_Level_One_Report_Incident_And_Policy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '93', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'usp_rpt_Level_One_Report_Incident_And_Policy', @level2type = N'PARAMETER', @level2name = N'@RiskClaim_Id';

