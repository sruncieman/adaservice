﻿CREATE TABLE [dbo].[MatchingRulesPolicy] (
    [RuleNo]       INT NOT NULL,
    [RiskClaim_Id] BIT NULL,
    [PolicyNumber] BIT NULL,
    [MatchType]    INT NULL, 
    CONSTRAINT [PK_MatchingRulesPolicy] PRIMARY KEY ([RuleNo])
);

