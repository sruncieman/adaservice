﻿CREATE TABLE [dbo].[Person2Person] (
    [Id]                       INT             IDENTITY (1, 1) NOT NULL,
    [Person1_Id]               INT             NOT NULL,
    [Person2_Id]               INT             NOT NULL,
    [Person2PersonLinkId]      NVARCHAR (50)   NULL,
    [Person2PersonLinkType_Id] INT             NOT NULL,
    [FiveGrading]              NVARCHAR (10)   NULL,
    [LinkConfidence]           INT             DEFAULT ((0)) NOT NULL,
    [Source]                   NVARCHAR (50)   NULL,
    [SourceReference]          NVARCHAR (50)   NULL,
    [SourceDescription]        NVARCHAR (1024) NULL,
    [CreatedBy]                NVARCHAR (50)   NOT NULL,
    [CreatedDate]              SMALLDATETIME   NOT NULL,
    [ModifiedBy]               NVARCHAR (50)   NULL,
    [ModifiedDate]             SMALLDATETIME   NULL,
    [IBaseId]                  NVARCHAR (50)   NULL,
    [RecordStatus]             TINYINT         CONSTRAINT [DF_Person2Person_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]          TINYINT         CONSTRAINT [DF_Person2Person_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]               ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2Person] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2Person_Person1] FOREIGN KEY ([Person1_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2Person_Person2] FOREIGN KEY ([Person2_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2Person_Person2PersonLinkType] FOREIGN KEY ([Person2PersonLinkType_Id]) REFERENCES [dbo].[Person2PersonLinkType] ([Id])
);


GO
ALTER TABLE [dbo].[Person2Person] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO

CREATE NONCLUSTERED INDEX [IX_Person2Person_Person2_Id]
    ON [dbo].[Person2Person]([Person2_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Person_Person1_Id]
    ON [dbo].[Person2Person]([Person1_Id] ASC);

