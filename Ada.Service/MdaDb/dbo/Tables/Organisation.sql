﻿CREATE TABLE [dbo].[Organisation] (
    [Id]                        INT             IDENTITY (1, 1) NOT NULL,
    [OrganisationId]            NVARCHAR (50)   NULL,
    [OrganisationName]          NVARCHAR (255)  NULL,
    [OrganisationType_Id]       INT             NOT NULL,
    [IncorporatedDate]          SMALLDATETIME   NULL,
    [OrganisationStatus_Id]     INT             NOT NULL,
    [EffectiveDateOfStatus]     SMALLDATETIME   NULL,
    [RegisteredNumber]          NVARCHAR (50)   NULL,
    [SIC]                       NVARCHAR (50)   NULL,
    [VATNumber]                 NVARCHAR (50)   NULL,
    [VATNumberValidationDate]   SMALLDATETIME   NULL,
    [MojCRMNumber]              NVARCHAR (10)   NULL,
    [MojCRMStatus_Id]           INT             NOT NULL,
    [CreditLicense]             NVARCHAR (50)   NULL,
    [KeyAttractor]              NVARCHAR (100)  NULL,
    [CHFKeyAttractor]           BIT             CONSTRAINT [DF_Organisation_KISKeyAttractor] DEFAULT ((0)) NOT NULL,
    [CHFKeyAttractorRemoveDate] SMALLDATETIME   NULL,
    [Source]                    NVARCHAR (50)   NULL,
    [SourceReference]           NVARCHAR (50)   NULL,
    [SourceDescription]         NVARCHAR (1024) NULL,
    [CreatedBy]                 NVARCHAR (50)   NOT NULL,
    [CreatedDate]               SMALLDATETIME   NOT NULL,
    [ModifiedBy]                NVARCHAR (50)   NULL,
    [ModifiedDate]              SMALLDATETIME   NULL,
    [IBaseId]                   NVARCHAR (50)   NULL,
    [RecordStatus]              TINYINT         CONSTRAINT [DF_Organisation_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]           TINYINT         CONSTRAINT [DF_Organisation_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]                ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Organisation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation_MojCRMStatus] FOREIGN KEY ([MojCRMStatus_Id]) REFERENCES [dbo].[MojCRMStatus] ([Id]),
    CONSTRAINT [FK_Organisation_OrganisationStatus] FOREIGN KEY ([OrganisationStatus_Id]) REFERENCES [dbo].[OrganisationStatus] ([Id]),
    CONSTRAINT [FK_Organisation_OrganisationType] FOREIGN KEY ([OrganisationType_Id]) REFERENCES [dbo].[OrganisationType] ([Id])
);


GO
ALTER TABLE [dbo].[Organisation] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);










GO























GO
CREATE NONCLUSTERED INDEX [IX_Organisation_VATNum]
    ON [dbo].[Organisation]([VATNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation_RegNum]
    ON [dbo].[Organisation]([RegisteredNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation_Name]
    ON [dbo].[Organisation]([OrganisationName] ASC);


GO

