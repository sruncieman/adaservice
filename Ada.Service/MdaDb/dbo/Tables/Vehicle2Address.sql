﻿CREATE TABLE [dbo].[Vehicle2Address] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [Vehicle_Id]         INT             NOT NULL,
    [Address_Id]         INT             NOT NULL,
    [AddressLinkId]      NVARCHAR (50)   NULL,
    [AddressLinkType_Id] INT             NOT NULL,
    [StartOfResidency]   SMALLDATETIME   NULL,
    [EndOfResidency]     SMALLDATETIME   NULL,
    [RawAddress]         NVARCHAR (512)  NULL,
    [Notes]              NVARCHAR (1024) NULL,
    [FiveGrading]        NVARCHAR (10)   NULL,
    [LinkConfidence]     INT             DEFAULT ((0)) NOT NULL,
    [Source]             NVARCHAR (50)   NULL,
    [SourceReference]    NVARCHAR (50)   NULL,
    [SourceDescription]  NVARCHAR (1024) NULL,
    [CreatedBy]          NVARCHAR (50)   NOT NULL,
    [CreatedDate]        SMALLDATETIME   NOT NULL,
    [ModifiedBy]         NVARCHAR (50)   NULL,
    [ModifiedDate]       SMALLDATETIME   NULL,
    [RiskClaim_Id]       INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]   INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]            NVARCHAR (50)   NULL,
    [RecordStatus]       TINYINT         CONSTRAINT [DF_Vehicle2Address_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]    TINYINT         CONSTRAINT [DF_Vehicle2Address_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]         ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Vehicle2Address] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vehicle2Address_Address] FOREIGN KEY ([Address_Id]) REFERENCES [dbo].[Address] ([Id]),
    CONSTRAINT [FK_Vehicle2Address_AddressLinkType] FOREIGN KEY ([AddressLinkType_Id]) REFERENCES [dbo].[AddressLinkType] ([Id]),
    CONSTRAINT [FK_Vehicle2Address_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Vehicle2Address_Vehicle] FOREIGN KEY ([Vehicle_Id]) REFERENCES [dbo].[Vehicle] ([Id])
);


GO
ALTER TABLE [dbo].[Vehicle2Address] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






















GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Address_RiskClaimId]
    ON [dbo].[Vehicle2Address]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Address_Vehicle_Id]
    ON [dbo].[Vehicle2Address]([Vehicle_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Address_Address_Id]
    ON [dbo].[Vehicle2Address]([Address_Id] ASC);

