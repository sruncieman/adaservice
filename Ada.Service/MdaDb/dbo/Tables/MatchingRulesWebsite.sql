﻿CREATE TABLE [dbo].[MatchingRulesWebsite] (
    [RuleNo]        INT NOT NULL,
    [RiskClaim_Id]  BIT NULL,
    [URL]           BIT NULL,
    [PriorityGroup] INT NULL,
    [MatchType]     INT NULL, 
    CONSTRAINT [PK_MatchingRulesWebsite] PRIMARY KEY ([RuleNo])
);

