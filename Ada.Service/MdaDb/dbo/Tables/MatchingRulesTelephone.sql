﻿CREATE TABLE [dbo].[MatchingRulesTelephone] (
    [RuleNo]          INT NOT NULL,
    [RiskClaim_Id]    BIT NULL,
    [TelephoneNumber] BIT NULL,
    [TelephoneType]   BIT NULL,
    [MatchType]       INT NULL, 
    CONSTRAINT [PK_MatchingRulesTelephone] PRIMARY KEY ([RuleNo])
);

