﻿CREATE TABLE [dbo].[IncidentType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [IncidentType]    NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_IncidentType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_IncidentType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_IncidentType] PRIMARY KEY CLUSTERED ([Id] ASC)
);









