﻿CREATE TABLE [dbo].[Telephone] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [TelephoneId]       NVARCHAR (50)   NULL,
    [TelephoneNumber]   NVARCHAR (50)   NULL,
    [CountryCode]       NVARCHAR (10)   NULL,
    [STDCode]           VARCHAR (10)    NULL,
    [TelephoneType_Id]  INT             NOT NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Telephone_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Telephone_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Telephone] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Telephone_TelephoneType] FOREIGN KEY ([TelephoneType_Id]) REFERENCES [dbo].[TelephoneType] ([Id])
);


GO
ALTER TABLE [dbo].[Telephone] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);














GO
CREATE NONCLUSTERED INDEX [IX_Telephone_Number]
    ON [dbo].[Telephone]([TelephoneNumber] ASC);

