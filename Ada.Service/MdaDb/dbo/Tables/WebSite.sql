﻿CREATE TABLE [dbo].[WebSite] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [WebSiteId]         NVARCHAR (50)   NULL,
    [URL]               NVARCHAR (255)  NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_WebSite_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_WebSite_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_WebSite] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
ALTER TABLE [dbo].[WebSite] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);












GO
CREATE NONCLUSTERED INDEX [IX_WebSite_URL]
    ON [dbo].[WebSite]([URL] ASC);

