﻿CREATE TABLE [dbo].[RiskExternalServicesRequests] (
    [RiskClaim_Id]           INT            NOT NULL,
    [RiskExternalService_Id] INT            NOT NULL,
    [Unique_Id]              INT            NOT NULL,
    [CallStatus]             INT            DEFAULT ((0)) NOT NULL,
    [ServicesCalledMask]     INT            DEFAULT ((0)) NOT NULL,
    [ErrorMessage]           VARCHAR (1024) NULL,
    [IsRepeatCall]           BIT            DEFAULT ((0)) NOT NULL,
    [RequestedBy]            VARCHAR (50)   NOT NULL,
    [RequestedWhen]          SMALLDATETIME  NOT NULL,
    [ReturnedData]           VARCHAR (MAX)  NULL,
    [RequestData]            VARCHAR (MAX)  NULL,
    [RiskReport_Id]          INT            NULL,
    [ReturnedDataFormat]     VARCHAR (4)    NULL,
    [RequestDataFormat]      VARCHAR (4)    NULL,
    CONSTRAINT [PK_RiskExternalServiceHistory] PRIMARY KEY CLUSTERED ([RiskClaim_Id] ASC, [RiskExternalService_Id] ASC, [Unique_Id] ASC),
    CONSTRAINT [FK_RiskExternalServiceHistory_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_RiskExternalServiceHistory_RiskExternalServices] FOREIGN KEY ([RiskExternalService_Id]) REFERENCES [dbo].[RiskExternalServices] ([Id])
);




