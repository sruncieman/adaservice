﻿CREATE TABLE [dbo].[MatchingRulesEmail] (
    [RuleNo]        INT NOT NULL,
    [RiskClaim_Id]  BIT NULL,
    [EmailAddress]  BIT NULL,
    [PriorityGroup] INT NULL,
    [MatchType]     INT NULL, 
    CONSTRAINT [PK_MatchingRulesEmail] PRIMARY KEY ([RuleNo])
);

