﻿CREATE TABLE [dbo].[TelephoneType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [PhoneText]       NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_TelephoneType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_TelephoneType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TelephoneType] PRIMARY KEY CLUSTERED ([Id] ASC)
);









