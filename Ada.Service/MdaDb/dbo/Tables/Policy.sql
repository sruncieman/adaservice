﻿CREATE TABLE [dbo].[Policy] (
    [Id]                         INT             IDENTITY (1, 1) NOT NULL,
    [PolicyId]                   NVARCHAR (50)   NULL,
    [PolicyNumber]               NVARCHAR (50)   NULL,
    [Insurer]                    VARCHAR (50)    NULL,
    [InsurerTradingName]         VARCHAR (50)    NULL,
    [Broker]                     VARCHAR (50)    NULL,
    [PolicyType_Id]              INT             NOT NULL,
    [PolicyCoverType_Id]         INT             NOT NULL,
    [PolicyStartDate]            SMALLDATETIME   NULL,
    [PolicyEndDate]              SMALLDATETIME   NULL,
    [PreviousNoFaultClaimsCount] INT             CONSTRAINT [DF_Policy_PreviousNoFaultClaimsCount] DEFAULT ((0)) NULL,
    [PreviousFaultClaimsCount]   INT             CONSTRAINT [DF_Policy_PreviousFaultClaimsCount] DEFAULT ((0)) NULL,
    [Premium]                    MONEY           NULL,
    [KeyAttractor]               NVARCHAR (100)  NULL,
    [Source]                     NVARCHAR (50)   NULL,
    [SourceReference]            NVARCHAR (50)   NULL,
    [SourceDescription]          NVARCHAR (1024) NULL,
    [CreatedBy]                  NVARCHAR (50)   NOT NULL,
    [CreatedDate]                SMALLDATETIME   NOT NULL,
    [ModifiedBy]                 NVARCHAR (50)   NULL,
    [ModifiedDate]               SMALLDATETIME   NULL,
    [IBaseId]                    NVARCHAR (50)   NULL,
    [RecordStatus]               TINYINT         CONSTRAINT [DF_Policy_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]            TINYINT         CONSTRAINT [DF_Policy_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]                 ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Policy] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Policy_PolicyCoverType] FOREIGN KEY ([PolicyCoverType_Id]) REFERENCES [dbo].[PolicyCoverType] ([Id]),
    CONSTRAINT [FK_Policy2TypePolicy] FOREIGN KEY ([PolicyType_Id]) REFERENCES [dbo].[PolicyType] ([Id])
);


GO
ALTER TABLE [dbo].[Policy] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);














GO
CREATE NONCLUSTERED INDEX [IX_FK_Policy2TypePolicy]
    ON [dbo].[Policy]([PolicyType_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Policy_Number]
    ON [dbo].[Policy]([PolicyNumber] ASC);

