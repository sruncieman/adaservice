﻿CREATE TABLE [dbo].[Staging_Accounts] (
    [Account_Rule No] NVARCHAR (255) NULL,
    [Claim]           FLOAT (53)     NULL,
    [Account]         FLOAT (53)     NULL,
    [F4]              FLOAT (53)     NULL,
    [F5]              FLOAT (53)     NULL,
    [Priority Group]  FLOAT (53)     NULL,
    [Matching Type]   FLOAT (53)     NULL,
    [Notes]           NVARCHAR (255) NULL,
    [F9]              NVARCHAR (255) NULL,
    [1]               NVARCHAR (255) NULL,
    [0]               FLOAT (53)     NULL,
    [Address]         NVARCHAR (255) NULL
);

