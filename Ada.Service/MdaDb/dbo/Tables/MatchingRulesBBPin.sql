﻿CREATE TABLE [dbo].[MatchingRulesBBPin] (
    [RuleNo]        INT NOT NULL,
    [RiskClaim_Id]  BIT NULL,
    [BBPin]         BIT NULL,
    [DisplayName]   BIT NULL,
    [PriorityGroup] INT NULL,
    [MatchType]     INT NULL, 
    CONSTRAINT [PK_MatchingRulesBBPin] PRIMARY KEY ([RuleNo])
);

