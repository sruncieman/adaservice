﻿CREATE TABLE [dbo].[Matching_Debug] (
    [Date_Time]     DATETIME       NOT NULL,
    [UserName]      NVARCHAR (128) NULL,
    [ProcedureName] VARCHAR (50)   NULL,
    [Xml_]          VARCHAR (3000) NULL,
    [Id]            INT            NULL,
    [RuleNo]        INT            NULL,
    [PriorityGroup] INT            NULL,
    [MatchType]     INT            NULL
);

