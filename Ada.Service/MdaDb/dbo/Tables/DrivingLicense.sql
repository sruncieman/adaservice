﻿CREATE TABLE [dbo].[DrivingLicense] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [DrivingLicenseId]  NVARCHAR (50)   NULL,
    [DriverNumber]      NVARCHAR (50)   NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_DrivingLicense_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_DrivingLicense_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_DrivingLicense] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
ALTER TABLE [dbo].[DrivingLicense] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);












GO
CREATE NONCLUSTERED INDEX [IX_DrivingLicense_Number]
    ON [dbo].[DrivingLicense]([DriverNumber] ASC);

