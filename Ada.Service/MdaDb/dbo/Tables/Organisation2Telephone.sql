﻿CREATE TABLE [dbo].[Organisation2Telephone] (
    [Id]                   INT             IDENTITY (1, 1) NOT NULL,
    [Organisation_Id]      INT             NOT NULL,
    [Telephone_Id]         INT             NOT NULL,
    [TelephoneLinkId]      NVARCHAR (50)   NULL,
    [TelephoneLinkType_Id] INT             DEFAULT ((0)) NOT NULL,
    [FiveGrading]          NVARCHAR (10)   NULL,
    [LinkConfidence]       INT             DEFAULT ((0)) NOT NULL,
    [Source]               NVARCHAR (50)   NULL,
    [SourceReference]      NVARCHAR (50)   NULL,
    [SourceDescription]    NVARCHAR (1024) NULL,
    [CreatedBy]            NVARCHAR (50)   NOT NULL,
    [CreatedDate]          SMALLDATETIME   NOT NULL,
    [ModifiedBy]           NVARCHAR (50)   NULL,
    [ModifiedDate]         SMALLDATETIME   NULL,
    [RiskClaim_Id]         INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]     INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]              NVARCHAR (50)   NULL,
    [RecordStatus]         TINYINT         CONSTRAINT [DF_Organisation2Telephone_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]      TINYINT         CONSTRAINT [DF_Organisation2Telephone_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]           ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Organisation2Telephone] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation2Telephone_Organisation] FOREIGN KEY ([Organisation_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Organisation2Telephone_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Organisation2Telephone_Telephone] FOREIGN KEY ([Telephone_Id]) REFERENCES [dbo].[Telephone] ([Id]),
    CONSTRAINT [FK_Organisation2Telephone_TelephoneLinkType] FOREIGN KEY ([TelephoneLinkType_Id]) REFERENCES [dbo].[TelephoneLinkType] ([Id])
);


GO
ALTER TABLE [dbo].[Organisation2Telephone] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






















GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Telephone_RiskClaimId]
    ON [dbo].[Organisation2Telephone]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Telephone_Telephone_Id]
    ON [dbo].[Organisation2Telephone]([Telephone_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Telephone_Organisation_Id]
    ON [dbo].[Organisation2Telephone]([Organisation_Id] ASC);

