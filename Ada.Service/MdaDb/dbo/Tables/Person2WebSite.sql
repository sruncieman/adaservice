﻿CREATE TABLE [dbo].[Person2WebSite] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Person_Id]         INT             NOT NULL,
    [WebSite_Id]        INT             NOT NULL,
    [WebSiteLinkId]     NVARCHAR (50)   NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Person2WebSite_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Person2WebSite_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2WebSite] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2WebSite_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2WebSite_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Person2WebSite_WebSite] FOREIGN KEY ([WebSite_Id]) REFERENCES [dbo].[WebSite] ([Id])
);


GO
ALTER TABLE [dbo].[Person2WebSite] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO
CREATE NONCLUSTERED INDEX [IX_Person2WebSite_RiskClaimId]
    ON [dbo].[Person2WebSite]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2WebSite_WebSite_Id]
    ON [dbo].[Person2WebSite]([WebSite_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2WebSite_Person_Id]
    ON [dbo].[Person2WebSite]([Person_Id] ASC);

