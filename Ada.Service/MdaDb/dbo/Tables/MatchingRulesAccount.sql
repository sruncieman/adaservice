﻿CREATE TABLE [dbo].[MatchingRulesAccount] (
    [RuleNo]        INT NOT NULL,
    [RiskClaim_Id]  BIT NULL,
    [AccountNumber] BIT NULL,
    [SortCode]      BIT NULL,
    [BankName]      BIT NULL,
    [PriorityGroup] INT NULL,
    [MatchType]     INT NULL, 
    CONSTRAINT [PK_MatchingRulesAccount] PRIMARY KEY ([RuleNo])
);

