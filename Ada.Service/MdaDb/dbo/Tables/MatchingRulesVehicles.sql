﻿CREATE TABLE [dbo].[MatchingRulesVehicles] (
    [RuleNo]              INT NOT NULL,
    [VehicleRegistration] BIT NULL,
    [VIN]                 BIT NULL,
    [VehicleMake]         BIT NULL,
    [Model]               BIT NULL,
    [Colour]              BIT NULL,
    [PostCode]            BIT NULL,
    [BuildingNumber]      BIT NULL,
    [PafUPRN]             BIT NULL,
    [PriorityGroup]       INT NULL,
    [MatchType]           INT NULL,
    [RiskClaimFlag]       INT NOT NULL,
    CONSTRAINT [PK_MatchingRulesVehicles] PRIMARY KEY CLUSTERED ([RuleNo] ASC)
);





