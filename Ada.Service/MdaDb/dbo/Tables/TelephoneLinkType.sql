﻿CREATE TABLE [dbo].[TelephoneLinkType] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [Text]            NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_TelephoneLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_TelephoneLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TelephoneLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);







