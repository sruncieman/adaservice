﻿CREATE TABLE [dbo].[Incident2Organisation] (
    [Id]                    INT             IDENTITY (1, 1) NOT NULL,
    [Incident_Id]           INT             NOT NULL,
    [Organisation_Id]       INT             NOT NULL,
    [IsClaimLevelLink]      BIT             CONSTRAINT [DF_Incident2Organisation_IsClaimLevelLink] DEFAULT ((0)) NOT NULL,
    [IncidentLinkId]        NVARCHAR (50)   NULL,
    [OrganisationType_Id]   INT             CONSTRAINT [DF_Incident2Organisation_OrganisationType_Id] DEFAULT ((0)) NOT NULL,
    [PartyType_Id]          INT             NOT NULL,
    [SubPartyType_Id]       INT             NOT NULL,
    [IncidentTime]          TIME (7)        NULL,
    [IncidentCircs]         NVARCHAR (1024) NULL,
    [IncidentLocation]      NVARCHAR (255)  NULL,
    [PaymentsToDate]        MONEY           NULL,
    [Reserve]               MONEY           NULL,
    [Insurer]               NVARCHAR (255)  NULL,
    [ClaimNumber]           NVARCHAR (50)   NULL,
    [ClaimStatus_Id]        INT             CONSTRAINT [DF_Incident2Organisation_ClaimStatus] DEFAULT ((0)) NOT NULL,
    [ClaimType]             NVARCHAR (50)   NULL,
    [ClaimCode]             NVARCHAR (80)   NULL,
    [MojStatus_Id]          INT             CONSTRAINT [DF_Incident2Organisation_MojStatus] DEFAULT ((0)) NOT NULL,
    [ClaimNotificationDate] SMALLDATETIME   NULL,
    [Broker]                NVARCHAR (255)  NULL,
    [ReferralSource]        NVARCHAR (255)  NULL,
    [Solicitors]            NVARCHAR (255)  NULL,
    [Engineer]              NVARCHAR (255)  NULL,
    [Recovery]              NVARCHAR (255)  NULL,
    [RecoveryAddress]       NVARCHAR (255)  NULL,
    [Storage]               NVARCHAR (255)  NULL,
    [StorageAddress]        NVARCHAR (255)  NULL,
    [Repairer]              NVARCHAR (255)  NULL,
    [RepairerAddress]       NVARCHAR (255)  NULL,
    [Hire]                  NVARCHAR (255)  NULL,
    [AccidentManagement]    NVARCHAR (255)  NULL,
    [MedicalExaminer]       NVARCHAR (255)  NULL,
    [MedicalLegal]          NVARCHAR (255)  NULL,
    [InspectionAddress]     NVARCHAR (255)  NULL,
    [UndefinedSupplier]     NVARCHAR (255)  NULL,
    [PoliceAttended]        BIT             NULL,
    [PoliceForce]           NVARCHAR (50)   NULL,
    [PoliceReference]       NVARCHAR (50)   NULL,
    [AmbulanceAttended]     BIT             NULL,
    [AttendedHospital]      BIT             NULL,
    [FiveGrading]           NVARCHAR (10)   NULL,
    [LinkConfidence]        INT             CONSTRAINT [DF__tmp_ms_xx__LinkC__7C3A67EB] DEFAULT ((0)) NOT NULL,
    [Source]                NVARCHAR (50)   NULL,
    [SourceReference]       NVARCHAR (50)   NULL,
    [SourceDescription]     NVARCHAR (1024) NULL,
    [CreatedBy]             NVARCHAR (50)   NOT NULL,
    [CreatedDate]           SMALLDATETIME   NOT NULL,
    [ModifiedBy]            NVARCHAR (50)   NULL,
    [ModifiedDate]          SMALLDATETIME   NULL,
    [RiskClaim_Id]          INT             CONSTRAINT [DF__Incident2__RiskC__5FC911C6] DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]      INT             CONSTRAINT [DF__Incident2__BaseR__60BD35FF] DEFAULT ((0)) NOT NULL,
    [IBaseId]               NVARCHAR (50)   NULL,
    [RecordStatus]          TINYINT         CONSTRAINT [DF_Incident2Organisation_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]       TINYINT         CONSTRAINT [DF_Incident2Organisation_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]            ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Incident2Organisation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2Organisation_ClaimStatus] FOREIGN KEY ([ClaimStatus_Id]) REFERENCES [dbo].[ClaimStatus] ([Id]),
    CONSTRAINT [FK_Incident2Organisation_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2Organisation_MojStatus] FOREIGN KEY ([MojStatus_Id]) REFERENCES [dbo].[MojStatus] ([Id]),
    CONSTRAINT [FK_Incident2Organisation_Organisation] FOREIGN KEY ([Organisation_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Incident2Organisation_PartyType] FOREIGN KEY ([PartyType_Id]) REFERENCES [dbo].[PartyType] ([Id]),
    CONSTRAINT [FK_Incident2Organisation_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Incident2Organisation_SubPartyType] FOREIGN KEY ([SubPartyType_Id]) REFERENCES [dbo].[SubPartyType] ([Id])
);





GO
ALTER TABLE [dbo].[Incident2Organisation] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






































GO
CREATE NONCLUSTERED INDEX [IX_Incident2Organisation_RiskClaimId]
    ON [dbo].[Incident2Organisation]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Organisation_Organisation_Id]
    ON [dbo].[Incident2Organisation]([Organisation_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Organisation_Incident_Id]
    ON [dbo].[Incident2Organisation]([Incident_Id] ASC);

