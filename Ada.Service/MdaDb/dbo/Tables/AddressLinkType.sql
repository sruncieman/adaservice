﻿CREATE TABLE [dbo].[AddressLinkType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [LinkType]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_AddressLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_AddressLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AddressLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);









