﻿CREATE TABLE [dbo].[Vehicle2Policy] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Vehicle_Id]        INT             NOT NULL,
    [Policy_Id]         INT             NOT NULL,
    [PolicyLinkId]      NVARCHAR (50)   NULL,
    [PolicyLinkType_Id] INT             NOT NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Vehicle2Policy_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Vehicle2Policy_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Vehicle2Policy] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vehicle2Policy_Policy] FOREIGN KEY ([Policy_Id]) REFERENCES [dbo].[Policy] ([Id]),
    CONSTRAINT [FK_Vehicle2Policy_PolicyLinkType] FOREIGN KEY ([PolicyLinkType_Id]) REFERENCES [dbo].[PolicyLinkType] ([Id]),
    CONSTRAINT [FK_Vehicle2Policy_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Vehicle2Policy_Vehicle] FOREIGN KEY ([Vehicle_Id]) REFERENCES [dbo].[Vehicle] ([Id])
);


GO
ALTER TABLE [dbo].[Vehicle2Policy] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Policy_RiskClaimId]
    ON [dbo].[Vehicle2Policy]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Policy_Vehicle_Id]
    ON [dbo].[Vehicle2Policy]([Vehicle_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Policy_Policy_Id]
    ON [dbo].[Vehicle2Policy]([Policy_Id] ASC);

