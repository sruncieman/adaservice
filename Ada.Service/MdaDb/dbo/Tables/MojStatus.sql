﻿CREATE TABLE [dbo].[MojStatus] (
    [Id]   INT          IDENTITY (1, 1) NOT NULL,
    [Text] VARCHAR (50) NOT NULL,
	[RecordStatus]    TINYINT       CONSTRAINT [DF_MojStatus_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_MojStatus_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MojStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);















