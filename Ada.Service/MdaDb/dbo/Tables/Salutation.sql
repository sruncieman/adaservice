﻿CREATE TABLE [dbo].[Salutation] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [Text]            NVARCHAR (20) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_Salutation_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_Salutation_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Salutation] PRIMARY KEY CLUSTERED ([Id] ASC)
);









