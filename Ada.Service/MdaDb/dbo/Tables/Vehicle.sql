﻿CREATE TABLE [dbo].[Vehicle] (
    [Id]                       INT             IDENTITY (1, 1) NOT NULL,
    [VehicleId]                NVARCHAR (50)   NULL,
    [VehicleColour_Id]         INT             NOT NULL,
    [VehicleRegistration]      NVARCHAR (20)   NULL,
    [VehicleMake]              NVARCHAR (50)   NULL,
    [Model]                    NVARCHAR (50)   NULL,
    [EngineCapacity]           NVARCHAR (50)   NULL,
    [VIN]                      NVARCHAR (20)   NULL,
    [VehicleFuel_Id]           INT             NOT NULL,
    [VehicleTransmission_Id]   INT             NOT NULL,
    [VehicleType_Id]           INT             NOT NULL,
    [VehicleCategoryOfLoss_Id] INT             CONSTRAINT [DF_Vehicle_VehicleCategoryOfLoss_Id] DEFAULT ((0)) NOT NULL,
    [Notes]                    NVARCHAR (1024) NULL,
    [KeyAttractor]             NVARCHAR (100)  NULL,
    [Source]                   NVARCHAR (50)   NULL,
    [SourceReference]          NVARCHAR (50)   NULL,
    [SourceDescription]        NVARCHAR (1024) NULL,
    [CreatedBy]                NVARCHAR (50)   NOT NULL,
    [CreatedDate]              SMALLDATETIME   NOT NULL,
    [ModifiedBy]               NVARCHAR (50)   NULL,
    [ModifiedDate]             SMALLDATETIME   NULL,
    [IBaseId]                  NVARCHAR (50)   NULL,
    [RecordStatus]             TINYINT         CONSTRAINT [DF_Vehicle_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]          TINYINT         CONSTRAINT [DF_Vehicle_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]               ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vehicle_VehicleCategoryOfLoss] FOREIGN KEY ([VehicleCategoryOfLoss_Id]) REFERENCES [dbo].[VehicleCategoryOfLoss] ([Id]),
    CONSTRAINT [FK_Vehicle_VehicleColour1] FOREIGN KEY ([VehicleColour_Id]) REFERENCES [dbo].[VehicleColour] ([Id]),
    CONSTRAINT [FK_Vehicle_VehicleFuel] FOREIGN KEY ([VehicleFuel_Id]) REFERENCES [dbo].[VehicleFuel] ([Id]),
    CONSTRAINT [FK_Vehicle_VehicleTransmission] FOREIGN KEY ([VehicleTransmission_Id]) REFERENCES [dbo].[VehicleTransmission] ([Id]),
    CONSTRAINT [FK_VehicleTypeVehicle] FOREIGN KEY ([VehicleType_Id]) REFERENCES [dbo].[VehicleType] ([Id])
);


GO
ALTER TABLE [dbo].[Vehicle] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO



GO
CREATE NONCLUSTERED INDEX [IX_Vehicle_VIN]
    ON [dbo].[Vehicle]([VIN] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle_Reg]
    ON [dbo].[Vehicle]([VehicleRegistration] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle_MakeModel]
    ON [dbo].[Vehicle]([VehicleMake] ASC, [Model] ASC)
    INCLUDE([Id]);

