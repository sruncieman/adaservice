﻿CREATE TABLE [dbo].[PaymentCardType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [CardTypeText]    NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_PaymentCardType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_PaymentCardType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PaymentCardType] PRIMARY KEY CLUSTERED ([Id] ASC)
);









