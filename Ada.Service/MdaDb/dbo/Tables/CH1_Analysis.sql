﻿CREATE TABLE [dbo].[CH1_Analysis] (
    [id]        INT           IDENTITY (1, 1) NOT NULL,
    [ClientID]  INT           NULL,
    [tableName] VARCHAR (100) NULL,
    [RowCount]  INT           NULL,
    [BatchNo]   INT           NULL
);

