﻿CREATE TABLE [dbo].[rpt_Level_One_Report_Person_Tracesmart] (
    [RiskClaim_Id]                  INT NULL,
    [Person_Id]                     INT NULL,
    [Vehicle_Id]                    INT NULL,
    [Tracesmart_CallStatus]         INT NULL,
    [Tracesmart_ServicesCalledMask] INT NULL,
    [ExperianDOB]                   INT NULL,
    [CreditIvaCheck]                INT NULL,
    [BankAccountValidated]          INT NULL,
    [DrivingLicenseValidated]       INT NULL,
    [NINumberValidated]             INT NULL,
    [PassportMRZValid]              INT NULL,
    [PassportDOBValid]              INT NULL,
    [PassportGenderValid]           INT NULL,
    [PaymentCardNumberValid]        INT NULL,
    [LandlineValidated]             INT NULL,
    [LandlineStatus]                INT NULL,
    [MobileCurrentLocation]         INT NULL,
    [MobileStatus]                  INT NULL
);

