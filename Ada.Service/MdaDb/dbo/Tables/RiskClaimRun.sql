﻿CREATE TABLE [dbo].[RiskClaimRun] (
    [Id]                          INT           IDENTITY (1, 1) NOT NULL,
    [RiskClaim_Id]                INT           NOT NULL,
    [EntityType]                  VARCHAR (200) NULL,
    [ScoreEntityEncodeFormat]     VARCHAR (4)   DEFAULT ('xml') NOT NULL,
    [ScoreEntityXml]              VARCHAR (MAX) NULL,
    [SummaryMessagesEncodeFormat] VARCHAR (4)   DEFAULT ('xml') NOT NULL,
    [SummaryMessagesXml]          VARCHAR (MAX) NULL,
    [TotalScore]                  INT           CONSTRAINT [DF_RiskClaimRun_TotalScore] DEFAULT ((0)) NOT NULL,
    [TotalKeyAttractorCount]      INT           CONSTRAINT [DF_RiskClaimRun_TotalKeyAttractorCounte] DEFAULT ((0)) NOT NULL,
    [ReportOneVersion]            VARCHAR (10)  NULL,
    [ReportTwoVersion]            VARCHAR (10)  NULL,
    [ExecutedBy]                  VARCHAR (50)  NOT NULL,
    [ExecutedWhen]                SMALLDATETIME NOT NULL,
    [UploadedWhen]                SMALLDATETIME NOT NULL,
    [ClientBatchReference]        VARCHAR (50)  NULL,
    [ScoringDurationInMs]         INT           DEFAULT ((0)) NOT NULL,
    [AmberThreshold]              INT           NULL,
    [RedThreshold]                INT           NULL,
    [LatestVersion]               BIT           DEFAULT ((0)) NOT NULL,
    [FinalScoreStatus]            INT           DEFAULT ((0)) NOT NULL,
    [PrevTotalScore]              INT           NULL,
    [PrevTotalKeyAttractorCount]  INT           NULL,
    CONSTRAINT [PK_RiskClaimRun] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskClaimRun_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);












GO
CREATE NONCLUSTERED INDEX [IX_RiskClaimRun_RiskClaim_Id]
    ON [dbo].[RiskClaimRun]([RiskClaim_Id] ASC);

