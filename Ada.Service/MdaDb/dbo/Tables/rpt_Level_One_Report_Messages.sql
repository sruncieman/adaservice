﻿CREATE TABLE [dbo].[rpt_Level_One_Report_Messages] (
    [RiskClaim_Id]  INT           NULL,
    [Per_DbId]      VARCHAR (50)  NULL,
    [Veh_DbId]      VARCHAR (50)  NULL,
    [Add_DbId]      VARCHAR (50)  NULL,
    [Org_DbId]      VARCHAR (50)  NULL,
    [LocalName]     VARCHAR (50)  NULL,
    [Rule_Key]      VARCHAR (50)  NULL,
    [Rule_Set_Key]  VARCHAR (50)  NULL,
    [ClaimNumber]   VARCHAR (100) NULL,
    [Header]        VARCHAR (500) NULL,
    [MessageType]   VARCHAR (10)  NULL,
    [Message]       VARCHAR (MAX) NULL,
    [MessageHeader] VARCHAR (100) NULL,
    [ReportHeader]  VARCHAR (100) NULL
);





