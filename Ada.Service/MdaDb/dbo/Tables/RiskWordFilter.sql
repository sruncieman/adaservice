﻿CREATE TABLE [dbo].[RiskWordFilter] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [RiskClient_Id]      INT           NOT NULL,
    [TableName]          VARCHAR (50)  NOT NULL,
    [FieldName]          VARCHAR (50)  NOT NULL,
    [LookupWord]         VARCHAR (50)  NOT NULL,
    [ReplacementWord]    VARCHAR (50)  NULL,
    [ReplaceWholeString] BIT           CONSTRAINT [DF__RiskWordF__Repla__4282C7A2] DEFAULT ((0)) NOT NULL,
    [DateAdded]          SMALLDATETIME NOT NULL,
    [SearchType]         VARCHAR (50)  CONSTRAINT [DF__RiskWordF__Searc__4BD727B2] DEFAULT ('~') NOT NULL,
    CONSTRAINT [PK__RiskWord__3214EC07409A7F30] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskWordFilter_RiskClient] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id])
);




