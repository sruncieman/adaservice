﻿CREATE TABLE [dbo].[MatchingRulesDrivingLicense] (
    [RuleNo]        INT NOT NULL,
    [RiskClaim_Id]  BIT NULL,
    [DriverNumber]  BIT NULL,
    [PriorityGroup] INT NULL,
    [MatchType]     INT NULL, 
    CONSTRAINT [PK_MatchingRulesDrivingLicense] PRIMARY KEY ([RuleNo])
);

