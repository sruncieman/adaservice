﻿CREATE TABLE [dbo].[VehicleMake] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [Make]            NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_VehicleMake_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_VehicleMake_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VehicleMake] PRIMARY KEY CLUSTERED ([Id] ASC)
);









