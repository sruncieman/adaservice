﻿CREATE TABLE [dbo].[Incident2FraudRing] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Incident_Id]       INT             NOT NULL,
    [FraudRing_Id]      INT             NOT NULL,
    [FraudRingLinkId]   NVARCHAR (50)   NULL,
    [LinkType]          INT             NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Incident2FraudRing_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Incident2FraudRing_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Incident2FraudRing] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2FraudRing_FraudRing] FOREIGN KEY ([FraudRing_Id]) REFERENCES [dbo].[FraudRing] ([Id]),
    CONSTRAINT [FK_Incident2FraudRing_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2FraudRing_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Incident2FraudRing] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO
CREATE NONCLUSTERED INDEX [IX_Incident2FraudRing_RiskClaimId]
    ON [dbo].[Incident2FraudRing]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2FraudRing_Incident_Id]
    ON [dbo].[Incident2FraudRing]([Incident_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2FraudRing_FraudRing_Id]
    ON [dbo].[Incident2FraudRing]([FraudRing_Id] ASC);

