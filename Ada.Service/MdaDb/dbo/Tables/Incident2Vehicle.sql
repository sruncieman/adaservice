﻿CREATE TABLE [dbo].[Incident2Vehicle] (
    [Id]                          INT             IDENTITY (1, 1) NOT NULL,
    [Incident_Id]                 INT             NOT NULL,
    [Vehicle_Id]                  INT             NOT NULL,
    [VehicleIncidentLinkId]       NVARCHAR (50)   NULL,
    [Incident2VehicleLinkType_Id] INT             NOT NULL,
    [VehicleCategoryOfLoss_Id]    INT             NOT NULL,
    [DamageDescription]           NVARCHAR (250)  NULL,
    [FiveGrading]                 NVARCHAR (10)   NULL,
    [LinkConfidence]              INT             CONSTRAINT [DF__Incident2__LinkC__74794A92] DEFAULT ((0)) NOT NULL,
    [Source]                      NVARCHAR (50)   NULL,
    [SourceReference]             NVARCHAR (50)   NULL,
    [SourceDescription]           NVARCHAR (1024) NULL,
    [CreatedBy]                   NVARCHAR (50)   NOT NULL,
    [CreatedDate]                 SMALLDATETIME   NOT NULL,
    [ModifiedBy]                  NVARCHAR (50)   NULL,
    [ModifiedDate]                SMALLDATETIME   NULL,
    [RiskClaim_Id]                INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]            INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]                     NVARCHAR (50)   NULL,
    [RecordStatus]                TINYINT         CONSTRAINT [DF_Incident2Vehicle_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]             TINYINT         CONSTRAINT [DF_Incident2Vehicle_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]                  ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Incident2Vehicle] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2Vehicle_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2Vehicle_Incident2VehicleLinkType] FOREIGN KEY ([Incident2VehicleLinkType_Id]) REFERENCES [dbo].[Incident2VehicleLinkType] ([Id]),
    CONSTRAINT [FK_Incident2Vehicle_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Incident2Vehicle_Vehicle] FOREIGN KEY ([Vehicle_Id]) REFERENCES [dbo].[Vehicle] ([Id]),
    CONSTRAINT [FK_Incident2Vehicle_VehicleCategoryOfLoss] FOREIGN KEY ([VehicleCategoryOfLoss_Id]) REFERENCES [dbo].[VehicleCategoryOfLoss] ([Id])
);


GO
ALTER TABLE [dbo].[Incident2Vehicle] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






















GO
CREATE NONCLUSTERED INDEX [IX_Incident2Vehicle_RiskClaimId]
    ON [dbo].[Incident2Vehicle]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Vehicle_Vehicle_Id]
    ON [dbo].[Incident2Vehicle]([Vehicle_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Vehicle_Incident_id]
    ON [dbo].[Incident2Vehicle]([Incident_Id] ASC);

