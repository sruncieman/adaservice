﻿CREATE TABLE [dbo].[Organisation2Policy] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Organisation_Id]   INT             NOT NULL,
    [Policy_Id]         INT             NOT NULL,
    [PolicyLinkId]      NVARCHAR (50)   NULL,
    [PolicyLinkType_Id] INT             NOT NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Organisation2Policy_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Organisation2Policy_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Organisation2Policy] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation2Policy_Organisation] FOREIGN KEY ([Organisation_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Organisation2Policy_Policy] FOREIGN KEY ([Policy_Id]) REFERENCES [dbo].[Policy] ([Id]),
    CONSTRAINT [FK_Organisation2Policy_PolicyLinkType] FOREIGN KEY ([PolicyLinkType_Id]) REFERENCES [dbo].[PolicyLinkType] ([Id]),
    CONSTRAINT [FK_Organisation2Policy_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Organisation2Policy] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Policy_RiskClaimId]
    ON [dbo].[Organisation2Policy]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Policy_Policy_Id]
    ON [dbo].[Organisation2Policy]([Policy_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Policy_Organisation_Id]
    ON [dbo].[Organisation2Policy]([Organisation_Id] ASC);

