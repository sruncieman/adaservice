﻿CREATE TABLE [dbo].[PartyType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [PartyTypeText]   NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_PartyType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_PartyType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PartyType] PRIMARY KEY CLUSTERED ([Id] ASC)
);









