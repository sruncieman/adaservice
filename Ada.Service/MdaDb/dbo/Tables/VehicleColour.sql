﻿CREATE TABLE [dbo].[VehicleColour] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [Colour]          NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_VehicleColour_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_VehicleColour_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VehicleColour] PRIMARY KEY CLUSTERED ([Id] ASC)
);









