﻿CREATE TABLE [dbo].[RiskOriginalFile] (
    [PkId]        INT                        IDENTITY (1, 1) NOT NULL,
    [Id]          UNIQUEIDENTIFIER           DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [Description] NVARCHAR (64)              NOT NULL,
    [FileData]    VARBINARY (MAX) FILESTREAM NULL,
    PRIMARY KEY CLUSTERED ([PkId] ASC),
    UNIQUE NONCLUSTERED ([Id] ASC)
);

