﻿CREATE TABLE [dbo].[RiskClient2RiskExternalServices] (
    [RiskClient_Id]               INT          NOT NULL,
    [RiskExternalServices_Id]     INT          NOT NULL,
    [ServiceUsage]                INT          DEFAULT ((0)) NOT NULL,
    [ServicePassword]             VARCHAR (30) NULL,
    [ServiceUserName]             VARCHAR (30) NULL,
    [Tracesmart_Use_Passport]     SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_Telephone]    SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_Driving]      SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_Birth]        SMALLINT     DEFAULT ((0)) NOT NULL,
    [Tracesmart_Use_Smartlink]    SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_NI]           SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_CardNumber]   SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_BankAccount]  SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_Mobile]       SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_Crediva]      SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_CreditActive] SMALLINT     DEFAULT ((0)) NOT NULL,
    [Tracesmart_Use_NHS]          SMALLINT     DEFAULT ((0)) NOT NULL,
    [Tracesmart_Use_Cardavs]      SMALLINT     DEFAULT ((0)) NOT NULL,
    [Tracesmart_Use_MPan]         SMALLINT     DEFAULT ((0)) NOT NULL,
    [Tracesmart_Use_Address]      SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_DeathScreen]  SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_DoB]          SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_Sanction]     SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_Insolvency]   SMALLINT     DEFAULT ((1)) NOT NULL,
    [Tracesmart_Use_CCJ]          SMALLINT     DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_RiskClient2RiskExternalServices] PRIMARY KEY CLUSTERED ([RiskClient_Id] ASC, [RiskExternalServices_Id] ASC),
    CONSTRAINT [FK_RiskClient2RiskExternalServices_RiskClient] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id]),
    CONSTRAINT [FK_RiskClient2RiskExternalServices_RiskExternalServices] FOREIGN KEY ([RiskExternalServices_Id]) REFERENCES [dbo].[RiskExternalServices] ([Id])
);



