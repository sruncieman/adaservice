﻿CREATE TABLE [dbo].[RiskUser] (
    [Id]            INT          NOT NULL IDENTITY,
    [RiskRole_Id]   INT          NOT NULL DEFAULT 0,
    [UserName]      VARCHAR (50) NULL,
    [Password]      NVARCHAR(128) NULL,
    [RiskTeam_Id]   INT          NOT NULL DEFAULT 0,
    [RiskClient_Id] INT          NOT NULL DEFAULT 0,
    [FirstName] VARCHAR(50) NULL, 
    [LastName] VARCHAR(50) NULL, 
    [TelephoneNumber] VARCHAR(16) NULL, 
    [LastGoodLoginDate] SMALLDATETIME NULL, 
    [BadLoginCount] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK__RiskUser__3214EC272FCF1A8A] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskUser_RiskTeam] FOREIGN KEY ([RiskTeam_Id]) REFERENCES [dbo].[RiskTeam] ([Id]),
    CONSTRAINT [FK_RiskUser_ToRiskClient] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id]),
    CONSTRAINT [FK_RiskUser_ToRiskRole] FOREIGN KEY ([RiskRole_Id]) REFERENCES [dbo].[RiskRole] ([Id])
);


