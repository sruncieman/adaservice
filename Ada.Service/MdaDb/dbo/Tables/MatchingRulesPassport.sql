﻿CREATE TABLE [dbo].[MatchingRulesPassport] (
    [RuleNo]         INT NOT NULL,
    [RiskClaim_Id]   BIT NULL,
    [PassportNumber] BIT NULL,
    [PriorityGroup]  INT NULL,
    [MatchType]      INT NULL, 
    CONSTRAINT [PK_MatchingRulesPassport] PRIMARY KEY ([RuleNo])
);

