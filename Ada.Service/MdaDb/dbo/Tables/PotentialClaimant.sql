﻿CREATE TABLE [dbo].[PotentialClaimant] (
    [ID]           INT IDENTITY (1, 1) NOT NULL,
    [PartyType]    INT NOT NULL,
    [SubPartyType] INT NOT NULL,
    [Flag]         INT NOT NULL,
	[RecordStatus]    TINYINT       CONSTRAINT [DF_PotentialClaimant_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_PotentialClaimant_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PotentialClaimant] PRIMARY KEY CLUSTERED ([ID] ASC)
);

