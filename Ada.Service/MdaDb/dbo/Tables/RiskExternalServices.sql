﻿CREATE TABLE [dbo].[RiskExternalServices] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [ServiceName] VARCHAR (30) NOT NULL,
    [MaskValue] INT NOT NULL, 
    [CurrentStatus] INT NOT NULL DEFAULT 0, 
    [StatusMessage] VARCHAR(50) NULL, 
    [ServiceEnabled] BIT NOT NULL DEFAULT 0, 
    [StatusLastChecked] SMALLDATETIME NULL, 
    [StatusTimeout] INT NOT NULL DEFAULT 10, 
    CONSTRAINT [PK_RiskExternalServices] PRIMARY KEY CLUSTERED ([Id] ASC)
);

