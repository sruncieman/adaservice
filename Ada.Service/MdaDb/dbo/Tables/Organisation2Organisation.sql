﻿CREATE TABLE [dbo].[Organisation2Organisation] (
    [Id]                      INT             IDENTITY (1, 1) NOT NULL,
    [Organisation1_Id]        INT             NOT NULL,
    [Organisation2_Id]        INT             NOT NULL,
    [Org2OrgLinkId]           NVARCHAR (50)   NULL,
    [OrganisationLinkType_Id] INT             NOT NULL,
    [FiveGrading]             NVARCHAR (10)   NULL,
    [LinkConfidence]          INT             DEFAULT ((0)) NOT NULL,
    [Source]                  NVARCHAR (50)   NULL,
    [SourceReference]         NVARCHAR (50)   NULL,
    [SourceDescription]       NVARCHAR (1024) NULL,
    [CreatedBy]               NVARCHAR (50)   NOT NULL,
    [CreatedDate]             SMALLDATETIME   NOT NULL,
    [ModifiedBy]              NVARCHAR (50)   NULL,
    [ModifiedDate]            SMALLDATETIME   NULL,
    [IBaseId]                 NVARCHAR (50)   NULL,
    [RecordStatus]            TINYINT         CONSTRAINT [DF_Organisation2Organisation_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]         TINYINT         CONSTRAINT [DF_Organisation2Organisation_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]              ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Organisation2Organisation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation2Organisation_Organisation1] FOREIGN KEY ([Organisation1_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Organisation2Organisation_Organisation2] FOREIGN KEY ([Organisation2_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Organisation2Organisation_OrganisationLinkType] FOREIGN KEY ([OrganisationLinkType_Id]) REFERENCES [dbo].[OrganisationLinkType] ([Id])
);


GO
ALTER TABLE [dbo].[Organisation2Organisation] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO

CREATE NONCLUSTERED INDEX [IX_Organisation2Organisation_Organisation2_Id]
    ON [dbo].[Organisation2Organisation]([Organisation2_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Organisation_Organisation1_Id]
    ON [dbo].[Organisation2Organisation]([Organisation1_Id] ASC);

