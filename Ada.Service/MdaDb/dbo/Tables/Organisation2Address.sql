﻿CREATE TABLE [dbo].[Organisation2Address] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [Organisation_Id]    INT             NOT NULL,
    [Address_Id]         INT             NOT NULL,
    [AddressLinkId]      NVARCHAR (50)   NULL,
    [AddressLinkType_Id] INT             NOT NULL,
    [StartOfResidency]   SMALLDATETIME   NULL,
    [EndOfResidency]     SMALLDATETIME   NULL,
    [RawAddress]         NVARCHAR (512)  NULL,
    [Notes]              NVARCHAR (1024) NULL,
    [FiveGrading]        NVARCHAR (10)   NULL,
    [LinkConfidence]     INT             DEFAULT ((0)) NOT NULL,
    [Source]             NVARCHAR (50)   NULL,
    [SourceReference]    NVARCHAR (50)   NULL,
    [SourceDescription]  NVARCHAR (1024) NULL,
    [CreatedBy]          NVARCHAR (50)   NOT NULL,
    [CreatedDate]        SMALLDATETIME   NOT NULL,
    [ModifiedBy]         NVARCHAR (50)   NULL,
    [ModifiedDate]       SMALLDATETIME   NULL,
    [RiskClaim_Id]       INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]   INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]            NVARCHAR (50)   NULL,
    [RecordStatus]       TINYINT         CONSTRAINT [DF_Organisation2Address_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]    TINYINT         CONSTRAINT [DF_Organisation2Address_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]         ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Organisation2Address] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation2Address_Address] FOREIGN KEY ([Address_Id]) REFERENCES [dbo].[Address] ([Id]),
    CONSTRAINT [FK_Organisation2Address_AddressLinkType] FOREIGN KEY ([AddressLinkType_Id]) REFERENCES [dbo].[AddressLinkType] ([Id]),
    CONSTRAINT [FK_Organisation2Address_Organisation] FOREIGN KEY ([Organisation_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Organisation2Address_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Organisation2Address] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






















GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Address_RiskClaimId]
    ON [dbo].[Organisation2Address]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Address_Organisation_Id]
    ON [dbo].[Organisation2Address]([Organisation_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Address_Address_Id]
    ON [dbo].[Organisation2Address]([Address_Id] ASC);

