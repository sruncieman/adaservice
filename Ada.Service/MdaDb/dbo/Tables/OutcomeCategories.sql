﻿CREATE TABLE [dbo].[OutcomeCategories] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [CategoryTypeId] INT           NOT NULL,
    [CategoryType]   VARCHAR (30)  NOT NULL,
    [CategoryText]   VARCHAR (100) NOT NULL,
	[RecordStatus]    TINYINT       CONSTRAINT [DF_OutcomeCategories_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_OutcomeCategories_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_OutcomeCategories] PRIMARY KEY CLUSTERED ([Id] ASC)
);

