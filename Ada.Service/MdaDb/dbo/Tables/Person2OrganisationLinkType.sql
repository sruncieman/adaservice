﻿CREATE TABLE [dbo].[Person2OrganisationLinkType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [LinkTypeText]    NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_Person2OrganisationLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_Person2OrganisationLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PersonOrganisationLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);









