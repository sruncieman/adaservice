﻿CREATE TABLE [dbo].[UKAreaCodes] (
    [AreaCode]                INT           NOT NULL,
    [OfcomDefinition]         VARCHAR (500) NULL,
    [TraditionalBTDefinition] VARCHAR (500) NULL,
    CONSTRAINT [PK_UKAreaCodes] PRIMARY KEY CLUSTERED ([AreaCode] ASC)
);

