﻿CREATE TABLE [dbo].[VehicleTransmission] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [Text]            NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_VehicleTransmission_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_VehicleTransmission_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VehicleTransmission] PRIMARY KEY CLUSTERED ([Id] ASC)
);









