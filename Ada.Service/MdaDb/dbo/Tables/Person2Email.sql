﻿CREATE TABLE [dbo].[Person2Email] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Person_Id]         INT             NOT NULL,
    [Email_Id]          INT             NOT NULL,
    [EmailLinkId]       NVARCHAR (50)   NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Person2Email_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Person2Email_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2Email] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2Email_Email] FOREIGN KEY ([Email_Id]) REFERENCES [dbo].[Email] ([Id]),
    CONSTRAINT [FK_Person2Email_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2Email_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Person2Email] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO
CREATE NONCLUSTERED INDEX [IX_Person2Email_RiskClaimId]
    ON [dbo].[Person2Email]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Email_Person_Id]
    ON [dbo].[Person2Email]([Person_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Email_Email_Id]
    ON [dbo].[Person2Email]([Email_Id] ASC);

