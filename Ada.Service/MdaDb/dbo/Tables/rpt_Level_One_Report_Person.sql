﻿CREATE TABLE [dbo].[rpt_Level_One_Report_Person] (
    [RiskClaim_Id]                  INT           NULL,
    [Person_Id]                     INT           NULL,
    [Vehicle_Id]                    INT           NULL,
    [ConfirmedAliases]              VARCHAR (50)  NULL,
    [Gender]                        VARCHAR (50)  NULL,
    [Salutation]                    VARCHAR (50)  NULL,
    [FullName]                      VARCHAR (152) NULL,
    [DateOfBirth]                   DATE          NULL,
    [MobileNumber]                  VARCHAR (50)  NOT NULL,
    [NINumber]                      VARCHAR (50)  NULL,
    [PartyType]                     VARCHAR (50)  NULL,
    [SubPartyType]                  VARCHAR (50)  NULL,
    [PossibleAlias]                 VARCHAR (500) NULL,
    [Occupation]                    VARCHAR (500) NULL,
    [OtherLinkedAddresses]          VARCHAR (500) NULL,
    [LinkedTelephone]               VARCHAR (500) NULL,
    [SortOrder]                     INT           NULL,
    [MessageHeader]                 VARCHAR (100) NULL,
    [ReportHeader]                  VARCHAR (100) NULL,
    [Telephone]                     VARCHAR (200) NULL,
    [Nationality]                   VARCHAR (50)  NULL,
    [DrivingLicenceNumber]          VARCHAR (50)  NULL,
    [PassportNumber]                VARCHAR (50)  NULL,
    [OccupationClaim]               VARCHAR (50)  NULL,
    [EmailAddress]                  VARCHAR (50)  NULL,
    [BankAccount]                   VARCHAR (100) NULL,
    [LinkedNINumber]                VARCHAR (500) NULL,
    [LinkedDrivingLicenceNumber]    VARCHAR (500) NULL,
    [LinkedPassportNumber]          VARCHAR (500) NULL,
    [LinkedTaxiDriverLicenceNumber] VARCHAR (500) NULL,
    [LinkedEmailAddress]            VARCHAR (500) NULL,
    [LinkedBankAccount]             VARCHAR (500) NULL,
    [LinkedRelativesAssociates]     VARCHAR (500) NULL
);





