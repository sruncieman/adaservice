﻿CREATE TABLE [dbo].[Person2Address] (
    [Id]                   INT             IDENTITY (1, 1) NOT NULL,
    [Person_Id]            INT             NOT NULL,
    [Address_Id]           INT             NOT NULL,
    [AddressLinkId]        NVARCHAR (50)   NULL,
    [AddressLinkType_Id]   INT             NOT NULL,
    [StartOfResidency]     SMALLDATETIME   NULL,
    [EndOfResidency]       SMALLDATETIME   NULL,
    [RawAddress]           NVARCHAR (512)  NULL,
    [Notes]                NVARCHAR (1024) NULL,
    [TracesmartIKey]       VARCHAR (20)    NULL,
    [TracesmartAddrSource] NVARCHAR (255)  NULL,
    [TSIDUAMLResult]       NVARCHAR (50)   NULL,
    [TracesmartDOB]        INT             NULL,
    [ExperianDOB]          INT             NULL,
    [GoneAway]             NVARCHAR (20)   NULL,
    [Insolvency]           NVARCHAR (1)    NULL,
    [CCJHistory]           NVARCHAR (500)  NULL,
    [InsolvencyHistory]    NVARCHAR (500)  NULL,
    [DeathScreenMatchType] NVARCHAR (50)   NULL,
    [DeathScreenDoD]       NVARCHAR (20)   NULL,
    [DeathScreenRegNo]     NVARCHAR (20)   NULL,
    [CredivaCheck]         BIT             NULL,
    [FiveGrading]          NVARCHAR (10)   NULL,
    [LinkConfidence]       INT             DEFAULT ((0)) NOT NULL,
    [Source]               NVARCHAR (50)   NULL,
    [SourceReference]      NVARCHAR (50)   NULL,
    [SourceDescription]    NVARCHAR (1024) NULL,
    [CreatedBy]            NVARCHAR (50)   NOT NULL,
    [CreatedDate]          SMALLDATETIME   NOT NULL,
    [ModifiedBy]           NVARCHAR (50)   NULL,
    [ModifiedDate]         SMALLDATETIME   NULL,
    [RiskClaim_Id]         INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]     INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]              NVARCHAR (50)   NULL,
    [RecordStatus]         TINYINT         CONSTRAINT [DF_Person2Address_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]      TINYINT         CONSTRAINT [DF_Person2Address_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]           ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2Address] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2Address_Address] FOREIGN KEY ([Address_Id]) REFERENCES [dbo].[Address] ([Id]),
    CONSTRAINT [FK_Person2Address_AddressLinkType] FOREIGN KEY ([AddressLinkType_Id]) REFERENCES [dbo].[AddressLinkType] ([Id]),
    CONSTRAINT [FK_Person2Address_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2Address_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Person2Address] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






















GO
CREATE NONCLUSTERED INDEX [IX_Person2Address_RiskClaimId]
    ON [dbo].[Person2Address]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Address_Person_Id]
    ON [dbo].[Person2Address]([Person_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Address_Address_Id]
    ON [dbo].[Person2Address]([Address_Id] ASC);

