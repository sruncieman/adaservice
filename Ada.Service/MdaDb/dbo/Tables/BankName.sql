﻿CREATE TABLE [dbo].[BankName] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [BankName]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_BankName_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_BankName_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BankName] PRIMARY KEY CLUSTERED ([Id] ASC)
);









