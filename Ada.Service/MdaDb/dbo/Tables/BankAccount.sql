﻿CREATE TABLE [dbo].[BankAccount] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [AccountId]         NVARCHAR (50)   NULL,
    [AccountNumber]     VARCHAR (50)    NOT NULL,
    [SortCode]          NVARCHAR (12)   NULL,
    [BankName]          NVARCHAR (50)   NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_BankAccount_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_BankAccount_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
ALTER TABLE [dbo].[BankAccount] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


















GO
CREATE NONCLUSTERED INDEX [IX_BankAccount_SortCode]
    ON [dbo].[BankAccount]([SortCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_BankAccount_BankName]
    ON [dbo].[BankAccount]([BankName] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_BankAccount_AccountNum]
    ON [dbo].[BankAccount]([AccountNumber] ASC);

