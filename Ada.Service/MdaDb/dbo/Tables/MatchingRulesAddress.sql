﻿CREATE TABLE [dbo].[MatchingRulesAddress] (
    [RuleNo]           INT NOT NULL,
    [SubBuilding]      BIT NULL,
    [Building]         BIT NULL,
    [BuildingNumber]   BIT NULL,
    [Street]           BIT NULL,
    [Locality]         BIT NULL,
    [Town]             BIT NULL,
    [PostCode]         BIT NULL,
    [OrganisationName] BIT NULL,
    [PriorityGroup]    INT NULL,
    [MatchType]        INT NULL,
    [RiskClaimFlag]    INT NOT NULL,
    CONSTRAINT [PK_MatchingRulesAddress] PRIMARY KEY CLUSTERED ([RuleNo] ASC)
);





