﻿CREATE TABLE [dbo].[RiskClaimEditor] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Status]          VARCHAR (50)   NOT NULL,
    [UserId]          INT            NOT NULL,
    [ClientId]        INT            NOT NULL,
    [RiskClaimNumber] VARCHAR (50)   NOT NULL,
    [ClaimType]       VARCHAR (50)   NOT NULL,
    [IncidentDate]    DATETIME       NOT NULL,
    [ModifiedDate]    SMALLDATETIME  CONSTRAINT [DF_RiskClaimEditor_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      NVARCHAR (50)  NOT NULL,
    [JSON]            NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_RiskClaimEditor_1] PRIMARY KEY CLUSTERED ([Id] ASC)
);







