﻿CREATE TABLE [dbo].[MatchingRulesClaim] (
    [RuleNo]                  INT NOT NULL,
    [ClientClaimReference]    BIT NULL,
    [ClientID]                BIT NULL,
    [IncidentDate]            BIT NULL,
    [IncidentTime]            BIT NULL,
    [IncidentLocation]        BIT NULL,
    [PeopleMatch]             INT NULL,
    [VehicleMatch]            INT NULL,
    [PeopleUnconfirmedMatch]  INT NULL,
    [VehicleUnconfirmedMatch] INT NULL,
    [IncidentCircumstances]   BIT NULL,
    [PriorityGroup]           INT NULL,
    [MatchType]               INT NULL,
    [RiskClaimFlag]           INT NOT NULL,
    CONSTRAINT [PK_MatchingRulesClaim] PRIMARY KEY CLUSTERED ([RuleNo] ASC)
);



