﻿CREATE TABLE [dbo].[County] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [County]          NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_County_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_County_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_County] PRIMARY KEY CLUSTERED ([Id] ASC)
);









