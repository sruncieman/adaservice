﻿CREATE TABLE [dbo].[KeoghsOffice] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [OfficeName]      NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_KeoghsOffice_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_KeoghsOffice_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_KeoghsOffice] PRIMARY KEY CLUSTERED ([Id] ASC)
);









