﻿CREATE TABLE [dbo].[RiskClaim] (
    [Id]                               INT           IDENTITY (1, 1) NOT NULL,
    [RiskBatch_Id]                     INT           NOT NULL,
    [Incident_Id]                      INT           NOT NULL,
    [MdaClaimRef]                      VARCHAR (50)  NOT NULL,
    [BaseRiskClaim_Id]                 INT           NOT NULL,
    [SuppliedClaimStatus]              INT           CONSTRAINT [DF_RiskClaim_SuppliedClaimStatus] DEFAULT ((0)) NOT NULL,
    [ClaimStatus]                      INT           CONSTRAINT [DF_RiskClaim_ClaimStatus] DEFAULT ((0)) NOT NULL,
    [ClaimStatusLastModified]          DATETIME      CONSTRAINT [DF_RiskClaim_ClaimStatusLastModified] DEFAULT (getdate()) NOT NULL,
    [ClientClaimRefNumber]             VARCHAR (50)  NOT NULL,
    [LevelOneRequestedCount]           INT           CONSTRAINT [DF_RiskClaim_LevelOneRequestedCount] DEFAULT ((0)) NOT NULL,
    [LevelTwoRequestedCount]           INT           CONSTRAINT [DF_RiskClaim_LevelTwoRequestedCount] DEFAULT ((0)) NOT NULL,
    [LevelOneRequestedWhen]            SMALLDATETIME NULL,
    [LevelTwoRequestedWhen]            SMALLDATETIME NULL,
    [LevelOneRequestedWho]             VARCHAR (50)  NULL,
    [LevelTwoRequestedWho]             VARCHAR (50)  NULL,
    [LevelOneUnavailable]              BIT           CONSTRAINT [DF__tmp_ms_xx__Level__0114F6F8] DEFAULT ((0)) NOT NULL,
    [LevelTwoUnavailable]              BIT           CONSTRAINT [DF__tmp_ms_xx__Level__02091B31] DEFAULT ((0)) NOT NULL,
    [SourceReference]                  VARCHAR (50)  NOT NULL,
    [SourceEntityEncodeFormat]         VARCHAR (4)   CONSTRAINT [DF__tmp_ms_xx__Sourc__02FD3F6A] DEFAULT ('xml') NOT NULL,
    [SourceEntityXML]                  VARCHAR (MAX) NOT NULL,
    [EntityClassType]                  VARCHAR (100) NOT NULL,
    [CreatedDate]                      DATETIME      NOT NULL,
    [CreatedBy]                        NVARCHAR (50) NOT NULL,
    [LatestVersion]                    BIT           CONSTRAINT [DF__tmp_ms_xx__Lates__03F163A3] DEFAULT ((0)) NOT NULL,
    [LatestScored]                     BIT           CONSTRAINT [DF__tmp_ms_xx__Lates__04E587DC] DEFAULT ((0)) NOT NULL,
    [CallbackRequested]                BIT           CONSTRAINT [DF__tmp_ms_xx__Callb__05D9AC15] DEFAULT ((0)) NOT NULL,
    [CallbackRequestedWhen]            SMALLDATETIME NULL,
    [CallbackRequestedWho]             VARCHAR (50)  NULL,
    [ExternalServicesRequired]         INT           CONSTRAINT [DF__tmp_ms_xx__Exter__06CDD04E] DEFAULT ((0)) NOT NULL,
    [ExternalServicesRequestsRequired] INT           CONSTRAINT [DF__tmp_ms_xx__Exter__07C1F487] DEFAULT ((0)) NOT NULL,
    [ClaimReadStatus]                  INT           CONSTRAINT [DF__tmp_ms_xx__Claim__08B618C0] DEFAULT ((0)) NOT NULL,
    [LoadingDurationInMs]              INT           CONSTRAINT [DF__tmp_ms_xx__Loadi__09AA3CF9] DEFAULT ((0)) NOT NULL,
    [ValidationResults]                VARCHAR (MAX) NULL,
    [CleansingResults]                 VARCHAR (MAX) NULL,
    [VisibilityStatus]                 INT           CONSTRAINT [DF_RiskClaim_VisibilityStatus] DEFAULT ((0)) NOT NULL,
    [RiskReportLevel1_Id]              INT           NULL,
    [RiskReportLevel2_Id]              INT           NULL,
    CONSTRAINT [PK_RiskClaim] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskClaim_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_RiskClaim_RiskBatch] FOREIGN KEY ([RiskBatch_Id]) REFERENCES [dbo].[RiskBatch] ([Id]),
    CONSTRAINT [FK_RiskClaim_RiskReportsLev1] FOREIGN KEY ([RiskReportLevel1_Id]) REFERENCES [dbo].[RiskReports] ([PkId]),
    CONSTRAINT [FK_RiskClaim_RiskReportsLev2] FOREIGN KEY ([RiskReportLevel2_Id]) REFERENCES [dbo].[RiskReports] ([PkId])
);




























GO
CREATE NONCLUSTERED INDEX [IX_RiskClaim_RiskBatch_Id]
    ON [dbo].[RiskClaim]([RiskBatch_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RiskClaim_Incident_Id]
    ON [dbo].[RiskClaim]([Incident_Id] ASC);

