﻿CREATE TABLE [dbo].[RiskClaimRunMessages] (
    [Id]              INT            NOT NULL IDENTITY,
    [RiskBatch_Id]    INT            NOT NULL,
    [RiskClaim_Id]    INT            NOT NULL,
    [RiskClaimRun_Id] INT            NOT NULL,
    [LineNumber]      INT            NOT NULL,
	[Header]		  VARCHAR(500)   NOT NULL,
    [Message]         VARCHAR (2000) NOT NULL,
    [ReportType]      INT            NOT NULL,
    CONSTRAINT [PK_RiskClaimRunMessages] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskClaimRunMessages_RiskBatch] FOREIGN KEY ([RiskBatch_Id]) REFERENCES [dbo].[RiskBatch] ([Id]),
    CONSTRAINT [FK_RiskClaimRunMessages_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_RiskClaimRunMessages_RiskClaimRun] FOREIGN KEY ([RiskClaimRun_Id]) REFERENCES [dbo].[RiskClaimRun] ([Id])
);




