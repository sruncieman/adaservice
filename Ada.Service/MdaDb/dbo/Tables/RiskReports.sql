﻿CREATE TABLE [dbo].[RiskReports] (
    [PkId]            INT              IDENTITY (1, 1) NOT NULL,
    [Id]              UNIQUEIDENTIFIER CONSTRAINT [DF__RiskReports__Id__61873D06] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [Description]     NVARCHAR (64)    NOT NULL,
    [FileData]        VARBINARY (MAX)  NULL,
    [ReturnedData]    VARCHAR (MAX)    NULL,
    [ParentEntity_Id] INT              NULL,
    CONSTRAINT [PK__RiskRepo__A7C03FF85CC287E9] PRIMARY KEY CLUSTERED ([PkId] ASC),
    CONSTRAINT [UQ__RiskRepo__3214EC065F9EF494] UNIQUE NONCLUSTERED ([Id] ASC)
);



