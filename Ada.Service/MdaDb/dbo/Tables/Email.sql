﻿CREATE TABLE [dbo].[Email] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [EmailId]           NVARCHAR (50)   NULL,
    [EmailAddress]      NVARCHAR (255)  NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Email_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Email_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Email] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
ALTER TABLE [dbo].[Email] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);












GO
CREATE NONCLUSTERED INDEX [IX_Email]
    ON [dbo].[Email]([EmailAddress] ASC);

