﻿CREATE TABLE [dbo].[MatchingRulesPerson] (
    [RuleNo]              INT NOT NULL,
    [FirstName]           BIT NULL,
    [LastName]            BIT NULL,
    [DateOfBirth]         BIT NULL,
    [NINumber]            BIT NULL,
    [DriverNumber]        BIT NULL,
    [PassportNumber]      BIT NULL,
    [PaymentCardNumber]   BIT NULL,
    [AccountNumber]       BIT NULL,
    [PolicyNumber]        BIT NULL,
    [SubBuilding]         BIT NULL,
    [Building]            BIT NULL,
    [BuildingNumber]      BIT NULL,
    [Street]              BIT NULL,
    [Locality]            BIT NULL,
    [Town]                BIT NULL,
    [PostCode]            BIT NULL,
    [TelephoneNumber]     BIT NULL,
    [VehicleRegistration] BIT NULL,
    [VehicleVIN]          BIT NULL,
    [OrganisationName]    BIT NULL,
    [RegisteredNumber]    BIT NULL,
    [EmailAddress]        BIT NULL,
    [VATNumber]           BIT NULL,
    [PafUPRN]             BIT NULL,
    [PriorityGroup]       INT NULL,
    [MatchType]           INT NULL,
    [RiskClaimFlag]       INT NOT NULL,
    CONSTRAINT [PK_MatchingRulesPerson] PRIMARY KEY CLUSTERED ([RuleNo] ASC)
);





