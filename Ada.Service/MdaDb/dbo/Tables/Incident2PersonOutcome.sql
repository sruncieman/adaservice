﻿CREATE TABLE [dbo].[Incident2PersonOutcome] (
    [Id]                       INT             IDENTITY (1, 1) NOT NULL,
    [Incident_Id]              INT             NOT NULL,
    [Person_Id]                INT             NOT NULL,
    [OutcomeLinkId]            NVARCHAR (50)   NULL,
    [SettlementDate]           SMALLDATETIME   NULL,
    [MannerOfResolution]       NVARCHAR (100)  NULL,
    [OutcomeCategory_Id]       INT             NULL,
    [EnforcementRole]          NVARCHAR (100)  NULL,
    [PropertyOwned]            NVARCHAR (20)   NULL,
    [MethodOfEnforcement]      NVARCHAR (100)  NULL,
    [DateOfSettlementAgreed]   SMALLDATETIME   NULL,
    [SettlementAmountAgreed]   NVARCHAR (20)   NULL,
    [AmountRecoveredToDate]    NVARCHAR (20)   NULL,
    [AmountStillOutstanding]   NVARCHAR (20)   NULL,
    [WasEnforcementSuccessful] NVARCHAR (20)   NULL,
    [TypeOfSuccess]            NVARCHAR (100)  NULL,
    [FiveGrading]              NVARCHAR (10)   NULL,
    [LinkConfidence]           INT             CONSTRAINT [DF__Incident2__LinkC__2EA5EC27] DEFAULT ((0)) NOT NULL,
    [Source]                   NVARCHAR (50)   NULL,
    [SourceReference]          NVARCHAR (50)   NULL,
    [SourceDescription]        NVARCHAR (1024) NULL,
    [CreatedBy]                NVARCHAR (50)   NOT NULL,
    [CreatedDate]              SMALLDATETIME   NOT NULL,
    [ModifiedBy]               NVARCHAR (50)   NULL,
    [ModifiedDate]             SMALLDATETIME   NULL,
    [RiskClaim_Id]             INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]         INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]                  NVARCHAR (50)   NULL,
    [RecordStatus]             TINYINT         CONSTRAINT [DF_Incident2PersonOutcome_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]          TINYINT         CONSTRAINT [DF_Incident2PersonOutcome_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]               ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Incident2PersonOutcome] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2PersonOutcome_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2PersonOutcome_OutcomeCategories] FOREIGN KEY ([OutcomeCategory_Id]) REFERENCES [dbo].[OutcomeCategories] ([Id]),
    CONSTRAINT [FK_Incident2PersonOutcome_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Incident2PersonOutcome_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Incident2PersonOutcome] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

















GO
CREATE NONCLUSTERED INDEX [IX_Incident2PersonOutcome_RiskClaim_Id]
    ON [dbo].[Incident2PersonOutcome]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2PersonOutcome_Person_Id]
    ON [dbo].[Incident2PersonOutcome]([Person_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2PersonOutcome_Incident_Id]
    ON [dbo].[Incident2PersonOutcome]([Incident_Id] ASC);

