﻿CREATE TABLE [dbo].[Person2Policy] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Person_Id]         INT             NOT NULL,
    [Policy_Id]         INT             NOT NULL,
    [PolicyLinkId]      NVARCHAR (50)   NULL,
    [PolicyLinkType_Id] INT             NOT NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Person2Policy_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Person2Policy_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2Policy] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2Policy_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2Policy_Policy] FOREIGN KEY ([Policy_Id]) REFERENCES [dbo].[Policy] ([Id]),
    CONSTRAINT [FK_Person2Policy_PolicyLinkType] FOREIGN KEY ([PolicyLinkType_Id]) REFERENCES [dbo].[PolicyLinkType] ([Id]),
    CONSTRAINT [FK_Person2Policy_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Person2Policy] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO
CREATE NONCLUSTERED INDEX [IX_Person2Policy_RiskClaimId]
    ON [dbo].[Person2Policy]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Policy_Policy_Id]
    ON [dbo].[Person2Policy]([Policy_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Policy_Person_Id]
    ON [dbo].[Person2Policy]([Person_Id] ASC);

