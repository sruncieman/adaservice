﻿CREATE TABLE [dbo].[Incident2Address] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Incident_Id]       INT             NOT NULL,
    [Address_Id]        INT             NOT NULL,
    [AddressLinkId]     NVARCHAR (50)   NULL,
    [RawAddress]        NVARCHAR (512)  NULL,
    [LinkType]          INT             NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Incident2Address_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Incident2Address_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Incident2Address] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2Address_Address] FOREIGN KEY ([Address_Id]) REFERENCES [dbo].[Address] ([Id]),
    CONSTRAINT [FK_Incident2Address_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2Address_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Incident2Address] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






















GO
CREATE NONCLUSTERED INDEX [IX_Incident2Address_RiskClaimId]
    ON [dbo].[Incident2Address]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Address_Incident_Id]
    ON [dbo].[Incident2Address]([Incident_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Address_Address_Id]
    ON [dbo].[Incident2Address]([Address_Id] ASC);

