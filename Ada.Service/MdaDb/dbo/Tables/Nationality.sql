﻿CREATE TABLE [dbo].[Nationality] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [NationalityText] NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_Nationality_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_Nationality_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Nationality] PRIMARY KEY CLUSTERED ([Id] ASC)
);









