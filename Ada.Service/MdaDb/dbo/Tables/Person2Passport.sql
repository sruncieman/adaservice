﻿CREATE TABLE [dbo].[Person2Passport] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Person_Id]         INT             NOT NULL,
    [Passport_Id]       INT             NOT NULL,
    [PassportLinkId]    NVARCHAR (50)   NULL,
    [MRZValid]          BIT             NULL,
    [DOBValid]          BIT             NULL,
    [GenderValid]       BIT             NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Person2Passport_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Person2Passport_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2Passport] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2Passport_Passport] FOREIGN KEY ([Passport_Id]) REFERENCES [dbo].[Passport] ([Id]),
    CONSTRAINT [FK_Person2Passport_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2Passport_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Person2Passport] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO
CREATE NONCLUSTERED INDEX [IX_Person2Passport_RiskClaimId]
    ON [dbo].[Person2Passport]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Passport_Person_Id]
    ON [dbo].[Person2Passport]([Person_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Passport_Passport_Id]
    ON [dbo].[Person2Passport]([Passport_Id] ASC);

