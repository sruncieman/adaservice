﻿CREATE TABLE [dbo].[PolicyType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [Text]            NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_PolicyType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_PolicyType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PolicyType] PRIMARY KEY CLUSTERED ([Id] ASC)
);









