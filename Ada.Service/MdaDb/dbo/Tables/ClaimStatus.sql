﻿CREATE TABLE [dbo].[ClaimStatus] (
    [Id]              INT          IDENTITY (-1, 1) NOT NULL,
    [Text]            VARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT      CONSTRAINT [DF_ClaimStatus_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT      CONSTRAINT [DF_ClaimStatus_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ClaimStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);









