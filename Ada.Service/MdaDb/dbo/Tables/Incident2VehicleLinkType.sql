﻿CREATE TABLE [dbo].[Incident2VehicleLinkType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [Text]            NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_Incident2VehicleLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_Incident2VehicleLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Incident2VehicleLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);









