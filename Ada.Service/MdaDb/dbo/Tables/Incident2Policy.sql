﻿CREATE TABLE [dbo].[Incident2Policy] (
    [Id]                  INT             IDENTITY (1, 1) NOT NULL,
    [Incident_Id]         INT             NOT NULL,
    [Policy_Id]           INT             NOT NULL,
    [VehiclePolicyLinkId] NVARCHAR (50)   NULL,
    [FiveGrading]         NVARCHAR (10)   NULL,
    [LinkConfidence]      INT             CONSTRAINT [DF__Incident2Policy__LinkC__74794A92] DEFAULT ((0)) NOT NULL,
    [Source]              NVARCHAR (50)   NULL,
    [SourceReference]     NVARCHAR (50)   NULL,
    [SourceDescription]   NVARCHAR (1024) NULL,
    [CreatedBy]           NVARCHAR (50)   NOT NULL,
    [CreatedDate]         SMALLDATETIME   NOT NULL,
    [ModifiedBy]          NVARCHAR (50)   NULL,
    [ModifiedDate]        SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             NOT NULL DEFAULT 0,
    [BaseRiskClaim_Id]  INT             NOT NULL DEFAULT 0,
    [IBaseId]             NVARCHAR (50)   NULL,
    [RecordStatus]        TINYINT         CONSTRAINT [DF_Incident2Policy_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]     TINYINT         CONSTRAINT [DF_Incident2Policy_DeletedReason] DEFAULT ((0)) NOT NULL,
	[RowVersion] rowversion NOT NULL,
    CONSTRAINT [PK_Incident2Policy] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2Policy_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2Policy_Policy] FOREIGN KEY ([Policy_Id]) REFERENCES [dbo].[Policy] ([Id]),
    CONSTRAINT [FK_Incident2Policy_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Policy_RiskClaim_Id]
    ON [dbo].[Incident2Policy]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Policy_Policy_Id]
    ON [dbo].[Incident2Policy]([Policy_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Policy_Incident_Id]
    ON [dbo].[Incident2Policy]([Incident_Id] ASC);

