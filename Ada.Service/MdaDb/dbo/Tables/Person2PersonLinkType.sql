﻿CREATE TABLE [dbo].[Person2PersonLinkType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [LinkText]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_Person2PersonLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_Person2PersonLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Person2PersonLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);









