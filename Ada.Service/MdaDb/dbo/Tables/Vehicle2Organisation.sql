﻿CREATE TABLE [dbo].[Vehicle2Organisation] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [Vehicle_Id]         INT             NOT NULL,
    [Organisation_Id]    INT             NOT NULL,
    [VehicleLinkId]      NVARCHAR (50)   NULL,
    [VehicleLinkType_Id] INT             NOT NULL,
    [RegKeeperStartDate] SMALLDATETIME   NULL,
    [RegKeeperEndDate]   SMALLDATETIME   NULL,
    [HireCompany]        NVARCHAR (255)  NULL,
    [CrossHireCompany]   NVARCHAR (255)  NULL,
    [HireStartDate]      SMALLDATETIME   NULL,
    [HireEndDate]        SMALLDATETIME   NULL,
    [FiveGrading]        NVARCHAR (10)   NULL,
    [LinkConfidence]     INT             DEFAULT ((0)) NOT NULL,
    [Source]             NVARCHAR (50)   NULL,
    [SourceReference]    NVARCHAR (50)   NULL,
    [SourceDescription]  NVARCHAR (1024) NULL,
    [CreatedBy]          NVARCHAR (50)   NOT NULL,
    [CreatedDate]        SMALLDATETIME   NOT NULL,
    [ModifiedBy]         NVARCHAR (50)   NULL,
    [ModifiedDate]       SMALLDATETIME   NULL,
    [RiskClaim_Id]       INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]   INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]            NVARCHAR (50)   NULL,
    [RecordStatus]       TINYINT         CONSTRAINT [DF_Vehicle2Organisation_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]    TINYINT         CONSTRAINT [DF_Vehicle2Organisation_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]         ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Vehicle2Organisation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vehicle2Organisation_Organisation] FOREIGN KEY ([Organisation_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Vehicle2Organisation_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Vehicle2Organisation_Vehicle] FOREIGN KEY ([Vehicle_Id]) REFERENCES [dbo].[Vehicle] ([Id]),
    CONSTRAINT [FK_Vehicle2Organisation_VehicleLinkType] FOREIGN KEY ([VehicleLinkType_Id]) REFERENCES [dbo].[VehicleLinkType] ([Id])
);


GO
ALTER TABLE [dbo].[Vehicle2Organisation] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




















GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Organisation_RiskClaimId]
    ON [dbo].[Vehicle2Organisation]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Organisation_Vehicle_Id]
    ON [dbo].[Vehicle2Organisation]([Vehicle_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Organisation_Organisation_Id]
    ON [dbo].[Vehicle2Organisation]([Organisation_Id] ASC);

