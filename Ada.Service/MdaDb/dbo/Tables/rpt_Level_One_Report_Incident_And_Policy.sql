﻿CREATE TABLE [dbo].[rpt_Level_One_Report_Incident_And_Policy] (
    [RiskClaim_Id]     INT           NULL,
    [LatestVersion]    INT           NULL,
    [CreateDate]       DATETIME      NULL,
    [LastRunDate]      DATETIME      NULL,
    [IncidentType]     VARCHAR (50)  NULL,
    [ClaimNumber]      VARCHAR (50)  NULL,
    [ClaimStatus]      VARCHAR (50)  NULL,
    [IncidentDate]     DATE          NULL,
    [IncidentDateTime] DATETIME      NULL,
    [ClaimCode]        VARCHAR (50)  NULL,
    [Reserve]          MONEY         NULL,
    [PaymentsToDate]   MONEY         NULL,
    [Location]         VARCHAR (50)  NULL,
    [Circumstances]    VARCHAR (500) NULL,
    [PolicyNumber]     VARCHAR (50)  NULL,
    [PolicyStartDate]  DATE          NULL,
    [PolicyEndDate]    DATE          NULL,
    [Broker]           VARCHAR (50)  NOT NULL,
    [PolicyType]       VARCHAR (50)  NULL,
    [CoverType]        VARCHAR (50)  NULL,
    [MessageHeader]    VARCHAR (100) NULL,
    [ReportHeader]     VARCHAR (100) NULL
);



