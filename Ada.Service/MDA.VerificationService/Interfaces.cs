﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common;
using MDA.Common.Server;
using MDA.DAL;


namespace MDA.VerificationService.Interface
{
    public interface IVerificationService
    {
        /// <summary>
        /// Perform initial validation when source is a file
        /// </summary>
        /// <param name="file">The uploaded file (any format)</param>
        /// <returns></returns>
        ProcessingResults QuickVerification(Stream file, MDA.Common.Server.CurrentContext ctx, string expectedFileExtns);
    }

    public interface IValidationService
    {
        /// <summary>
        /// Perform full validation on ClaimBatch structure
        /// </summary>
        /// <param name="batch">The complete ClaimBatch structure</param>
        /// <returns></returns>
        ProcessingResults ValidateClaim(MDA.Pipeline.Model.IPipelineClaim claim);
    }
}
